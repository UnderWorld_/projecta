{
    "id": "8c8362b3-2818-443a-8453-8c5e480daedb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5afac65b-f3f3-42bf-835e-8a93d0c5ccc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c8362b3-2818-443a-8453-8c5e480daedb",
            "compositeImage": {
                "id": "1c69c28e-b6ff-4562-9eba-579a0315c50c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5afac65b-f3f3-42bf-835e-8a93d0c5ccc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4ff9bb-36dd-404d-bae0-bdc620e8a338",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5afac65b-f3f3-42bf-835e-8a93d0c5ccc1",
                    "LayerId": "4389755a-4148-4da8-b144-a379fe528024"
                }
            ]
        },
        {
            "id": "2f8b1def-7527-48ff-9630-ee0b10c9f43c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c8362b3-2818-443a-8453-8c5e480daedb",
            "compositeImage": {
                "id": "cab4e9ee-92a3-4ff4-be67-a6b09951126b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f8b1def-7527-48ff-9630-ee0b10c9f43c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "159272dc-cbc0-45d8-85b8-c2853c2c0cfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f8b1def-7527-48ff-9630-ee0b10c9f43c",
                    "LayerId": "4389755a-4148-4da8-b144-a379fe528024"
                }
            ]
        },
        {
            "id": "f588e574-47b1-43ed-aeb6-99936026e1b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c8362b3-2818-443a-8453-8c5e480daedb",
            "compositeImage": {
                "id": "fe40e4fd-e8b9-4624-b7e2-795d33812826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f588e574-47b1-43ed-aeb6-99936026e1b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59322871-3c53-4539-b465-4b0c9b72d5d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f588e574-47b1-43ed-aeb6-99936026e1b4",
                    "LayerId": "4389755a-4148-4da8-b144-a379fe528024"
                }
            ]
        },
        {
            "id": "d092cded-617b-4edb-91a8-2fc9cf8da920",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c8362b3-2818-443a-8453-8c5e480daedb",
            "compositeImage": {
                "id": "7cce6205-bb71-47db-897c-64790c842d6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d092cded-617b-4edb-91a8-2fc9cf8da920",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92e8af37-65fb-483b-bce3-f98ebd9ff583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d092cded-617b-4edb-91a8-2fc9cf8da920",
                    "LayerId": "4389755a-4148-4da8-b144-a379fe528024"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4389755a-4148-4da8-b144-a379fe528024",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c8362b3-2818-443a-8453-8c5e480daedb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}