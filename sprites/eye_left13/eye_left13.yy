{
    "id": "836e0f4a-b420-4cd8-ae97-4cdb21731008",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55941a6f-9b12-4328-bad1-0a696b822389",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836e0f4a-b420-4cd8-ae97-4cdb21731008",
            "compositeImage": {
                "id": "050d19b5-48e5-4a27-bbf8-9dca95e15514",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55941a6f-9b12-4328-bad1-0a696b822389",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a973dda-17d7-4a66-a4d7-b75c5182dcd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55941a6f-9b12-4328-bad1-0a696b822389",
                    "LayerId": "f3cd4502-25e8-4d41-a638-a8eb1397ced9"
                }
            ]
        },
        {
            "id": "6d46913e-bfc4-4103-9329-9a666cab6c88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "836e0f4a-b420-4cd8-ae97-4cdb21731008",
            "compositeImage": {
                "id": "5f33533b-4f27-441b-96a9-e29f9646b241",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d46913e-bfc4-4103-9329-9a666cab6c88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be864be5-3db9-4efd-8765-fe7c0629e748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d46913e-bfc4-4103-9329-9a666cab6c88",
                    "LayerId": "f3cd4502-25e8-4d41-a638-a8eb1397ced9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f3cd4502-25e8-4d41-a638-a8eb1397ced9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "836e0f4a-b420-4cd8-ae97-4cdb21731008",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}