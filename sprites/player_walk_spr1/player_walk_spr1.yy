{
    "id": "97241344-1ef5-4d2e-a4bf-e91b9e4741ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_walk_spr1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 142,
    "bbox_left": 37,
    "bbox_right": 108,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33d5f88d-6386-4c1d-92df-1206abe4891a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97241344-1ef5-4d2e-a4bf-e91b9e4741ac",
            "compositeImage": {
                "id": "42797ee6-6211-4700-9b38-783223ba8b6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33d5f88d-6386-4c1d-92df-1206abe4891a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f91c430c-f931-4735-92ad-b4f0ded9f67b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33d5f88d-6386-4c1d-92df-1206abe4891a",
                    "LayerId": "e942c5e8-1b7d-43b3-ac6d-6254b0ec7e71"
                }
            ]
        },
        {
            "id": "6430561a-319e-4c3d-b6b4-056512e60f23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97241344-1ef5-4d2e-a4bf-e91b9e4741ac",
            "compositeImage": {
                "id": "a2a9e9c5-cc7d-4cc3-a905-980a808f667a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6430561a-319e-4c3d-b6b4-056512e60f23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1627f66e-1d09-436b-8fde-01a4636ee1e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6430561a-319e-4c3d-b6b4-056512e60f23",
                    "LayerId": "e942c5e8-1b7d-43b3-ac6d-6254b0ec7e71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "e942c5e8-1b7d-43b3-ac6d-6254b0ec7e71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97241344-1ef5-4d2e-a4bf-e91b9e4741ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 36,
    "yorig": 47
}