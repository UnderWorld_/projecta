{
    "id": "6a0a5f03-0596-4873-b2ac-417bf47417ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin4_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b29b7d83-ecda-4ea9-9c49-a47fa6eaf31b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a0a5f03-0596-4873-b2ac-417bf47417ab",
            "compositeImage": {
                "id": "e9fc577c-fa01-4a58-a0b2-ba0355800b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b29b7d83-ecda-4ea9-9c49-a47fa6eaf31b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b27401-35ee-49a8-8290-c628b82fcd98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b29b7d83-ecda-4ea9-9c49-a47fa6eaf31b",
                    "LayerId": "fa17abe7-4efb-42df-97b5-a6afc372b6af"
                }
            ]
        },
        {
            "id": "2bc1c9fc-9073-430e-bee2-d6cabdf6c269",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a0a5f03-0596-4873-b2ac-417bf47417ab",
            "compositeImage": {
                "id": "abb6c432-e619-4dad-8861-db9f8ada68e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bc1c9fc-9073-430e-bee2-d6cabdf6c269",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18646028-1e24-4176-8e2f-0dc1328b091f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bc1c9fc-9073-430e-bee2-d6cabdf6c269",
                    "LayerId": "fa17abe7-4efb-42df-97b5-a6afc372b6af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "fa17abe7-4efb-42df-97b5-a6afc372b6af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a0a5f03-0596-4873-b2ac-417bf47417ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}