{
    "id": "0c5683d1-4d6c-4e5c-8802-6987848cabad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bunny1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 16,
    "bbox_right": 191,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24dc8ab8-e40e-4d25-8841-b0a7210bf4e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c5683d1-4d6c-4e5c-8802-6987848cabad",
            "compositeImage": {
                "id": "d2a37b38-fa9a-4432-af69-676d9689e739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24dc8ab8-e40e-4d25-8841-b0a7210bf4e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10035ecd-00ed-49f1-af4e-2d7d63231cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24dc8ab8-e40e-4d25-8841-b0a7210bf4e4",
                    "LayerId": "b88741a7-2d08-4c66-8678-8c9d34458e4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b88741a7-2d08-4c66-8678-8c9d34458e4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c5683d1-4d6c-4e5c-8802-6987848cabad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 101,
    "yorig": 206
}