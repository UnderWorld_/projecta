{
    "id": "0304eb3f-37b6-4ddd-8c2c-2c76bba85bdd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin3_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f734e80-5af5-4922-83b7-d4cbb94c2c39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0304eb3f-37b6-4ddd-8c2c-2c76bba85bdd",
            "compositeImage": {
                "id": "d2657006-3c00-4147-91af-26b446457535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f734e80-5af5-4922-83b7-d4cbb94c2c39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b786e988-eb7e-4689-ae63-2d4e350deb36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f734e80-5af5-4922-83b7-d4cbb94c2c39",
                    "LayerId": "4d1d8a93-18e9-465b-aa33-6aa90022e93d"
                }
            ]
        },
        {
            "id": "b72c19a3-412c-4a34-96ad-4aadde0da39c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0304eb3f-37b6-4ddd-8c2c-2c76bba85bdd",
            "compositeImage": {
                "id": "9ff06e0c-d3dc-43ee-ba98-7646c5f64b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b72c19a3-412c-4a34-96ad-4aadde0da39c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f06f38f1-c446-4df9-b981-d20e0382d8c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b72c19a3-412c-4a34-96ad-4aadde0da39c",
                    "LayerId": "4d1d8a93-18e9-465b-aa33-6aa90022e93d"
                }
            ]
        },
        {
            "id": "cdad60db-6e01-4694-a3d6-df2841407e66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0304eb3f-37b6-4ddd-8c2c-2c76bba85bdd",
            "compositeImage": {
                "id": "53a37147-e601-4939-bd90-1375e8a3e540",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdad60db-6e01-4694-a3d6-df2841407e66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d013479-7792-4210-8be1-c50a78b19706",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdad60db-6e01-4694-a3d6-df2841407e66",
                    "LayerId": "4d1d8a93-18e9-465b-aa33-6aa90022e93d"
                }
            ]
        },
        {
            "id": "32ced57e-e084-474a-a54b-53b317915610",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0304eb3f-37b6-4ddd-8c2c-2c76bba85bdd",
            "compositeImage": {
                "id": "d3aa84a6-6b9d-4f74-8af6-cec4189727ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32ced57e-e084-474a-a54b-53b317915610",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72aed032-e650-4703-81f9-ed7f06dac9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32ced57e-e084-474a-a54b-53b317915610",
                    "LayerId": "4d1d8a93-18e9-465b-aa33-6aa90022e93d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4d1d8a93-18e9-465b-aa33-6aa90022e93d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0304eb3f-37b6-4ddd-8c2c-2c76bba85bdd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}