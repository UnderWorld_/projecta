{
    "id": "5f093ca9-54fa-4307-aa43-41fa28d59c19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cat_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a78c8e5f-a4fd-4002-aeee-0c4b74dad811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f093ca9-54fa-4307-aa43-41fa28d59c19",
            "compositeImage": {
                "id": "98f6c043-17d0-43c9-b416-821702b03592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a78c8e5f-a4fd-4002-aeee-0c4b74dad811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77b55532-9fb6-438e-af46-a76036267dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a78c8e5f-a4fd-4002-aeee-0c4b74dad811",
                    "LayerId": "765fccdd-5a70-4d73-ae73-ed45b128e535"
                }
            ]
        },
        {
            "id": "b5924c49-9742-42c2-a84f-f6eb632d9470",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5f093ca9-54fa-4307-aa43-41fa28d59c19",
            "compositeImage": {
                "id": "41462d0d-b478-4ae5-9cb3-2061ef705966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5924c49-9742-42c2-a84f-f6eb632d9470",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "893ad248-20cd-42f8-9791-ba3a156e5358",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5924c49-9742-42c2-a84f-f6eb632d9470",
                    "LayerId": "765fccdd-5a70-4d73-ae73-ed45b128e535"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "765fccdd-5a70-4d73-ae73-ed45b128e535",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5f093ca9-54fa-4307-aa43-41fa28d59c19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}