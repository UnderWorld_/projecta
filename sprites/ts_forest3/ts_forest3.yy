{
    "id": "ada3de1d-c81a-4434-aca3-8d6bb6edc88e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_forest3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 46,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5d708586-347b-455d-9d8c-c1a88198ea1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ada3de1d-c81a-4434-aca3-8d6bb6edc88e",
            "compositeImage": {
                "id": "2857a8d4-b232-45d9-8165-d4aaea23263f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d708586-347b-455d-9d8c-c1a88198ea1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb7d85a6-4a48-498f-aca7-be5eded7d516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d708586-347b-455d-9d8c-c1a88198ea1f",
                    "LayerId": "94661ca5-c762-4115-9d44-b820ee3b8540"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "94661ca5-c762-4115-9d44-b820ee3b8540",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ada3de1d-c81a-4434-aca3-8d6bb6edc88e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}