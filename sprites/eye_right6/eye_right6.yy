{
    "id": "8f8a304e-a38b-4544-af11-944e73feb16e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "148733ec-0add-476e-a826-86f86a38ddbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f8a304e-a38b-4544-af11-944e73feb16e",
            "compositeImage": {
                "id": "2cc7ad87-b14a-4ff0-9fde-ebc2cd40f21f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148733ec-0add-476e-a826-86f86a38ddbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29d8e91f-f2a4-4ccb-9b28-56570e5db2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148733ec-0add-476e-a826-86f86a38ddbd",
                    "LayerId": "be973ea6-ad29-4e6f-9ef9-9efa6436367f"
                }
            ]
        },
        {
            "id": "70ab3240-12af-4cb3-8832-1858e60344c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f8a304e-a38b-4544-af11-944e73feb16e",
            "compositeImage": {
                "id": "d1470e64-1166-4d38-bb70-0b33c3de361f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70ab3240-12af-4cb3-8832-1858e60344c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95569679-d936-47ff-9dd5-a9a44aa313df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70ab3240-12af-4cb3-8832-1858e60344c0",
                    "LayerId": "be973ea6-ad29-4e6f-9ef9-9efa6436367f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "be973ea6-ad29-4e6f-9ef9-9efa6436367f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f8a304e-a38b-4544-af11-944e73feb16e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}