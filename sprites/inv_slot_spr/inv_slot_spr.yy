{
    "id": "29b0fa07-4252-455e-99ab-a43e7ea1609a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "inv_slot_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b175416e-17c0-4128-b5cb-5d78150c3339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29b0fa07-4252-455e-99ab-a43e7ea1609a",
            "compositeImage": {
                "id": "59e4920f-c842-4f69-ae2b-2f05914e2266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b175416e-17c0-4128-b5cb-5d78150c3339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4b5ccf7-dfb7-4ee3-862a-1f0098bc379a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b175416e-17c0-4128-b5cb-5d78150c3339",
                    "LayerId": "dcfb9c30-1ba6-41d1-97ba-1db149e06a32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "dcfb9c30-1ba6-41d1-97ba-1db149e06a32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29b0fa07-4252-455e-99ab-a43e7ea1609a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}