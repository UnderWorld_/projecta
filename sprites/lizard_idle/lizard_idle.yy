{
    "id": "8faf70be-12f8-4d85-8009-eb502cd37814",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lizard_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 20,
    "bbox_right": 67,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fc14e1d-f35d-4ff8-9f14-d5f377583014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf70be-12f8-4d85-8009-eb502cd37814",
            "compositeImage": {
                "id": "fb94ddae-beb5-46e3-9579-cd7a29da34b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fc14e1d-f35d-4ff8-9f14-d5f377583014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8489f45-f4a2-4772-aa4b-f8ae01a11e97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fc14e1d-f35d-4ff8-9f14-d5f377583014",
                    "LayerId": "566cbfa2-caba-4452-a2dc-57ab57eb74c6"
                }
            ]
        },
        {
            "id": "f1f91b7e-be67-4bc1-9062-7755af8a5283",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8faf70be-12f8-4d85-8009-eb502cd37814",
            "compositeImage": {
                "id": "261bb145-4918-474e-a196-b2dd6d30591e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1f91b7e-be67-4bc1-9062-7755af8a5283",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50252f4c-39e7-4398-9889-00bb3a96ad30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1f91b7e-be67-4bc1-9062-7755af8a5283",
                    "LayerId": "566cbfa2-caba-4452-a2dc-57ab57eb74c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "566cbfa2-caba-4452-a2dc-57ab57eb74c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8faf70be-12f8-4d85-8009-eb502cd37814",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 39,
    "yorig": 4
}