{
    "id": "a911f7ba-972c-4a1c-8982-65791fba6a4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin3_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e11473ad-fb80-4165-8aaf-5453f5ab3d2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a911f7ba-972c-4a1c-8982-65791fba6a4c",
            "compositeImage": {
                "id": "608151fe-ccaf-4673-8a2d-9306233ad7b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e11473ad-fb80-4165-8aaf-5453f5ab3d2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea8ea8d-11dc-4dad-b40e-97de4b269345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e11473ad-fb80-4165-8aaf-5453f5ab3d2b",
                    "LayerId": "691f0b86-9e45-45d7-8ef9-673ddda05441"
                }
            ]
        },
        {
            "id": "89bb5c52-f249-4ee4-8372-535a7ce73686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a911f7ba-972c-4a1c-8982-65791fba6a4c",
            "compositeImage": {
                "id": "bddda897-97e0-4536-93fc-f48537d2bf03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89bb5c52-f249-4ee4-8372-535a7ce73686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a684da3b-44cf-454d-b3a2-9447f62119b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89bb5c52-f249-4ee4-8372-535a7ce73686",
                    "LayerId": "691f0b86-9e45-45d7-8ef9-673ddda05441"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "691f0b86-9e45-45d7-8ef9-673ddda05441",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a911f7ba-972c-4a1c-8982-65791fba6a4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}