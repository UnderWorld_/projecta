{
    "id": "fa96e6b6-fbdb-44e5-81e4-a00372a20ac1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair_creation_scr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 10,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d30bb4ac-61ca-462d-b893-396968ab3b5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa96e6b6-fbdb-44e5-81e4-a00372a20ac1",
            "compositeImage": {
                "id": "14679f03-da84-4e48-86a5-5b9385b98813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d30bb4ac-61ca-462d-b893-396968ab3b5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "477646bf-d1b6-49aa-aa45-c8faffb09990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d30bb4ac-61ca-462d-b893-396968ab3b5a",
                    "LayerId": "2fea0138-3399-4968-ada1-3da160818771"
                }
            ]
        },
        {
            "id": "4b5e5f5b-8f33-48dd-abd4-abf38079cdc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa96e6b6-fbdb-44e5-81e4-a00372a20ac1",
            "compositeImage": {
                "id": "aa04b731-a01b-4a2c-adef-a0c18cdba557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b5e5f5b-8f33-48dd-abd4-abf38079cdc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee828d7-7364-4e9f-b7cb-68b9ef358b85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b5e5f5b-8f33-48dd-abd4-abf38079cdc7",
                    "LayerId": "2fea0138-3399-4968-ada1-3da160818771"
                }
            ]
        },
        {
            "id": "92be9423-e327-4592-97e7-1ec1c538b96d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa96e6b6-fbdb-44e5-81e4-a00372a20ac1",
            "compositeImage": {
                "id": "1ae26729-4126-4c3f-a574-59e2467b3844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92be9423-e327-4592-97e7-1ec1c538b96d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50cf6a4d-99c8-46b8-aeaf-d6ca535b1f5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92be9423-e327-4592-97e7-1ec1c538b96d",
                    "LayerId": "2fea0138-3399-4968-ada1-3da160818771"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "2fea0138-3399-4968-ada1-3da160818771",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa96e6b6-fbdb-44e5-81e4-a00372a20ac1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}