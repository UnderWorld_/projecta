{
    "id": "2966cd0c-89fe-4ee8-9755-51dd2de3631d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cat_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d9ae028-7ead-47d0-868d-50357ff0f556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2966cd0c-89fe-4ee8-9755-51dd2de3631d",
            "compositeImage": {
                "id": "c1c02fc0-2c43-40ae-a074-f3aad7b407bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d9ae028-7ead-47d0-868d-50357ff0f556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b11fb11-beb7-4dae-846b-00821ad898a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d9ae028-7ead-47d0-868d-50357ff0f556",
                    "LayerId": "e92d61f8-3e6e-402f-a668-4c8be03d9525"
                }
            ]
        },
        {
            "id": "d9a41396-0022-468e-ba04-eed784ae2eec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2966cd0c-89fe-4ee8-9755-51dd2de3631d",
            "compositeImage": {
                "id": "f812c6a5-5870-4673-b9ad-4f1774ab3c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9a41396-0022-468e-ba04-eed784ae2eec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39238997-f402-43fd-9db2-75a912d70c57",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9a41396-0022-468e-ba04-eed784ae2eec",
                    "LayerId": "e92d61f8-3e6e-402f-a668-4c8be03d9525"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e92d61f8-3e6e-402f-a668-4c8be03d9525",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2966cd0c-89fe-4ee8-9755-51dd2de3631d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}