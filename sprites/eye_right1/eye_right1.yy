{
    "id": "1b7edf78-133e-4ca9-b374-d0588e3a9562",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fe67e4f-b70e-4213-b344-9c0ce088ce2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b7edf78-133e-4ca9-b374-d0588e3a9562",
            "compositeImage": {
                "id": "84579a46-a8ef-4c4f-8395-99c0a15123fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fe67e4f-b70e-4213-b344-9c0ce088ce2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47acdf1f-4679-482b-a8fc-e53b5e86efc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fe67e4f-b70e-4213-b344-9c0ce088ce2f",
                    "LayerId": "cb7f05bc-c7d3-4441-9f1e-fc80309c0a32"
                }
            ]
        },
        {
            "id": "541edd1a-366d-4fdb-96ec-eea32bfe0647",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b7edf78-133e-4ca9-b374-d0588e3a9562",
            "compositeImage": {
                "id": "3e868202-920c-451a-9eea-b9a0f40e4c8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "541edd1a-366d-4fdb-96ec-eea32bfe0647",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51c2d3dc-3740-4f16-bdbe-f48796e0fce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "541edd1a-366d-4fdb-96ec-eea32bfe0647",
                    "LayerId": "cb7f05bc-c7d3-4441-9f1e-fc80309c0a32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "cb7f05bc-c7d3-4441-9f1e-fc80309c0a32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b7edf78-133e-4ca9-b374-d0588e3a9562",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}