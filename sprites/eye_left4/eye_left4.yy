{
    "id": "e1a152f0-4434-486a-b3e2-f9e18eccd7ee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d07b82a2-1be2-4611-8344-30bc5275fea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1a152f0-4434-486a-b3e2-f9e18eccd7ee",
            "compositeImage": {
                "id": "5f1d8aef-d65a-4f7a-8c9e-c92e5fa05e10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07b82a2-1be2-4611-8344-30bc5275fea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f185b73e-2e1e-4477-88e8-2d201955d29f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07b82a2-1be2-4611-8344-30bc5275fea6",
                    "LayerId": "96251c6b-b70a-4dd7-b9eb-d277b6ed0af7"
                }
            ]
        },
        {
            "id": "9ab297b0-d938-4896-aefd-7bd647219595",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1a152f0-4434-486a-b3e2-f9e18eccd7ee",
            "compositeImage": {
                "id": "471fcd7d-af9a-441d-bbee-28cb1340db8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab297b0-d938-4896-aefd-7bd647219595",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af4721fb-baa8-4252-aee3-8488d2d7ab31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab297b0-d938-4896-aefd-7bd647219595",
                    "LayerId": "96251c6b-b70a-4dd7-b9eb-d277b6ed0af7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "96251c6b-b70a-4dd7-b9eb-d277b6ed0af7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1a152f0-4434-486a-b3e2-f9e18eccd7ee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}