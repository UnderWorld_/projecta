{
    "id": "1f2f25b8-b81f-4ffc-adb2-d9c8f9e21f4b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5f27d68a-5c49-42ca-8319-6f4e1a4dd36a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f2f25b8-b81f-4ffc-adb2-d9c8f9e21f4b",
            "compositeImage": {
                "id": "8f8681dd-67ac-4684-a032-3c9e062a5495",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f27d68a-5c49-42ca-8319-6f4e1a4dd36a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "443baaf7-5aba-4481-b8bf-95da817567e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f27d68a-5c49-42ca-8319-6f4e1a4dd36a",
                    "LayerId": "6281c0cc-43d0-4bcc-947e-f42e47a5fa1d"
                }
            ]
        },
        {
            "id": "17fa0c41-1d2a-4b0b-b26d-da2e97980e78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f2f25b8-b81f-4ffc-adb2-d9c8f9e21f4b",
            "compositeImage": {
                "id": "fda5188f-f0bb-4029-83cc-79eebeacc076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17fa0c41-1d2a-4b0b-b26d-da2e97980e78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76901874-a172-4187-9c1f-fc49b52b73ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17fa0c41-1d2a-4b0b-b26d-da2e97980e78",
                    "LayerId": "6281c0cc-43d0-4bcc-947e-f42e47a5fa1d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "6281c0cc-43d0-4bcc-947e-f42e47a5fa1d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f2f25b8-b81f-4ffc-adb2-d9c8f9e21f4b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}