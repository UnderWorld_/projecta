{
    "id": "915716fa-19cd-4a35-a70c-c7d611ccf909",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "terrain3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bcc980b-2e77-4a14-b81b-36d18b3cd273",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "915716fa-19cd-4a35-a70c-c7d611ccf909",
            "compositeImage": {
                "id": "95f91dfa-a8f4-4282-808e-13889ca09eb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bcc980b-2e77-4a14-b81b-36d18b3cd273",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ca4b4ff-2c3c-42b7-98a5-add1f532bfcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bcc980b-2e77-4a14-b81b-36d18b3cd273",
                    "LayerId": "5c5020a4-aac7-4260-a359-9df4da944940"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5c5020a4-aac7-4260-a359-9df4da944940",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "915716fa-19cd-4a35-a70c-c7d611ccf909",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}