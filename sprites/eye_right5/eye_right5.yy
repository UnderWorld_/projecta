{
    "id": "8e65ff3c-c8fb-4495-93b0-e3abb16f3fb1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f86f0247-5b76-420d-a931-f0e098b4c969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e65ff3c-c8fb-4495-93b0-e3abb16f3fb1",
            "compositeImage": {
                "id": "72916d2b-25fa-4c1e-8c11-ef62129e60e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f86f0247-5b76-420d-a931-f0e098b4c969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80523e09-858c-477c-ae9c-36f0daa6e76f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f86f0247-5b76-420d-a931-f0e098b4c969",
                    "LayerId": "4e7c58db-ec7f-44f0-97d5-7c552f558130"
                }
            ]
        },
        {
            "id": "0dc28ea7-9697-49bb-ba6e-28f736c2d8fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e65ff3c-c8fb-4495-93b0-e3abb16f3fb1",
            "compositeImage": {
                "id": "f96161b8-1151-4e98-8b7a-acbbb9201d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dc28ea7-9697-49bb-ba6e-28f736c2d8fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81a21932-02ae-4cf5-bb76-aeaa1e553ff3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dc28ea7-9697-49bb-ba6e-28f736c2d8fc",
                    "LayerId": "4e7c58db-ec7f-44f0-97d5-7c552f558130"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4e7c58db-ec7f-44f0-97d5-7c552f558130",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e65ff3c-c8fb-4495-93b0-e3abb16f3fb1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}