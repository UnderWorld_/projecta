{
    "id": "97e3eaae-1261-4195-b3dd-94eff3dc4364",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6f7445f-068e-44c9-8587-80b27447dc1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97e3eaae-1261-4195-b3dd-94eff3dc4364",
            "compositeImage": {
                "id": "ad116d5a-e166-43d3-817d-6aa5ccd708f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6f7445f-068e-44c9-8587-80b27447dc1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7fbc03-8fd4-47ee-bf88-3e417fb23352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6f7445f-068e-44c9-8587-80b27447dc1b",
                    "LayerId": "f684390a-2180-4232-94d4-fb8a24d38b27"
                }
            ]
        },
        {
            "id": "a9cef58d-1f00-496b-8caf-0e14b964961c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "97e3eaae-1261-4195-b3dd-94eff3dc4364",
            "compositeImage": {
                "id": "2bad8393-5738-4b89-a67e-da8e5df06460",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9cef58d-1f00-496b-8caf-0e14b964961c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c60fce4-dc7c-4cce-b5c9-141ae05fdfd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9cef58d-1f00-496b-8caf-0e14b964961c",
                    "LayerId": "f684390a-2180-4232-94d4-fb8a24d38b27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f684390a-2180-4232-94d4-fb8a24d38b27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "97e3eaae-1261-4195-b3dd-94eff3dc4364",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}