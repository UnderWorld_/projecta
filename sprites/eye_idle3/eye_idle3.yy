{
    "id": "33a69447-90d0-47db-bf30-70f1fb892a7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2bdbad0-71e5-461a-bc2c-e2adf92820f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a69447-90d0-47db-bf30-70f1fb892a7a",
            "compositeImage": {
                "id": "c74789b0-d7b9-4078-9d3d-bc53b92a6204",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2bdbad0-71e5-461a-bc2c-e2adf92820f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fd9c7f-838f-4ef1-8e70-0a37cd082aa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2bdbad0-71e5-461a-bc2c-e2adf92820f2",
                    "LayerId": "f909144e-7555-4025-a23e-367e529275f2"
                }
            ]
        },
        {
            "id": "11e28227-20ab-4400-b423-cd0f756a6ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a69447-90d0-47db-bf30-70f1fb892a7a",
            "compositeImage": {
                "id": "34e5345e-7c31-42be-b0c5-033135f0d07a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e28227-20ab-4400-b423-cd0f756a6ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6a7c19e-3b43-4220-b55e-ca5fa50a3226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e28227-20ab-4400-b423-cd0f756a6ed1",
                    "LayerId": "f909144e-7555-4025-a23e-367e529275f2"
                }
            ]
        },
        {
            "id": "91d12713-9907-42cc-8fbf-d1fd19e9e7c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a69447-90d0-47db-bf30-70f1fb892a7a",
            "compositeImage": {
                "id": "b2dc05ad-8961-4f14-9bb7-f77a1c4b92d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91d12713-9907-42cc-8fbf-d1fd19e9e7c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9820767a-9216-4d9e-bd0c-d17c94406001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91d12713-9907-42cc-8fbf-d1fd19e9e7c4",
                    "LayerId": "f909144e-7555-4025-a23e-367e529275f2"
                }
            ]
        },
        {
            "id": "472dd5b5-defa-401b-b7ca-cad7a8c6c25b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33a69447-90d0-47db-bf30-70f1fb892a7a",
            "compositeImage": {
                "id": "a091316a-0116-43d0-8ab3-98dde6551c1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472dd5b5-defa-401b-b7ca-cad7a8c6c25b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73fdcc9d-504e-44fc-baa3-b479ec606820",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472dd5b5-defa-401b-b7ca-cad7a8c6c25b",
                    "LayerId": "f909144e-7555-4025-a23e-367e529275f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f909144e-7555-4025-a23e-367e529275f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33a69447-90d0-47db-bf30-70f1fb892a7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}