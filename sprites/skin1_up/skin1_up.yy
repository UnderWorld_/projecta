{
    "id": "c233159f-8d2a-430e-b995-420911361338",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin1_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85dd02da-389b-4a2a-8ce1-b89473569e15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c233159f-8d2a-430e-b995-420911361338",
            "compositeImage": {
                "id": "866047b0-c788-455a-b48b-9730310cc47e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85dd02da-389b-4a2a-8ce1-b89473569e15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3625ca81-84e4-48fd-8655-944c70e14f11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85dd02da-389b-4a2a-8ce1-b89473569e15",
                    "LayerId": "26fddcac-4591-4f00-99b7-ab49a88b1b29"
                }
            ]
        },
        {
            "id": "ddc51dfd-c827-4333-996c-f937bbca70e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c233159f-8d2a-430e-b995-420911361338",
            "compositeImage": {
                "id": "53c9c1bd-3eec-4549-9344-ab870b71effa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddc51dfd-c827-4333-996c-f937bbca70e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec3798a3-5a70-44d5-a079-960a5d468e92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddc51dfd-c827-4333-996c-f937bbca70e6",
                    "LayerId": "26fddcac-4591-4f00-99b7-ab49a88b1b29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "26fddcac-4591-4f00-99b7-ab49a88b1b29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c233159f-8d2a-430e-b995-420911361338",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}