{
    "id": "87fcf594-7c5a-4621-93ce-9d78bbcf5307",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f98ecf54-f635-4a8c-8efb-482d43185911",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87fcf594-7c5a-4621-93ce-9d78bbcf5307",
            "compositeImage": {
                "id": "77cc56ed-6468-4b7d-a6eb-87a6fc192ae3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98ecf54-f635-4a8c-8efb-482d43185911",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae43b441-c006-41bd-9aa8-5152abceb29b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98ecf54-f635-4a8c-8efb-482d43185911",
                    "LayerId": "f837d059-e168-4ac7-aa25-232181f1b694"
                }
            ]
        },
        {
            "id": "51d3e66e-fe02-4fba-9b55-929e6099ac10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "87fcf594-7c5a-4621-93ce-9d78bbcf5307",
            "compositeImage": {
                "id": "dfe818b9-021b-4c88-9f9c-0907e445409e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51d3e66e-fe02-4fba-9b55-929e6099ac10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90c1c7b2-7021-45e9-932c-fbdd2ee9a07a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51d3e66e-fe02-4fba-9b55-929e6099ac10",
                    "LayerId": "f837d059-e168-4ac7-aa25-232181f1b694"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "f837d059-e168-4ac7-aa25-232181f1b694",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "87fcf594-7c5a-4621-93ce-9d78bbcf5307",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 4,
    "yorig": 4
}