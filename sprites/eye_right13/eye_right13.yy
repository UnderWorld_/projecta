{
    "id": "3403a27d-44ef-439f-bd6d-06f36a6fcbde",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f6a7ece5-eaca-4aa3-aa4c-8b0dfcc5224b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3403a27d-44ef-439f-bd6d-06f36a6fcbde",
            "compositeImage": {
                "id": "c4c00a25-00ef-4641-87bf-4796a69b4703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6a7ece5-eaca-4aa3-aa4c-8b0dfcc5224b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e912dd1f-64c9-4019-832d-6ff03048fa33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6a7ece5-eaca-4aa3-aa4c-8b0dfcc5224b",
                    "LayerId": "1dd78ce5-d90a-4ed0-b225-3046e1ef690a"
                }
            ]
        },
        {
            "id": "883650e6-5c8c-45d7-a3a8-493aa07aaf1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3403a27d-44ef-439f-bd6d-06f36a6fcbde",
            "compositeImage": {
                "id": "76fc59b2-7840-4db2-aecb-6c3e54d8fb57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883650e6-5c8c-45d7-a3a8-493aa07aaf1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1eb52e2f-986d-4ed0-a02a-17a01c70d6ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883650e6-5c8c-45d7-a3a8-493aa07aaf1e",
                    "LayerId": "1dd78ce5-d90a-4ed0-b225-3046e1ef690a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "1dd78ce5-d90a-4ed0-b225-3046e1ef690a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3403a27d-44ef-439f-bd6d-06f36a6fcbde",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}