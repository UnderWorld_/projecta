{
    "id": "4de6e2e5-a49e-4e0d-9ef7-8f88ee5edc4d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dog_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 11,
    "bbox_right": 87,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "46d249cb-bc85-4d7b-9560-329fa0416df5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4de6e2e5-a49e-4e0d-9ef7-8f88ee5edc4d",
            "compositeImage": {
                "id": "529ef1f1-202d-467c-8a05-b813043c04a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46d249cb-bc85-4d7b-9560-329fa0416df5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dbf3afc-8cd4-4613-9a0a-96eb7d5cfbf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46d249cb-bc85-4d7b-9560-329fa0416df5",
                    "LayerId": "f9b235e3-9524-4906-a13b-cc6fdf0f5894"
                }
            ]
        },
        {
            "id": "fd95d473-c769-4a20-ad0e-d03384f72f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4de6e2e5-a49e-4e0d-9ef7-8f88ee5edc4d",
            "compositeImage": {
                "id": "db509bd4-3099-4b3d-a243-18254bc71c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd95d473-c769-4a20-ad0e-d03384f72f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91a7391f-0edc-40b3-89fb-c5425279ae77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd95d473-c769-4a20-ad0e-d03384f72f78",
                    "LayerId": "f9b235e3-9524-4906-a13b-cc6fdf0f5894"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "f9b235e3-9524-4906-a13b-cc6fdf0f5894",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4de6e2e5-a49e-4e0d-9ef7-8f88ee5edc4d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 84,
    "yorig": 130
}