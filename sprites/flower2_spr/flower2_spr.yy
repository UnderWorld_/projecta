{
    "id": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flower2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d12087f3-efb5-4c58-923d-92f93fe1bb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
            "compositeImage": {
                "id": "421a2ce8-494d-4594-835e-f67d4e28a971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d12087f3-efb5-4c58-923d-92f93fe1bb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "107ee2c3-741e-4214-81a3-466b32e15be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d12087f3-efb5-4c58-923d-92f93fe1bb3f",
                    "LayerId": "e596d869-068f-434e-92f4-8d3e72ff1c0a"
                }
            ]
        },
        {
            "id": "621f710f-6c9c-4f35-930f-fb43d7946065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
            "compositeImage": {
                "id": "7ac8e7c6-615d-4894-9778-57fdbef11f7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "621f710f-6c9c-4f35-930f-fb43d7946065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29b5139b-13aa-4b89-af48-7e0e8f5aec1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "621f710f-6c9c-4f35-930f-fb43d7946065",
                    "LayerId": "e596d869-068f-434e-92f4-8d3e72ff1c0a"
                }
            ]
        },
        {
            "id": "a48ce705-7269-449e-85e3-ad607d3c721c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
            "compositeImage": {
                "id": "df60c280-44ff-4a12-b1e3-b65861b34c66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a48ce705-7269-449e-85e3-ad607d3c721c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35e80bca-f0bb-4363-b421-d5bc92afc609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a48ce705-7269-449e-85e3-ad607d3c721c",
                    "LayerId": "e596d869-068f-434e-92f4-8d3e72ff1c0a"
                }
            ]
        },
        {
            "id": "ae4dd9ac-a85b-41c5-8782-e8d9548e94c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
            "compositeImage": {
                "id": "fdcf6224-698f-4f2e-863e-2c0ec79561c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae4dd9ac-a85b-41c5-8782-e8d9548e94c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc00e47d-c3bf-4589-801a-b96704efb738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae4dd9ac-a85b-41c5-8782-e8d9548e94c9",
                    "LayerId": "e596d869-068f-434e-92f4-8d3e72ff1c0a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e596d869-068f-434e-92f4-8d3e72ff1c0a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.01,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 17,
    "yorig": 31
}