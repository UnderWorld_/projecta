{
    "id": "bb9b28bc-723c-4cca-98ce-289d7ae3f49e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mushroom5_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "94602eb7-f0b7-494d-a342-b184cea784c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bb9b28bc-723c-4cca-98ce-289d7ae3f49e",
            "compositeImage": {
                "id": "de138eb9-1e61-4e8e-9ec7-1b7073622da2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94602eb7-f0b7-494d-a342-b184cea784c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f60daabf-8c10-4a36-8bcd-bfc31189a52a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94602eb7-f0b7-494d-a342-b184cea784c5",
                    "LayerId": "47a30666-8586-48d5-91b9-693593b868e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "47a30666-8586-48d5-91b9-693593b868e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bb9b28bc-723c-4cca-98ce-289d7ae3f49e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 16
}