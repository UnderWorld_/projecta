{
    "id": "7761b68d-b914-45e8-ae95-cfceecdd1683",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite46",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57c645b4-4b1d-4a61-8135-313b4344bb17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7761b68d-b914-45e8-ae95-cfceecdd1683",
            "compositeImage": {
                "id": "a718551b-c126-4d62-9a9a-f77c5c3d2d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57c645b4-4b1d-4a61-8135-313b4344bb17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70f34524-09d4-404d-96b3-8bef471222ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57c645b4-4b1d-4a61-8135-313b4344bb17",
                    "LayerId": "4b3703c4-2a45-4c7a-b709-de4afd95c2d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4b3703c4-2a45-4c7a-b709-de4afd95c2d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7761b68d-b914-45e8-ae95-cfceecdd1683",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 0,
    "yorig": 0
}