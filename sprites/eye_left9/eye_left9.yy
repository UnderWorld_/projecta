{
    "id": "c02de099-36f8-41d7-ab87-2eaf7237d042",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12c2f1ee-65c4-4ba6-b866-9e63c06e6b2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c02de099-36f8-41d7-ab87-2eaf7237d042",
            "compositeImage": {
                "id": "9b0665ff-fa68-4f92-808d-7b38fd671ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12c2f1ee-65c4-4ba6-b866-9e63c06e6b2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93503c7d-6489-4365-9a36-0e8ad5cc59c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12c2f1ee-65c4-4ba6-b866-9e63c06e6b2e",
                    "LayerId": "24ba259d-42d0-4ec2-b090-123d3cc0f38a"
                }
            ]
        },
        {
            "id": "9b8de820-3bb9-4e90-8504-e36a740b3d5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c02de099-36f8-41d7-ab87-2eaf7237d042",
            "compositeImage": {
                "id": "4af22433-862e-4895-97d9-62783c05a6c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b8de820-3bb9-4e90-8504-e36a740b3d5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8466af3a-9552-4fb0-bf98-8eef576dc581",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b8de820-3bb9-4e90-8504-e36a740b3d5e",
                    "LayerId": "24ba259d-42d0-4ec2-b090-123d3cc0f38a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "24ba259d-42d0-4ec2-b090-123d3cc0f38a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c02de099-36f8-41d7-ab87-2eaf7237d042",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}