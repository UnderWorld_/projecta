{
    "id": "66b9c13a-10a2-4ccb-b7b0-a11e6f2b2356",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 942,
    "bbox_left": 94,
    "bbox_right": 1006,
    "bbox_top": 267,
    "bboxmode": 2,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eec6a70c-8ce9-476b-81c4-bd64e937698b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "66b9c13a-10a2-4ccb-b7b0-a11e6f2b2356",
            "compositeImage": {
                "id": "db8ce3ce-8c42-401c-9fef-51d467d67a34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eec6a70c-8ce9-476b-81c4-bd64e937698b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0f49aa3-1191-4887-b764-f94b62698988",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eec6a70c-8ce9-476b-81c4-bd64e937698b",
                    "LayerId": "a60f6cb4-c8e5-47bc-8951-b517fba59e62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1316,
    "layers": [
        {
            "id": "a60f6cb4-c8e5-47bc-8951-b517fba59e62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "66b9c13a-10a2-4ccb-b7b0-a11e6f2b2356",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1040,
    "xorig": 657,
    "yorig": 1291
}