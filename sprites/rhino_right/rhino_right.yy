{
    "id": "1c22f5c7-39b5-420f-a4a7-933c0cf93925",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rhino_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 24,
    "bbox_right": 231,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04629c3b-8725-44a3-b598-fad75380369c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22f5c7-39b5-420f-a4a7-933c0cf93925",
            "compositeImage": {
                "id": "59c672da-e037-4223-aefd-c2bf026dd842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04629c3b-8725-44a3-b598-fad75380369c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63653a88-0a58-4f1d-b733-312c715e89cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04629c3b-8725-44a3-b598-fad75380369c",
                    "LayerId": "09d76356-d45c-4c29-90f5-5ef35142ca52"
                }
            ]
        },
        {
            "id": "7ae610bc-9c42-443e-89dd-2cdc430ef575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c22f5c7-39b5-420f-a4a7-933c0cf93925",
            "compositeImage": {
                "id": "7af152db-88ed-457b-af34-a6cbb3262599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ae610bc-9c42-443e-89dd-2cdc430ef575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2fd505e-bcc9-45fb-bd2b-a2973ff40014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ae610bc-9c42-443e-89dd-2cdc430ef575",
                    "LayerId": "09d76356-d45c-4c29-90f5-5ef35142ca52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "09d76356-d45c-4c29-90f5-5ef35142ca52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c22f5c7-39b5-420f-a4a7-933c0cf93925",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}