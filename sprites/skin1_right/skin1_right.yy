{
    "id": "1f1824a4-413a-4e84-871c-0093bd5f2aa8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin1_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ec15277e-80b7-416e-8a6b-7b7e2be42880",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f1824a4-413a-4e84-871c-0093bd5f2aa8",
            "compositeImage": {
                "id": "2e4afe6f-af08-4769-93b5-9fa29e5253e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec15277e-80b7-416e-8a6b-7b7e2be42880",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a267643c-bf56-41d6-8d85-f40e7e1f4f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec15277e-80b7-416e-8a6b-7b7e2be42880",
                    "LayerId": "fa1c8639-5e46-4dd9-a369-db333a598de7"
                }
            ]
        },
        {
            "id": "e2ac32f8-0ea8-42c6-8103-2e5d194c35d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f1824a4-413a-4e84-871c-0093bd5f2aa8",
            "compositeImage": {
                "id": "a86b0abb-600f-4494-a75f-22623c976c03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2ac32f8-0ea8-42c6-8103-2e5d194c35d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f2bf70c-cc9f-4c25-828f-41a2c3172a61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2ac32f8-0ea8-42c6-8103-2e5d194c35d9",
                    "LayerId": "fa1c8639-5e46-4dd9-a369-db333a598de7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "fa1c8639-5e46-4dd9-a369-db333a598de7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f1824a4-413a-4e84-871c-0093bd5f2aa8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}