{
    "id": "96e083d0-e3d3-44fd-a35c-3ee050c59e7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "inventory_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 387,
    "bbox_left": 0,
    "bbox_right": 346,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e40449f-cafb-40ed-b978-e2324090f835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96e083d0-e3d3-44fd-a35c-3ee050c59e7f",
            "compositeImage": {
                "id": "c4bc302e-7d4b-4eeb-a712-58df4fb9a065",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e40449f-cafb-40ed-b978-e2324090f835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "582654a6-0f46-4e17-b7c2-28261fa16efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e40449f-cafb-40ed-b978-e2324090f835",
                    "LayerId": "f56baae1-3105-45e6-9712-4d16c532b557"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 388,
    "layers": [
        {
            "id": "f56baae1-3105-45e6-9712-4d16c532b557",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96e083d0-e3d3-44fd-a35c-3ee050c59e7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 347,
    "xorig": 0,
    "yorig": 0
}