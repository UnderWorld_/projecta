{
    "id": "fb88a4f0-a79f-4673-bd4e-529b1a9fc3b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flower4_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "164907a4-6175-4b36-a450-fbbdb3d313ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb88a4f0-a79f-4673-bd4e-529b1a9fc3b6",
            "compositeImage": {
                "id": "de58190e-dfce-4bc6-ba20-7b3b48c03bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164907a4-6175-4b36-a450-fbbdb3d313ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e503ae7e-785d-4e46-9106-d0aad84e6916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164907a4-6175-4b36-a450-fbbdb3d313ef",
                    "LayerId": "abe5a295-72ee-4691-a76e-1bfc38bd4289"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "abe5a295-72ee-4691-a76e-1bfc38bd4289",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb88a4f0-a79f-4673-bd4e-529b1a9fc3b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}