{
    "id": "f529923e-1509-4367-8d23-fdfe1d7544f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "df_tree2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 976,
    "bbox_left": 176,
    "bbox_right": 225,
    "bbox_top": 784,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4873ef30-ccdd-4c0a-b9a7-c13baf1c26cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f529923e-1509-4367-8d23-fdfe1d7544f4",
            "compositeImage": {
                "id": "1e1f47d3-454c-452c-b268-8f5f5b2e1a73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4873ef30-ccdd-4c0a-b9a7-c13baf1c26cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6711645d-b340-4eaf-b345-bf2fa9778277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4873ef30-ccdd-4c0a-b9a7-c13baf1c26cb",
                    "LayerId": "7927cab0-b7ea-46b3-bcf2-5e83c0a0a639"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 990,
    "layers": [
        {
            "id": "7927cab0-b7ea-46b3-bcf2-5e83c0a0a639",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f529923e-1509-4367-8d23-fdfe1d7544f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 396,
    "xorig": 195,
    "yorig": 963
}