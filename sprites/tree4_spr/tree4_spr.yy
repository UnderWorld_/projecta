{
    "id": "212f931d-8b52-4000-a0db-7d609d6ce833",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree4_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 70,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f86dd22-a2ed-4349-b38d-fea5a8b7cae3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "212f931d-8b52-4000-a0db-7d609d6ce833",
            "compositeImage": {
                "id": "e1f2de8f-5088-40f1-a702-f2098e2710a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f86dd22-a2ed-4349-b38d-fea5a8b7cae3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75454d28-6698-4407-83f0-783a75ef085c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f86dd22-a2ed-4349-b38d-fea5a8b7cae3",
                    "LayerId": "f4ccc000-7821-477f-a379-46e48cbb3028"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 872,
    "layers": [
        {
            "id": "f4ccc000-7821-477f-a379-46e48cbb3028",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "212f931d-8b52-4000-a0db-7d609d6ce833",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 272,
    "xorig": 128,
    "yorig": 864
}