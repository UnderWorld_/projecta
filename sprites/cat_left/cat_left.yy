{
    "id": "8a8d70d8-3afa-4e55-b410-959b25c60913",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cat_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "722fd02c-4acf-4dbc-8afd-29514db72ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a8d70d8-3afa-4e55-b410-959b25c60913",
            "compositeImage": {
                "id": "aba6d5ba-6034-402a-8387-da958912e57a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "722fd02c-4acf-4dbc-8afd-29514db72ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "068cc943-cfa8-48bb-9586-67cf7ac676ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "722fd02c-4acf-4dbc-8afd-29514db72ab5",
                    "LayerId": "adfb9f4f-cdde-4699-84e5-1e06151444df"
                }
            ]
        },
        {
            "id": "8bfcd832-9fb3-4c8f-9479-e21d7f530f38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8a8d70d8-3afa-4e55-b410-959b25c60913",
            "compositeImage": {
                "id": "c065b7ce-6da3-43f4-92f4-9a18f57a0aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bfcd832-9fb3-4c8f-9479-e21d7f530f38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43717432-8b99-4a3e-ad26-b6831cdc63ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bfcd832-9fb3-4c8f-9479-e21d7f530f38",
                    "LayerId": "adfb9f4f-cdde-4699-84e5-1e06151444df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "adfb9f4f-cdde-4699-84e5-1e06151444df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8a8d70d8-3afa-4e55-b410-959b25c60913",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}