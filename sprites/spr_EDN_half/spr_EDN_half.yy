{
    "id": "e33a6b51-dff1-461d-a79f-8a1d3cde85b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_half",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 399,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb749583-495b-4d5c-9a5a-4152a743349d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e33a6b51-dff1-461d-a79f-8a1d3cde85b5",
            "compositeImage": {
                "id": "01d17421-8f22-44a7-b42e-5e251373f9da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb749583-495b-4d5c-9a5a-4152a743349d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2384307-71a3-4887-85e6-38b9d0568af0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb749583-495b-4d5c-9a5a-4152a743349d",
                    "LayerId": "4a75869a-06ec-46aa-8d7e-c2c5f9aa6d67"
                }
            ]
        },
        {
            "id": "1ee6ff40-eff7-462e-b4f1-66edf80bb9f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e33a6b51-dff1-461d-a79f-8a1d3cde85b5",
            "compositeImage": {
                "id": "6e72df14-c582-4710-b92b-9af86dcb874e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ee6ff40-eff7-462e-b4f1-66edf80bb9f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2227f079-1058-4a52-8923-840a8bbc3e08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ee6ff40-eff7-462e-b4f1-66edf80bb9f6",
                    "LayerId": "4a75869a-06ec-46aa-8d7e-c2c5f9aa6d67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "4a75869a-06ec-46aa-8d7e-c2c5f9aa6d67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e33a6b51-dff1-461d-a79f-8a1d3cde85b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 400,
    "xorig": 0,
    "yorig": 0
}