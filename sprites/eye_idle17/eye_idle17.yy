{
    "id": "bc5560b2-99d1-4e7f-aa87-876a0b3a364b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68133aaf-6485-4c99-9385-dd0f8eb0ecb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc5560b2-99d1-4e7f-aa87-876a0b3a364b",
            "compositeImage": {
                "id": "5ae0d5ce-3287-43fa-ab1c-9804477bfde4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68133aaf-6485-4c99-9385-dd0f8eb0ecb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0297d18-4e4f-4fe0-a3ea-e89cd1a5ef73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68133aaf-6485-4c99-9385-dd0f8eb0ecb7",
                    "LayerId": "f26320a7-b018-4630-8484-8170b84c0d15"
                }
            ]
        },
        {
            "id": "8d2e9d9c-009e-41b1-8ad8-406737012f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc5560b2-99d1-4e7f-aa87-876a0b3a364b",
            "compositeImage": {
                "id": "cf26d082-c86e-487a-921a-2cd79e527d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d2e9d9c-009e-41b1-8ad8-406737012f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c6f5428-6fc8-408b-a60a-e9a77fb4b592",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d2e9d9c-009e-41b1-8ad8-406737012f3c",
                    "LayerId": "f26320a7-b018-4630-8484-8170b84c0d15"
                }
            ]
        },
        {
            "id": "26d48c21-0208-42f6-9280-8dc5544ad5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc5560b2-99d1-4e7f-aa87-876a0b3a364b",
            "compositeImage": {
                "id": "f230196d-c15c-4b99-88cf-13efec223005",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26d48c21-0208-42f6-9280-8dc5544ad5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6ee65c-22de-4d08-9c05-0e1bd4b3e147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26d48c21-0208-42f6-9280-8dc5544ad5a4",
                    "LayerId": "f26320a7-b018-4630-8484-8170b84c0d15"
                }
            ]
        },
        {
            "id": "a4923c49-c9dc-4d37-b51b-f644924bc255",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bc5560b2-99d1-4e7f-aa87-876a0b3a364b",
            "compositeImage": {
                "id": "e957f236-0180-4259-8ec8-bbe7bbe5b21b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4923c49-c9dc-4d37-b51b-f644924bc255",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "865680ff-cb90-45fa-8859-5dafcf19cb85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4923c49-c9dc-4d37-b51b-f644924bc255",
                    "LayerId": "f26320a7-b018-4630-8484-8170b84c0d15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f26320a7-b018-4630-8484-8170b84c0d15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bc5560b2-99d1-4e7f-aa87-876a0b3a364b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}