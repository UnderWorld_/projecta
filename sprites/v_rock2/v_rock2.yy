{
    "id": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_rock2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a6959ec-b197-48ae-aed2-e48ae5eff5f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "a51b98cd-c470-4a90-87c1-b7b44dd6eba5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a6959ec-b197-48ae-aed2-e48ae5eff5f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3ee0ccd-c31d-44e7-a9c5-86876c54d2f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a6959ec-b197-48ae-aed2-e48ae5eff5f5",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "ebf22b08-f609-48d3-8128-b85045c85222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "e096fab7-a82e-419b-908c-9eb4e80296c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebf22b08-f609-48d3-8128-b85045c85222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebd5557-4c47-4527-89eb-beef41ccf008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebf22b08-f609-48d3-8128-b85045c85222",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "fe659e74-8336-4684-bda1-322d7a241eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "f8f27787-71b2-46d9-a126-39424af9a80e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe659e74-8336-4684-bda1-322d7a241eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a12daf9-328b-41e3-987d-a8dc7232073a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe659e74-8336-4684-bda1-322d7a241eaf",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "6a5c36e3-97a6-42cc-9020-1f34a6cd0a24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "2f684196-9daa-42ba-8523-508aafe817e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5c36e3-97a6-42cc-9020-1f34a6cd0a24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01d1afae-5740-4744-b5ac-78a650141ec9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5c36e3-97a6-42cc-9020-1f34a6cd0a24",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "85ce060d-faf5-4ab4-8b82-1e5e865ca5a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "71efc44d-fd13-40e9-b7f4-053bc2c0e52a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85ce060d-faf5-4ab4-8b82-1e5e865ca5a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3968d2e2-53c5-4fed-811b-c899250fd9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85ce060d-faf5-4ab4-8b82-1e5e865ca5a5",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "93b9e3f5-aa58-4d56-ade6-843345306bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "b512c2de-f87c-474e-876d-f6d2ace39f18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93b9e3f5-aa58-4d56-ade6-843345306bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19791095-957f-438b-a2f4-8de19ce20cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93b9e3f5-aa58-4d56-ade6-843345306bfe",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "509bc8a8-7361-49f4-86ee-8af237a36a9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "39e29ee0-5a91-498e-ad31-bb3140d40d94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "509bc8a8-7361-49f4-86ee-8af237a36a9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7495f20-5a15-404d-8226-c9f53c19b5b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "509bc8a8-7361-49f4-86ee-8af237a36a9b",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "a6f2b1fe-7cb2-4ea8-9384-eca495bd9808",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "9fd8af84-2403-4870-9212-a5a88694fce4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6f2b1fe-7cb2-4ea8-9384-eca495bd9808",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0108878-6e73-486e-928e-d6175cbaf3d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6f2b1fe-7cb2-4ea8-9384-eca495bd9808",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "5f16fec5-36f0-4147-970e-955a0f58ff94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "fec0a9cd-c1ef-45cf-bd27-53b1a4e4f002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f16fec5-36f0-4147-970e-955a0f58ff94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ec7ef5-8477-4abb-87bc-e25259a29c16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f16fec5-36f0-4147-970e-955a0f58ff94",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "96d93536-dd4d-4f4f-9546-b1fe413c08fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "8fd67b14-ffe8-4582-b4cc-a6547a885e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96d93536-dd4d-4f4f-9546-b1fe413c08fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ef08dc0-26a6-4280-97c1-258cc6cefde5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96d93536-dd4d-4f4f-9546-b1fe413c08fd",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "b2628fbb-338e-457b-b5e3-f163769752a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "4f6b67da-2f9a-4ed5-820a-fe74c6328fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2628fbb-338e-457b-b5e3-f163769752a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9240a2a-a453-4a6e-beea-ecfe425f36cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2628fbb-338e-457b-b5e3-f163769752a6",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        },
        {
            "id": "91e39944-15fc-4857-8921-c7151043a947",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "compositeImage": {
                "id": "78838f0f-1980-4752-b278-92586e29c8e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91e39944-15fc-4857-8921-c7151043a947",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ff263fb-a640-4d67-8883-45625bdb94f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91e39944-15fc-4857-8921-c7151043a947",
                    "LayerId": "3484065a-bdfc-4428-a7ea-b8943e5b027a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "3484065a-bdfc-4428-a7ea-b8943e5b027a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4787f5d5-8ced-40e5-80ed-cfe7d3a152d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}