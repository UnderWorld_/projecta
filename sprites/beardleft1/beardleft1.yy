{
    "id": "46eddc96-5047-421d-8bd2-120170ec6d2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardleft1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 5,
    "bbox_right": 49,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2f2c96ed-f4cc-4b03-a6e0-e34f5fc874cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46eddc96-5047-421d-8bd2-120170ec6d2b",
            "compositeImage": {
                "id": "4a6eca8b-9e73-4833-b78c-92f9ae8b2e9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f2c96ed-f4cc-4b03-a6e0-e34f5fc874cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ac86305-5c3d-4565-8a86-b32b1ce81e44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f2c96ed-f4cc-4b03-a6e0-e34f5fc874cc",
                    "LayerId": "12584060-e598-4f8b-a8a6-d0c6f88f26a2"
                }
            ]
        },
        {
            "id": "71998673-27b9-470a-a076-88574e95ed08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46eddc96-5047-421d-8bd2-120170ec6d2b",
            "compositeImage": {
                "id": "ad0bf745-b9ca-45f7-9984-29f428dfc6c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71998673-27b9-470a-a076-88574e95ed08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aa4c463-70bb-4a00-81b3-0da3c51c6414",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71998673-27b9-470a-a076-88574e95ed08",
                    "LayerId": "12584060-e598-4f8b-a8a6-d0c6f88f26a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "12584060-e598-4f8b-a8a6-d0c6f88f26a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46eddc96-5047-421d-8bd2-120170ec6d2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}