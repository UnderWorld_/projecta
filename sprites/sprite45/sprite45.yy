{
    "id": "5652af03-1131-4fc6-aa63-92399593e59a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite45",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "da71f154-1994-4796-aecf-560d19749936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5652af03-1131-4fc6-aa63-92399593e59a",
            "compositeImage": {
                "id": "0c8fd38e-8cae-4085-b146-10c061c22509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da71f154-1994-4796-aecf-560d19749936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "117d723b-3baf-46ae-9a48-0e4859bf53ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da71f154-1994-4796-aecf-560d19749936",
                    "LayerId": "c822b068-ab5f-4443-8b93-076d97a10875"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c822b068-ab5f-4443-8b93-076d97a10875",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5652af03-1131-4fc6-aa63-92399593e59a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}