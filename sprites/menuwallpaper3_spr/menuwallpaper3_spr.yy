{
    "id": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menuwallpaper3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3dee9831-ffa9-47f7-9b2b-90a6f55f832a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "e3bcd063-c974-42d9-b3ba-a46afd297163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3dee9831-ffa9-47f7-9b2b-90a6f55f832a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1d5b4cd-2936-4435-a9e7-3254da4ed198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3dee9831-ffa9-47f7-9b2b-90a6f55f832a",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "27f65c1b-ed86-4c0b-b88a-0e96ca703c85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "2d42cb39-6c0f-48d2-8468-d3d8b11ebc2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27f65c1b-ed86-4c0b-b88a-0e96ca703c85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89bfa3c6-5d6d-4ac0-b70d-fc868ed814d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27f65c1b-ed86-4c0b-b88a-0e96ca703c85",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "a055d2e8-2f38-4e3f-a24d-86704050891a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "913a1159-2c0d-4bd3-a64c-46079f215cf1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a055d2e8-2f38-4e3f-a24d-86704050891a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f889b858-8d86-478e-9903-03c9f9f306a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a055d2e8-2f38-4e3f-a24d-86704050891a",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "9ab9ab98-9537-488d-893f-5acaf30a3f95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "aa88504b-dffc-43a4-bf0e-56d2a6fbd06b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ab9ab98-9537-488d-893f-5acaf30a3f95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f14a16f5-802b-4e1b-8860-0b8ada6c1c6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ab9ab98-9537-488d-893f-5acaf30a3f95",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "a84a6e91-fc77-4aac-aa46-126f6b9f77e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "936bdb47-2a95-41cd-983d-f8f91309f428",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a84a6e91-fc77-4aac-aa46-126f6b9f77e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f30afed4-2245-4bc9-bda7-da15bd2de68d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a84a6e91-fc77-4aac-aa46-126f6b9f77e5",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "23bb206b-97be-4b64-bc24-559b321d3922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "e41534c1-cc5b-4e48-8551-0036b1e93791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23bb206b-97be-4b64-bc24-559b321d3922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8de506-4304-462c-9153-72644858af2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23bb206b-97be-4b64-bc24-559b321d3922",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "d6415af8-45dc-4f62-9caa-e2a4d9346ef2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "3c93ab58-45df-4ada-8521-9461e0a366f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6415af8-45dc-4f62-9caa-e2a4d9346ef2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7067f7c2-a790-47ae-a68a-814f167476ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6415af8-45dc-4f62-9caa-e2a4d9346ef2",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "f528ef13-52f1-4d71-8ec0-dd9651944bbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "57491dd6-36fa-44d2-b36b-43e99672ab42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f528ef13-52f1-4d71-8ec0-dd9651944bbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8631bf-b698-40b7-94b2-d24964cd0c3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f528ef13-52f1-4d71-8ec0-dd9651944bbd",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "04ed3fe3-d7a9-4ed1-b961-55bf8d185e16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "5b9dfbc4-f9cb-4032-9c27-8d2cf8a15092",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04ed3fe3-d7a9-4ed1-b961-55bf8d185e16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507df00f-6013-4a12-8f0b-28d3e7b9123b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04ed3fe3-d7a9-4ed1-b961-55bf8d185e16",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        },
        {
            "id": "55c6b5fa-9876-49d9-9a70-329678cdfde2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "compositeImage": {
                "id": "7a4b08cd-9c46-48ef-8b53-3dff409c0f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55c6b5fa-9876-49d9-9a70-329678cdfde2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6010f4f1-679e-4b20-97ea-d6a8a25f92e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55c6b5fa-9876-49d9-9a70-329678cdfde2",
                    "LayerId": "faba01fc-6f88-4cef-baab-ac39c143494c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "faba01fc-6f88-4cef-baab-ac39c143494c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cee74ea5-acd3-42bd-8d47-554560e7ef7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}