{
    "id": "4092a79d-8fc9-4cfe-968f-c53c2856742d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_door_spr1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 85,
    "bbox_right": 237,
    "bbox_top": 82,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7241a5dc-0d63-4106-95a9-d53b49a9d8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4092a79d-8fc9-4cfe-968f-c53c2856742d",
            "compositeImage": {
                "id": "072d4836-05e5-44ee-9337-f809f6511ed9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7241a5dc-0d63-4106-95a9-d53b49a9d8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df09896b-74f5-4868-8cd3-4c1f764486a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7241a5dc-0d63-4106-95a9-d53b49a9d8ea",
                    "LayerId": "b51010cf-0a73-426d-862f-4d6c0975751f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "b51010cf-0a73-426d-862f-4d6c0975751f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4092a79d-8fc9-4cfe-968f-c53c2856742d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 39
}