{
    "id": "cf6adce9-8659-44d0-9d81-930cb4501626",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "snake_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ce97d3a-1d70-4bcc-a0f5-c5e6b6e2e299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf6adce9-8659-44d0-9d81-930cb4501626",
            "compositeImage": {
                "id": "10d6c503-d217-4019-a081-5d5a62c38db1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce97d3a-1d70-4bcc-a0f5-c5e6b6e2e299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd5e6a9e-925d-449b-a75a-30047c257007",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce97d3a-1d70-4bcc-a0f5-c5e6b6e2e299",
                    "LayerId": "e56d1958-b9b7-4262-84e8-84a9e67860f2"
                }
            ]
        },
        {
            "id": "402c92a8-ad6f-4822-8ce9-2e8a7192301d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf6adce9-8659-44d0-9d81-930cb4501626",
            "compositeImage": {
                "id": "fc9bb51c-589b-421f-9f33-eeebe697d8f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "402c92a8-ad6f-4822-8ce9-2e8a7192301d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de321fb-2d1c-487a-b8ea-766070065df5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "402c92a8-ad6f-4822-8ce9-2e8a7192301d",
                    "LayerId": "e56d1958-b9b7-4262-84e8-84a9e67860f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e56d1958-b9b7-4262-84e8-84a9e67860f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf6adce9-8659-44d0-9d81-930cb4501626",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}