{
    "id": "11016ee0-de7a-4332-8e25-51e34a203bb2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 649,
    "bbox_left": 0,
    "bbox_right": 549,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9343f4c-ae69-429e-82e9-6b2a482d2c76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11016ee0-de7a-4332-8e25-51e34a203bb2",
            "compositeImage": {
                "id": "e17c31b5-0b5b-42f0-a977-1bc4a34be063",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9343f4c-ae69-429e-82e9-6b2a482d2c76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5403e18e-df09-4732-be08-dd97abab4017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9343f4c-ae69-429e-82e9-6b2a482d2c76",
                    "LayerId": "3bbd7c0c-8af9-4edb-8f93-268b8bea8271"
                }
            ]
        },
        {
            "id": "a6286f72-741c-4197-adcb-60f3dd098cb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "11016ee0-de7a-4332-8e25-51e34a203bb2",
            "compositeImage": {
                "id": "1ea0eabe-63ec-4c29-9443-d933d4ecafe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6286f72-741c-4197-adcb-60f3dd098cb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0a0243-f690-4d7c-8f50-a167e1c5d811",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6286f72-741c-4197-adcb-60f3dd098cb9",
                    "LayerId": "3bbd7c0c-8af9-4edb-8f93-268b8bea8271"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 650,
    "layers": [
        {
            "id": "3bbd7c0c-8af9-4edb-8f93-268b8bea8271",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "11016ee0-de7a-4332-8e25-51e34a203bb2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 550,
    "xorig": 0,
    "yorig": 0
}