{
    "id": "78729441-e280-49fa-830c-8fff516e1bfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cd72cad-b73d-475b-b71c-fd6e2d128744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78729441-e280-49fa-830c-8fff516e1bfd",
            "compositeImage": {
                "id": "2ba46139-a18f-4b05-aa5d-c897d2431bba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cd72cad-b73d-475b-b71c-fd6e2d128744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28018642-2bc9-4138-88f3-e9b663fbfaf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cd72cad-b73d-475b-b71c-fd6e2d128744",
                    "LayerId": "ae8415fd-5b79-4e6e-b8a0-89e8e2e716f7"
                }
            ]
        },
        {
            "id": "aeefe270-f4cc-4bde-9cc9-2f45d036e67e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78729441-e280-49fa-830c-8fff516e1bfd",
            "compositeImage": {
                "id": "0951a979-1c7a-4020-8bbb-6f9a833e7b81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeefe270-f4cc-4bde-9cc9-2f45d036e67e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0aaad76-62ea-4649-b4f2-c43cea514bce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeefe270-f4cc-4bde-9cc9-2f45d036e67e",
                    "LayerId": "ae8415fd-5b79-4e6e-b8a0-89e8e2e716f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ae8415fd-5b79-4e6e-b8a0-89e8e2e716f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78729441-e280-49fa-830c-8fff516e1bfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}