{
    "id": "9b33c860-bfe8-4556-9b37-3cbfa43752ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de727c3f-fbb4-4316-9c05-9c49c9a05735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b33c860-bfe8-4556-9b37-3cbfa43752ad",
            "compositeImage": {
                "id": "e35cb9d2-713d-43e5-953b-2f6e31e03bf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de727c3f-fbb4-4316-9c05-9c49c9a05735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b63cbc70-73ee-4eb2-8e3b-d06538c24f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de727c3f-fbb4-4316-9c05-9c49c9a05735",
                    "LayerId": "c478e142-90ea-468e-b9d7-cfbc32657449"
                }
            ]
        },
        {
            "id": "5d1fc734-906a-4f41-98b2-7f115ae0b5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b33c860-bfe8-4556-9b37-3cbfa43752ad",
            "compositeImage": {
                "id": "5f43deee-3fb3-4302-902e-ab3989ee033a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d1fc734-906a-4f41-98b2-7f115ae0b5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6065cd44-e4ef-4c1c-aada-dca22b9f612b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d1fc734-906a-4f41-98b2-7f115ae0b5fa",
                    "LayerId": "c478e142-90ea-468e-b9d7-cfbc32657449"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "c478e142-90ea-468e-b9d7-cfbc32657449",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b33c860-bfe8-4556-9b37-3cbfa43752ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}