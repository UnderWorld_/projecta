{
    "id": "0664957a-2624-4e77-b6c5-3fe6f25066be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1e97e6b4-e2dc-4183-b72d-314f2944902a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0664957a-2624-4e77-b6c5-3fe6f25066be",
            "compositeImage": {
                "id": "13209503-aba0-4ff0-8ecf-4b47b2f962b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e97e6b4-e2dc-4183-b72d-314f2944902a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3afe272-69d9-4fb0-a7a9-a5ca35a35a1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e97e6b4-e2dc-4183-b72d-314f2944902a",
                    "LayerId": "70434a2b-f773-4c1f-a794-4d46e7e52800"
                }
            ]
        },
        {
            "id": "8f4c67b4-c612-4cfc-ad94-21a6f06cbe42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0664957a-2624-4e77-b6c5-3fe6f25066be",
            "compositeImage": {
                "id": "68a434b4-f81d-4b34-97e2-4f77e3b3ac01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4c67b4-c612-4cfc-ad94-21a6f06cbe42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f45e2c88-b1c3-4faa-80d7-cdb0c8c6c380",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4c67b4-c612-4cfc-ad94-21a6f06cbe42",
                    "LayerId": "70434a2b-f773-4c1f-a794-4d46e7e52800"
                }
            ]
        },
        {
            "id": "31034d30-7403-455b-a309-6bd4b6ee81be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0664957a-2624-4e77-b6c5-3fe6f25066be",
            "compositeImage": {
                "id": "8aae919e-a977-40a2-a970-b8749de8c8f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31034d30-7403-455b-a309-6bd4b6ee81be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbddb35e-d7a5-491c-8e59-f27bd87e1b53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31034d30-7403-455b-a309-6bd4b6ee81be",
                    "LayerId": "70434a2b-f773-4c1f-a794-4d46e7e52800"
                }
            ]
        },
        {
            "id": "ed73e242-f9a9-49d8-987f-fc46009bf4b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0664957a-2624-4e77-b6c5-3fe6f25066be",
            "compositeImage": {
                "id": "95f5df80-8c8e-42cc-b199-14b42598a54a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed73e242-f9a9-49d8-987f-fc46009bf4b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0d28496-6c67-434d-bf2e-5a25765b3316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed73e242-f9a9-49d8-987f-fc46009bf4b4",
                    "LayerId": "70434a2b-f773-4c1f-a794-4d46e7e52800"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "70434a2b-f773-4c1f-a794-4d46e7e52800",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0664957a-2624-4e77-b6c5-3fe6f25066be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}