{
    "id": "a0fa7bb0-26e7-4ac8-afd0-b11479aa3b3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardleft2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 15,
    "bbox_right": 29,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "82f8e3cf-c89b-492c-9f5c-4328a42f4c26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0fa7bb0-26e7-4ac8-afd0-b11479aa3b3d",
            "compositeImage": {
                "id": "ad2bf5cf-c331-477c-ba27-ebdcee71244a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f8e3cf-c89b-492c-9f5c-4328a42f4c26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd8fcad8-5bd8-42c3-9fa9-70ef19035c7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f8e3cf-c89b-492c-9f5c-4328a42f4c26",
                    "LayerId": "7b07e629-b7cb-4faf-a470-541f5f99eee1"
                }
            ]
        },
        {
            "id": "5b4874b3-8bd4-4de0-bd49-fd4e970da233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0fa7bb0-26e7-4ac8-afd0-b11479aa3b3d",
            "compositeImage": {
                "id": "41a85862-9cbb-4bc9-8147-163c0daba93c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b4874b3-8bd4-4de0-bd49-fd4e970da233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34e56370-cc5c-4084-be30-abffe0386563",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b4874b3-8bd4-4de0-bd49-fd4e970da233",
                    "LayerId": "7b07e629-b7cb-4faf-a470-541f5f99eee1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "7b07e629-b7cb-4faf-a470-541f5f99eee1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0fa7bb0-26e7-4ac8-afd0-b11479aa3b3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}