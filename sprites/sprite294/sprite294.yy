{
    "id": "8714627d-10c0-4611-984b-956979e141c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite294",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 8,
    "bbox_right": 25,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f2762cb-fdee-4a94-8604-6c254a90fe57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8714627d-10c0-4611-984b-956979e141c0",
            "compositeImage": {
                "id": "951b3c1a-032d-4b93-b610-e3160fc99661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f2762cb-fdee-4a94-8604-6c254a90fe57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce7330b1-d3eb-4192-8d36-b9b262c04c99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f2762cb-fdee-4a94-8604-6c254a90fe57",
                    "LayerId": "020fb35e-baa2-42a5-9752-0d40c3726f32"
                }
            ]
        },
        {
            "id": "26280800-debe-4495-a734-374aa22f7e97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8714627d-10c0-4611-984b-956979e141c0",
            "compositeImage": {
                "id": "465eb9a9-bd08-44fd-9f90-3b75d97da01c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26280800-debe-4495-a734-374aa22f7e97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cd1b5ad-0829-47a8-b8f0-fed4c65086c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26280800-debe-4495-a734-374aa22f7e97",
                    "LayerId": "020fb35e-baa2-42a5-9752-0d40c3726f32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "020fb35e-baa2-42a5-9752-0d40c3726f32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8714627d-10c0-4611-984b-956979e141c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}