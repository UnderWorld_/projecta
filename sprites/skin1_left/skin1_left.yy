{
    "id": "b6fe6bfc-c963-449f-82fb-bb1e17fe4910",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin1_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13116131-d2f7-45bc-97a8-22c767b7e34c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6fe6bfc-c963-449f-82fb-bb1e17fe4910",
            "compositeImage": {
                "id": "8517ba2b-262b-4702-ae7b-1817ecbacd8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13116131-d2f7-45bc-97a8-22c767b7e34c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494ed842-d18a-4c57-940e-229e68dcca69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13116131-d2f7-45bc-97a8-22c767b7e34c",
                    "LayerId": "1cb06c08-5ff9-4252-82c3-b1253f99508d"
                }
            ]
        },
        {
            "id": "2eec5992-f57e-4264-9939-94ab164f3be1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6fe6bfc-c963-449f-82fb-bb1e17fe4910",
            "compositeImage": {
                "id": "4ee1dc5e-2481-4bf2-b825-280576e7652f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2eec5992-f57e-4264-9939-94ab164f3be1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b5f4824-1e22-4e13-92f3-942f40c637b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2eec5992-f57e-4264-9939-94ab164f3be1",
                    "LayerId": "1cb06c08-5ff9-4252-82c3-b1253f99508d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "1cb06c08-5ff9-4252-82c3-b1253f99508d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6fe6bfc-c963-449f-82fb-bb1e17fe4910",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}