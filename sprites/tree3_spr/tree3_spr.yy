{
    "id": "00943bf8-20e3-4807-8d4f-b6420d4c4784",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 28,
    "bbox_right": 34,
    "bbox_top": 78,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a0ce717-5fcc-4321-8fc5-1a897a110fda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00943bf8-20e3-4807-8d4f-b6420d4c4784",
            "compositeImage": {
                "id": "e4ce525d-d32a-4f07-b10f-2495df0cf943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a0ce717-5fcc-4321-8fc5-1a897a110fda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4644713c-b2b6-4781-af90-ae58ada7438f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a0ce717-5fcc-4321-8fc5-1a897a110fda",
                    "LayerId": "6233cf93-4586-4ce4-8864-6fd8fb637f8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 861,
    "layers": [
        {
            "id": "6233cf93-4586-4ce4-8864-6fd8fb637f8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00943bf8-20e3-4807-8d4f-b6420d4c4784",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 368,
    "xorig": 248,
    "yorig": 944
}