{
    "id": "05ad2be3-6dfc-4c9d-bba6-2d9ecb016763",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c644196a-35cd-418a-8883-93f007b8525d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05ad2be3-6dfc-4c9d-bba6-2d9ecb016763",
            "compositeImage": {
                "id": "b69dbdeb-5f9b-422b-b8ee-a1478a56888c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c644196a-35cd-418a-8883-93f007b8525d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84860c20-29e9-4107-8927-e636079e7321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c644196a-35cd-418a-8883-93f007b8525d",
                    "LayerId": "01096f35-e53f-4c37-a90d-3c3a4032b32a"
                }
            ]
        },
        {
            "id": "6835ba6d-b90d-46d1-a898-fd3100f8e852",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05ad2be3-6dfc-4c9d-bba6-2d9ecb016763",
            "compositeImage": {
                "id": "117d6c60-6ad8-4043-bb25-3c8ea55cec5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6835ba6d-b90d-46d1-a898-fd3100f8e852",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20663353-ea72-4e0f-9c6b-89d7f4260093",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6835ba6d-b90d-46d1-a898-fd3100f8e852",
                    "LayerId": "01096f35-e53f-4c37-a90d-3c3a4032b32a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "01096f35-e53f-4c37-a90d-3c3a4032b32a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05ad2be3-6dfc-4c9d-bba6-2d9ecb016763",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}