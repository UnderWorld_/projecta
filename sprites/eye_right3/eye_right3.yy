{
    "id": "db800d14-15b3-4060-9f38-35cb4fd8e2f8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aabe2ac5-2efd-4ab8-84a6-5f9dc6f68e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db800d14-15b3-4060-9f38-35cb4fd8e2f8",
            "compositeImage": {
                "id": "bd4c17a2-2675-4895-b228-ea236ce0f8d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aabe2ac5-2efd-4ab8-84a6-5f9dc6f68e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ee5c77-2578-43de-83b7-f22c03cecbfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aabe2ac5-2efd-4ab8-84a6-5f9dc6f68e27",
                    "LayerId": "f2fc0df4-cdca-434b-810c-55b4c3bfb3af"
                }
            ]
        },
        {
            "id": "a742cf00-e69f-484e-b3ee-932ddd67e26c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "db800d14-15b3-4060-9f38-35cb4fd8e2f8",
            "compositeImage": {
                "id": "5127c792-f20a-466c-a429-a4cb4fe8d00b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a742cf00-e69f-484e-b3ee-932ddd67e26c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da54b91b-af55-43c9-b637-e21d1ff0510f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a742cf00-e69f-484e-b3ee-932ddd67e26c",
                    "LayerId": "f2fc0df4-cdca-434b-810c-55b4c3bfb3af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f2fc0df4-cdca-434b-810c-55b4c3bfb3af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "db800d14-15b3-4060-9f38-35cb4fd8e2f8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}