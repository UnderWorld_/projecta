{
    "id": "e920e1bf-5115-45b4-b4b6-8985642517ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lizard3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 239,
    "bbox_left": 32,
    "bbox_right": 191,
    "bbox_top": 160,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60b3f9cb-9228-42f7-b8d1-3a136dc21403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e920e1bf-5115-45b4-b4b6-8985642517ae",
            "compositeImage": {
                "id": "51d03fb6-3a4b-41ab-9c4a-e98ae1aaf2aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60b3f9cb-9228-42f7-b8d1-3a136dc21403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1882c45-c1ec-41e9-83b8-d2b50f713f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60b3f9cb-9228-42f7-b8d1-3a136dc21403",
                    "LayerId": "fb3e0620-7cf4-4089-be48-a20da8efc761"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fb3e0620-7cf4-4089-be48-a20da8efc761",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e920e1bf-5115-45b4-b4b6-8985642517ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 110,
    "yorig": 238
}