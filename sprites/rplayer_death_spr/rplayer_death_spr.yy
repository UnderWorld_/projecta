{
    "id": "61cf527e-ed11-4b28-8830-1d8442c1080e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_death_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 6,
    "bbox_right": 27,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adb5b823-4856-42b0-a309-2a3ff9ae8412",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "db877dbc-1c8f-4bd4-b695-e7b2593ca5f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adb5b823-4856-42b0-a309-2a3ff9ae8412",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c449c2e5-81f4-4aef-ae5a-acae14ef944b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adb5b823-4856-42b0-a309-2a3ff9ae8412",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "825a034c-94a8-437d-8125-a509f89d5568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "a92d5dee-8cd1-47d0-bcf5-d3ce88b78200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825a034c-94a8-437d-8125-a509f89d5568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bb0901d-4bd3-46b7-9994-344bb1d71e94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825a034c-94a8-437d-8125-a509f89d5568",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "58b92ea7-e012-49e2-86b1-a05afc9488c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "1a3e8d7f-21f1-47aa-aa2e-c6018d9d4900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58b92ea7-e012-49e2-86b1-a05afc9488c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc0cedad-99a3-4338-a1ae-a3ba8faec795",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58b92ea7-e012-49e2-86b1-a05afc9488c6",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "2fa8aaa9-f2e0-432b-b5c0-6ab0f4194c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "3bc3e4e5-49e2-475e-b573-202638e48df1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fa8aaa9-f2e0-432b-b5c0-6ab0f4194c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d821025e-a853-480a-9018-b2c616c803e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fa8aaa9-f2e0-432b-b5c0-6ab0f4194c4d",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "64604a44-1873-4d4c-b788-68c59999ae32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "18472a5f-200c-48aa-a5e6-6c6913a493a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64604a44-1873-4d4c-b788-68c59999ae32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a293b8d2-7bbe-4644-a099-6fe9837b66e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64604a44-1873-4d4c-b788-68c59999ae32",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "d8353fae-c2c4-401f-9995-8f2db7c5fa71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "2d4b8ef2-8f01-484e-89e6-a623bdf52aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8353fae-c2c4-401f-9995-8f2db7c5fa71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20b41bb3-80bf-41dd-a518-13899a36a6fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8353fae-c2c4-401f-9995-8f2db7c5fa71",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "2504da55-e339-4190-ada6-417e116cec30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "a386baa9-2dff-4f4b-85a3-59a58ea55e30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2504da55-e339-4190-ada6-417e116cec30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "731dc56e-cdcb-467b-9513-5264b7e989aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2504da55-e339-4190-ada6-417e116cec30",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "4c191a5f-7214-47aa-aecb-944fae2f9ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "bda5e389-2ddf-48c9-bc9e-fb25066540b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c191a5f-7214-47aa-aecb-944fae2f9ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07f053c7-b256-4c8b-bf91-75fa906f0e46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c191a5f-7214-47aa-aecb-944fae2f9ac1",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "825588d2-7e9e-4f05-af6b-923b3d5d116d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "5d606038-bd93-4a12-b958-d9a3b41dffa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "825588d2-7e9e-4f05-af6b-923b3d5d116d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "788da9fb-0363-438e-a60d-ac119de4bd74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "825588d2-7e9e-4f05-af6b-923b3d5d116d",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        },
        {
            "id": "716c3ca3-8e9e-49a6-91e8-a7a0f7ca850e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "compositeImage": {
                "id": "6ff84179-f2ce-499c-a6a1-421ac4b405ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "716c3ca3-8e9e-49a6-91e8-a7a0f7ca850e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd809950-1157-4e5e-87cb-bae600234f2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "716c3ca3-8e9e-49a6-91e8-a7a0f7ca850e",
                    "LayerId": "b821dfa0-3118-4978-a36a-dbf0629b5fd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b821dfa0-3118-4978-a36a-dbf0629b5fd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61cf527e-ed11-4b28-8830-1d8442c1080e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -12,
    "yorig": 2
}