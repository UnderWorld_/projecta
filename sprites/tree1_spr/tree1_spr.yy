{
    "id": "e0cbefa5-68a3-417c-87bc-6be46c874866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 482,
    "bbox_left": 93,
    "bbox_right": 163,
    "bbox_top": 349,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64991ff3-da7b-4436-93ee-baabdecd1509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0cbefa5-68a3-417c-87bc-6be46c874866",
            "compositeImage": {
                "id": "5db9b45a-25b9-4ba9-b54c-17c99674b6b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64991ff3-da7b-4436-93ee-baabdecd1509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdf2ad5c-749f-494f-a478-39ab1703e342",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64991ff3-da7b-4436-93ee-baabdecd1509",
                    "LayerId": "c7ea80b0-8204-48ee-be42-f62a6a3b673c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c7ea80b0-8204-48ee-be42-f62a6a3b673c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0cbefa5-68a3-417c-87bc-6be46c874866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 124,
    "yorig": 460
}