{
    "id": "f4592bdb-216b-4321-9926-76e816bce832",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair2_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 10,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb15daca-1df9-41b9-bea8-db1655ce1bca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4592bdb-216b-4321-9926-76e816bce832",
            "compositeImage": {
                "id": "503806d1-da68-4773-8a49-060395571389",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb15daca-1df9-41b9-bea8-db1655ce1bca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed260342-f2c3-4817-a68a-c7732eb1be48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb15daca-1df9-41b9-bea8-db1655ce1bca",
                    "LayerId": "da3752de-51e5-4bd4-a5cd-d4def5404930"
                }
            ]
        },
        {
            "id": "db3cd4ad-1ac9-4893-a019-b58cf602cc7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4592bdb-216b-4321-9926-76e816bce832",
            "compositeImage": {
                "id": "6b9f2e3d-7c84-46fa-8822-ea51e8f48d1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db3cd4ad-1ac9-4893-a019-b58cf602cc7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbc82edc-9841-4975-9db6-c7c3a10aa1b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db3cd4ad-1ac9-4893-a019-b58cf602cc7d",
                    "LayerId": "da3752de-51e5-4bd4-a5cd-d4def5404930"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "da3752de-51e5-4bd4-a5cd-d4def5404930",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4592bdb-216b-4321-9926-76e816bce832",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}