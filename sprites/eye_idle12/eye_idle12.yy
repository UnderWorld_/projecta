{
    "id": "de04a217-2eb3-4501-a4d4-748f6644f5c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "016494ba-acd4-47d2-8241-b9d17090089e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de04a217-2eb3-4501-a4d4-748f6644f5c1",
            "compositeImage": {
                "id": "21733e07-66b5-4265-8d0f-aa00990bb8f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "016494ba-acd4-47d2-8241-b9d17090089e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0586b22a-bf5e-43ef-bff7-bb87d7911512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "016494ba-acd4-47d2-8241-b9d17090089e",
                    "LayerId": "6abb4df7-c745-4d53-8fa9-a940f9396e55"
                }
            ]
        },
        {
            "id": "85228b18-d59a-42e6-9fad-522474bb8089",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de04a217-2eb3-4501-a4d4-748f6644f5c1",
            "compositeImage": {
                "id": "1d734559-fae1-43b6-b464-e6db9841cd05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85228b18-d59a-42e6-9fad-522474bb8089",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "387886a1-90be-4874-add1-8583901378d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85228b18-d59a-42e6-9fad-522474bb8089",
                    "LayerId": "6abb4df7-c745-4d53-8fa9-a940f9396e55"
                }
            ]
        },
        {
            "id": "c8bc6bb2-d1c7-4085-881d-1fc3050c3715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de04a217-2eb3-4501-a4d4-748f6644f5c1",
            "compositeImage": {
                "id": "9826e902-e8d9-44b2-aeae-ef203f8009af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8bc6bb2-d1c7-4085-881d-1fc3050c3715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26f6bace-ce51-40c8-b35a-077f45edf699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8bc6bb2-d1c7-4085-881d-1fc3050c3715",
                    "LayerId": "6abb4df7-c745-4d53-8fa9-a940f9396e55"
                }
            ]
        },
        {
            "id": "79020311-4b14-48de-b73e-9992d9af044d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "de04a217-2eb3-4501-a4d4-748f6644f5c1",
            "compositeImage": {
                "id": "19d43eb5-7ba1-4d6e-b90c-ee0763bd3940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79020311-4b14-48de-b73e-9992d9af044d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51830f81-e980-4ea2-ade4-ed6a6e04c887",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79020311-4b14-48de-b73e-9992d9af044d",
                    "LayerId": "6abb4df7-c745-4d53-8fa9-a940f9396e55"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "6abb4df7-c745-4d53-8fa9-a940f9396e55",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "de04a217-2eb3-4501-a4d4-748f6644f5c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}