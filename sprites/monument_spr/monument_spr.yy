{
    "id": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "monument_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 90,
    "bbox_left": 0,
    "bbox_right": 113,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31119844-deb9-4ca2-9b93-1ad56651d728",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "400a22bb-0a48-4a79-9f83-c54cc5de8ac4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31119844-deb9-4ca2-9b93-1ad56651d728",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c3db873-33a1-4f0c-ac24-b28dc4815798",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31119844-deb9-4ca2-9b93-1ad56651d728",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "fc0c8c3a-5b7e-4b7b-83c9-f98e8bb82b3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "3bd0f54b-2493-46fe-93ac-22d23425def9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc0c8c3a-5b7e-4b7b-83c9-f98e8bb82b3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6e4cf4-0218-4204-bb3c-d02e0daab91f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc0c8c3a-5b7e-4b7b-83c9-f98e8bb82b3c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "26df9e5a-f66c-429b-bd3a-7b9c66a85c48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "627ba219-ab91-4b32-9f2e-4204f5849df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26df9e5a-f66c-429b-bd3a-7b9c66a85c48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66fd9d10-bf35-4d3e-b584-d8c0fb1b613d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26df9e5a-f66c-429b-bd3a-7b9c66a85c48",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "148c51d9-ad61-438c-9bac-f2df7f90a828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "ba945f8b-1144-45ed-850a-0bed3bd1b6df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "148c51d9-ad61-438c-9bac-f2df7f90a828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5052162-03d0-4fde-a781-6b40a049f876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "148c51d9-ad61-438c-9bac-f2df7f90a828",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "1a8eba04-3484-4014-988c-334e8ea2ce45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "eeeaf49a-36ea-40c1-bd11-941d6bd6b174",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a8eba04-3484-4014-988c-334e8ea2ce45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24ad6ea5-c074-45e6-825e-453306838e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a8eba04-3484-4014-988c-334e8ea2ce45",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "49f55276-c9dc-4ac6-a6d7-350fbb4e225a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "c09432f6-df04-46a9-b6b4-b2a50f42b1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49f55276-c9dc-4ac6-a6d7-350fbb4e225a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af032bc3-a7b5-49e5-8a9f-6fddd1c545b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49f55276-c9dc-4ac6-a6d7-350fbb4e225a",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "be3cf258-90b2-4b2e-8f0f-20ec8cf5e213",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "4d3d009b-e792-4b73-9786-d987f8675694",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be3cf258-90b2-4b2e-8f0f-20ec8cf5e213",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "599a8f4c-6c87-41ba-8913-77d4b8a16abe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be3cf258-90b2-4b2e-8f0f-20ec8cf5e213",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7c872fdf-711f-4981-96f4-83c03dc3beae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "7d005283-1f58-46b4-9ab7-1d65ef86cb78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c872fdf-711f-4981-96f4-83c03dc3beae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d032a921-d4a2-4692-9cb8-821493b7e266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c872fdf-711f-4981-96f4-83c03dc3beae",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "45f078ea-2599-472a-a4bc-ec0d3ef71be5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "5c77e434-8545-4317-bc1d-5676e76df09f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45f078ea-2599-472a-a4bc-ec0d3ef71be5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e77c492f-96cf-437a-8e67-4f1eedf7b278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45f078ea-2599-472a-a4bc-ec0d3ef71be5",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "37123ce7-da2e-4644-a1d5-7fc37d14251f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "8d68dbb9-488b-402a-9630-b63937ebdca2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37123ce7-da2e-4644-a1d5-7fc37d14251f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f5fcee-5c82-43b1-979f-4ba9025901c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37123ce7-da2e-4644-a1d5-7fc37d14251f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "1611e73c-74f5-4b9c-9cbd-1f778f689028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "e6477823-f118-486f-bd65-63ded048d922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1611e73c-74f5-4b9c-9cbd-1f778f689028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05fb08d6-93c2-4bc2-9f1a-6115e8a31ec0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1611e73c-74f5-4b9c-9cbd-1f778f689028",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "fa07ed8e-7484-4b38-a11e-8e2a223afc07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "e8adadcf-3f00-43de-96ef-493d44803eea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa07ed8e-7484-4b38-a11e-8e2a223afc07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1787aa81-c343-4451-aa9b-94d3f978f311",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa07ed8e-7484-4b38-a11e-8e2a223afc07",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "b6dd88b1-e603-4d3f-8454-fea907db9d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "d3d72d75-8336-4940-8f6d-debf5d19ae4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6dd88b1-e603-4d3f-8454-fea907db9d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d9c55d8-80ad-4c99-a614-1114cd9851f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6dd88b1-e603-4d3f-8454-fea907db9d8d",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "279442c1-3b60-4bb9-a829-46e6a878fa1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "58060b5e-83a0-46ec-bcaf-e7aaa1cdffb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "279442c1-3b60-4bb9-a829-46e6a878fa1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09d8c451-9e09-4157-8710-8d86b420c0c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "279442c1-3b60-4bb9-a829-46e6a878fa1c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "eaa2ddbf-15a4-4349-9196-8a201f7dba4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "cf378450-6708-4c44-8411-5bfbbf03d875",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eaa2ddbf-15a4-4349-9196-8a201f7dba4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab16c49-ddd9-4fa4-a790-bef11ee318d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eaa2ddbf-15a4-4349-9196-8a201f7dba4e",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "8bf3b9f4-6379-45ff-9e71-2dadb9eea59f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "b2ea35fd-1692-4ee9-8236-dd41a36c7ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bf3b9f4-6379-45ff-9e71-2dadb9eea59f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8235ac-a34d-42bd-90e6-087dd3d6841e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bf3b9f4-6379-45ff-9e71-2dadb9eea59f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "c1196be3-1d5c-454f-a6de-cc24a6cac25f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "61495c38-2703-47a7-9bdb-9194d494936c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1196be3-1d5c-454f-a6de-cc24a6cac25f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c849a4a6-6056-4f8e-92a4-40bedc82de5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1196be3-1d5c-454f-a6de-cc24a6cac25f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "d6bc01f4-0879-486d-8e3f-ecb980f7bf48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "fde670a7-7954-42c0-9f75-5ceed311a197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6bc01f4-0879-486d-8e3f-ecb980f7bf48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f26dd3e-31cd-43ba-9ddb-9cb702abe579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6bc01f4-0879-486d-8e3f-ecb980f7bf48",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "556db81b-4f83-42bb-b5c9-9e06c9a32edd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f93cb081-c500-41f3-8c0c-3e67433b4add",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "556db81b-4f83-42bb-b5c9-9e06c9a32edd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1da59d50-4e1b-4e30-a51c-f1ae0d058115",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "556db81b-4f83-42bb-b5c9-9e06c9a32edd",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "d8654653-590f-483f-a071-8851c7fbfa59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "e6d155ae-b945-48fa-b341-757568dc0cfb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8654653-590f-483f-a071-8851c7fbfa59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54bbe5bf-9236-41bf-8342-c03ef79b0e7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8654653-590f-483f-a071-8851c7fbfa59",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "1a2904de-9ac1-44ef-af47-7ba3d721ecab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "2058640d-9d11-496c-a0cc-d2e7e698c7eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2904de-9ac1-44ef-af47-7ba3d721ecab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bbf6621-8088-4d44-b750-3c92b8986e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2904de-9ac1-44ef-af47-7ba3d721ecab",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "c966bed3-c2f8-420e-960a-06bb39bb88a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "10257436-1b86-4740-9f30-466ede58fce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c966bed3-c2f8-420e-960a-06bb39bb88a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abea50b6-7fa5-44ac-ad5b-96e249f28839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c966bed3-c2f8-420e-960a-06bb39bb88a3",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "a24230ca-5bd2-490e-8516-aa4bd24badc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "1afe2f58-6baf-4ffb-a3cc-6ddf996c67fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a24230ca-5bd2-490e-8516-aa4bd24badc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66e20a97-9fe2-430d-ac35-17521e2fdaab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a24230ca-5bd2-490e-8516-aa4bd24badc2",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "5c97dc2c-82ed-4520-8f49-645267794c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "76b44468-99e7-4169-ac2c-f3a18c522bf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c97dc2c-82ed-4520-8f49-645267794c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50599929-b411-45e3-8d77-6e4026509da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c97dc2c-82ed-4520-8f49-645267794c77",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "060f3d65-dabd-4d6b-ae86-141b5e5276cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f3832c41-1c50-45a2-a06c-cf24c6bc2a65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "060f3d65-dabd-4d6b-ae86-141b5e5276cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f91507c-0be1-4450-805f-7a06db6be864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "060f3d65-dabd-4d6b-ae86-141b5e5276cc",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "bdcf6c7a-c198-44b9-b5ce-e73413c74ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "be555810-838d-40f4-bbf3-19915aed49f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdcf6c7a-c198-44b9-b5ce-e73413c74ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0872e040-bb61-48e0-b96e-71240375aa27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdcf6c7a-c198-44b9-b5ce-e73413c74ed6",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "0d07359c-2fca-42fe-866b-befa8b626306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "61716651-efae-430e-b7e8-4d4d32fa14b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d07359c-2fca-42fe-866b-befa8b626306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "140e0a68-3f57-4bf8-b98d-83afa87a15c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d07359c-2fca-42fe-866b-befa8b626306",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "91dc8541-e464-4e4b-96b0-a217edbe4387",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "49c1c77e-ab73-40d7-896f-f56caeb8afd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91dc8541-e464-4e4b-96b0-a217edbe4387",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd277f9-9e82-4748-818b-e45a94f30d9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91dc8541-e464-4e4b-96b0-a217edbe4387",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "34198276-a5fa-4fc0-83f7-c05e3f1e6e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "29833bb1-4438-4ebe-935b-2aa96d2f742e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34198276-a5fa-4fc0-83f7-c05e3f1e6e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b9e450-c6a2-4215-aab9-fd7ef51eaf71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34198276-a5fa-4fc0-83f7-c05e3f1e6e91",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "04eaa7c6-4efd-4901-8556-08365188a6ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "db96a24b-1e15-4ec7-8ab4-7250b508745a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04eaa7c6-4efd-4901-8556-08365188a6ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f3fde6-4be7-476d-aa99-8fa196b91ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04eaa7c6-4efd-4901-8556-08365188a6ad",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "d2971691-f425-4d7f-ae0f-81ac6aada025",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "8f4c1730-fbb6-4444-b6cd-4c405d253199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2971691-f425-4d7f-ae0f-81ac6aada025",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb652d65-0532-454a-8e47-02a9f9ce6636",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2971691-f425-4d7f-ae0f-81ac6aada025",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "efb5e5e1-bd9d-49e8-9723-4b5f6c706f2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "e012fedb-f505-4086-9d07-37a6cbd20d7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb5e5e1-bd9d-49e8-9723-4b5f6c706f2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64e80ece-6a54-4ad0-84d4-901313305071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb5e5e1-bd9d-49e8-9723-4b5f6c706f2b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "d00c74b3-82aa-49d8-a643-623f1acb5c9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "edce735f-ccd7-4395-9302-6596ec6c4b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d00c74b3-82aa-49d8-a643-623f1acb5c9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3222f2-e854-4061-bca5-bfea30b0359d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d00c74b3-82aa-49d8-a643-623f1acb5c9e",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "808078cc-024b-4fe7-a927-834540dd86d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "e698bbd5-7404-44fc-b436-29594033b5a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "808078cc-024b-4fe7-a927-834540dd86d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55edafd0-8043-484b-b551-567121d9eebb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "808078cc-024b-4fe7-a927-834540dd86d6",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "c4716131-8668-498e-8e17-a6695c1c570e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "7a5f0fb1-1247-4e5c-8512-16dd7bd8eae5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4716131-8668-498e-8e17-a6695c1c570e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d03b623e-ec99-4f97-8c80-3026d777e5e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4716131-8668-498e-8e17-a6695c1c570e",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "d78e1a05-1f38-42f4-bd5c-fc1f1750f674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "93d01c31-9969-4fb2-ac97-c095cb19a1a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d78e1a05-1f38-42f4-bd5c-fc1f1750f674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6c0a904-81e8-4e1d-9c48-c4fbc095699b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d78e1a05-1f38-42f4-bd5c-fc1f1750f674",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "426db70e-f9c0-4c32-8817-7e68147769dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "e096afe9-e36c-403d-ab96-201572e52501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426db70e-f9c0-4c32-8817-7e68147769dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b0ad0ff-7592-43b8-a0bd-c486adc47406",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426db70e-f9c0-4c32-8817-7e68147769dd",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "c9d24147-e0c3-4542-8ad5-5ab79a5ddb85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "030fd6f2-cb0b-4a19-aeee-932b4b619e87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9d24147-e0c3-4542-8ad5-5ab79a5ddb85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c25ee733-8546-4234-83b2-ff9bb43078bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9d24147-e0c3-4542-8ad5-5ab79a5ddb85",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "733b9017-e19f-414d-a28f-b10427949854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "06707069-8228-4981-91d8-6d2e2a73fb8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733b9017-e19f-414d-a28f-b10427949854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe978991-1faa-40bd-bf0e-8409061d5b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733b9017-e19f-414d-a28f-b10427949854",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "18499ac1-1ada-40f4-8ef5-90b24be486c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "6b711814-cbab-4f7f-bdf8-67d929388bae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18499ac1-1ada-40f4-8ef5-90b24be486c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d948c8c-4272-480f-ab92-0560796af46d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18499ac1-1ada-40f4-8ef5-90b24be486c1",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "6fcef533-aae9-44ef-9f4e-929c55f04e9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f471712b-2c5b-41ce-8562-8e6c9a5b579a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fcef533-aae9-44ef-9f4e-929c55f04e9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "946378a9-e03f-4070-a9ab-7530bd6af2aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fcef533-aae9-44ef-9f4e-929c55f04e9c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "be074cd8-5f32-461c-a602-6e63c0db446b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "af01db5c-7a80-4c86-9137-12f0c37eee01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be074cd8-5f32-461c-a602-6e63c0db446b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "931a7cd3-8455-4e66-a4a2-196839efe792",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be074cd8-5f32-461c-a602-6e63c0db446b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "46c22adb-dafd-489f-9347-1c85c2ede537",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "7119e7e3-a4ac-4fc9-8b82-1accb84abb9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46c22adb-dafd-489f-9347-1c85c2ede537",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e76de679-7c6b-4025-aa93-103e5563f43a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46c22adb-dafd-489f-9347-1c85c2ede537",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "6a97a742-7dd5-4599-8313-e9f188b21dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "68929e65-dd70-4518-8738-d0172ea651bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a97a742-7dd5-4599-8313-e9f188b21dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5867e1a-21b1-4a50-870c-5fabe00aac9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a97a742-7dd5-4599-8313-e9f188b21dfc",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "a8376ae2-fbce-4e1f-9502-7c51ca473a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "068ddd28-0e5c-438a-9a8a-a174ac386db4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8376ae2-fbce-4e1f-9502-7c51ca473a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21219e8a-74e6-4b36-be3b-ccfc79b2a6fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8376ae2-fbce-4e1f-9502-7c51ca473a93",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "83fad0a1-76e1-4836-8815-a1ec84569f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "6bab5823-d4dd-40fb-8f9e-0086d215e6c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83fad0a1-76e1-4836-8815-a1ec84569f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fa9fc01-c2af-44c8-9d72-f747ad96b574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83fad0a1-76e1-4836-8815-a1ec84569f66",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "b39178a6-c2b9-4ec9-b7a1-f0c80cfcaa38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "a6790336-5a80-4782-b88e-3a645e182fd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b39178a6-c2b9-4ec9-b7a1-f0c80cfcaa38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb410c9-0222-41f1-a87c-bcf6dcb1db6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b39178a6-c2b9-4ec9-b7a1-f0c80cfcaa38",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "fa9da699-0c0f-4ffd-b374-3f19d94457d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f51a533d-3962-450e-b3e0-dda827aedf83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa9da699-0c0f-4ffd-b374-3f19d94457d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9002b323-c785-484a-a6ea-34194ae900c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa9da699-0c0f-4ffd-b374-3f19d94457d2",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "2f6afcb7-7d0c-4996-9c33-fed067ac071f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "0fc8b830-587e-4d95-97ce-3838c54b19e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f6afcb7-7d0c-4996-9c33-fed067ac071f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55791b71-cfb7-4b3f-ba35-d32e0d5fd6f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f6afcb7-7d0c-4996-9c33-fed067ac071f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "b7493146-ef88-4fa5-bf27-833b02b7939c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "0197155d-548c-45c3-b38a-39adde445758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7493146-ef88-4fa5-bf27-833b02b7939c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71090d08-0951-48d6-8493-dca34defce7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7493146-ef88-4fa5-bf27-833b02b7939c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "0d842b59-4e50-45ea-8033-75994b1da31a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "51a98181-241b-4006-9e2b-b913d3bb6078",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d842b59-4e50-45ea-8033-75994b1da31a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e212f351-5844-401f-9167-e5494fc13c42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d842b59-4e50-45ea-8033-75994b1da31a",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "96c7b2cf-294d-433b-af08-1af90b76d7b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "2f7feaaa-0c80-4fa3-b338-ca3f121edda4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96c7b2cf-294d-433b-af08-1af90b76d7b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccf0479a-27eb-4e7e-b8c9-98ecf0b50107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96c7b2cf-294d-433b-af08-1af90b76d7b6",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7e239933-fa09-4995-add6-a4d5a2eb7d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "a20c25bd-be31-45ea-bf60-03e1b9e8b6de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e239933-fa09-4995-add6-a4d5a2eb7d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f6c9ce-335e-422a-a139-289ab90b0f6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e239933-fa09-4995-add6-a4d5a2eb7d22",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "463b2d08-25c5-4cbf-8d7e-2731b809a383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "39ac1abf-0897-47fc-b713-c96d41a00af5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "463b2d08-25c5-4cbf-8d7e-2731b809a383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26a85c69-e88e-4309-8a98-e041223ace26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "463b2d08-25c5-4cbf-8d7e-2731b809a383",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "25a8b4bc-a723-41c4-9425-7e1c94b22178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "7d1e15e1-30f3-4cf7-96cf-1801e65fd7f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a8b4bc-a723-41c4-9425-7e1c94b22178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8a80f2-4427-477c-acba-30851af371ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a8b4bc-a723-41c4-9425-7e1c94b22178",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "c27a0270-767b-497b-bdd7-7445c98cee56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "669020df-8961-455d-8bc5-86c02f96cef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c27a0270-767b-497b-bdd7-7445c98cee56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b9e02bf-7864-453d-9ee4-6434aa1d43f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c27a0270-767b-497b-bdd7-7445c98cee56",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "07c43026-c7c4-432b-a8d4-6ff2328b15da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "07d56bfe-e080-489c-aac5-4b0866e073e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07c43026-c7c4-432b-a8d4-6ff2328b15da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8ee459-8bf6-4e89-86b5-d2ee2cc03ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07c43026-c7c4-432b-a8d4-6ff2328b15da",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "0f8e71cb-3865-48cf-b539-75d6642b833f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "5b22710b-0e35-4efa-8146-d539390ebbc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f8e71cb-3865-48cf-b539-75d6642b833f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77a63d7f-548f-4ba8-8d63-3fd7c2754489",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f8e71cb-3865-48cf-b539-75d6642b833f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "e069ad0b-ef2b-49eb-b26d-56764b837f62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "a3406f20-88c9-4cca-b4c4-1c688bd28727",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e069ad0b-ef2b-49eb-b26d-56764b837f62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2bc57da-490e-4e62-8c44-44f51102bec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e069ad0b-ef2b-49eb-b26d-56764b837f62",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "1e14991f-ae55-417b-8f7d-95952ae27d26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "499dea9f-a1bf-47d9-8e1c-50504f093b61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e14991f-ae55-417b-8f7d-95952ae27d26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e44befac-3b88-407b-bc46-a9a06e7f6dae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e14991f-ae55-417b-8f7d-95952ae27d26",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "30682f9e-8387-48a6-8b57-7019e84fa692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "15f80745-912a-415a-847c-67312caa6c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30682f9e-8387-48a6-8b57-7019e84fa692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42f559c8-64ae-4668-a2f8-450d4629441a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30682f9e-8387-48a6-8b57-7019e84fa692",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "795eafe2-22c4-49f2-ab93-add22d67bfe7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "b474c234-be63-4a77-bb3c-300100525de3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "795eafe2-22c4-49f2-ab93-add22d67bfe7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1431ac9c-4cc7-4f06-bc7f-92d7629ddcf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "795eafe2-22c4-49f2-ab93-add22d67bfe7",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "10b373d1-3db6-46ed-b36b-4bb7c53844ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "00a3f380-3a7d-4c0b-8060-20370afa4dc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10b373d1-3db6-46ed-b36b-4bb7c53844ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37400ca0-da43-4382-bbb3-c9e37dc12209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10b373d1-3db6-46ed-b36b-4bb7c53844ed",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "af7a52fe-8867-4e55-8c28-1f2dd859c0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "4dff7aba-701c-446e-b2a1-32afddaf67ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af7a52fe-8867-4e55-8c28-1f2dd859c0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "257013b2-438f-47b1-a6b0-b2e6fd408db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af7a52fe-8867-4e55-8c28-1f2dd859c0a9",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "4d48d8db-ee23-45e5-979c-6b32db3892b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "d68a741f-14ec-46cf-8491-93f8a85e9f86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d48d8db-ee23-45e5-979c-6b32db3892b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d9b304e-ecd0-4420-8f6e-3b694e8d9a25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d48d8db-ee23-45e5-979c-6b32db3892b2",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "5ea059b8-3240-44d8-ba83-4510d29f96be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "4808b9b9-d213-4096-a021-fd16dd279253",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ea059b8-3240-44d8-ba83-4510d29f96be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5275ba6c-b3aa-42fb-821d-077a2c5111da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ea059b8-3240-44d8-ba83-4510d29f96be",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "82208bdf-0df6-4ba7-931e-5e2e76434571",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "67e60672-80ae-44e1-8ade-0e63d7480690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82208bdf-0df6-4ba7-931e-5e2e76434571",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14ff0b51-5dfd-44a7-9086-333bb81a57c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82208bdf-0df6-4ba7-931e-5e2e76434571",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "3bd1f33a-3547-406d-9cfe-e540be8c103f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "1c5c2fcc-36dd-482e-8f4f-69c5f2bd119e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd1f33a-3547-406d-9cfe-e540be8c103f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dff7f99c-1f50-454d-8dbf-7ed7a8cdee1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd1f33a-3547-406d-9cfe-e540be8c103f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "5e29f5ad-7bba-4b95-8c93-6d7e38d0a995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "c0a29a07-59a1-4d05-91a7-dcd4b10b8573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e29f5ad-7bba-4b95-8c93-6d7e38d0a995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "504e743c-1955-4ea7-9970-f38d24ee99fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e29f5ad-7bba-4b95-8c93-6d7e38d0a995",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "d2612899-7689-46bf-b8a5-e76d55ad0019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "d642d3d5-63f2-4ff1-ba55-b77e159ede42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2612899-7689-46bf-b8a5-e76d55ad0019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ad5e308-b878-49c9-9716-01e54a44fd6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2612899-7689-46bf-b8a5-e76d55ad0019",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "57a7d8a7-3fd2-45a4-b129-93cad3a313a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "8fca8f80-f0f1-4df1-9755-ab41722c9a51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57a7d8a7-3fd2-45a4-b129-93cad3a313a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb53f650-d04d-429c-9e24-57bf6691bbcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57a7d8a7-3fd2-45a4-b129-93cad3a313a4",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "914f6b76-051c-43d2-9d8b-1bede0a4ebe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "32301762-5593-4c7f-96b4-c66daeee43c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "914f6b76-051c-43d2-9d8b-1bede0a4ebe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "071736cb-3d43-4ee5-be39-7f65a51d0bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "914f6b76-051c-43d2-9d8b-1bede0a4ebe3",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "4ef410d7-abb0-4b4b-a227-382fd84bd19b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "4f694746-f3e8-4312-9f56-ce82dc54ec33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ef410d7-abb0-4b4b-a227-382fd84bd19b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e488088-a2af-4df6-9fe2-0600adee250a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ef410d7-abb0-4b4b-a227-382fd84bd19b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "ef12b15e-6a6b-498c-bf69-bc0bb0a9bf81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "0e458b42-dcbd-40c9-8092-3396ac2761a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef12b15e-6a6b-498c-bf69-bc0bb0a9bf81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ffaaba-384c-4b0c-875a-7e576f155444",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef12b15e-6a6b-498c-bf69-bc0bb0a9bf81",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "0b7af1cb-e80e-4931-9676-aaf5f015a55d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "297c119d-120c-42be-842e-8a21971c4cb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b7af1cb-e80e-4931-9676-aaf5f015a55d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df77ab9a-95cc-405f-8d6a-daedfcd97722",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b7af1cb-e80e-4931-9676-aaf5f015a55d",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7f0dac38-4735-41ac-ba29-a6d5f9942ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "22207fd1-e910-4f31-b254-ecd1aeb17bd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f0dac38-4735-41ac-ba29-a6d5f9942ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7f48644-deec-4c86-93ed-db8375f2adff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f0dac38-4735-41ac-ba29-a6d5f9942ad3",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "edd37d8d-aefc-470e-bb97-3382f93cb6cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "a30dce66-a181-4f9d-95e3-8f1cc9b83df0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd37d8d-aefc-470e-bb97-3382f93cb6cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ecf4185-3d97-43f5-9274-d8fb6ed169d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd37d8d-aefc-470e-bb97-3382f93cb6cf",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "5215cfb0-49ff-466e-9660-a17e0e2c739e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "32195c9a-0e0d-4a26-ad6e-e889f5b9ffa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5215cfb0-49ff-466e-9660-a17e0e2c739e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6552c78c-e98b-4d07-8511-259870a4d945",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5215cfb0-49ff-466e-9660-a17e0e2c739e",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "a10f0a1f-7af2-4292-99b7-625034713021",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "dcb883af-22d2-4d92-9e2f-fe0e98b9588c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a10f0a1f-7af2-4292-99b7-625034713021",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d46f81d-833e-45d1-9bcc-710caf635be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a10f0a1f-7af2-4292-99b7-625034713021",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "92adf780-c4b2-4ff9-8698-0df56848529d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "40c15016-b78e-4c14-86cb-6e3afa0825aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92adf780-c4b2-4ff9-8698-0df56848529d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e35e91ee-7a1e-4837-b6fe-c4d5729fcdb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92adf780-c4b2-4ff9-8698-0df56848529d",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "bf2fb645-81fd-4473-82ee-347de80786f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "dfa8cabc-b9ef-46ef-9721-f48b4449ba5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf2fb645-81fd-4473-82ee-347de80786f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "130d8e0c-4b3c-473d-96ff-f23d200734be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf2fb645-81fd-4473-82ee-347de80786f3",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "87842dbb-c24f-4478-8e06-51d735a32992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "421578f9-923b-4cfe-b740-7f83f1ee10f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87842dbb-c24f-4478-8e06-51d735a32992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "274125a8-ea02-4229-946e-c336bdf3ada3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87842dbb-c24f-4478-8e06-51d735a32992",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "1f9bbf4c-d21d-4f76-9643-4cabeb2b6dae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "6c986e18-4f3b-4ea9-8e17-c29189f2cd79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f9bbf4c-d21d-4f76-9643-4cabeb2b6dae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac45e158-9299-41dd-81b1-2c752311b6b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f9bbf4c-d21d-4f76-9643-4cabeb2b6dae",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "388e3930-9b1b-46aa-9687-52580cfe51ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f24235a3-8f90-4554-9cca-f2f9e7d0d5b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388e3930-9b1b-46aa-9687-52580cfe51ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f5228bc-40c3-4c26-ad1a-6b8fc691ef63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388e3930-9b1b-46aa-9687-52580cfe51ce",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "e5ef5260-dba9-4522-bb08-a6f44e474897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "1a243cd1-966e-40bb-94ae-cf3897fff0dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5ef5260-dba9-4522-bb08-a6f44e474897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea5c7f72-2e95-4aff-9ad3-99a20f012897",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5ef5260-dba9-4522-bb08-a6f44e474897",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "18cd0e1a-8193-41d5-960e-ab03a1e5244c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "77b95b64-1ca6-40b9-9366-f93e4ad911f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18cd0e1a-8193-41d5-960e-ab03a1e5244c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a32ef030-0d53-4669-91ee-5bbc00579518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18cd0e1a-8193-41d5-960e-ab03a1e5244c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "393f36aa-811e-4af2-8847-b701dfb78a1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "53611c7c-92bb-477e-91d3-ba458998e28d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "393f36aa-811e-4af2-8847-b701dfb78a1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0212d55-89ef-4e69-b393-5967ce1df608",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "393f36aa-811e-4af2-8847-b701dfb78a1c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "b3eb97c0-486a-45cb-a049-86efc98b9ec7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "8fd7cffc-9015-4199-96c1-810d602209ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3eb97c0-486a-45cb-a049-86efc98b9ec7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28c0ba92-166a-4b51-95c2-e3d3d8148601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3eb97c0-486a-45cb-a049-86efc98b9ec7",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "8fca05fa-0b39-4455-b988-550215a593d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "8257aa62-40bd-4f00-839c-65909974a93c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fca05fa-0b39-4455-b988-550215a593d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3a6c2b2-6de5-4dea-99df-4c748bb7219e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fca05fa-0b39-4455-b988-550215a593d4",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "e12e842e-2038-4d8f-80e6-aaaa5155bb18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "45fd1132-4e62-4125-8177-d569220933f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e12e842e-2038-4d8f-80e6-aaaa5155bb18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "890e1ce3-46e5-4fcf-adb0-3625aeee1d8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e12e842e-2038-4d8f-80e6-aaaa5155bb18",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "769ad85b-8fc7-4376-876f-e0b96497182a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f618ce94-54f6-4816-bec2-778ee0141384",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769ad85b-8fc7-4376-876f-e0b96497182a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "318e41af-0eaf-40c4-8f3a-2e609c07ed4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769ad85b-8fc7-4376-876f-e0b96497182a",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7cffca08-411b-4563-9cf4-590a57d64afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "70e7cd7e-2961-42ca-8a89-0d7431089e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cffca08-411b-4563-9cf4-590a57d64afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a9a48bc-10e0-4ac0-ac7b-ff620a7392f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cffca08-411b-4563-9cf4-590a57d64afc",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "99b6aace-7f30-494c-a243-bb7336d6b2f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "95238f1a-c85c-4e31-b443-e1bafa1cc2e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99b6aace-7f30-494c-a243-bb7336d6b2f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58f633bb-e57e-48d1-b928-049ad5b8d905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99b6aace-7f30-494c-a243-bb7336d6b2f6",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "10844265-efff-41bf-a42b-7ff7ac10b7d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "78398061-7fcd-4c8d-be56-d03eb6c39307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10844265-efff-41bf-a42b-7ff7ac10b7d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3be41369-1039-4243-b26f-95dd26eba741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10844265-efff-41bf-a42b-7ff7ac10b7d9",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7d3fefe8-779c-4096-bee7-472bb366bc21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "24ad13f5-a1b9-48a9-979f-03bbb8a2585f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d3fefe8-779c-4096-bee7-472bb366bc21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a41e752-91f7-4618-b96e-e2096cf2ef0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d3fefe8-779c-4096-bee7-472bb366bc21",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "6d6aa68b-7366-4d37-ba33-c3221b311dbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "51f3a4b8-e8f9-4625-bded-f6bb31527b64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6aa68b-7366-4d37-ba33-c3221b311dbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4787b77-1661-4b30-b375-34330c5aa626",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6aa68b-7366-4d37-ba33-c3221b311dbd",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "bf1d7a21-d08c-4d39-a3d0-1a9fe782e7f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "89d7afb5-4082-4aeb-b7c2-bed344cb5cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf1d7a21-d08c-4d39-a3d0-1a9fe782e7f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4fd367e-2eb6-47fd-afdd-dbc98a6b457c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf1d7a21-d08c-4d39-a3d0-1a9fe782e7f4",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "833dc122-e315-4f52-83ab-cb4f5229462b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "4590a110-fded-49c4-a5b4-8d6955f11cf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "833dc122-e315-4f52-83ab-cb4f5229462b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4fbcd35-5eb0-451d-8d64-a7569dedf66a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "833dc122-e315-4f52-83ab-cb4f5229462b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "3a173eb5-6908-4c14-addf-68744364dc05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "303c8acc-4bc9-482e-8c27-98968d3765b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a173eb5-6908-4c14-addf-68744364dc05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c183b9b-a9a8-4d3a-8b7d-28e0b03ba2ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a173eb5-6908-4c14-addf-68744364dc05",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "37489dc1-45c1-41fa-a0dc-d05bab42620f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "6bb5f9ae-c9f4-4f57-a45f-c533ebe6ff6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37489dc1-45c1-41fa-a0dc-d05bab42620f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2381fcb-872e-4b40-9687-6477087e73a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37489dc1-45c1-41fa-a0dc-d05bab42620f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "16575762-c977-4526-a428-feb6718ad7a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f5dc2a71-2361-4899-ab21-cfc4b4579aca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16575762-c977-4526-a428-feb6718ad7a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae4b77cd-0a9a-415f-9aed-e786827000c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16575762-c977-4526-a428-feb6718ad7a7",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "6af4c761-6791-493c-95f8-b53819d1b60b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "3622f91e-94bc-472c-bec5-bf5dfd1b1f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6af4c761-6791-493c-95f8-b53819d1b60b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5af3d4b-ac78-437d-959f-d499087914fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6af4c761-6791-493c-95f8-b53819d1b60b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "928c180f-fcb7-4cac-8dbc-58bf4c188dfd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "1705701c-0577-49a0-8888-2705fc7624ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928c180f-fcb7-4cac-8dbc-58bf4c188dfd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "572411b6-3530-474f-94e4-64da68097227",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928c180f-fcb7-4cac-8dbc-58bf4c188dfd",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "fa1466b8-e764-492e-b822-262160066985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "83116127-4a48-41f7-8c8b-f5a3210bb0bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa1466b8-e764-492e-b822-262160066985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b19bd9-dd3d-4484-918b-05eaeb0d998d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa1466b8-e764-492e-b822-262160066985",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "97acebb1-b97a-446a-87ab-e6c9a7ff3024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "8f0b90f5-4b98-4289-9ee4-d66422a078e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97acebb1-b97a-446a-87ab-e6c9a7ff3024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94c0f22a-dd18-4f48-8d89-a3d1dbb0e8c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97acebb1-b97a-446a-87ab-e6c9a7ff3024",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "3fa56693-43ae-4ec9-b681-6e32eec4f51f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "475021a8-3466-4819-8329-c3cc2468b70a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fa56693-43ae-4ec9-b681-6e32eec4f51f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef2b424b-f4fe-4586-b365-038e1c61a5c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fa56693-43ae-4ec9-b681-6e32eec4f51f",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7d9a384a-6f32-46ba-ad28-a370206cfe3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "63f5d50d-94ec-497d-95e4-a0d77851b910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d9a384a-6f32-46ba-ad28-a370206cfe3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31c11e21-aecd-49c2-bdee-6540dbe6754a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d9a384a-6f32-46ba-ad28-a370206cfe3a",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "76122c3e-4bfd-4d8c-b817-fb73dcff32e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "17029571-34ed-4c63-a70e-e2712a5c549f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76122c3e-4bfd-4d8c-b817-fb73dcff32e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc94c52e-a247-4e9b-8739-e09a69415680",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76122c3e-4bfd-4d8c-b817-fb73dcff32e8",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "7610b5ab-7fc7-4dbe-824d-0ca69476b43b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "cf7701f9-27d2-4ebc-b55f-ad9ef72fe64f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7610b5ab-7fc7-4dbe-824d-0ca69476b43b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6ae73da-d6af-43f3-b5d0-35f78fcbde96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7610b5ab-7fc7-4dbe-824d-0ca69476b43b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "39bd1f6d-7fd4-42be-bd42-f6cbd9082bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "a0c2306c-37b9-4fb3-a9ed-b06b19650b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39bd1f6d-7fd4-42be-bd42-f6cbd9082bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f3a3c27-eda2-4bb9-a10d-874f0143ae5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39bd1f6d-7fd4-42be-bd42-f6cbd9082bcc",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "a994acc0-ffb4-4b6e-ac17-72275cad0d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "fffb06aa-d8bd-4f25-86c8-d16cc3e4c988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a994acc0-ffb4-4b6e-ac17-72275cad0d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "734bf25c-8bac-4125-b1ee-f9e395518064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a994acc0-ffb4-4b6e-ac17-72275cad0d22",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "c6e91cba-9511-4d13-a94d-f7f10bf8bf0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "67d8c7dd-51b1-4b80-9b40-f09a580833c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6e91cba-9511-4d13-a94d-f7f10bf8bf0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c33f8534-b80d-4e2a-8ab1-4f872f72d515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6e91cba-9511-4d13-a94d-f7f10bf8bf0e",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "9fb36f8e-e89b-43df-ab5d-19df7a14086c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "afb7ad1c-fc3e-4a77-9961-e6c3cb0a4458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb36f8e-e89b-43df-ab5d-19df7a14086c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d95843-0ac1-4809-beb3-5519d9758cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb36f8e-e89b-43df-ab5d-19df7a14086c",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "82f71caf-e767-40a7-98ff-c4b360976eb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "c0e08170-0554-4870-9045-e59c10b2bbe6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82f71caf-e767-40a7-98ff-c4b360976eb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466aece2-d3c7-40db-8e5a-a55fd22588c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82f71caf-e767-40a7-98ff-c4b360976eb6",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "37c6052f-f57b-49f1-a7f3-01dc59ab6714",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "82b7993b-c44f-4dd9-84b4-2d2c527c2f54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37c6052f-f57b-49f1-a7f3-01dc59ab6714",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9cfa346-4569-4192-ad08-aaa143d9b5f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37c6052f-f57b-49f1-a7f3-01dc59ab6714",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "a7998218-74b5-4942-85c8-c4dd695c2415",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "83ad8b96-3dbd-4dfb-9e9f-56c77df66293",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7998218-74b5-4942-85c8-c4dd695c2415",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2bfce05-4243-40b6-b418-12394242bfd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7998218-74b5-4942-85c8-c4dd695c2415",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "8533064e-a92a-4493-a20c-fad24a45cd5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "f1c03501-24ee-4ffb-88d7-c534fc017bdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8533064e-a92a-4493-a20c-fad24a45cd5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac08981f-249f-49d0-947f-6bf743601905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8533064e-a92a-4493-a20c-fad24a45cd5b",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        },
        {
            "id": "8c5bef3b-8fc8-4409-9a6c-f1585a6dbbdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "compositeImage": {
                "id": "0e0c27f6-af94-4f6a-866c-0bbf97480cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c5bef3b-8fc8-4409-9a6c-f1585a6dbbdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dd3b0a2-2263-4ae9-826f-74fd2e7d7bbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c5bef3b-8fc8-4409-9a6c-f1585a6dbbdf",
                    "LayerId": "fed3744e-9a61-45b1-b052-ecf79230d1c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "fed3744e-9a61-45b1-b052-ecf79230d1c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}