{
    "id": "cf8470a1-24f5-4db9-ab56-e39c29da9e7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite56",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c136a661-3156-4d44-9bdc-7b183e1d3244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf8470a1-24f5-4db9-ab56-e39c29da9e7e",
            "compositeImage": {
                "id": "04637878-3e15-46ea-8151-056660f0d468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c136a661-3156-4d44-9bdc-7b183e1d3244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e79c41-0c64-4e87-a3b3-06ee2d8d0908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c136a661-3156-4d44-9bdc-7b183e1d3244",
                    "LayerId": "5a07cfe5-3d44-4cdf-8c64-4994c6b3f0f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5a07cfe5-3d44-4cdf-8c64-4994c6b3f0f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf8470a1-24f5-4db9-ab56-e39c29da9e7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}