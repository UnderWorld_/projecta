{
    "id": "77b36bc3-987a-45ee-b906-2f0f51e9cf11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dog_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 20,
    "bbox_right": 96,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd78b384-66cc-444a-830d-a992c3c97a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b36bc3-987a-45ee-b906-2f0f51e9cf11",
            "compositeImage": {
                "id": "7da236ce-e66c-46c5-b9f5-6babd9e76458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd78b384-66cc-444a-830d-a992c3c97a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5031514-0f86-4670-9694-5e2b7e7aac10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd78b384-66cc-444a-830d-a992c3c97a10",
                    "LayerId": "b1a548ec-e631-4f20-a58d-b6a69efc08c2"
                }
            ]
        },
        {
            "id": "ab11b1e9-ba6b-43c1-8d63-0819ca7050ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77b36bc3-987a-45ee-b906-2f0f51e9cf11",
            "compositeImage": {
                "id": "4f9c3e66-7855-4356-acdd-a2467846e6c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab11b1e9-ba6b-43c1-8d63-0819ca7050ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed80090-2aa2-44d6-98a8-bc2086b4a4f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab11b1e9-ba6b-43c1-8d63-0819ca7050ad",
                    "LayerId": "b1a548ec-e631-4f20-a58d-b6a69efc08c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "b1a548ec-e631-4f20-a58d-b6a69efc08c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77b36bc3-987a-45ee-b906-2f0f51e9cf11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 84,
    "yorig": 130
}