{
    "id": "206a34be-deb4-424b-b683-8e384b89f09b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_hut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5384d795-3917-4e58-8607-07cd2af080ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "206a34be-deb4-424b-b683-8e384b89f09b",
            "compositeImage": {
                "id": "05440936-7b68-4fea-8e40-d892570a6b29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5384d795-3917-4e58-8607-07cd2af080ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2fef602-18a6-4fef-8e52-e860f7942084",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5384d795-3917-4e58-8607-07cd2af080ae",
                    "LayerId": "bad352a8-3dbd-4fb0-8e83-776a13619dc5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bad352a8-3dbd-4fb0-8e83-776a13619dc5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "206a34be-deb4-424b-b683-8e384b89f09b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}