{
    "id": "800ad192-da47-4974-8983-a2c258d7a71d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin4_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2746c34-b5f1-432e-8053-0198b36010c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "800ad192-da47-4974-8983-a2c258d7a71d",
            "compositeImage": {
                "id": "83f7342b-e78c-43cc-b30d-9252f4e67b86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2746c34-b5f1-432e-8053-0198b36010c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2914d90-0658-4116-bfbe-ac54796a68e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2746c34-b5f1-432e-8053-0198b36010c0",
                    "LayerId": "c9267f35-92fc-41d4-9622-32020cbf55da"
                }
            ]
        },
        {
            "id": "efb51742-5582-401d-bfa5-f3165d084d96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "800ad192-da47-4974-8983-a2c258d7a71d",
            "compositeImage": {
                "id": "35808180-fde5-4140-b2d3-ebcd1bccdfee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efb51742-5582-401d-bfa5-f3165d084d96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d665d13-96cb-4871-b773-7b2e3a7adc60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efb51742-5582-401d-bfa5-f3165d084d96",
                    "LayerId": "c9267f35-92fc-41d4-9622-32020cbf55da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "c9267f35-92fc-41d4-9622-32020cbf55da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "800ad192-da47-4974-8983-a2c258d7a71d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}