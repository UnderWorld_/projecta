{
    "id": "c3fe8f9b-1c92-4545-83db-862143d5eaab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "747ba28f-b8c6-47c9-9183-49f89b4fc7fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3fe8f9b-1c92-4545-83db-862143d5eaab",
            "compositeImage": {
                "id": "255f3178-a48c-42a8-810f-6030d2ee327a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747ba28f-b8c6-47c9-9183-49f89b4fc7fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42c3ee83-fc5f-4940-ad31-8ae46f299133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747ba28f-b8c6-47c9-9183-49f89b4fc7fe",
                    "LayerId": "e7ba7fee-c85c-44b4-92ec-14e61a24e514"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "e7ba7fee-c85c-44b4-92ec-14e61a24e514",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3fe8f9b-1c92-4545-83db-862143d5eaab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}