{
    "id": "e8987c94-f649-4971-889c-575c3e02a43e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 5,
    "bbox_right": 40,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad0a7752-0b42-4815-906f-38602714898a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8987c94-f649-4971-889c-575c3e02a43e",
            "compositeImage": {
                "id": "1f03794d-ea24-4eb8-909d-36afe2cf8bf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad0a7752-0b42-4815-906f-38602714898a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4f70949-543b-4660-9d21-9304f23618ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad0a7752-0b42-4815-906f-38602714898a",
                    "LayerId": "e8729f8f-6565-4b55-9179-b1f810f2c791"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e8729f8f-6565-4b55-9179-b1f810f2c791",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8987c94-f649-4971-889c-575c3e02a43e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 26,
    "yorig": 31
}