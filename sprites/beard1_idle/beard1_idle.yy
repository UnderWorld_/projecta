{
    "id": "9367ea03-f082-4f52-9d69-7644a674b654",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beard1_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 5,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8224ad81-e227-4c9f-bf0c-b5e81f5e7132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9367ea03-f082-4f52-9d69-7644a674b654",
            "compositeImage": {
                "id": "902a9532-c3c7-40b4-8b1f-e904ba61d71b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8224ad81-e227-4c9f-bf0c-b5e81f5e7132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a0426d9-7524-493f-812d-e88618bab294",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8224ad81-e227-4c9f-bf0c-b5e81f5e7132",
                    "LayerId": "4906d34c-4615-4b4e-a172-ebf5dcf597c4"
                }
            ]
        },
        {
            "id": "f44f1a53-642f-42de-9283-3368af524203",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9367ea03-f082-4f52-9d69-7644a674b654",
            "compositeImage": {
                "id": "ecc4afaa-b59f-4db4-b5fb-5ebb37b679b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f44f1a53-642f-42de-9283-3368af524203",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded4133a-cf0a-4349-a5de-de0ce94ee839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f44f1a53-642f-42de-9283-3368af524203",
                    "LayerId": "4906d34c-4615-4b4e-a172-ebf5dcf597c4"
                }
            ]
        },
        {
            "id": "7c537092-6b27-42c3-a728-ffe37000d8ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9367ea03-f082-4f52-9d69-7644a674b654",
            "compositeImage": {
                "id": "1b70bb49-3d8e-4b44-8beb-57a9bdf2efc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c537092-6b27-42c3-a728-ffe37000d8ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bbdf3df-484c-4f2a-bd11-84c29632ce56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c537092-6b27-42c3-a728-ffe37000d8ea",
                    "LayerId": "4906d34c-4615-4b4e-a172-ebf5dcf597c4"
                }
            ]
        },
        {
            "id": "34378e72-a022-4c88-b05b-99707b602710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9367ea03-f082-4f52-9d69-7644a674b654",
            "compositeImage": {
                "id": "8c7d8d66-3b23-4ba0-b831-b44bbf7bfdc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34378e72-a022-4c88-b05b-99707b602710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1f3956-3153-4660-ada2-c9a77fcd8dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34378e72-a022-4c88-b05b-99707b602710",
                    "LayerId": "4906d34c-4615-4b4e-a172-ebf5dcf597c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4906d34c-4615-4b4e-a172-ebf5dcf597c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9367ea03-f082-4f52-9d69-7644a674b654",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}