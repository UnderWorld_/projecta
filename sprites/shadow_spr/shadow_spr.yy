{
    "id": "02d67f72-cdb0-4ffa-b3e2-a6619facffa6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shadow_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 3,
    "bbox_right": 46,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d40d0a04-f96a-42cd-bd8b-3a0e0c1aaabc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02d67f72-cdb0-4ffa-b3e2-a6619facffa6",
            "compositeImage": {
                "id": "ed72c860-4238-4424-b0fe-bd96f2f9b674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d40d0a04-f96a-42cd-bd8b-3a0e0c1aaabc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc6885dd-42c3-4b03-a668-18e633b2bc0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d40d0a04-f96a-42cd-bd8b-3a0e0c1aaabc",
                    "LayerId": "63d5b5d2-529f-4f74-b5f5-16434260e745"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "63d5b5d2-529f-4f74-b5f5-16434260e745",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02d67f72-cdb0-4ffa-b3e2-a6619facffa6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 11
}