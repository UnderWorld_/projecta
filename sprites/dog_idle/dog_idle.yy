{
    "id": "f555e27b-2e8a-4f53-bc5e-d49626b3d60a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dog_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 20,
    "bbox_right": 96,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b32e6ea9-c830-4213-b550-3e3d607fa088",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f555e27b-2e8a-4f53-bc5e-d49626b3d60a",
            "compositeImage": {
                "id": "2eaefe1d-cb00-4e3e-88e0-9292a370846c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b32e6ea9-c830-4213-b550-3e3d607fa088",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffc622ab-3d11-45a3-9e74-d353102f44e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b32e6ea9-c830-4213-b550-3e3d607fa088",
                    "LayerId": "796140fa-ebaa-4ed1-a8ab-b58496fa6d16"
                }
            ]
        },
        {
            "id": "64651de0-46ec-4318-8080-d58318ff5fe1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f555e27b-2e8a-4f53-bc5e-d49626b3d60a",
            "compositeImage": {
                "id": "9586d6e7-4172-485d-a881-66d873fafbfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64651de0-46ec-4318-8080-d58318ff5fe1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "161f9562-11f9-4f9e-ab7f-fa75d09ccef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64651de0-46ec-4318-8080-d58318ff5fe1",
                    "LayerId": "796140fa-ebaa-4ed1-a8ab-b58496fa6d16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "796140fa-ebaa-4ed1-a8ab-b58496fa6d16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f555e27b-2e8a-4f53-bc5e-d49626b3d60a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 84,
    "yorig": 136
}