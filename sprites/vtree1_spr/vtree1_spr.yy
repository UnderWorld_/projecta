{
    "id": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "vtree1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6d4977be-368d-48cd-ae49-0c65397503d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "7ad6f142-c178-4e11-912d-bf491c6d529f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d4977be-368d-48cd-ae49-0c65397503d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c883660d-2f77-48ae-b061-49de685cb8a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d4977be-368d-48cd-ae49-0c65397503d5",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "fe3feb8c-4b1e-44b2-80c0-d422b2744cef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "22f60702-adf0-4829-b8e1-2116132f1562",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe3feb8c-4b1e-44b2-80c0-d422b2744cef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee7030e-dd34-4eee-9076-a5b234b31269",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe3feb8c-4b1e-44b2-80c0-d422b2744cef",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "9c786581-bac6-4f47-8de2-70bdd5eca33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "43e97d38-dede-484a-927c-c4772a132731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c786581-bac6-4f47-8de2-70bdd5eca33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf180acf-5d97-4750-b603-7b4fd28f269c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c786581-bac6-4f47-8de2-70bdd5eca33c",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "cf2fd998-548d-4505-a458-4bc6bdf4b927",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "feb9437c-e178-4e34-abf0-bddbfe23ddc6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf2fd998-548d-4505-a458-4bc6bdf4b927",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3971aa0d-685e-4d16-8f62-a07222e614ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf2fd998-548d-4505-a458-4bc6bdf4b927",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "1fcb2cda-aeba-4ad0-b4ba-1ca665986ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "3519af48-fc84-4773-a20a-6851daab859c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fcb2cda-aeba-4ad0-b4ba-1ca665986ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d519eb4a-7d19-44e9-8da4-5b0ba368d0bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fcb2cda-aeba-4ad0-b4ba-1ca665986ead",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "41196974-0bcf-4244-8e6b-6aad2cf9794f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "36ba93f8-8233-42f4-879b-dd31f01bf2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41196974-0bcf-4244-8e6b-6aad2cf9794f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5537f87-00b0-494b-8d3d-05cb866c26d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41196974-0bcf-4244-8e6b-6aad2cf9794f",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "7e6f19be-6640-4396-ba2a-73fbe2c0457e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "98ac8c98-579c-40e6-961c-7397ed857f91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6f19be-6640-4396-ba2a-73fbe2c0457e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee8f14e-505d-401f-99f7-c019b1280af2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6f19be-6640-4396-ba2a-73fbe2c0457e",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "c8ba0f86-1202-4457-bca3-f2bdbb9eb95f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "4eedc2b7-0312-4c21-95fd-d5a28e25af8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8ba0f86-1202-4457-bca3-f2bdbb9eb95f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5a6b5f7-cc89-4e13-af94-6f2ddc80878d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8ba0f86-1202-4457-bca3-f2bdbb9eb95f",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "a133cd2b-2364-435e-a806-58e8f194378e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "8c30ea22-2928-457d-b6ba-b7eeae7ea87d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a133cd2b-2364-435e-a806-58e8f194378e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e91ea7a6-fdc5-4e56-bc88-4074d4d3ef17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a133cd2b-2364-435e-a806-58e8f194378e",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "b64e1912-7dcb-4b52-a636-c55a32c23b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "dd274024-3ce4-4f72-a4ce-60444734ded0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b64e1912-7dcb-4b52-a636-c55a32c23b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05006841-c26b-40df-a53b-a89b4599ba3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b64e1912-7dcb-4b52-a636-c55a32c23b4f",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "18655362-2f3d-4e41-a91b-f7ed11927915",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "5cb68e13-c8e9-484e-98e4-fe4d87e7fe6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18655362-2f3d-4e41-a91b-f7ed11927915",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc7e4bc-4482-4036-a85d-ca268939cfe5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18655362-2f3d-4e41-a91b-f7ed11927915",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "378e1582-3b81-4388-ba3b-a838104f3f09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "8aee76ed-6a6d-42a4-92a4-17cba6d38083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "378e1582-3b81-4388-ba3b-a838104f3f09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb9dbb24-6eed-4852-894d-fa7092c65b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "378e1582-3b81-4388-ba3b-a838104f3f09",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "511f5c25-dfcc-40a6-9a2a-a3b34a9b4713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "19878a4a-b0d8-4099-9998-8aded494cfcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511f5c25-dfcc-40a6-9a2a-a3b34a9b4713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "719f5805-f85e-43da-9409-10035105f514",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511f5c25-dfcc-40a6-9a2a-a3b34a9b4713",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "536d884f-f0ec-4068-8243-6fcae282b9ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "0df72d5f-f1a0-4d96-bfd2-97673ba70913",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536d884f-f0ec-4068-8243-6fcae282b9ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d0e26ba-5ea1-4b03-943f-088fca93a6f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536d884f-f0ec-4068-8243-6fcae282b9ff",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "99f37448-1c2f-42da-b39a-cbd224501732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "01383d7f-c06c-4e53-b02f-4a2fcbccc71b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f37448-1c2f-42da-b39a-cbd224501732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "945d68f7-54e4-4fee-a2d2-01a7b1e41345",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f37448-1c2f-42da-b39a-cbd224501732",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "c5d60871-77c7-4f25-88a5-c07eb7087624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "ce53b5cd-1734-4d32-a069-9c69a8ec9872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d60871-77c7-4f25-88a5-c07eb7087624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e34d95db-90fb-4d0c-bd1f-f2b2a3c446bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d60871-77c7-4f25-88a5-c07eb7087624",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "6f742cfe-110d-4f04-bc48-eec061919872",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "c44fb262-ccc2-43fb-8b54-0eba28b5665d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f742cfe-110d-4f04-bc48-eec061919872",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "542b3644-53ed-44a3-8fd4-d3966c4df5b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f742cfe-110d-4f04-bc48-eec061919872",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "db8fa254-b082-40f8-acf9-65765d87668b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "c3d6404f-7260-492e-9723-e3e09ff600fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db8fa254-b082-40f8-acf9-65765d87668b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c110a7e3-a723-4680-851e-bba2e8d4c5c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db8fa254-b082-40f8-acf9-65765d87668b",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        },
        {
            "id": "f468e91b-6c1b-43f3-8dd6-c141b234d392",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "compositeImage": {
                "id": "29165b14-7219-4b86-95f4-0717fd896bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f468e91b-6c1b-43f3-8dd6-c141b234d392",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03f0daac-58b4-4ed3-96aa-6517a40fd28e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f468e91b-6c1b-43f3-8dd6-c141b234d392",
                    "LayerId": "be057500-10af-4eed-bfbf-6db2350a2ec8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "be057500-10af-4eed-bfbf-6db2350a2ec8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a94e39a3-c8ca-49cf-8c9f-af25a7245f8b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 0,
    "yorig": 0
}