{
    "id": "d3052b27-a03c-4940-b19b-9fc2fbf05a5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin3_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50ae2079-d7af-4f56-9748-8a282a2038d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3052b27-a03c-4940-b19b-9fc2fbf05a5b",
            "compositeImage": {
                "id": "8e61b1b2-0032-4210-bce9-ad64249f2e05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50ae2079-d7af-4f56-9748-8a282a2038d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab401275-0a1b-45cb-b592-d94c3139f8bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50ae2079-d7af-4f56-9748-8a282a2038d0",
                    "LayerId": "dc92e3df-9116-4000-924d-c3c4e1d4a1cd"
                }
            ]
        },
        {
            "id": "6de1b53f-42d3-410e-9e86-21c530e976ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3052b27-a03c-4940-b19b-9fc2fbf05a5b",
            "compositeImage": {
                "id": "570ae341-d1d9-4444-83b8-0fd826b4563e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6de1b53f-42d3-410e-9e86-21c530e976ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c075fc72-0591-463f-b37e-a327b6c03c65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6de1b53f-42d3-410e-9e86-21c530e976ac",
                    "LayerId": "dc92e3df-9116-4000-924d-c3c4e1d4a1cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "dc92e3df-9116-4000-924d-c3c4e1d4a1cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3052b27-a03c-4940-b19b-9fc2fbf05a5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 90,
    "yorig": 221
}