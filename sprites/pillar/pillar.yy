{
    "id": "d8a19ed8-811b-4832-8d51-77116ceec389",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pillar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "38004521-0a9e-4922-b3b5-7dccf06c4b70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "00ef2c2f-bc7f-49de-8f03-c518b1d9ce68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38004521-0a9e-4922-b3b5-7dccf06c4b70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94ce30a5-732b-479a-8f39-ef5a08d57da3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38004521-0a9e-4922-b3b5-7dccf06c4b70",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "31611e61-2e6c-46a1-97ba-bd233482330d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "6828a451-c39c-4481-a4a0-4d416eac70e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31611e61-2e6c-46a1-97ba-bd233482330d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d865f2e2-666a-4ad3-9f63-04588d1d415f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31611e61-2e6c-46a1-97ba-bd233482330d",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "99f3d600-d9b2-4696-89c2-8f4521ba49ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "88c02158-89d8-4080-b6d4-a2eff3155fb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f3d600-d9b2-4696-89c2-8f4521ba49ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bf84235-7af4-44df-a700-2958667b5a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f3d600-d9b2-4696-89c2-8f4521ba49ff",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "f8633d55-d919-4621-9e5a-60502c7b0d60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "3f5636b4-af60-459e-9936-48e0dcd8fcd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8633d55-d919-4621-9e5a-60502c7b0d60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d2ea75-05d8-4594-bfde-5ec41f629ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8633d55-d919-4621-9e5a-60502c7b0d60",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7dd6d8a6-968b-486b-8542-c4afe8a23707",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a6d05788-3973-4e49-92ca-32398413a856",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dd6d8a6-968b-486b-8542-c4afe8a23707",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e15975ec-223e-460e-93f9-a77e4af14cf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dd6d8a6-968b-486b-8542-c4afe8a23707",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a2eeeeae-e600-4a42-ad7e-adee871f6e36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "278568b4-5e28-42f5-ae8b-c22024e057c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2eeeeae-e600-4a42-ad7e-adee871f6e36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a05f5864-d2d8-49cd-bcf9-9cb8bbe59828",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2eeeeae-e600-4a42-ad7e-adee871f6e36",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "196b2c94-6d70-4229-8dc0-ad0d4de1315c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "60997aaf-c362-49ef-a6ba-995c71d941cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "196b2c94-6d70-4229-8dc0-ad0d4de1315c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e43c12be-0d8a-438a-af4d-58a1229dd9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "196b2c94-6d70-4229-8dc0-ad0d4de1315c",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "220553bf-f613-4e8f-94f9-0fbadb442d0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "d7ad4f37-62fd-4c8b-a407-b3e6aeee905a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220553bf-f613-4e8f-94f9-0fbadb442d0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9d16baa-85ee-40d0-92d0-c92452a07bff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220553bf-f613-4e8f-94f9-0fbadb442d0a",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "2b5c9e77-6152-41fd-8874-1d023f050f64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "7862bba9-f2b2-4bd6-b65f-26190dc9fcab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b5c9e77-6152-41fd-8874-1d023f050f64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7114a88-a175-4f9a-aa54-2339a4d7c33f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b5c9e77-6152-41fd-8874-1d023f050f64",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "63a29d80-4552-486d-af97-6b4d286af196",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "55def762-ae28-4461-8306-81c656909274",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63a29d80-4552-486d-af97-6b4d286af196",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "833b4276-3f18-4ac2-9057-3e8ed05dfd40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63a29d80-4552-486d-af97-6b4d286af196",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "de7e65dc-d156-47bb-addc-2b9ddaf4df37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "e0410693-9b21-4918-a360-7b23f32b7c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de7e65dc-d156-47bb-addc-2b9ddaf4df37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333f4686-1a2e-4e08-90d2-9f70460a4cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de7e65dc-d156-47bb-addc-2b9ddaf4df37",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "419e392b-89cf-4666-a8f0-ccf892c08916",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "5a343155-ea0a-4638-b40d-3e5493447c17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "419e392b-89cf-4666-a8f0-ccf892c08916",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722f7b84-c3d1-4176-a06f-ac93b696594f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "419e392b-89cf-4666-a8f0-ccf892c08916",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "e7cc40cf-e55a-4230-89f4-a4df0435a3da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "8a0cd7d4-4062-4112-a89d-3fce0208abaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7cc40cf-e55a-4230-89f4-a4df0435a3da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85c79af0-bc54-4c64-96b9-ba3094d19721",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7cc40cf-e55a-4230-89f4-a4df0435a3da",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a7b19322-a89a-42a7-aa1a-c97c536a60bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "29e1a2a6-4a0b-4ebc-9ab5-bcf49c7d8bf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7b19322-a89a-42a7-aa1a-c97c536a60bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0390b917-a00b-4c69-85df-c25b7b4ee5d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7b19322-a89a-42a7-aa1a-c97c536a60bf",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7a449e81-938a-4b55-addc-25e22d1ecd8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a9864918-de3a-46f9-9610-ffe1f5be6d76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a449e81-938a-4b55-addc-25e22d1ecd8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "396196e0-e8b8-4c7a-bb13-6fea555b898f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a449e81-938a-4b55-addc-25e22d1ecd8d",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7b883eeb-1429-4531-b312-ff5d88d8e46d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a4b91255-ec44-4087-9b6f-65d7c14f006f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b883eeb-1429-4531-b312-ff5d88d8e46d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "678a28b6-c729-4fce-80a7-28838b52b5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b883eeb-1429-4531-b312-ff5d88d8e46d",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a317666b-cbd6-4ddd-b69c-e9b477d04e0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "b80df80e-86c5-479b-b1c3-9e140a846791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a317666b-cbd6-4ddd-b69c-e9b477d04e0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb1dd1c5-d732-4b22-8e1a-66adb28d0014",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a317666b-cbd6-4ddd-b69c-e9b477d04e0a",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "c73fcb6c-bafe-46c4-88cc-6ecd633080bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "d745402c-b7f0-4a56-ad25-ea0cd76b693d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73fcb6c-bafe-46c4-88cc-6ecd633080bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f86accf-6ee0-482f-8024-20efd2104a3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73fcb6c-bafe-46c4-88cc-6ecd633080bd",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "aad2be18-b661-457f-978b-867cf18fba60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "b160628c-f7b3-45af-9e79-e5cc6d3e6a4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad2be18-b661-457f-978b-867cf18fba60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab00a3ff-24e6-4fcb-8123-76418889c362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad2be18-b661-457f-978b-867cf18fba60",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "5f7c80e5-fea1-4ebc-99ab-3b11df9d6ed0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "05ecd74f-84bb-45bf-90a0-4e66794f3d48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f7c80e5-fea1-4ebc-99ab-3b11df9d6ed0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c999f1-2c91-4ee0-8fb5-43f55ce53a66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f7c80e5-fea1-4ebc-99ab-3b11df9d6ed0",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "2bbecca9-0fc1-4586-8280-d795dde370b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "7a397799-3dc7-4d2b-a0a7-db08ddcaa19b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bbecca9-0fc1-4586-8280-d795dde370b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e12f52d-4a74-4a41-b421-81642a18325e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bbecca9-0fc1-4586-8280-d795dde370b7",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a22516dc-27e5-4432-809c-c534880689fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "e31a3f24-61bc-4022-b698-c3f723e14c2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a22516dc-27e5-4432-809c-c534880689fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73ba5b3-2a09-40a0-bb16-9dfc440bdfdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a22516dc-27e5-4432-809c-c534880689fe",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "59a09484-72d6-42f7-a674-85ec1a30572c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "f384fd6a-199d-434b-b529-2544571e7c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59a09484-72d6-42f7-a674-85ec1a30572c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5062516c-3219-44e7-9f5c-6436010d5392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59a09484-72d6-42f7-a674-85ec1a30572c",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "76bd061b-43a3-44ec-b941-90491b7f6ca0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "e834eb07-d6ae-4740-97bd-0457fe230b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76bd061b-43a3-44ec-b941-90491b7f6ca0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efdb7399-5c4b-4ed8-bd6b-72ae67f6f846",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76bd061b-43a3-44ec-b941-90491b7f6ca0",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "03819d55-c99c-4cdf-9198-876e95eba6f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a79721ae-173e-4461-aafd-f50011381f1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03819d55-c99c-4cdf-9198-876e95eba6f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f666e99e-eabe-480f-8387-c0618d0043dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03819d55-c99c-4cdf-9198-876e95eba6f8",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "1a6d6a14-6cce-4e15-a277-d3ab5c663681",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "d5b74340-73d6-49e7-8b9a-88cd9cd97580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a6d6a14-6cce-4e15-a277-d3ab5c663681",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fa05d93-9b32-4bce-bc81-37d7451120e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a6d6a14-6cce-4e15-a277-d3ab5c663681",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "b8bebf28-7417-43da-a97d-6c898a53280b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "0af71ba8-3d24-4441-975e-d14732b8d259",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8bebf28-7417-43da-a97d-6c898a53280b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb1bcb90-bc4d-4891-beec-8d8939e6e548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8bebf28-7417-43da-a97d-6c898a53280b",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "c85ae147-8635-4b78-aeac-ed2af811ae8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "cb5fc806-23b2-4db7-8560-01cf1cf929f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c85ae147-8635-4b78-aeac-ed2af811ae8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f072c6b-1bf3-47bd-905e-eed9c5cbf155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c85ae147-8635-4b78-aeac-ed2af811ae8b",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a711c6db-cd36-4b77-be14-d505215d3394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "087a2d4c-b63b-4815-9c25-e4cb47e4f191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a711c6db-cd36-4b77-be14-d505215d3394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32a9d50f-1d4f-4d27-ab5e-ff04b602cc69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a711c6db-cd36-4b77-be14-d505215d3394",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "95fe6ff3-b2e1-4e4c-9a36-23c61fd73ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "4f9855b1-3c09-464b-8b2f-cb6206cc2d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95fe6ff3-b2e1-4e4c-9a36-23c61fd73ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f043b0c3-cd72-40db-9886-a12b6925dc5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95fe6ff3-b2e1-4e4c-9a36-23c61fd73ded",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "1f37a283-dca2-4284-83a8-f9afd6d0d669",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "6eb8a921-b3b8-4842-923f-de1dd90cde55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f37a283-dca2-4284-83a8-f9afd6d0d669",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4154bbb-695a-4c1a-b41f-c92b65bec902",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f37a283-dca2-4284-83a8-f9afd6d0d669",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "9987ac7d-36ba-4689-a6be-cca15c719d6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "dc6b5c00-1272-4be0-9a84-231edbac2799",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9987ac7d-36ba-4689-a6be-cca15c719d6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de8b289d-71f5-4822-b5d6-ef82b7fe2b88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9987ac7d-36ba-4689-a6be-cca15c719d6c",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "c085ef51-6bba-4ed1-80af-92ab287ef790",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "cc0379ad-0ecc-4c58-8edc-aada31377f99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c085ef51-6bba-4ed1-80af-92ab287ef790",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c93286d-8f4c-41aa-a6a9-5af49eb5a3a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c085ef51-6bba-4ed1-80af-92ab287ef790",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "d5181a84-0ac5-4dc3-98d3-8c2b5acb1493",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "e6f56f81-9da8-47c0-959e-47a038072a61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5181a84-0ac5-4dc3-98d3-8c2b5acb1493",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd1ea9d-90fd-4c5e-895e-db973ba67da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5181a84-0ac5-4dc3-98d3-8c2b5acb1493",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "101277ad-f89b-48d0-a854-add0c8b4c703",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "bed18fb2-cbfa-41d8-8cc6-2bff907b0169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101277ad-f89b-48d0-a854-add0c8b4c703",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a67066-c890-4676-8078-4ab94f7f7d77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101277ad-f89b-48d0-a854-add0c8b4c703",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "26211b0b-a053-4a9b-8ecb-c8f10d385606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "5c613f75-066e-485e-8dfe-cca665d3189c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26211b0b-a053-4a9b-8ecb-c8f10d385606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "045378d3-a7bf-45d0-bc41-b666df434646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26211b0b-a053-4a9b-8ecb-c8f10d385606",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "ce8823ca-20ac-46e6-97aa-69c86b1b7b1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "5be7cba3-85e9-4fb2-848f-6bc25adc18ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce8823ca-20ac-46e6-97aa-69c86b1b7b1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba2a4295-72f0-49e4-924d-a4a85bcf3622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce8823ca-20ac-46e6-97aa-69c86b1b7b1a",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "822f87e8-244b-48c6-a0f2-408c1711755f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "8a972edc-5ff6-44c7-b22e-b961d4f7ad7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822f87e8-244b-48c6-a0f2-408c1711755f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ff20cee-c08a-4ca9-a88b-e6f209066678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822f87e8-244b-48c6-a0f2-408c1711755f",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "602fb7b6-73f4-4330-ae8d-78a798534d99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "f9ca991b-9d60-4f72-aebe-84091449b7b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "602fb7b6-73f4-4330-ae8d-78a798534d99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88f650a-1613-45d4-a1a5-9decda6d1477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "602fb7b6-73f4-4330-ae8d-78a798534d99",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "75ca5b99-6aca-46e9-bab9-c511c61daaca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "aa8b755d-dd18-43f9-bf53-d72c309a4297",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75ca5b99-6aca-46e9-bab9-c511c61daaca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958f54b5-791b-4a38-90cb-54c2c8c64738",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75ca5b99-6aca-46e9-bab9-c511c61daaca",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7bddaf3e-0969-4d58-813f-332c6d415db5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "79a8e388-b90c-471b-8bdd-a57c7befa170",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bddaf3e-0969-4d58-813f-332c6d415db5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18d19526-f6f7-4fcb-81b5-19e6842da699",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bddaf3e-0969-4d58-813f-332c6d415db5",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "241fa75a-ff51-4abc-bd55-7041cd37f31e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "10cc97df-2ad3-4b2d-8ee4-c3107174d67a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241fa75a-ff51-4abc-bd55-7041cd37f31e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab078918-6caa-42e4-8c6e-d5f292c4da48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241fa75a-ff51-4abc-bd55-7041cd37f31e",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "6c606d5e-a7a2-4ec9-a832-10dfdf92cf1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "bf077162-5723-4f51-a0de-38eec1c10b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c606d5e-a7a2-4ec9-a832-10dfdf92cf1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c6323c-6e4f-4fe7-92d0-4c46bfffebd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c606d5e-a7a2-4ec9-a832-10dfdf92cf1b",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "fa415319-9ba1-415b-80b9-3cf80ea34f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "1e154f62-3da1-4289-9503-4a0454bba3d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa415319-9ba1-415b-80b9-3cf80ea34f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4f0b4b-9586-454e-8ceb-49441b888c26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa415319-9ba1-415b-80b9-3cf80ea34f68",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7df709dd-6db4-4fe9-b3c4-c768f804df5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "c2b8f0b2-cc5b-4d55-872e-550d0db83914",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df709dd-6db4-4fe9-b3c4-c768f804df5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e55e8c6-4d60-48cc-9035-58b720e9436e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df709dd-6db4-4fe9-b3c4-c768f804df5b",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "358a296a-30c0-46d9-8c25-d460a658149e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "0363857b-dbf1-4fad-8c40-1c55cca6397c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "358a296a-30c0-46d9-8c25-d460a658149e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17726699-2280-4258-b8f9-0797a03c811b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "358a296a-30c0-46d9-8c25-d460a658149e",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "104fd1c0-bb52-4206-9a3c-83f8284fef34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "8b028d69-55ac-40ba-997e-5d4a5f6f66fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "104fd1c0-bb52-4206-9a3c-83f8284fef34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15742fbd-db07-4588-86d8-7f78b591c08e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "104fd1c0-bb52-4206-9a3c-83f8284fef34",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "8033ffd1-7e69-40a7-b9c8-9383349dcc8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "9e0d5174-22d5-4d7f-a4d1-bb1deac6465d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8033ffd1-7e69-40a7-b9c8-9383349dcc8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d1d194a-142b-4bd9-b95e-d8f93453fc9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8033ffd1-7e69-40a7-b9c8-9383349dcc8c",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "95aed6b5-e130-4905-9675-138d1469d114",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "cd76453f-07b8-4414-bdc9-ab51d8e09b35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95aed6b5-e130-4905-9675-138d1469d114",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47344f16-4760-4baa-bcc7-a034b39bc1bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95aed6b5-e130-4905-9675-138d1469d114",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "baea5290-fe48-4d1e-9f53-a4c87ca2d38f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "b59b152d-55f2-4a50-b783-1ae0d1ab497e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "baea5290-fe48-4d1e-9f53-a4c87ca2d38f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90dd0d36-7c57-44d7-8afc-c4c6d7e92ff2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "baea5290-fe48-4d1e-9f53-a4c87ca2d38f",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "ca1b31fa-a5ea-4659-b011-96a8acb606eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "2b0bd99b-9b8c-4362-a8e4-b2f2f79164e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca1b31fa-a5ea-4659-b011-96a8acb606eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d526e9ce-4c89-4f8b-a481-cdc5468af857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca1b31fa-a5ea-4659-b011-96a8acb606eb",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "ad25d1fe-352e-40e1-99ae-3e530503e603",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "36923ead-3b8a-49f3-bb42-b7933609fcaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad25d1fe-352e-40e1-99ae-3e530503e603",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ebefea-cd49-4e1b-bf11-fb8fdcb2b878",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad25d1fe-352e-40e1-99ae-3e530503e603",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "2521a46c-ea69-4443-9837-6b2e1415f507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "37417a8c-a921-46e7-9026-c9ce6ba502e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2521a46c-ea69-4443-9837-6b2e1415f507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9953b86-7db7-41af-8972-7b43c21d5f47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2521a46c-ea69-4443-9837-6b2e1415f507",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "392a0979-914f-4cbc-ade0-b6ec2734d81d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "213d668a-8d51-4898-8fd3-08c8709c819b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "392a0979-914f-4cbc-ade0-b6ec2734d81d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6f1afdd-6b76-4307-8606-b692bb23035f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "392a0979-914f-4cbc-ade0-b6ec2734d81d",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "78d4662e-abd1-40d5-8061-9becef0e3583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a409401f-cd8d-44f1-8168-0d0db5f09a2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78d4662e-abd1-40d5-8061-9becef0e3583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "927d88e6-607c-49b0-9999-6b3bafda1db8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78d4662e-abd1-40d5-8061-9becef0e3583",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "1d24366c-9542-4d62-8849-b4f5bdec25eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "137a4648-d2d3-44b7-87a1-86c3c1d5c4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d24366c-9542-4d62-8849-b4f5bdec25eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14c43f46-6f3f-4519-b532-d32cf70dd4a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d24366c-9542-4d62-8849-b4f5bdec25eb",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "e6c40aba-0288-418f-9d51-03b4149ecfa7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "eb139f88-02c5-4ccf-97e4-1cca0a1710c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6c40aba-0288-418f-9d51-03b4149ecfa7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38ad1354-53e7-41d6-8e8f-ed5e1112a791",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6c40aba-0288-418f-9d51-03b4149ecfa7",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "c7c75cbe-6093-4295-b1cf-8b228d776b65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "5abb6756-0bc7-45be-8927-842d65042743",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7c75cbe-6093-4295-b1cf-8b228d776b65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31d1085b-2266-4e98-853d-a8325198e3ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7c75cbe-6093-4295-b1cf-8b228d776b65",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "1dcbfd2a-f3ba-4574-a718-030274d0f186",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "47866e25-7928-48cc-b432-bf6b237fbe18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dcbfd2a-f3ba-4574-a718-030274d0f186",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f9e1d4a-62e9-4461-bf90-c5b6b493ac10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dcbfd2a-f3ba-4574-a718-030274d0f186",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "b77b5bab-9f76-4251-98e5-77fd19eb0239",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "58a251c2-f9a6-4161-9412-257347c7005d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b77b5bab-9f76-4251-98e5-77fd19eb0239",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7bae351d-622f-484c-b13c-db146da4851e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b77b5bab-9f76-4251-98e5-77fd19eb0239",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "0edeeda0-7589-4f35-a02e-088e34652ad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "d2d9b2c4-0cbf-4863-9474-807f0e7328eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0edeeda0-7589-4f35-a02e-088e34652ad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119b9d28-356d-4c29-a579-4f33f2c682c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0edeeda0-7589-4f35-a02e-088e34652ad8",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "45ad5273-0a7e-40b8-b53f-a8b81b1aaf65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "d6f1161a-15a0-4b87-8fee-d70f1a8a39e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45ad5273-0a7e-40b8-b53f-a8b81b1aaf65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d06d8fc5-5164-41f0-b023-4a4ea8a84cbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45ad5273-0a7e-40b8-b53f-a8b81b1aaf65",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "e356ad5c-eec7-414d-86c3-d11d36ee9aab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "31a066ab-bf93-4a31-ad48-7c626a308633",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e356ad5c-eec7-414d-86c3-d11d36ee9aab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41b46ef6-2551-4322-a76b-bce116973cf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e356ad5c-eec7-414d-86c3-d11d36ee9aab",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "0d34bead-1943-4557-8182-c9180804e95e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "c9b3eddf-d920-4ce6-a322-3e88bc4dc0bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d34bead-1943-4557-8182-c9180804e95e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a137a8a7-afac-4adc-932f-49148cad613c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d34bead-1943-4557-8182-c9180804e95e",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "9f32bdf5-2d71-4fc4-a8dc-601335fe5b46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "68d59b24-65de-4e77-845f-bea4ae3caa4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f32bdf5-2d71-4fc4-a8dc-601335fe5b46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5da65e94-9d64-4f07-bb79-0c7fdf9c0497",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f32bdf5-2d71-4fc4-a8dc-601335fe5b46",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a6464b40-72de-4b29-96d7-ed5d7c207006",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "407f9f25-3c3e-4daa-91fd-506be7ea0085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6464b40-72de-4b29-96d7-ed5d7c207006",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1224b68c-1fdd-4aaa-a695-212a9a7633dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6464b40-72de-4b29-96d7-ed5d7c207006",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a4c7e0ca-8be8-4634-87e0-01f26217131a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "21db9f68-afce-429f-8a01-cbc1ef4b27f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c7e0ca-8be8-4634-87e0-01f26217131a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bad7a6e6-c933-4c63-8d6b-97b3df17d73d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c7e0ca-8be8-4634-87e0-01f26217131a",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "1c6a2718-e50b-4570-b707-991fc0c57405",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "13cab9ed-dea9-4133-b2b5-de1729940cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c6a2718-e50b-4570-b707-991fc0c57405",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4114aa2-85bd-4bf6-ac85-c125edcff17b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c6a2718-e50b-4570-b707-991fc0c57405",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7df4685e-73ae-42c8-ab9a-e931eb09a206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "dc7e924c-3b0f-45ab-a14c-4b43b15ea533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df4685e-73ae-42c8-ab9a-e931eb09a206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a0dbcf2-9f01-4e21-9d65-6c1b7135d3fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df4685e-73ae-42c8-ab9a-e931eb09a206",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "cdfd4a55-574a-40aa-821b-f2dbbeb27de0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "3404798a-8ea9-4f13-9fc8-adac24517b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdfd4a55-574a-40aa-821b-f2dbbeb27de0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f2357a-b154-4914-b0f0-035ac1c2468e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdfd4a55-574a-40aa-821b-f2dbbeb27de0",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "fe58b3e3-ff5c-4cb3-9636-a48b9aaab292",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "cd582d12-06bb-4eac-80bd-6363e7a475f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe58b3e3-ff5c-4cb3-9636-a48b9aaab292",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20c90b6e-96af-46e4-926a-d32cc8ba2bb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe58b3e3-ff5c-4cb3-9636-a48b9aaab292",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "35ac2a44-2097-4193-bbdc-884301ae75c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "c9ea49ec-0f53-4faa-a1b0-19fb3ceada32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ac2a44-2097-4193-bbdc-884301ae75c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0506aaaa-98c2-4809-83f5-4c683627d943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ac2a44-2097-4193-bbdc-884301ae75c9",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "24886446-bc53-4ae2-963a-a82f0752a7de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "fa05f2d6-5ded-4b28-bf93-cc3bb6599ae1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24886446-bc53-4ae2-963a-a82f0752a7de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ebdaf6c-4778-4ca8-be67-df288dd4b6e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24886446-bc53-4ae2-963a-a82f0752a7de",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "75cd51d2-64fd-4038-ba18-534692eec21d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "1e61c8de-4ad1-49c4-add9-473d188696f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75cd51d2-64fd-4038-ba18-534692eec21d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29bcbd6a-8e0f-4abb-88cc-59f6eb531723",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75cd51d2-64fd-4038-ba18-534692eec21d",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "d011b283-8108-4320-aef2-d6afd73a0cda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "f260784c-3198-4090-b63e-afe42d258b94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d011b283-8108-4320-aef2-d6afd73a0cda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "958a7fb0-2b00-4c29-90aa-2d99f2bdd35a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d011b283-8108-4320-aef2-d6afd73a0cda",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "8cd9fd12-566d-4e5d-b4ac-5cec2c3b167f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "137b9d1d-5efe-4a56-9e64-db87427fb0db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd9fd12-566d-4e5d-b4ac-5cec2c3b167f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24efe741-796d-4a39-8e79-b1b299bbf4a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd9fd12-566d-4e5d-b4ac-5cec2c3b167f",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "28cf7071-e363-4930-95ff-bc5cc579a4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a9f16f06-b3bd-44fb-8f63-8be5773483a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28cf7071-e363-4930-95ff-bc5cc579a4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98bb7c8d-a5e3-4e4e-815c-f3f9e9ecda1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28cf7071-e363-4930-95ff-bc5cc579a4c2",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "3fdb57ff-162c-433d-8c9a-2f57d2ed1041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "8f6da5a0-af56-4f59-8f01-8e54cc19dbcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fdb57ff-162c-433d-8c9a-2f57d2ed1041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dcee074-ccd5-41ce-9606-d1c9be00c573",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fdb57ff-162c-433d-8c9a-2f57d2ed1041",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a07f2b97-0da9-4314-b60a-9baa32bc482b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "c5635d8f-0c0d-467e-abd3-6811588e88ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a07f2b97-0da9-4314-b60a-9baa32bc482b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "743144b3-5e3d-43bc-90f5-2d7f3e1d7a2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a07f2b97-0da9-4314-b60a-9baa32bc482b",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "d6cc59c6-2a5b-4fd2-848e-2fa239459709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "6cf4567d-9226-4c67-a993-2bb674361a98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6cc59c6-2a5b-4fd2-848e-2fa239459709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf33f805-6135-4b55-be10-e9ba0745e034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6cc59c6-2a5b-4fd2-848e-2fa239459709",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "fc3c4cfd-2781-4fab-979e-3623dcdcc722",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "c38f0768-0a22-4a82-8505-99184ae0bb7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3c4cfd-2781-4fab-979e-3623dcdcc722",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04d5a3cf-d4c8-4e77-a3e0-7169a887cddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3c4cfd-2781-4fab-979e-3623dcdcc722",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "2b769114-6fc4-4e0f-8d0e-b4bef1a71e37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "d8605ec9-2fb9-4740-8023-b1834ee2c57e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b769114-6fc4-4e0f-8d0e-b4bef1a71e37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "489cbd5d-9c9d-42da-9152-ba2089014f49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b769114-6fc4-4e0f-8d0e-b4bef1a71e37",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "0d4674ac-b736-474f-8e14-62788b542442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "193217f3-ca0e-45d1-a607-30dd09f23051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d4674ac-b736-474f-8e14-62788b542442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac08b81e-b492-4828-bf34-a09ba1f0835a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d4674ac-b736-474f-8e14-62788b542442",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "add6e8d9-54ce-446d-a451-870b2428fbe8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "9430262c-a3f6-40c9-9349-0df42b2921b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "add6e8d9-54ce-446d-a451-870b2428fbe8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b75503cc-63e8-43a5-881c-0051d2d955e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "add6e8d9-54ce-446d-a451-870b2428fbe8",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "91980b86-51f0-4069-92c9-28aaf3216cb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "2357ac19-44cf-4cea-a74f-be7ce35edd30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91980b86-51f0-4069-92c9-28aaf3216cb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ebb0581-54de-4a4c-a7b3-a01232d584ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91980b86-51f0-4069-92c9-28aaf3216cb8",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a4b984a0-7301-49cf-8d03-ecd8ac7ab36a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "2eb43757-af91-44dd-a8d9-5b75c2934e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4b984a0-7301-49cf-8d03-ecd8ac7ab36a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "923673ae-ed5c-43ab-8825-53b38396cacb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4b984a0-7301-49cf-8d03-ecd8ac7ab36a",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "db7199f1-25e9-4e9c-b506-b788182559af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "6ff1649a-c818-463a-956c-c5bfadbe473e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db7199f1-25e9-4e9c-b506-b788182559af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebb20d43-cde9-4dd1-a1fb-3c40e9031303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db7199f1-25e9-4e9c-b506-b788182559af",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "7d2965b8-a7b6-4d27-9fbf-901395ad7624",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "b30cd579-5908-4498-9fab-3eb43ed0f558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d2965b8-a7b6-4d27-9fbf-901395ad7624",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb27364-5178-416e-892b-c2bc29b74ec4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d2965b8-a7b6-4d27-9fbf-901395ad7624",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "6903cace-cd7a-4d62-b8b6-cf464f50204b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "e44e7e51-d402-4d9a-ab62-0e974e4f3ba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6903cace-cd7a-4d62-b8b6-cf464f50204b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f4a0549-0acf-4724-9557-af0a625dfcaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6903cace-cd7a-4d62-b8b6-cf464f50204b",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "423df8ce-682d-411c-84ac-072c381a6ab3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "e8a6e0fd-6e06-4e6a-8c34-f9aec98bd39a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "423df8ce-682d-411c-84ac-072c381a6ab3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ff9a2d-f9f0-48c7-9fd2-a5e82e0832be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "423df8ce-682d-411c-84ac-072c381a6ab3",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "4a8ef385-4021-4615-9deb-0b10fd3498e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "8d70a9d2-c43e-4aea-92dd-e60617a44f9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a8ef385-4021-4615-9deb-0b10fd3498e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "384335a0-9489-4c81-b465-74edd730ad9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a8ef385-4021-4615-9deb-0b10fd3498e7",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "e8aa1079-f483-4263-9bf4-ba83e2cee323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "6ccbb1d5-4982-49a9-a5c0-c7f501b73b7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8aa1079-f483-4263-9bf4-ba83e2cee323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15c335e5-c169-4518-bc68-6c2271c33817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8aa1079-f483-4263-9bf4-ba83e2cee323",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "a97f9f83-19c6-49b1-912c-9a0d6ac30ef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "2de1279d-4092-4b93-99d2-5f0290d5aa67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a97f9f83-19c6-49b1-912c-9a0d6ac30ef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad4afa1-b9da-4f74-8ea6-805c8bbbd7b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a97f9f83-19c6-49b1-912c-9a0d6ac30ef5",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "b912fd11-ac5d-41ae-8b70-974c67675d7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "a5c40c3f-7bea-4bdb-b5ce-18c627676e86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b912fd11-ac5d-41ae-8b70-974c67675d7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "224945ae-c85a-4a1c-af58-650a77947c50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b912fd11-ac5d-41ae-8b70-974c67675d7e",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "c10d293b-9bf0-42fb-a316-8a56d679ea35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "25d04231-c84d-474a-8c3c-4735b4f09e06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c10d293b-9bf0-42fb-a316-8a56d679ea35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774a2abf-2806-4d4c-b28d-2dcb566d15df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c10d293b-9bf0-42fb-a316-8a56d679ea35",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        },
        {
            "id": "17aa2d09-02a9-4d45-bf36-58351f661150",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "compositeImage": {
                "id": "0fed692e-3997-443e-b48e-6a59cfaeab68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17aa2d09-02a9-4d45-bf36-58351f661150",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb02f931-1971-4102-82e2-4f93bbde3c25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17aa2d09-02a9-4d45-bf36-58351f661150",
                    "LayerId": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "e86fcc16-fd7d-418a-9572-2f7f0fe8e759",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}