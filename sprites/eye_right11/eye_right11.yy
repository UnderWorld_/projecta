{
    "id": "22bf4eea-ee6a-4776-8069-d8661390bd59",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9bc1bc8a-8640-4704-9eae-f2667929401d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22bf4eea-ee6a-4776-8069-d8661390bd59",
            "compositeImage": {
                "id": "97e5305d-5b76-493b-b737-9aca42640e81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bc1bc8a-8640-4704-9eae-f2667929401d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c0a8135-1b9d-47b6-95bf-4b6a8ecc40af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bc1bc8a-8640-4704-9eae-f2667929401d",
                    "LayerId": "a16dc8af-e7a6-4fc2-8030-fcce9188c765"
                }
            ]
        },
        {
            "id": "c9f9e30d-f631-42be-84a6-9937bb9194cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22bf4eea-ee6a-4776-8069-d8661390bd59",
            "compositeImage": {
                "id": "68604afb-0c60-4172-8437-3e4bf87c8aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9f9e30d-f631-42be-84a6-9937bb9194cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c64c899-db57-41ff-9a95-d37929f62536",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9f9e30d-f631-42be-84a6-9937bb9194cb",
                    "LayerId": "a16dc8af-e7a6-4fc2-8030-fcce9188c765"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "a16dc8af-e7a6-4fc2-8030-fcce9188c765",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22bf4eea-ee6a-4776-8069-d8661390bd59",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}