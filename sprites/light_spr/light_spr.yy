{
    "id": "d624ae66-4b82-4de0-b1a4-8a489909cb48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "light_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1535,
    "bbox_left": 0,
    "bbox_right": 1535,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e72b2e2-f239-45f1-85cd-5d543efcc737",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d624ae66-4b82-4de0-b1a4-8a489909cb48",
            "compositeImage": {
                "id": "9c51c950-532c-4b9c-aa09-42b89fea84df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e72b2e2-f239-45f1-85cd-5d543efcc737",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da4d8b9b-4dd8-49d9-805a-7a14b4e973c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e72b2e2-f239-45f1-85cd-5d543efcc737",
                    "LayerId": "465b2402-5b9d-4709-a4be-ec5d3812e11d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1536,
    "layers": [
        {
            "id": "465b2402-5b9d-4709-a4be-ec5d3812e11d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d624ae66-4b82-4de0-b1a4-8a489909cb48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1536,
    "xorig": 768,
    "yorig": 768
}