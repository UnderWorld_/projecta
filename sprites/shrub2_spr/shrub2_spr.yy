{
    "id": "58c4cc72-3b38-4323-8f49-08cd569809cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shrub2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c2d3fb0-8b63-457e-8b66-f3867ff08204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58c4cc72-3b38-4323-8f49-08cd569809cf",
            "compositeImage": {
                "id": "04ddf296-1b8f-4dba-b935-eedaeeb1c543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2d3fb0-8b63-457e-8b66-f3867ff08204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdbcdc09-6e18-43f7-a520-bb8b662de3ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2d3fb0-8b63-457e-8b66-f3867ff08204",
                    "LayerId": "5f569ce3-abcd-4072-9a21-c78a19ad0c27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "5f569ce3-abcd-4072-9a21-c78a19ad0c27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58c4cc72-3b38-4323-8f49-08cd569809cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}