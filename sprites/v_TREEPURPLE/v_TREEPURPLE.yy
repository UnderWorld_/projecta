{
    "id": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_TREEPURPLE",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 210,
    "bbox_left": 172,
    "bbox_right": 213,
    "bbox_top": 174,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c5356a8b-ff6b-4843-ada6-cf9f41a62802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "b6bac659-40f1-498a-bd6d-b8c61f92f1cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5356a8b-ff6b-4843-ada6-cf9f41a62802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce329329-73a9-4027-9aae-76c68a6f8bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5356a8b-ff6b-4843-ada6-cf9f41a62802",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "ec480a9d-d047-4a5c-a2a5-3adaab8d0730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "6310c5bd-5b20-4034-a73c-322595f8fa2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec480a9d-d047-4a5c-a2a5-3adaab8d0730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c605ff8-2216-4369-9dd5-c2db080ea899",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec480a9d-d047-4a5c-a2a5-3adaab8d0730",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "ea43648a-2156-41ba-aa55-091a6f75bd69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "eca143ae-7597-45ac-840c-9d3638288283",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea43648a-2156-41ba-aa55-091a6f75bd69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd77754a-1c50-4287-9327-6a89b5d52462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea43648a-2156-41ba-aa55-091a6f75bd69",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "b894c0e0-68f7-4788-a60a-e8ad4fe7762d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "f8c57233-e14f-44d9-b079-c7d41de9834b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b894c0e0-68f7-4788-a60a-e8ad4fe7762d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b024b0-31b2-46b8-a808-a848692ca33c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b894c0e0-68f7-4788-a60a-e8ad4fe7762d",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "ab0544d9-a9db-438f-b17d-901c81ada7e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "7d9595c3-f949-4b76-bf6f-42f0018a84f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab0544d9-a9db-438f-b17d-901c81ada7e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0eeea91-3481-4c1c-9e2b-57cd0a2e9aa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab0544d9-a9db-438f-b17d-901c81ada7e9",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "95f2a589-9348-47ad-a691-87bc7575a33b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "1176c858-efc9-4bc2-8cac-b9e8f6401626",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95f2a589-9348-47ad-a691-87bc7575a33b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6568cf74-047a-46a4-9642-87357506e583",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95f2a589-9348-47ad-a691-87bc7575a33b",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "883ca6ce-92d9-432d-9142-7a07dacfbeea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "e7428401-cb86-456d-8384-0e68b24ac00c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "883ca6ce-92d9-432d-9142-7a07dacfbeea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d31547d-af35-4a40-8c6d-25196470a93b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "883ca6ce-92d9-432d-9142-7a07dacfbeea",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "bf1cdf63-a6a4-45b8-ba1c-b463432fcfb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "352e8756-d71c-4da4-b19d-9f3c077b26fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf1cdf63-a6a4-45b8-ba1c-b463432fcfb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c82c19-001b-48a0-bbad-44ebd933205e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf1cdf63-a6a4-45b8-ba1c-b463432fcfb5",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "1df162c9-df18-452d-804c-7c66435ec871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "bfb52f66-469c-4af3-b7c5-943636c0d56a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1df162c9-df18-452d-804c-7c66435ec871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4909fe30-b6c1-4811-b6e2-b55b4fc578d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1df162c9-df18-452d-804c-7c66435ec871",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "80826451-3261-4e9e-b1fc-2a3b518d7154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "d10fb5f5-f569-4540-8430-ae119360f6d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80826451-3261-4e9e-b1fc-2a3b518d7154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a35de86-b6fa-4156-b398-8220b4c81909",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80826451-3261-4e9e-b1fc-2a3b518d7154",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "03de929c-afce-4732-ba62-759a237e9ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "b0a26e45-ad26-47d6-9e1b-c16dd9d3a890",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03de929c-afce-4732-ba62-759a237e9ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4784754-e582-4801-bc38-26bdfab5fa88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03de929c-afce-4732-ba62-759a237e9ee4",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "1fbf955b-0d81-4fd2-a11c-2450f154a045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "195d5b30-5073-4fdb-bba7-062914b7cf5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fbf955b-0d81-4fd2-a11c-2450f154a045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b876e2de-4210-4f98-bf16-b74dc61d5fef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fbf955b-0d81-4fd2-a11c-2450f154a045",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "604144a2-742e-4a61-ad3e-24939dfc590f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "2ac71fdc-e252-4078-ad6e-c034a951863e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "604144a2-742e-4a61-ad3e-24939dfc590f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad0caabb-dc4e-4e27-9f13-92b508997436",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "604144a2-742e-4a61-ad3e-24939dfc590f",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "de4ee416-dccf-4701-8510-d1c624cb017b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "54bd03f8-80a2-4e14-8744-12a7d08caff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4ee416-dccf-4701-8510-d1c624cb017b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1618910-0362-4069-ac6a-d940046b7ab0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4ee416-dccf-4701-8510-d1c624cb017b",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "bc909e22-9f96-424e-b679-b8245e249a9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "05cc7e85-008e-46a2-8602-890faff17258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc909e22-9f96-424e-b679-b8245e249a9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a0cb563-f6dc-4b26-979e-2b229916bbc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc909e22-9f96-424e-b679-b8245e249a9d",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "865f48cd-3c17-4e20-8a4f-1245378935ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "07a0f62f-254f-4bfd-8e36-2f798fd93255",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865f48cd-3c17-4e20-8a4f-1245378935ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31eaee5c-efd0-4e76-8ad9-a72971df544f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865f48cd-3c17-4e20-8a4f-1245378935ec",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a5c1f915-18ff-41ef-9675-38bfb74681ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "d54d13dd-8ff9-421a-ac42-ca7ddf346b73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5c1f915-18ff-41ef-9675-38bfb74681ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0570642c-0b59-4277-8200-b2cbd4171780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5c1f915-18ff-41ef-9675-38bfb74681ba",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "908d3d9f-f817-4958-8f67-e1449b46b68d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "96a67d36-d80f-41c5-960c-a7531413b24a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "908d3d9f-f817-4958-8f67-e1449b46b68d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb55dedd-81fc-4b4a-bf17-974722d5892b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908d3d9f-f817-4958-8f67-e1449b46b68d",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "47ec4601-4921-4b63-b136-59aac316134a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "a2441456-4506-4ddd-b1e5-4493a19dc127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47ec4601-4921-4b63-b136-59aac316134a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30d397d5-cada-4b38-a959-d6bf9678736d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47ec4601-4921-4b63-b136-59aac316134a",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "bcdcffc4-4ce5-4f8c-a05d-1b1d86329f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "b29d955d-d39a-4b9d-956d-c5b64facc6e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcdcffc4-4ce5-4f8c-a05d-1b1d86329f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1656a2e0-cc86-468a-a2f9-559230389e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcdcffc4-4ce5-4f8c-a05d-1b1d86329f76",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "5d771824-59d5-4dc8-b8b9-ff6c3e3a7775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "c2c88d43-2e10-475f-8b6b-b5f9ee6f464a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d771824-59d5-4dc8-b8b9-ff6c3e3a7775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d8222bf-851d-4583-9561-4f3da093171c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d771824-59d5-4dc8-b8b9-ff6c3e3a7775",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a8837ded-61f9-4308-a572-811d5f208e1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "4cf430d5-926c-4718-8c34-30804b278be6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8837ded-61f9-4308-a572-811d5f208e1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a54ec91-0fad-4bd6-ab33-d8cedc731c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8837ded-61f9-4308-a572-811d5f208e1a",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "08bc70bb-56ce-4b69-9a29-6ef92d441208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "1c5165a2-f3dc-44ef-993d-309a51adc7ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08bc70bb-56ce-4b69-9a29-6ef92d441208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199950fb-082d-4cab-a537-d7c47385e801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08bc70bb-56ce-4b69-9a29-6ef92d441208",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "be2358a2-7d66-4017-8e29-407b8bafbdb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "aa241c43-a98a-4833-9c06-c719c66e2641",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be2358a2-7d66-4017-8e29-407b8bafbdb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26277ae5-782d-4fb7-b74f-d7c59c04318e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be2358a2-7d66-4017-8e29-407b8bafbdb7",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "fc14e1f5-9689-4f47-984a-2a33dc595033",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "92641041-ed8e-4614-8d10-5630e2270264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc14e1f5-9689-4f47-984a-2a33dc595033",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a735c82c-4d73-48f0-92e5-f2105347915c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc14e1f5-9689-4f47-984a-2a33dc595033",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "e1223db5-f030-4ed8-bcf4-dda762d66bc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "b4a17e14-5c58-4247-b703-f79d26ae91b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1223db5-f030-4ed8-bcf4-dda762d66bc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f5a38b0-55dd-4c3a-8a62-6aaf6cc4090b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1223db5-f030-4ed8-bcf4-dda762d66bc5",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "0ed04405-740b-4ca4-9245-f7eb66985bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "d22c6924-0ee7-43a5-9556-836108c0d124",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ed04405-740b-4ca4-9245-f7eb66985bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "353ca653-b5d6-46f5-bbd1-6530c09d7f2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ed04405-740b-4ca4-9245-f7eb66985bed",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a26c51fb-46d5-465a-b5df-ebd415ce2001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "7a418467-b9ba-47d4-b0f6-3b963503531e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a26c51fb-46d5-465a-b5df-ebd415ce2001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f89c9179-3fe5-4808-8b24-c6f78202ce2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a26c51fb-46d5-465a-b5df-ebd415ce2001",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "2d19bdcc-b844-48b7-89ce-4c0ca280de98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "8da892bf-31cd-49b9-ba36-e0ed30efb54d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d19bdcc-b844-48b7-89ce-4c0ca280de98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5cb96cf-fae2-4242-b8c2-a0e3bc7c63e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d19bdcc-b844-48b7-89ce-4c0ca280de98",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "c0c8ae48-9334-4321-8b4c-b4db69ec0a4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "b1317987-ee08-481a-a31a-6ec1af0f9741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c8ae48-9334-4321-8b4c-b4db69ec0a4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b73a2b54-61ca-445a-a20e-9099fe8dc053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c8ae48-9334-4321-8b4c-b4db69ec0a4f",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "132a9b07-dfce-49ba-80e6-417fc9faeb25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "6813b53d-edbe-458b-b71e-4773f5177e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "132a9b07-dfce-49ba-80e6-417fc9faeb25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1bf9887-05b5-49dd-aafe-94b7e5dc9365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "132a9b07-dfce-49ba-80e6-417fc9faeb25",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "f98327ad-0b85-472f-9e46-8b4c16d4ae68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "583c17a8-095d-4511-871c-586a123a3f02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f98327ad-0b85-472f-9e46-8b4c16d4ae68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbdb8ff9-854b-4d24-81ab-0a41302b7843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f98327ad-0b85-472f-9e46-8b4c16d4ae68",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "85b2b815-b3db-4d29-8178-932f3a2af24c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "e9d7a2ae-abd8-440f-a8af-6c1d38a44122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85b2b815-b3db-4d29-8178-932f3a2af24c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ddf7b8-8f09-4372-9939-433e5efc736e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85b2b815-b3db-4d29-8178-932f3a2af24c",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "6e1e5944-19c8-40d6-81ee-92a0407387ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "f1c5ae74-66b9-43d7-b2a3-80820c5467f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e1e5944-19c8-40d6-81ee-92a0407387ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c60c2fd7-f8ce-4962-a0b3-2f44433d7674",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e1e5944-19c8-40d6-81ee-92a0407387ac",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "7d208a6d-098d-4549-8160-ecb2c7d6567c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "ba1ccd26-6063-44af-a125-0f757bd89210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d208a6d-098d-4549-8160-ecb2c7d6567c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54ed00f6-68fa-491f-bf7e-8c6330c6cd31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d208a6d-098d-4549-8160-ecb2c7d6567c",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "4e228080-3e3f-430e-a027-b3911aa247f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "01e51eab-6893-47ae-8c6a-4ae22b038926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e228080-3e3f-430e-a027-b3911aa247f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e74e004-8693-410d-965d-afe27b44e28b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e228080-3e3f-430e-a027-b3911aa247f0",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "ca54949f-16ff-4ceb-beb8-8e065282837e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "9e281273-02d7-41e4-9888-4e11ef985637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca54949f-16ff-4ceb-beb8-8e065282837e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "031e1a8b-a13d-4948-a0e1-320c803f4b61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca54949f-16ff-4ceb-beb8-8e065282837e",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "0c20611f-c4a7-4d78-88a9-b465ee3ce031",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "3c311d24-248f-498b-80a8-8c84d4dc153b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c20611f-c4a7-4d78-88a9-b465ee3ce031",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efbcb79f-9a5c-4d36-a0c8-727595aec2cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c20611f-c4a7-4d78-88a9-b465ee3ce031",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "79384f09-a506-4850-94d1-7d74e22ce183",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "44344a9c-8710-46b9-a87a-cfc6b9af092b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79384f09-a506-4850-94d1-7d74e22ce183",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c1168c1-7ddb-408c-b878-e27d18c155d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79384f09-a506-4850-94d1-7d74e22ce183",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "229f40dc-9ebe-43e1-a3f0-b12386ae8518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "7dd7b597-2c74-481c-8e53-f5966bd184b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "229f40dc-9ebe-43e1-a3f0-b12386ae8518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b546c08e-5c9e-4d46-987c-4c5dc4461bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "229f40dc-9ebe-43e1-a3f0-b12386ae8518",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "4131aa0a-a6e6-4eaf-8652-b81ec05eb21b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "cad99e95-cfa8-4656-a334-e1b066156cf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4131aa0a-a6e6-4eaf-8652-b81ec05eb21b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241b7b31-fc38-467f-beaf-23cea091170a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4131aa0a-a6e6-4eaf-8652-b81ec05eb21b",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "61c75c61-f42d-4fb0-a8fd-222869236690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "2c609597-7e88-4957-b5ec-dbc627428be8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61c75c61-f42d-4fb0-a8fd-222869236690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a95901c-dd7f-4782-83d8-8d9e16579693",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61c75c61-f42d-4fb0-a8fd-222869236690",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "0b8c12d1-8b0b-4f2d-a409-ed06e1ca3dc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "fe0bd242-d384-45b7-94ee-d3821b866f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8c12d1-8b0b-4f2d-a409-ed06e1ca3dc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0d5acc-518c-431d-89d8-ff171722523c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8c12d1-8b0b-4f2d-a409-ed06e1ca3dc8",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "67972eb8-7464-4f93-b59b-5cd9101db5b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "652abace-6f33-4d66-952f-0ae4df8e9b4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67972eb8-7464-4f93-b59b-5cd9101db5b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "019806be-4042-495a-847d-63b04de8d849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67972eb8-7464-4f93-b59b-5cd9101db5b0",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a31f7bf4-09ab-498f-8a12-a5a273c90bb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "ac73f2b7-deb5-401e-a114-3aabbdb1f360",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a31f7bf4-09ab-498f-8a12-a5a273c90bb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8b9e1b3-0540-4388-bcd4-2a1e1b313d91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a31f7bf4-09ab-498f-8a12-a5a273c90bb3",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "f1fcb301-2983-45cb-86b8-4ad60ee8afc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "327c8787-5c2c-4759-947e-347de05466b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1fcb301-2983-45cb-86b8-4ad60ee8afc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6fbc69-02bb-4caf-ab8c-661d2d379754",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1fcb301-2983-45cb-86b8-4ad60ee8afc5",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "be284b83-3ea1-4130-98a9-765a85342dce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "01f961f2-8445-4f72-9d34-fbe5e992a43d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be284b83-3ea1-4130-98a9-765a85342dce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17cf63c0-c9a4-49dc-8267-a5477b882acb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be284b83-3ea1-4130-98a9-765a85342dce",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "ce162e29-7530-4dc0-8f58-c69e0577ea7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "08eba37a-c8f0-45e3-a08a-d393a8d5bde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce162e29-7530-4dc0-8f58-c69e0577ea7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dc88695-a1a1-4139-b332-ee9b4dc92f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce162e29-7530-4dc0-8f58-c69e0577ea7b",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "3f170ee5-86cd-408e-93e4-cddb7767113c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "2036ab70-615a-4f10-a446-c4d5dc289aee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f170ee5-86cd-408e-93e4-cddb7767113c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "decbdc83-134f-46fd-acba-66030767a614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f170ee5-86cd-408e-93e4-cddb7767113c",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "d5f3c49b-d236-40a5-a91c-4306c8deeac2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "24f8d100-1fa7-4caa-a10a-a60fcf06e87a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5f3c49b-d236-40a5-a91c-4306c8deeac2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba9d33f9-f50f-459b-9a59-bfab48f29e13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5f3c49b-d236-40a5-a91c-4306c8deeac2",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "1d213653-31cf-48a2-b091-ed7fe194d7f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "9dd78027-7d4b-4d15-89e4-4ed90afae48f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d213653-31cf-48a2-b091-ed7fe194d7f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9f6ced2-8fb8-4eee-8829-e7973934ebab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d213653-31cf-48a2-b091-ed7fe194d7f1",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "4b661db7-c9a6-4eea-895d-bba2784dbfc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "70fde899-93cd-49ea-81a8-af3000d578fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b661db7-c9a6-4eea-895d-bba2784dbfc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2992840b-5d39-4e76-bf99-11d0aa98f75e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b661db7-c9a6-4eea-895d-bba2784dbfc2",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "3ff1ac96-c553-46dd-bcf7-6077cb6317f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "68aee4c1-ff5e-497e-a9b6-42c83dd19b39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ff1ac96-c553-46dd-bcf7-6077cb6317f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e771e21f-aa54-4718-9fa5-a4d56db263e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ff1ac96-c553-46dd-bcf7-6077cb6317f1",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "99e4fbb3-b564-4e59-a2fc-7c53125df76a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "ada3e08f-555d-4841-98ca-1477a6946b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99e4fbb3-b564-4e59-a2fc-7c53125df76a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34cc332-4370-4756-abeb-68463de76410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99e4fbb3-b564-4e59-a2fc-7c53125df76a",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "f94cfed2-9737-44fe-99f0-780fa9875200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "e4671d22-53e5-4ec7-9d4d-424e6665f3a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94cfed2-9737-44fe-99f0-780fa9875200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3bc05fb-c56c-46c6-9ce9-3b59f727350a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94cfed2-9737-44fe-99f0-780fa9875200",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "17c984b0-5678-447d-b7d5-faefa2cc031e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "633d2c4d-a21e-493c-adf4-de091c869d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17c984b0-5678-447d-b7d5-faefa2cc031e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b5d6f7-fd2c-4595-96ba-4b9b8bc75a0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17c984b0-5678-447d-b7d5-faefa2cc031e",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "b3c44838-ecbb-4fde-bd53-5f92beacb65c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "dbc41a65-276e-42d8-ad88-75656ffd306b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3c44838-ecbb-4fde-bd53-5f92beacb65c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e411fc4-5b43-4a69-bbf2-6e574940b48c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3c44838-ecbb-4fde-bd53-5f92beacb65c",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "bd5025ef-b73f-435a-8c00-ccb7959e4b0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "9b13ec32-85af-4060-bdc7-be4278402286",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd5025ef-b73f-435a-8c00-ccb7959e4b0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de0917cf-bdfa-4667-87e9-217698d35101",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd5025ef-b73f-435a-8c00-ccb7959e4b0e",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "97d99e7e-6ba3-4b70-a0e7-c80eaf01fca3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "b2c7c486-f00b-45f7-9b5b-672401569335",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97d99e7e-6ba3-4b70-a0e7-c80eaf01fca3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85f31e9e-b0c7-451c-8cb2-105a781bf63a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97d99e7e-6ba3-4b70-a0e7-c80eaf01fca3",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "dee50966-0f54-4efd-b2e0-61b921fd06c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "6eb22e24-0f85-4e8f-ad9a-3cf3f897e410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dee50966-0f54-4efd-b2e0-61b921fd06c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5494baa6-701b-4273-b684-1cd6a0d4951f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dee50966-0f54-4efd-b2e0-61b921fd06c7",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "bdebc20d-7fe4-4d0d-a19b-89c312bb003d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "a96c7615-deb9-48ab-86a4-b1f1175e492f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdebc20d-7fe4-4d0d-a19b-89c312bb003d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ea69f8-d531-4b5d-a2b2-1e20ab3241c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdebc20d-7fe4-4d0d-a19b-89c312bb003d",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "b1003551-5b04-42f9-b16e-c4c9f875ebfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "0bd98da8-710f-40a7-bfd0-e1c164bf0f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1003551-5b04-42f9-b16e-c4c9f875ebfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12658430-ae01-42e5-83ac-37c89d7f5b9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1003551-5b04-42f9-b16e-c4c9f875ebfa",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "0ad3ea7a-7a26-4ab8-a615-5eaa2c06fef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "97cb183f-0abc-4d07-8de8-9032007025a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad3ea7a-7a26-4ab8-a615-5eaa2c06fef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd5484f5-95a4-4036-8373-53c84224d60d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad3ea7a-7a26-4ab8-a615-5eaa2c06fef8",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "8ae45b5e-0834-4516-a532-5ba31c4d6441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "0bd7d6c3-1214-4566-8a5e-e3e27afad3e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ae45b5e-0834-4516-a532-5ba31c4d6441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c88313-ed4f-4416-983b-fafdc5c96552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ae45b5e-0834-4516-a532-5ba31c4d6441",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "e9e3159f-57fd-40ba-94e7-158a40ae8fe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "c657bed2-90e8-4ac1-8204-85bccf1be6be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9e3159f-57fd-40ba-94e7-158a40ae8fe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135eb8a7-299d-452d-bc22-cefa569718d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9e3159f-57fd-40ba-94e7-158a40ae8fe0",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "83b6229d-3312-4499-92e9-0a4c2d25eb1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "30710f5c-6249-492d-8692-0ddcd2fe5872",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83b6229d-3312-4499-92e9-0a4c2d25eb1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "deed92f2-6627-4149-bc9b-7c05a4197beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83b6229d-3312-4499-92e9-0a4c2d25eb1c",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "8fec3db4-e6bd-4b47-8c98-dc746c64ab36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "2951ffe9-bcc7-44f8-9c53-f7ce3f37c76b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fec3db4-e6bd-4b47-8c98-dc746c64ab36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a8d8d51-9cce-48b5-9cb1-b4893a16f9ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fec3db4-e6bd-4b47-8c98-dc746c64ab36",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "e2208b24-4490-49d4-bbbf-8e5b1e96b127",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "bc299bdf-de1a-412a-9c27-1a471fe6cf68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2208b24-4490-49d4-bbbf-8e5b1e96b127",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f52f1daa-2b34-4797-a4e1-a0df7acdc09e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2208b24-4490-49d4-bbbf-8e5b1e96b127",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "9409b771-70cd-44cc-b412-98dbc0126169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "4f154244-7be5-499b-a743-5ec337edb380",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9409b771-70cd-44cc-b412-98dbc0126169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6305bc4-bff5-453c-914f-718f3ef75a23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9409b771-70cd-44cc-b412-98dbc0126169",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "6b7a9317-bc88-4d6f-99ce-9daffdfbe065",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "65e3e67a-ee7b-4943-a220-891b800c7196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b7a9317-bc88-4d6f-99ce-9daffdfbe065",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65fd46c1-33d5-469f-b77b-68052b86ed8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b7a9317-bc88-4d6f-99ce-9daffdfbe065",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "efecbc05-910f-4d90-bb7b-838cfbb981ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "db26f684-8f2b-4867-aaef-539a8c18ca2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efecbc05-910f-4d90-bb7b-838cfbb981ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e76599a-1d9d-4463-b8b1-3f138a4de073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efecbc05-910f-4d90-bb7b-838cfbb981ea",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "18da098b-6209-454d-8c90-9cfdef40431e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "fc4cfbf0-212e-44a9-b720-185731f50ddf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18da098b-6209-454d-8c90-9cfdef40431e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e0895cd-767f-4900-89ec-a873111c3e5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18da098b-6209-454d-8c90-9cfdef40431e",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "2e8051d0-8401-4cc1-a50e-3ed6c22448f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "05f14560-1602-4fad-8621-f2b6e674fe66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8051d0-8401-4cc1-a50e-3ed6c22448f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47aff56b-3a2a-4de7-befb-a76d38afba9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8051d0-8401-4cc1-a50e-3ed6c22448f6",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "b18b371a-985b-4abd-b82d-b3c9228af0fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "c8423a24-9562-44d9-a9a1-a0ea7c94c4bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b18b371a-985b-4abd-b82d-b3c9228af0fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "087804db-9947-4205-a96b-f2145b83ecfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b18b371a-985b-4abd-b82d-b3c9228af0fd",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "8ab5ba0f-ae1f-4e98-98dd-85dfe21421fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "604d37a6-f060-4066-9976-84f842d18df5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ab5ba0f-ae1f-4e98-98dd-85dfe21421fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b96539a-d117-4a59-b85a-a5ba039def23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ab5ba0f-ae1f-4e98-98dd-85dfe21421fd",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "e53e48aa-4fb3-4ea5-bcc7-cd546f69dc5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "a6965209-707c-46a3-be12-5ca4d3e48b54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e53e48aa-4fb3-4ea5-bcc7-cd546f69dc5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "303e6c87-e662-489f-b697-bd0fdd705664",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e53e48aa-4fb3-4ea5-bcc7-cd546f69dc5d",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "997b5ed0-5e78-4064-b5db-bfdfb63680e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "299d6cdb-d5b2-4bf2-a595-1bd674ce2985",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "997b5ed0-5e78-4064-b5db-bfdfb63680e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4539a15d-e8e6-4dac-9555-071cd6d3fad5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "997b5ed0-5e78-4064-b5db-bfdfb63680e6",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "0584fc11-f867-42e9-9227-2ae94501bd0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "fa790945-6e0a-436c-8a56-97ab2083fc17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0584fc11-f867-42e9-9227-2ae94501bd0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85d1cb91-3abf-4939-a48a-ad5a1e08c456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0584fc11-f867-42e9-9227-2ae94501bd0d",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a3c96f66-9b0e-40b3-9668-b0b3b420790b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "706c9790-b4e1-4642-8016-91fdba85dff4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3c96f66-9b0e-40b3-9668-b0b3b420790b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42b66174-d011-4990-b968-1f8e78538934",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3c96f66-9b0e-40b3-9668-b0b3b420790b",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "61528e0b-a79c-4c3a-84da-d8071a52c35a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "0102e1e0-b0b0-460f-b2ab-a63c8f20d0e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61528e0b-a79c-4c3a-84da-d8071a52c35a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa983d8-c8d6-49b2-87c5-23cd0e13bb67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61528e0b-a79c-4c3a-84da-d8071a52c35a",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "4b9aae46-c369-497e-bd49-3312b61d4a8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "2645733b-05f9-4e87-9dd1-2614c66c41b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b9aae46-c369-497e-bd49-3312b61d4a8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6c55a14-c263-4c4f-b384-107b4be54ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b9aae46-c369-497e-bd49-3312b61d4a8a",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "d07f88d2-8878-4ebd-9893-46ddbf250bc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "53bba8dd-df78-48fc-8dc5-cb87d3ecfe93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07f88d2-8878-4ebd-9893-46ddbf250bc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86081cce-3ac9-42ab-a70f-d339d11a4969",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07f88d2-8878-4ebd-9893-46ddbf250bc1",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "3d5943c3-25e2-4b1c-9efe-60179e5ef059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "6ec4ed83-762e-4aad-8de9-b6ef0a8a96ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5943c3-25e2-4b1c-9efe-60179e5ef059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63295da3-5ce0-436a-b19b-8da3a23107d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5943c3-25e2-4b1c-9efe-60179e5ef059",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "1d771da2-f36e-458f-babc-941f1b799b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "a27edb01-1378-42b8-a4dd-54360968323a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d771da2-f36e-458f-babc-941f1b799b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7b59b38-2a72-4bd3-aab9-5792d92041c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d771da2-f36e-458f-babc-941f1b799b27",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "cc229276-ff3e-4e13-9c8e-6ee5f3aee796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "953b72e1-23e7-475b-8640-5405faf38991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc229276-ff3e-4e13-9c8e-6ee5f3aee796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c8604da-e271-477b-aead-6896cbb17c62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc229276-ff3e-4e13-9c8e-6ee5f3aee796",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "da6f4be0-fa04-4b8a-9a4a-78b9831bf27f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "bcaec80e-a332-4a14-9b38-0727c10544a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da6f4be0-fa04-4b8a-9a4a-78b9831bf27f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e6d1ad9-3b59-43f7-83f2-5bec6265e321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da6f4be0-fa04-4b8a-9a4a-78b9831bf27f",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "1bae6903-df9e-43db-8593-a7c376f51928",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "24123900-e0ac-414b-ba2c-243db03188bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bae6903-df9e-43db-8593-a7c376f51928",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "645f9d0b-de02-4634-8678-46b4ba15fac8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bae6903-df9e-43db-8593-a7c376f51928",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a8f39347-be40-4100-804b-95f58b377ae8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "dc99f3ad-1e98-470e-97ff-082b178f2543",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f39347-be40-4100-804b-95f58b377ae8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371d7ae9-5e26-4412-8e55-d4d80f83fd63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f39347-be40-4100-804b-95f58b377ae8",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "f7b27fb0-8c76-486c-b934-0b0fd1ba6881",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "19f74e7d-7e80-445a-a8cb-4f6e15924489",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b27fb0-8c76-486c-b934-0b0fd1ba6881",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af3a6814-9d4e-4c79-bc6a-7071178caa56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b27fb0-8c76-486c-b934-0b0fd1ba6881",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "b0b79fbb-bc06-4496-aef6-36fea98a5da9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "86ebcc85-b457-4623-a865-400d8c3f9d85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0b79fbb-bc06-4496-aef6-36fea98a5da9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e74bf2d2-2c09-45ab-a2a3-736265846e61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0b79fbb-bc06-4496-aef6-36fea98a5da9",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "3bd03d6e-0ff2-4e73-8ce3-dd05081d441e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "70223947-cadf-47d7-aee8-21fc52c8d12c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd03d6e-0ff2-4e73-8ce3-dd05081d441e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "287e5a7f-e4ae-43bc-a566-96171fa73cf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd03d6e-0ff2-4e73-8ce3-dd05081d441e",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "f819997f-800f-4692-838b-ec2d8fcba7ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "ef3c4a6d-05d7-4120-9e15-2438c9973692",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f819997f-800f-4692-838b-ec2d8fcba7ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83abd2b6-5abb-47ee-853c-8e07f85bfa2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f819997f-800f-4692-838b-ec2d8fcba7ca",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "2fdcfba5-9ae9-481d-8ec9-8f2a1b47a136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "0a3fc862-98d2-4dee-9508-67f739a70af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fdcfba5-9ae9-481d-8ec9-8f2a1b47a136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f483e4c-45da-44de-88aa-79e5bc71a9d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fdcfba5-9ae9-481d-8ec9-8f2a1b47a136",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "a99e975c-61ad-40c3-bef5-ad2bd94a5c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "adce47b0-5ac8-437f-a532-509742665ece",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99e975c-61ad-40c3-bef5-ad2bd94a5c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "669a8208-4bf1-4d9a-8ad6-9320ce5ef1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99e975c-61ad-40c3-bef5-ad2bd94a5c00",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "cf9c403d-de54-4c78-a690-bf5ad5bb840f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "d7ed2a1a-1a1a-49e1-8647-63ba5cec2aad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf9c403d-de54-4c78-a690-bf5ad5bb840f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe15d66-af86-4a8b-82f6-afd6ecec1e5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf9c403d-de54-4c78-a690-bf5ad5bb840f",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "7dbd5ee4-3c4f-4fc6-a496-6ba993471668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "141a118f-9a7b-4d0b-a77e-348a9e6fb4b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7dbd5ee4-3c4f-4fc6-a496-6ba993471668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7be0a090-7041-4831-978c-92d87ea84387",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7dbd5ee4-3c4f-4fc6-a496-6ba993471668",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        },
        {
            "id": "abc490d1-3425-4ec9-b732-0a9e648d0770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "compositeImage": {
                "id": "d47c1c98-47e0-40a1-8dcd-e929881879df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abc490d1-3425-4ec9-b732-0a9e648d0770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2d2ca20-225f-4b5d-b508-bea2b11cffb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abc490d1-3425-4ec9-b732-0a9e648d0770",
                    "LayerId": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 388,
    "layers": [
        {
            "id": "e7de8fda-f808-43cb-a8b5-ab7e59385ed9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebff9d6b-c3cf-4cf7-b9aa-1e610747b786",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 388,
    "xorig": 194,
    "yorig": 194
}