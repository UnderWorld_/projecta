{
    "id": "dcf564bd-7986-42f3-b614-040f2d2687c6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_forest1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 46,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16eb93ce-9832-488e-837f-8e1935a63ee4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcf564bd-7986-42f3-b614-040f2d2687c6",
            "compositeImage": {
                "id": "786b17ad-441b-4531-bf09-efb3fb36033f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16eb93ce-9832-488e-837f-8e1935a63ee4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9217509f-f1f2-4da4-a8f4-01d1ffe21780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16eb93ce-9832-488e-837f-8e1935a63ee4",
                    "LayerId": "4089c054-b51c-4794-8e1e-30eae8528964"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4089c054-b51c-4794-8e1e-30eae8528964",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcf564bd-7986-42f3-b614-040f2d2687c6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}