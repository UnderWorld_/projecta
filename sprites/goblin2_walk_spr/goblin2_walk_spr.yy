{
    "id": "5785f00c-40d8-4f85-a300-d740eeb93a8d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin2_walk_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 180,
    "bbox_left": 23,
    "bbox_right": 102,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5cfae4ad-49ac-4637-8ab7-109bf864a27c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5785f00c-40d8-4f85-a300-d740eeb93a8d",
            "compositeImage": {
                "id": "6c733e6a-c10a-4667-bcfa-a148cb22b06f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5cfae4ad-49ac-4637-8ab7-109bf864a27c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dd2e28d-9169-4bbc-a64a-63bafe6c3bb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5cfae4ad-49ac-4637-8ab7-109bf864a27c",
                    "LayerId": "240641dd-4f74-4271-bc0c-90875354bde6"
                }
            ]
        },
        {
            "id": "5d636d5a-4885-4d4d-aabc-0b2a6d4ef827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5785f00c-40d8-4f85-a300-d740eeb93a8d",
            "compositeImage": {
                "id": "cb22a290-9ead-4a97-b042-cc0e0970b940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d636d5a-4885-4d4d-aabc-0b2a6d4ef827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0050feaf-70d8-4830-80ec-da1e9d687463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d636d5a-4885-4d4d-aabc-0b2a6d4ef827",
                    "LayerId": "240641dd-4f74-4271-bc0c-90875354bde6"
                }
            ]
        },
        {
            "id": "159b5ada-3eb9-4e1f-9362-c7ccf78b7168",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5785f00c-40d8-4f85-a300-d740eeb93a8d",
            "compositeImage": {
                "id": "2c19b68f-9800-4018-87c7-754445f2de7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "159b5ada-3eb9-4e1f-9362-c7ccf78b7168",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92502446-a4f6-4291-9b3f-1970bda94bb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "159b5ada-3eb9-4e1f-9362-c7ccf78b7168",
                    "LayerId": "240641dd-4f74-4271-bc0c-90875354bde6"
                }
            ]
        },
        {
            "id": "404c5513-0244-43b5-b974-fe76dfaec750",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5785f00c-40d8-4f85-a300-d740eeb93a8d",
            "compositeImage": {
                "id": "ff20bd24-1f6e-4dd0-94e6-f6552a75d739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "404c5513-0244-43b5-b974-fe76dfaec750",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b90e35b8-7037-4fe4-89ff-8e71ed57c234",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "404c5513-0244-43b5-b974-fe76dfaec750",
                    "LayerId": "240641dd-4f74-4271-bc0c-90875354bde6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "240641dd-4f74-4271-bc0c-90875354bde6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5785f00c-40d8-4f85-a300-d740eeb93a8d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 120,
    "xorig": 61,
    "yorig": 78
}