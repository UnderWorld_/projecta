{
    "id": "fbc7c2e0-56e5-406c-bc54-ddd2b5f25b85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_townhouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 32,
    "bbox_right": 95,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e048dc16-3d9a-4779-b7b8-e0f1c40fc72b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fbc7c2e0-56e5-406c-bc54-ddd2b5f25b85",
            "compositeImage": {
                "id": "9c0234a0-1326-4d2d-9033-0d168cdc0b32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e048dc16-3d9a-4779-b7b8-e0f1c40fc72b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057a2840-64a4-4d34-8bd4-0e2be8f8b5f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e048dc16-3d9a-4779-b7b8-e0f1c40fc72b",
                    "LayerId": "a3d35c1d-a8ef-4017-b027-227157fc3778"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a3d35c1d-a8ef-4017-b027-227157fc3778",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fbc7c2e0-56e5-406c-bc54-ddd2b5f25b85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}