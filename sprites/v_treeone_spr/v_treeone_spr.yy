{
    "id": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_treeone_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 9,
    "bbox_right": 20,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b157652-3900-405d-bffc-1fd9fd98f231",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "3e32290f-4fdd-48ce-ae7a-12b5d10761c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b157652-3900-405d-bffc-1fd9fd98f231",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c23cf60-2b3b-4dcd-82cf-e4c80ef2eb58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b157652-3900-405d-bffc-1fd9fd98f231",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "83378356-ceb5-4519-afeb-0f9be87e4b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "58a1ff7a-bdad-424f-9482-929d5b7878d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83378356-ceb5-4519-afeb-0f9be87e4b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d1ffa37-10ea-4037-b653-a0242723eb8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83378356-ceb5-4519-afeb-0f9be87e4b99",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "9c3f53ec-d2b9-4e3f-9e74-a3459fa84692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "8605e5c8-42fe-4bf4-8932-32cfb94c5fa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c3f53ec-d2b9-4e3f-9e74-a3459fa84692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b9b28ee-f0f8-451e-af06-5c83d5cde532",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c3f53ec-d2b9-4e3f-9e74-a3459fa84692",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "6385f077-37ff-47ab-ae48-b44b8f2f09db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "ec2ee5cd-792e-49d6-b652-dce43f42414c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6385f077-37ff-47ab-ae48-b44b8f2f09db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b905fd9f-ecc7-4f7d-aa1f-5bf7554f43b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6385f077-37ff-47ab-ae48-b44b8f2f09db",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "43ae10e8-03f0-4b6c-ac33-2baf5d2a6be3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "d6f94bd8-6d81-4c31-b19f-fc0bc6a7966d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43ae10e8-03f0-4b6c-ac33-2baf5d2a6be3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8ace1b4-0638-434d-8605-104198e40718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43ae10e8-03f0-4b6c-ac33-2baf5d2a6be3",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "0b2fa788-0ebe-4765-81b6-457c885d5235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "af7b4afa-4b11-46e5-a1e2-fd1491878734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b2fa788-0ebe-4765-81b6-457c885d5235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2c162b8-4e24-43fb-ac32-02880c8ee483",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b2fa788-0ebe-4765-81b6-457c885d5235",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "87670585-0442-4e19-9ecb-0617a8651040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "ca510565-3531-4e69-8a3f-3a1767905c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87670585-0442-4e19-9ecb-0617a8651040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd5cfa2-d99d-4fd2-83ca-876ab3a492d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87670585-0442-4e19-9ecb-0617a8651040",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "8c812e4c-e9b5-4907-a467-e9560c29997a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "9c2dc47d-4dc2-4917-a5dc-3b19e4662e2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c812e4c-e9b5-4907-a467-e9560c29997a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21808fa8-7f80-4014-8d25-585cfe50c336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c812e4c-e9b5-4907-a467-e9560c29997a",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ebb577ee-bc2c-4d41-b47d-972f8c1813fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "7fa0c11a-0511-4d94-878f-3b41ca64c1ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb577ee-bc2c-4d41-b47d-972f8c1813fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0be2055a-8884-4630-94a0-041e621667ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb577ee-bc2c-4d41-b47d-972f8c1813fa",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "52cf8141-c604-496d-880a-56562e2151a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "c98fd6bb-3a37-4b9d-9e95-a9568ba08a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52cf8141-c604-496d-880a-56562e2151a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1dd9a309-35df-4d0b-9cd5-25cd4dae6f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52cf8141-c604-496d-880a-56562e2151a9",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "dd8a37ca-957d-448f-a7ba-c1dc35b4abec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "806c97cd-1ecf-472e-9cba-50670e488e01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd8a37ca-957d-448f-a7ba-c1dc35b4abec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ed08368-6596-4aea-bccd-03eca92d170f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd8a37ca-957d-448f-a7ba-c1dc35b4abec",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "6d66e998-9786-41f9-af20-38f2ee56c5bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "30a4a3b1-9690-424a-8ecb-55ee6d576501",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d66e998-9786-41f9-af20-38f2ee56c5bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "710a3cb6-fb24-425b-a239-cc7dec63e24d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d66e998-9786-41f9-af20-38f2ee56c5bf",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "b307fc9d-7d28-4acf-a456-07f9ec3ffb2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "ed77cd9c-aa97-4c0f-b4cf-0091af82c382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b307fc9d-7d28-4acf-a456-07f9ec3ffb2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bac493e-5dbb-4a1a-a5e8-1cbfa4ef896c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b307fc9d-7d28-4acf-a456-07f9ec3ffb2c",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ca5c7be2-227a-4c40-ab6a-27834b858e5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a7f25ad5-234e-4c4a-9698-a1eff920819f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca5c7be2-227a-4c40-ab6a-27834b858e5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53e7b228-ef9e-4fad-a3a5-f1e669f67e68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca5c7be2-227a-4c40-ab6a-27834b858e5e",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "bcba2dd1-80dd-4854-bd27-c223b1829b78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "d731aa28-71b2-4f1f-8633-1e68cdeac7fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcba2dd1-80dd-4854-bd27-c223b1829b78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aafc094b-d461-419a-993e-0ca99f9d33ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcba2dd1-80dd-4854-bd27-c223b1829b78",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "102984ae-dc82-4d2f-a4d6-9b737d5f3b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "c9e5acae-4ee4-43d0-be3f-769bb95dd94d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "102984ae-dc82-4d2f-a4d6-9b737d5f3b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b35c23e-f5a5-47e9-9918-db15c3ad918f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "102984ae-dc82-4d2f-a4d6-9b737d5f3b32",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2fcf5afe-9de1-4534-be42-a7b42098fa27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "5d81de61-a33f-4f8a-aa13-165a50192d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fcf5afe-9de1-4534-be42-a7b42098fa27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32061b63-936d-4d3d-a443-af45ce603f13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fcf5afe-9de1-4534-be42-a7b42098fa27",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ad50fd7d-f236-4481-b3f5-7ca4bbf1a50b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "47ac35df-9f13-496d-9f69-24d49d4d4dd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad50fd7d-f236-4481-b3f5-7ca4bbf1a50b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b27a825-9a0c-42c3-8150-ac4d38daf18c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad50fd7d-f236-4481-b3f5-7ca4bbf1a50b",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "0e74bfb1-e33d-4719-bc9a-0bc0aceab1cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "c5d54756-31d6-4732-a7e0-441598041b24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e74bfb1-e33d-4719-bc9a-0bc0aceab1cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8cc2079-1fb5-48de-b471-5736c6681277",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e74bfb1-e33d-4719-bc9a-0bc0aceab1cb",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "67a06269-4465-4ce1-a157-04d85e26a507",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "7c0d1c89-e237-4363-8a84-7a73ded1cd22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a06269-4465-4ce1-a157-04d85e26a507",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7948ef75-9c1d-4ba8-a6ca-486f1f71d180",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a06269-4465-4ce1-a157-04d85e26a507",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "a68660cb-9532-4dd6-852a-ff8bcc604616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "966cdd70-f601-47ac-9d1d-fed483d70f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a68660cb-9532-4dd6-852a-ff8bcc604616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71c5e17e-6a4a-4e9c-89b4-dfe4c787aa87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a68660cb-9532-4dd6-852a-ff8bcc604616",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "7f237bf5-72db-4306-abed-adb85072fcbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "8410cc6f-df6a-4935-9bf3-1d0bf492097c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f237bf5-72db-4306-abed-adb85072fcbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c371d26-534b-4061-bc3a-b9359243c9ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f237bf5-72db-4306-abed-adb85072fcbe",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "5bc512a3-1dc1-422d-908d-0acbb51b5306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "242bac9a-4925-4a17-8d19-1d5a741c337d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bc512a3-1dc1-422d-908d-0acbb51b5306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9373c93-60e7-409d-8d3c-e5b0c5ea78c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bc512a3-1dc1-422d-908d-0acbb51b5306",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "e712c76b-cd53-485f-a821-c7ddd491a029",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "10b72841-7df6-451c-88ae-8fed3f3d552d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e712c76b-cd53-485f-a821-c7ddd491a029",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d449e9f-d3f4-4e3a-b0ed-5d28080aa341",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e712c76b-cd53-485f-a821-c7ddd491a029",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "f11f444f-95bc-4bfe-bda0-6ee9c8d1184a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "e564b3ed-48a7-4fbf-a5f8-8eb2c35f3550",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f11f444f-95bc-4bfe-bda0-6ee9c8d1184a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a944e6e8-1440-4965-b1c8-61db77602045",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f11f444f-95bc-4bfe-bda0-6ee9c8d1184a",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "6e6082f2-2732-4d6e-af1a-a747d20428c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "f87d502f-6062-4ea3-922a-df27c5597575",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e6082f2-2732-4d6e-af1a-a747d20428c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1f494ee-e2d3-4763-a082-6e050c1cd7d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e6082f2-2732-4d6e-af1a-a747d20428c5",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "4d1c436a-1595-429e-aae9-7dec65228ac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a6a0f4b2-6d1c-4ef5-af19-f12eeef4ad9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1c436a-1595-429e-aae9-7dec65228ac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27b3056d-8e22-4cde-aa33-24debeed3a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1c436a-1595-429e-aae9-7dec65228ac8",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2cca3fa0-7dd2-4b84-9a48-642b1f44aa6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "21b562a3-1398-466a-9df0-dd4b0ea804df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cca3fa0-7dd2-4b84-9a48-642b1f44aa6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4181df33-a96b-4cba-8948-5767f050b33a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cca3fa0-7dd2-4b84-9a48-642b1f44aa6f",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "e5b8adf2-6ee6-44e2-966f-6ca517a114e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "88a60815-fa8a-44a0-9193-f5c824461a6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5b8adf2-6ee6-44e2-966f-6ca517a114e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54825b06-2ab9-4b60-aca0-86fd622388d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5b8adf2-6ee6-44e2-966f-6ca517a114e0",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ce2372d3-4d57-41d0-b9dd-877f2ce0237a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "f6a6c9fd-a077-4cdf-bba6-7dfa01e76c05",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce2372d3-4d57-41d0-b9dd-877f2ce0237a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6363726-2c44-4dba-8b8f-fbf1faf7042d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce2372d3-4d57-41d0-b9dd-877f2ce0237a",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "20646d26-ecbe-49f4-aba0-7532332b2583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "d91f0795-f6b5-4eb5-9640-b71c61a1aefa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20646d26-ecbe-49f4-aba0-7532332b2583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afd9fc0f-4d00-42cd-869b-f0c342d22b8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20646d26-ecbe-49f4-aba0-7532332b2583",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "1cbac139-65ff-43b9-b1af-365a4126c948",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "ed1bdc2b-e018-4572-9afc-d9ac57be167f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cbac139-65ff-43b9-b1af-365a4126c948",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158593c9-c896-4b9a-a1a0-81629ba00f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cbac139-65ff-43b9-b1af-365a4126c948",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d1504e68-0e92-4a6f-a849-25b23a8b55d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "028ad5d3-232d-40e9-b9f4-e036815d7a11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1504e68-0e92-4a6f-a849-25b23a8b55d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc8b08cc-6a15-4a4d-9cff-ed9d1e0add88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1504e68-0e92-4a6f-a849-25b23a8b55d7",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "151ef651-5f69-4e0a-ae28-3d96a333714f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "63dcbdfe-bcdf-4ec5-b027-d9a34a340f0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "151ef651-5f69-4e0a-ae28-3d96a333714f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9838f23-0553-42b3-880f-7c94d7bf774e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "151ef651-5f69-4e0a-ae28-3d96a333714f",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "1acd6ed5-6bd6-49e8-b8d5-81b982b8d49a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "4df29ad3-90dc-4797-b632-604ef769e066",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1acd6ed5-6bd6-49e8-b8d5-81b982b8d49a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9da92be9-ed43-4fe8-864c-4fe8c35f821a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1acd6ed5-6bd6-49e8-b8d5-81b982b8d49a",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "3174f29a-bfd8-446a-80fa-c8cec0faef55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a90a27f7-7930-44ca-8ed6-389922437b13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3174f29a-bfd8-446a-80fa-c8cec0faef55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0080d9a2-5ed4-4a7e-ad37-86ce7deebb71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3174f29a-bfd8-446a-80fa-c8cec0faef55",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2522d4f2-9c9b-4aee-ae47-0247685329cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "35fbe833-7807-4738-b220-869f4c48f273",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2522d4f2-9c9b-4aee-ae47-0247685329cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d5b6d93-a71a-4bfe-abed-9ec92f86ac58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2522d4f2-9c9b-4aee-ae47-0247685329cf",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "0dcd6c7b-3875-4b24-8f35-69069ea657bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "bdcefd99-ea06-4b89-8c4d-36f009906f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dcd6c7b-3875-4b24-8f35-69069ea657bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea412d3-ba68-44e9-8ab3-048107332941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dcd6c7b-3875-4b24-8f35-69069ea657bf",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "f786ca3a-da22-4799-ab63-c957e2143aef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "08201906-ac0b-4cf5-aa43-016336fc5268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f786ca3a-da22-4799-ab63-c957e2143aef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3994ebe-ce10-4ea3-b8b2-510cda1f224f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f786ca3a-da22-4799-ab63-c957e2143aef",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "91ff9308-6a58-41cb-a4db-cccf0201c6b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "98e838c8-f957-4570-8991-f897e0a8314e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ff9308-6a58-41cb-a4db-cccf0201c6b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11e22aab-a892-49c0-87dc-a714538e19c3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ff9308-6a58-41cb-a4db-cccf0201c6b6",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "034cadf8-164f-4c3e-bfb8-c8dc2ea887a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "e3c38822-5d4e-4608-88da-ff13d6838be3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "034cadf8-164f-4c3e-bfb8-c8dc2ea887a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8202fc31-90e3-4ce4-8694-646a74fa70fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "034cadf8-164f-4c3e-bfb8-c8dc2ea887a8",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "0cececf2-1e3a-4e33-b65d-49388b0eb362",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a4a13560-1b34-40dd-9609-05d00e5c89f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cececf2-1e3a-4e33-b65d-49388b0eb362",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fffac1ac-e25f-4717-839e-c52813c931a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cececf2-1e3a-4e33-b65d-49388b0eb362",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "93fc5685-9745-43dd-94ad-39a5bff0b6a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "25948191-6636-4667-bef8-4f7890839664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93fc5685-9745-43dd-94ad-39a5bff0b6a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c1eb588-d2d0-4464-8b96-88937cf34258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93fc5685-9745-43dd-94ad-39a5bff0b6a9",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ea037f3b-38a9-436d-8a43-8b74380de32a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "91e1ff3c-8fa6-4fe5-a0a7-3726479d1199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea037f3b-38a9-436d-8a43-8b74380de32a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d23b4694-793b-4598-8993-9145b1692e0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea037f3b-38a9-436d-8a43-8b74380de32a",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "a2539bba-f088-4ece-af2c-cfa52b73cdc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "bdfe2008-05bb-42c0-b30b-e15b329d4cb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2539bba-f088-4ece-af2c-cfa52b73cdc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d987ffdb-c409-4575-8647-c5d1a165566e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2539bba-f088-4ece-af2c-cfa52b73cdc7",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "da149767-8e50-46f2-b0b5-4e7e08193846",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "479052f3-8d8d-4036-ad02-04f03a7aee6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da149767-8e50-46f2-b0b5-4e7e08193846",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dad229b9-fab2-4a01-8d2c-ee6be9310375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da149767-8e50-46f2-b0b5-4e7e08193846",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "6af0521f-3489-426c-8d6d-5e0948770373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "6f4c72be-0e3a-4cb4-9f64-de49bc7fbd8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6af0521f-3489-426c-8d6d-5e0948770373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30cd6230-16a2-48e5-a100-36223ea7a4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6af0521f-3489-426c-8d6d-5e0948770373",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "a47f16d6-f8bb-4689-8f98-f4af11fe1706",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "ad7dd6e8-85d4-4f81-a3b9-fbb863f5fe4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a47f16d6-f8bb-4689-8f98-f4af11fe1706",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61dd9761-5222-4ae0-8323-ceaa555a24f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a47f16d6-f8bb-4689-8f98-f4af11fe1706",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "8ca8917c-87ea-4b7f-9c14-a5fd3adf45a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "63308c68-979d-4701-9aa5-921f938d5c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ca8917c-87ea-4b7f-9c14-a5fd3adf45a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc5dc192-2f41-4a27-a320-592bd84d2b16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ca8917c-87ea-4b7f-9c14-a5fd3adf45a1",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d24365b1-0317-46a8-a6ca-0dc44c5077be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "168ef193-8566-4e62-af21-a9a9a05b6e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d24365b1-0317-46a8-a6ca-0dc44c5077be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75345a87-7d69-4dd0-b36c-2b55548d8214",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d24365b1-0317-46a8-a6ca-0dc44c5077be",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ec8335ee-b263-4a19-b865-e3d34dcb9d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "f1a4222c-9481-4580-95c1-168274d00a2b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec8335ee-b263-4a19-b865-e3d34dcb9d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab908d4b-f5bc-4f85-bd54-050a5c6a9bfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec8335ee-b263-4a19-b865-e3d34dcb9d39",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "ad2bf8d1-d1ea-480d-a846-c4f27c5a8566",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "5110b860-9839-46f4-98f1-bcc6b6351677",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2bf8d1-d1ea-480d-a846-c4f27c5a8566",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a12df67-e090-4f76-ae11-25dbf88874bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2bf8d1-d1ea-480d-a846-c4f27c5a8566",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "12d97080-5106-4d98-b3dc-e5b9a8ade452",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "13fa9aaa-8c16-4c4a-ac90-83a17f7d64eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12d97080-5106-4d98-b3dc-e5b9a8ade452",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46ec572b-5150-4dfd-9a60-f4ff34267d4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12d97080-5106-4d98-b3dc-e5b9a8ade452",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "06ce05c4-a74e-4bb6-8b25-4d91c1c4b237",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "921cab03-0f76-475c-aca4-613acef84814",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ce05c4-a74e-4bb6-8b25-4d91c1c4b237",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5dcda17-8d7e-4e0d-bfc3-eef7145acf35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ce05c4-a74e-4bb6-8b25-4d91c1c4b237",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "457b6dfd-c4c0-40a7-b386-eb40de5d3d1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "16f0631c-e3a8-4569-bc95-c79d4fbfc854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "457b6dfd-c4c0-40a7-b386-eb40de5d3d1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffdf37ed-6799-408b-bda2-ab31c1f4e84e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "457b6dfd-c4c0-40a7-b386-eb40de5d3d1c",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "cd016c69-a9d3-4bbf-b4e6-d7933523a6d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "1de2fac8-59f5-46f7-be0a-b4436d058d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd016c69-a9d3-4bbf-b4e6-d7933523a6d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "522ee160-1bed-459b-b6e0-035010b8eb4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd016c69-a9d3-4bbf-b4e6-d7933523a6d3",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "750dd0ef-115f-436e-acfa-8534ae5cf59b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "17b7dfc2-b04c-42db-8d36-f693fa5af2fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "750dd0ef-115f-436e-acfa-8534ae5cf59b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eef321e3-102d-491d-84b8-ed8a6fc79f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "750dd0ef-115f-436e-acfa-8534ae5cf59b",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "14b5badf-9d71-4c7f-9789-2ec8d2a5a973",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "21f10563-a5fd-401c-931b-ec0ab98642bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b5badf-9d71-4c7f-9789-2ec8d2a5a973",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2463d3c5-36cc-4e31-9733-86a6cd8a2df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b5badf-9d71-4c7f-9789-2ec8d2a5a973",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "02633462-b8b0-460c-beb7-6d393e3cab20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "1edbd6e3-7c8d-426f-bb94-2304fb32da68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02633462-b8b0-460c-beb7-6d393e3cab20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2be6993e-eb7e-4263-8e97-c9f12641a23d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02633462-b8b0-460c-beb7-6d393e3cab20",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "9fbd5d1c-81b4-4a58-97dc-e818806a9bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "986e2a1f-07ff-4fa9-a98b-3d11f9e2f1d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fbd5d1c-81b4-4a58-97dc-e818806a9bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e54cb542-2407-4742-b8a9-acdd09535f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fbd5d1c-81b4-4a58-97dc-e818806a9bc8",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2a964aff-6180-4037-bd18-16a54bc4f223",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "c26dc731-9a3d-46ae-8a7f-6423a0620907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a964aff-6180-4037-bd18-16a54bc4f223",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9837d97-b608-4726-97f5-b7b707799a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a964aff-6180-4037-bd18-16a54bc4f223",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "622f86a2-3635-4f65-9c55-f98e09299dbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a1a49c6d-a89e-4d47-9bce-151f395cc70f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "622f86a2-3635-4f65-9c55-f98e09299dbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd0fadc7-0e3a-4afb-a313-624578a06511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "622f86a2-3635-4f65-9c55-f98e09299dbe",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "6aab60cc-9f1a-4b50-b98e-8027170a2536",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "dbbca744-9c29-47e2-a14e-ff65370b4b4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aab60cc-9f1a-4b50-b98e-8027170a2536",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a559769e-ad91-41ca-959c-b84750913c4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aab60cc-9f1a-4b50-b98e-8027170a2536",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "635a74f7-5197-40b8-86e9-0b3df4aab1f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a172f023-a6f0-4139-a055-77fd3b6b2842",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "635a74f7-5197-40b8-86e9-0b3df4aab1f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6836a0e-decf-406e-8e43-ddd08d386855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "635a74f7-5197-40b8-86e9-0b3df4aab1f6",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "67cfd86e-8da9-4ac0-8d36-854cd404aa52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "8be640b6-fc32-4aba-8300-a0e92786b4a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67cfd86e-8da9-4ac0-8d36-854cd404aa52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0056871d-bf4c-4c9d-ae4e-a44bba92899f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67cfd86e-8da9-4ac0-8d36-854cd404aa52",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "091ef2b3-89bc-4bf0-950a-a4b621f6a736",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "f2640199-df79-4208-b9a1-61c1d7dc27c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091ef2b3-89bc-4bf0-950a-a4b621f6a736",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d3fcd0d-4992-4c28-8e13-836d13a104a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091ef2b3-89bc-4bf0-950a-a4b621f6a736",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "b04312a5-e47e-4724-afe3-2d9653480983",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a5d53f78-f52d-4c74-92f0-7a19e6640f5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b04312a5-e47e-4724-afe3-2d9653480983",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c04c5eca-986a-476e-a4fa-301273698b03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b04312a5-e47e-4724-afe3-2d9653480983",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "3b86a50f-75ef-4535-96b3-f02bd158f6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "5ae3f284-7c73-434e-a258-680c4058459e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b86a50f-75ef-4535-96b3-f02bd158f6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5776c07-8f0b-4677-8c49-8c982cda0da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b86a50f-75ef-4535-96b3-f02bd158f6b5",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "fcd47be8-085b-4242-991e-399a8ec7db33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "2ba05abe-8454-4417-9d0a-f86a7f03a734",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcd47be8-085b-4242-991e-399a8ec7db33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49688712-8a44-4d0d-9c36-08034fd37786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcd47be8-085b-4242-991e-399a8ec7db33",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "40e7490e-4e38-401c-a254-d2d6d4fa997f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "de5fe24f-1653-4247-a220-15f738087a89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40e7490e-4e38-401c-a254-d2d6d4fa997f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfda1d71-d721-4ceb-8220-73fabd5a344c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40e7490e-4e38-401c-a254-d2d6d4fa997f",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "46b8cd7c-cb40-4df7-8db8-4e7354504339",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "8c427205-09b1-442a-a883-2b7203f33833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46b8cd7c-cb40-4df7-8db8-4e7354504339",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cff5bed-2446-4185-ad15-d4e2ee5e6ee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46b8cd7c-cb40-4df7-8db8-4e7354504339",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "be2da33d-e49a-4682-8813-79da4fd63195",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "3ce625de-c239-459c-8271-1f8d14464fe0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be2da33d-e49a-4682-8813-79da4fd63195",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78e7ba1-bcd8-4f79-9f43-b3c67fe6fc77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be2da33d-e49a-4682-8813-79da4fd63195",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d160bb19-8521-4412-ae6e-d23e1747cd3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "5b694a9a-7fd8-45a1-9d89-63151076184c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d160bb19-8521-4412-ae6e-d23e1747cd3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf463f48-113c-42b4-8663-fa50f6838997",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d160bb19-8521-4412-ae6e-d23e1747cd3c",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "87e184cd-212f-4067-8ffc-e9f55693d6e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "62781487-1d25-4509-a482-541b56276e0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87e184cd-212f-4067-8ffc-e9f55693d6e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "348d046d-53b6-42c1-9707-402be9b99017",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87e184cd-212f-4067-8ffc-e9f55693d6e4",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "26747d33-de5a-411a-93dc-e654bc51f0da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "b149f770-548e-4067-b707-54c72ef8c0d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26747d33-de5a-411a-93dc-e654bc51f0da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b3b2a6d-563b-4aa3-9073-162198b4a010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26747d33-de5a-411a-93dc-e654bc51f0da",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2c34ca28-0866-46b0-be39-4387027eb9ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "202f4e95-ec2c-44f1-8085-5af109379c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c34ca28-0866-46b0-be39-4387027eb9ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3acc36-92c7-49f1-b80b-be1975626517",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c34ca28-0866-46b0-be39-4387027eb9ff",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "432a6f82-e622-4dc9-8a3a-8654feef0ddf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "b9134153-2fa1-46ac-8c6a-d5ebde48233c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "432a6f82-e622-4dc9-8a3a-8654feef0ddf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "874eb9ca-180a-4e3f-ba38-05517b91f2ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "432a6f82-e622-4dc9-8a3a-8654feef0ddf",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "5b0aff29-d1b4-4222-b471-d6bac45837db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "1d48d8d7-2583-4eb0-8ad9-b5558b284cca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0aff29-d1b4-4222-b471-d6bac45837db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a41e97a9-d217-4ec4-a315-3124e7b4d87b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0aff29-d1b4-4222-b471-d6bac45837db",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "88872a2a-d434-48d1-a207-140f59f2f902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "9a53f2f0-921a-4332-a527-ebe01ad22df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88872a2a-d434-48d1-a207-140f59f2f902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08535e11-468c-4df4-bbbc-ac80aa2ece65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88872a2a-d434-48d1-a207-140f59f2f902",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "b501b3ba-413e-495f-a8e2-ad93c91a9028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "27721841-e665-4dbe-b7ff-bcc8bc192324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b501b3ba-413e-495f-a8e2-ad93c91a9028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf83d567-1633-4340-87a8-7a2256ed6bb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b501b3ba-413e-495f-a8e2-ad93c91a9028",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "b8640490-dd6f-4b65-8284-208653fb4997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "8076f4de-c70f-4808-889f-55dd5ba2c9be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8640490-dd6f-4b65-8284-208653fb4997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c65c8b1-98a5-4403-9185-948189d4a932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8640490-dd6f-4b65-8284-208653fb4997",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "81cbcdac-787e-4e89-8349-8587637503fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a478cd2d-309c-41cd-842f-a3cc680249e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81cbcdac-787e-4e89-8349-8587637503fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a70af55d-d644-4a96-924f-1e8ff30c1731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81cbcdac-787e-4e89-8349-8587637503fd",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d42f1a19-460d-44de-83fe-6015dc193bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "7df10d7e-b824-4c60-b439-7c6a2ae80030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d42f1a19-460d-44de-83fe-6015dc193bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "646406b7-5d0a-4aa7-883b-484584f54370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d42f1a19-460d-44de-83fe-6015dc193bcb",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "641fe47e-b1d4-4c3b-aee5-053296feec3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "1b62ed2c-1c9b-44ae-aaa8-742e4c881c5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "641fe47e-b1d4-4c3b-aee5-053296feec3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a52e74c-71be-4bf6-baab-443df3ae74ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "641fe47e-b1d4-4c3b-aee5-053296feec3f",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "591aeab2-07ad-4b54-b34e-e0bde61adbea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "dae8e9da-20be-45bd-b9d7-61994e8fa7f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "591aeab2-07ad-4b54-b34e-e0bde61adbea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e60e21-def4-4282-a0a8-c353123d63eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "591aeab2-07ad-4b54-b34e-e0bde61adbea",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "63913d4a-1978-457a-8812-d12faefc617d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "7f120dd3-739a-4033-bea7-161f7de3a6ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63913d4a-1978-457a-8812-d12faefc617d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3de34919-4758-4e25-b2a2-112246decd47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63913d4a-1978-457a-8812-d12faefc617d",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d8c36c0b-1d49-4ca7-8a72-140a2c48c091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "b8756699-f31f-4eab-9514-33add77506a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8c36c0b-1d49-4ca7-8a72-140a2c48c091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66d16cec-683b-4ad4-bc69-a852d8a548f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8c36c0b-1d49-4ca7-8a72-140a2c48c091",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "abeda515-2132-4b2a-92ed-be31e3967a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "431a7942-56a9-4dd4-9917-7e5075bb7bfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abeda515-2132-4b2a-92ed-be31e3967a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd3b1b5e-166d-4660-858e-c5c4b5ba5b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abeda515-2132-4b2a-92ed-be31e3967a35",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2e5c48aa-048e-4715-ac21-eaea26ae3f59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "5a10d29a-11a2-43f5-a45f-47906f6e59dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e5c48aa-048e-4715-ac21-eaea26ae3f59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f3fd446-e290-4e8f-8e94-bf7127aba516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e5c48aa-048e-4715-ac21-eaea26ae3f59",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "03042e11-b4f5-49a3-a59d-f9eaadbd88bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "32686f80-c8a4-4175-8b6f-d65e7f0cb772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03042e11-b4f5-49a3-a59d-f9eaadbd88bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8460a49-483b-4a3b-bb9f-3303d7d4cd75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03042e11-b4f5-49a3-a59d-f9eaadbd88bc",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "0f9ffdeb-f355-43b4-bafa-955dfcb84092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "20bfa55f-5eb9-434b-8740-52584d3ba636",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f9ffdeb-f355-43b4-bafa-955dfcb84092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ea74056-f74b-4989-8c27-6b800480ab7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f9ffdeb-f355-43b4-bafa-955dfcb84092",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "fa0e55e9-31c7-4915-ae8e-b2a3f9e006f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "1926b584-d597-472f-8de3-d01725590918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa0e55e9-31c7-4915-ae8e-b2a3f9e006f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e18fd91e-2f39-45b6-8b1d-d6f11b48a74c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa0e55e9-31c7-4915-ae8e-b2a3f9e006f1",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "96288a7f-c8e2-4f35-be60-216881547cc5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "781a1327-17b4-4824-88cf-4a00d98d4559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96288a7f-c8e2-4f35-be60-216881547cc5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13104743-9fb0-4820-b767-846979e5de7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96288a7f-c8e2-4f35-be60-216881547cc5",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "03e2e43c-dd14-4dd3-87c8-7243b8d7e5c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "a147bd9a-84dd-4110-9980-8582d3667930",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e2e43c-dd14-4dd3-87c8-7243b8d7e5c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c44ab8-a8a0-40d8-81a2-0f5ed1b2d239",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e2e43c-dd14-4dd3-87c8-7243b8d7e5c7",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "c4eb1776-7d21-474e-adc7-b7a8be9dbfe3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "2fe7bde1-a58c-4cc5-bb51-373339cb5ff3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4eb1776-7d21-474e-adc7-b7a8be9dbfe3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ed89139-5751-48cb-a773-50078ee063cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4eb1776-7d21-474e-adc7-b7a8be9dbfe3",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "4eed2270-48bd-493e-a57a-ec5f7b7def1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "3122cdaa-b50f-4378-8ead-dc3e0e835f9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eed2270-48bd-493e-a57a-ec5f7b7def1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b18891-df59-42c7-b8c4-c0a86cc897e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eed2270-48bd-493e-a57a-ec5f7b7def1a",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d3ece39b-197b-4c9e-b594-ba023fe91d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "c869e345-02a5-4fc0-8fd2-7df69f1ec075",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ece39b-197b-4c9e-b594-ba023fe91d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb8508ed-f1b2-42ab-b091-5f90a503731d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ece39b-197b-4c9e-b594-ba023fe91d74",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "3f4329ec-0bbf-46ef-942d-f373f72d847c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "1b17524c-c4d4-45a5-a93b-c378058e76c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f4329ec-0bbf-46ef-942d-f373f72d847c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8827eb-adf6-46c6-9004-38588f9b4c0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f4329ec-0bbf-46ef-942d-f373f72d847c",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "b85a3395-43db-4f23-be21-725e533cd533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "35bcb853-d5d9-4b63-b03d-3a3442c14c29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b85a3395-43db-4f23-be21-725e533cd533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de6f875b-de1e-4358-b850-73201d537703",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b85a3395-43db-4f23-be21-725e533cd533",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "a88578ab-7f28-4aa5-b155-d12a92facc52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "fc3be9ff-1241-47b6-8076-856cb90e9c8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a88578ab-7f28-4aa5-b155-d12a92facc52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f40e0693-e653-4562-b54c-0f528680e4e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a88578ab-7f28-4aa5-b155-d12a92facc52",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "9cb0949c-d9b4-4b7e-bf30-3280ec4419f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "fc820d40-7310-4871-adff-4464c40a5939",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cb0949c-d9b4-4b7e-bf30-3280ec4419f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e632531-1e1b-4443-a1b3-c5e413ae1de5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cb0949c-d9b4-4b7e-bf30-3280ec4419f6",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "2bfe9afa-6726-4aae-9f1d-ab0aa7ab10f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "09587380-5faa-485a-85b8-fe7aa7f0294b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bfe9afa-6726-4aae-9f1d-ab0aa7ab10f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff77f3e4-3c2f-48ff-9d2a-2247c7f5b4fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bfe9afa-6726-4aae-9f1d-ab0aa7ab10f5",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "d6fd2f66-3eff-44fd-9646-7b2be69b3949",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "f03c52a2-9f23-4d30-a2fe-78c8b5796c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6fd2f66-3eff-44fd-9646-7b2be69b3949",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04c615f9-f82f-4569-9782-153fe6b7fd7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6fd2f66-3eff-44fd-9646-7b2be69b3949",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "6706568f-91d5-4959-8c00-13f5a1c13b30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "6763ed4a-458a-4d9c-9a7d-7a8625775c00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6706568f-91d5-4959-8c00-13f5a1c13b30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27daf751-25b9-41b4-9c8f-c6bcd15b9bde",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6706568f-91d5-4959-8c00-13f5a1c13b30",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "c4b6bc4f-7b99-409e-af8c-d16f2a14fbc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "28bae147-1f30-41bd-8435-f52a6b7724d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b6bc4f-7b99-409e-af8c-d16f2a14fbc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0375ebc5-f038-4e34-9be6-7d5402a6742b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b6bc4f-7b99-409e-af8c-d16f2a14fbc7",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "0b8aae2c-a6c6-44c0-990d-f87e346ba2a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "6ab07c29-11f4-4e30-aabe-6446ef64f5bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b8aae2c-a6c6-44c0-990d-f87e346ba2a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581ef3c1-d559-4209-b186-83f4bc9d48ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b8aae2c-a6c6-44c0-990d-f87e346ba2a5",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        },
        {
            "id": "81aa62d5-415f-4c63-a611-c82498a75861",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "compositeImage": {
                "id": "2ff54d2d-b62b-4c9c-9e09-4de5a64b4296",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81aa62d5-415f-4c63-a611-c82498a75861",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f57d91b4-0265-43bd-aae1-dd2b4f60acc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81aa62d5-415f-4c63-a611-c82498a75861",
                    "LayerId": "d13d198e-0b3e-4367-bad3-cc1563980c49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d13d198e-0b3e-4367-bad3-cc1563980c49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3a87cc7-76ea-408b-9728-26f3fabad7b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}