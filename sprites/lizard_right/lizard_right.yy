{
    "id": "d15aed7c-c1c3-48bd-8cd1-f61bf56bb36f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lizard_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 20,
    "bbox_right": 67,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ea8b93a-de8e-4963-9f74-a60d676a1a99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d15aed7c-c1c3-48bd-8cd1-f61bf56bb36f",
            "compositeImage": {
                "id": "b0f25e91-0e2d-4b0f-aac1-ec05a5389682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ea8b93a-de8e-4963-9f74-a60d676a1a99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20008d3-3a7b-4723-b7b4-4d47204f5e42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ea8b93a-de8e-4963-9f74-a60d676a1a99",
                    "LayerId": "bd68b764-cfdf-4317-b0b1-f08c8dc67a8e"
                }
            ]
        },
        {
            "id": "36a9fd24-93e8-44ad-94dc-711dccc678fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d15aed7c-c1c3-48bd-8cd1-f61bf56bb36f",
            "compositeImage": {
                "id": "5563d6ec-79cd-46a2-b09c-31074148048a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a9fd24-93e8-44ad-94dc-711dccc678fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f168ba8e-2d9a-473c-8569-770354a9093e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a9fd24-93e8-44ad-94dc-711dccc678fd",
                    "LayerId": "bd68b764-cfdf-4317-b0b1-f08c8dc67a8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "bd68b764-cfdf-4317-b0b1-f08c8dc67a8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d15aed7c-c1c3-48bd-8cd1-f61bf56bb36f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 39,
    "yorig": 4
}