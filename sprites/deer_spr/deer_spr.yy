{
    "id": "27bcb68a-e9af-4286-a748-a7bbe2961dd6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "deer_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 12,
    "bbox_right": 87,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca00c093-bca7-46ab-b609-32629a70938c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27bcb68a-e9af-4286-a748-a7bbe2961dd6",
            "compositeImage": {
                "id": "dc80d2e7-20e4-49b2-954c-6fa11eb04938",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca00c093-bca7-46ab-b609-32629a70938c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e6951c2-cf47-4ad7-a6c0-04ba0f1d89f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca00c093-bca7-46ab-b609-32629a70938c",
                    "LayerId": "64d4b4f0-6dbe-47e9-9a98-116f73898e0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "64d4b4f0-6dbe-47e9-9a98-116f73898e0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27bcb68a-e9af-4286-a748-a7bbe2961dd6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}