{
    "id": "e0bc2553-b845-44e8-b469-d81be15b4b40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tileset1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9584cbf-1f41-4744-a1fc-d1db673274b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0bc2553-b845-44e8-b469-d81be15b4b40",
            "compositeImage": {
                "id": "d4e6b749-6a6d-4d5f-9cd4-a73c0a7f879a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9584cbf-1f41-4744-a1fc-d1db673274b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad113eec-522b-4ee6-a736-5e6f9321e06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9584cbf-1f41-4744-a1fc-d1db673274b1",
                    "LayerId": "7a0131ed-b014-4685-b629-fae3ea7f1f2b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7a0131ed-b014-4685-b629-fae3ea7f1f2b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0bc2553-b845-44e8-b469-d81be15b4b40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}