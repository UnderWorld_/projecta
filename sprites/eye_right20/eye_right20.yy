{
    "id": "2fc324e6-5f1b-49cc-bfea-17ba46c2a5bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2e83426-c25d-47a0-a4fc-bad561a6e52c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc324e6-5f1b-49cc-bfea-17ba46c2a5bc",
            "compositeImage": {
                "id": "a4fdeeef-b439-4a51-aa60-893b5b2450fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2e83426-c25d-47a0-a4fc-bad561a6e52c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a030e79-ec66-426a-ab8b-9c2c7075fef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2e83426-c25d-47a0-a4fc-bad561a6e52c",
                    "LayerId": "57d24735-e044-447f-8a3a-a46b2a8bc646"
                }
            ]
        },
        {
            "id": "9cbcf22c-fa4f-4a2a-ad64-2752a918d89c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fc324e6-5f1b-49cc-bfea-17ba46c2a5bc",
            "compositeImage": {
                "id": "c4e69d76-e8c1-4130-98a7-64b77dbba8ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cbcf22c-fa4f-4a2a-ad64-2752a918d89c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebc10fab-af00-46d8-bc63-028849edab25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cbcf22c-fa4f-4a2a-ad64-2752a918d89c",
                    "LayerId": "57d24735-e044-447f-8a3a-a46b2a8bc646"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "57d24735-e044-447f-8a3a-a46b2a8bc646",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fc324e6-5f1b-49cc-bfea-17ba46c2a5bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}