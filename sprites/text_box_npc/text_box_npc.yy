{
    "id": "267f2421-2c2a-4ff1-8224-71a25bd723b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "text_box_npc",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 379,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c62faef8-b59f-476b-8728-f7fc6d9168f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "267f2421-2c2a-4ff1-8224-71a25bd723b4",
            "compositeImage": {
                "id": "d1214e79-641c-48c4-890a-2c96a903119a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c62faef8-b59f-476b-8728-f7fc6d9168f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af35e47a-b902-4ec4-aec2-bc16ed428bf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c62faef8-b59f-476b-8728-f7fc6d9168f5",
                    "LayerId": "36322e68-05c0-4fa2-9158-b80d084ca33d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "36322e68-05c0-4fa2-9158-b80d084ca33d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "267f2421-2c2a-4ff1-8224-71a25bd723b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 380,
    "xorig": 190,
    "yorig": 180
}