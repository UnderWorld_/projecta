{
    "id": "0b561cb4-6c6f-4530-9bc4-97fee1d63973",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_walk_up_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 29,
    "bbox_right": 99,
    "bbox_top": 50,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c32fad40-0efe-4105-a13a-e799c57ba42f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b561cb4-6c6f-4530-9bc4-97fee1d63973",
            "compositeImage": {
                "id": "0f3cc874-cae9-4d71-b477-fec45a931a99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c32fad40-0efe-4105-a13a-e799c57ba42f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca7c88f7-f109-43d8-9bb3-3733625ca841",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c32fad40-0efe-4105-a13a-e799c57ba42f",
                    "LayerId": "155650e9-0766-4d30-9e64-557e4d713bb0"
                }
            ]
        },
        {
            "id": "1c3274d8-2c55-4566-a1b5-b291aab740c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0b561cb4-6c6f-4530-9bc4-97fee1d63973",
            "compositeImage": {
                "id": "f8ca215c-01f4-4ff1-b794-b94c7dd77017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c3274d8-2c55-4566-a1b5-b291aab740c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d8b2e86-a7f2-4a75-8c3c-9fa2c0bd5a19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c3274d8-2c55-4566-a1b5-b291aab740c3",
                    "LayerId": "155650e9-0766-4d30-9e64-557e4d713bb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "155650e9-0766-4d30-9e64-557e4d713bb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0b561cb4-6c6f-4530-9bc4-97fee1d63973",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 31,
    "yorig": 51
}