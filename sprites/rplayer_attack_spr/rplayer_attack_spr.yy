{
    "id": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_attack_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 38,
    "bbox_right": 89,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1aed7278-2806-4f82-8464-51cf4e6e0936",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "8fdf2e27-2e7f-48a1-a73c-9c21b08c29e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1aed7278-2806-4f82-8464-51cf4e6e0936",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd9c4a2d-5241-405f-8a18-dfe2810d9cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1aed7278-2806-4f82-8464-51cf4e6e0936",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "a05ccd2d-b399-4daa-a5ef-5e81226b4b12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "8d39ffa4-45b4-40ca-8f85-d03fb00d34c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a05ccd2d-b399-4daa-a5ef-5e81226b4b12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "761193c7-7a2a-4efa-8d7f-81db9979891f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a05ccd2d-b399-4daa-a5ef-5e81226b4b12",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "ecc542e1-5c43-4400-8c42-f5a08d8bdb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "873f0a0f-fc68-4dc0-a082-4f00a705251e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc542e1-5c43-4400-8c42-f5a08d8bdb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e34ad185-6456-43db-916c-f01ae2ae063a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc542e1-5c43-4400-8c42-f5a08d8bdb87",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "cbd0484c-e167-47f8-8670-b8ec0450bf7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "9d571607-f893-4060-b1aa-414c6a19e9df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbd0484c-e167-47f8-8670-b8ec0450bf7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce27f6ea-363c-4d8b-b132-3792e40329e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbd0484c-e167-47f8-8670-b8ec0450bf7f",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "107e2156-4d89-4075-be74-9096716c558d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "bfb46d34-6afd-4673-99b3-ebfdb4b2ed4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "107e2156-4d89-4075-be74-9096716c558d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4faaf7e-75e0-41ce-9adc-766796444e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "107e2156-4d89-4075-be74-9096716c558d",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "e8e6baa3-6000-4388-96cd-cfd56feff7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "ca6baaec-1777-46ff-a7a0-90f5bf9db1d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8e6baa3-6000-4388-96cd-cfd56feff7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc7db002-c1a0-402c-a631-47a05f673134",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8e6baa3-6000-4388-96cd-cfd56feff7b7",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "fc66a8f4-8608-45ef-a8bf-05a2d974d92f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "2f54a998-1221-457f-95dc-f2afa0e6e4dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc66a8f4-8608-45ef-a8bf-05a2d974d92f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8f41afb-accb-442a-abfc-8bc3f9207904",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc66a8f4-8608-45ef-a8bf-05a2d974d92f",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "0d85246c-2832-4aaf-ba36-b295a4e14e75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "44ff7fa5-a281-4981-a6c6-2122063179fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d85246c-2832-4aaf-ba36-b295a4e14e75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c83854-7c03-4cbd-94d0-f9d0e5289dbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d85246c-2832-4aaf-ba36-b295a4e14e75",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "25998744-6388-43b5-9b21-a0dd0f0d373d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "a7d5fe8d-d417-46e1-8aa3-b6ddf0a50818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25998744-6388-43b5-9b21-a0dd0f0d373d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dea4dd7-8497-4d7b-b8d0-e02224e65f78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25998744-6388-43b5-9b21-a0dd0f0d373d",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        },
        {
            "id": "5be583d8-55e4-42c1-8b51-24f26404af20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "compositeImage": {
                "id": "89bc5c63-97f1-4872-9fb7-b844a09f0cce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5be583d8-55e4-42c1-8b51-24f26404af20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a9bbb9-ff70-4689-b435-9e9b71b5b4c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5be583d8-55e4-42c1-8b51-24f26404af20",
                    "LayerId": "a70fe364-7594-488f-81a1-473d30220e9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a70fe364-7594-488f-81a1-473d30220e9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20eee01a-4230-4f69-9bab-6c7aca812ba3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 66,
    "yorig": 66
}