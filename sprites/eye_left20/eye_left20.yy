{
    "id": "c9fba06c-7a31-4a0e-b41e-9f6592f8f588",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a26cbe1-f61f-4add-8178-691ad6a49474",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9fba06c-7a31-4a0e-b41e-9f6592f8f588",
            "compositeImage": {
                "id": "f448cba9-d39f-4fbe-98ae-eabdc0af86ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a26cbe1-f61f-4add-8178-691ad6a49474",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "280bc402-2e35-4f4d-a43f-788cb4a979aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a26cbe1-f61f-4add-8178-691ad6a49474",
                    "LayerId": "d607e203-b83a-4286-8c5c-bb718112c725"
                }
            ]
        },
        {
            "id": "37d59ea3-b905-438e-bfcd-5efda3da3378",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9fba06c-7a31-4a0e-b41e-9f6592f8f588",
            "compositeImage": {
                "id": "83e2d71a-d9e3-411a-9a89-7e2f3e02d526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37d59ea3-b905-438e-bfcd-5efda3da3378",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e308e83b-8900-4ad9-840f-abf40b7e9dbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37d59ea3-b905-438e-bfcd-5efda3da3378",
                    "LayerId": "d607e203-b83a-4286-8c5c-bb718112c725"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "d607e203-b83a-4286-8c5c-bb718112c725",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9fba06c-7a31-4a0e-b41e-9f6592f8f588",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}