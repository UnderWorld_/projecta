{
    "id": "8c281671-f3d9-4c19-8f99-7e59bd041907",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite265",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 5,
    "bbox_right": 64,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "364254ab-3b14-4822-8f7e-95a3ff46aaea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c281671-f3d9-4c19-8f99-7e59bd041907",
            "compositeImage": {
                "id": "84f6a2ea-5523-4fde-a1ba-7d7f120f6004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "364254ab-3b14-4822-8f7e-95a3ff46aaea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0fc3094-5e2f-4f51-844d-ab389796eff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "364254ab-3b14-4822-8f7e-95a3ff46aaea",
                    "LayerId": "86a232ca-a4fb-4663-acc4-843a70fb7ce5"
                }
            ]
        },
        {
            "id": "3531e9b9-7c4d-4007-9e92-bea4225a7954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c281671-f3d9-4c19-8f99-7e59bd041907",
            "compositeImage": {
                "id": "b4be5e2e-92ae-48cf-967f-2df9a18f0a24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3531e9b9-7c4d-4007-9e92-bea4225a7954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef85c4e9-9c84-49bb-a49b-c0c9fe8d2166",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3531e9b9-7c4d-4007-9e92-bea4225a7954",
                    "LayerId": "86a232ca-a4fb-4663-acc4-843a70fb7ce5"
                }
            ]
        },
        {
            "id": "777ace66-5899-451e-81da-1d1b8688f8ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c281671-f3d9-4c19-8f99-7e59bd041907",
            "compositeImage": {
                "id": "d97b0cdf-c919-4bde-a189-7d2270a50daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "777ace66-5899-451e-81da-1d1b8688f8ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3852aee-6327-4235-913d-45ca1fb7929a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "777ace66-5899-451e-81da-1d1b8688f8ac",
                    "LayerId": "86a232ca-a4fb-4663-acc4-843a70fb7ce5"
                }
            ]
        },
        {
            "id": "9940a99f-b4f3-4e39-a819-145d3af772ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c281671-f3d9-4c19-8f99-7e59bd041907",
            "compositeImage": {
                "id": "e8d3cc41-60d4-4bc7-8289-27adb9864951",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9940a99f-b4f3-4e39-a819-145d3af772ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9a959ad-6e5e-4521-9743-79a7b3fc7e2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9940a99f-b4f3-4e39-a819-145d3af772ae",
                    "LayerId": "86a232ca-a4fb-4663-acc4-843a70fb7ce5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "86a232ca-a4fb-4663-acc4-843a70fb7ce5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c281671-f3d9-4c19-8f99-7e59bd041907",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 35,
    "yorig": 50
}