{
    "id": "0934abc7-901c-4f3e-9a2e-49dc71a48c87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shrub5_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d453a43f-1c5c-43a7-a49f-3f4984b9c992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0934abc7-901c-4f3e-9a2e-49dc71a48c87",
            "compositeImage": {
                "id": "207935c7-f229-4586-8adf-5f68fabbf51e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d453a43f-1c5c-43a7-a49f-3f4984b9c992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b4c6a4-0059-40f3-bf49-fe64c98419ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d453a43f-1c5c-43a7-a49f-3f4984b9c992",
                    "LayerId": "33a5c0eb-b614-4eb4-b7d8-9ba69d74d9fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "33a5c0eb-b614-4eb4-b7d8-9ba69d74d9fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0934abc7-901c-4f3e-9a2e-49dc71a48c87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}