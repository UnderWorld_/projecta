{
    "id": "92d6e811-778b-4942-abc7-b0df94b43200",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin3_walk_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4365f45a-d7a7-46e1-bf80-88fe47650dde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d6e811-778b-4942-abc7-b0df94b43200",
            "compositeImage": {
                "id": "05747ca7-2471-47a5-90c6-d450135eff0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4365f45a-d7a7-46e1-bf80-88fe47650dde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cb7ba79-5afa-40e9-a2c0-693da70a87bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4365f45a-d7a7-46e1-bf80-88fe47650dde",
                    "LayerId": "059e054f-48b7-407f-ad6f-6790c4732f71"
                }
            ]
        },
        {
            "id": "51170a5e-ff44-40d5-8bb3-7802eec075db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d6e811-778b-4942-abc7-b0df94b43200",
            "compositeImage": {
                "id": "0be243fd-cbbb-4092-82fb-5eb3ec081fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51170a5e-ff44-40d5-8bb3-7802eec075db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c03f188-bca5-4f03-a723-77baa80c349b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51170a5e-ff44-40d5-8bb3-7802eec075db",
                    "LayerId": "059e054f-48b7-407f-ad6f-6790c4732f71"
                }
            ]
        },
        {
            "id": "21218d9e-cf97-4ac4-85bb-07425eb55788",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d6e811-778b-4942-abc7-b0df94b43200",
            "compositeImage": {
                "id": "b905b738-ac11-49f4-8767-ed65d4872332",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21218d9e-cf97-4ac4-85bb-07425eb55788",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "456271d3-8b16-4a8e-97e9-462fcc4d7939",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21218d9e-cf97-4ac4-85bb-07425eb55788",
                    "LayerId": "059e054f-48b7-407f-ad6f-6790c4732f71"
                }
            ]
        },
        {
            "id": "69ffcf0d-fa97-4949-8dd1-406e5c211345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d6e811-778b-4942-abc7-b0df94b43200",
            "compositeImage": {
                "id": "fbc0d8dc-e45d-4b73-b027-661e63a6d916",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69ffcf0d-fa97-4949-8dd1-406e5c211345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c3b0a27-7541-45f5-886f-5d2ef7e678a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69ffcf0d-fa97-4949-8dd1-406e5c211345",
                    "LayerId": "059e054f-48b7-407f-ad6f-6790c4732f71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "059e054f-48b7-407f-ad6f-6790c4732f71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92d6e811-778b-4942-abc7-b0df94b43200",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 88,
    "yorig": 220
}