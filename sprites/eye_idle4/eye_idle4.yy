{
    "id": "1ca126f7-8789-4b36-8738-898b759734d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0d9a707-49ab-43aa-9ba3-bc46e0eea121",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ca126f7-8789-4b36-8738-898b759734d2",
            "compositeImage": {
                "id": "8ce90a32-bffd-4ee9-ad23-a7c31dfdb004",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0d9a707-49ab-43aa-9ba3-bc46e0eea121",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f59d150-a9ce-4c83-b238-c352649ffa8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0d9a707-49ab-43aa-9ba3-bc46e0eea121",
                    "LayerId": "578116e2-0660-4526-9b67-bbbd139ca314"
                }
            ]
        },
        {
            "id": "7cdfb4a1-89fa-41e8-8984-d07a08d73f3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ca126f7-8789-4b36-8738-898b759734d2",
            "compositeImage": {
                "id": "58e72496-b9d0-4a9e-b071-93cb1da3793a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cdfb4a1-89fa-41e8-8984-d07a08d73f3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b615aa7c-977d-47f8-9cf9-2e949163af1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cdfb4a1-89fa-41e8-8984-d07a08d73f3c",
                    "LayerId": "578116e2-0660-4526-9b67-bbbd139ca314"
                }
            ]
        },
        {
            "id": "926d9a85-f63c-493a-89e7-820ffb286c37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ca126f7-8789-4b36-8738-898b759734d2",
            "compositeImage": {
                "id": "3b7f48ee-fa3f-4cc0-b2f0-2abcd0fcc74c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "926d9a85-f63c-493a-89e7-820ffb286c37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50518331-6fa6-4af7-9ca4-3fa18c676153",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "926d9a85-f63c-493a-89e7-820ffb286c37",
                    "LayerId": "578116e2-0660-4526-9b67-bbbd139ca314"
                }
            ]
        },
        {
            "id": "34c5ae99-3b15-4fc0-9d3c-909eeccd32f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1ca126f7-8789-4b36-8738-898b759734d2",
            "compositeImage": {
                "id": "478817fa-b7fd-4fcd-9bea-1d7778d56a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34c5ae99-3b15-4fc0-9d3c-909eeccd32f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36493117-0cf4-42fb-a3f2-a058e763ba43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34c5ae99-3b15-4fc0-9d3c-909eeccd32f0",
                    "LayerId": "578116e2-0660-4526-9b67-bbbd139ca314"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "578116e2-0660-4526-9b67-bbbd139ca314",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1ca126f7-8789-4b36-8738-898b759734d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}