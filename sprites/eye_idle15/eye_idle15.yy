{
    "id": "15f462f1-262b-48fb-9334-f0ba52fc5961",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f96d1037-031b-4003-a4d6-d3eaa44f09d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15f462f1-262b-48fb-9334-f0ba52fc5961",
            "compositeImage": {
                "id": "39b20181-2409-4646-b52a-8b35bbc89988",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f96d1037-031b-4003-a4d6-d3eaa44f09d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af0af201-2c7f-4cc9-bc3d-4ce58a68e8da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f96d1037-031b-4003-a4d6-d3eaa44f09d3",
                    "LayerId": "b3abe73b-4967-47cc-8f85-13d1e1804ece"
                }
            ]
        },
        {
            "id": "9908852c-ee97-4320-86ea-72877152c388",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15f462f1-262b-48fb-9334-f0ba52fc5961",
            "compositeImage": {
                "id": "06cb06db-ed31-4c18-a6dd-4794531d348c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9908852c-ee97-4320-86ea-72877152c388",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a92444c-e83a-4e1c-a07f-abdb0c2148dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9908852c-ee97-4320-86ea-72877152c388",
                    "LayerId": "b3abe73b-4967-47cc-8f85-13d1e1804ece"
                }
            ]
        },
        {
            "id": "7524817b-4c0f-4ab2-aaa4-ff4cf0c4fcc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15f462f1-262b-48fb-9334-f0ba52fc5961",
            "compositeImage": {
                "id": "3c14b7a9-c1c7-495d-8750-534254862850",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7524817b-4c0f-4ab2-aaa4-ff4cf0c4fcc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96cc4d02-9f34-45c1-a8a9-c461fce01657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7524817b-4c0f-4ab2-aaa4-ff4cf0c4fcc0",
                    "LayerId": "b3abe73b-4967-47cc-8f85-13d1e1804ece"
                }
            ]
        },
        {
            "id": "a414a079-0afd-4781-b148-d4c1b0b56692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15f462f1-262b-48fb-9334-f0ba52fc5961",
            "compositeImage": {
                "id": "62b27224-a6b6-41ae-bceb-7bfb3363f8a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a414a079-0afd-4781-b148-d4c1b0b56692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1483a74a-41c7-4138-ad9b-bbfa3aa7eb2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a414a079-0afd-4781-b148-d4c1b0b56692",
                    "LayerId": "b3abe73b-4967-47cc-8f85-13d1e1804ece"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "b3abe73b-4967-47cc-8f85-13d1e1804ece",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15f462f1-262b-48fb-9334-f0ba52fc5961",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}