{
    "id": "0e6541b9-6af8-481d-ac8e-b680dbac3e31",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shrub4_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ae81209-2574-46e8-a7b6-e606ee0aaf46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e6541b9-6af8-481d-ac8e-b680dbac3e31",
            "compositeImage": {
                "id": "1640756c-6cf2-4247-920d-a1d45f3258b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ae81209-2574-46e8-a7b6-e606ee0aaf46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a07b26c8-568f-4293-b9e4-1e392af2fcf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ae81209-2574-46e8-a7b6-e606ee0aaf46",
                    "LayerId": "945b5853-544d-464c-8dfc-ecdf1693cf3e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "945b5853-544d-464c-8dfc-ecdf1693cf3e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e6541b9-6af8-481d-ac8e-b680dbac3e31",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 15
}