{
    "id": "9447168f-ae1b-45e6-a506-f3bfabf6c5ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_darkforest2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 46,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e6539027-1a95-496d-be92-4ce69d32cad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9447168f-ae1b-45e6-a506-f3bfabf6c5ac",
            "compositeImage": {
                "id": "f5752fc1-2a4f-411c-a20f-358346193c0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6539027-1a95-496d-be92-4ce69d32cad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e59ffb92-f0f1-4742-b81b-b73c11ca8c43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6539027-1a95-496d-be92-4ce69d32cad5",
                    "LayerId": "7d1767d1-4058-40e5-8b5e-eccfc951701f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7d1767d1-4058-40e5-8b5e-eccfc951701f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9447168f-ae1b-45e6-a506-f3bfabf6c5ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}