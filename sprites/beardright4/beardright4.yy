{
    "id": "063a7129-fbbf-426e-9bf4-bdee05699084",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardright4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d33d0d4e-fe3e-4d6e-a1f8-0dcefee57ca6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "063a7129-fbbf-426e-9bf4-bdee05699084",
            "compositeImage": {
                "id": "e592eb10-8fad-430f-a499-bdb2d0b5c9e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33d0d4e-fe3e-4d6e-a1f8-0dcefee57ca6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14cbf28c-650b-42cf-bf98-59fd33c5fb43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33d0d4e-fe3e-4d6e-a1f8-0dcefee57ca6",
                    "LayerId": "f5ae3598-85e9-4f11-9a73-7e6386864623"
                }
            ]
        },
        {
            "id": "81f65ad1-81ff-4c82-b8ae-b16e8a36b88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "063a7129-fbbf-426e-9bf4-bdee05699084",
            "compositeImage": {
                "id": "92663e92-ab9f-48c5-93a8-550e95fbfb82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f65ad1-81ff-4c82-b8ae-b16e8a36b88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4378a2b3-b203-4d8f-bf87-0d0a752cde29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f65ad1-81ff-4c82-b8ae-b16e8a36b88f",
                    "LayerId": "f5ae3598-85e9-4f11-9a73-7e6386864623"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f5ae3598-85e9-4f11-9a73-7e6386864623",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "063a7129-fbbf-426e-9bf4-bdee05699084",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}