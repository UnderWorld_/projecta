{
    "id": "76cc67c5-c453-4b6c-b12a-14e0680f9452",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c57bef26-20b2-496b-9919-78992cc1daf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76cc67c5-c453-4b6c-b12a-14e0680f9452",
            "compositeImage": {
                "id": "40ad33da-9fcf-4300-968a-99da74ee4f4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c57bef26-20b2-496b-9919-78992cc1daf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d6c89c4-6dfd-40e3-ae55-135cc9faddc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c57bef26-20b2-496b-9919-78992cc1daf4",
                    "LayerId": "bf865b17-42e7-4f4d-91e9-b4c45155f3ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "bf865b17-42e7-4f4d-91e9-b4c45155f3ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76cc67c5-c453-4b6c-b12a-14e0680f9452",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}