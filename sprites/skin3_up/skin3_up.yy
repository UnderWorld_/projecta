{
    "id": "25618619-e131-4626-a89b-f2a9a8f43421",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin3_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "266b9069-b8f0-4696-b871-248d4864901a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25618619-e131-4626-a89b-f2a9a8f43421",
            "compositeImage": {
                "id": "54ee2fc8-202b-4443-b577-01d50c730c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "266b9069-b8f0-4696-b871-248d4864901a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2caf814c-1241-495b-899e-f7486c413f49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "266b9069-b8f0-4696-b871-248d4864901a",
                    "LayerId": "62f4b0a4-dcf3-4219-87dc-6d59c4fe4c05"
                }
            ]
        },
        {
            "id": "d93b0051-885d-4525-9562-ad9908770bd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "25618619-e131-4626-a89b-f2a9a8f43421",
            "compositeImage": {
                "id": "9f65add3-2878-4c83-9d97-4ac14123feed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d93b0051-885d-4525-9562-ad9908770bd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08fe0f51-6669-44d8-b163-f3beb5d03fb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d93b0051-885d-4525-9562-ad9908770bd3",
                    "LayerId": "62f4b0a4-dcf3-4219-87dc-6d59c4fe4c05"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "62f4b0a4-dcf3-4219-87dc-6d59c4fe4c05",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "25618619-e131-4626-a89b-f2a9a8f43421",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}