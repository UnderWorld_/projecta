{
    "id": "411ecd3b-4b44-4aaf-92c8-16ba0ea11644",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_tileset1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8515a632-1794-4be3-b848-a0a8a3db209e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "411ecd3b-4b44-4aaf-92c8-16ba0ea11644",
            "compositeImage": {
                "id": "2dd9c385-f896-491b-bf4e-7bd38726a910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8515a632-1794-4be3-b848-a0a8a3db209e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c9861e5-74d1-4361-b703-c172026b4e2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8515a632-1794-4be3-b848-a0a8a3db209e",
                    "LayerId": "7f799f95-9565-4d39-b838-f834ee673688"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "7f799f95-9565-4d39-b838-f834ee673688",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "411ecd3b-4b44-4aaf-92c8-16ba0ea11644",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}