{
    "id": "f8b51f97-3e61-4ad5-9cab-5fd57f073ba1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_tree",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 2,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ab9c816-1d7c-425a-bd4b-d9b5c4183d0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8b51f97-3e61-4ad5-9cab-5fd57f073ba1",
            "compositeImage": {
                "id": "069ee565-457b-4561-9fd8-9314ac0c0264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ab9c816-1d7c-425a-bd4b-d9b5c4183d0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a9bb978-7d3a-44fe-ba6b-4026250bf1dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ab9c816-1d7c-425a-bd4b-d9b5c4183d0c",
                    "LayerId": "88afb838-7661-4197-a674-01ed17ae6612"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "88afb838-7661-4197-a674-01ed17ae6612",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8b51f97-3e61-4ad5-9cab-5fd57f073ba1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}