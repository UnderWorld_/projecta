{
    "id": "e6e22ebb-2ac7-4dd4-8f15-a6ca391f6862",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grown_gobrit1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 97,
    "bbox_left": 17,
    "bbox_right": 90,
    "bbox_top": 33,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db312304-8b25-4b0e-8a37-4ec9735c9071",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e22ebb-2ac7-4dd4-8f15-a6ca391f6862",
            "compositeImage": {
                "id": "376f4d8a-b4f4-4a57-9331-e6fa28f499aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db312304-8b25-4b0e-8a37-4ec9735c9071",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8ef0033-a347-42af-ac7d-c47576b43ef8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db312304-8b25-4b0e-8a37-4ec9735c9071",
                    "LayerId": "e66ab131-a069-40bd-af0a-bcad241b3da9"
                }
            ]
        },
        {
            "id": "9fc515ba-0204-41ab-a462-af089816412b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6e22ebb-2ac7-4dd4-8f15-a6ca391f6862",
            "compositeImage": {
                "id": "c79394c1-e556-4319-b4e0-f94726215e9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc515ba-0204-41ab-a462-af089816412b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba293e0-587d-4fa3-b3a5-e06ce2979439",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc515ba-0204-41ab-a462-af089816412b",
                    "LayerId": "e66ab131-a069-40bd-af0a-bcad241b3da9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "e66ab131-a069-40bd-af0a-bcad241b3da9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6e22ebb-2ac7-4dd4-8f15-a6ca391f6862",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 49,
    "yorig": 108
}