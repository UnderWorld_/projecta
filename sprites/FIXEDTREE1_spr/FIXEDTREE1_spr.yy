{
    "id": "512796e0-c52f-42a3-907f-c3f8016637d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "FIXEDTREE1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 195,
    "bbox_left": 149,
    "bbox_right": 193,
    "bbox_top": 152,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6da6a0a4-8324-4e80-ba54-1a5afe113ae4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "a0595e6c-6dbe-4f46-8296-0ca275dc8fff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6da6a0a4-8324-4e80-ba54-1a5afe113ae4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bf063f2-8dd6-4c89-8049-5b6e279a18bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6da6a0a4-8324-4e80-ba54-1a5afe113ae4",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "4e33d73b-85a5-4812-9058-8c5d35dce633",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4117372a-6150-4df5-9763-64246b459ef7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e33d73b-85a5-4812-9058-8c5d35dce633",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fd0ab93-9a2a-4b39-97d0-5f0b23ec3412",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e33d73b-85a5-4812-9058-8c5d35dce633",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "fdf9bcec-94bc-4d15-a121-175cb7b6540d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2690288a-9249-448e-95d3-907dd62b69a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdf9bcec-94bc-4d15-a121-175cb7b6540d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e113d953-dbfd-4769-a2f8-5c039a391b5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdf9bcec-94bc-4d15-a121-175cb7b6540d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "fae9a6be-61ae-4fda-b8b5-b3ba562a653a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9100e944-1add-4ee2-abfc-c9979143f04d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fae9a6be-61ae-4fda-b8b5-b3ba562a653a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fff42f80-f49f-4a6b-9a25-7a8b78e99de6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fae9a6be-61ae-4fda-b8b5-b3ba562a653a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "fdb35223-5ac1-4140-81cf-1a2c91333590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "19b3dcb8-43e2-46d6-ac8c-2d5e1f59372a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdb35223-5ac1-4140-81cf-1a2c91333590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ad79a7b-b8e8-4243-acd5-83057fd9f5ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdb35223-5ac1-4140-81cf-1a2c91333590",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "7c9ad71c-0b85-4ff3-9526-ab3fb4ed647d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "614523d4-e91a-4f36-bc5c-672ce0861256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c9ad71c-0b85-4ff3-9526-ab3fb4ed647d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c911e8-6a2e-42e0-ac00-525ff00a05a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c9ad71c-0b85-4ff3-9526-ab3fb4ed647d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "9a7b3086-0b60-421d-b8c3-574af5333efd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "bf5c8c92-96d9-4e20-9015-81f673290519",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a7b3086-0b60-421d-b8c3-574af5333efd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6bec68b-aae2-4367-827b-43a57fe85474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a7b3086-0b60-421d-b8c3-574af5333efd",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "2254bdcb-9400-4eab-a279-a4c961a555f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "06771ca7-9a54-4c31-90e3-62cd95d9ba85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2254bdcb-9400-4eab-a279-a4c961a555f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a86a06ad-6fbb-479d-a159-432fa74b5953",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2254bdcb-9400-4eab-a279-a4c961a555f2",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "98a2beb0-7a78-4a46-b729-7035ca0c1695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "94e921ef-a302-4407-bd90-8a22a5a591bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98a2beb0-7a78-4a46-b729-7035ca0c1695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fb69bf8-d965-4697-91a7-552f4343ec9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98a2beb0-7a78-4a46-b729-7035ca0c1695",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "02cadb1b-d35d-4182-8b67-4f2e07a032a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "bee8014e-2e5e-410d-9e9f-7ea31b5ad010",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02cadb1b-d35d-4182-8b67-4f2e07a032a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "171327d2-a636-4e36-83bd-1700caf12fd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02cadb1b-d35d-4182-8b67-4f2e07a032a6",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "9835bf0a-ee86-4e34-9c83-78ac8d0af985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "085c1069-c281-4a96-a68f-49c3ed9703e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9835bf0a-ee86-4e34-9c83-78ac8d0af985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa8c7166-f5f7-4c28-9dfc-32e82dfe1125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9835bf0a-ee86-4e34-9c83-78ac8d0af985",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "98c399a4-b775-48d1-92ac-89736cc95361",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "87bdd881-c9a3-4c34-b396-4f71b7c22702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c399a4-b775-48d1-92ac-89736cc95361",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "191ac5d1-b313-4e6d-bdd6-e3f356afcb72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c399a4-b775-48d1-92ac-89736cc95361",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "59472d74-31c4-48f6-b8a8-141a9ac78f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "08fb6050-e275-4e6e-a793-8f2d6b7dffe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59472d74-31c4-48f6-b8a8-141a9ac78f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d2fd63f-8484-40fb-9368-4df5f9f0d9a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59472d74-31c4-48f6-b8a8-141a9ac78f78",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "c5d6d458-b4f0-4bb6-9d35-9f3c15bf616d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "e1c63b0a-28f3-44d4-8ce4-8dc5a3fb8c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d6d458-b4f0-4bb6-9d35-9f3c15bf616d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10fd03b0-5604-4a16-b1a2-f2a3b2a42e32",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d6d458-b4f0-4bb6-9d35-9f3c15bf616d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "5a7f9e7c-cb51-4aa9-8847-963079e523fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1b739138-19b6-4240-b349-2d33e1908509",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a7f9e7c-cb51-4aa9-8847-963079e523fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3155aa8-ad34-4c4d-8cb1-b15330c83048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a7f9e7c-cb51-4aa9-8847-963079e523fe",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "3216150c-210e-4d97-be34-8dbfe7e66f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1d75f7f2-1f7a-4051-bf2e-5eae900e936f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3216150c-210e-4d97-be34-8dbfe7e66f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1522256e-cce3-44af-a723-d98eedaa1dc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3216150c-210e-4d97-be34-8dbfe7e66f31",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1cd8440f-6ff7-4148-b6b1-ffcf6f387668",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "82dda77f-a788-4924-9657-23357cf9f090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1cd8440f-6ff7-4148-b6b1-ffcf6f387668",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cd3f20c-ca02-4a22-92f8-4571b3342716",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1cd8440f-6ff7-4148-b6b1-ffcf6f387668",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "fd23ccc1-4007-46f2-9d5c-5f43e53dd0b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "c574dfa6-21aa-4e1a-8050-df820b6486c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd23ccc1-4007-46f2-9d5c-5f43e53dd0b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5d4238c-b1bf-40e6-ae15-4feb4fb47290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd23ccc1-4007-46f2-9d5c-5f43e53dd0b6",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "05eafe3c-ee10-47ad-8c40-cfc25de6a145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "a27be344-7f6f-40c8-9a56-bb683ae8ee43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05eafe3c-ee10-47ad-8c40-cfc25de6a145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e098396-9e4c-41ff-a646-5083ebad3cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05eafe3c-ee10-47ad-8c40-cfc25de6a145",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "d456d3c6-b2ea-4643-aa8b-6932832995a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "d09d5734-b38a-433e-b576-562fe12714fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d456d3c6-b2ea-4643-aa8b-6932832995a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19585543-5446-4bf3-b1bf-346a5aa98453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d456d3c6-b2ea-4643-aa8b-6932832995a9",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1a5cc63e-c046-45be-9f19-dbdc162410da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "ac00c46d-94c1-48ed-be92-8387b9b9ac33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a5cc63e-c046-45be-9f19-dbdc162410da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f7abac5-2dbd-49b2-9d21-35afb1ad8d82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a5cc63e-c046-45be-9f19-dbdc162410da",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "6477d810-f96a-4fac-8394-ecbfff82caf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9bbfeb33-eb60-4779-ad9d-ef3931400ef6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6477d810-f96a-4fac-8394-ecbfff82caf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5029619b-c336-46e6-b4c5-98d3efc59217",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6477d810-f96a-4fac-8394-ecbfff82caf7",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "980302cd-d5d8-4087-a51d-3a2e3fda82cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1988911a-1794-4da5-9b77-3d5d271e8107",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "980302cd-d5d8-4087-a51d-3a2e3fda82cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c872b745-967e-4479-b35c-47e527c62ef9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "980302cd-d5d8-4087-a51d-3a2e3fda82cf",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "936d87dd-0411-4528-a944-97010693fcaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "63b5903d-f183-4449-a4f1-7873b7c1a883",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "936d87dd-0411-4528-a944-97010693fcaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c4af87-e82a-4737-af5c-e4f1969a0494",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "936d87dd-0411-4528-a944-97010693fcaa",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "b932c8b8-36f0-4c09-87df-d829faf4fcce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "401f953a-59f5-43b1-b1ea-fb7fed2dc9fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b932c8b8-36f0-4c09-87df-d829faf4fcce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cf4d40f-0bf9-431d-9887-e9158a1916c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b932c8b8-36f0-4c09-87df-d829faf4fcce",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "c661fc98-7ea0-4b8e-be31-8681c26c9f10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "8539be52-b21a-4fd2-8c8b-94998c90dd3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c661fc98-7ea0-4b8e-be31-8681c26c9f10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a0fcbd-2913-4214-883d-08b079c1574c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c661fc98-7ea0-4b8e-be31-8681c26c9f10",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "8cd52016-6017-4288-b376-4bef31a61e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9d8c34af-5eaa-4e0c-9220-cc63c9c52440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd52016-6017-4288-b376-4bef31a61e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70a1495-f1c3-451e-bcba-10f38f621392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd52016-6017-4288-b376-4bef31a61e5a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "3af14d4b-8114-4182-81f0-92825b60973d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1cdb05c6-7818-48f1-b077-907ea217d243",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af14d4b-8114-4182-81f0-92825b60973d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7166f5b2-af7d-4ce4-bbb0-a697db6d41bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af14d4b-8114-4182-81f0-92825b60973d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "74601bc4-df05-4453-bb1a-4abd4982d4a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "41f01056-50f6-474c-909c-787eb3445beb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74601bc4-df05-4453-bb1a-4abd4982d4a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc459579-960e-4dac-8a72-0cb8a868d599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74601bc4-df05-4453-bb1a-4abd4982d4a1",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "4dfeda2e-672b-4c0a-b42b-697225ba7220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "03563225-8099-4039-9eb7-938c4683f4af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dfeda2e-672b-4c0a-b42b-697225ba7220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a9a3c7f-adcc-417c-bf63-e078e9552cf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dfeda2e-672b-4c0a-b42b-697225ba7220",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "26c34aa3-0717-4db3-93bd-eb10b0f855b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "ba86faec-4ed4-4728-a74b-d4b8b3efd823",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c34aa3-0717-4db3-93bd-eb10b0f855b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4304aa2f-0f3c-4cb5-acf6-93b1936c38d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c34aa3-0717-4db3-93bd-eb10b0f855b4",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "2ca60c14-6139-4ae3-8401-57b3af9ea921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "e653f939-9b21-4d87-b38f-54a0ec52a517",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ca60c14-6139-4ae3-8401-57b3af9ea921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61ae7aee-1602-4ff6-b20b-185675c23921",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ca60c14-6139-4ae3-8401-57b3af9ea921",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "44e3993f-26e8-4556-bf5f-d36e640815c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "645ff6ab-081d-4f74-80e0-31a54790af90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44e3993f-26e8-4556-bf5f-d36e640815c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "664d253e-ed80-4821-802d-54db07914343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44e3993f-26e8-4556-bf5f-d36e640815c3",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "ed9a9fc5-769b-457b-873a-1bd01f0f155f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "6171ff7e-3db5-4a20-a9e5-44e1e5852cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9a9fc5-769b-457b-873a-1bd01f0f155f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162883a7-5575-407a-a8e6-ba4bf365f5e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9a9fc5-769b-457b-873a-1bd01f0f155f",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1de971ec-b053-419f-80f6-9ef57223f70a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "d7b6b546-0dbd-4146-ae3f-c52876025afb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de971ec-b053-419f-80f6-9ef57223f70a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "158c4fcc-c2da-4d73-ae18-5d8b48ba420b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de971ec-b053-419f-80f6-9ef57223f70a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "e65633af-1da9-46a7-aa0b-56135b22df06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "7ef5326d-6318-4c1f-bd88-dc7766ae5eb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e65633af-1da9-46a7-aa0b-56135b22df06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd951754-2c0a-44c0-9c01-9408f361c381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e65633af-1da9-46a7-aa0b-56135b22df06",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "ac8c23b8-0b47-4bf4-b683-dd1331261069",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9fae10f7-231a-45f0-bf71-c91329906f25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8c23b8-0b47-4bf4-b683-dd1331261069",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10e19f2d-f177-4cde-89e3-d22fc7fee43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8c23b8-0b47-4bf4-b683-dd1331261069",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "6d7024a0-b535-4185-8a25-a54800da4592",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2ac68422-688e-4df8-8c49-7d6f29a5e4d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d7024a0-b535-4185-8a25-a54800da4592",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c80df377-4f6f-471f-85a6-9b22c7adb749",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d7024a0-b535-4185-8a25-a54800da4592",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "721d56c2-13a8-4f9f-81ff-c2bc1c147c4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "0fd64fbe-00eb-4e30-af6c-511d3259762b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "721d56c2-13a8-4f9f-81ff-c2bc1c147c4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4885df69-9fd3-4e3d-8ef4-e0c0528e0113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "721d56c2-13a8-4f9f-81ff-c2bc1c147c4d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "6bd92999-37ec-4d79-9391-12e85098a991",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "455e8564-0b39-4ca5-895f-b1dad6f8d17c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bd92999-37ec-4d79-9391-12e85098a991",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bb760d3-3ae7-4fa0-9617-bafd655d20c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bd92999-37ec-4d79-9391-12e85098a991",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "e64dd304-1483-40ed-bfea-79367e453432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "fec2a3fb-ec13-4435-aece-67ac24a64182",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e64dd304-1483-40ed-bfea-79367e453432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e9ffb41-44d1-4193-ba7e-7dbd71c5c6a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e64dd304-1483-40ed-bfea-79367e453432",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "57bb0342-2851-440b-add5-70f2684f4046",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4083ff51-4219-45ea-b7b2-4ef40dd3b11f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57bb0342-2851-440b-add5-70f2684f4046",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf9275a-bb2c-452b-aa1f-f01b16e3a474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57bb0342-2851-440b-add5-70f2684f4046",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "ad2f6962-e924-49d2-9feb-58969ebcc81b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "c12b12ea-fdec-4b42-9a0d-5f2d47910422",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad2f6962-e924-49d2-9feb-58969ebcc81b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81bf395d-340d-4d9b-808e-53f031837894",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad2f6962-e924-49d2-9feb-58969ebcc81b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "86e15065-fe50-4b51-9702-a32fa03ec4f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "c0b5d5df-34de-4834-95cc-1553bfc6bd87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e15065-fe50-4b51-9702-a32fa03ec4f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f085e39c-0c73-4e32-b3b0-2fd46d04f850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e15065-fe50-4b51-9702-a32fa03ec4f0",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1909e9f3-e173-4b90-88d2-787a99790747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "3ac69c1a-f777-42de-a67d-1583901555f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1909e9f3-e173-4b90-88d2-787a99790747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2303489e-f2be-491a-b643-f4cee48c105b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1909e9f3-e173-4b90-88d2-787a99790747",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "f7245bf2-8e2a-412a-8ff0-1c011e4125aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4e1fd915-793d-4c73-a2e4-07bf35881340",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7245bf2-8e2a-412a-8ff0-1c011e4125aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "431faaa1-5143-4cb0-9b70-102c59d0ae67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7245bf2-8e2a-412a-8ff0-1c011e4125aa",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "17535220-6c96-4807-946b-97309ff8e600",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "7bf1dbda-460a-423a-853a-cadc24da2441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17535220-6c96-4807-946b-97309ff8e600",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1810576-2bc4-4876-8240-ad9c12e89c27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17535220-6c96-4807-946b-97309ff8e600",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "727eee83-6eb0-49f8-84d2-b206eb33eb41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "841ef239-04a2-43f5-89ee-7d8ee79701fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "727eee83-6eb0-49f8-84d2-b206eb33eb41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61ba9de-ca94-4ade-9e3e-1c05a67dacc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "727eee83-6eb0-49f8-84d2-b206eb33eb41",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "decc0351-a099-47a8-b728-cb9f2c134d50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "765378aa-7d4a-4f1e-b9e7-10950629ecf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "decc0351-a099-47a8-b728-cb9f2c134d50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0562482a-6318-40ae-930e-b2bae0e139c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "decc0351-a099-47a8-b728-cb9f2c134d50",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "c535b3df-2b5b-4b43-aed6-35c22bd0a4c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "6e634794-ce81-4026-a3b1-52a0f3baa510",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c535b3df-2b5b-4b43-aed6-35c22bd0a4c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b3d4ad3-498f-473d-a224-09e97a88cbe9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c535b3df-2b5b-4b43-aed6-35c22bd0a4c2",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "21869ab9-5133-482e-8968-46340ca39da9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "aec37fa2-fbc9-49ec-bedb-e6db043f8468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21869ab9-5133-482e-8968-46340ca39da9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d44ce71e-135f-4817-b12f-334886b07004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21869ab9-5133-482e-8968-46340ca39da9",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "64f85cab-be6d-4401-8b77-6070bee936b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "c4f401c1-d21f-432e-aadd-10470be57448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64f85cab-be6d-4401-8b77-6070bee936b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3ddb074-fce6-473d-a07b-18e82b4b9a90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64f85cab-be6d-4401-8b77-6070bee936b1",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "3a07662b-81b6-4487-a23b-eb1a4eac3f7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "41aad040-2239-4e9a-af31-1e3341b61c99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a07662b-81b6-4487-a23b-eb1a4eac3f7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63f3dba0-aef8-4635-bd71-0b09e9d4e0a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a07662b-81b6-4487-a23b-eb1a4eac3f7d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "9d71e606-6571-444e-bd79-120bf5ef0580",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "33afbf64-33e0-4484-bbd5-8a81b9bb770c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d71e606-6571-444e-bd79-120bf5ef0580",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a2d0133-6b57-4186-a0a1-b39801a99b9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d71e606-6571-444e-bd79-120bf5ef0580",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "f9dcd3ca-1da3-4def-acdb-2d50abb3aeeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "6e4db0de-32a9-4684-8ff9-695c50eee6a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9dcd3ca-1da3-4def-acdb-2d50abb3aeeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27eea4ec-0406-4d00-900c-b2235882ee8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9dcd3ca-1da3-4def-acdb-2d50abb3aeeb",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "e843f5af-2797-4d19-921a-442cf115dc79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2ccdcd19-8b8a-4586-8e79-e44a26dc3841",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e843f5af-2797-4d19-921a-442cf115dc79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "987eb861-294f-45f2-999d-a814b49a054d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e843f5af-2797-4d19-921a-442cf115dc79",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "aa71bd0a-0dc3-4a4a-b7cb-096223bb4459",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "35fea147-e4d4-47b8-b631-a1c795150921",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa71bd0a-0dc3-4a4a-b7cb-096223bb4459",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57af2710-bae4-4026-9fd8-b14fab3723bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa71bd0a-0dc3-4a4a-b7cb-096223bb4459",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "775d28be-e5a1-4211-aeac-f9286f6a08f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "b915efd8-24b7-4673-977e-5d631b398cab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "775d28be-e5a1-4211-aeac-f9286f6a08f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8538b3f6-c1a4-47fd-b960-d1840a491fc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "775d28be-e5a1-4211-aeac-f9286f6a08f2",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "538d1f46-3508-4a4f-9c50-92a6ca7a2037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "0687770f-2a17-4730-bc97-a3794fe43c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "538d1f46-3508-4a4f-9c50-92a6ca7a2037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c7fcdc-9b1d-4d4e-8ec2-5c27dc041028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "538d1f46-3508-4a4f-9c50-92a6ca7a2037",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "676776a7-e751-467f-9e15-b752d0458ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2a1221f4-c760-495c-9bdc-721c80941cba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "676776a7-e751-467f-9e15-b752d0458ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7920549e-0cd5-4238-bc4a-8c7708042442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "676776a7-e751-467f-9e15-b752d0458ce3",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "4423e874-34c6-45be-bc33-df115e45117b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1dffc81f-ccd0-40df-a0df-58ff6cc7c491",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4423e874-34c6-45be-bc33-df115e45117b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a965f32f-3eef-402f-8fba-2e547125f576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4423e874-34c6-45be-bc33-df115e45117b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "9bf05af6-8a27-471c-a63d-4b768f03875b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "ae4fa0b2-8c87-4dd2-ace9-077e1293b336",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bf05af6-8a27-471c-a63d-4b768f03875b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bff89f3-09b9-402e-8d14-3296053caceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bf05af6-8a27-471c-a63d-4b768f03875b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "7a8e789b-053a-42eb-9daf-260a4d1d2ffd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "f56ea6d3-7b4d-4a7f-9883-78f102257b6a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a8e789b-053a-42eb-9daf-260a4d1d2ffd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "944659fc-90e5-472a-8065-16da828bc9b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a8e789b-053a-42eb-9daf-260a4d1d2ffd",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "0ce4706f-7fa8-478a-84e6-836b82b5e771",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "21bbf6ac-89e7-418e-be98-658b134114c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ce4706f-7fa8-478a-84e6-836b82b5e771",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "771bab47-4af5-48b9-a8c7-ea2d02b1d4f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ce4706f-7fa8-478a-84e6-836b82b5e771",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "b48e6475-a42e-4697-a9fa-3908ed4982aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9e3e6dc5-eaad-4b3c-b9ab-b674df946140",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48e6475-a42e-4697-a9fa-3908ed4982aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49cdb8fe-61f8-4088-ba22-d0a5a3fdc301",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48e6475-a42e-4697-a9fa-3908ed4982aa",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "c6382950-93f0-4b6a-bf7d-f8d04a6dbb14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "6028ae9b-fe7e-4714-953b-98337e48d0b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6382950-93f0-4b6a-bf7d-f8d04a6dbb14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4b1a6c7-acff-4fc5-82b8-5a0f430bcc1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6382950-93f0-4b6a-bf7d-f8d04a6dbb14",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "46ed07a8-55bb-463b-beca-3d0dc2666879",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "119a2dbd-f3ea-450a-a782-b8f0945c39cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ed07a8-55bb-463b-beca-3d0dc2666879",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0af64a1-6ac8-435c-b2ce-52abb069ab8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ed07a8-55bb-463b-beca-3d0dc2666879",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "0eecca58-9d3c-4a8d-8c61-fc283f67f76d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "a641b495-2735-4954-96a3-c2c1e3a35c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0eecca58-9d3c-4a8d-8c61-fc283f67f76d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b01167b-f5c5-4a12-af5e-cfd3b1ae2d16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0eecca58-9d3c-4a8d-8c61-fc283f67f76d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "0c8ad700-f03c-4c94-8be0-529656b416a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "94947e1e-00ec-46be-8d48-2f6d5580d548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8ad700-f03c-4c94-8be0-529656b416a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2deda4c-0001-4f8a-9dbb-f63d4a04d2b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8ad700-f03c-4c94-8be0-529656b416a5",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "5fc97405-95ba-4ac4-a768-1236499601e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "25ce75cf-e58c-4029-8f4f-b030c167b96d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5fc97405-95ba-4ac4-a768-1236499601e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8ddd4b4-6a5b-42d8-a0e9-c6513844c9ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5fc97405-95ba-4ac4-a768-1236499601e8",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "f7a153d3-c534-43b4-940d-368f8a88413b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "afdb8c8d-976e-438e-90b6-71e9a0e6f2ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7a153d3-c534-43b4-940d-368f8a88413b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3833921-2377-4d3b-948c-4774cc418c82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7a153d3-c534-43b4-940d-368f8a88413b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "5b2d952e-4ef8-4d27-a0e1-ac0936e034e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9d0d079f-e6b1-44a1-a8dc-224351fa9da5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b2d952e-4ef8-4d27-a0e1-ac0936e034e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a58b466-d53e-4e53-a375-5ce417e3560b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b2d952e-4ef8-4d27-a0e1-ac0936e034e8",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "3b94afd4-3134-463c-b785-bd175eef7f1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2a8ea4bc-746a-40f2-ae9c-30845f8bf123",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b94afd4-3134-463c-b785-bd175eef7f1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b44f9b-2001-47a6-b65f-0cfb40a561ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b94afd4-3134-463c-b785-bd175eef7f1c",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "893bef73-867c-4682-9d63-3d2d39164f9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "334451b7-f1a1-4d0f-bfad-55020c91eef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "893bef73-867c-4682-9d63-3d2d39164f9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03ff5b5e-ab50-4dc4-b5ec-57773d33fdce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "893bef73-867c-4682-9d63-3d2d39164f9f",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "66056a87-2571-449b-a34e-de701b8075b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4d80c0ac-600c-4498-968c-6c079d253b76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66056a87-2571-449b-a34e-de701b8075b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5de79f88-a7e8-4ee8-b1bd-e9918d42947b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66056a87-2571-449b-a34e-de701b8075b4",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "29a05f01-94d5-4a64-8ce1-12ba88996cf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "210f5e4f-aeb1-4c63-8a41-0492f819877e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a05f01-94d5-4a64-8ce1-12ba88996cf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0818274b-f89b-4086-a49a-6fe6e1e58468",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a05f01-94d5-4a64-8ce1-12ba88996cf8",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "599470de-b9c9-4455-b3e1-d91bfa1cd027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4552506c-5274-4a0d-8ead-eb18ffa557e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "599470de-b9c9-4455-b3e1-d91bfa1cd027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a60b052-7b9a-46d0-b77b-b78e9b891566",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "599470de-b9c9-4455-b3e1-d91bfa1cd027",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "de221db9-9352-4fca-8872-5350c9a9a84e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "5a143ecd-4faa-4b73-ba32-d2f57443d3e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de221db9-9352-4fca-8872-5350c9a9a84e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "776aa697-bdd8-4777-98cb-bd61d1aa0911",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de221db9-9352-4fca-8872-5350c9a9a84e",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "e7084fb5-ca79-4303-bed8-4aa5cf5632ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "031c81b0-6daf-4e05-8f17-afcda8d567a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7084fb5-ca79-4303-bed8-4aa5cf5632ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4e7bded-219b-4e08-a80f-b804bfce697e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7084fb5-ca79-4303-bed8-4aa5cf5632ba",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "5132dc85-aab9-4198-aa26-bbbea045abc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9b0b695a-dd7c-41db-840b-6e1ed1568e9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5132dc85-aab9-4198-aa26-bbbea045abc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96b78237-a686-4f7a-b9c3-e47e528c7508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5132dc85-aab9-4198-aa26-bbbea045abc7",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "ea016162-f3f2-4e92-8539-37750b2a1a6d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2b007117-71c3-419e-8345-e5fa5a73ea0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea016162-f3f2-4e92-8539-37750b2a1a6d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42d6619a-7a7d-4458-b42b-80341d7250f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea016162-f3f2-4e92-8539-37750b2a1a6d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "8ec5f662-8cf0-4d9a-8f0e-a6d5faf3841a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "8b4baa0e-7c6b-4101-b7a9-29c227f394ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ec5f662-8cf0-4d9a-8f0e-a6d5faf3841a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e794d0b6-667c-4c61-99f5-777ed20cc93d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ec5f662-8cf0-4d9a-8f0e-a6d5faf3841a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "63af9b18-e43b-40a9-8d99-cd8e46237c25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "78f729b9-763f-4ef9-9d8e-84860456143e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63af9b18-e43b-40a9-8d99-cd8e46237c25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a21866e3-8c72-4e79-990c-ee11f71cec43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63af9b18-e43b-40a9-8d99-cd8e46237c25",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "ba844c75-779e-4667-a275-0a7fa977fa8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "7813c8ad-d606-47b3-b2ce-b7bda42da36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba844c75-779e-4667-a275-0a7fa977fa8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13140e2-d5d4-4b9e-9714-fd454311e54b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba844c75-779e-4667-a275-0a7fa977fa8e",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "cd963379-77de-4ad5-8fb4-790f90594863",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1433e447-5e0a-4ed6-bd69-2d4fb5dbed21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd963379-77de-4ad5-8fb4-790f90594863",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "612934a0-41d5-451a-a4c0-49d264a502c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd963379-77de-4ad5-8fb4-790f90594863",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "d9751f6d-9c73-4764-ac26-984af4ee43b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "158b9f0f-d635-4763-9c18-28c942f67847",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9751f6d-9c73-4764-ac26-984af4ee43b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1061dc2-78ea-4b78-9ff4-f6ffea565d42",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9751f6d-9c73-4764-ac26-984af4ee43b2",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "21bf80c6-ed68-46d5-8f58-12b7188f0af2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "2f090bf2-2f5b-4369-a722-12babec18706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21bf80c6-ed68-46d5-8f58-12b7188f0af2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff4286f0-ebd6-4649-9181-6b24241b5211",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21bf80c6-ed68-46d5-8f58-12b7188f0af2",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "dc4b01e4-dc4b-4dfc-aa71-d161479fb67c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1d5b368e-c3b4-4ae9-9aaf-fe87c8fbf725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc4b01e4-dc4b-4dfc-aa71-d161479fb67c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeae6996-0b58-4814-8564-d278d3fe7676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc4b01e4-dc4b-4dfc-aa71-d161479fb67c",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "ed4fb108-5a0c-4e1a-ae71-54430006898b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "cf07f92a-e38e-499e-af20-3590c6cea97a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed4fb108-5a0c-4e1a-ae71-54430006898b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b82e2962-2104-4afb-aeef-a3d49ad4c70c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed4fb108-5a0c-4e1a-ae71-54430006898b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "bb196f75-b251-4189-a85c-41a767eb369b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "377c622d-8b3a-4df1-9cb9-8611022fd0de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb196f75-b251-4189-a85c-41a767eb369b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "434f8a2b-6ad4-43ed-bee3-62b75cca5f40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb196f75-b251-4189-a85c-41a767eb369b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "e62f0c6e-f641-4036-9e26-f186e51d18b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "90e21381-dc0c-4a2b-a21c-672b8ce04efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e62f0c6e-f641-4036-9e26-f186e51d18b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8642619-7bd0-4e7f-953e-8946857b2c3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e62f0c6e-f641-4036-9e26-f186e51d18b9",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "96622374-d7e4-4e90-8347-4fa634690b2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "48d940c3-2d3a-4167-b63d-7aee745ceda1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96622374-d7e4-4e90-8347-4fa634690b2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d207c234-fa5a-4c89-b469-811768e36141",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96622374-d7e4-4e90-8347-4fa634690b2e",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1a3c86c1-e848-479e-88d4-a1251ab8d1ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "65948428-9607-43be-bb72-2d2572f295d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3c86c1-e848-479e-88d4-a1251ab8d1ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d1b3f05-db64-47cf-9bdf-4463abcbca8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3c86c1-e848-479e-88d4-a1251ab8d1ab",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "77ec6862-98c3-4e95-aee4-78563e571cec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "eef5bd2c-5013-41d5-af57-48e5983baf9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ec6862-98c3-4e95-aee4-78563e571cec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbfb5ae9-af13-4a4e-9679-96e4eae1e8e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ec6862-98c3-4e95-aee4-78563e571cec",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "471e0fee-ec01-402b-a7c2-94f8c2e088d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "6e4d4c7e-78f6-40ba-a100-b637dad9ba1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471e0fee-ec01-402b-a7c2-94f8c2e088d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f7b78e-58a8-4c05-a107-7da5665a6ecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471e0fee-ec01-402b-a7c2-94f8c2e088d1",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "46465f01-63cd-4326-85d0-c643d3709d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "a5dacd28-b7f1-4ab1-b5da-31bd0c3a948e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46465f01-63cd-4326-85d0-c643d3709d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffd29918-ef15-4b78-a9e4-973217cf1f27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46465f01-63cd-4326-85d0-c643d3709d30",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "8c43c9fd-7a00-4e0c-8edf-6ac3ad0f3d15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "7243aaa3-5a3a-4cbc-b2df-a57552cb33a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c43c9fd-7a00-4e0c-8edf-6ac3ad0f3d15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5a3b76f-5283-4ffc-a031-ed91c80f3741",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c43c9fd-7a00-4e0c-8edf-6ac3ad0f3d15",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "313c1af7-5d09-4838-b94a-f36392dd5903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4efa8fdd-a146-4e4f-be4d-dde4d3fadd39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "313c1af7-5d09-4838-b94a-f36392dd5903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de60a802-b97e-4dd0-aa39-b7e94eb4d282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "313c1af7-5d09-4838-b94a-f36392dd5903",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "0014ddf1-01d0-476a-9a90-c8d67d6dc39a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "7b0065be-e76c-4a5d-897a-f387c381d372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0014ddf1-01d0-476a-9a90-c8d67d6dc39a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aece3680-b318-4a69-a545-a19d41f3cd39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0014ddf1-01d0-476a-9a90-c8d67d6dc39a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "d7a9207c-6e31-4754-9563-faa381282bf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "ec2fa1ca-4cee-4e92-9ddc-abec3917f525",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a9207c-6e31-4754-9563-faa381282bf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c5a3d17-09ce-4db2-af47-46fad2d71a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a9207c-6e31-4754-9563-faa381282bf3",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "733a1437-f564-4c6b-abc1-07e819d97bbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "034d9b6e-1c2a-4bda-8c8f-840606f4535f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "733a1437-f564-4c6b-abc1-07e819d97bbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74a9fe90-d4e8-4c85-bd8e-9bdaa0567f6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "733a1437-f564-4c6b-abc1-07e819d97bbc",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "20bfa368-ee4a-4ab1-a8d0-0079c606219f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "57b6e9e0-552e-46a0-b27a-eac0e102184f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20bfa368-ee4a-4ab1-a8d0-0079c606219f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1feb91d9-5a29-4861-979c-ce85632fd87c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20bfa368-ee4a-4ab1-a8d0-0079c606219f",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "cccce54d-45e7-4623-a8b5-656d7d525dc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "f3811d61-317c-4aa9-9450-98a5ee1b02c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cccce54d-45e7-4623-a8b5-656d7d525dc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a252bb70-18ac-49de-b29e-0e7c32c9abbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cccce54d-45e7-4623-a8b5-656d7d525dc7",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "106c2a60-92e7-440f-8650-1754f3de06ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "788be082-8067-4f78-ac17-482f8b63ab69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "106c2a60-92e7-440f-8650-1754f3de06ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cd9614e-be97-4f51-a639-4f0d43dbec99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "106c2a60-92e7-440f-8650-1754f3de06ec",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1a779cfa-4057-41f6-8aae-8198ab2ed626",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "8534f62b-416e-4d95-a6a7-8904b6334dc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a779cfa-4057-41f6-8aae-8198ab2ed626",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c2a71d2-e31f-49ab-a8b6-b5bb6dea299a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a779cfa-4057-41f6-8aae-8198ab2ed626",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "cf4bc7b1-a71b-4136-b682-2074b780a1cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "4ecba80b-0fc7-4983-8484-0047c2225b5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf4bc7b1-a71b-4136-b682-2074b780a1cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5573d080-ad5e-4568-afbb-bd34653249f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf4bc7b1-a71b-4136-b682-2074b780a1cf",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "8eda0962-51c3-4c04-93f9-d8895fc1ba9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "38255309-1ad4-4cb2-a6fc-254308226f6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eda0962-51c3-4c04-93f9-d8895fc1ba9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e57103-ccd9-431d-9d33-647cd1cd5266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eda0962-51c3-4c04-93f9-d8895fc1ba9b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "4a32548f-ff5f-4a3f-a2db-ff313f72eb2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1756713e-6480-4750-9429-7cf984ccfc96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a32548f-ff5f-4a3f-a2db-ff313f72eb2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1387164e-e210-4155-bd8d-4acb5c0b1270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a32548f-ff5f-4a3f-a2db-ff313f72eb2e",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "87f9dfed-f11a-483a-b0b6-79a70261da31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "5494bff1-bd42-401d-9c8d-85cc858f5bd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87f9dfed-f11a-483a-b0b6-79a70261da31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a133f8c-6b1f-4087-88b3-a8dc7ae690ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87f9dfed-f11a-483a-b0b6-79a70261da31",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "f755f5c9-526a-48f1-a403-b0d39e39810a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "bbfb50a4-835b-44fd-b422-c4c78ce8865d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f755f5c9-526a-48f1-a403-b0d39e39810a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69240489-e26a-40c3-aeed-984132476c87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f755f5c9-526a-48f1-a403-b0d39e39810a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "1f4d0562-1790-4db4-891f-ddffdc56873d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "1a83d614-19a7-4330-a59c-2ff7a1285529",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f4d0562-1790-4db4-891f-ddffdc56873d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f151351d-0922-4ea4-b149-260b7bda8a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f4d0562-1790-4db4-891f-ddffdc56873d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "e1144eed-8c27-4ec1-b97a-36b462195b0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "adf2cecf-c4bd-417e-894f-8c0e2664b1a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1144eed-8c27-4ec1-b97a-36b462195b0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "373c154d-212c-4f0b-9ecf-82c8a10c4ad2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1144eed-8c27-4ec1-b97a-36b462195b0a",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "b53b9fd5-fd76-48e5-8a33-9d7a650d485d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "7e359e43-714d-44ef-8266-6ff5bf572af5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b53b9fd5-fd76-48e5-8a33-9d7a650d485d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb1ca02-3042-4799-8ff3-576cd5b51d3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b53b9fd5-fd76-48e5-8a33-9d7a650d485d",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "3056467f-3bd8-478a-b657-fe583470177f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "d3f43695-1024-4ab3-8d81-f60620f0c64d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3056467f-3bd8-478a-b657-fe583470177f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2445a34-86ae-4f5a-a1e4-847df8c52b66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3056467f-3bd8-478a-b657-fe583470177f",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "c581d4aa-a571-4fc9-a8e9-cc88ef26606f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "639f9029-f486-45ec-a0cd-2766ae8df1c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c581d4aa-a571-4fc9-a8e9-cc88ef26606f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8e6244e-367e-4420-8c18-66ff83a93657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c581d4aa-a571-4fc9-a8e9-cc88ef26606f",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "2b79e28c-9097-4db2-88aa-1eaf14fdc63c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "dd785b11-77f9-4b64-bc11-3196c4941167",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b79e28c-9097-4db2-88aa-1eaf14fdc63c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "371f6e72-109a-40ed-be04-4b71beb52339",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b79e28c-9097-4db2-88aa-1eaf14fdc63c",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "4d7f4a59-81c8-4bb7-899f-038e1bd889d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "b11246db-5bed-43a0-883a-37378f4135a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d7f4a59-81c8-4bb7-899f-038e1bd889d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c001b5a7-cb75-464b-ba16-ec4cd6a78b82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d7f4a59-81c8-4bb7-899f-038e1bd889d1",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "d8968538-6c1f-4007-9220-470485b320a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "9922a25d-4044-465d-bc15-de27f2b0296b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8968538-6c1f-4007-9220-470485b320a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee64725-660b-4b21-b170-535ceb776760",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8968538-6c1f-4007-9220-470485b320a2",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        },
        {
            "id": "4f56431f-c663-4599-b2a0-fbafdf138c6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "compositeImage": {
                "id": "6a88d1f9-2812-4297-a07c-245941f2f13e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f56431f-c663-4599-b2a0-fbafdf138c6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2110c590-16c3-4e8e-a8ad-785900bf8bfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f56431f-c663-4599-b2a0-fbafdf138c6b",
                    "LayerId": "1428ac61-9598-4a6f-9539-241cf13aedb6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 340,
    "layers": [
        {
            "id": "1428ac61-9598-4a6f-9539-241cf13aedb6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "512796e0-c52f-42a3-907f-c3f8016637d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 340,
    "xorig": 170,
    "yorig": 170
}