{
    "id": "c9ba89b6-9ba5-4ef3-a07b-7620662452f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90a1d6e0-8c1f-4eac-b8c3-16c9e749a206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9ba89b6-9ba5-4ef3-a07b-7620662452f4",
            "compositeImage": {
                "id": "911a7a07-eb84-4300-ab0e-d669ba1cf018",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90a1d6e0-8c1f-4eac-b8c3-16c9e749a206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9049728-67f9-440f-9dfe-3acb76fe9307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90a1d6e0-8c1f-4eac-b8c3-16c9e749a206",
                    "LayerId": "72be98a8-7656-43e8-9d4b-58d5f3c3f5f3"
                }
            ]
        },
        {
            "id": "3378e5d7-d7df-4a7e-8f14-56fe42e2ebb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9ba89b6-9ba5-4ef3-a07b-7620662452f4",
            "compositeImage": {
                "id": "a97705b4-19f8-4c71-988b-9f9351017f04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3378e5d7-d7df-4a7e-8f14-56fe42e2ebb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbfc9246-725f-48b6-a6ed-421594768b6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3378e5d7-d7df-4a7e-8f14-56fe42e2ebb0",
                    "LayerId": "72be98a8-7656-43e8-9d4b-58d5f3c3f5f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "72be98a8-7656-43e8-9d4b-58d5f3c3f5f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9ba89b6-9ba5-4ef3-a07b-7620662452f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}