{
    "id": "d1540df5-c5e8-4450-a9b3-fd566e6cf863",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lion_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 5,
    "bbox_right": 139,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db5fe431-acae-4c34-b9ba-c516c201c8b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1540df5-c5e8-4450-a9b3-fd566e6cf863",
            "compositeImage": {
                "id": "05bc100c-10d4-4d26-b0a2-f4f1957b0905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db5fe431-acae-4c34-b9ba-c516c201c8b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b71cb09-d5fe-439f-8c5b-a1f6f0bbdc61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5fe431-acae-4c34-b9ba-c516c201c8b3",
                    "LayerId": "82ff2f95-7d4d-4d5e-ad97-0cfa64ccfebf"
                }
            ]
        },
        {
            "id": "ac65f63a-1045-4a8e-bb5e-2a37de2a0326",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1540df5-c5e8-4450-a9b3-fd566e6cf863",
            "compositeImage": {
                "id": "bc8f0e52-c8ef-4bc7-837d-7081b0851674",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac65f63a-1045-4a8e-bb5e-2a37de2a0326",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e417fee-b135-487e-9d96-cd3267ffb356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac65f63a-1045-4a8e-bb5e-2a37de2a0326",
                    "LayerId": "82ff2f95-7d4d-4d5e-ad97-0cfa64ccfebf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "82ff2f95-7d4d-4d5e-ad97-0cfa64ccfebf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1540df5-c5e8-4450-a9b3-fd566e6cf863",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}