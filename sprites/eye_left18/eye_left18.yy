{
    "id": "b8eb572f-112b-448d-b210-461e01f8bed7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8287cf70-7cad-48c4-96fd-68ff09c87811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8eb572f-112b-448d-b210-461e01f8bed7",
            "compositeImage": {
                "id": "ad015409-58e0-4cc0-945f-04bbfca7ff40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8287cf70-7cad-48c4-96fd-68ff09c87811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9c4c842-1072-43d5-bb12-fa3c4c9f74a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8287cf70-7cad-48c4-96fd-68ff09c87811",
                    "LayerId": "389e0388-1605-4105-9330-4c7d4cc9f66a"
                }
            ]
        },
        {
            "id": "696d2430-e2f8-4551-a00f-e74af2a49e44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8eb572f-112b-448d-b210-461e01f8bed7",
            "compositeImage": {
                "id": "b92f6ed4-75cd-482b-895f-3ae281dbef60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "696d2430-e2f8-4551-a00f-e74af2a49e44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf60dffc-c554-4684-80e4-52eb76630ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "696d2430-e2f8-4551-a00f-e74af2a49e44",
                    "LayerId": "389e0388-1605-4105-9330-4c7d4cc9f66a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "389e0388-1605-4105-9330-4c7d4cc9f66a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8eb572f-112b-448d-b210-461e01f8bed7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}