{
    "id": "9606e87c-ad77-41ee-8211-d9b04e912ca3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin3_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36998c14-67c1-475c-a4f7-911ee31edd94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9606e87c-ad77-41ee-8211-d9b04e912ca3",
            "compositeImage": {
                "id": "0aa5e4a2-b697-40ae-a4d6-9d7f14e1fe0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36998c14-67c1-475c-a4f7-911ee31edd94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc99422-60f2-442b-a145-431ca100fdc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36998c14-67c1-475c-a4f7-911ee31edd94",
                    "LayerId": "97351809-c6ce-45d4-b0b5-6c9d95481781"
                }
            ]
        },
        {
            "id": "c28f3180-58e7-468b-844b-b318657b9263",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9606e87c-ad77-41ee-8211-d9b04e912ca3",
            "compositeImage": {
                "id": "b95793b5-d77c-4801-8f0e-854ecdb9e36e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28f3180-58e7-468b-844b-b318657b9263",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09c9e90d-c36a-4ff9-a73c-cfe10d04acd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28f3180-58e7-468b-844b-b318657b9263",
                    "LayerId": "97351809-c6ce-45d4-b0b5-6c9d95481781"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "97351809-c6ce-45d4-b0b5-6c9d95481781",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9606e87c-ad77-41ee-8211-d9b04e912ca3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}