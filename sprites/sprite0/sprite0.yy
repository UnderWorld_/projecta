{
    "id": "dd1072a7-8be9-48ec-98e7-3d79895b6868",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d8558f6-2af5-49ee-9fbd-bdd586e29796",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dd1072a7-8be9-48ec-98e7-3d79895b6868",
            "compositeImage": {
                "id": "22639749-4953-4cf0-abcf-2a17cb85c278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d8558f6-2af5-49ee-9fbd-bdd586e29796",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fd03344-3b9f-4a53-829e-f1ba88a253bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d8558f6-2af5-49ee-9fbd-bdd586e29796",
                    "LayerId": "d62cffaa-091c-4133-a203-06aa5bfbac4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d62cffaa-091c-4133-a203-06aa5bfbac4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dd1072a7-8be9-48ec-98e7-3d79895b6868",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}