{
    "id": "655f5b75-f2d9-4c64-b16d-35f17f1418a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flower3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0599a008-f61b-4e1a-a670-c3371a00eed3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "655f5b75-f2d9-4c64-b16d-35f17f1418a8",
            "compositeImage": {
                "id": "0c3fd523-10eb-45bf-a6b9-7e60cdd906b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0599a008-f61b-4e1a-a670-c3371a00eed3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "391e667a-d473-4788-9505-b2d2c8fe2520",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0599a008-f61b-4e1a-a670-c3371a00eed3",
                    "LayerId": "de3abd6b-3109-4c90-9eb4-881c42b278f8"
                }
            ]
        },
        {
            "id": "6c7ff33c-59c9-408a-80e2-609a891aced0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "655f5b75-f2d9-4c64-b16d-35f17f1418a8",
            "compositeImage": {
                "id": "9aed5571-b540-4946-a909-e293c3fba766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c7ff33c-59c9-408a-80e2-609a891aced0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9177a6e4-d2bc-4c7e-9e79-8c48970428ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c7ff33c-59c9-408a-80e2-609a891aced0",
                    "LayerId": "de3abd6b-3109-4c90-9eb4-881c42b278f8"
                }
            ]
        },
        {
            "id": "6f48da96-2a47-4257-af8d-5519e9b5a284",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "655f5b75-f2d9-4c64-b16d-35f17f1418a8",
            "compositeImage": {
                "id": "1d087124-91fd-44ed-adb3-bc715214144e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f48da96-2a47-4257-af8d-5519e9b5a284",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "941d2609-3b75-44df-947a-fd0ed31820ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f48da96-2a47-4257-af8d-5519e9b5a284",
                    "LayerId": "de3abd6b-3109-4c90-9eb4-881c42b278f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "de3abd6b-3109-4c90-9eb4-881c42b278f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "655f5b75-f2d9-4c64-b16d-35f17f1418a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0.01,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 14,
    "yorig": 31
}