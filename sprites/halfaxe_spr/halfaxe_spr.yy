{
    "id": "48210e7e-e9f7-41e3-a476-d469aac467b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "halfaxe_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 127,
    "bbox_right": 185,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99f17289-206b-4e20-b523-16eed0b9dba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48210e7e-e9f7-41e3-a476-d469aac467b3",
            "compositeImage": {
                "id": "d48365dd-bef0-4c53-bbd1-149d6be84d63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99f17289-206b-4e20-b523-16eed0b9dba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f585246-b3c3-4ee5-b38c-2587a2c856d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99f17289-206b-4e20-b523-16eed0b9dba1",
                    "LayerId": "c0bc144f-a3a3-4ba5-a0ff-5fc1c8afb38f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "c0bc144f-a3a3-4ba5-a0ff-5fc1c8afb38f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48210e7e-e9f7-41e3-a476-d469aac467b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 3
}