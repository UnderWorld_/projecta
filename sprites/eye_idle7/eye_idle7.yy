{
    "id": "c545f1de-daf2-49f5-9437-ae4bd56d08ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e96f5dfa-ed9d-459c-a38a-e1651d4df098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c545f1de-daf2-49f5-9437-ae4bd56d08ce",
            "compositeImage": {
                "id": "73cdfe73-536b-405e-8b59-40064cd0fd91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e96f5dfa-ed9d-459c-a38a-e1651d4df098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7749681e-3d3e-4818-b2ca-862ece233067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e96f5dfa-ed9d-459c-a38a-e1651d4df098",
                    "LayerId": "f32afe2c-f93a-4238-bedb-bd355af35bf3"
                }
            ]
        },
        {
            "id": "7fa3fc7c-b78e-49e1-8854-43c43ef6eaef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c545f1de-daf2-49f5-9437-ae4bd56d08ce",
            "compositeImage": {
                "id": "ea3597b2-aa90-4ed7-90fd-cbacf438d2ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa3fc7c-b78e-49e1-8854-43c43ef6eaef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b34eef7-3bee-4d4a-b612-edd1745c9ac6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa3fc7c-b78e-49e1-8854-43c43ef6eaef",
                    "LayerId": "f32afe2c-f93a-4238-bedb-bd355af35bf3"
                }
            ]
        },
        {
            "id": "d07d3d57-5705-466b-aeeb-5f16cc983c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c545f1de-daf2-49f5-9437-ae4bd56d08ce",
            "compositeImage": {
                "id": "85c2b7e0-22a2-4102-a4cd-9b0a78f0cfba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d07d3d57-5705-466b-aeeb-5f16cc983c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "433959d4-d9ed-41e1-abc9-d4939ac769d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d07d3d57-5705-466b-aeeb-5f16cc983c08",
                    "LayerId": "f32afe2c-f93a-4238-bedb-bd355af35bf3"
                }
            ]
        },
        {
            "id": "c96c091a-0f43-461b-8d49-a1d11e01730f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c545f1de-daf2-49f5-9437-ae4bd56d08ce",
            "compositeImage": {
                "id": "30861e1e-eedb-4574-9a3d-666e2af0e49f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c96c091a-0f43-461b-8d49-a1d11e01730f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494666e2-a722-46f2-9acc-ed8e11146752",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c96c091a-0f43-461b-8d49-a1d11e01730f",
                    "LayerId": "f32afe2c-f93a-4238-bedb-bd355af35bf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f32afe2c-f93a-4238-bedb-bd355af35bf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c545f1de-daf2-49f5-9437-ae4bd56d08ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}