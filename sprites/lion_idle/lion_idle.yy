{
    "id": "05425cc9-73b1-4abf-ba60-6f254e447e58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lion_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 5,
    "bbox_right": 139,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a0eea77-01f8-4af3-8e99-d8295517447f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05425cc9-73b1-4abf-ba60-6f254e447e58",
            "compositeImage": {
                "id": "6df80465-9f52-4186-aa08-2d1c6663c655",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a0eea77-01f8-4af3-8e99-d8295517447f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268453e1-0356-496c-99f0-544c510933b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a0eea77-01f8-4af3-8e99-d8295517447f",
                    "LayerId": "0a6ac2b1-3a78-4917-91aa-9d0c3bf51e67"
                }
            ]
        },
        {
            "id": "be8850c9-b784-4789-a654-940a033aca17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05425cc9-73b1-4abf-ba60-6f254e447e58",
            "compositeImage": {
                "id": "f16b7e1b-fb57-4540-b687-7a1e47948114",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be8850c9-b784-4789-a654-940a033aca17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3574863a-aeb3-4017-9d21-945b7169ad8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be8850c9-b784-4789-a654-940a033aca17",
                    "LayerId": "0a6ac2b1-3a78-4917-91aa-9d0c3bf51e67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "0a6ac2b1-3a78-4917-91aa-9d0c3bf51e67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05425cc9-73b1-4abf-ba60-6f254e447e58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}