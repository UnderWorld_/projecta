{
    "id": "9fa55b60-91fe-485e-9f7d-0717f3acf1f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mushroom2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 48,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d849ca8-438c-403c-8015-af8e0401ca3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9fa55b60-91fe-485e-9f7d-0717f3acf1f7",
            "compositeImage": {
                "id": "d4407386-c57f-4351-9eee-267cd6a3ceb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d849ca8-438c-403c-8015-af8e0401ca3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20761c16-f436-4a10-821c-175d7b57b81e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d849ca8-438c-403c-8015-af8e0401ca3e",
                    "LayerId": "0574efc2-8ec3-4dd5-ab64-dc381b57b876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "0574efc2-8ec3-4dd5-ab64-dc381b57b876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9fa55b60-91fe-485e-9f7d-0717f3acf1f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 24,
    "yorig": 21
}