{
    "id": "0186f850-f5a4-4b77-b10e-e8590c77d0c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin4_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "385cf5d9-7b23-40d6-9a20-6498496fb4b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0186f850-f5a4-4b77-b10e-e8590c77d0c1",
            "compositeImage": {
                "id": "88e48dd4-d664-4082-af74-eae49e3c4057",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "385cf5d9-7b23-40d6-9a20-6498496fb4b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b21015b4-6762-491f-adcf-7ea338cc47ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "385cf5d9-7b23-40d6-9a20-6498496fb4b0",
                    "LayerId": "48453092-00b8-4a06-b968-9eb0a288df22"
                }
            ]
        },
        {
            "id": "0a7aa012-f58a-41d4-82e5-f11311fefcf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0186f850-f5a4-4b77-b10e-e8590c77d0c1",
            "compositeImage": {
                "id": "aded6602-5755-4b1a-970c-fa4c83200dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a7aa012-f58a-41d4-82e5-f11311fefcf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bbb2591-3724-48ff-ad13-5558c3204a1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a7aa012-f58a-41d4-82e5-f11311fefcf7",
                    "LayerId": "48453092-00b8-4a06-b968-9eb0a288df22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "48453092-00b8-4a06-b968-9eb0a288df22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0186f850-f5a4-4b77-b10e-e8590c77d0c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}