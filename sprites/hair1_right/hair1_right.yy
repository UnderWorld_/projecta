{
    "id": "2bc27fcd-7d2e-4528-a135-f7a5d72d671e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair1_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 9,
    "bbox_right": 79,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d88a162e-0bbf-432e-992c-c8133ad90e05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bc27fcd-7d2e-4528-a135-f7a5d72d671e",
            "compositeImage": {
                "id": "6bae8e1d-0fad-47eb-b6d2-e15aafad2b2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d88a162e-0bbf-432e-992c-c8133ad90e05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548e8a92-3eea-42b0-abe1-8e0f80b42da5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d88a162e-0bbf-432e-992c-c8133ad90e05",
                    "LayerId": "fa289789-0ff0-419c-ae2d-c4099cede73c"
                }
            ]
        },
        {
            "id": "89d6a6d6-9cd8-4b0e-a8e4-53af9329e0c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2bc27fcd-7d2e-4528-a135-f7a5d72d671e",
            "compositeImage": {
                "id": "c0360dcf-9d56-4bb6-ab48-61125b9e2a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d6a6d6-9cd8-4b0e-a8e4-53af9329e0c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be1228d2-808d-4974-9a6b-ea08409bf25b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d6a6d6-9cd8-4b0e-a8e4-53af9329e0c0",
                    "LayerId": "fa289789-0ff0-419c-ae2d-c4099cede73c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "fa289789-0ff0-419c-ae2d-c4099cede73c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2bc27fcd-7d2e-4528-a135-f7a5d72d671e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}