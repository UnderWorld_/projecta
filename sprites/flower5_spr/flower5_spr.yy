{
    "id": "746d6927-a1c4-42db-be7f-7fcd39a7cf92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flower5_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a0b02be-4472-4f04-a63f-8286e2a1eca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "746d6927-a1c4-42db-be7f-7fcd39a7cf92",
            "compositeImage": {
                "id": "43829fe1-fd5c-42ec-9295-6b5243dfcfb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a0b02be-4472-4f04-a63f-8286e2a1eca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3104f61c-7380-4725-831f-95d881ab14de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a0b02be-4472-4f04-a63f-8286e2a1eca2",
                    "LayerId": "cac922a5-8232-45bd-a900-99c97994c8df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 19,
    "layers": [
        {
            "id": "cac922a5-8232-45bd-a900-99c97994c8df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "746d6927-a1c4-42db-be7f-7fcd39a7cf92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 0,
    "yorig": 0
}