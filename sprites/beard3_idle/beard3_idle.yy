{
    "id": "f03d7079-0ce2-4843-8310-c69990967a67",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beard3_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 5,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "601d3298-0f96-4ec3-8e2f-9d73a6403f65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03d7079-0ce2-4843-8310-c69990967a67",
            "compositeImage": {
                "id": "a6df3d65-33ca-4ca9-9661-b685ac73b62e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "601d3298-0f96-4ec3-8e2f-9d73a6403f65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37bce4f0-0372-48be-b39b-613c099812e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "601d3298-0f96-4ec3-8e2f-9d73a6403f65",
                    "LayerId": "62662c12-81ef-451a-9bc6-4ead20cb6c51"
                }
            ]
        },
        {
            "id": "57353a40-e01b-48d3-964d-0eea42ef2382",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03d7079-0ce2-4843-8310-c69990967a67",
            "compositeImage": {
                "id": "9b8a4937-08a4-48fc-9d5f-5ca95f01f2de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57353a40-e01b-48d3-964d-0eea42ef2382",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8fc5fe8-7c5c-422f-82ca-b7ed4188202e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57353a40-e01b-48d3-964d-0eea42ef2382",
                    "LayerId": "62662c12-81ef-451a-9bc6-4ead20cb6c51"
                }
            ]
        },
        {
            "id": "86fb567d-0b20-434a-aad5-91c18b4be14e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03d7079-0ce2-4843-8310-c69990967a67",
            "compositeImage": {
                "id": "fb0b12e2-4141-45ac-9963-f042bd2ec306",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86fb567d-0b20-434a-aad5-91c18b4be14e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "953494f4-e804-4a51-bac5-2d059a258c9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86fb567d-0b20-434a-aad5-91c18b4be14e",
                    "LayerId": "62662c12-81ef-451a-9bc6-4ead20cb6c51"
                }
            ]
        },
        {
            "id": "982ef233-9f6d-465b-9ecc-c7a51dde3868",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f03d7079-0ce2-4843-8310-c69990967a67",
            "compositeImage": {
                "id": "80a43ca2-1d14-4de2-a64d-c319db1e8fc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "982ef233-9f6d-465b-9ecc-c7a51dde3868",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e431b15-a943-43de-9379-e95ab46b49bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "982ef233-9f6d-465b-9ecc-c7a51dde3868",
                    "LayerId": "62662c12-81ef-451a-9bc6-4ead20cb6c51"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "62662c12-81ef-451a-9bc6-4ead20cb6c51",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f03d7079-0ce2-4843-8310-c69990967a67",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}