{
    "id": "84f49617-6c12-4afe-8b5a-346d9b130dec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_walk_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 14,
    "bbox_right": 61,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0c58e00-6aad-4be6-808e-67d52cbdefe0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "f1287223-ce08-4205-bb4a-48b5530c1c38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0c58e00-6aad-4be6-808e-67d52cbdefe0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3b469e0-8b90-4c01-b5a7-641a20ae9057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0c58e00-6aad-4be6-808e-67d52cbdefe0",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "26a17943-987a-42a2-81d1-6a5a7726713e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "a734c302-a086-412b-8163-33e1730e2cf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a17943-987a-42a2-81d1-6a5a7726713e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "454aea41-8d6b-4ba5-91b6-85c046c7cb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a17943-987a-42a2-81d1-6a5a7726713e",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "dd705c4b-31d9-4839-99e7-534860837dd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "1b9dfc57-26e1-4bb3-9eae-7325519050e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd705c4b-31d9-4839-99e7-534860837dd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d9587e-ffe2-45da-9489-e9cacc9f7ca8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd705c4b-31d9-4839-99e7-534860837dd5",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "2934f5f9-924b-4aa1-a084-e8075c367206",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "7f5a0939-1f40-473b-aa32-5e6a56baf97b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2934f5f9-924b-4aa1-a084-e8075c367206",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a07ec39-1043-4cba-9912-9ba55e70698a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2934f5f9-924b-4aa1-a084-e8075c367206",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "cb28e1cb-272c-4280-b2b8-d80d99b23613",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "5a1f5e2f-0c53-4d92-9968-ced6235a9d37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb28e1cb-272c-4280-b2b8-d80d99b23613",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5822530a-4c49-4ec7-850f-e284ca265678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb28e1cb-272c-4280-b2b8-d80d99b23613",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "3f330d9c-564e-4353-bed5-0893cf723dea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "69c7cda6-3948-459d-8174-4b872978a771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f330d9c-564e-4353-bed5-0893cf723dea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b32cb41-cd26-41d4-9a06-3d4b8fbf058b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f330d9c-564e-4353-bed5-0893cf723dea",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "a5994cfa-2ecb-49d4-a3ae-dc7091228d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "ccdeef2d-7843-4c6b-8518-f9c49f37ca3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5994cfa-2ecb-49d4-a3ae-dc7091228d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a72306-381f-4f7a-a813-acb5ec8ccf67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5994cfa-2ecb-49d4-a3ae-dc7091228d43",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "0a355bf2-be15-470f-82f8-0952e4dc2332",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "5f6a7a2c-5242-47e3-a4d6-ad5c93569a7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a355bf2-be15-470f-82f8-0952e4dc2332",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0615f87-dea2-4693-88b1-4907dba1883c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a355bf2-be15-470f-82f8-0952e4dc2332",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "2f435136-ed7c-459b-9621-a60556c783fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "9aa9dac4-c5eb-4fe5-884c-a35dcda5429c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f435136-ed7c-459b-9621-a60556c783fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a01e6c-aaff-4cd4-b68b-eaf01d82acd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f435136-ed7c-459b-9621-a60556c783fb",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        },
        {
            "id": "5c9f68d0-8318-412d-9d2a-5b75ed1e4b9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "compositeImage": {
                "id": "e5c45685-86bc-45c8-a286-307168fc8fe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c9f68d0-8318-412d-9d2a-5b75ed1e4b9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c317c4de-241a-469d-84b9-1dc2a6b78cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c9f68d0-8318-412d-9d2a-5b75ed1e4b9d",
                    "LayerId": "807cca07-e8cb-41b0-a071-c1ff15edf566"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "807cca07-e8cb-41b0-a071-c1ff15edf566",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84f49617-6c12-4afe-8b5a-346d9b130dec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 64
}