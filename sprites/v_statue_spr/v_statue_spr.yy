{
    "id": "6d7dccaf-899d-44db-9b9d-9681cc276783",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_statue_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e562e564-15aa-42ab-9381-612297d62cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "8a65df98-ab40-428a-a03e-42bc09b0027c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e562e564-15aa-42ab-9381-612297d62cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "576b1e40-a42d-4e86-b51d-919b07289434",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e562e564-15aa-42ab-9381-612297d62cc7",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "2cd9c3f5-d467-4d1b-97c9-4422e92d2107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "b306a776-9f7e-44a1-ba23-8d7289810702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2cd9c3f5-d467-4d1b-97c9-4422e92d2107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc09c9ed-369e-4fef-90d2-3afb928a8b46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2cd9c3f5-d467-4d1b-97c9-4422e92d2107",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "29a07511-ecc3-49d7-8e10-9bb58c4d2ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "e16d2320-2eb9-4795-b62d-9765845a4111",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29a07511-ecc3-49d7-8e10-9bb58c4d2ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d4fadb7-5574-4ccf-9850-0d93ae8f94eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29a07511-ecc3-49d7-8e10-9bb58c4d2ff7",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "d79d1df6-f80a-4d10-816a-fc2c2dd0c71e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "f768720e-63f5-4d1c-9eac-0e9448a37a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d79d1df6-f80a-4d10-816a-fc2c2dd0c71e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9730350-fe02-4c78-9f36-af6000a7b4e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d79d1df6-f80a-4d10-816a-fc2c2dd0c71e",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "d21a5d7a-74b2-4349-b577-3ecd00dd74ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "5415afb6-60ad-42cc-b710-cabf6583294c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d21a5d7a-74b2-4349-b577-3ecd00dd74ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dcc70c5-af3b-4bdf-b453-1b4ab3f7cc94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d21a5d7a-74b2-4349-b577-3ecd00dd74ab",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "3f89f97b-ce0a-4413-9d7a-0340b8fe60cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "a494f292-462d-4d2e-8459-cfa997a25436",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f89f97b-ce0a-4413-9d7a-0340b8fe60cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de2925eb-ee41-4cd2-a696-c6b41a15362a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f89f97b-ce0a-4413-9d7a-0340b8fe60cf",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "e5e8885d-009a-4dc7-b42a-0b88779127e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "ba0dc0a4-810c-4445-9f01-58ca6ec58bca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e8885d-009a-4dc7-b42a-0b88779127e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fdc280f-e241-4eea-a2f5-09d709cc90ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e8885d-009a-4dc7-b42a-0b88779127e2",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "9e24e149-2937-4b81-a3d8-8ff434317495",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "472e4b0b-41e2-4d11-9ce3-f0db9e7bdcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e24e149-2937-4b81-a3d8-8ff434317495",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58450ec6-e524-443c-b614-e6a20c62d1b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e24e149-2937-4b81-a3d8-8ff434317495",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "a0bb74f0-72fa-4e58-9189-700b4f7a67c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "1bd2a2a7-9eb2-4341-ac61-eae5ae14ce07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bb74f0-72fa-4e58-9189-700b4f7a67c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1795c7d0-d4d7-448d-97d0-171ebccd26ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bb74f0-72fa-4e58-9189-700b4f7a67c3",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "886b0dc0-8d68-4345-9981-f1c0b6876585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "9369112e-df85-4723-bc90-276bd473defd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886b0dc0-8d68-4345-9981-f1c0b6876585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d9a9822-b5d1-4e72-86dd-005a926d2a7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886b0dc0-8d68-4345-9981-f1c0b6876585",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "14b02d15-85b6-458c-94c7-73494bfd7894",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "49566af6-814c-4412-a81f-18d094340f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14b02d15-85b6-458c-94c7-73494bfd7894",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f6585d8-1bbc-400e-a0ae-8a1b07d6e977",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14b02d15-85b6-458c-94c7-73494bfd7894",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "cdc4cb88-fa23-4b0e-b58d-4719cf6a8b5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "96e9ab67-423e-4ae9-bb15-aeb77a65da6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdc4cb88-fa23-4b0e-b58d-4719cf6a8b5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9efa5d4a-0aae-4334-a89a-6f3f9d699744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdc4cb88-fa23-4b0e-b58d-4719cf6a8b5d",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "f3206e9f-0b62-4de5-bfc9-fb1a3ddb468d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "d370200c-6950-4685-b2dc-4c8f16d58bb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3206e9f-0b62-4de5-bfc9-fb1a3ddb468d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6d5fa7a-4d80-4ae9-b16e-8f1f35974890",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3206e9f-0b62-4de5-bfc9-fb1a3ddb468d",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "e8ff6fe8-532d-445a-a4dd-4bae696b3ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "bc2aba55-3ca7-4fb7-ae67-cedfbdeff8b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8ff6fe8-532d-445a-a4dd-4bae696b3ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58832bd5-72a3-44c8-8f9b-ddf352b3a102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8ff6fe8-532d-445a-a4dd-4bae696b3ee6",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "6c669d0f-b1e5-4e23-a1d4-4366d2eca95e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "ca0345f1-cebe-402e-afd7-b01128de1042",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c669d0f-b1e5-4e23-a1d4-4366d2eca95e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0b82785-789e-47b0-8087-924791a5378b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c669d0f-b1e5-4e23-a1d4-4366d2eca95e",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "a127fee0-c513-43ad-9caf-99c0e15b627e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "2d0bc4d9-1436-41fe-8381-72d20c6a486e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a127fee0-c513-43ad-9caf-99c0e15b627e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa43bd4f-f30d-4820-baec-d48817e3eb23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a127fee0-c513-43ad-9caf-99c0e15b627e",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "27a441d8-5316-4d66-ba57-8241ab6b34ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "cd15fa7c-29ae-41e6-9f68-59063b6e3792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27a441d8-5316-4d66-ba57-8241ab6b34ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aff27c2-9a95-4db9-bb07-a7e749d20ca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27a441d8-5316-4d66-ba57-8241ab6b34ac",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "5b320f68-cfc7-4189-9eba-6946369d2bc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "a9350d7b-9777-4422-b8e9-c69530997e89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b320f68-cfc7-4189-9eba-6946369d2bc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be2cec14-07e6-4a0e-9e12-18d27d08ce97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b320f68-cfc7-4189-9eba-6946369d2bc4",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        },
        {
            "id": "1c594c92-02ca-4e67-92c7-46b2d1271621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "compositeImage": {
                "id": "ea66f700-aad2-43a9-a967-b5c0e27410e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c594c92-02ca-4e67-92c7-46b2d1271621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3df14ff-03a1-4a39-aa6a-f2b4773e046c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c594c92-02ca-4e67-92c7-46b2d1271621",
                    "LayerId": "a75e94ec-8945-4888-a99e-0054514de37d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a75e94ec-8945-4888-a99e-0054514de37d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}