{
    "id": "119cab8f-5a4b-421b-ab7a-d048abee424c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardleft5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 5,
    "bbox_right": 49,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ef99d04-1515-42d9-becd-1f283c442f5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "119cab8f-5a4b-421b-ab7a-d048abee424c",
            "compositeImage": {
                "id": "fa0d0fec-18f8-4aa0-b87e-457e53e4ebe4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ef99d04-1515-42d9-becd-1f283c442f5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57fc9a40-3811-4419-9edd-f997fd964d2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ef99d04-1515-42d9-becd-1f283c442f5a",
                    "LayerId": "3edbaf68-86cf-4f7c-8adc-6774b503924e"
                }
            ]
        },
        {
            "id": "9f8a1b24-739a-4a5f-bd9f-d514fd8c022a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "119cab8f-5a4b-421b-ab7a-d048abee424c",
            "compositeImage": {
                "id": "a9f89119-7bbb-41bc-967e-b5064a7f98fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f8a1b24-739a-4a5f-bd9f-d514fd8c022a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "db93cc19-1277-4eb9-8442-3e8f66e378ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f8a1b24-739a-4a5f-bd9f-d514fd8c022a",
                    "LayerId": "3edbaf68-86cf-4f7c-8adc-6774b503924e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "3edbaf68-86cf-4f7c-8adc-6774b503924e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "119cab8f-5a4b-421b-ab7a-d048abee424c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}