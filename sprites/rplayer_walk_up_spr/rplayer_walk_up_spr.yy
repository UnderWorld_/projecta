{
    "id": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_walk_up_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 121,
    "bbox_left": 14,
    "bbox_right": 61,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87cd4307-9baa-43b6-95b0-154ce005b985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "bf98b150-ffb9-4c39-970a-a294d8ae97f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87cd4307-9baa-43b6-95b0-154ce005b985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6223f15b-5151-415c-bd08-14069d3d0478",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87cd4307-9baa-43b6-95b0-154ce005b985",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "aeffa499-b26d-4829-a21a-366caa21e202",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "098a8663-9c95-43f7-9b74-652bb51fbe6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeffa499-b26d-4829-a21a-366caa21e202",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47697c41-ac6c-4ce2-9c38-4a7d152c3990",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeffa499-b26d-4829-a21a-366caa21e202",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "58f9bf50-44cd-442c-8e0d-44b5b9810a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "067de21e-b25f-4f30-aa8e-3f35faadaa29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58f9bf50-44cd-442c-8e0d-44b5b9810a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e31a8e51-8bc9-4b7b-8d0e-f66b55a50ab3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58f9bf50-44cd-442c-8e0d-44b5b9810a3a",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "bbe9619b-6ed2-48a6-84f0-5c0070d416f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "a7bd8c2b-3945-4410-92a4-f0faf7f78538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe9619b-6ed2-48a6-84f0-5c0070d416f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "507fb081-af47-473a-8146-b81290378179",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe9619b-6ed2-48a6-84f0-5c0070d416f3",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "c3279cde-5b3c-4472-be0b-f52acbb917b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "d5d83241-3b1e-4154-844f-7482c874df63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3279cde-5b3c-4472-be0b-f52acbb917b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bddd5d04-fa9a-44b8-9437-8bc4e0c114b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3279cde-5b3c-4472-be0b-f52acbb917b8",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "ac8c21da-bd35-4ded-8fdf-dbe164e1a1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "571bc4bf-9601-4ae6-b1b6-654ee6cc983a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8c21da-bd35-4ded-8fdf-dbe164e1a1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f2fa1e5-bb35-46f3-9c4e-8d2dfcf89df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8c21da-bd35-4ded-8fdf-dbe164e1a1c0",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "046a975b-6be9-47ae-b6cf-edf07f57fe71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "9241198a-bba1-4f9e-aa07-d353e4cc633d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "046a975b-6be9-47ae-b6cf-edf07f57fe71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2603170-b3ba-449d-bb11-3356d5333c24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "046a975b-6be9-47ae-b6cf-edf07f57fe71",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "9401b17b-b0d9-483c-b927-fcbe88a833e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "9887522d-3bc3-405a-aa4d-d5bc355d8c59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9401b17b-b0d9-483c-b927-fcbe88a833e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aca7e8e-bc06-4657-a71f-39128fc2ace1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9401b17b-b0d9-483c-b927-fcbe88a833e2",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "0f033ebd-3b0d-4ec8-b636-35d268a28279",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "1e1276fa-3595-4745-9efc-1288b861cede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f033ebd-3b0d-4ec8-b636-35d268a28279",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e18c3f2-0e6b-4dfe-a01b-5891c69e6b2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f033ebd-3b0d-4ec8-b636-35d268a28279",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        },
        {
            "id": "e2b43f16-7f5b-44c1-9af9-0b8f15e15244",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "compositeImage": {
                "id": "eaea26cf-5bf8-44d1-b2f2-771454a7b143",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2b43f16-7f5b-44c1-9af9-0b8f15e15244",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85e0dff8-4273-4c28-8fdd-3323e0641efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2b43f16-7f5b-44c1-9af9-0b8f15e15244",
                    "LayerId": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "41e2c225-8d8f-4dd9-ade6-53e1dea98dd7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6ba4f1f3-8eaa-49ee-9cae-981caaa0991d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 64
}