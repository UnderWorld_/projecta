{
    "id": "1f138ccf-626c-4371-b474-b02eb72b60a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle13",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25b34fa2-5299-4b16-89a6-32cf2ae928bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f138ccf-626c-4371-b474-b02eb72b60a7",
            "compositeImage": {
                "id": "d3e4f488-e1ed-4fc5-9129-d1f370b447d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25b34fa2-5299-4b16-89a6-32cf2ae928bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12e13398-559a-4bbd-acce-b42478024499",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25b34fa2-5299-4b16-89a6-32cf2ae928bd",
                    "LayerId": "d42c480d-b6cc-4f3d-9fe8-734786dae6f8"
                }
            ]
        },
        {
            "id": "81a3746d-5282-4682-b8ba-7f70f025c3c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f138ccf-626c-4371-b474-b02eb72b60a7",
            "compositeImage": {
                "id": "fe95129e-1bf6-46a3-a49f-b421ffa76c51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81a3746d-5282-4682-b8ba-7f70f025c3c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e852af84-5df0-4fa9-a627-e0c168074a16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81a3746d-5282-4682-b8ba-7f70f025c3c0",
                    "LayerId": "d42c480d-b6cc-4f3d-9fe8-734786dae6f8"
                }
            ]
        },
        {
            "id": "522efbab-39b4-4eda-8eb4-fd5bb6910d22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f138ccf-626c-4371-b474-b02eb72b60a7",
            "compositeImage": {
                "id": "fd141c62-8e90-4f95-bd26-eb3270b29e6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "522efbab-39b4-4eda-8eb4-fd5bb6910d22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3a38ed1-503e-4e80-97fc-43465a24f7bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "522efbab-39b4-4eda-8eb4-fd5bb6910d22",
                    "LayerId": "d42c480d-b6cc-4f3d-9fe8-734786dae6f8"
                }
            ]
        },
        {
            "id": "0966cc71-5b1d-4c45-b8fc-890c78fc9582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f138ccf-626c-4371-b474-b02eb72b60a7",
            "compositeImage": {
                "id": "5f5f6f41-a5d4-4354-9650-d0aae3d8e043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0966cc71-5b1d-4c45-b8fc-890c78fc9582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a9239d5-6068-45ee-b9ad-e282e8def6e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0966cc71-5b1d-4c45-b8fc-890c78fc9582",
                    "LayerId": "d42c480d-b6cc-4f3d-9fe8-734786dae6f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "d42c480d-b6cc-4f3d-9fe8-734786dae6f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f138ccf-626c-4371-b474-b02eb72b60a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}