{
    "id": "c62a4aa6-b13c-4c90-8825-ae6d45fb6621",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite70",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa4810b5-36e9-4143-93b3-4ca9e229dbfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c62a4aa6-b13c-4c90-8825-ae6d45fb6621",
            "compositeImage": {
                "id": "0a20cdf2-41b8-4ce9-b850-5ae07cf9efc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa4810b5-36e9-4143-93b3-4ca9e229dbfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3157a84-0273-4e9d-ad44-b8eb05a9631c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa4810b5-36e9-4143-93b3-4ca9e229dbfc",
                    "LayerId": "6fcfde8f-7468-46c6-8449-0963d540ddea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "6fcfde8f-7468-46c6-8449-0963d540ddea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c62a4aa6-b13c-4c90-8825-ae6d45fb6621",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}