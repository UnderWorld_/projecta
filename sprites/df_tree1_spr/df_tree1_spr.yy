{
    "id": "ec186e70-266e-4cd6-b69f-2ee39fcd6924",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "df_tree1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 938,
    "bbox_left": 158,
    "bbox_right": 223,
    "bbox_top": 690,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05675b53-beeb-49ab-83c7-cb51a4eade1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec186e70-266e-4cd6-b69f-2ee39fcd6924",
            "compositeImage": {
                "id": "c8e7e891-ddd0-4600-af5f-86770c2b6daf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05675b53-beeb-49ab-83c7-cb51a4eade1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b12db994-e60c-4ad0-b5a1-c13319c7ec96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05675b53-beeb-49ab-83c7-cb51a4eade1d",
                    "LayerId": "c882afb0-9682-46f3-a539-197bec81fa06"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 954,
    "layers": [
        {
            "id": "c882afb0-9682-46f3-a539-197bec81fa06",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec186e70-266e-4cd6-b69f-2ee39fcd6924",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 378,
    "xorig": 189,
    "yorig": 927
}