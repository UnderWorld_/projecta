{
    "id": "90b340fb-2db3-461b-a3fa-f088a2ba7f18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird2_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 35,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26ef6e98-1eee-486e-9f15-9cd57cbbb2a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90b340fb-2db3-461b-a3fa-f088a2ba7f18",
            "compositeImage": {
                "id": "bbacb9e4-4379-4ed9-85df-b607b6ced12e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ef6e98-1eee-486e-9f15-9cd57cbbb2a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab732686-a911-429f-8fd3-6529ebd67c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ef6e98-1eee-486e-9f15-9cd57cbbb2a1",
                    "LayerId": "d8bc7b51-79f2-43c6-825d-e6243704e8ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d8bc7b51-79f2-43c6-825d-e6243704e8ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90b340fb-2db3-461b-a3fa-f088a2ba7f18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 0,
    "yorig": 0
}