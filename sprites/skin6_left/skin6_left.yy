{
    "id": "a01035f2-6a1e-4217-a8c3-6364f6a2d6cf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin6_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f544affa-d270-4bb8-9993-de7d0cb06908",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a01035f2-6a1e-4217-a8c3-6364f6a2d6cf",
            "compositeImage": {
                "id": "5b82e6b8-0bad-4a8a-b2ef-f25e687e8f72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f544affa-d270-4bb8-9993-de7d0cb06908",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91f675b2-7ca3-4320-bb7f-5729b4b2ca25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f544affa-d270-4bb8-9993-de7d0cb06908",
                    "LayerId": "ef3fbce8-77f6-4261-85fa-722113c9adba"
                }
            ]
        },
        {
            "id": "e6b0448b-9d46-4ca9-97bd-df4129071781",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a01035f2-6a1e-4217-a8c3-6364f6a2d6cf",
            "compositeImage": {
                "id": "811fc866-e703-4265-ae66-6d15ae28c61b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b0448b-9d46-4ca9-97bd-df4129071781",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fc3ff0e-39b2-4796-8c54-ad537da8138f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b0448b-9d46-4ca9-97bd-df4129071781",
                    "LayerId": "ef3fbce8-77f6-4261-85fa-722113c9adba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "ef3fbce8-77f6-4261-85fa-722113c9adba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a01035f2-6a1e-4217-a8c3-6364f6a2d6cf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}