{
    "id": "a11fc98f-279d-447c-af4a-98afead61d01",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin6_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dd2510fe-bffc-4fa1-a140-332b2f8bb90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a11fc98f-279d-447c-af4a-98afead61d01",
            "compositeImage": {
                "id": "0da3b552-bea8-4af7-91e8-52aa24f5a9a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2510fe-bffc-4fa1-a140-332b2f8bb90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f571c06b-aed1-49f9-ab91-a8bc39a9ad96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2510fe-bffc-4fa1-a140-332b2f8bb90c",
                    "LayerId": "6691ecc8-6649-464e-9950-ca4bcc76f959"
                }
            ]
        },
        {
            "id": "910ab428-2e08-4394-88e5-d61dabd8306a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a11fc98f-279d-447c-af4a-98afead61d01",
            "compositeImage": {
                "id": "b5bef22e-530c-4258-b5c5-68fd03741f43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910ab428-2e08-4394-88e5-d61dabd8306a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3c977e-eb3e-453f-a85c-b73987ed2786",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910ab428-2e08-4394-88e5-d61dabd8306a",
                    "LayerId": "6691ecc8-6649-464e-9950-ca4bcc76f959"
                }
            ]
        },
        {
            "id": "86a11abc-5de2-4ba6-abd8-31aadbb43c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a11fc98f-279d-447c-af4a-98afead61d01",
            "compositeImage": {
                "id": "29fe2032-9280-4274-af7e-dd877c5da237",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86a11abc-5de2-4ba6-abd8-31aadbb43c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c459e46a-ae8e-4e2e-a038-bd23c8d37224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86a11abc-5de2-4ba6-abd8-31aadbb43c47",
                    "LayerId": "6691ecc8-6649-464e-9950-ca4bcc76f959"
                }
            ]
        },
        {
            "id": "9138a393-719f-4c6a-9f87-4aa9da4eddee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a11fc98f-279d-447c-af4a-98afead61d01",
            "compositeImage": {
                "id": "407600cc-5e96-4b93-ad9d-604ce2b6cbb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9138a393-719f-4c6a-9f87-4aa9da4eddee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "854be3fa-28e3-4bfa-8a30-60a5b78aaba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9138a393-719f-4c6a-9f87-4aa9da4eddee",
                    "LayerId": "6691ecc8-6649-464e-9950-ca4bcc76f959"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "6691ecc8-6649-464e-9950-ca4bcc76f959",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a11fc98f-279d-447c-af4a-98afead61d01",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}