{
    "id": "b6ace912-a6e4-4865-9207-8b2afd1153fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin2_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9159458-13c2-477a-8673-e0fa6bbeafa3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6ace912-a6e4-4865-9207-8b2afd1153fa",
            "compositeImage": {
                "id": "021a4a81-753e-405b-a910-739cc5c336ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9159458-13c2-477a-8673-e0fa6bbeafa3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8b81a6-d6c7-44da-a642-61c40d6ccbd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9159458-13c2-477a-8673-e0fa6bbeafa3",
                    "LayerId": "de81e190-7556-432c-b1d2-8c0e391a8027"
                }
            ]
        },
        {
            "id": "3a9c8659-2fc5-4887-8932-76f2a48927fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6ace912-a6e4-4865-9207-8b2afd1153fa",
            "compositeImage": {
                "id": "9ddadc1e-810a-4f3e-a3dd-62730f6b6533",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a9c8659-2fc5-4887-8932-76f2a48927fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca27428-51f1-4414-92fd-138e77420d83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a9c8659-2fc5-4887-8932-76f2a48927fb",
                    "LayerId": "de81e190-7556-432c-b1d2-8c0e391a8027"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "de81e190-7556-432c-b1d2-8c0e391a8027",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6ace912-a6e4-4865-9207-8b2afd1153fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}