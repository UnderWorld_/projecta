{
    "id": "6b53048a-2402-46cd-a859-7106ff8ac3c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair3_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 69,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1da01307-98bc-4f04-9314-e1f6ecf5cca9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b53048a-2402-46cd-a859-7106ff8ac3c3",
            "compositeImage": {
                "id": "d138d90e-b9d3-4da5-8249-45dc17dd11db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1da01307-98bc-4f04-9314-e1f6ecf5cca9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "814e1753-878f-4ab8-bfb9-157c78431500",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1da01307-98bc-4f04-9314-e1f6ecf5cca9",
                    "LayerId": "27d559c7-3142-440b-909b-317d317c67a4"
                }
            ]
        },
        {
            "id": "c46e6421-51f5-42ec-b244-49fbbff59dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b53048a-2402-46cd-a859-7106ff8ac3c3",
            "compositeImage": {
                "id": "c44979b0-db82-4cb1-bbce-0fff5c0e3ef4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c46e6421-51f5-42ec-b244-49fbbff59dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d37f144f-f82a-4fcb-b939-93caf779accf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c46e6421-51f5-42ec-b244-49fbbff59dc3",
                    "LayerId": "27d559c7-3142-440b-909b-317d317c67a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "27d559c7-3142-440b-909b-317d317c67a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b53048a-2402-46cd-a859-7106ff8ac3c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}