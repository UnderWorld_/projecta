{
    "id": "1a6e007c-186e-4caa-9835-d531d5af956e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardleft4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 5,
    "bbox_right": 49,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aedad9b2-7c97-4765-bcd5-b4edd928b320",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6e007c-186e-4caa-9835-d531d5af956e",
            "compositeImage": {
                "id": "7cf78026-efbd-443c-b39a-fb55cd1d1f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aedad9b2-7c97-4765-bcd5-b4edd928b320",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f675b47-effe-47b9-8d2b-f10a3b969a46",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aedad9b2-7c97-4765-bcd5-b4edd928b320",
                    "LayerId": "9d4e8a32-2732-4fc4-8831-f09b37925e9d"
                }
            ]
        },
        {
            "id": "928adc62-5101-4755-b653-9fa63390dc4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1a6e007c-186e-4caa-9835-d531d5af956e",
            "compositeImage": {
                "id": "0c22b0b0-572a-4743-839b-762696855448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "928adc62-5101-4755-b653-9fa63390dc4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acbd0b19-4aa5-4192-9eaa-253cdc9e2457",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "928adc62-5101-4755-b653-9fa63390dc4d",
                    "LayerId": "9d4e8a32-2732-4fc4-8831-f09b37925e9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "9d4e8a32-2732-4fc4-8831-f09b37925e9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1a6e007c-186e-4caa-9835-d531d5af956e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}