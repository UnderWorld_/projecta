{
    "id": "77dc3bb8-07e9-421a-9a5f-ac0e5de887fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dmg_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "307287d9-23e9-4b47-9a13-858839b07a9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "77dc3bb8-07e9-421a-9a5f-ac0e5de887fb",
            "compositeImage": {
                "id": "aac9486d-e5aa-4b26-9f9f-73ea3580962a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307287d9-23e9-4b47-9a13-858839b07a9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7967f70c-6ab8-4b1f-a28b-7dc783af87c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307287d9-23e9-4b47-9a13-858839b07a9f",
                    "LayerId": "61ffb5c1-10c6-4245-a0b9-0e9929271eea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "61ffb5c1-10c6-4245-a0b9-0e9929271eea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "77dc3bb8-07e9-421a-9a5f-ac0e5de887fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}