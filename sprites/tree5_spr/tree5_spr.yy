{
    "id": "0e490ef4-2413-4620-9441-4d86fa3538af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree5_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 3,
    "bbox_right": 34,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63ae47ef-37cb-4b81-8b2f-b9c5df455765",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e490ef4-2413-4620-9441-4d86fa3538af",
            "compositeImage": {
                "id": "3d8c294d-3bb5-4309-b551-eb6b1f784dd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63ae47ef-37cb-4b81-8b2f-b9c5df455765",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35db9a0-a136-4ed2-895b-e7360ee60452",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63ae47ef-37cb-4b81-8b2f-b9c5df455765",
                    "LayerId": "8ff49a19-8898-4375-8feb-03d0d09bb70e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "8ff49a19-8898-4375-8feb-03d0d09bb70e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e490ef4-2413-4620-9441-4d86fa3538af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 0,
    "yorig": 0
}