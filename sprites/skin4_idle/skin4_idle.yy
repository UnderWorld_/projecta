{
    "id": "e5471460-bcbf-49dc-90c5-bb9031c8fa0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin4_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3567523-fc62-4f69-824e-98c84edac14b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5471460-bcbf-49dc-90c5-bb9031c8fa0c",
            "compositeImage": {
                "id": "525fdaed-dbb3-4513-90ea-a2cf1ca0b11d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3567523-fc62-4f69-824e-98c84edac14b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffde1610-d51d-42e0-ad1f-f77e7cf57a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3567523-fc62-4f69-824e-98c84edac14b",
                    "LayerId": "bffda9af-0b8e-46b0-8e8e-fea8ee38fd47"
                }
            ]
        },
        {
            "id": "78126e93-1600-4cf0-a4c5-a2c798dd9aac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5471460-bcbf-49dc-90c5-bb9031c8fa0c",
            "compositeImage": {
                "id": "f19c08e2-4a87-403b-9bfa-25d4f588e66b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78126e93-1600-4cf0-a4c5-a2c798dd9aac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19e83260-1e5b-4794-a2d2-0d26a222d6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78126e93-1600-4cf0-a4c5-a2c798dd9aac",
                    "LayerId": "bffda9af-0b8e-46b0-8e8e-fea8ee38fd47"
                }
            ]
        },
        {
            "id": "e94bcca7-2e4e-48af-a1e1-00fc68272797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5471460-bcbf-49dc-90c5-bb9031c8fa0c",
            "compositeImage": {
                "id": "f815627d-e16d-43ab-b24a-daf7b561a10c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e94bcca7-2e4e-48af-a1e1-00fc68272797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8451a448-7cd7-4368-b208-85d09f36a732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e94bcca7-2e4e-48af-a1e1-00fc68272797",
                    "LayerId": "bffda9af-0b8e-46b0-8e8e-fea8ee38fd47"
                }
            ]
        },
        {
            "id": "07de407d-77a4-405c-b56d-91dfec1b838e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5471460-bcbf-49dc-90c5-bb9031c8fa0c",
            "compositeImage": {
                "id": "559c05ee-3afe-44cb-8a5d-bb1ed200dddb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07de407d-77a4-405c-b56d-91dfec1b838e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7199290-2625-475f-88bf-9270dd751310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07de407d-77a4-405c-b56d-91dfec1b838e",
                    "LayerId": "bffda9af-0b8e-46b0-8e8e-fea8ee38fd47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "bffda9af-0b8e-46b0-8e8e-fea8ee38fd47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5471460-bcbf-49dc-90c5-bb9031c8fa0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}