{
    "id": "5748ce85-726b-49a2-85dd-d14e7c352f62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "snake_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2fcb5269-bd44-4588-a62f-63b294f6b25f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ce85-726b-49a2-85dd-d14e7c352f62",
            "compositeImage": {
                "id": "142d3a96-0cd7-4b4c-bf04-610d82acd695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fcb5269-bd44-4588-a62f-63b294f6b25f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b082a3e4-f702-4cbd-ab53-0623799daee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fcb5269-bd44-4588-a62f-63b294f6b25f",
                    "LayerId": "ac50fb7a-291d-414b-9b53-1c9389e0e8b5"
                }
            ]
        },
        {
            "id": "b9d09149-2b02-4af4-9195-6a1fba726b92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5748ce85-726b-49a2-85dd-d14e7c352f62",
            "compositeImage": {
                "id": "e43a20a4-558e-4bdb-964a-4b16122a3267",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d09149-2b02-4af4-9195-6a1fba726b92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b32dd3c-b281-4816-8717-712288cbeca1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d09149-2b02-4af4-9195-6a1fba726b92",
                    "LayerId": "ac50fb7a-291d-414b-9b53-1c9389e0e8b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ac50fb7a-291d-414b-9b53-1c9389e0e8b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5748ce85-726b-49a2-85dd-d14e7c352f62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}