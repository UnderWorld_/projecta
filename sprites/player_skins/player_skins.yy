{
    "id": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_skins",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b65cd65e-fef0-430b-8ded-70dbe204cd83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "compositeImage": {
                "id": "f3936bef-5b6d-4309-9ffc-7ccb1a03acd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65cd65e-fef0-430b-8ded-70dbe204cd83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dae1982-92a4-404e-beb4-6de232ceb96c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65cd65e-fef0-430b-8ded-70dbe204cd83",
                    "LayerId": "4873377b-e7e0-47f0-a4a4-52b779104158"
                }
            ]
        },
        {
            "id": "6f896bdf-b920-490e-a0c1-f4f5f54b905c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "compositeImage": {
                "id": "7ea2f037-e6f7-47a2-97c4-230f0669cec7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f896bdf-b920-490e-a0c1-f4f5f54b905c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6217271a-f7eb-4297-965f-c2293422cd6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f896bdf-b920-490e-a0c1-f4f5f54b905c",
                    "LayerId": "4873377b-e7e0-47f0-a4a4-52b779104158"
                }
            ]
        },
        {
            "id": "349e5f64-8d7c-4f1f-b2a6-96abb7cbc554",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "compositeImage": {
                "id": "a41331c8-e0cf-4e8c-b257-ec57645f4746",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349e5f64-8d7c-4f1f-b2a6-96abb7cbc554",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e14ad0b-8e8d-4621-986c-f587ca47ab92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349e5f64-8d7c-4f1f-b2a6-96abb7cbc554",
                    "LayerId": "4873377b-e7e0-47f0-a4a4-52b779104158"
                }
            ]
        },
        {
            "id": "91ca9e89-4d6c-41f0-ac26-96a2b1c51c44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "compositeImage": {
                "id": "12e33f2a-f56d-4d09-ab43-7a44558cb7d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ca9e89-4d6c-41f0-ac26-96a2b1c51c44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3745176e-8563-44f5-b42e-da42ad6fc7ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ca9e89-4d6c-41f0-ac26-96a2b1c51c44",
                    "LayerId": "4873377b-e7e0-47f0-a4a4-52b779104158"
                }
            ]
        },
        {
            "id": "6b3e6338-9c7c-438c-80b8-816dba216e86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "compositeImage": {
                "id": "6a067c37-38dd-4bf2-a0ab-4cec34862691",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b3e6338-9c7c-438c-80b8-816dba216e86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83ec95cc-eb74-425d-937f-f6f7175ff4cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b3e6338-9c7c-438c-80b8-816dba216e86",
                    "LayerId": "4873377b-e7e0-47f0-a4a4-52b779104158"
                }
            ]
        },
        {
            "id": "76995674-d564-40df-a57d-a17f84fd42fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "compositeImage": {
                "id": "b53ada51-d2cc-42c3-9fb7-01939d990c84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76995674-d564-40df-a57d-a17f84fd42fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b60a284-17fa-44bc-a9e7-9644902f981c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76995674-d564-40df-a57d-a17f84fd42fd",
                    "LayerId": "4873377b-e7e0-47f0-a4a4-52b779104158"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4873377b-e7e0-47f0-a4a4-52b779104158",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}