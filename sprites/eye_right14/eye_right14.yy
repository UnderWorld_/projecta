{
    "id": "8f020902-cdd0-44e1-937f-815856381d23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "05d22496-07bc-4aca-ac11-3bb63404d243",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f020902-cdd0-44e1-937f-815856381d23",
            "compositeImage": {
                "id": "fbda1315-e6a7-4a4c-8cf7-667aa5385f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05d22496-07bc-4aca-ac11-3bb63404d243",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7473948a-5f6e-4773-9ac0-58c44ffc291a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05d22496-07bc-4aca-ac11-3bb63404d243",
                    "LayerId": "e0271ce4-73e4-46d9-9611-64c5371589d9"
                }
            ]
        },
        {
            "id": "43f6fd4b-b591-4736-ba91-5db74a10649c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8f020902-cdd0-44e1-937f-815856381d23",
            "compositeImage": {
                "id": "d650b3ee-9aa0-4e4b-bb4c-e18cbfbcc530",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f6fd4b-b591-4736-ba91-5db74a10649c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fa3a230-69ce-4bda-8242-9936db4965e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f6fd4b-b591-4736-ba91-5db74a10649c",
                    "LayerId": "e0271ce4-73e4-46d9-9611-64c5371589d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "e0271ce4-73e4-46d9-9611-64c5371589d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8f020902-cdd0-44e1-937f-815856381d23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}