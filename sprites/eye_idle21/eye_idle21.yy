{
    "id": "15cb3829-06af-4834-9d1e-85a1e2a24a33",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc9f45f0-752c-4500-a144-31ca582dc115",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15cb3829-06af-4834-9d1e-85a1e2a24a33",
            "compositeImage": {
                "id": "85302fdf-c86c-4127-8d53-de990a01348c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9f45f0-752c-4500-a144-31ca582dc115",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34206f3-16c8-4dda-a611-a628f09dfa90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9f45f0-752c-4500-a144-31ca582dc115",
                    "LayerId": "00f15a2f-074b-4b00-8de9-d0e2a76bd5c6"
                }
            ]
        },
        {
            "id": "10c7abfd-12aa-449c-ade1-76e2740c78d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15cb3829-06af-4834-9d1e-85a1e2a24a33",
            "compositeImage": {
                "id": "5eeede62-3d5c-49dc-a881-1efc96a73d95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10c7abfd-12aa-449c-ade1-76e2740c78d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d101d4f6-c485-4730-bed5-deb15f770b04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10c7abfd-12aa-449c-ade1-76e2740c78d0",
                    "LayerId": "00f15a2f-074b-4b00-8de9-d0e2a76bd5c6"
                }
            ]
        },
        {
            "id": "ff2efbaf-ed7f-4f55-9223-120b189ab442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15cb3829-06af-4834-9d1e-85a1e2a24a33",
            "compositeImage": {
                "id": "1dd86a96-ad91-4ac3-8eda-0512483ba735",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff2efbaf-ed7f-4f55-9223-120b189ab442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f572fb5-f557-4de3-bcc7-24c65bf17ace",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff2efbaf-ed7f-4f55-9223-120b189ab442",
                    "LayerId": "00f15a2f-074b-4b00-8de9-d0e2a76bd5c6"
                }
            ]
        },
        {
            "id": "b284499f-adad-45e2-8c0b-711b56c2a471",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15cb3829-06af-4834-9d1e-85a1e2a24a33",
            "compositeImage": {
                "id": "c345d931-b8ca-48d2-a270-43ef751d0ebe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b284499f-adad-45e2-8c0b-711b56c2a471",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6e62033-9ba1-43d3-a61f-625c51f2ffae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b284499f-adad-45e2-8c0b-711b56c2a471",
                    "LayerId": "00f15a2f-074b-4b00-8de9-d0e2a76bd5c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "00f15a2f-074b-4b00-8de9-d0e2a76bd5c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15cb3829-06af-4834-9d1e-85a1e2a24a33",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}