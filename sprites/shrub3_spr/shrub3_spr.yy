{
    "id": "f17b2d22-9432-4a69-ab02-980a762e4ad4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shrub3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9b489d3b-4c90-4d2a-b465-c978456109d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f17b2d22-9432-4a69-ab02-980a762e4ad4",
            "compositeImage": {
                "id": "651e5962-f75c-4071-9328-8122be5d6739",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b489d3b-4c90-4d2a-b465-c978456109d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4abedb7-fb63-4c3d-9c54-cf3a391b69a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b489d3b-4c90-4d2a-b465-c978456109d2",
                    "LayerId": "50e9e186-e50b-4190-bf17-0ecdf125de5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "50e9e186-e50b-4190-bf17-0ecdf125de5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f17b2d22-9432-4a69-ab02-980a762e4ad4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 21
}