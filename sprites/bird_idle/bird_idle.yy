{
    "id": "8ff450f9-554b-4cd2-8f38-84ac7020ce0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 22,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "560b291f-7adb-4396-9361-5e3d4ab5c022",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff450f9-554b-4cd2-8f38-84ac7020ce0d",
            "compositeImage": {
                "id": "1f327fb4-214b-41bb-aeda-d69d474dc02e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "560b291f-7adb-4396-9361-5e3d4ab5c022",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95c0af30-5f82-465c-8f9a-864fefb07002",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "560b291f-7adb-4396-9361-5e3d4ab5c022",
                    "LayerId": "00fde9e4-19db-40b9-892f-41c3c18c5d73"
                }
            ]
        },
        {
            "id": "f066d26f-c01b-41e5-83fa-6ef8a0eade31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8ff450f9-554b-4cd2-8f38-84ac7020ce0d",
            "compositeImage": {
                "id": "5a43f656-cbcb-4b59-a48d-b6e30bd51f27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f066d26f-c01b-41e5-83fa-6ef8a0eade31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eceb344-a2a5-42de-882a-55b29f49cbf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f066d26f-c01b-41e5-83fa-6ef8a0eade31",
                    "LayerId": "00fde9e4-19db-40b9-892f-41c3c18c5d73"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "00fde9e4-19db-40b9-892f-41c3c18c5d73",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8ff450f9-554b-4cd2-8f38-84ac7020ce0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}