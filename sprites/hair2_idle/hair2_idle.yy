{
    "id": "3194af22-b1ba-4a3c-aa09-a38d75ac1d3d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair2_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 10,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48d2e870-8fbc-4d8b-8962-c3a9cb8254aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3194af22-b1ba-4a3c-aa09-a38d75ac1d3d",
            "compositeImage": {
                "id": "b4c82243-9c16-45bc-80b4-da254038fb66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d2e870-8fbc-4d8b-8962-c3a9cb8254aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35246049-5d51-4dd2-941f-15f6af70d941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d2e870-8fbc-4d8b-8962-c3a9cb8254aa",
                    "LayerId": "3d401d9c-c0b1-43ba-9431-cea83d611285"
                }
            ]
        },
        {
            "id": "e1d413e5-e4ce-4130-a875-5594f0bc1f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3194af22-b1ba-4a3c-aa09-a38d75ac1d3d",
            "compositeImage": {
                "id": "1514d9af-19c7-4587-a5e8-21c8be419efa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1d413e5-e4ce-4130-a875-5594f0bc1f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10468b5d-2108-4c33-bdd2-a830b742e071",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1d413e5-e4ce-4130-a875-5594f0bc1f0d",
                    "LayerId": "3d401d9c-c0b1-43ba-9431-cea83d611285"
                }
            ]
        },
        {
            "id": "53e9b07f-d689-4e4f-be62-d30455f03f3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3194af22-b1ba-4a3c-aa09-a38d75ac1d3d",
            "compositeImage": {
                "id": "5100e65c-ebcd-41f0-85f5-d266d76cfb0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53e9b07f-d689-4e4f-be62-d30455f03f3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d53991a2-ed6b-4d20-b811-f9899ec4b0e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53e9b07f-d689-4e4f-be62-d30455f03f3b",
                    "LayerId": "3d401d9c-c0b1-43ba-9431-cea83d611285"
                }
            ]
        },
        {
            "id": "c4a09b02-f5d5-4830-b85b-8617ff3e0e32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3194af22-b1ba-4a3c-aa09-a38d75ac1d3d",
            "compositeImage": {
                "id": "a42e1f2d-01e8-4061-b763-a995d7af700a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a09b02-f5d5-4830-b85b-8617ff3e0e32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00509221-50d5-4509-b078-78ef12d2fab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a09b02-f5d5-4830-b85b-8617ff3e0e32",
                    "LayerId": "3d401d9c-c0b1-43ba-9431-cea83d611285"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "3d401d9c-c0b1-43ba-9431-cea83d611285",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3194af22-b1ba-4a3c-aa09-a38d75ac1d3d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}