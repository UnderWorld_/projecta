{
    "id": "0c6159ac-c5bc-497c-a1c5-8b94f0669f15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lemur_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "492c9e19-86d5-4357-8c97-18cc2712947d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c6159ac-c5bc-497c-a1c5-8b94f0669f15",
            "compositeImage": {
                "id": "dc63abc8-c605-4293-a893-4162b2a3358c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "492c9e19-86d5-4357-8c97-18cc2712947d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e32b531-60c2-4abb-824e-2f72b3931ddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "492c9e19-86d5-4357-8c97-18cc2712947d",
                    "LayerId": "fc2715b7-247f-428c-b8bd-6adaa496a06e"
                }
            ]
        },
        {
            "id": "f439446c-a564-4b4d-890c-8d7be64ebc60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c6159ac-c5bc-497c-a1c5-8b94f0669f15",
            "compositeImage": {
                "id": "a59dde11-1983-454c-ad31-c1347926ba29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f439446c-a564-4b4d-890c-8d7be64ebc60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab39411c-cd18-488a-acbf-b47e1864acbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f439446c-a564-4b4d-890c-8d7be64ebc60",
                    "LayerId": "fc2715b7-247f-428c-b8bd-6adaa496a06e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "fc2715b7-247f-428c-b8bd-6adaa496a06e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c6159ac-c5bc-497c-a1c5-8b94f0669f15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 36
}