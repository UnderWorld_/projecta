{
    "id": "90ff788f-6f05-4e46-a544-149cffb32d79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin2_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "520c38f9-e65d-47cc-8954-53048ed07528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90ff788f-6f05-4e46-a544-149cffb32d79",
            "compositeImage": {
                "id": "9537fc88-69ba-43f3-a5e7-e637a43e6c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "520c38f9-e65d-47cc-8954-53048ed07528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6f059e-f7ec-4156-84d9-ed4da97e0176",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "520c38f9-e65d-47cc-8954-53048ed07528",
                    "LayerId": "6351032b-ee54-483f-b7eb-9550ea785ae5"
                }
            ]
        },
        {
            "id": "68d9fa2c-79e9-45a1-8b72-96842d20d513",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90ff788f-6f05-4e46-a544-149cffb32d79",
            "compositeImage": {
                "id": "5bbbfbb3-83cb-46c8-82a0-782af6d34a4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68d9fa2c-79e9-45a1-8b72-96842d20d513",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d371fde-e7f7-44a3-8e93-03146abd39a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68d9fa2c-79e9-45a1-8b72-96842d20d513",
                    "LayerId": "6351032b-ee54-483f-b7eb-9550ea785ae5"
                }
            ]
        },
        {
            "id": "d2b1bb09-6de6-44e8-b71d-a61a4f9e63e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90ff788f-6f05-4e46-a544-149cffb32d79",
            "compositeImage": {
                "id": "9ce868da-88e6-4316-a0de-eebdf21172c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2b1bb09-6de6-44e8-b71d-a61a4f9e63e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "632dea5e-df97-463b-8bc2-f472b89e346e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2b1bb09-6de6-44e8-b71d-a61a4f9e63e3",
                    "LayerId": "6351032b-ee54-483f-b7eb-9550ea785ae5"
                }
            ]
        },
        {
            "id": "a160a300-c585-4d27-802b-b241def061d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90ff788f-6f05-4e46-a544-149cffb32d79",
            "compositeImage": {
                "id": "5ef77b84-558b-45b3-99fb-49020a7dd2d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a160a300-c585-4d27-802b-b241def061d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1c143f2-5964-49fb-b307-a9c542566d2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a160a300-c585-4d27-802b-b241def061d7",
                    "LayerId": "6351032b-ee54-483f-b7eb-9550ea785ae5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "6351032b-ee54-483f-b7eb-9550ea785ae5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90ff788f-6f05-4e46-a544-149cffb32d79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}