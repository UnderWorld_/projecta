{
    "id": "3c5da084-5f9c-4e5f-8c81-54314c816f35",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardleft3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 5,
    "bbox_right": 49,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fdc7a2a-49e3-444f-bf20-4f43b2c52113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c5da084-5f9c-4e5f-8c81-54314c816f35",
            "compositeImage": {
                "id": "53d2dde5-b73d-4f14-a749-a1e350ac4e22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fdc7a2a-49e3-444f-bf20-4f43b2c52113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a753811b-0ce1-4da6-9dc8-1564e58cc2fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fdc7a2a-49e3-444f-bf20-4f43b2c52113",
                    "LayerId": "90b1a715-bba3-495e-a533-1acbe5274925"
                }
            ]
        },
        {
            "id": "03f68113-23b9-4284-af94-1f538d8fab98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c5da084-5f9c-4e5f-8c81-54314c816f35",
            "compositeImage": {
                "id": "ceed2396-23f0-4ee9-989c-d936c3519fd3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f68113-23b9-4284-af94-1f538d8fab98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca89e7e3-6292-4174-ba5d-0d5f229f043e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f68113-23b9-4284-af94-1f538d8fab98",
                    "LayerId": "90b1a715-bba3-495e-a533-1acbe5274925"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "90b1a715-bba3-495e-a533-1acbe5274925",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c5da084-5f9c-4e5f-8c81-54314c816f35",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}