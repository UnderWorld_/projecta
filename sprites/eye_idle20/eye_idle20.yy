{
    "id": "a4e18187-84e2-4993-b25d-768d31fddf85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle20",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d3883c14-6616-4771-8071-1c95e1562e4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4e18187-84e2-4993-b25d-768d31fddf85",
            "compositeImage": {
                "id": "7783e9d7-5a91-48cd-abef-5af84aa5ac34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3883c14-6616-4771-8071-1c95e1562e4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26e92176-e6dd-4cc8-a331-04014eaec9cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3883c14-6616-4771-8071-1c95e1562e4b",
                    "LayerId": "374356df-39ab-4488-a482-2679aeb71682"
                }
            ]
        },
        {
            "id": "46ce7d22-7a50-406e-9065-e4c612157deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4e18187-84e2-4993-b25d-768d31fddf85",
            "compositeImage": {
                "id": "f51cf4c5-4859-4322-a536-ab61e70d0fb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46ce7d22-7a50-406e-9065-e4c612157deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1670cb25-78ec-4bce-b5a4-4b63611a9d97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46ce7d22-7a50-406e-9065-e4c612157deb",
                    "LayerId": "374356df-39ab-4488-a482-2679aeb71682"
                }
            ]
        },
        {
            "id": "9fb70c86-d517-4315-aeaf-49a3df93c7e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4e18187-84e2-4993-b25d-768d31fddf85",
            "compositeImage": {
                "id": "1b71315e-f219-4075-81a4-08a18806b747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fb70c86-d517-4315-aeaf-49a3df93c7e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f72c7c74-5601-46b5-b681-6b33dc4c7492",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fb70c86-d517-4315-aeaf-49a3df93c7e2",
                    "LayerId": "374356df-39ab-4488-a482-2679aeb71682"
                }
            ]
        },
        {
            "id": "aeaf0107-62c1-4902-a54a-51c62b695acf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4e18187-84e2-4993-b25d-768d31fddf85",
            "compositeImage": {
                "id": "7f90c113-18bc-4128-9dc3-1385a028943d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aeaf0107-62c1-4902-a54a-51c62b695acf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b9be86-7ca1-43e7-92db-2ba98e95f9f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aeaf0107-62c1-4902-a54a-51c62b695acf",
                    "LayerId": "374356df-39ab-4488-a482-2679aeb71682"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "374356df-39ab-4488-a482-2679aeb71682",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4e18187-84e2-4993-b25d-768d31fddf85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}