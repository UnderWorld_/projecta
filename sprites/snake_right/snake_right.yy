{
    "id": "86fdc4bb-1fc6-465c-9bdd-665400478233",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "snake_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba0d68cf-29dd-40b2-9aa4-29fa48de03af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86fdc4bb-1fc6-465c-9bdd-665400478233",
            "compositeImage": {
                "id": "b62e9c28-0483-4d80-a599-017f4a28f46a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0d68cf-29dd-40b2-9aa4-29fa48de03af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56c39acd-22ba-4c0a-a1b3-098fa744531d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0d68cf-29dd-40b2-9aa4-29fa48de03af",
                    "LayerId": "9e843a81-d836-46d5-b7ae-9c27e38a538e"
                }
            ]
        },
        {
            "id": "33c17b92-2db0-4ab3-b94d-40cbcb903621",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86fdc4bb-1fc6-465c-9bdd-665400478233",
            "compositeImage": {
                "id": "f1ea87ad-1fd3-4b51-ac26-c3d428ca4781",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c17b92-2db0-4ab3-b94d-40cbcb903621",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14f65f64-a932-48ef-9a68-c74667a86c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c17b92-2db0-4ab3-b94d-40cbcb903621",
                    "LayerId": "9e843a81-d836-46d5-b7ae-9c27e38a538e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e843a81-d836-46d5-b7ae-9c27e38a538e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86fdc4bb-1fc6-465c-9bdd-665400478233",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}