{
    "id": "9cdb2402-6f1d-44a3-bd58-a8b2c6e49f87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 460,
    "bbox_left": 225,
    "bbox_right": 308,
    "bbox_top": 329,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "91eaf12f-3dbe-49b6-9287-95395508d2f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9cdb2402-6f1d-44a3-bd58-a8b2c6e49f87",
            "compositeImage": {
                "id": "d729c052-52d4-4fa0-aeb1-54c2896853e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91eaf12f-3dbe-49b6-9287-95395508d2f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "247a03c7-558f-4a07-b0c4-b1c5fb250ef4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91eaf12f-3dbe-49b6-9287-95395508d2f0",
                    "LayerId": "f9b3fc9b-fd67-4066-96be-b268a6d158d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "f9b3fc9b-fd67-4066-96be-b268a6d158d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9cdb2402-6f1d-44a3-bd58-a8b2c6e49f87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 258,
    "yorig": 444
}