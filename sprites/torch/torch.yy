{
    "id": "61186641-9c33-4b06-859b-6f83ba2af575",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 28,
    "bbox_right": 33,
    "bbox_top": 32,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3b8a307-99c3-4bf3-9179-fa494be66c04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "61186641-9c33-4b06-859b-6f83ba2af575",
            "compositeImage": {
                "id": "ca29b729-ed87-47e5-b62f-03bee209093f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b8a307-99c3-4bf3-9179-fa494be66c04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "593a3c09-465b-4090-8669-369da6c8ba14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b8a307-99c3-4bf3-9179-fa494be66c04",
                    "LayerId": "992d93da-02cc-4485-a830-cfbf2f50975e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "992d93da-02cc-4485-a830-cfbf2f50975e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "61186641-9c33-4b06-859b-6f83ba2af575",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 34
}