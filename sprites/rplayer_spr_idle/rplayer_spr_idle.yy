{
    "id": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 44,
    "bbox_right": 82,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73cbc252-5c21-4063-87a6-2694797af24c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "7432b9e3-921b-4297-a132-8cc609cf764c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73cbc252-5c21-4063-87a6-2694797af24c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0edc1983-8294-4cde-9a7b-1d0a068d295a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73cbc252-5c21-4063-87a6-2694797af24c",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "d9bf8b67-df3c-429a-8dd6-6ef14cfebc7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "9a6fe48e-21ff-4437-b423-ee0279f169d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9bf8b67-df3c-429a-8dd6-6ef14cfebc7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2bb515f-0a88-4272-8c4b-e9fcb063c40c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9bf8b67-df3c-429a-8dd6-6ef14cfebc7a",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "de2f1cfe-3ed3-4945-94f5-840a3871ca5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "dadbd3e7-a946-4a0a-9af1-dfdc081d8a0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de2f1cfe-3ed3-4945-94f5-840a3871ca5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46b73e1a-cfb6-4671-b966-0149934c9e6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de2f1cfe-3ed3-4945-94f5-840a3871ca5c",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "76cf9c35-2eeb-4b8a-899d-33b596b1f813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "f646b92b-e9dc-496a-a684-4d8f242a3359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76cf9c35-2eeb-4b8a-899d-33b596b1f813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1000932-67c3-4998-b1b7-33bd0f4001d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76cf9c35-2eeb-4b8a-899d-33b596b1f813",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "6a2d349c-20ae-4056-b4d2-e6a42b10bf6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "05c0d80e-7942-4d8b-a332-8bbb3b79fd73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a2d349c-20ae-4056-b4d2-e6a42b10bf6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf38496d-5cb6-4fe0-be80-8a844b9b4929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a2d349c-20ae-4056-b4d2-e6a42b10bf6f",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "49c8f5a1-4dfb-4cbd-ae1f-29771ede497a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "96ea1722-5666-404e-8e68-c0e09f7578a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49c8f5a1-4dfb-4cbd-ae1f-29771ede497a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2dde2ef-f340-4126-a4df-e7f946443d3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49c8f5a1-4dfb-4cbd-ae1f-29771ede497a",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "7b2e6084-01f8-4f9d-aafe-8477c8475b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "0cf440e8-1dcd-427c-9dba-0edfbc4a4bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b2e6084-01f8-4f9d-aafe-8477c8475b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f9b1c6d-1566-4d38-a97b-2c9436626900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b2e6084-01f8-4f9d-aafe-8477c8475b2c",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "2a740411-4769-41ea-8f8c-439fb32b998a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "2c9f2e61-abeb-49a6-bd48-1de3d68490c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a740411-4769-41ea-8f8c-439fb32b998a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "172ab068-77f3-4c57-b602-f985a72acff8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a740411-4769-41ea-8f8c-439fb32b998a",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "6a8fd729-8ec0-4065-91c5-f7d924eea05d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "bdbd9106-24a8-474c-83cd-eba79ff51f6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a8fd729-8ec0-4065-91c5-f7d924eea05d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe16bd0e-4adc-4417-997e-29c5793e4780",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a8fd729-8ec0-4065-91c5-f7d924eea05d",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        },
        {
            "id": "9992fc1b-fd06-4def-b885-0dbadd4ab413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "compositeImage": {
                "id": "1d6df68c-a029-4dac-a071-82790a9ed14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9992fc1b-fd06-4def-b885-0dbadd4ab413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c76864f6-26c0-42b2-a3a6-2034bd1b5152",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9992fc1b-fd06-4def-b885-0dbadd4ab413",
                    "LayerId": "72aa0a78-aa87-4b92-aafe-9dcc82d43342"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "72aa0a78-aa87-4b92-aafe-9dcc82d43342",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82d3a07f-ec2f-4be8-9d6a-3eab0b5757c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 64
}