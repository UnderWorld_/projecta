{
    "id": "d95c9471-fc37-46f0-ada4-9c88d3a27d28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "empty_sprite",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d20b5e1-9d39-4945-9aa8-551da556c32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d95c9471-fc37-46f0-ada4-9c88d3a27d28",
            "compositeImage": {
                "id": "e2fc72e3-f62b-4891-b975-6fac36dd6902",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d20b5e1-9d39-4945-9aa8-551da556c32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e1c5270-79b0-467b-af19-6bbdcacdf6da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d20b5e1-9d39-4945-9aa8-551da556c32c",
                    "LayerId": "c7325dd7-0091-4c36-83bd-894e1d41a5c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c7325dd7-0091-4c36-83bd-894e1d41a5c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d95c9471-fc37-46f0-ada4-9c88d3a27d28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 30,
    "yorig": 30
}