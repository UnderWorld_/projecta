{
    "id": "c9fbcfa8-01da-4e8b-9dbb-c714bec9d31f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "n_player_walk_up_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 64,
    "bbox_right": 107,
    "bbox_top": 38,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "989bc037-c843-46b0-ae06-54221501fa84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9fbcfa8-01da-4e8b-9dbb-c714bec9d31f",
            "compositeImage": {
                "id": "3b9d7d29-cbff-4a3d-9c6c-378cb513cad2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "989bc037-c843-46b0-ae06-54221501fa84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71eece0f-a92b-4d4b-b678-1f63a11c40f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "989bc037-c843-46b0-ae06-54221501fa84",
                    "LayerId": "2970766a-f85e-4369-b5f5-c6f914e7b2c6"
                }
            ]
        },
        {
            "id": "a4eec5f5-6e2a-4815-b032-a77ffa2f90cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9fbcfa8-01da-4e8b-9dbb-c714bec9d31f",
            "compositeImage": {
                "id": "18007fd8-ecd4-446d-abf2-d00fb7a433ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4eec5f5-6e2a-4815-b032-a77ffa2f90cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b9730d0-ec22-4053-9777-f9d5ff8baa87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4eec5f5-6e2a-4815-b032-a77ffa2f90cb",
                    "LayerId": "2970766a-f85e-4369-b5f5-c6f914e7b2c6"
                }
            ]
        },
        {
            "id": "ef63abf2-ff4f-449a-92bd-36e13742b5ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9fbcfa8-01da-4e8b-9dbb-c714bec9d31f",
            "compositeImage": {
                "id": "3df6005d-1533-4c6a-be05-4cf3cf2f92d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef63abf2-ff4f-449a-92bd-36e13742b5ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8686193-83d6-48d9-b701-b20fe01817fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef63abf2-ff4f-449a-92bd-36e13742b5ab",
                    "LayerId": "2970766a-f85e-4369-b5f5-c6f914e7b2c6"
                }
            ]
        },
        {
            "id": "59f8c3d9-d7c0-4cba-8d03-e9053934bfa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9fbcfa8-01da-4e8b-9dbb-c714bec9d31f",
            "compositeImage": {
                "id": "ef10cbba-8cf7-4937-9371-4e4b98ba2977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59f8c3d9-d7c0-4cba-8d03-e9053934bfa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "863f1b73-f4ce-4895-9cd4-fb62d15200a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59f8c3d9-d7c0-4cba-8d03-e9053934bfa9",
                    "LayerId": "2970766a-f85e-4369-b5f5-c6f914e7b2c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "2970766a-f85e-4369-b5f5-c6f914e7b2c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9fbcfa8-01da-4e8b-9dbb-c714bec9d31f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 85
}