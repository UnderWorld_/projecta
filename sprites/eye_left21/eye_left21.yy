{
    "id": "5760d989-7ba4-4f9c-8418-d15a58f5c9e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4de9377d-3e5b-4de3-8913-7709d140b7b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5760d989-7ba4-4f9c-8418-d15a58f5c9e3",
            "compositeImage": {
                "id": "4ac4a45a-b028-4de6-9e26-b51149089a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de9377d-3e5b-4de3-8913-7709d140b7b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbdad75-7a35-46e1-b89d-82f6f3ecd662",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de9377d-3e5b-4de3-8913-7709d140b7b7",
                    "LayerId": "1ba13934-ace0-48be-b274-d1e9aaa93ad0"
                }
            ]
        },
        {
            "id": "2614ad15-c54c-4fa0-aa30-48e22c914d7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5760d989-7ba4-4f9c-8418-d15a58f5c9e3",
            "compositeImage": {
                "id": "d68e40d8-213d-479b-86ba-34b3ddef6803",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2614ad15-c54c-4fa0-aa30-48e22c914d7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5353a03a-ba3e-48e1-a35f-916166b08106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2614ad15-c54c-4fa0-aa30-48e22c914d7e",
                    "LayerId": "1ba13934-ace0-48be-b274-d1e9aaa93ad0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "1ba13934-ace0-48be-b274-d1e9aaa93ad0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5760d989-7ba4-4f9c-8418-d15a58f5c9e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}