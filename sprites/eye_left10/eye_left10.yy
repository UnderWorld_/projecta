{
    "id": "116b20c3-c626-42a0-a26c-df91fbce85f2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae6ba3b6-a541-4ed5-a7c4-b869824a6433",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "116b20c3-c626-42a0-a26c-df91fbce85f2",
            "compositeImage": {
                "id": "6b765ba8-0178-4013-a308-71859bf8314a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae6ba3b6-a541-4ed5-a7c4-b869824a6433",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a543b83f-4185-4361-9bd4-e2ae25c61603",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae6ba3b6-a541-4ed5-a7c4-b869824a6433",
                    "LayerId": "c2e4e76f-cd1e-4f1b-93bf-6b33d3785eb8"
                }
            ]
        },
        {
            "id": "0624426b-b5ee-4e08-b5b6-b7591b56217a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "116b20c3-c626-42a0-a26c-df91fbce85f2",
            "compositeImage": {
                "id": "c7cee376-b791-47e2-b63a-f788fbb66bf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0624426b-b5ee-4e08-b5b6-b7591b56217a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3b0627a-2cc4-4602-b279-d51263e2a38d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0624426b-b5ee-4e08-b5b6-b7591b56217a",
                    "LayerId": "c2e4e76f-cd1e-4f1b-93bf-6b33d3785eb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "c2e4e76f-cd1e-4f1b-93bf-6b33d3785eb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "116b20c3-c626-42a0-a26c-df91fbce85f2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}