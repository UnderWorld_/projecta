{
    "id": "3b244fdc-7602-49f7-b20e-44d2b2b507bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1de9a643-ccb5-4ec9-86e1-fc6d8f2f2a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b244fdc-7602-49f7-b20e-44d2b2b507bd",
            "compositeImage": {
                "id": "dd20c86b-75e0-4516-abf6-fd15f4c359fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1de9a643-ccb5-4ec9-86e1-fc6d8f2f2a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16d17c1-4ac7-4c33-aa97-896aaf2e9d81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1de9a643-ccb5-4ec9-86e1-fc6d8f2f2a8d",
                    "LayerId": "c7cbaba0-0dd6-49a7-97b3-88e55d39a5c2"
                }
            ]
        },
        {
            "id": "e099f1bf-5c3c-42ac-a856-bd237e93c134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b244fdc-7602-49f7-b20e-44d2b2b507bd",
            "compositeImage": {
                "id": "47626a89-835b-47a9-914f-dc6a865e1e32",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e099f1bf-5c3c-42ac-a856-bd237e93c134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb0549c-dc92-42d0-81ee-bc9aba4a9004",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e099f1bf-5c3c-42ac-a856-bd237e93c134",
                    "LayerId": "c7cbaba0-0dd6-49a7-97b3-88e55d39a5c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "c7cbaba0-0dd6-49a7-97b3-88e55d39a5c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b244fdc-7602-49f7-b20e-44d2b2b507bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}