{
    "id": "749b0f44-f02d-4702-b7db-0f4fef520caf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beard2_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 65,
    "bbox_left": 15,
    "bbox_right": 44,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fde714c-6087-4a16-a9bc-aafbf835946f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749b0f44-f02d-4702-b7db-0f4fef520caf",
            "compositeImage": {
                "id": "a6cc9058-c4c5-41fb-941f-d33e0af78b18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fde714c-6087-4a16-a9bc-aafbf835946f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d15aac04-7e10-4392-8102-6f847a0693f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fde714c-6087-4a16-a9bc-aafbf835946f",
                    "LayerId": "d54813ee-5ce0-4d3b-8004-ecf942659d66"
                }
            ]
        },
        {
            "id": "3b683d4b-b5f3-49d2-b9dc-2b27ba539746",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749b0f44-f02d-4702-b7db-0f4fef520caf",
            "compositeImage": {
                "id": "2d7396a4-5ed7-4f5f-8069-de2f024bcec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b683d4b-b5f3-49d2-b9dc-2b27ba539746",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a9f2881-51e5-441b-8f7e-c7a65754289e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b683d4b-b5f3-49d2-b9dc-2b27ba539746",
                    "LayerId": "d54813ee-5ce0-4d3b-8004-ecf942659d66"
                }
            ]
        },
        {
            "id": "fdcf2365-230c-4bc5-a1dd-10dd31a79a0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749b0f44-f02d-4702-b7db-0f4fef520caf",
            "compositeImage": {
                "id": "17302de7-ae9d-466c-81d9-b123944f9628",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdcf2365-230c-4bc5-a1dd-10dd31a79a0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b61fa130-b8aa-4ceb-9208-697270c6e362",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdcf2365-230c-4bc5-a1dd-10dd31a79a0a",
                    "LayerId": "d54813ee-5ce0-4d3b-8004-ecf942659d66"
                }
            ]
        },
        {
            "id": "13e61ce2-6865-4099-9761-85a1242dce33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "749b0f44-f02d-4702-b7db-0f4fef520caf",
            "compositeImage": {
                "id": "7dab1f79-957e-41b2-a6f1-e6356ac76e20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13e61ce2-6865-4099-9761-85a1242dce33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d9eab62-d2f7-4f25-ad75-b981a50ff441",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13e61ce2-6865-4099-9761-85a1242dce33",
                    "LayerId": "d54813ee-5ce0-4d3b-8004-ecf942659d66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "d54813ee-5ce0-4d3b-8004-ecf942659d66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "749b0f44-f02d-4702-b7db-0f4fef520caf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}