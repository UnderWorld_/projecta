{
    "id": "64e65e26-6cbb-4711-9e69-1aca583c3aa9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "light1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "618d280c-1366-4325-8653-36d1483eafac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64e65e26-6cbb-4711-9e69-1aca583c3aa9",
            "compositeImage": {
                "id": "e4571225-ec3c-4dff-b605-27d2cb9eeb55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "618d280c-1366-4325-8653-36d1483eafac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e9b87d6-54d0-435f-ae3b-0d12054430c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "618d280c-1366-4325-8653-36d1483eafac",
                    "LayerId": "7be3e548-ae15-4d0a-aea6-05da65e32436"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "7be3e548-ae15-4d0a-aea6-05da65e32436",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64e65e26-6cbb-4711-9e69-1aca583c3aa9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}