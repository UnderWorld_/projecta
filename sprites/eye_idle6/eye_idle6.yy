{
    "id": "5619c94b-25ec-4e16-a3d8-06e547f9958c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "01c61f24-9964-4b67-b043-3668c41628e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5619c94b-25ec-4e16-a3d8-06e547f9958c",
            "compositeImage": {
                "id": "2f705b87-d878-49e8-8413-07f7b3522ce7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01c61f24-9964-4b67-b043-3668c41628e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f25e6d6-2875-47f0-9c0c-b2b52ea55db6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01c61f24-9964-4b67-b043-3668c41628e5",
                    "LayerId": "84da74e0-e951-4a6c-9874-d92a55f3e116"
                }
            ]
        },
        {
            "id": "84bf978f-e7c3-4def-9368-4ea6578f6b83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5619c94b-25ec-4e16-a3d8-06e547f9958c",
            "compositeImage": {
                "id": "d6448156-fa7b-425c-aab0-30100e3c9857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84bf978f-e7c3-4def-9368-4ea6578f6b83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd111327-89be-40b8-ae1f-09e1cbc9bef0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84bf978f-e7c3-4def-9368-4ea6578f6b83",
                    "LayerId": "84da74e0-e951-4a6c-9874-d92a55f3e116"
                }
            ]
        },
        {
            "id": "7826190f-0e43-4e05-b3a1-a761f22b769e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5619c94b-25ec-4e16-a3d8-06e547f9958c",
            "compositeImage": {
                "id": "d2075aee-f3e0-4912-bd70-fd80ce071125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7826190f-0e43-4e05-b3a1-a761f22b769e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7562df9-6d63-4bcd-9a46-e13ef57f7caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7826190f-0e43-4e05-b3a1-a761f22b769e",
                    "LayerId": "84da74e0-e951-4a6c-9874-d92a55f3e116"
                }
            ]
        },
        {
            "id": "120be160-6225-4ce6-9f7e-78162c08e117",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5619c94b-25ec-4e16-a3d8-06e547f9958c",
            "compositeImage": {
                "id": "b01c734a-3fc2-484d-bae8-9f1e5256eb09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "120be160-6225-4ce6-9f7e-78162c08e117",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b12dc652-8699-40d2-87b9-10bbaf903ba8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "120be160-6225-4ce6-9f7e-78162c08e117",
                    "LayerId": "84da74e0-e951-4a6c-9874-d92a55f3e116"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "84da74e0-e951-4a6c-9874-d92a55f3e116",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5619c94b-25ec-4e16-a3d8-06e547f9958c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}