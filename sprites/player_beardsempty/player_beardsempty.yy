{
    "id": "316538a0-1afa-4c8c-a0f1-b2d55933d628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_beardsempty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 28,
    "bbox_right": 28,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "298ca395-b21e-43d0-a69b-3364915dcf4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316538a0-1afa-4c8c-a0f1-b2d55933d628",
            "compositeImage": {
                "id": "f50a073d-20da-44fd-a4a8-93b1b7317c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "298ca395-b21e-43d0-a69b-3364915dcf4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8bc787-d526-43a4-9f01-a92999834abf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "298ca395-b21e-43d0-a69b-3364915dcf4d",
                    "LayerId": "09147f59-805c-4d8b-814f-4a1b19b9407c"
                }
            ]
        },
        {
            "id": "50b0f95f-9dba-4ef1-91f5-97db0e9c8ac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "316538a0-1afa-4c8c-a0f1-b2d55933d628",
            "compositeImage": {
                "id": "87f35ff8-10bf-4d23-ab12-77fec690d613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b0f95f-9dba-4ef1-91f5-97db0e9c8ac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dc5a3d1-dfaa-4eac-a211-f836fcf234c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b0f95f-9dba-4ef1-91f5-97db0e9c8ac6",
                    "LayerId": "09147f59-805c-4d8b-814f-4a1b19b9407c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "09147f59-805c-4d8b-814f-4a1b19b9407c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "316538a0-1afa-4c8c-a0f1-b2d55933d628",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}