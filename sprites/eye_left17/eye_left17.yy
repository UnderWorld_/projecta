{
    "id": "6d6024b9-f808-4ab6-86b5-6fb21004cd5f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8f5ba9c1-db9c-4a8e-b2d9-34687b956103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6024b9-f808-4ab6-86b5-6fb21004cd5f",
            "compositeImage": {
                "id": "44c66b7b-49d0-495c-acf9-5472f3fc9a5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f5ba9c1-db9c-4a8e-b2d9-34687b956103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69c156a8-258b-4a13-bae9-c6afd46f163e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f5ba9c1-db9c-4a8e-b2d9-34687b956103",
                    "LayerId": "4cb1b984-43e6-4995-a3ca-c20edce873b9"
                }
            ]
        },
        {
            "id": "164561bf-eb24-4c9b-a2cb-243695dc82fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6d6024b9-f808-4ab6-86b5-6fb21004cd5f",
            "compositeImage": {
                "id": "195073ae-9f21-467a-997b-499cd9cf896d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164561bf-eb24-4c9b-a2cb-243695dc82fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f292beb-a4a8-4d15-b72e-2b5185bd4ad8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164561bf-eb24-4c9b-a2cb-243695dc82fd",
                    "LayerId": "4cb1b984-43e6-4995-a3ca-c20edce873b9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4cb1b984-43e6-4995-a3ca-c20edce873b9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6d6024b9-f808-4ab6-86b5-6fb21004cd5f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}