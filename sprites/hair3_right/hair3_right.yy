{
    "id": "7de9a81f-7f50-48b9-9ffe-629d339255b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair3_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 10,
    "bbox_right": 79,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ebeb56fa-3bfd-4db7-8676-0aa57ab46c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de9a81f-7f50-48b9-9ffe-629d339255b4",
            "compositeImage": {
                "id": "32366f58-5fcb-478b-a252-9697eb99db8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebeb56fa-3bfd-4db7-8676-0aa57ab46c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4a800b-e82d-4395-8d90-188585483e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebeb56fa-3bfd-4db7-8676-0aa57ab46c73",
                    "LayerId": "0dcd5e97-23eb-4845-9310-7c06c1e7797d"
                }
            ]
        },
        {
            "id": "c5cdc0dc-143f-4578-b11b-72e676295854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7de9a81f-7f50-48b9-9ffe-629d339255b4",
            "compositeImage": {
                "id": "7b7aba48-5ff8-4cda-ac90-52f5eda8c485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5cdc0dc-143f-4578-b11b-72e676295854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "611c160c-fb57-456f-b80c-d77aa8091539",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5cdc0dc-143f-4578-b11b-72e676295854",
                    "LayerId": "0dcd5e97-23eb-4845-9310-7c06c1e7797d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "0dcd5e97-23eb-4845-9310-7c06c1e7797d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7de9a81f-7f50-48b9-9ffe-629d339255b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}