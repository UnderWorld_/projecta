{
    "id": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 10,
    "bbox_right": 20,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4ccb4e2-5849-49be-9aea-01e279a5dab6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
            "compositeImage": {
                "id": "ec85af64-6fcc-4592-8eb9-1b00183b652a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4ccb4e2-5849-49be-9aea-01e279a5dab6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f300ed3-3e05-4055-a86e-6c0e2a70de2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4ccb4e2-5849-49be-9aea-01e279a5dab6",
                    "LayerId": "f7e1cc6c-6ec1-4538-aa5a-99475df285a8"
                }
            ]
        },
        {
            "id": "0547f281-cadf-48cc-8fd8-15341fc1da91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
            "compositeImage": {
                "id": "1629291a-72b0-4d7d-8ec2-741d1dea9926",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0547f281-cadf-48cc-8fd8-15341fc1da91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2b52711-2cf0-4a6e-8429-8a0c3969adaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0547f281-cadf-48cc-8fd8-15341fc1da91",
                    "LayerId": "f7e1cc6c-6ec1-4538-aa5a-99475df285a8"
                }
            ]
        },
        {
            "id": "ed80a1cb-93ff-4b86-8010-65d574b16ad9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
            "compositeImage": {
                "id": "33fb7120-9f5c-4ec1-b135-573680c7afe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed80a1cb-93ff-4b86-8010-65d574b16ad9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3bbee8c-cbfd-49c0-a0c1-8681a6aff511",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed80a1cb-93ff-4b86-8010-65d574b16ad9",
                    "LayerId": "f7e1cc6c-6ec1-4538-aa5a-99475df285a8"
                }
            ]
        },
        {
            "id": "b688e898-2cc5-4f38-9a20-0f9d1d7f4ec2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
            "compositeImage": {
                "id": "4b830ca6-3586-4259-a6bd-7c705d6e826e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b688e898-2cc5-4f38-9a20-0f9d1d7f4ec2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261da886-34d2-42e7-8363-41da71088258",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b688e898-2cc5-4f38-9a20-0f9d1d7f4ec2",
                    "LayerId": "f7e1cc6c-6ec1-4538-aa5a-99475df285a8"
                }
            ]
        },
        {
            "id": "0ae88186-08ff-47a1-aeda-44c05b155f42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
            "compositeImage": {
                "id": "efeb5bd9-c675-4bcb-95ca-97afcb0f0c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae88186-08ff-47a1-aeda-44c05b155f42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7cfa400-e504-4460-8add-b6d422b37121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae88186-08ff-47a1-aeda-44c05b155f42",
                    "LayerId": "f7e1cc6c-6ec1-4538-aa5a-99475df285a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f7e1cc6c-6ec1-4538-aa5a-99475df285a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dcc6a1dc-f0aa-4a7e-a27e-2850492a0cd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 16
}