{
    "id": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "campfire_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "905d9eb0-f0f3-4114-97a5-8ab2ab7e1bf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "42650200-3d70-42c0-ae2e-c4e55c483595",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "905d9eb0-f0f3-4114-97a5-8ab2ab7e1bf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97db02dd-659b-45db-8b15-d7c03cc061ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "905d9eb0-f0f3-4114-97a5-8ab2ab7e1bf1",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        },
        {
            "id": "7bc863c8-a076-474d-b9d1-49dca43971f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "e768c937-db32-4574-837f-17c50b4fea3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bc863c8-a076-474d-b9d1-49dca43971f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f9cd33f-14d9-43c6-9c69-fc6d3941eb18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bc863c8-a076-474d-b9d1-49dca43971f7",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        },
        {
            "id": "442060e1-a34e-4db1-8a53-6b80f4196062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "dd95f047-c052-4151-8c7c-c9563941e8f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "442060e1-a34e-4db1-8a53-6b80f4196062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "692f056f-0944-485a-902a-b00423b3b777",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "442060e1-a34e-4db1-8a53-6b80f4196062",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        },
        {
            "id": "65f66fa9-5527-4d75-954e-cd3cb037b1c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "9dfd9cfe-5b51-4923-9b33-6cbe4f5a8bd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f66fa9-5527-4d75-954e-cd3cb037b1c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60686136-6d9f-4f72-9ea3-ec8486f32fac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f66fa9-5527-4d75-954e-cd3cb037b1c3",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        },
        {
            "id": "7df08043-31fc-4e4d-b505-c07d1e8ef66f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "571c7f04-0019-443f-86c2-e82ae0bc5e99",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7df08043-31fc-4e4d-b505-c07d1e8ef66f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7d95d44-0603-41e9-acb9-d4b5dfcb5ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7df08043-31fc-4e4d-b505-c07d1e8ef66f",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        },
        {
            "id": "46affc42-47e3-4e2d-9233-3bd27cfcad58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "1c89296a-d09d-4f83-8212-6cf66e46bf58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46affc42-47e3-4e2d-9233-3bd27cfcad58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad2fdf31-c7b5-4221-afe0-2c63332317e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46affc42-47e3-4e2d-9233-3bd27cfcad58",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        },
        {
            "id": "c06303f7-9025-4f7e-afae-f9ceb88aa532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "compositeImage": {
                "id": "17e80cf9-8290-4867-9654-d65acea0149e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c06303f7-9025-4f7e-afae-f9ceb88aa532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe02cee-511d-4f0a-9a11-c37c59ac8a4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c06303f7-9025-4f7e-afae-f9ceb88aa532",
                    "LayerId": "b624ce0f-a786-4667-b2de-2734d14db5f1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "b624ce0f-a786-4667-b2de-2734d14db5f1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}