{
    "id": "3929c78d-3866-4023-8dc7-7dd762855c20",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite110",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 335,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f1d5b0c-4f7d-4293-94f0-24fb38782313",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3929c78d-3866-4023-8dc7-7dd762855c20",
            "compositeImage": {
                "id": "f05b4907-e90a-4af0-8e86-abe15e15fbb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f1d5b0c-4f7d-4293-94f0-24fb38782313",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5fefd10-b3ed-4a0c-be85-f1f7556090fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f1d5b0c-4f7d-4293-94f0-24fb38782313",
                    "LayerId": "d55d51bf-7123-4704-a490-6434faf19b4f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 336,
    "layers": [
        {
            "id": "d55d51bf-7123-4704-a490-6434faf19b4f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3929c78d-3866-4023-8dc7-7dd762855c20",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}