{
    "id": "40523d33-a236-43f5-85e4-04251681c4e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite82",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 1,
    "bbox_right": 363,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6dcd25e7-76de-485d-826b-c5679c24b9f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40523d33-a236-43f5-85e4-04251681c4e0",
            "compositeImage": {
                "id": "d3dffc36-808c-4744-a696-eeb0ad3b2c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dcd25e7-76de-485d-826b-c5679c24b9f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "811232a7-8290-4763-bb83-2d085a8cbc1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dcd25e7-76de-485d-826b-c5679c24b9f3",
                    "LayerId": "24fee0fb-1bbf-4255-9185-e876714f41b2"
                }
            ]
        },
        {
            "id": "3e4840d7-272d-4cbc-8310-3dcfe6df3d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "40523d33-a236-43f5-85e4-04251681c4e0",
            "compositeImage": {
                "id": "02ee3066-801d-47a4-b12f-be66509a8fa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e4840d7-272d-4cbc-8310-3dcfe6df3d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c87a0b76-0e3b-4cd4-b217-214d64586a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e4840d7-272d-4cbc-8310-3dcfe6df3d74",
                    "LayerId": "24fee0fb-1bbf-4255-9185-e876714f41b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "24fee0fb-1bbf-4255-9185-e876714f41b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "40523d33-a236-43f5-85e4-04251681c4e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 367,
    "xorig": 0,
    "yorig": 0
}