{
    "id": "334488f5-033f-4b3e-b4c6-9aaafc16d538",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shrub6_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 0,
    "bbox_right": 16,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbe3d513-53c2-421c-9da0-e6bd98c74782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "334488f5-033f-4b3e-b4c6-9aaafc16d538",
            "compositeImage": {
                "id": "88039ccb-7ef1-4f78-bb2e-6afe65ef0bc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe3d513-53c2-421c-9da0-e6bd98c74782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ed922dc-6fa6-4421-a398-ecac8942931a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe3d513-53c2-421c-9da0-e6bd98c74782",
                    "LayerId": "e0bedf2d-963e-4f21-b4a2-67a88eaf5e9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 17,
    "layers": [
        {
            "id": "e0bedf2d-963e-4f21-b4a2-67a88eaf5e9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "334488f5-033f-4b3e-b4c6-9aaafc16d538",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 17,
    "xorig": 8,
    "yorig": 16
}