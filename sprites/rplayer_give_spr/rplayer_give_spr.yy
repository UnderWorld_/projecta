{
    "id": "a290969c-4f59-4439-9466-43766051af0c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_give_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf5ea367-bc61-46ea-aaca-ceeb12856a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "191800c3-7ca9-4f8f-a718-e31a286a4f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf5ea367-bc61-46ea-aaca-ceeb12856a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0384955-6c73-4108-b809-5e789ae10a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf5ea367-bc61-46ea-aaca-ceeb12856a4d",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "f6f926d2-cb08-4464-8c0a-8aded9961591",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "874d77a3-ba2a-4196-8fcf-4d8727c29b87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6f926d2-cb08-4464-8c0a-8aded9961591",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f309496f-ba0a-470e-833e-71cc3b438ce2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6f926d2-cb08-4464-8c0a-8aded9961591",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "b87e9fc3-8026-4409-8dea-0d56844dfa97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "e0f95677-5ce3-4861-a4f7-78303f69e2d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b87e9fc3-8026-4409-8dea-0d56844dfa97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa3d40a7-b3cd-4206-bd2e-b386bff89bf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b87e9fc3-8026-4409-8dea-0d56844dfa97",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "ba23f41a-c2e6-4a3e-addc-0926b22d4548",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "51394c47-568a-430f-8f3e-d10da592f4ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba23f41a-c2e6-4a3e-addc-0926b22d4548",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19a26fd5-0bfb-4a41-9732-465972429caf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba23f41a-c2e6-4a3e-addc-0926b22d4548",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "b80f15df-318e-4d59-a774-cf5d0085a19e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "94c6416d-778e-4930-832e-4889b2a37cbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b80f15df-318e-4d59-a774-cf5d0085a19e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34ea8c53-14e7-4bdb-9aa0-d1df0b6bb8e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b80f15df-318e-4d59-a774-cf5d0085a19e",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "caff04d3-3b75-4573-8c86-dad6bdf64056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "ca3052cc-aeef-4ab0-b0ce-c4aa8f5f9931",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caff04d3-3b75-4573-8c86-dad6bdf64056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99470b8d-ea64-47f6-9a38-6bf84270ccd1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caff04d3-3b75-4573-8c86-dad6bdf64056",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "c841f7da-d054-4d29-831c-f6ffea8c6f91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "4b9c5202-4742-43ae-8f64-efbfd2c1f722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c841f7da-d054-4d29-831c-f6ffea8c6f91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1da90540-5284-40fe-befd-9328150f68c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c841f7da-d054-4d29-831c-f6ffea8c6f91",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "6d6e37ce-b481-4081-a722-ab2f213a813a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "8d70fb3d-5b88-4cf5-9244-3df3690cbc35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6e37ce-b481-4081-a722-ab2f213a813a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf5466f3-9cef-4aed-b8dd-dd3c38d6b07a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6e37ce-b481-4081-a722-ab2f213a813a",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "fea0e437-4ffe-44f0-b48b-03d78182fc24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "21e44400-86b4-4abf-9841-3ed50beb880c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fea0e437-4ffe-44f0-b48b-03d78182fc24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e9e1f98-7a4b-4b6f-a359-98da5b8f188e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fea0e437-4ffe-44f0-b48b-03d78182fc24",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        },
        {
            "id": "8f3a93e6-9800-4eb4-970b-60161cb94d1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "compositeImage": {
                "id": "198a0504-f8c2-4b5f-87e9-3fa15100331f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f3a93e6-9800-4eb4-970b-60161cb94d1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfd3d3a3-f01f-47b1-91be-05c1fe309b12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f3a93e6-9800-4eb4-970b-60161cb94d1b",
                    "LayerId": "47f5d42b-9869-4d21-8a1b-76ac86eac495"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "47f5d42b-9869-4d21-8a1b-76ac86eac495",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a290969c-4f59-4439-9466-43766051af0c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 17
}