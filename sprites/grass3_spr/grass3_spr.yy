{
    "id": "e8a56c40-4622-46d2-9beb-d63b43cda3f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 64,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f7da224-d6f1-4607-a4c1-e0ce1d9c85d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8a56c40-4622-46d2-9beb-d63b43cda3f6",
            "compositeImage": {
                "id": "6463a55b-e347-43c0-b994-a7895ba31647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f7da224-d6f1-4607-a4c1-e0ce1d9c85d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a57ae7b-015b-4fad-a4b5-efeede2f9111",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f7da224-d6f1-4607-a4c1-e0ce1d9c85d0",
                    "LayerId": "5de7af60-fb0e-4ccd-a1bb-9ef62531d21d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5de7af60-fb0e-4ccd-a1bb-9ef62531d21d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8a56c40-4622-46d2-9beb-d63b43cda3f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}