{
    "id": "864c35ff-2c98-4206-abdc-73eb08f15e90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1ed70dc8-2994-49af-979b-eec4987bd9b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864c35ff-2c98-4206-abdc-73eb08f15e90",
            "compositeImage": {
                "id": "4e015743-63d7-491f-8a4e-a1ecf9578188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ed70dc8-2994-49af-979b-eec4987bd9b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1600a6e4-9ca9-4c4c-8663-548d35b52b83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ed70dc8-2994-49af-979b-eec4987bd9b6",
                    "LayerId": "41840bd6-3d71-4bdb-95e1-5c84ae98149b"
                }
            ]
        },
        {
            "id": "c7aa3e03-f126-41cb-a097-bfaccd148997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864c35ff-2c98-4206-abdc-73eb08f15e90",
            "compositeImage": {
                "id": "502c9b37-ad6a-4e7f-b3d4-8d12e4825bc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7aa3e03-f126-41cb-a097-bfaccd148997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d6f4e13-cd0a-4267-b8c0-af0b143d1bab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7aa3e03-f126-41cb-a097-bfaccd148997",
                    "LayerId": "41840bd6-3d71-4bdb-95e1-5c84ae98149b"
                }
            ]
        },
        {
            "id": "27e445ab-eaf4-4431-a0ba-8dabab2cfd62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864c35ff-2c98-4206-abdc-73eb08f15e90",
            "compositeImage": {
                "id": "54ef5154-b2bc-477b-9fda-0f78bdeb1de0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27e445ab-eaf4-4431-a0ba-8dabab2cfd62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afc08ed4-9637-41c4-be04-a51c97f61181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27e445ab-eaf4-4431-a0ba-8dabab2cfd62",
                    "LayerId": "41840bd6-3d71-4bdb-95e1-5c84ae98149b"
                }
            ]
        },
        {
            "id": "5de7975c-97d9-4fb6-938c-9b7b86852256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "864c35ff-2c98-4206-abdc-73eb08f15e90",
            "compositeImage": {
                "id": "26dea3fd-2fc2-4e2f-92dc-91c835dd947d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5de7975c-97d9-4fb6-938c-9b7b86852256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6be2900b-d5fd-41ec-9988-9b112a06d1b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5de7975c-97d9-4fb6-938c-9b7b86852256",
                    "LayerId": "41840bd6-3d71-4bdb-95e1-5c84ae98149b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "41840bd6-3d71-4bdb-95e1-5c84ae98149b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "864c35ff-2c98-4206-abdc-73eb08f15e90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}