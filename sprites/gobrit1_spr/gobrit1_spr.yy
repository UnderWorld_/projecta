{
    "id": "7c5391b2-30a9-4164-a845-144d48aa784d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gobrit1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 17,
    "bbox_right": 49,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd0b0c93-938e-4c45-974d-302f86e5207e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c5391b2-30a9-4164-a845-144d48aa784d",
            "compositeImage": {
                "id": "b097136c-1536-4a02-9604-a141f6a16f36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0b0c93-938e-4c45-974d-302f86e5207e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41d036c6-81c7-48f2-b220-4e53b48ec4df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0b0c93-938e-4c45-974d-302f86e5207e",
                    "LayerId": "140ac192-7773-43be-8f02-4cb161bec2fb"
                }
            ]
        },
        {
            "id": "35ba4a49-d993-461f-905b-d673775b10e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c5391b2-30a9-4164-a845-144d48aa784d",
            "compositeImage": {
                "id": "209af787-8e36-49a2-ac3c-8f85951678a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35ba4a49-d993-461f-905b-d673775b10e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6785be9-7f44-4016-afd3-f9d22179fab7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35ba4a49-d993-461f-905b-d673775b10e0",
                    "LayerId": "140ac192-7773-43be-8f02-4cb161bec2fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "140ac192-7773-43be-8f02-4cb161bec2fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c5391b2-30a9-4164-a845-144d48aa784d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 55
}