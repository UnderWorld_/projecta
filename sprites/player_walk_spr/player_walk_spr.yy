{
    "id": "d34a86f9-2240-48c1-9449-7ce59aaded4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_walk_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 142,
    "bbox_left": 37,
    "bbox_right": 108,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2485d969-eae0-4a50-a356-715adf8daa39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d34a86f9-2240-48c1-9449-7ce59aaded4c",
            "compositeImage": {
                "id": "cd90d79a-e6d0-425e-b246-14a49806bbea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2485d969-eae0-4a50-a356-715adf8daa39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8a6a15-6cb6-422c-9f53-fe364e819978",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2485d969-eae0-4a50-a356-715adf8daa39",
                    "LayerId": "81c79972-a0f8-4cfa-b4a4-537a4193964f"
                }
            ]
        },
        {
            "id": "18c971d7-9bb2-4a63-b480-2da3413b84b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d34a86f9-2240-48c1-9449-7ce59aaded4c",
            "compositeImage": {
                "id": "8850819a-d29d-4a29-9603-65d8940b7398",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18c971d7-9bb2-4a63-b480-2da3413b84b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4efaba62-48af-47cb-9b90-13c2f0c2709b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18c971d7-9bb2-4a63-b480-2da3413b84b0",
                    "LayerId": "81c79972-a0f8-4cfa-b4a4-537a4193964f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "81c79972-a0f8-4cfa-b4a4-537a4193964f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d34a86f9-2240-48c1-9449-7ce59aaded4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 36,
    "yorig": 47
}