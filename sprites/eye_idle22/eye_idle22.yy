{
    "id": "a9117dd6-29ca-4892-a2f3-ce33278ff5df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "187b239f-8095-4472-a5e1-051de80f9daf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9117dd6-29ca-4892-a2f3-ce33278ff5df",
            "compositeImage": {
                "id": "4c165cf1-a681-41cd-aa31-8db49e4884c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "187b239f-8095-4472-a5e1-051de80f9daf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42dc310c-4366-43b0-90a5-af0bc46ac0f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "187b239f-8095-4472-a5e1-051de80f9daf",
                    "LayerId": "63d9e760-6af0-48b0-a354-a8bdbe70372e"
                }
            ]
        },
        {
            "id": "334d1c66-3b60-4c2a-8d3c-f69a7280f0e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9117dd6-29ca-4892-a2f3-ce33278ff5df",
            "compositeImage": {
                "id": "63bda1d4-992e-4a8b-b11c-d711458a91c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "334d1c66-3b60-4c2a-8d3c-f69a7280f0e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "083fb90d-285b-4e32-9bae-9446df913322",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "334d1c66-3b60-4c2a-8d3c-f69a7280f0e1",
                    "LayerId": "63d9e760-6af0-48b0-a354-a8bdbe70372e"
                }
            ]
        },
        {
            "id": "9c650e38-5926-4b8a-8f31-f77143f3ac06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9117dd6-29ca-4892-a2f3-ce33278ff5df",
            "compositeImage": {
                "id": "71744281-fd5b-49eb-b349-1492d55d1e5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c650e38-5926-4b8a-8f31-f77143f3ac06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d3dd111-2309-4404-aa5a-4e8486dd159d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c650e38-5926-4b8a-8f31-f77143f3ac06",
                    "LayerId": "63d9e760-6af0-48b0-a354-a8bdbe70372e"
                }
            ]
        },
        {
            "id": "f2bf31b3-f509-459a-a8f1-89e8fe82130e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9117dd6-29ca-4892-a2f3-ce33278ff5df",
            "compositeImage": {
                "id": "39d2a3f7-fd96-46f2-97d2-74d92efb750d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2bf31b3-f509-459a-a8f1-89e8fe82130e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11bfd5db-847f-4977-95fa-64b34db3d329",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2bf31b3-f509-459a-a8f1-89e8fe82130e",
                    "LayerId": "63d9e760-6af0-48b0-a354-a8bdbe70372e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "63d9e760-6af0-48b0-a354-a8bdbe70372e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9117dd6-29ca-4892-a2f3-ce33278ff5df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}