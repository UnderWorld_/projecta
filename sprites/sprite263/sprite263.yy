{
    "id": "004fc0bd-4de4-4bba-9470-76453a1541d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite263",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c526efc9-090c-402e-a38c-286de9ec3e89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "aff731ed-8508-4488-9ce3-32d2eeb758b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c526efc9-090c-402e-a38c-286de9ec3e89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f2d5b76-513c-4388-971f-1204d4165808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c526efc9-090c-402e-a38c-286de9ec3e89",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "719fc1f4-c3ca-47b2-b7e8-6da1bd6ef32b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "730f8ede-a992-4f21-9a20-3d12deede5b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "719fc1f4-c3ca-47b2-b7e8-6da1bd6ef32b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aeeffa17-c5b3-49d2-8e59-040f2ee71049",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "719fc1f4-c3ca-47b2-b7e8-6da1bd6ef32b",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "3060bb43-490d-4d60-b9ca-3b1f6839413e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "2ec3fad5-a0b7-4512-94eb-680d1a863a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3060bb43-490d-4d60-b9ca-3b1f6839413e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb23c638-496a-4bbd-96c6-d0ff358d3450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3060bb43-490d-4d60-b9ca-3b1f6839413e",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "5bfc3afe-237f-4917-b3dc-0e94e1eb3727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "cf4ba929-d4eb-4173-ac99-9eda9387be21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bfc3afe-237f-4917-b3dc-0e94e1eb3727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6407b1f2-67be-489b-8050-508508c3a167",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bfc3afe-237f-4917-b3dc-0e94e1eb3727",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "6f0d082e-8086-4062-a534-51a4aaee678e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "3e1031d9-2306-4645-bfe5-72739def5d83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f0d082e-8086-4062-a534-51a4aaee678e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba3d76a9-eb79-4ef4-95f9-9e3068630ef5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f0d082e-8086-4062-a534-51a4aaee678e",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "000b781d-90ae-4bcd-9885-5b59dad16d3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "28bfc983-b928-4fb1-a564-5c5162fbab5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "000b781d-90ae-4bcd-9885-5b59dad16d3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52c047f7-c748-4e89-8874-ee1005e57b63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "000b781d-90ae-4bcd-9885-5b59dad16d3c",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "4307dae7-8ca5-45b2-ad2c-61c9c615d7a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "eab8f90e-15c4-4579-9caa-6beb56f340ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4307dae7-8ca5-45b2-ad2c-61c9c615d7a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "132f612b-d37b-4804-9572-62aeb64c5daf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4307dae7-8ca5-45b2-ad2c-61c9c615d7a0",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "f957e1be-5629-458b-84fd-144e47089252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "7b3d40b0-a13b-4d10-af72-2c385717f7c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f957e1be-5629-458b-84fd-144e47089252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7872690-1886-42c5-8133-3ab1d0fb0b13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f957e1be-5629-458b-84fd-144e47089252",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "8d301ea1-7b73-4107-a55a-8d2615ef9348",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "22365e7d-8ea4-43c0-bd93-b0c01a01f2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d301ea1-7b73-4107-a55a-8d2615ef9348",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55af9ee5-6685-4340-a38c-637f9089e06d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d301ea1-7b73-4107-a55a-8d2615ef9348",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "9cc4d672-6226-46a3-a3a1-c87746cee408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "b6ba4070-e3e7-406a-9e21-ff2d9b0fc8fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cc4d672-6226-46a3-a3a1-c87746cee408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e234a3bb-46c7-4a93-b4be-42289a6e7720",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cc4d672-6226-46a3-a3a1-c87746cee408",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "3c6c5635-5dfa-40a0-b56a-fb56ea369346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "e5d31ad2-8155-4ad3-8ab2-020c0bc1f9fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c6c5635-5dfa-40a0-b56a-fb56ea369346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b922a587-4d8f-42d7-9371-f54ca61c9beb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c6c5635-5dfa-40a0-b56a-fb56ea369346",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "91b57bfa-26ec-451b-91e6-f22c2aac023e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "accd892a-c833-4106-a188-667efc14a792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91b57bfa-26ec-451b-91e6-f22c2aac023e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac3488b2-238e-4189-bf57-6831aea15ae8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91b57bfa-26ec-451b-91e6-f22c2aac023e",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "1b38e9d8-2af3-4300-850b-9ec773c3dd0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "cfdc39d8-5b3f-45bf-bd9a-be7e5d405047",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b38e9d8-2af3-4300-850b-9ec773c3dd0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee0d59f1-44fd-489e-92b3-65e364ea3852",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b38e9d8-2af3-4300-850b-9ec773c3dd0d",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "b361d86b-cb79-457a-893e-169520eaeffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "8dd5b9f5-de1d-44ab-b5df-2ffa6c6e1285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b361d86b-cb79-457a-893e-169520eaeffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63c8ef3b-4106-4e52-86c7-10ac57e6f5ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b361d86b-cb79-457a-893e-169520eaeffc",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "e9a83e04-153a-4563-8d56-0c6787e141ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "0fb72b9e-3b23-403b-b5c1-3e52cf798b02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9a83e04-153a-4563-8d56-0c6787e141ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b9728d6-82d9-4f56-9470-4a31253cefe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9a83e04-153a-4563-8d56-0c6787e141ec",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "d4bf5b35-0a35-4d42-a5af-7acb401bdf01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "ee7e4d3d-f437-4d3c-8277-08b38154c502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4bf5b35-0a35-4d42-a5af-7acb401bdf01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11102d47-04a3-455a-b22d-21f6c7992574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4bf5b35-0a35-4d42-a5af-7acb401bdf01",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "59b9b414-f70d-489f-a65d-c0d69e3538d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "c8c9ca43-0581-4b4b-ace0-0ccc778877dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b9b414-f70d-489f-a65d-c0d69e3538d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78e3ddec-c484-4fb6-9b4b-0bbdd5a11d3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b9b414-f70d-489f-a65d-c0d69e3538d7",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "1f25eacb-8ed7-4774-876a-46944cc94a70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "8765c68e-c1b3-4641-906b-c46fd6a6a1ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f25eacb-8ed7-4774-876a-46944cc94a70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48ef7a65-37a8-4377-94a1-5d4581a39d8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f25eacb-8ed7-4774-876a-46944cc94a70",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "20af33f0-f2b9-4afa-b49a-ec927caf3090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "c5d28346-ee1e-409d-87e6-39a948053452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20af33f0-f2b9-4afa-b49a-ec927caf3090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6d5ce6b-0e2d-425f-ad9f-5cc0ba0f1686",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20af33f0-f2b9-4afa-b49a-ec927caf3090",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "5da0a83f-d61b-42d8-8cfb-9c8c03354515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "4d93621a-8c88-47ea-8343-52126617e011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da0a83f-d61b-42d8-8cfb-9c8c03354515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4582a0b-679f-4f27-8ec2-83eeca327d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da0a83f-d61b-42d8-8cfb-9c8c03354515",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "426404db-a036-4312-8426-edfff490d4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "f8d38156-e6c4-47c9-ae54-11da789dcf6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "426404db-a036-4312-8426-edfff490d4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ab815e3-e48e-42ed-b22b-473d89593bf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "426404db-a036-4312-8426-edfff490d4cc",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "a064b463-7fee-43fb-ac44-68ebab5cc76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "ad8d3986-d7a0-4ba5-a4e8-09c1fc2e8d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a064b463-7fee-43fb-ac44-68ebab5cc76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d022b0-eb6c-439a-8ed8-919fb107f95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a064b463-7fee-43fb-ac44-68ebab5cc76f",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "a1053a08-d964-40ad-a136-4ba21761f887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "796516b9-ec3c-4cdc-8967-4307d74b41fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1053a08-d964-40ad-a136-4ba21761f887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38328139-307f-4289-846b-beb66ab70e5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1053a08-d964-40ad-a136-4ba21761f887",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "cd70c0c3-0329-4c9f-9658-de1f8f45e907",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "99708e14-2b02-427f-86eb-761b1e723367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd70c0c3-0329-4c9f-9658-de1f8f45e907",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76e2d8af-ca75-408d-8a23-ee77ee13b881",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd70c0c3-0329-4c9f-9658-de1f8f45e907",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "82d202c0-33e1-4aca-93d1-3dec70c3db2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "179d601e-b90a-4e25-b9fc-e5f7277ea660",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82d202c0-33e1-4aca-93d1-3dec70c3db2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10135fe3-663b-499e-8c21-a558cb706077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82d202c0-33e1-4aca-93d1-3dec70c3db2b",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "b7abb099-336f-4eb4-b71a-222b1f631409",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "2019a439-93b4-4079-b584-7ea497f370dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7abb099-336f-4eb4-b71a-222b1f631409",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b7d88a-76ab-40bc-ba05-d4373e8881bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7abb099-336f-4eb4-b71a-222b1f631409",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "1822e81a-d75d-4184-aa44-e8d00e013525",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "93cdedd5-c958-4494-b008-53baa61bc5b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1822e81a-d75d-4184-aa44-e8d00e013525",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c6f0202-5872-41f4-95f2-60802dca21c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1822e81a-d75d-4184-aa44-e8d00e013525",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "ccecdeb4-2479-42d2-907b-4ed10c26e3dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "8c675140-bc24-479e-875c-5c8b1dc30e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccecdeb4-2479-42d2-907b-4ed10c26e3dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57614ff8-ade2-4f79-9bad-2371e0351c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccecdeb4-2479-42d2-907b-4ed10c26e3dc",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "a61c888f-97bb-400c-ab23-88c5f694150d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "83643074-93dd-4604-8416-eea485abf8d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a61c888f-97bb-400c-ab23-88c5f694150d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a71faa2f-424f-43cd-8411-4ba630f09e86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a61c888f-97bb-400c-ab23-88c5f694150d",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "d937cfb7-7070-4c46-9461-d78af0574cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "76d6be7a-b101-4774-87a2-114ae2ac8f98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d937cfb7-7070-4c46-9461-d78af0574cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5068e50e-ca04-430e-9746-ec516ee3c064",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d937cfb7-7070-4c46-9461-d78af0574cb7",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "29c7f951-e9e9-4309-bac4-cda71f3b0a85",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "eab71da7-838b-42b8-8917-01c673a2b0d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29c7f951-e9e9-4309-bac4-cda71f3b0a85",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74aef249-94bc-48c4-95b0-cea81eed8ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29c7f951-e9e9-4309-bac4-cda71f3b0a85",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "e2a064f3-a716-4e5d-b490-036e03934955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "3e85878a-bac2-42ac-981f-9bd284050b4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2a064f3-a716-4e5d-b490-036e03934955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7149c2ca-e06a-463e-979f-55594aecba04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2a064f3-a716-4e5d-b490-036e03934955",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "dbe1a54e-6d39-4a8c-b460-167ed7550929",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "ef383db8-a5e8-4d64-ab42-7aac7a260567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe1a54e-6d39-4a8c-b460-167ed7550929",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ac24c5c-7253-40bc-bebf-62e3a10fad1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe1a54e-6d39-4a8c-b460-167ed7550929",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "73b72c67-ab9d-4ecc-9e9a-8444a92256b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "d77c5fe2-3a98-428e-9847-b619cb286407",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73b72c67-ab9d-4ecc-9e9a-8444a92256b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d43aeb83-4154-4a03-9f0a-bfc28ba89656",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73b72c67-ab9d-4ecc-9e9a-8444a92256b6",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "17b57d00-ee2a-4f33-9f59-7a4b1cc9dffc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "51d0d288-250d-4986-b6eb-1f5930765634",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17b57d00-ee2a-4f33-9f59-7a4b1cc9dffc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cef0f66-521f-4a37-97c4-53f6af48867d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17b57d00-ee2a-4f33-9f59-7a4b1cc9dffc",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "ab364cd3-f28f-46c2-a46f-1906b1b6bab2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "89753f6c-3d2e-405f-b5d5-741eb7b28ec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab364cd3-f28f-46c2-a46f-1906b1b6bab2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d78698e9-1d31-4262-88f1-e78a304c6a6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab364cd3-f28f-46c2-a46f-1906b1b6bab2",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "becd766f-b06f-4437-a613-2933c47ec85a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "c9534f93-ce9b-4810-91eb-83dc55d1e72e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "becd766f-b06f-4437-a613-2933c47ec85a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "119dd8ff-0312-4467-a07c-4e1ac43d4b08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "becd766f-b06f-4437-a613-2933c47ec85a",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "2a86ff36-6b0d-46c0-82e6-5dc399722831",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "fd2f11e7-e199-4e31-9ef5-ce1f992a86bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a86ff36-6b0d-46c0-82e6-5dc399722831",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9011a39-e15d-44bb-877f-0a5433acc7f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a86ff36-6b0d-46c0-82e6-5dc399722831",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "fed475f3-2a10-4083-a7a2-bd0aecf61c2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "18c20d12-db1c-4852-a5d7-60e4e8df359b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fed475f3-2a10-4083-a7a2-bd0aecf61c2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bf2d9ab-fd4d-46e9-8eec-255a00c4fa74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fed475f3-2a10-4083-a7a2-bd0aecf61c2e",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "1d76baac-192d-4210-add0-45f6e4b49364",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "0b8193b6-1dfd-4973-b379-3f6dea186e7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d76baac-192d-4210-add0-45f6e4b49364",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "375e921c-ecbd-4ddb-abf1-011c20c14c9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d76baac-192d-4210-add0-45f6e4b49364",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "b272c068-6e35-4b23-9583-2192c68a5994",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "40c533c5-69de-4af9-bd6d-3b3274a22b4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b272c068-6e35-4b23-9583-2192c68a5994",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d2582a-8e5e-4123-b195-093ad6c15385",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b272c068-6e35-4b23-9583-2192c68a5994",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "c4bde63e-22a2-4fc9-a1bb-9359b82b2730",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "67c9b9c2-940f-4432-8103-2f7e6649675c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4bde63e-22a2-4fc9-a1bb-9359b82b2730",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a6937e-3f41-4d5e-b01f-8e3ccdfd192f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4bde63e-22a2-4fc9-a1bb-9359b82b2730",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "01df354c-9dc3-411c-86c4-8d00ad5c9099",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "f4f3de0c-d5c7-4b3f-b090-2a7e3d7bd601",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01df354c-9dc3-411c-86c4-8d00ad5c9099",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fbda0de-872e-4166-9fb2-863bd2f2d844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01df354c-9dc3-411c-86c4-8d00ad5c9099",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "76c2753a-b438-46da-8120-afac0158b15f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "493d0b7b-28a8-4682-94b6-ae7712086367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76c2753a-b438-46da-8120-afac0158b15f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6cedb1-28d6-4997-802e-306d317cb29b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76c2753a-b438-46da-8120-afac0158b15f",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "7d1b275e-5461-43b9-b56c-d6f31ebfd32c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "62ee9afe-2725-42a0-87a3-4b51b49ca433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d1b275e-5461-43b9-b56c-d6f31ebfd32c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c0f1b6a-00bc-430d-9281-c64aea578326",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d1b275e-5461-43b9-b56c-d6f31ebfd32c",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "28ce3ec2-827e-41c1-bd3f-c2a0dd2871dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "4d36c298-7172-4f92-83b2-17e1e49c51c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28ce3ec2-827e-41c1-bd3f-c2a0dd2871dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a4a6f83-efd1-46d9-83f9-45243ab25785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28ce3ec2-827e-41c1-bd3f-c2a0dd2871dd",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "7d4e3a29-e385-4a44-bfc9-eb0647018639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "0de9505c-aeb2-41b9-a228-6a6630f512af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d4e3a29-e385-4a44-bfc9-eb0647018639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20dd2f23-fbdf-40c8-b976-afd55b115205",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d4e3a29-e385-4a44-bfc9-eb0647018639",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        },
        {
            "id": "31d89390-87cd-440d-b33a-1d04415c2639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "compositeImage": {
                "id": "7bf5ba6f-9ed7-4aeb-a183-273f1f55f7b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31d89390-87cd-440d-b33a-1d04415c2639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f717b5bc-8edd-42e7-9a45-18da3d95f027",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31d89390-87cd-440d-b33a-1d04415c2639",
                    "LayerId": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "7a7ddb73-5af5-41ec-939a-2692ff8a5de5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}