{
    "id": "b145ada0-df06-4c37-93db-0749abfb5249",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menuwallpaper2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4c2fa934-fa23-4957-accc-179ccf2c0eab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "a920d64c-9905-4aed-a25c-f8c9a5f8b1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c2fa934-fa23-4957-accc-179ccf2c0eab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbdbf024-0323-472e-8142-454fada82a56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c2fa934-fa23-4957-accc-179ccf2c0eab",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "3e68e0d2-7da6-4c2c-b93a-1ccdcb6849f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "d470ee26-5a46-4e27-a571-218a30548722",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e68e0d2-7da6-4c2c-b93a-1ccdcb6849f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ccb1e61-c6e7-4724-99ee-47e8518c5de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e68e0d2-7da6-4c2c-b93a-1ccdcb6849f0",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "db92bebb-e49b-4227-8316-f7860e3b2a5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "24524389-8a43-4362-b798-c5bce26f03a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db92bebb-e49b-4227-8316-f7860e3b2a5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0118a2a-4b9c-4f8d-8f6c-04ec4bb715a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db92bebb-e49b-4227-8316-f7860e3b2a5f",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "6375f27f-04d6-4ed5-a196-ee8320a5c89c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "c55a792e-8a86-4650-bde7-692dafc6bb25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6375f27f-04d6-4ed5-a196-ee8320a5c89c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32d75d4f-eeba-4ab3-b295-62e2fa957482",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6375f27f-04d6-4ed5-a196-ee8320a5c89c",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "0d3d97de-f464-4f67-8bad-95323e0e477b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "5b8897db-f2be-4e43-abb4-e20be26e71a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d3d97de-f464-4f67-8bad-95323e0e477b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da47d575-eee1-447e-b42d-d9cc237180cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d3d97de-f464-4f67-8bad-95323e0e477b",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "566d1c0a-c997-4c7f-a53b-9d498ba64191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "c7ff9840-cbec-404f-9ff3-6dd2cc3cc6ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "566d1c0a-c997-4c7f-a53b-9d498ba64191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e8c8ee-7829-48cd-a46f-2799aa48ecdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "566d1c0a-c997-4c7f-a53b-9d498ba64191",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "9d78005b-0197-420b-b208-5e865a416d6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "f4da196b-1173-4397-b7eb-9f378c6d02a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d78005b-0197-420b-b208-5e865a416d6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ef3c33b-ff1a-4b67-9a7e-ef8b2ec8b6d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d78005b-0197-420b-b208-5e865a416d6c",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "430a3dcc-7a11-4270-b7ea-fb491223e7bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "429f6ace-0730-49a4-83c9-3ee68b227602",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "430a3dcc-7a11-4270-b7ea-fb491223e7bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d121d04c-3bc9-49d9-868e-df497d4c71a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "430a3dcc-7a11-4270-b7ea-fb491223e7bb",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        },
        {
            "id": "000f36cb-b32b-4540-be04-e9c815c4446f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "compositeImage": {
                "id": "83c4472d-e95e-4e2e-acf6-4b216cb126a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "000f36cb-b32b-4540-be04-e9c815c4446f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc67bd5c-2d9d-498c-a9cc-f49a0b18bb3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "000f36cb-b32b-4540-be04-e9c815c4446f",
                    "LayerId": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "0db4af67-9ef3-4bb4-9fad-140173fa6cb2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b145ada0-df06-4c37-93db-0749abfb5249",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 13,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}