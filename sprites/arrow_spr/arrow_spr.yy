{
    "id": "ed0e2b69-ce4b-42ca-b3dc-a8e28a3bc435",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "arrow_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 23,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75a805d7-bfdd-4d05-abd5-7606cb864688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed0e2b69-ce4b-42ca-b3dc-a8e28a3bc435",
            "compositeImage": {
                "id": "66f2d05c-dbd6-4f22-9bc9-e24a3d85a411",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75a805d7-bfdd-4d05-abd5-7606cb864688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7eead9-deb9-4ef2-9841-f6d0c61d2a34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75a805d7-bfdd-4d05-abd5-7606cb864688",
                    "LayerId": "d0333e49-cd21-4305-be69-e784aae78f4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "d0333e49-cd21-4305-be69-e784aae78f4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed0e2b69-ce4b-42ca-b3dc-a8e28a3bc435",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": -2,
    "yorig": 8
}