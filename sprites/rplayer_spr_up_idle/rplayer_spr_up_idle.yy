{
    "id": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_spr_up_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 115,
    "bbox_left": 44,
    "bbox_right": 82,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a09d735d-26bb-4611-a8a9-6396739fa5aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
            "compositeImage": {
                "id": "17979713-3fa5-44e7-b59c-abfdd8b9d79a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a09d735d-26bb-4611-a8a9-6396739fa5aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63b008ee-034f-4976-be9d-9b861f765802",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a09d735d-26bb-4611-a8a9-6396739fa5aa",
                    "LayerId": "67a2df85-a91c-48fb-87a2-d37755d458ad"
                }
            ]
        },
        {
            "id": "8548752b-ee57-430e-8123-a6eb88905840",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
            "compositeImage": {
                "id": "ff0c891f-0c4f-4634-b4b1-ecc0ccd6a217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8548752b-ee57-430e-8123-a6eb88905840",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "368f5c62-9e54-4948-b729-be1ce5427bc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8548752b-ee57-430e-8123-a6eb88905840",
                    "LayerId": "67a2df85-a91c-48fb-87a2-d37755d458ad"
                }
            ]
        },
        {
            "id": "fcb59a1c-bdff-476e-ace4-35d2d1ba7b35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
            "compositeImage": {
                "id": "b6cb7f66-fa5c-43ae-8e99-87f9d8024832",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcb59a1c-bdff-476e-ace4-35d2d1ba7b35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90d3d649-c87d-4c27-9a71-db3644d3b801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcb59a1c-bdff-476e-ace4-35d2d1ba7b35",
                    "LayerId": "67a2df85-a91c-48fb-87a2-d37755d458ad"
                }
            ]
        },
        {
            "id": "b6fd83be-81ec-4213-be0e-7953a45c4b06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
            "compositeImage": {
                "id": "3ecfc98d-9df9-4077-a769-f0ae57f5b709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6fd83be-81ec-4213-be0e-7953a45c4b06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "027f9461-c632-4ab1-8924-1704c8322cf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6fd83be-81ec-4213-be0e-7953a45c4b06",
                    "LayerId": "67a2df85-a91c-48fb-87a2-d37755d458ad"
                }
            ]
        },
        {
            "id": "a724506a-9040-4f10-a474-af03a12e7c54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
            "compositeImage": {
                "id": "22ffc208-11be-4ec8-b307-792fea0e4776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a724506a-9040-4f10-a474-af03a12e7c54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bc32e9b-9730-4463-83bf-4ab6f20cd71b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a724506a-9040-4f10-a474-af03a12e7c54",
                    "LayerId": "67a2df85-a91c-48fb-87a2-d37755d458ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "67a2df85-a91c-48fb-87a2-d37755d458ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "870d0d3f-45a1-45f1-941c-4c8c73c04776",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 38,
    "yorig": 65
}