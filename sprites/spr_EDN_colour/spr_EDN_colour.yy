{
    "id": "998e3d00-47f0-4c92-ae48-8362930cd34f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_colour",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b84ed55d-f3ff-4f87-9b5b-caebe1331888",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "998e3d00-47f0-4c92-ae48-8362930cd34f",
            "compositeImage": {
                "id": "f4a2fb4d-ddd2-4b28-931d-c8866282204f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b84ed55d-f3ff-4f87-9b5b-caebe1331888",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14bb5293-a6fe-4d8c-bc45-e1f0d88e63b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b84ed55d-f3ff-4f87-9b5b-caebe1331888",
                    "LayerId": "9483f28e-17eb-4daf-9540-44653610cc3f"
                }
            ]
        },
        {
            "id": "8218acb9-c31d-4d4f-a634-b5633bc9a211",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "998e3d00-47f0-4c92-ae48-8362930cd34f",
            "compositeImage": {
                "id": "e52548cc-f781-4231-99df-6553c49b5b80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8218acb9-c31d-4d4f-a634-b5633bc9a211",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6f5204c-dfb6-47d0-9fe5-f6a31faf0114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8218acb9-c31d-4d4f-a634-b5633bc9a211",
                    "LayerId": "9483f28e-17eb-4daf-9540-44653610cc3f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "9483f28e-17eb-4daf-9540-44653610cc3f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "998e3d00-47f0-4c92-ae48-8362930cd34f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 100,
    "yorig": 100
}