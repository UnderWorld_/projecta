{
    "id": "a5d7bfc1-63da-4288-bee2-3cc9bffe60ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house_veg_ds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1599,
    "bbox_left": 0,
    "bbox_right": 1299,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c795c50-b1a1-47db-8cba-390df36594b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5d7bfc1-63da-4288-bee2-3cc9bffe60ac",
            "compositeImage": {
                "id": "3bc5cc28-17c6-42d3-8ef2-4165bd2ffe11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c795c50-b1a1-47db-8cba-390df36594b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ec565ef-ca8d-494a-b323-73199952f447",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c795c50-b1a1-47db-8cba-390df36594b0",
                    "LayerId": "0bddbb69-69e8-485f-94b9-9ab5f1dac664"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1600,
    "layers": [
        {
            "id": "0bddbb69-69e8-485f-94b9-9ab5f1dac664",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5d7bfc1-63da-4288-bee2-3cc9bffe60ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1300,
    "xorig": 650,
    "yorig": 1599
}