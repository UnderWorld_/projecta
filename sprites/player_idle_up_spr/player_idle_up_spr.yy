{
    "id": "63ea46cf-b946-49a5-aef2-c7966133f5e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_idle_up_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 157,
    "bbox_left": 22,
    "bbox_right": 97,
    "bbox_top": 48,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "967f666e-83fc-4471-8a84-c2077fc7225b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63ea46cf-b946-49a5-aef2-c7966133f5e3",
            "compositeImage": {
                "id": "3f13fdb9-ad7e-454f-886a-ae17dcb1bb34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "967f666e-83fc-4471-8a84-c2077fc7225b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d0565fd-c36a-466b-8a58-dcaac11f9279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "967f666e-83fc-4471-8a84-c2077fc7225b",
                    "LayerId": "4cc961dd-8915-4adf-9e93-173ff0eb80a6"
                }
            ]
        },
        {
            "id": "abb339f9-380a-4968-92ef-4c07866ec993",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63ea46cf-b946-49a5-aef2-c7966133f5e3",
            "compositeImage": {
                "id": "0555fcca-1eb4-4468-9c0f-5d935be50a39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abb339f9-380a-4968-92ef-4c07866ec993",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "854bc05e-6eb8-4b14-ba4c-8655ec080c75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abb339f9-380a-4968-92ef-4c07866ec993",
                    "LayerId": "4cc961dd-8915-4adf-9e93-173ff0eb80a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "4cc961dd-8915-4adf-9e93-173ff0eb80a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63ea46cf-b946-49a5-aef2-c7966133f5e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 68,
    "xorig": 32,
    "yorig": 52
}