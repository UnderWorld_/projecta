{
    "id": "39bf0801-e260-4e18-b93a-5f8b4f513429",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin2_idle_sprTEST",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 36,
    "bbox_right": 155,
    "bbox_top": -2,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19107ab7-0759-4a78-a744-48ce2a83d0a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39bf0801-e260-4e18-b93a-5f8b4f513429",
            "compositeImage": {
                "id": "73ca86bc-0f83-46f6-8a3e-4023832da35a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19107ab7-0759-4a78-a744-48ce2a83d0a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566d0a20-9c8b-4e91-8f8a-d8df3255712b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19107ab7-0759-4a78-a744-48ce2a83d0a9",
                    "LayerId": "74c367e8-5ea0-432d-a7d9-8eb2291627c5"
                }
            ]
        },
        {
            "id": "f67bed2e-db9f-4bb0-bacd-f475381cad84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39bf0801-e260-4e18-b93a-5f8b4f513429",
            "compositeImage": {
                "id": "dde2b4db-8f3b-4981-9959-0afb654bc717",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f67bed2e-db9f-4bb0-bacd-f475381cad84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dacbe76-1d06-444d-b95a-d85fb2b7ff99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f67bed2e-db9f-4bb0-bacd-f475381cad84",
                    "LayerId": "74c367e8-5ea0-432d-a7d9-8eb2291627c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "74c367e8-5ea0-432d-a7d9-8eb2291627c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39bf0801-e260-4e18-b93a-5f8b4f513429",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 64
}