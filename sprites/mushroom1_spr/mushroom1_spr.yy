{
    "id": "0f550efd-05e4-4e2d-ab01-034c867ce300",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mushroom1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 1,
    "bbox_right": 48,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "feb36ff5-404f-4c2b-b3b6-35bb66f54f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f550efd-05e4-4e2d-ab01-034c867ce300",
            "compositeImage": {
                "id": "0b163084-271d-4076-b9ce-bcda9654e54d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feb36ff5-404f-4c2b-b3b6-35bb66f54f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c46f50-373c-459a-b5f6-4b9510a31974",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feb36ff5-404f-4c2b-b3b6-35bb66f54f55",
                    "LayerId": "859cbba7-a072-498d-bccd-1bad11aa6102"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "859cbba7-a072-498d-bccd-1bad11aa6102",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f550efd-05e4-4e2d-ab01-034c867ce300",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 24,
    "yorig": 20
}