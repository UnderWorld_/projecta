{
    "id": "3b619901-dc53-4777-ab05-534e36a5ac3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "small_axe_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9564d684-1473-4986-a674-6c0d9cc980b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b619901-dc53-4777-ab05-534e36a5ac3a",
            "compositeImage": {
                "id": "22b143e3-ea5b-44c8-8726-ef8c07f8a34c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9564d684-1473-4986-a674-6c0d9cc980b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f834b75-7869-426d-8a00-dbebb3908c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9564d684-1473-4986-a674-6c0d9cc980b5",
                    "LayerId": "9b51a46f-9a12-4f5f-aa33-c84f2ccbbd5f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 44,
    "layers": [
        {
            "id": "9b51a46f-9a12-4f5f-aa33-c84f2ccbbd5f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b619901-dc53-4777-ab05-534e36a5ac3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": -50,
    "yorig": 17
}