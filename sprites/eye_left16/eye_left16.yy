{
    "id": "fd82ce5a-5fc0-4384-a85a-1b78beab72bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcf5a353-d63a-4d69-8d39-cdbcb6646341",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd82ce5a-5fc0-4384-a85a-1b78beab72bd",
            "compositeImage": {
                "id": "d5438206-85f5-4f33-9812-0e59fc6e0f73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcf5a353-d63a-4d69-8d39-cdbcb6646341",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "374747c5-b0cf-4be6-bd48-81b94c9ed965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcf5a353-d63a-4d69-8d39-cdbcb6646341",
                    "LayerId": "e8ec8f8f-6bce-4d39-b047-61a6c4782786"
                }
            ]
        },
        {
            "id": "bd44470b-b82c-4778-bcf9-d17d5a11ed81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd82ce5a-5fc0-4384-a85a-1b78beab72bd",
            "compositeImage": {
                "id": "235bf192-a4ff-45db-b448-58aea88825ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd44470b-b82c-4778-bcf9-d17d5a11ed81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc37027e-27ce-4d70-9fcf-5df5af3452ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd44470b-b82c-4778-bcf9-d17d5a11ed81",
                    "LayerId": "e8ec8f8f-6bce-4d39-b047-61a6c4782786"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "e8ec8f8f-6bce-4d39-b047-61a6c4782786",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd82ce5a-5fc0-4384-a85a-1b78beab72bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}