{
    "id": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_eyes_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "47b07119-7f12-4825-8654-727ac4de70ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "e5a5f409-8621-4e37-bb1b-f25d69fce54e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b07119-7f12-4825-8654-727ac4de70ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b849d9-7e53-4826-9101-a20e1b9650d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b07119-7f12-4825-8654-727ac4de70ab",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "d833926a-c1bb-43e7-9012-5bc421b3fb31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "bb9f0858-0339-4edd-be37-457d13d3ec03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d833926a-c1bb-43e7-9012-5bc421b3fb31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8671381-9f8c-462b-8403-c730d31ab17e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d833926a-c1bb-43e7-9012-5bc421b3fb31",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "e2740d81-dbcf-47bc-a237-804d6a361287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "80366dd4-e275-47b6-8855-0b5a511d22b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2740d81-dbcf-47bc-a237-804d6a361287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "232ade64-1db2-4e70-ba0a-8672d2c719f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2740d81-dbcf-47bc-a237-804d6a361287",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "ffabf7b2-b3a6-42a2-8758-5d3eb4ff7c32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "e15b3068-720a-4344-b890-6ac370efed90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffabf7b2-b3a6-42a2-8758-5d3eb4ff7c32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50e5d250-e939-4a8e-b68e-622f417f5008",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffabf7b2-b3a6-42a2-8758-5d3eb4ff7c32",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "4296509b-a99b-4d09-9f32-2ecbec2374a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "b4196d60-2d43-4ab7-a00a-b5e4f1f16021",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4296509b-a99b-4d09-9f32-2ecbec2374a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8893bc6-5e72-4de3-8607-eb7de2c9a0fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4296509b-a99b-4d09-9f32-2ecbec2374a7",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "91bd3282-ac57-462a-acd1-7777403f901a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "517f1762-bab3-442f-805f-f983e0943af2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91bd3282-ac57-462a-acd1-7777403f901a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a6acf9d-ca83-44ff-aa08-a0f5f5c0a810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91bd3282-ac57-462a-acd1-7777403f901a",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "dad7404d-747a-421b-a00d-18faf6274c9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "0b6e407d-989b-4e34-85c5-0ce4a3f7dd5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dad7404d-747a-421b-a00d-18faf6274c9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7554e034-a1f2-4c43-b8bb-15dc24e992fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dad7404d-747a-421b-a00d-18faf6274c9c",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "19f0c9de-e950-4924-970c-c22486363cc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "ecbed02e-eb15-4e72-a725-34885b8d0863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19f0c9de-e950-4924-970c-c22486363cc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86f53079-fe01-46c2-9609-e5bebdee4315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19f0c9de-e950-4924-970c-c22486363cc1",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "580834da-408f-41eb-b99c-16cf870d298c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "40c76bfe-5d7e-480e-a403-9182b7d04028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "580834da-408f-41eb-b99c-16cf870d298c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21d34daf-e1f8-41d0-a6af-9e0fe1ce0375",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "580834da-408f-41eb-b99c-16cf870d298c",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "b4949aa7-e5d7-4158-94aa-dcca5815ac86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "4dbebd66-350b-4e21-88e8-f4dfec0bccfa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4949aa7-e5d7-4158-94aa-dcca5815ac86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7795297b-47d8-45fc-abbb-a22241cdeaf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4949aa7-e5d7-4158-94aa-dcca5815ac86",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "987f6c99-143a-4ce3-b14e-5c0484d3eded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "00ecab5f-9e5d-40fc-be86-9dc09503e138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "987f6c99-143a-4ce3-b14e-5c0484d3eded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82cb3bd8-1e3b-4f16-94e4-e6826b7b7dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "987f6c99-143a-4ce3-b14e-5c0484d3eded",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "ceef0c6a-dc60-450a-9340-478358929827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "2a437e21-0956-4647-a67b-48b1284fc82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ceef0c6a-dc60-450a-9340-478358929827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29704c61-ae5e-435a-810f-9f06b9695d05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ceef0c6a-dc60-450a-9340-478358929827",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "96f24111-f701-43ae-9b50-45c295c19ef0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "fae93380-a1cb-43f5-9333-299fcc7ff867",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96f24111-f701-43ae-9b50-45c295c19ef0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bdbfb222-5b36-4ef5-bec4-8e127d2a6952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96f24111-f701-43ae-9b50-45c295c19ef0",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "50b349bd-b403-4910-86f6-e1c1cd373422",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "bc7c7c75-5180-4536-904d-bfd7d0215b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b349bd-b403-4910-86f6-e1c1cd373422",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "213298d1-1b9a-422c-ba13-82c4ff594a62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b349bd-b403-4910-86f6-e1c1cd373422",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "f83a4c0d-bd84-4a15-9588-7f224f399d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "6e71a994-16f7-498f-ac4d-4109cf8109bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f83a4c0d-bd84-4a15-9588-7f224f399d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c4ea6fa-3212-46b2-80ac-38b4535e5471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f83a4c0d-bd84-4a15-9588-7f224f399d1a",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "7e2d4f34-1a97-489e-a52a-fe67021c8ba8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "fdb2c588-db8e-4d9a-b52e-085e3e8dfced",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e2d4f34-1a97-489e-a52a-fe67021c8ba8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e0179e2-6ce6-4155-a294-1e7ae2c36ac4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e2d4f34-1a97-489e-a52a-fe67021c8ba8",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "6f477762-f164-4e2b-b5b1-a80d533ae3db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "63ce23d1-dea4-4eb1-ac01-96eba39193b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f477762-f164-4e2b-b5b1-a80d533ae3db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b371ff82-0f4d-4bfa-a160-af6166b3e80c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f477762-f164-4e2b-b5b1-a80d533ae3db",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "e09db402-3264-4e62-be02-01043c14863e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "91f59585-37e6-4a8f-93a9-75b6b4fa28f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09db402-3264-4e62-be02-01043c14863e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa8d11ed-9119-40ee-bee4-142bae365afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09db402-3264-4e62-be02-01043c14863e",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "4f8c1c1a-b813-46a6-8f24-c7c6054435d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "e47aa20b-f08d-4140-a442-ad7d62724ce6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f8c1c1a-b813-46a6-8f24-c7c6054435d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d19e86af-8ce7-4bff-ad1c-2d7646875145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f8c1c1a-b813-46a6-8f24-c7c6054435d4",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "e065bda1-e370-4834-914e-8560ac92b4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "d62bd9d8-7788-4695-a585-5b34a698c864",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e065bda1-e370-4834-914e-8560ac92b4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6bc203-6f9a-47de-81bf-2c62fdb9dccf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e065bda1-e370-4834-914e-8560ac92b4c4",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "8ff6b2fe-9bb8-4bb5-ad73-ae4e331fd5cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "afb38256-5750-43a2-bebe-7cbc48e241a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff6b2fe-9bb8-4bb5-ad73-ae4e331fd5cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "727b80ab-544f-4bd1-9f06-9f858b1fbfdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff6b2fe-9bb8-4bb5-ad73-ae4e331fd5cb",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        },
        {
            "id": "9c286c79-d9d3-4000-833d-fa86b5f6f0ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "compositeImage": {
                "id": "2ea1c7a8-4b58-49e4-a54d-e222748a905c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c286c79-d9d3-4000-833d-fa86b5f6f0ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28d102f2-3a8b-4528-87af-7bc098acde07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c286c79-d9d3-4000-833d-fa86b5f6f0ec",
                    "LayerId": "b461d114-2dee-4581-844c-7a9c47ba0e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "b461d114-2dee-4581-844c-7a9c47ba0e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}