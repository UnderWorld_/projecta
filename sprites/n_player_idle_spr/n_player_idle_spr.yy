{
    "id": "183d1e1f-cafb-47fe-bb3c-fe37fe90fb98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "n_player_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 54,
    "bbox_right": 109,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9104d0e-133e-4c94-a646-2b0b9002da88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183d1e1f-cafb-47fe-bb3c-fe37fe90fb98",
            "compositeImage": {
                "id": "e3b0155a-5fc0-4e9f-96ce-72e357a713b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9104d0e-133e-4c94-a646-2b0b9002da88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9057a325-5e62-4382-b0cf-602ca43afaba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9104d0e-133e-4c94-a646-2b0b9002da88",
                    "LayerId": "3b20ab3e-ea76-4d0e-a724-3a8363726a46"
                }
            ]
        },
        {
            "id": "3d813c42-74d1-4883-8832-827816584488",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183d1e1f-cafb-47fe-bb3c-fe37fe90fb98",
            "compositeImage": {
                "id": "429263d4-bd03-4643-b5e3-227df29a95f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d813c42-74d1-4883-8832-827816584488",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5b7ab52-13e5-47c2-87e2-96a500e80fa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d813c42-74d1-4883-8832-827816584488",
                    "LayerId": "3b20ab3e-ea76-4d0e-a724-3a8363726a46"
                }
            ]
        },
        {
            "id": "08400fbb-ed6c-4338-b1c0-b7b94f98563a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183d1e1f-cafb-47fe-bb3c-fe37fe90fb98",
            "compositeImage": {
                "id": "4c57cc28-0b95-45e6-b0cd-80e6aa05f6a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08400fbb-ed6c-4338-b1c0-b7b94f98563a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f50bbc8-29eb-42f4-80a2-de9743005d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08400fbb-ed6c-4338-b1c0-b7b94f98563a",
                    "LayerId": "3b20ab3e-ea76-4d0e-a724-3a8363726a46"
                }
            ]
        },
        {
            "id": "d6752f78-b927-4338-96ad-c3e01a96605b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183d1e1f-cafb-47fe-bb3c-fe37fe90fb98",
            "compositeImage": {
                "id": "91939be0-e45b-44d0-bde4-63673bef552e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6752f78-b927-4338-96ad-c3e01a96605b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fa754fd-f6b6-4a19-8b0f-077caf839623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6752f78-b927-4338-96ad-c3e01a96605b",
                    "LayerId": "3b20ab3e-ea76-4d0e-a724-3a8363726a46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "3b20ab3e-ea76-4d0e-a724-3a8363726a46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "183d1e1f-cafb-47fe-bb3c-fe37fe90fb98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 85
}