{
    "id": "a36adcb0-d9bb-4025-a287-1929cbc7e72d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f391e679-eb1f-4d51-85f9-11cd75c6ca50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36adcb0-d9bb-4025-a287-1929cbc7e72d",
            "compositeImage": {
                "id": "bfd694fb-71e7-4d74-9126-bf079b38db67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f391e679-eb1f-4d51-85f9-11cd75c6ca50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae8a10ee-56d7-455e-a009-1409c8c4746e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f391e679-eb1f-4d51-85f9-11cd75c6ca50",
                    "LayerId": "d37c70f4-d1d0-4dc4-8d36-431b4add9ce7"
                }
            ]
        },
        {
            "id": "6faac699-f1f7-4671-a429-dc60e2a5b4da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36adcb0-d9bb-4025-a287-1929cbc7e72d",
            "compositeImage": {
                "id": "0059e683-1d33-4a22-8759-a4d34442031e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6faac699-f1f7-4671-a429-dc60e2a5b4da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "824e2258-4f18-42f2-ac84-32429af0c297",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6faac699-f1f7-4671-a429-dc60e2a5b4da",
                    "LayerId": "d37c70f4-d1d0-4dc4-8d36-431b4add9ce7"
                }
            ]
        },
        {
            "id": "04119238-1b02-4925-8542-f64efbc41a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36adcb0-d9bb-4025-a287-1929cbc7e72d",
            "compositeImage": {
                "id": "f0cb22c1-5837-4a00-9bbc-e7052721a56c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04119238-1b02-4925-8542-f64efbc41a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa68bbe0-e130-4bb4-b864-cf8253b8bf2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04119238-1b02-4925-8542-f64efbc41a53",
                    "LayerId": "d37c70f4-d1d0-4dc4-8d36-431b4add9ce7"
                }
            ]
        },
        {
            "id": "dd3a60ef-75e6-47cd-b027-f985c11be93c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a36adcb0-d9bb-4025-a287-1929cbc7e72d",
            "compositeImage": {
                "id": "b64e7aa1-296b-43f0-823d-cfba1f9b0122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd3a60ef-75e6-47cd-b027-f985c11be93c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "509296f3-8614-4949-996f-9876063cf352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd3a60ef-75e6-47cd-b027-f985c11be93c",
                    "LayerId": "d37c70f4-d1d0-4dc4-8d36-431b4add9ce7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "d37c70f4-d1d0-4dc4-8d36-431b4add9ce7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a36adcb0-d9bb-4025-a287-1929cbc7e72d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}