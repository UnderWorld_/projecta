{
    "id": "0f4cd51d-ec05-4060-9214-9ee659a32b2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "idle_grown_gobrit1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 102,
    "bbox_left": 16,
    "bbox_right": 93,
    "bbox_top": 32,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "948aa710-f28c-4e79-9808-7c389a6c6a93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f4cd51d-ec05-4060-9214-9ee659a32b2b",
            "compositeImage": {
                "id": "e7e07c87-825a-4c7e-b034-899bd0423c60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "948aa710-f28c-4e79-9808-7c389a6c6a93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "088ca3de-93c5-40f1-bbb4-a875a3d46dfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "948aa710-f28c-4e79-9808-7c389a6c6a93",
                    "LayerId": "85e1d7f6-d592-49fe-8e33-892059f64576"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "85e1d7f6-d592-49fe-8e33-892059f64576",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f4cd51d-ec05-4060-9214-9ee659a32b2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 116,
    "xorig": 51,
    "yorig": 111
}