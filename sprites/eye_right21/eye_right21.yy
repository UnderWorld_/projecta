{
    "id": "9467716e-c751-42c5-b693-10a374a47c9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "005d9de7-3999-4cc1-a564-bed0236450e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9467716e-c751-42c5-b693-10a374a47c9f",
            "compositeImage": {
                "id": "a0986385-dcbf-4c4c-8103-b9942232d522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005d9de7-3999-4cc1-a564-bed0236450e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de5ae6a-11a7-4f53-b7c1-1a51a8baa6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005d9de7-3999-4cc1-a564-bed0236450e6",
                    "LayerId": "97e4d8d9-dac6-41b2-b2c7-884cfedc81b8"
                }
            ]
        },
        {
            "id": "11200701-f2d3-4f0c-b433-0ef834ff3e5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9467716e-c751-42c5-b693-10a374a47c9f",
            "compositeImage": {
                "id": "f92e4fe3-c2c2-4958-8213-632d055f2e0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11200701-f2d3-4f0c-b433-0ef834ff3e5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5775b18-0ad5-42ba-af5d-71ebffaae10c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11200701-f2d3-4f0c-b433-0ef834ff3e5c",
                    "LayerId": "97e4d8d9-dac6-41b2-b2c7-884cfedc81b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "97e4d8d9-dac6-41b2-b2c7-884cfedc81b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9467716e-c751-42c5-b693-10a374a47c9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}