{
    "id": "e6ecae04-67a3-4a3b-bf85-94f690ecd62c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_door_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 85,
    "bbox_right": 237,
    "bbox_top": 82,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dac0a412-03f6-4500-b60d-ddd027132a26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6ecae04-67a3-4a3b-bf85-94f690ecd62c",
            "compositeImage": {
                "id": "fdaa5f37-f048-43c2-a547-c3c464cc75b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dac0a412-03f6-4500-b60d-ddd027132a26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48dd1300-0822-4505-bcf7-6854359bb34b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dac0a412-03f6-4500-b60d-ddd027132a26",
                    "LayerId": "0a15bbdf-68d2-4c87-87bf-80a4666c9061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "0a15bbdf-68d2-4c87-87bf-80a4666c9061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6ecae04-67a3-4a3b-bf85-94f690ecd62c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 319
}