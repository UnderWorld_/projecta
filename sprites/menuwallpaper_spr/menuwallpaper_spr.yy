{
    "id": "58e727ba-a2eb-40c3-9735-f37be1af5ac2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menuwallpaper_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "282e3e19-b983-42bf-b1da-5a0c42138e78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58e727ba-a2eb-40c3-9735-f37be1af5ac2",
            "compositeImage": {
                "id": "669d8a42-0958-4368-a8d2-554629c8a3dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "282e3e19-b983-42bf-b1da-5a0c42138e78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21de1880-9db9-475f-9f83-1a3117b18330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "282e3e19-b983-42bf-b1da-5a0c42138e78",
                    "LayerId": "aa3bf727-87d5-4f52-a090-b3eb50aebc27"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "aa3bf727-87d5-4f52-a090-b3eb50aebc27",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58e727ba-a2eb-40c3-9735-f37be1af5ac2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 2236,
    "yorig": 649
}