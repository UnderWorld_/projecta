{
    "id": "03d5684b-0bb0-4733-a46c-7776c29f389f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "290f30d4-0298-4f26-a66f-f5eadb665370",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03d5684b-0bb0-4733-a46c-7776c29f389f",
            "compositeImage": {
                "id": "fb8288f9-691e-49a8-a3d6-af27271cef00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "290f30d4-0298-4f26-a66f-f5eadb665370",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "333b9977-cc39-4ea9-b72f-93f9b91b563d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "290f30d4-0298-4f26-a66f-f5eadb665370",
                    "LayerId": "0e8c393e-6053-4587-bb5f-2ff9694e4c21"
                }
            ]
        },
        {
            "id": "0f4e15a2-74c9-4df1-a6dd-ff8dcf2e9642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03d5684b-0bb0-4733-a46c-7776c29f389f",
            "compositeImage": {
                "id": "3056e071-e738-4826-bfa5-f2ffcf793da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f4e15a2-74c9-4df1-a6dd-ff8dcf2e9642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a0077e-8551-4881-96cf-8abeb411d959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f4e15a2-74c9-4df1-a6dd-ff8dcf2e9642",
                    "LayerId": "0e8c393e-6053-4587-bb5f-2ff9694e4c21"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "0e8c393e-6053-4587-bb5f-2ff9694e4c21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03d5684b-0bb0-4733-a46c-7776c29f389f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}