{
    "id": "dc4682fc-351f-4ca6-a7dd-384a3b7df8af",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardright5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "019361c8-5c14-4152-80bc-2b335e7ef0e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4682fc-351f-4ca6-a7dd-384a3b7df8af",
            "compositeImage": {
                "id": "77ef090c-33c8-46dc-8c3b-cda77b8ba130",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "019361c8-5c14-4152-80bc-2b335e7ef0e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c698aabc-f8a2-485c-a5eb-ca661702d6bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "019361c8-5c14-4152-80bc-2b335e7ef0e7",
                    "LayerId": "1b105265-84c4-4086-a8ef-5f989f2edf46"
                }
            ]
        },
        {
            "id": "62386a4c-f2ff-406e-ae0f-d411d36fee2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4682fc-351f-4ca6-a7dd-384a3b7df8af",
            "compositeImage": {
                "id": "81cc2b7b-7793-4a1e-a62d-b1acaaf38c72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62386a4c-f2ff-406e-ae0f-d411d36fee2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec637e21-8514-4fa2-ae06-d88fb84778d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62386a4c-f2ff-406e-ae0f-d411d36fee2b",
                    "LayerId": "1b105265-84c4-4086-a8ef-5f989f2edf46"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "1b105265-84c4-4086-a8ef-5f989f2edf46",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc4682fc-351f-4ca6-a7dd-384a3b7df8af",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}