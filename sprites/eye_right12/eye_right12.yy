{
    "id": "9f5d0100-3f0e-4fdb-bbf0-ab4bdc327427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "886df621-457c-42a9-92cb-bb7969746408",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5d0100-3f0e-4fdb-bbf0-ab4bdc327427",
            "compositeImage": {
                "id": "3a0acd45-c76b-46fa-a698-6d6e6517cbdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "886df621-457c-42a9-92cb-bb7969746408",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "636e4d0c-035c-4d3f-82ea-4a07899abcbd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "886df621-457c-42a9-92cb-bb7969746408",
                    "LayerId": "838bd67b-bb75-44c5-a774-879923c531ff"
                }
            ]
        },
        {
            "id": "f9c2171f-8eb5-4867-a57b-7a31caa6ac9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f5d0100-3f0e-4fdb-bbf0-ab4bdc327427",
            "compositeImage": {
                "id": "80516748-f41d-4fea-8df0-5effb03e6138",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9c2171f-8eb5-4867-a57b-7a31caa6ac9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "925e1dc1-9f1e-447e-b207-e1fe02c31929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9c2171f-8eb5-4867-a57b-7a31caa6ac9e",
                    "LayerId": "838bd67b-bb75-44c5-a774-879923c531ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "838bd67b-bb75-44c5-a774-879923c531ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f5d0100-3f0e-4fdb-bbf0-ab4bdc327427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}