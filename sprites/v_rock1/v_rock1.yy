{
    "id": "a1d24423-2bf7-4105-8b89-cc070412b44a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_rock1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 8,
    "bbox_right": 30,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "006695ed-802f-40e6-aa3d-df182a99d080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1d24423-2bf7-4105-8b89-cc070412b44a",
            "compositeImage": {
                "id": "6996a90c-7645-440d-ba24-dd172181751a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "006695ed-802f-40e6-aa3d-df182a99d080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b8de72-727c-45e8-85ce-28c97fdb2f21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "006695ed-802f-40e6-aa3d-df182a99d080",
                    "LayerId": "2159cac5-babf-4024-b62d-23fbd7da2916"
                }
            ]
        },
        {
            "id": "a9a08a8d-795c-493a-a248-9527906fc569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1d24423-2bf7-4105-8b89-cc070412b44a",
            "compositeImage": {
                "id": "1703d745-b60b-488c-8f25-71af396f7fb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9a08a8d-795c-493a-a248-9527906fc569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e13d6c4-005e-4e18-9b7b-a09fbd8f1019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9a08a8d-795c-493a-a248-9527906fc569",
                    "LayerId": "2159cac5-babf-4024-b62d-23fbd7da2916"
                }
            ]
        },
        {
            "id": "e310c3be-f7ac-43d8-97ad-039d6d64ce92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1d24423-2bf7-4105-8b89-cc070412b44a",
            "compositeImage": {
                "id": "aac3e11c-e807-4637-b458-a44b9a3d2e55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e310c3be-f7ac-43d8-97ad-039d6d64ce92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc814366-e535-485b-86f0-1083e5663d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e310c3be-f7ac-43d8-97ad-039d6d64ce92",
                    "LayerId": "2159cac5-babf-4024-b62d-23fbd7da2916"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "2159cac5-babf-4024-b62d-23fbd7da2916",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1d24423-2bf7-4105-8b89-cc070412b44a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 20
}