{
    "id": "398ce39f-6141-4750-9b22-c5f8a836f0b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdffb8bf-8d9c-46b8-acb0-23c4b0d7be5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "398ce39f-6141-4750-9b22-c5f8a836f0b0",
            "compositeImage": {
                "id": "5967b087-ba24-4820-9f8b-7fa754ea7ff2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdffb8bf-8d9c-46b8-acb0-23c4b0d7be5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0965592e-a585-465a-8a3f-1d346c4be880",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdffb8bf-8d9c-46b8-acb0-23c4b0d7be5d",
                    "LayerId": "a0632c93-d532-4748-a18d-045d38af73fb"
                }
            ]
        },
        {
            "id": "bc364a91-997e-4d15-b14f-f4ebf146944f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "398ce39f-6141-4750-9b22-c5f8a836f0b0",
            "compositeImage": {
                "id": "9c9ba4aa-6d5b-42a8-94d3-0c9cc65165e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc364a91-997e-4d15-b14f-f4ebf146944f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff5fa107-72a9-4896-99da-9ebdec30ac69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc364a91-997e-4d15-b14f-f4ebf146944f",
                    "LayerId": "a0632c93-d532-4748-a18d-045d38af73fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "a0632c93-d532-4748-a18d-045d38af73fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "398ce39f-6141-4750-9b22-c5f8a836f0b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}