{
    "id": "0f297820-ca37-4111-ad2c-fdc243b3e126",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "terrain4_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7761ce9a-1180-4387-acaa-1d5e2ca2b799",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f297820-ca37-4111-ad2c-fdc243b3e126",
            "compositeImage": {
                "id": "e356c687-d289-462f-bfab-d6411943b0c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7761ce9a-1180-4387-acaa-1d5e2ca2b799",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5494e36d-06ce-4101-b354-3b6ea7d45035",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7761ce9a-1180-4387-acaa-1d5e2ca2b799",
                    "LayerId": "7056d8d9-668f-431c-ab13-ac8e2da6eb6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7056d8d9-668f-431c-ab13-ac8e2da6eb6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f297820-ca37-4111-ad2c-fdc243b3e126",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}