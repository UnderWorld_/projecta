{
    "id": "c49111e8-97e3-4839-a4e5-a3dc65eccea7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin6_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64f21209-900e-404e-8491-f46388337859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c49111e8-97e3-4839-a4e5-a3dc65eccea7",
            "compositeImage": {
                "id": "21ec93e2-28a5-4f09-9e37-731a56745a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64f21209-900e-404e-8491-f46388337859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abc9b7d9-052a-432d-8116-9e9b8da63fcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64f21209-900e-404e-8491-f46388337859",
                    "LayerId": "d26636a8-01c1-42f0-ab5f-ab49328d1b95"
                }
            ]
        },
        {
            "id": "1e30b4ff-7336-4193-bc9c-80bf439eec30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c49111e8-97e3-4839-a4e5-a3dc65eccea7",
            "compositeImage": {
                "id": "45a84f7c-4618-4edc-bc4d-6e7e6e78f47b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e30b4ff-7336-4193-bc9c-80bf439eec30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9f336c9-6dec-45d1-bd23-a40201556bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e30b4ff-7336-4193-bc9c-80bf439eec30",
                    "LayerId": "d26636a8-01c1-42f0-ab5f-ab49328d1b95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "d26636a8-01c1-42f0-ab5f-ab49328d1b95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c49111e8-97e3-4839-a4e5-a3dc65eccea7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}