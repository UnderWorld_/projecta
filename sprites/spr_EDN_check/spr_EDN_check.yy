{
    "id": "c431a6c6-d532-412f-85eb-e1c00f6d5502",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EDN_check",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e7ac843-3eaf-45d9-9088-d8970168c36a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c431a6c6-d532-412f-85eb-e1c00f6d5502",
            "compositeImage": {
                "id": "682d035a-96e8-4646-bb0c-fd895ab99893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e7ac843-3eaf-45d9-9088-d8970168c36a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d98f97e-e54e-4265-96c5-1e3d8d40b348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e7ac843-3eaf-45d9-9088-d8970168c36a",
                    "LayerId": "e12cd51b-47b1-4096-a209-36d2ed9fb9f8"
                }
            ]
        },
        {
            "id": "2ab5f0e2-bd65-4003-8458-c48546817116",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c431a6c6-d532-412f-85eb-e1c00f6d5502",
            "compositeImage": {
                "id": "38b3dfdc-6947-47ef-ba7d-4b024a7ba75b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ab5f0e2-bd65-4003-8458-c48546817116",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93f624ce-0180-48e1-993a-652f844f39dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ab5f0e2-bd65-4003-8458-c48546817116",
                    "LayerId": "e12cd51b-47b1-4096-a209-36d2ed9fb9f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "e12cd51b-47b1-4096-a209-36d2ed9fb9f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c431a6c6-d532-412f-85eb-e1c00f6d5502",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}