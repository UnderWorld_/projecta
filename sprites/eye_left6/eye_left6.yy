{
    "id": "0ce5248d-92d2-4c5a-b974-28ad88c0fc41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41691345-a26f-4b22-88c4-1267192ebdd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce5248d-92d2-4c5a-b974-28ad88c0fc41",
            "compositeImage": {
                "id": "42e6b11f-22d8-40fa-abc4-e637feb86183",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41691345-a26f-4b22-88c4-1267192ebdd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "453401a3-de04-452c-be04-0ce3c5839e1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41691345-a26f-4b22-88c4-1267192ebdd0",
                    "LayerId": "4455c1bf-5582-404b-8c41-8b7447592f7c"
                }
            ]
        },
        {
            "id": "71d5e51f-f4c3-459a-9172-20634ce07627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ce5248d-92d2-4c5a-b974-28ad88c0fc41",
            "compositeImage": {
                "id": "722df1c4-13ac-4794-b996-1389a8ec4bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71d5e51f-f4c3-459a-9172-20634ce07627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e191fb9-1bf7-4036-ae00-ca73b8cb4ea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71d5e51f-f4c3-459a-9172-20634ce07627",
                    "LayerId": "4455c1bf-5582-404b-8c41-8b7447592f7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4455c1bf-5582-404b-8c41-8b7447592f7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ce5248d-92d2-4c5a-b974-28ad88c0fc41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}