{
    "id": "31cafd44-7aa3-4822-b277-0cea17d175df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin6_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4fa35163-3411-459e-a361-fbb29bfa46b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31cafd44-7aa3-4822-b277-0cea17d175df",
            "compositeImage": {
                "id": "7c9f1e1b-e13f-4f26-a0c7-874ba62dcc97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fa35163-3411-459e-a361-fbb29bfa46b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9c8d428-6856-4988-8792-3cd0cd30bf65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fa35163-3411-459e-a361-fbb29bfa46b9",
                    "LayerId": "76c0d880-6ac3-4d9d-bc9f-c95c3b98d36c"
                }
            ]
        },
        {
            "id": "2122509c-1604-491e-a2d6-b087c36d752e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31cafd44-7aa3-4822-b277-0cea17d175df",
            "compositeImage": {
                "id": "2e8a85eb-0d1e-49f5-a0ec-de84321bc244",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2122509c-1604-491e-a2d6-b087c36d752e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ec87786-7f4f-4415-9310-534b79e49309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2122509c-1604-491e-a2d6-b087c36d752e",
                    "LayerId": "76c0d880-6ac3-4d9d-bc9f-c95c3b98d36c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "76c0d880-6ac3-4d9d-bc9f-c95c3b98d36c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31cafd44-7aa3-4822-b277-0cea17d175df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}