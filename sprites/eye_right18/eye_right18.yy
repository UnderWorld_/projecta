{
    "id": "1d73ab2a-fd52-4f8b-bbc5-45b59f814beb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2e500e5-f091-4b7d-95dd-7da0a2d56403",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d73ab2a-fd52-4f8b-bbc5-45b59f814beb",
            "compositeImage": {
                "id": "18a4b20b-52f9-4a77-a3e8-93cd3370e176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e500e5-f091-4b7d-95dd-7da0a2d56403",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "329a8521-e3c8-4200-bf20-714cfee46eec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e500e5-f091-4b7d-95dd-7da0a2d56403",
                    "LayerId": "f05b5009-2694-4bf1-be47-6a38743d3b29"
                }
            ]
        },
        {
            "id": "dc18b4c0-0b04-4d64-930c-ca84d3e301f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d73ab2a-fd52-4f8b-bbc5-45b59f814beb",
            "compositeImage": {
                "id": "a2a93325-9f43-40ea-b865-c3939b9e4ebf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc18b4c0-0b04-4d64-930c-ca84d3e301f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25c13dde-21b2-4829-a951-51080970fd5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc18b4c0-0b04-4d64-930c-ca84d3e301f2",
                    "LayerId": "f05b5009-2694-4bf1-be47-6a38743d3b29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f05b5009-2694-4bf1-be47-6a38743d3b29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d73ab2a-fd52-4f8b-bbc5-45b59f814beb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}