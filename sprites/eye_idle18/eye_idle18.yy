{
    "id": "92102f93-eeba-485c-9d93-f0a0f3300a79",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a26aa5c-50d6-4a1a-8283-88b5803c05a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92102f93-eeba-485c-9d93-f0a0f3300a79",
            "compositeImage": {
                "id": "bd230a2f-7540-4ba4-89ce-ca813e504116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a26aa5c-50d6-4a1a-8283-88b5803c05a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58741649-85b8-4081-b082-489622edbf6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a26aa5c-50d6-4a1a-8283-88b5803c05a3",
                    "LayerId": "4d0501ec-83eb-409c-98cf-585762b7b4ad"
                }
            ]
        },
        {
            "id": "550516e5-e828-415f-b2f9-189bac112be9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92102f93-eeba-485c-9d93-f0a0f3300a79",
            "compositeImage": {
                "id": "025a361a-cfb2-46b1-888c-79969fefdb44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "550516e5-e828-415f-b2f9-189bac112be9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22bd34b3-6c71-4bc9-adac-b17c5627717b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "550516e5-e828-415f-b2f9-189bac112be9",
                    "LayerId": "4d0501ec-83eb-409c-98cf-585762b7b4ad"
                }
            ]
        },
        {
            "id": "b11c6176-72d9-4be5-936a-a58c3fcea271",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92102f93-eeba-485c-9d93-f0a0f3300a79",
            "compositeImage": {
                "id": "a8b30235-017d-4040-bdf0-e5dbe11a8265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b11c6176-72d9-4be5-936a-a58c3fcea271",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5cfdea2-ab91-4d29-8763-5484c876dd5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b11c6176-72d9-4be5-936a-a58c3fcea271",
                    "LayerId": "4d0501ec-83eb-409c-98cf-585762b7b4ad"
                }
            ]
        },
        {
            "id": "bef1e33e-cb71-4ce7-9dba-bd74cc27149b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92102f93-eeba-485c-9d93-f0a0f3300a79",
            "compositeImage": {
                "id": "9360759f-e33d-4806-babd-c192d7ac3a29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bef1e33e-cb71-4ce7-9dba-bd74cc27149b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b49b25-b08c-4807-a5f9-f1c7fc529bec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bef1e33e-cb71-4ce7-9dba-bd74cc27149b",
                    "LayerId": "4d0501ec-83eb-409c-98cf-585762b7b4ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4d0501ec-83eb-409c-98cf-585762b7b4ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92102f93-eeba-485c-9d93-f0a0f3300a79",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}