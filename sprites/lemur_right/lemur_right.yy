{
    "id": "b0f6f0ee-5e4c-49f9-9a47-fe7df49c014e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lemur_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e585f848-54e9-4884-a77d-eb0350ebe298",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0f6f0ee-5e4c-49f9-9a47-fe7df49c014e",
            "compositeImage": {
                "id": "c4922bc7-9b37-4b28-a4f2-f014d81d6d2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e585f848-54e9-4884-a77d-eb0350ebe298",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19acdce9-a806-49b9-a428-42f5c93fb9f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e585f848-54e9-4884-a77d-eb0350ebe298",
                    "LayerId": "071b3c66-0d0c-4584-9e40-e11556a26bf5"
                }
            ]
        },
        {
            "id": "1f767fbc-1ab2-4ce9-a901-38f90e001128",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0f6f0ee-5e4c-49f9-9a47-fe7df49c014e",
            "compositeImage": {
                "id": "c9f1db04-3cef-4516-98b2-da58bc623d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f767fbc-1ab2-4ce9-a901-38f90e001128",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52426008-8fc2-49e3-9c1d-ae7c7cf40fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f767fbc-1ab2-4ce9-a901-38f90e001128",
                    "LayerId": "071b3c66-0d0c-4584-9e40-e11556a26bf5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "071b3c66-0d0c-4584-9e40-e11556a26bf5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0f6f0ee-5e4c-49f9-9a47-fe7df49c014e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 36
}