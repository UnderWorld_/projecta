{
    "id": "189e01f8-be5a-4d0f-a816-928fe717a725",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_wall_corner",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 78,
    "bbox_top": 52,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cafabfc-6a2e-4e41-9ac2-8db2ce4b9e70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "8b48f297-8a49-4028-87d6-8686f446ff6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cafabfc-6a2e-4e41-9ac2-8db2ce4b9e70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d708ef3f-77c7-4ef3-b630-0134e2320cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cafabfc-6a2e-4e41-9ac2-8db2ce4b9e70",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "639bba77-6599-48a0-a0bc-154ca00e1c2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "1327dbaf-7db2-4fec-b2b7-8d46aa3c5077",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "639bba77-6599-48a0-a0bc-154ca00e1c2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5e0fe5f-b48a-4697-8f59-3c05ee9d5da1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "639bba77-6599-48a0-a0bc-154ca00e1c2c",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7ce2e8f7-31ff-45f7-9ab6-eb786c502218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "11d0fc9d-2db9-4bc9-8a22-d22f30f66d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce2e8f7-31ff-45f7-9ab6-eb786c502218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a656af67-2c59-4741-9671-3b48c9075a99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce2e8f7-31ff-45f7-9ab6-eb786c502218",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "3e0a23e7-445b-4733-8022-1eb465a4865b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "5aa2b5f0-908c-4479-b1c6-dd97b369dae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e0a23e7-445b-4733-8022-1eb465a4865b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d58116db-3be1-4c17-8448-c22675348a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e0a23e7-445b-4733-8022-1eb465a4865b",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "a02d350b-5313-4be8-b060-998066a4cf91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "0becade3-4f07-4af5-8564-cdd522f6f991",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a02d350b-5313-4be8-b060-998066a4cf91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b312e996-d32b-4e2c-9534-5e7a965ebc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a02d350b-5313-4be8-b060-998066a4cf91",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "8b5da2b9-362e-45fa-99db-e72479ca4cb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "10c08a4f-5d5b-41b8-9ec6-6d194c7c0eb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b5da2b9-362e-45fa-99db-e72479ca4cb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7843bbf1-4504-4c3e-bfc1-bf8a4706231b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b5da2b9-362e-45fa-99db-e72479ca4cb3",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "a3d87abb-67d9-4b3f-8f97-4f2b26408b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "1f2eb50f-c2eb-47e0-b7f2-d81aab3eefe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3d87abb-67d9-4b3f-8f97-4f2b26408b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80d79bb-ee7f-4e6c-b6cc-aeac8beee5e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3d87abb-67d9-4b3f-8f97-4f2b26408b68",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "444b9074-464d-4e1e-91b6-d0ace42635e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "7f75ac6f-1bc8-45f0-bbc4-adde95f5afca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444b9074-464d-4e1e-91b6-d0ace42635e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a509c69-7e3a-4360-9377-a5fc915b4f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444b9074-464d-4e1e-91b6-d0ace42635e5",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "728114c9-b65a-4bbf-9f60-d83a512ab787",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "4271b4e6-3cce-43ec-a0c4-c73822744c5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "728114c9-b65a-4bbf-9f60-d83a512ab787",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fea9ec6-2239-4592-aa8b-0afbc0ce01bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "728114c9-b65a-4bbf-9f60-d83a512ab787",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "29f71840-68ce-4907-9fc0-70fc736c79f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "9ec1f670-54e5-4dce-91b7-ffd1f8c55712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29f71840-68ce-4907-9fc0-70fc736c79f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "017216a9-6149-4116-a1b3-43851afa770c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29f71840-68ce-4907-9fc0-70fc736c79f8",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f4a1982a-87c8-4c04-963f-558995ea049f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e3089547-4665-4507-b174-339006f103d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4a1982a-87c8-4c04-963f-558995ea049f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b0131a4-4a86-47c0-9a4e-0626737e93a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4a1982a-87c8-4c04-963f-558995ea049f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ac6620ea-bda7-40f7-aff9-0fec30cad930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "9f7daaf9-5f17-456c-9bd5-0748eec213ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac6620ea-bda7-40f7-aff9-0fec30cad930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6aa3b2d1-2051-4690-90bd-804394d7ec8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac6620ea-bda7-40f7-aff9-0fec30cad930",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ea99ab82-2b5c-48e9-8d94-7ac330f30157",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "0ef50710-fecf-4621-bdaf-db28fd5db07d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea99ab82-2b5c-48e9-8d94-7ac330f30157",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ac6de8a-bbe7-4e39-aa36-bf3d3942e254",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea99ab82-2b5c-48e9-8d94-7ac330f30157",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "32e2acbc-239c-437f-a18c-9e11a18788e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e0827a78-a1cc-401b-843c-d2867015fcf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e2acbc-239c-437f-a18c-9e11a18788e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5037c762-8971-4701-b910-e924836f0e98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e2acbc-239c-437f-a18c-9e11a18788e2",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e0ea5f25-8d56-467d-8904-fd578ae0794f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "294b5cde-6726-4bdb-a821-20eaf42e710f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0ea5f25-8d56-467d-8904-fd578ae0794f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a1922ef-4460-4f45-86d6-b3a400491c81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0ea5f25-8d56-467d-8904-fd578ae0794f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "c298ed3a-d038-4624-94cf-08ab44a356c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "9db65fdf-a39f-4003-9fb2-7ab10db94eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c298ed3a-d038-4624-94cf-08ab44a356c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dc9624a-1a5f-4afd-a7e3-259161072e9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c298ed3a-d038-4624-94cf-08ab44a356c8",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "cfb5e48e-cdbc-47e4-b700-f0d876cd4d2a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "47f00a30-7c18-46be-8166-f97e3e6782f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb5e48e-cdbc-47e4-b700-f0d876cd4d2a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6291bfdc-f688-4940-8859-37704e57842c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb5e48e-cdbc-47e4-b700-f0d876cd4d2a",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "c4a24796-0fd8-4062-b043-1755e9dda3ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "7a8675a3-0e24-47cf-b4e5-86080957cd3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4a24796-0fd8-4062-b043-1755e9dda3ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7a8df37-9183-47cf-9ece-fd1dddb24670",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4a24796-0fd8-4062-b043-1755e9dda3ec",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "74f46f9b-943e-4cb3-ac83-9fa3001446f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e9425e76-0b62-40bc-8ff9-4c0842269936",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74f46f9b-943e-4cb3-ac83-9fa3001446f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd49646c-f0a3-4e59-95b1-5a4a9b8e7a28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74f46f9b-943e-4cb3-ac83-9fa3001446f5",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ab553ba4-3271-4350-8914-237c15bf05ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "5b26f6e6-a869-49d0-9ec8-ee7c821abb8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab553ba4-3271-4350-8914-237c15bf05ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2aa9d01a-b573-405b-bda2-4d0aca2a07df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab553ba4-3271-4350-8914-237c15bf05ea",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "b354927f-5727-4eee-96bc-101b5d7c456b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "419d0b01-4ca9-44d4-b7fa-932d6f79fa19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b354927f-5727-4eee-96bc-101b5d7c456b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8cedcab-d03a-45aa-ae85-0979f8b2bb4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b354927f-5727-4eee-96bc-101b5d7c456b",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "b14b04b2-0f59-478f-a77e-a316e2a96fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "6671179a-661d-4811-87ba-80d37c4183b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b14b04b2-0f59-478f-a77e-a316e2a96fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b653359-f385-467a-89bf-3b83c31821f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b14b04b2-0f59-478f-a77e-a316e2a96fde",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "38736cf5-ee24-446d-a447-c6e8f9312d67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "bca6817d-0aa0-4472-a4b1-e365b5532a8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38736cf5-ee24-446d-a447-c6e8f9312d67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22a53f77-4f34-4122-a1c5-73bd37448e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38736cf5-ee24-446d-a447-c6e8f9312d67",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "255cc56c-2825-407f-b61a-875b739167a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "fce6e1bc-f97e-4d60-b1b7-2fbd55751dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "255cc56c-2825-407f-b61a-875b739167a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95859d7f-f6db-4232-9671-48e3747a77ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "255cc56c-2825-407f-b61a-875b739167a8",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "55e4d173-749c-4236-b300-d38ae808f9b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "5f03f097-4d6f-49cc-8bb6-d6e5c15386fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55e4d173-749c-4236-b300-d38ae808f9b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f240cdd9-1294-40e4-8420-97c193999028",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55e4d173-749c-4236-b300-d38ae808f9b8",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "42e1fdfb-f092-41bf-b695-06c9df0235e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "32375293-b5b9-457c-b73c-37a1f1f6ef2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42e1fdfb-f092-41bf-b695-06c9df0235e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2339908c-ef0f-4778-b5e9-203953bb0ee7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42e1fdfb-f092-41bf-b695-06c9df0235e0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e3e00c90-83ce-4fc2-b43c-0dd8613f97e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "40bdac11-dd94-4af8-844e-aee0ba828591",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3e00c90-83ce-4fc2-b43c-0dd8613f97e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2271c8d-3851-42b1-9df9-505687869952",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3e00c90-83ce-4fc2-b43c-0dd8613f97e0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "11e93e18-9e26-43be-a3c2-4d4272b980a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "0bcd69ba-8300-4439-9379-e5d6f665e3ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11e93e18-9e26-43be-a3c2-4d4272b980a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b02f53e-d738-4f93-9df0-d151325ef810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11e93e18-9e26-43be-a3c2-4d4272b980a8",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "2dbc08c1-8b9e-4e37-8f1e-d3462a001f55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "8f4fc764-3416-4af5-ba89-4415bbe7c0dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2dbc08c1-8b9e-4e37-8f1e-d3462a001f55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96738e68-84f8-401e-8ad5-5a79d1adc17f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2dbc08c1-8b9e-4e37-8f1e-d3462a001f55",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "2e3ee29e-3040-4227-ace2-4a4b090e194e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a562ed96-4ebb-4f2d-8413-4e8b324c667f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e3ee29e-3040-4227-ace2-4a4b090e194e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "becd7bd8-b940-47eb-89b3-0e55833dc940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e3ee29e-3040-4227-ace2-4a4b090e194e",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7cf7faa6-2190-47f0-a0d3-b204793390c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a37ce67b-60e8-425e-8fa0-9bf8fd1601e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf7faa6-2190-47f0-a0d3-b204793390c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "964629e0-5f82-4ec2-b653-b1d6912bba6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf7faa6-2190-47f0-a0d3-b204793390c3",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "a553c05c-8116-4e27-bf12-9073dd58bded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "f6ab6af2-aa37-42c1-8e62-d54e4a2de268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a553c05c-8116-4e27-bf12-9073dd58bded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa88b3d7-78d3-4fd1-bbf9-cb9cb613a580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a553c05c-8116-4e27-bf12-9073dd58bded",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ac4929c1-bfe4-47fa-a641-ea4136839c66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "2aaccf63-7e38-4792-8401-99a87d6b1362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4929c1-bfe4-47fa-a641-ea4136839c66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b4c31f1-782e-420b-8dba-04b1313acd85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4929c1-bfe4-47fa-a641-ea4136839c66",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7fb810f2-e043-4e29-9914-1892db4e5c94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a56b9cfd-4253-4133-9176-87c79a3ee846",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb810f2-e043-4e29-9914-1892db4e5c94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f42491b6-4d8f-488e-bd38-3607987e7416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb810f2-e043-4e29-9914-1892db4e5c94",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "6c6f3916-fbed-46df-8eac-34dde71e816f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "c6c85855-0494-4a8c-8f03-a09ac9812fcf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c6f3916-fbed-46df-8eac-34dde71e816f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cb67406-78b1-4496-9a2c-f179b05ea6f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c6f3916-fbed-46df-8eac-34dde71e816f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "026b8189-e8dc-4792-b067-c544de563217",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "799c9c87-7c9a-477e-b95a-78fae14b46cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "026b8189-e8dc-4792-b067-c544de563217",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2faf56c-6cb5-4d01-b696-f85c02ff346f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "026b8189-e8dc-4792-b067-c544de563217",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7caded81-8344-49cf-b148-92a03ea2251a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "3869035d-8e69-41d7-8a85-95be1cf11f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7caded81-8344-49cf-b148-92a03ea2251a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da8af60-2fe2-499f-9621-a3683ecc7260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7caded81-8344-49cf-b148-92a03ea2251a",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ae86addd-30ab-453e-91ed-9ee685dacd89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "8b45df59-5c9b-4425-a8e8-6fce189168eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae86addd-30ab-453e-91ed-9ee685dacd89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2553e7dd-6c43-4c01-9a5e-d544712abf5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae86addd-30ab-453e-91ed-9ee685dacd89",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "fe8a973d-07dc-4c04-a5ff-3c777af5a9cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a75f9b35-ba4b-4a61-860c-3689e5c6135f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe8a973d-07dc-4c04-a5ff-3c777af5a9cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e0eae0-f35a-4a62-9194-a6adf298ec92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe8a973d-07dc-4c04-a5ff-3c777af5a9cc",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e756ea44-6a33-462c-85c5-95ff77d02950",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "8bbc9ea5-1ba0-46bd-862c-e32b64d0d1ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e756ea44-6a33-462c-85c5-95ff77d02950",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52dbe339-1656-4e87-9c68-a03e645dc426",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e756ea44-6a33-462c-85c5-95ff77d02950",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "4c476bcd-a07a-4de0-80ff-16e3016584a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "9b67e7cd-6bc8-4139-9d8e-4015725648cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c476bcd-a07a-4de0-80ff-16e3016584a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42627be8-388b-4cac-b742-978b7d186273",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c476bcd-a07a-4de0-80ff-16e3016584a1",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e0974aa2-528d-459d-b177-03c292c54e06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "fc543222-ff18-4058-9038-52f4825efbf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0974aa2-528d-459d-b177-03c292c54e06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88a09634-c469-472f-b427-76026db4c684",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0974aa2-528d-459d-b177-03c292c54e06",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "20be7004-769f-405c-8ccf-1ffa9e4c399b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e9c17f82-d7d1-4d35-ae57-65b9dd9065ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20be7004-769f-405c-8ccf-1ffa9e4c399b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0521dda4-fd56-4929-a11b-650d23eab8ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20be7004-769f-405c-8ccf-1ffa9e4c399b",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "902828fb-41ec-47d9-b61e-5012ca853c1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "1d11f382-4921-4cf4-adca-09c38bd164e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "902828fb-41ec-47d9-b61e-5012ca853c1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c027bd0-4acc-4ca8-a611-ec07b1552bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "902828fb-41ec-47d9-b61e-5012ca853c1b",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "efe170cd-ef4e-4f43-be4e-a05140c0ee22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "11386f06-7af9-41b5-beab-b478ff766a5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efe170cd-ef4e-4f43-be4e-a05140c0ee22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee687992-953a-4cd5-b004-e81e4350a617",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efe170cd-ef4e-4f43-be4e-a05140c0ee22",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f37279fa-1cd3-477a-a8a9-e4fa5cae201d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "b262059b-c4a3-49b2-a299-bb3b224b370c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f37279fa-1cd3-477a-a8a9-e4fa5cae201d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b94197f7-fc00-43bc-a931-5ab2aa9ddd94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f37279fa-1cd3-477a-a8a9-e4fa5cae201d",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "6b3547ff-17e8-442b-92a7-4f7b6865bbd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "4f5ed533-3303-4ad6-9455-537d07fd5154",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b3547ff-17e8-442b-92a7-4f7b6865bbd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efd77298-ee4c-4de8-af68-32e16f958807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b3547ff-17e8-442b-92a7-4f7b6865bbd9",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "940cc3f7-ecc1-44df-807b-323727f2c754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ab6ded5b-e916-498f-bad4-2a6cadb7875c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "940cc3f7-ecc1-44df-807b-323727f2c754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc0ebfec-5b1a-482a-84d0-084c2cef7767",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "940cc3f7-ecc1-44df-807b-323727f2c754",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "85f2668a-6e05-4cf1-8251-bbe8edc88ded",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a53e0158-8b66-45d1-9a1f-b888741f5795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85f2668a-6e05-4cf1-8251-bbe8edc88ded",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b39e56a7-783d-4a4a-9b55-db193b8608df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85f2668a-6e05-4cf1-8251-bbe8edc88ded",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e3f02495-0230-4fcf-94ea-6ee84ccb9144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "af68ad6d-1f68-4938-b2c5-6ffdc9c41c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3f02495-0230-4fcf-94ea-6ee84ccb9144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "985c3c18-e779-4ee3-a74d-eb2e7115513b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3f02495-0230-4fcf-94ea-6ee84ccb9144",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "011c3dbf-d224-48a8-95e0-6af7a53b68a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "2ce7a4e8-68d8-4b87-9879-3fba4911f6c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "011c3dbf-d224-48a8-95e0-6af7a53b68a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b98962bc-7553-42ea-b59d-05d2ae0be2d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "011c3dbf-d224-48a8-95e0-6af7a53b68a1",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "04ab8de3-f579-4499-8f1b-1933f041820d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "6d353434-1185-4a4d-b624-c7c7e1d3e176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04ab8de3-f579-4499-8f1b-1933f041820d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72711931-7be2-43c8-86d7-49126257399b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04ab8de3-f579-4499-8f1b-1933f041820d",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "da18f515-4595-4fd1-9bb6-6af73a94c287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a5612cba-83ad-4bfe-8ccf-6a7a45180deb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da18f515-4595-4fd1-9bb6-6af73a94c287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7342c178-6fca-4ad9-8d19-6fcb4416a442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da18f515-4595-4fd1-9bb6-6af73a94c287",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "c1970b0a-1c11-4037-9c27-61b9f0124145",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "52ca2bc9-bb35-4acf-8936-6efc59a41eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1970b0a-1c11-4037-9c27-61b9f0124145",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3daf2d8-513d-4df6-953d-c2f2a5ed6613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1970b0a-1c11-4037-9c27-61b9f0124145",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "4bb0c009-fd55-4f8e-8f74-6ef9de428ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "48f971a3-a789-4679-af0f-98c14b646aed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bb0c009-fd55-4f8e-8f74-6ef9de428ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7731a84-b70d-47cd-b739-6f16ce8fb9eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bb0c009-fd55-4f8e-8f74-6ef9de428ade",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "1e8288c8-4736-48ce-94c4-74be3248d2b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "c765266f-790c-490b-baf4-58e195cd4c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1e8288c8-4736-48ce-94c4-74be3248d2b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dedbe60-1867-48e0-b78c-fcbac951bc74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1e8288c8-4736-48ce-94c4-74be3248d2b6",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7866aa58-6270-4c37-96ab-6fa134c12b0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "24f57601-ae3f-47cb-b1a9-cd0f57e9b73d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7866aa58-6270-4c37-96ab-6fa134c12b0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ffc16b8-0f42-417c-b52a-cbea52d7b219",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7866aa58-6270-4c37-96ab-6fa134c12b0d",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "0691ac80-5e63-4f19-b491-2e349725238c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "36d03396-48de-4cb0-87c0-f90eccc5b9a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0691ac80-5e63-4f19-b491-2e349725238c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70c408e4-eb67-4b26-99ea-d16cf1b49c14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0691ac80-5e63-4f19-b491-2e349725238c",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "25447293-744e-4003-bf71-fa7344edd1c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a6336b52-c4ff-4b70-a5e6-5c42b29ff379",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25447293-744e-4003-bf71-fa7344edd1c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0827c156-a05d-47db-94ad-80a3c4754e04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25447293-744e-4003-bf71-fa7344edd1c0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "2ee5885c-6f0f-458c-88dc-e03dabae9b67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e96e7662-7c22-45c6-8674-1f0aacdf8887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee5885c-6f0f-458c-88dc-e03dabae9b67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f270f001-11f7-4991-9297-81078745e164",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee5885c-6f0f-458c-88dc-e03dabae9b67",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "b76ee91f-f022-442a-a1e6-506fc5e08f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "16294b59-9772-413f-a17b-4a31be31fefd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b76ee91f-f022-442a-a1e6-506fc5e08f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f36c6e06-74f1-42b4-8ab2-08d198a4a22b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b76ee91f-f022-442a-a1e6-506fc5e08f47",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "640714ea-bdb3-4630-b038-7fba364483cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "12a1ed04-7d0b-413e-8baa-210437230775",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "640714ea-bdb3-4630-b038-7fba364483cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a26d76e1-22cf-478d-b19e-7ac8ecb07891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "640714ea-bdb3-4630-b038-7fba364483cc",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f22260b6-4ac7-434d-aea1-ac2d1dfc7d63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "c85c672b-9930-4197-b5e1-2573d390f98f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f22260b6-4ac7-434d-aea1-ac2d1dfc7d63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "541bd7e5-523b-470f-a0b4-c9a54df9a1aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f22260b6-4ac7-434d-aea1-ac2d1dfc7d63",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "bc9b67b3-e56b-4fb5-8335-747c4b333ab5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "3baba94f-d849-48b4-b7a3-5942bbc5a3f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc9b67b3-e56b-4fb5-8335-747c4b333ab5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e4bf5d0-8970-433a-b400-f5749c619946",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc9b67b3-e56b-4fb5-8335-747c4b333ab5",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "2996c019-9a39-49e3-890d-d219f09d4338",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "bc343c30-1412-41f7-b542-55c1503a6fb6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2996c019-9a39-49e3-890d-d219f09d4338",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "581fc92e-7dd3-44c4-80df-81ff7b311568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2996c019-9a39-49e3-890d-d219f09d4338",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e6f3ae7a-fc61-441c-a1a3-ecb02e150cf4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "2480232a-6801-44b9-92e2-94955f8d313f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6f3ae7a-fc61-441c-a1a3-ecb02e150cf4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83b3a1ae-b6a1-4f21-970d-8a2a7ffd3400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6f3ae7a-fc61-441c-a1a3-ecb02e150cf4",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "c266486f-a5f1-4697-be03-4f905cd673ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "919826e4-7bdd-43d5-bed6-2e4f238d8372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c266486f-a5f1-4697-be03-4f905cd673ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9581aea0-63e3-47c8-89fb-2538bb9ed49e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c266486f-a5f1-4697-be03-4f905cd673ca",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "d3ebe881-1b31-4af5-93df-7d78b275eddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "29c6b3eb-07aa-49f4-9d66-87a41a7efadc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3ebe881-1b31-4af5-93df-7d78b275eddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96d3670f-dfb2-465e-872d-43e7eb2afcf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3ebe881-1b31-4af5-93df-7d78b275eddb",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "3a3ed067-b1c8-4cbd-b569-e4af61cac887",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "3f0bde99-4cc3-423b-b4a8-534499770acc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a3ed067-b1c8-4cbd-b569-e4af61cac887",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "337fbcd3-1848-4f69-a724-ef71fc3289e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a3ed067-b1c8-4cbd-b569-e4af61cac887",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "1fdc13c9-b8c4-4239-b4e1-89be03c6046e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "1605d083-40aa-4dda-986c-4568b126cde7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fdc13c9-b8c4-4239-b4e1-89be03c6046e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53f225ce-4c70-4cc0-b992-ebe088a8a71e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fdc13c9-b8c4-4239-b4e1-89be03c6046e",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "96ad313d-265b-4d73-9b09-1c538d3da1b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a352197f-54e8-484b-9da9-8051450a2c5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96ad313d-265b-4d73-9b09-1c538d3da1b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da1b86f8-4f36-40da-a31b-9af989a15296",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96ad313d-265b-4d73-9b09-1c538d3da1b3",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ea6b1b0d-1783-4312-8b21-7500eb40e82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "3e381cbf-4f56-4ac1-b5b2-df2fdfb41ce8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6b1b0d-1783-4312-8b21-7500eb40e82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70efab9b-bca8-4364-b289-6f54b8a26604",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6b1b0d-1783-4312-8b21-7500eb40e82c",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "67db57ac-78a6-4dfd-a2a5-881cda5200c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "959606f4-3844-4d8b-9a52-b11be0fb22c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67db57ac-78a6-4dfd-a2a5-881cda5200c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a963de7-1977-44d6-b4b7-482e3d975bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67db57ac-78a6-4dfd-a2a5-881cda5200c0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "9d1aa87b-9e14-4869-afd1-1c64cf35e47e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "9a37b71c-1fda-4b58-8154-c33b89be7da4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d1aa87b-9e14-4869-afd1-1c64cf35e47e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8d0b4cf-8788-492c-9b6b-fc2e185eafe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d1aa87b-9e14-4869-afd1-1c64cf35e47e",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "aad2778e-5963-403d-b7e1-d6d6a2e1a15d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "dd05dc38-56ae-4451-ab69-b4ec8eadf0a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aad2778e-5963-403d-b7e1-d6d6a2e1a15d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa6e2eb0-2ec8-4d79-87ac-120693b80279",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aad2778e-5963-403d-b7e1-d6d6a2e1a15d",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "8413a6c7-eb45-438f-b022-bce768cc19fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e9dc4dc3-2a69-4698-aa15-32695555330a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8413a6c7-eb45-438f-b022-bce768cc19fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d95ebe-43b8-43bd-bcfe-f6f37664bbbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8413a6c7-eb45-438f-b022-bce768cc19fb",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ca03e5e9-c44e-4308-9671-cda450b587f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e9475984-6853-430c-9409-1be460e2c015",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca03e5e9-c44e-4308-9671-cda450b587f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9b347a56-f729-4eaa-9fb0-f4e29352b3a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca03e5e9-c44e-4308-9671-cda450b587f0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "074e9249-cb85-4617-b560-82709c19f404",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ed0e32d3-6172-47b6-8e94-cc2edc3d271b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074e9249-cb85-4617-b560-82709c19f404",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c0e6b7-0cd2-4525-b851-e50f504ead27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074e9249-cb85-4617-b560-82709c19f404",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "08d316b0-8fe1-4023-94f8-8fb684a7378f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ce7dae2b-3aca-414f-915c-b08c76a70ea9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08d316b0-8fe1-4023-94f8-8fb684a7378f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8978385-565d-4489-8c5b-0f19cabdb954",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08d316b0-8fe1-4023-94f8-8fb684a7378f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "4ce70067-8b5f-425a-ac87-4e606874c6de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ce00b27a-2952-48a4-8fcf-6d76fbf50348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ce70067-8b5f-425a-ac87-4e606874c6de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f65b4a4f-d6d9-4b4a-94fb-d0efb596853e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ce70067-8b5f-425a-ac87-4e606874c6de",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e40c77b6-f958-4dba-875d-0061eee50a63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "19c79f42-ee96-478f-b737-d2cedafe1367",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e40c77b6-f958-4dba-875d-0061eee50a63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae084832-b430-4ed7-ac1c-421d8cc16866",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e40c77b6-f958-4dba-875d-0061eee50a63",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "d20969b9-cfa5-40d5-959a-4a6583e51245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a1dc09e5-3173-4646-81d6-c39198eb30c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d20969b9-cfa5-40d5-959a-4a6583e51245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74e6b7ae-055d-470b-a4fe-cfa6f8393f3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d20969b9-cfa5-40d5-959a-4a6583e51245",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7d96dfba-a1b0-48d7-b925-378bcd48529f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "abaf231d-dba0-4024-a29b-003c3f288597",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d96dfba-a1b0-48d7-b925-378bcd48529f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2edd2a4-64d7-4db8-b124-5c948e1b8910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d96dfba-a1b0-48d7-b925-378bcd48529f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "6dfd56d8-3279-4872-b20e-7338336a16da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e1a6d644-49eb-446a-a0b9-0b3c72a6696a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6dfd56d8-3279-4872-b20e-7338336a16da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a4a19dd-a9a2-41ab-8628-254a2733895b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6dfd56d8-3279-4872-b20e-7338336a16da",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "62ed7a31-a0fd-4801-bba8-26b5f464e59d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ce8cecbf-e83b-4b80-b859-a2a5230fb2f2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62ed7a31-a0fd-4801-bba8-26b5f464e59d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1501991d-45ba-49ae-81fb-88ec586515b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62ed7a31-a0fd-4801-bba8-26b5f464e59d",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "67cc3f3b-914a-45d7-8fec-715eee8d6761",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "24a51c85-dbfc-421e-849c-bc7136e6e5f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67cc3f3b-914a-45d7-8fec-715eee8d6761",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d52e495-474e-4655-94f2-cdc1acc14b01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67cc3f3b-914a-45d7-8fec-715eee8d6761",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "9bb2b75f-65d0-485b-a9e6-b1e8ed7ac889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "c7970e26-b1e0-4dcd-b89e-79d6edf095ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9bb2b75f-65d0-485b-a9e6-b1e8ed7ac889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a0e24a9-30a5-4414-ad3b-abc895b2d39f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9bb2b75f-65d0-485b-a9e6-b1e8ed7ac889",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "4f5de437-306f-467d-9886-655782ffd688",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "05df5462-5010-46c2-9fa9-fa8f500b1ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f5de437-306f-467d-9886-655782ffd688",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eed79742-39d1-46d5-a92d-04bedc585547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f5de437-306f-467d-9886-655782ffd688",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "d18067d6-6b9c-4a17-a40a-fd2ec4fcf685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "0a746a77-092c-47bd-a217-650d62fe5ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18067d6-6b9c-4a17-a40a-fd2ec4fcf685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7633e16-296e-4c16-a0e5-f38c240e0210",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18067d6-6b9c-4a17-a40a-fd2ec4fcf685",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "0a32b345-b4d9-4a35-8865-e778fe55ca0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "821fbd88-9abc-4aba-a98e-30f0088bea3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a32b345-b4d9-4a35-8865-e778fe55ca0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d90e51-26c8-4df6-9505-0244329c1552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a32b345-b4d9-4a35-8865-e778fe55ca0e",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "ccac2643-f3b7-4ac8-a236-c3f04a8f24f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "df4c64c1-2ace-4578-b8d2-94c397c48d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccac2643-f3b7-4ac8-a236-c3f04a8f24f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21ad0218-f558-4554-a613-ef28ce126c5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccac2643-f3b7-4ac8-a236-c3f04a8f24f4",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "0fa0aa5e-db39-4c88-a761-53e0d30bf16f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "03e728d4-20db-4949-9257-ded4182979eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fa0aa5e-db39-4c88-a761-53e0d30bf16f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0addbffe-c5aa-4bc6-9cc5-9bf4f92535ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fa0aa5e-db39-4c88-a761-53e0d30bf16f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "d382e7f6-78b6-44ce-9a29-126fa3cc8689",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "c784d588-9609-49d4-9528-09f51aa297a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d382e7f6-78b6-44ce-9a29-126fa3cc8689",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd213756-47b1-4ac9-92ce-117a35c47e9c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d382e7f6-78b6-44ce-9a29-126fa3cc8689",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "966e783b-0d31-4f0d-8a7c-0c520fc13da6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e4fbae7b-d4d3-4d38-a055-b3bce2a66fe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "966e783b-0d31-4f0d-8a7c-0c520fc13da6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7483005e-6138-43ec-830f-ceaf21690d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "966e783b-0d31-4f0d-8a7c-0c520fc13da6",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "241c1f63-b275-485a-bde9-e872b58e639c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "cb5e9b84-1889-44cb-b06b-557560087947",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "241c1f63-b275-485a-bde9-e872b58e639c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abb91f32-85d4-4c18-b4ca-dbfd09aa2fa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "241c1f63-b275-485a-bde9-e872b58e639c",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "74042a0c-6107-4845-988a-2bf759326ba7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "f177a9c0-0279-4858-9cf3-067c10ff6472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74042a0c-6107-4845-988a-2bf759326ba7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82c5d271-3143-4015-9c57-93e755092764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74042a0c-6107-4845-988a-2bf759326ba7",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "2a231fd0-31d2-4652-b15b-ecab8ef077b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "18281fdd-98e4-4597-a74b-ad90d154952a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a231fd0-31d2-4652-b15b-ecab8ef077b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3935715b-0153-4261-9ad9-d78586c6b23a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a231fd0-31d2-4652-b15b-ecab8ef077b0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "574d6b30-f6da-49ba-a54e-abc6c97ee453",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "1f0e1252-d452-4753-ade1-2ce8715e3912",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "574d6b30-f6da-49ba-a54e-abc6c97ee453",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0792337-0009-4a3d-acf0-3c33d7964d76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "574d6b30-f6da-49ba-a54e-abc6c97ee453",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f814444e-00c2-4a36-9fe7-45b30499b0bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "b2f6da83-cfd6-4bfc-8514-fd6fbd314f16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f814444e-00c2-4a36-9fe7-45b30499b0bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f28af4b0-b311-4f82-8072-1a3baa775764",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f814444e-00c2-4a36-9fe7-45b30499b0bf",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7501b797-8fcb-4ac0-8377-2ed605035a9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "91dac9f2-9c3c-4472-9f14-64d93720d922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7501b797-8fcb-4ac0-8377-2ed605035a9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca60e06-5cb7-45a7-a440-d7ad0cf97dad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7501b797-8fcb-4ac0-8377-2ed605035a9a",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "2786cff0-05e1-4984-ad7b-d9520f60e3a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "21db5dd2-acf0-418b-9de9-6b7b9c00c6d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2786cff0-05e1-4984-ad7b-d9520f60e3a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57386b47-b0b3-4b25-a7ab-1a03aa00d4c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2786cff0-05e1-4984-ad7b-d9520f60e3a5",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "5234eb1f-5bb9-4cbc-afa7-61e9967bf54e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "8b4a14b8-71e0-4b79-b6c5-01a2fc3b588f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5234eb1f-5bb9-4cbc-afa7-61e9967bf54e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeed4267-b071-46ac-8fe5-c24748d6bf25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5234eb1f-5bb9-4cbc-afa7-61e9967bf54e",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "b17a5777-e76a-4a70-872c-7710b3f0f3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "6d4dad95-1b45-4a35-a5ae-29638747a723",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b17a5777-e76a-4a70-872c-7710b3f0f3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "011925c0-6b55-4696-82ab-7ff1dcf888a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b17a5777-e76a-4a70-872c-7710b3f0f3e5",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "82a552a2-3a85-4946-8649-f689eca4b095",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a8c8ec31-0ba8-45e7-8dc7-838288e21210",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a552a2-3a85-4946-8649-f689eca4b095",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32147e5d-fcc8-49d0-8420-5c097679293c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a552a2-3a85-4946-8649-f689eca4b095",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "165dc5db-1f54-41ad-96a0-49124b097b90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "d269224d-f62a-46cc-b0ee-ca9dbd8bc0c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "165dc5db-1f54-41ad-96a0-49124b097b90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a812ab2-fa86-4c31-ac47-54c8bc860874",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "165dc5db-1f54-41ad-96a0-49124b097b90",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f77300ae-f2ba-438c-bb33-fe92a1e2ab57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "e8d69fae-1bec-4fe0-874d-ae7e2be95d51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f77300ae-f2ba-438c-bb33-fe92a1e2ab57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb54907c-03d9-472b-b312-2355b54cfa52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f77300ae-f2ba-438c-bb33-fe92a1e2ab57",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "79ab9180-02dd-4564-9140-fe0d4d10ddc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ea3097b3-181c-4a35-838f-a658443b3c08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "79ab9180-02dd-4564-9140-fe0d4d10ddc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f33aadf-56bf-4544-b385-2c1ea1ae8848",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "79ab9180-02dd-4564-9140-fe0d4d10ddc0",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "389bce8c-763e-49cd-9176-aca29d7c94ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "4aaebb69-ae54-4657-9a19-409f95c6f44e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "389bce8c-763e-49cd-9176-aca29d7c94ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8e82b78-2988-417f-b2e9-bd8e9f6851a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "389bce8c-763e-49cd-9176-aca29d7c94ff",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "6aae20d8-107f-4f3d-a164-7c65ad38a830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "57270999-ae48-42d0-bc01-7e9a14fe9465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aae20d8-107f-4f3d-a164-7c65ad38a830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca704a7a-88f0-402f-aac6-f6fb4b74980c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aae20d8-107f-4f3d-a164-7c65ad38a830",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "c89ff06d-adab-4819-b402-faa044823adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "b3f67ba2-62ba-4bb7-af05-a5bf359137a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c89ff06d-adab-4819-b402-faa044823adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de2eb704-485b-4c28-a5f3-d80254fda034",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c89ff06d-adab-4819-b402-faa044823adf",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "925cfd05-45e4-49b2-91cc-22bcf7040d91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "d23b6bd6-d9ca-48aa-b178-76ae18674772",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "925cfd05-45e4-49b2-91cc-22bcf7040d91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1545d742-4561-40a2-b60e-30e310b57106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "925cfd05-45e4-49b2-91cc-22bcf7040d91",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "fcfa066f-003c-477e-a7a1-26dc0bb63aa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "68d8fce6-f2b8-4101-82bb-f92d0b043632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcfa066f-003c-477e-a7a1-26dc0bb63aa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eed2b556-5335-415f-b993-e139424c5c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcfa066f-003c-477e-a7a1-26dc0bb63aa5",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "e0718998-d340-45c9-92d1-894b6d6dde0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "1efe17c2-2045-4b46-ab02-a78682a84e92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0718998-d340-45c9-92d1-894b6d6dde0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e020e7-c80c-4254-ba9c-c3e8c6297077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0718998-d340-45c9-92d1-894b6d6dde0f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "91ae614d-0abc-434d-8881-a28ad471a533",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "8b34bfb4-5c39-480e-8cc5-66441c2ccaff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91ae614d-0abc-434d-8881-a28ad471a533",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "42a1667c-bf8c-40d9-a6ff-3561c59477b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91ae614d-0abc-434d-8881-a28ad471a533",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "4b09c2c5-2d88-455f-a452-40461e41c848",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "93890bf1-427c-4025-8050-2592cdd8dc4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b09c2c5-2d88-455f-a452-40461e41c848",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd03c65-9084-4e39-8eaf-28931975780d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b09c2c5-2d88-455f-a452-40461e41c848",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f12634f1-ef2c-4ec8-a3e8-15bdcab4aac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "86a1a29d-e849-4abb-bb70-971a7674e7f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f12634f1-ef2c-4ec8-a3e8-15bdcab4aac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34f5315-08ac-4ee8-8143-2c6038c6cbaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f12634f1-ef2c-4ec8-a3e8-15bdcab4aac8",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "d238e594-8450-4d66-aa70-c5e3cac61132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "a3863e40-a282-4d35-abdc-71ce0f3f9866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d238e594-8450-4d66-aa70-c5e3cac61132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a3e21b3-3331-4492-8e6c-ef93c916d9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d238e594-8450-4d66-aa70-c5e3cac61132",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "8cbc8e5b-48d3-4cf7-8ae8-9f527c343373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "83fb8ea0-e7f8-4269-89d1-40878c3a622d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cbc8e5b-48d3-4cf7-8ae8-9f527c343373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd502ddf-23ba-44c1-a18a-6a2ac39935c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cbc8e5b-48d3-4cf7-8ae8-9f527c343373",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "7fa412e5-ff11-4a9e-bc3a-a5cbf1785622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "ae49cd4e-5a0c-488e-96e6-754a743694c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fa412e5-ff11-4a9e-bc3a-a5cbf1785622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a536b450-8b01-48fd-a399-3f91a999dc5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fa412e5-ff11-4a9e-bc3a-a5cbf1785622",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "f60fa2db-28cc-4bdc-a1f5-317d1d6c0d7f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "503e9168-346d-4908-b8c5-fb50b26e1e5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60fa2db-28cc-4bdc-a1f5-317d1d6c0d7f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7659caa-e361-4791-a376-b8edc7eed0cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60fa2db-28cc-4bdc-a1f5-317d1d6c0d7f",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "287b270e-a212-412f-b15a-6465d6858f69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "9d6f9eb0-636a-4967-9a5f-47fd58f681c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "287b270e-a212-412f-b15a-6465d6858f69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ee8a51c-77ca-4eb9-8db7-fa2c11573d47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "287b270e-a212-412f-b15a-6465d6858f69",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "d118adab-ae79-4179-b2c8-8597f4d8be86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "cfe0546a-76c2-4dcd-82c3-d77c3a6cdd23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d118adab-ae79-4179-b2c8-8597f4d8be86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee07bba7-e04e-441e-9a0e-a011d6f83b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d118adab-ae79-4179-b2c8-8597f4d8be86",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "98c08292-b8a7-43f8-8687-313e2dfe4f13",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "76268619-30fd-4c60-a1f5-56917018f79c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98c08292-b8a7-43f8-8687-313e2dfe4f13",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aee14b00-b02a-476b-9822-321ddb36a260",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98c08292-b8a7-43f8-8687-313e2dfe4f13",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "fd2dc1d2-3c6a-4e84-a66f-ca2e8bb3c5a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "45648192-01ef-44eb-a611-170771f015cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd2dc1d2-3c6a-4e84-a66f-ca2e8bb3c5a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b6a9f7-b4a0-4cb4-bb1e-a25e6363b5dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd2dc1d2-3c6a-4e84-a66f-ca2e8bb3c5a4",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "eff9bd8a-78cf-4edb-89d6-34dee64abdc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "23bd2f0d-8d76-4098-9819-bd9464f7effc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eff9bd8a-78cf-4edb-89d6-34dee64abdc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d49170b9-a52c-4703-93ad-7a75b21c993b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eff9bd8a-78cf-4edb-89d6-34dee64abdc9",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        },
        {
            "id": "da87ea95-ed81-4b1c-b284-ea5b18d673bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "compositeImage": {
                "id": "c804038d-d5d8-461c-b7ae-ac499cae844e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da87ea95-ed81-4b1c-b284-ea5b18d673bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4309dcfc-4856-4ff0-8f2c-390e636f4d2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da87ea95-ed81-4b1c-b284-ea5b18d673bd",
                    "LayerId": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "f36ec40e-0a58-4f6f-aa01-7f0ef4612452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 63,
    "yorig": 63
}