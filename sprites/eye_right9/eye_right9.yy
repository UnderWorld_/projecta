{
    "id": "96c020fb-7eee-4453-81fa-ee905710b486",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7cd88525-46b4-4c6e-a5c4-3968490538e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96c020fb-7eee-4453-81fa-ee905710b486",
            "compositeImage": {
                "id": "ef429419-a1e7-40f3-88dd-f8ea43b31e07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cd88525-46b4-4c6e-a5c4-3968490538e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5aebfd62-dae7-4e5c-b216-98c69c0e849c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cd88525-46b4-4c6e-a5c4-3968490538e0",
                    "LayerId": "875ddb72-3925-4abd-8a0f-b88b309b8339"
                }
            ]
        },
        {
            "id": "be36188b-da09-411f-8b30-7be74cd12f9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96c020fb-7eee-4453-81fa-ee905710b486",
            "compositeImage": {
                "id": "a08cf2da-c1fc-4a4f-beee-c7d3013a693f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be36188b-da09-411f-8b30-7be74cd12f9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b16b97c3-afed-4b81-b1a4-3bbb4ce04b84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be36188b-da09-411f-8b30-7be74cd12f9b",
                    "LayerId": "875ddb72-3925-4abd-8a0f-b88b309b8339"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "875ddb72-3925-4abd-8a0f-b88b309b8339",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96c020fb-7eee-4453-81fa-ee905710b486",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}