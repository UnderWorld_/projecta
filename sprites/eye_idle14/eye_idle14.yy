{
    "id": "4e302f7a-a6e7-4856-9041-178b814a4f83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle14",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db5716f7-36bf-4f49-8d15-42dfa0740d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e302f7a-a6e7-4856-9041-178b814a4f83",
            "compositeImage": {
                "id": "28cc1eac-bafc-4b6a-ad53-caab787a714c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db5716f7-36bf-4f49-8d15-42dfa0740d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "861a22ab-07eb-4de7-bbbf-91876bde4f29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5716f7-36bf-4f49-8d15-42dfa0740d12",
                    "LayerId": "128740ad-7fd4-403a-826b-7a7a05e50051"
                }
            ]
        },
        {
            "id": "52b97b50-f59a-4cde-b2bd-dc6d2f0ba560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e302f7a-a6e7-4856-9041-178b814a4f83",
            "compositeImage": {
                "id": "217fb9a1-3a6f-4fd7-837e-7b058f3c085b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b97b50-f59a-4cde-b2bd-dc6d2f0ba560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2091f800-f56e-40bf-b136-d8cc7645dd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b97b50-f59a-4cde-b2bd-dc6d2f0ba560",
                    "LayerId": "128740ad-7fd4-403a-826b-7a7a05e50051"
                }
            ]
        },
        {
            "id": "65e63e8f-09e0-43e7-b0a5-ff5ceed152d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e302f7a-a6e7-4856-9041-178b814a4f83",
            "compositeImage": {
                "id": "1c9b85b5-795c-4b34-9a35-ec4abcf03a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65e63e8f-09e0-43e7-b0a5-ff5ceed152d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c96ba760-d704-45ca-98db-4988b321cb6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65e63e8f-09e0-43e7-b0a5-ff5ceed152d3",
                    "LayerId": "128740ad-7fd4-403a-826b-7a7a05e50051"
                }
            ]
        },
        {
            "id": "fc3889a5-9153-498d-988b-6296c90905a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e302f7a-a6e7-4856-9041-178b814a4f83",
            "compositeImage": {
                "id": "f1e6bb99-7d89-41e0-b0ce-bd0f0b851863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc3889a5-9153-498d-988b-6296c90905a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0a23e3f-c3d9-4cf5-a7e8-ce1fd0e1a373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc3889a5-9153-498d-988b-6296c90905a4",
                    "LayerId": "128740ad-7fd4-403a-826b-7a7a05e50051"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "128740ad-7fd4-403a-826b-7a7a05e50051",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e302f7a-a6e7-4856-9041-178b814a4f83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}