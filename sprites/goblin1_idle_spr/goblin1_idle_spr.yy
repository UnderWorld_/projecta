{
    "id": "b636f81f-50f2-4063-9bec-c94f9a00bed7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin1_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbe08d2a-cfc3-4c7d-acff-d4fbe4cf5304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b636f81f-50f2-4063-9bec-c94f9a00bed7",
            "compositeImage": {
                "id": "b49c73f7-ad49-4984-9775-e47b046520e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe08d2a-cfc3-4c7d-acff-d4fbe4cf5304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20382a4c-a02b-4e6f-8f07-03d8dc590cce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe08d2a-cfc3-4c7d-acff-d4fbe4cf5304",
                    "LayerId": "732b9298-b097-4535-a5fa-d8f7bd61dfe6"
                }
            ]
        },
        {
            "id": "956792ff-7466-4613-84c6-b2bf7e51e461",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b636f81f-50f2-4063-9bec-c94f9a00bed7",
            "compositeImage": {
                "id": "31a41005-0774-4902-b91e-6b82b2cd9c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "956792ff-7466-4613-84c6-b2bf7e51e461",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "336255f0-e827-4f0a-9f1f-47727a8bbc84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "956792ff-7466-4613-84c6-b2bf7e51e461",
                    "LayerId": "732b9298-b097-4535-a5fa-d8f7bd61dfe6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "732b9298-b097-4535-a5fa-d8f7bd61dfe6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b636f81f-50f2-4063-9bec-c94f9a00bed7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 191
}