{
    "id": "402fce28-97bd-4baa-b9e8-f7a08afa4d99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "n_player_walk_down_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 27,
    "bbox_right": 53,
    "bbox_top": 19,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6777e5a-59f3-4756-ba5d-cbb998d9f913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "402fce28-97bd-4baa-b9e8-f7a08afa4d99",
            "compositeImage": {
                "id": "714feab5-e9a2-4e42-af9a-c08abba43eff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6777e5a-59f3-4756-ba5d-cbb998d9f913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e898c3c7-ec57-4cfc-9781-2fec0d297039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6777e5a-59f3-4756-ba5d-cbb998d9f913",
                    "LayerId": "6f4838e9-6a57-4a17-a07c-4575aaf99eef"
                }
            ]
        },
        {
            "id": "444d18de-5149-4436-b740-1b30064365ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "402fce28-97bd-4baa-b9e8-f7a08afa4d99",
            "compositeImage": {
                "id": "9b57b1dc-53a4-4345-a6fa-576891a3144d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "444d18de-5149-4436-b740-1b30064365ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee453c1a-5005-4c27-abfe-67616a86d730",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "444d18de-5149-4436-b740-1b30064365ea",
                    "LayerId": "6f4838e9-6a57-4a17-a07c-4575aaf99eef"
                }
            ]
        },
        {
            "id": "0ae7cf8d-2a5c-4578-8568-37dfb2db3fe2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "402fce28-97bd-4baa-b9e8-f7a08afa4d99",
            "compositeImage": {
                "id": "9fa451c7-2943-4a6d-ae0f-0ddfd577acce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ae7cf8d-2a5c-4578-8568-37dfb2db3fe2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea67e780-494a-4aa2-ab12-dfa9292edd01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ae7cf8d-2a5c-4578-8568-37dfb2db3fe2",
                    "LayerId": "6f4838e9-6a57-4a17-a07c-4575aaf99eef"
                }
            ]
        },
        {
            "id": "053faded-ea6f-4a05-b721-b6acb7480ce6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "402fce28-97bd-4baa-b9e8-f7a08afa4d99",
            "compositeImage": {
                "id": "ba82a1de-334b-4fc6-b1e5-302b6b0f5385",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "053faded-ea6f-4a05-b721-b6acb7480ce6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "965ad8c8-2151-4635-bd93-26f636d599a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "053faded-ea6f-4a05-b721-b6acb7480ce6",
                    "LayerId": "6f4838e9-6a57-4a17-a07c-4575aaf99eef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "6f4838e9-6a57-4a17-a07c-4575aaf99eef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "402fce28-97bd-4baa-b9e8-f7a08afa4d99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 85
}