{
    "id": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_treetwo_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 9,
    "bbox_right": 21,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca6c756f-a5fa-4e15-8f30-28518862bd05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "a1d534e0-f55d-4faa-9b0f-08b5a090139f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6c756f-a5fa-4e15-8f30-28518862bd05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed8c2f27-894f-49d8-b3f0-19857fc04555",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6c756f-a5fa-4e15-8f30-28518862bd05",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f94efa59-300f-4697-adf8-5c1f992bd5fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "baf8f2f3-41d6-4e4f-a973-f6a8a16cc2f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f94efa59-300f-4697-adf8-5c1f992bd5fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1477bcd-3903-44dd-8e7f-e25115498f0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f94efa59-300f-4697-adf8-5c1f992bd5fa",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f6e9d66d-6732-4de7-9c24-d1490229ba62",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "7d1931bd-e28c-4e50-8225-61b934566f8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6e9d66d-6732-4de7-9c24-d1490229ba62",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f0feff2-573c-4621-960b-0da9295b84cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6e9d66d-6732-4de7-9c24-d1490229ba62",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "ad4ba70b-cdc6-4c34-9946-29d19f07f5ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "c8549f48-f4c8-4039-93f3-5e8f6e6b3c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad4ba70b-cdc6-4c34-9946-29d19f07f5ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "255432f7-8483-4e50-a92b-0f94c49f8130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad4ba70b-cdc6-4c34-9946-29d19f07f5ae",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "380a75fc-ef4a-451d-b8af-ae2d2a27f5e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "24a9b8ac-b914-4b6b-b82e-3801061f4f33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "380a75fc-ef4a-451d-b8af-ae2d2a27f5e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d2a075-6ccc-4987-ab07-b4978b3fc8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "380a75fc-ef4a-451d-b8af-ae2d2a27f5e4",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "391151b2-6f6c-47ce-b9a2-976c510c8b49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "09721673-d018-42c2-90f2-d983c0f06b0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "391151b2-6f6c-47ce-b9a2-976c510c8b49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f8e1d4-7229-4af1-8a0b-8706f98c5f97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "391151b2-6f6c-47ce-b9a2-976c510c8b49",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a4c984e7-8e4e-4cda-bbcd-4c4bca5d3066",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "189ab8e1-3ba6-4594-a33a-7894b77ea4f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4c984e7-8e4e-4cda-bbcd-4c4bca5d3066",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a721212-3320-4eef-ad5f-b66c9358b416",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4c984e7-8e4e-4cda-bbcd-4c4bca5d3066",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "ec75c5c5-4dfc-4cf5-b0bb-db68308f72b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f51e93f6-5638-4ec0-9c25-dcc805da1587",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec75c5c5-4dfc-4cf5-b0bb-db68308f72b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "144f0045-8658-4cf1-8fec-353a991dd3da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec75c5c5-4dfc-4cf5-b0bb-db68308f72b7",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "9083eb23-5002-4896-9c09-0928010f4119",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "202da0ae-95d2-492d-b550-1ed372b1ebc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9083eb23-5002-4896-9c09-0928010f4119",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf3405b5-5d19-4319-a087-44e58719800d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9083eb23-5002-4896-9c09-0928010f4119",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a81d9694-1572-4f0a-9023-299de34ee3b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "fb4745f3-7c37-4178-afd7-fc1c554e3443",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a81d9694-1572-4f0a-9023-299de34ee3b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dd3a1c1-6265-4d2f-84d6-550629d3bfbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a81d9694-1572-4f0a-9023-299de34ee3b2",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "11a8fbca-301d-4aa6-8984-28a3226c2e54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "4f88d89f-b88d-4079-984a-5214aff169fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11a8fbca-301d-4aa6-8984-28a3226c2e54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbc09764-7859-4797-b262-3bdbf834617e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11a8fbca-301d-4aa6-8984-28a3226c2e54",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "3b7b8ca6-1c54-4aa2-9e5f-a8a46c0050f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "44be511c-82a3-4b2a-a251-d24ebabb71a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b7b8ca6-1c54-4aa2-9e5f-a8a46c0050f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "199ba9d3-89d7-4bf3-8fce-fd025c0e1574",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b7b8ca6-1c54-4aa2-9e5f-a8a46c0050f5",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "d3092b01-ed9a-448b-87ac-c991abcfc85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "5c76955d-e73e-4fde-a6c2-3c754afa7e83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3092b01-ed9a-448b-87ac-c991abcfc85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6aa224-627b-422f-bf9d-c1b028ce9a5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3092b01-ed9a-448b-87ac-c991abcfc85b",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "b4830a52-9fcb-4bfd-95b6-150f3379b082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "623f97db-6fb4-40c6-8851-4348764cf64e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4830a52-9fcb-4bfd-95b6-150f3379b082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de33f3af-4373-4c74-aee7-a58830d71fd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4830a52-9fcb-4bfd-95b6-150f3379b082",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "9f59dc98-622b-4a6a-a32c-48a3a0f8bd04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "552c2094-5985-4846-a429-b44ccbebd86d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f59dc98-622b-4a6a-a32c-48a3a0f8bd04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac0e0038-e0ec-4d59-979b-08154e9602fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f59dc98-622b-4a6a-a32c-48a3a0f8bd04",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "e207e06c-f9c2-4e2e-9260-cf9df19fc841",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "9ed0e2b9-a9ce-4d69-b92b-574528ff291a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e207e06c-f9c2-4e2e-9260-cf9df19fc841",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7af46e0c-f77f-451d-bce1-40d03b2c3d90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e207e06c-f9c2-4e2e-9260-cf9df19fc841",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "6b3e54a2-9e5c-4981-bb5c-0aa354adeaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ef2a9af8-2cf1-4ef0-bdd7-70f87c80b06d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b3e54a2-9e5c-4981-bb5c-0aa354adeaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dfb11b7-f1d2-4408-a18a-cb84b5f5dbe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b3e54a2-9e5c-4981-bb5c-0aa354adeaf2",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "87befb9e-9587-4df6-8773-97499094299a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "a2467137-797f-4c58-8742-110c348300f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87befb9e-9587-4df6-8773-97499094299a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dc866b0-fbb3-4299-ba13-31e917c9a81e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87befb9e-9587-4df6-8773-97499094299a",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "63495b13-b9b3-4e9f-bec5-f0d45d9bec9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "6dea45d2-cd34-4926-9b4c-6ccef65d4e2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63495b13-b9b3-4e9f-bec5-f0d45d9bec9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1e497fc-bde8-477c-a9ab-5964a606adb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63495b13-b9b3-4e9f-bec5-f0d45d9bec9d",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "56d4d5da-f582-418e-8826-42fae2cb0946",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ee6a3bb5-0aaf-4fea-b5cc-8ab4248ef9b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d4d5da-f582-418e-8826-42fae2cb0946",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c947025-0ce7-46ac-8dfa-05d39721d623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d4d5da-f582-418e-8826-42fae2cb0946",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "305cc3b1-2ab0-4226-b66a-8ab43cf5e8b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "41fb717f-4163-4a60-8f76-038163ff0568",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "305cc3b1-2ab0-4226-b66a-8ab43cf5e8b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "390b2a4a-52b6-4e82-bf74-5329bb6c5aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "305cc3b1-2ab0-4226-b66a-8ab43cf5e8b9",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "c5d6e3b1-d316-44d3-bcda-68e33aa4de4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "edc84974-1241-4bfe-9599-0f1143eda36a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5d6e3b1-d316-44d3-bcda-68e33aa4de4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca539e26-ebc4-408a-8630-c84016704cb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5d6e3b1-d316-44d3-bcda-68e33aa4de4d",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "fab72ae2-f543-41c7-8186-6e796b106a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "cc80d63b-eebd-44e8-a5d2-bfb50f15ed03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fab72ae2-f543-41c7-8186-6e796b106a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "393d2ac2-7f71-4442-aa55-24ded924281e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fab72ae2-f543-41c7-8186-6e796b106a2e",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "6a787723-7bb2-48d8-a359-c8145ac13743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "292582f8-cc80-49c0-a83a-39ca0c4c0c5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a787723-7bb2-48d8-a359-c8145ac13743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fba6993-bbea-47ef-bbb4-64ca90bbcc2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a787723-7bb2-48d8-a359-c8145ac13743",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "1b8271d7-4cb9-4cd6-b102-9aaa639e08e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "9067b12f-cba6-432c-a818-bbde15ea43e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8271d7-4cb9-4cd6-b102-9aaa639e08e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24d9dc7a-7467-4024-bb34-ff5e4f5a7d19",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8271d7-4cb9-4cd6-b102-9aaa639e08e3",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "025fb433-37be-49de-a16e-b9a08f288e73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "2814adbb-9afb-4535-9101-674cd7b37a71",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025fb433-37be-49de-a16e-b9a08f288e73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e87618d4-0b0b-4e1b-ad2d-3c23dfb71298",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025fb433-37be-49de-a16e-b9a08f288e73",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a85e7a8a-65a6-43c8-ae05-a891cfd1fd1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "bf85f2fc-21f7-4ea5-a91e-9f58120ff4dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a85e7a8a-65a6-43c8-ae05-a891cfd1fd1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84f049f-99e1-4507-98bb-ae99b43d1908",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a85e7a8a-65a6-43c8-ae05-a891cfd1fd1f",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "954b16ba-a905-48ee-8681-df0349533309",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "3597abcc-89c2-4fe1-8418-3f21311245c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "954b16ba-a905-48ee-8681-df0349533309",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f0d7ad0-8b31-486c-b9cb-9c8680781b3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "954b16ba-a905-48ee-8681-df0349533309",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "dbb34a0a-be2a-4bf4-ac72-485c442643b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "b22e3c29-494e-46e9-a14b-859fd9ce3ddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbb34a0a-be2a-4bf4-ac72-485c442643b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b318a6b-71c0-45ed-9be1-2eb7a0fda4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbb34a0a-be2a-4bf4-ac72-485c442643b3",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "7c0dc096-1a11-49e8-83e1-aefd8d2d5fa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "29cdee0c-9dd4-4420-9ec5-1d55fcf3579c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c0dc096-1a11-49e8-83e1-aefd8d2d5fa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebda1e0-f3b2-4b25-8357-953b60e4161d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c0dc096-1a11-49e8-83e1-aefd8d2d5fa5",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "b2537849-09d1-4f15-ae8f-8ead29a63854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "050ab3d7-6b84-4491-b051-8c3d0b4394ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2537849-09d1-4f15-ae8f-8ead29a63854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a62e3d4-1d29-4ba2-b5ed-d595ea172c4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2537849-09d1-4f15-ae8f-8ead29a63854",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "b351e434-64f5-4ddb-a49c-d5bafd6197db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "12ef8b3d-0c17-4969-a149-06cfc58f0a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b351e434-64f5-4ddb-a49c-d5bafd6197db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa1784c0-b472-4a09-a731-58019d67138f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b351e434-64f5-4ddb-a49c-d5bafd6197db",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "b366a20b-c9aa-48ae-9d84-d94697f92ca7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ff5da2bd-64a1-4c8f-815d-500be9ec7178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b366a20b-c9aa-48ae-9d84-d94697f92ca7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d556102a-d3d4-4697-b32e-9c55907848fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b366a20b-c9aa-48ae-9d84-d94697f92ca7",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "bef99fa6-5236-44cf-8f81-8187f92bd2c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "85cfb40a-11b0-4b07-be5f-b3faf003358d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bef99fa6-5236-44cf-8f81-8187f92bd2c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21afcd42-5629-42d5-8019-2f3a3a1a7a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bef99fa6-5236-44cf-8f81-8187f92bd2c2",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a98ac1e0-4f84-445c-b3fa-92ebc7b3a468",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "b407313a-18fa-4e03-ad9f-e50406f8f0ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a98ac1e0-4f84-445c-b3fa-92ebc7b3a468",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da13ea96-300d-445f-b5ae-e76938967f67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a98ac1e0-4f84-445c-b3fa-92ebc7b3a468",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "7127932a-8840-4372-867c-f9b4b4414019",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "003edbbe-b1a9-4e41-bfd4-c002d7562d16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7127932a-8840-4372-867c-f9b4b4414019",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "caa779b4-6f63-4685-b4bb-70163f55544e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7127932a-8840-4372-867c-f9b4b4414019",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "12ead8d1-1202-48a3-bf36-f4f96076d1f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "2124032f-ca18-4ff7-8240-ce2dc1786999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ead8d1-1202-48a3-bf36-f4f96076d1f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa970bd3-d11d-413d-b221-49c35d778f3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ead8d1-1202-48a3-bf36-f4f96076d1f9",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "1fb8248d-5523-446b-8e96-bc46d04aaafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "2388e9d6-db35-4aea-8a40-24ed0b2a24d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb8248d-5523-446b-8e96-bc46d04aaafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f6d6fa5-b149-496b-b559-043acf654dd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb8248d-5523-446b-8e96-bc46d04aaafa",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "fb578e54-9a11-4c19-8aa9-b2c5c5d4f565",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "e2c99c12-c508-41d3-af16-9357199f5081",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb578e54-9a11-4c19-8aa9-b2c5c5d4f565",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994b1723-3245-4d4d-b55c-92f085707f4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb578e54-9a11-4c19-8aa9-b2c5c5d4f565",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "b6179db7-eff0-4fa0-9f72-536210d28b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "81fb34f9-c34a-4fba-a566-d206000d34a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6179db7-eff0-4fa0-9f72-536210d28b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d7a8b51-4b63-42e2-a6b3-bebc0b639960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6179db7-eff0-4fa0-9f72-536210d28b4a",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "472b8654-7d85-4a52-83b5-4cf573e0d491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "a310d5cd-e677-4954-803c-11defa9731ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "472b8654-7d85-4a52-83b5-4cf573e0d491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eae7bd89-282a-4fe6-bacc-7f2ee33f612d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "472b8654-7d85-4a52-83b5-4cf573e0d491",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a316bbc9-e176-4d93-ae4d-499ad64386e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "039e00cd-1b51-44c7-9ad6-9aa09524d9b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a316bbc9-e176-4d93-ae4d-499ad64386e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac7c5b6b-ef30-4dc2-ab94-5021baffa8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a316bbc9-e176-4d93-ae4d-499ad64386e0",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a0899c7b-65fa-4622-b03b-081cad154c69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "bc7ee2fd-5ec5-44d4-80b8-1ae17c3e0792",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0899c7b-65fa-4622-b03b-081cad154c69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7637ddc1-129f-4333-82e7-60284b50cb81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0899c7b-65fa-4622-b03b-081cad154c69",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "dfbdcde3-f50c-4682-87da-a745342e43f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "73499d4b-05e3-4c62-8b15-ab45bd4e585a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbdcde3-f50c-4682-87da-a745342e43f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0caf3260-eb28-4908-aff7-374b60f3ff5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbdcde3-f50c-4682-87da-a745342e43f9",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "52b18135-3836-497a-95f1-9687a8e7e1ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "c703be8a-3fbf-449a-a367-32fcc659b363",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b18135-3836-497a-95f1-9687a8e7e1ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66ee0c78-72cd-4e96-97c9-b2cf1092a55a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b18135-3836-497a-95f1-9687a8e7e1ed",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "4eb099c4-c7aa-4da6-8ef8-8cd0f9903f77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "25981918-e775-4c61-a787-21eacea4487c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4eb099c4-c7aa-4da6-8ef8-8cd0f9903f77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f91a5b13-b697-4ef2-9271-f0ea56e5096b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4eb099c4-c7aa-4da6-8ef8-8cd0f9903f77",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "3218cf8f-c131-4295-819c-124db80fda30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "4af89b03-8ed4-4534-88dd-efa09dffd879",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3218cf8f-c131-4295-819c-124db80fda30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aa79a05-d271-411d-9857-b60db21aa8ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3218cf8f-c131-4295-819c-124db80fda30",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "23c098f3-5ebd-4dfc-a42f-02d8fe9dfcae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "8ed0b6b3-61a8-473f-a8da-4e0f2a352bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23c098f3-5ebd-4dfc-a42f-02d8fe9dfcae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09fdffe9-1882-4a1d-9074-486b770e4f1d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23c098f3-5ebd-4dfc-a42f-02d8fe9dfcae",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "049c35d8-f09d-4fcc-87f2-c77e01a8759b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "3e80fb24-73de-418f-89f1-fa62b17f4929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "049c35d8-f09d-4fcc-87f2-c77e01a8759b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3717e402-7e27-4892-88dc-1dfd949469f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "049c35d8-f09d-4fcc-87f2-c77e01a8759b",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "df49a7bb-4b25-40c7-bf20-af2fc12bb4a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "7b39288b-4c7e-4b43-8a7d-9f01181ae362",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df49a7bb-4b25-40c7-bf20-af2fc12bb4a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa92e09-fa28-4808-b82f-8445e8dda32a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df49a7bb-4b25-40c7-bf20-af2fc12bb4a3",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "7441feab-0bfc-4fa8-81a2-43c8c78a6ee8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "19ddcefc-4732-4b69-a684-1a87d40e8f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7441feab-0bfc-4fa8-81a2-43c8c78a6ee8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ad30487-c164-4113-a93a-01cc384effcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7441feab-0bfc-4fa8-81a2-43c8c78a6ee8",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "217737cc-aeb8-4f25-a866-789d20cf00ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "d5b65107-c220-4073-afcf-020c90b26f81",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "217737cc-aeb8-4f25-a866-789d20cf00ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bde4b81f-fc06-41f1-9c6a-6a12eca3e2fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "217737cc-aeb8-4f25-a866-789d20cf00ba",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "208a15e1-b43b-4bce-aa06-e2cd9804f77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "901332f3-c262-42ac-986e-f0592f2113e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "208a15e1-b43b-4bce-aa06-e2cd9804f77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dea66cf-1def-4fab-8b4f-cd534db6f1c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "208a15e1-b43b-4bce-aa06-e2cd9804f77a",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "cede6e31-dbbf-4d96-847c-477e48cc2544",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "1fb9e71d-21c5-445d-9809-0a911e1401e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cede6e31-dbbf-4d96-847c-477e48cc2544",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59f6cbff-16c3-4f72-a2f4-6684747ab31e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cede6e31-dbbf-4d96-847c-477e48cc2544",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "54e0b0d4-7eb2-4ae4-9a22-2eccf87df889",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f2a27855-8fb7-417a-8be9-d6b4fdc158eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e0b0d4-7eb2-4ae4-9a22-2eccf87df889",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9b59428-1189-481a-914e-f12693f6d9cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e0b0d4-7eb2-4ae4-9a22-2eccf87df889",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f6275991-554f-48da-9c81-015e5af0592c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "a998de84-3127-4a0d-97aa-c8154fbf7f00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6275991-554f-48da-9c81-015e5af0592c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae22f0bf-6d89-465a-b9bd-f62b56b8bade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6275991-554f-48da-9c81-015e5af0592c",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "5924f89c-7ea5-488a-92c3-1b0a4a4c0d12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "809f86e0-c1fe-4333-815d-8d22cffce178",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5924f89c-7ea5-488a-92c3-1b0a4a4c0d12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69851783-b905-4cbb-93cf-1d0a76267da3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5924f89c-7ea5-488a-92c3-1b0a4a4c0d12",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "8b903b09-a9a6-430b-968d-3d0bf317775f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "500304b0-40ad-43d5-98c9-3ef5b3ba372f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b903b09-a9a6-430b-968d-3d0bf317775f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd1994e6-ec94-4f58-9507-64bb78913cb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b903b09-a9a6-430b-968d-3d0bf317775f",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "2d899552-828b-44ba-a612-deaebe67672a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "18d1ca90-b840-4363-bd2b-dff83eabd8ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d899552-828b-44ba-a612-deaebe67672a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d434d71d-32ad-49db-bdbf-579a31c38922",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d899552-828b-44ba-a612-deaebe67672a",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "ad1b535c-5bd8-4559-b18c-f3a1c644acad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "4f6df994-965c-4ade-b257-1d868b6c2a94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad1b535c-5bd8-4559-b18c-f3a1c644acad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "970e5e19-01ea-4492-8378-fb7a4f4b3869",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad1b535c-5bd8-4559-b18c-f3a1c644acad",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "bf0c46a2-de95-462e-82d8-885a74880d84",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "bae05560-70e3-4a87-a7a0-9ee8c1bbf47e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf0c46a2-de95-462e-82d8-885a74880d84",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11f15d50-a272-4957-bf0d-e06dd38e6a14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf0c46a2-de95-462e-82d8-885a74880d84",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "e7a5d0ab-d3de-4284-87a8-6d59da06cb25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "1094a2d6-1523-47d0-8ffd-bf6075c1a998",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7a5d0ab-d3de-4284-87a8-6d59da06cb25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f50b6ef-0992-40c3-8f75-0fe9a93b7740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7a5d0ab-d3de-4284-87a8-6d59da06cb25",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f39cd1f2-4e45-4833-a040-c84fae090ed6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "3ce3386c-fb56-46f3-b275-f4ee2854ac76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f39cd1f2-4e45-4833-a040-c84fae090ed6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fd86379-6c02-42b5-8d8a-650cd27d2e69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f39cd1f2-4e45-4833-a040-c84fae090ed6",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "52fd5536-c965-4ddc-a331-d228cdd46f0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "0ef24583-e017-4806-9863-06fa8b219fdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52fd5536-c965-4ddc-a331-d228cdd46f0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "952d03a0-3266-419b-b31f-ba0a8c09cc03",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52fd5536-c965-4ddc-a331-d228cdd46f0d",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "eb81d22d-0e5a-4f51-a5bc-0d14975cffb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "e4360eed-4d0b-4a22-b7c3-56ca7318393c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb81d22d-0e5a-4f51-a5bc-0d14975cffb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfe49912-cb79-4e67-b4b9-b0789c7ae12b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb81d22d-0e5a-4f51-a5bc-0d14975cffb3",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "139592b1-0b5d-40a6-894b-bab3803ebbcf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "10b5e2dc-210d-4697-9522-698ff729ea21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "139592b1-0b5d-40a6-894b-bab3803ebbcf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ad8a6b-8b82-4a69-9858-05fbd4cc22e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "139592b1-0b5d-40a6-894b-bab3803ebbcf",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "e1dfa815-ebb6-434d-9f8c-8c83ce833573",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "847eff4b-c75f-4309-9d3c-81182d94f77d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1dfa815-ebb6-434d-9f8c-8c83ce833573",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e0fa65e-8c1b-48ca-af37-c25896429c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1dfa815-ebb6-434d-9f8c-8c83ce833573",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "c2a6137f-ef5a-4a2f-986b-2512f927dd72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "a2883f02-cc9c-4053-8931-b07a63984f09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2a6137f-ef5a-4a2f-986b-2512f927dd72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "357b3384-5d8f-41d7-a787-e9262c4bdba0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2a6137f-ef5a-4a2f-986b-2512f927dd72",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "5abf756b-5098-4272-91c9-f9985118fc47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "1985a737-247f-40d2-9ee9-8876c574ee92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5abf756b-5098-4272-91c9-f9985118fc47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "684430fb-4185-4fc9-8262-97bd46b04e8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5abf756b-5098-4272-91c9-f9985118fc47",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "88977441-b2c2-4b24-ba88-9944c71afbc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "6f58004f-b1d5-497a-9bed-c1b1a41df108",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88977441-b2c2-4b24-ba88-9944c71afbc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4340e82-85be-46cd-acc6-1e300962c56e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88977441-b2c2-4b24-ba88-9944c71afbc8",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "57e8ba52-2a80-4fc5-aacf-43da043baeea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "236abae2-22c9-4d51-a4d7-aae7bedd98a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57e8ba52-2a80-4fc5-aacf-43da043baeea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d51334a-c7d8-44b6-ad1b-1b4d8a4a006d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57e8ba52-2a80-4fc5-aacf-43da043baeea",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "ae94a258-910c-4551-902b-eedbcb96d5b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "549d87e9-c356-45aa-9eaf-d03ac24c09c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae94a258-910c-4551-902b-eedbcb96d5b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30b72e96-2376-405e-8caa-69106192cd4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae94a258-910c-4551-902b-eedbcb96d5b6",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "96946e96-b38e-4d43-9d95-5538f43a9871",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "65e5e395-c0f3-4b84-bff3-c44248258309",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96946e96-b38e-4d43-9d95-5538f43a9871",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ef82e60-7436-467b-ab6a-f012e143f1f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96946e96-b38e-4d43-9d95-5538f43a9871",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f7912503-7d6b-4e80-8754-7968e816b1f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ebe0cbaa-3e3b-4584-bcd0-2003866cd27f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7912503-7d6b-4e80-8754-7968e816b1f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8260312-a6ff-4d2c-a2d3-9bb3d28c5013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7912503-7d6b-4e80-8754-7968e816b1f5",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "37044dd8-89d0-464c-9fdb-8730b6175288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "61ee29fc-3ccb-4191-93cd-7f296861ec45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37044dd8-89d0-464c-9fdb-8730b6175288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6656af7-f490-4ee7-84a8-6cd267d0f022",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37044dd8-89d0-464c-9fdb-8730b6175288",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "0094b86b-5be1-493b-bc0e-b9a952dd2dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f7ae3909-c32b-4f57-8a4f-b6b8b3f03f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0094b86b-5be1-493b-bc0e-b9a952dd2dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7acf739f-dd07-49f7-9cee-ff5fe470e8b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0094b86b-5be1-493b-bc0e-b9a952dd2dd0",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "93521392-0111-4cc7-b312-5280541f8f4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "8712ff35-0b2a-4c7d-a228-d26e1409ca77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93521392-0111-4cc7-b312-5280541f8f4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "218e444f-7468-4d95-967b-b0a9e160c3a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93521392-0111-4cc7-b312-5280541f8f4d",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "dfe9f0c0-702a-4bd6-9561-a725d11979f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "28336759-2066-47dc-9e16-8969b8168016",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfe9f0c0-702a-4bd6-9561-a725d11979f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f1d6176-3792-44e3-b204-643019f0d36d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfe9f0c0-702a-4bd6-9561-a725d11979f9",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "2a77d3e0-3085-4c8d-b636-3560c81b6cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "770c05d1-9dba-4d58-9e81-24e120530ac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a77d3e0-3085-4c8d-b636-3560c81b6cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75491841-b0b8-4399-a37b-74ced395ddf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a77d3e0-3085-4c8d-b636-3560c81b6cde",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "d24a44c2-e23c-4398-9409-69e8cc36d75e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "8f36f8f2-175c-46f1-8fad-35a5ce731f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d24a44c2-e23c-4398-9409-69e8cc36d75e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8edeb087-58c4-47f7-8fd2-4db12ab063d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d24a44c2-e23c-4398-9409-69e8cc36d75e",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "e0dbe8ef-639a-4fea-8804-ebbe2e637014",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "0a9840ab-7dba-4a13-b58f-39e47806b7b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0dbe8ef-639a-4fea-8804-ebbe2e637014",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf79578-1cd5-4761-ac71-909749a94366",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0dbe8ef-639a-4fea-8804-ebbe2e637014",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "c344d93a-5323-4096-a47c-1469582e24f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "d81d9029-5528-4e59-b155-740f326c3464",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c344d93a-5323-4096-a47c-1469582e24f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6998b0-de8a-4206-bee7-bd72c365c7a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c344d93a-5323-4096-a47c-1469582e24f6",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "ac4dff8b-07b9-4b05-ad96-e523d832df22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "462445f7-6297-483e-baf0-df200201ae94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac4dff8b-07b9-4b05-ad96-e523d832df22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc3b189a-02a5-4888-90ce-27abfb880be2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac4dff8b-07b9-4b05-ad96-e523d832df22",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f55410a4-fbef-4be8-8e6e-6fd333f5a80b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ad34365a-66c2-4381-8a64-e21f64b1b563",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f55410a4-fbef-4be8-8e6e-6fd333f5a80b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a29b007b-7857-4fa2-839f-02e3edfc9377",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f55410a4-fbef-4be8-8e6e-6fd333f5a80b",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "749c85ac-b968-466c-a306-1a9e5e538941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "c47bb67f-85fb-447e-9f0c-23230af86d65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749c85ac-b968-466c-a306-1a9e5e538941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d90c4ec-04ae-4b71-891c-ee05a0a1e9db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749c85ac-b968-466c-a306-1a9e5e538941",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "70e39388-aac3-4150-ba9e-9ec79e230d0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "5a47c2d7-bf71-49c2-8fbf-01aaf94efe42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70e39388-aac3-4150-ba9e-9ec79e230d0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf2a3a83-af45-4886-aa27-040c431cd625",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70e39388-aac3-4150-ba9e-9ec79e230d0f",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "26706ed5-abbd-4938-8b4a-aefcdd6f5a3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ef137cb5-33ca-4537-9716-c846c021044c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26706ed5-abbd-4938-8b4a-aefcdd6f5a3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "505b7676-6809-4f84-a2be-1a7b17245d73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26706ed5-abbd-4938-8b4a-aefcdd6f5a3d",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "01066229-27f0-4193-94e7-f464b38cb1fb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "8c618ecc-c140-4b40-88b0-0164b017a279",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01066229-27f0-4193-94e7-f464b38cb1fb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0c0a75-aaae-4939-bec4-0b65e651038a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01066229-27f0-4193-94e7-f464b38cb1fb",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "c60c5875-50f1-4201-bbbe-1afafc52e45c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f68c8fcc-98bf-42e3-9092-25a93aee3b2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c60c5875-50f1-4201-bbbe-1afafc52e45c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a15bf2d1-98f7-4d1c-a401-b245f593b8c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c60c5875-50f1-4201-bbbe-1afafc52e45c",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "4d16c360-5452-4c30-85bc-ae2292cf74f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "8ea16645-c63c-4e97-8a08-9df6482c122c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d16c360-5452-4c30-85bc-ae2292cf74f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93ee9bbd-4316-424e-bdc4-2314a7baaa1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d16c360-5452-4c30-85bc-ae2292cf74f7",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "a94158e9-54f0-4785-ae34-1875c4fed37e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ff6b2ab6-d72e-4e67-a9e2-231dc6970fc9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a94158e9-54f0-4785-ae34-1875c4fed37e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0168095-2479-4bf2-bdad-a02c299eb316",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a94158e9-54f0-4785-ae34-1875c4fed37e",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "d2da4772-2a00-49bd-b040-b77867148492",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "1049354c-4987-4269-9291-9e3eda36e261",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2da4772-2a00-49bd-b040-b77867148492",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe6db8af-81dd-4f9f-a6ba-00eca9400aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2da4772-2a00-49bd-b040-b77867148492",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "2d489921-30de-43c8-9d71-75f51e54ab88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "269ae47a-96a3-4fa1-a5b0-164523367f77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d489921-30de-43c8-9d71-75f51e54ab88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dbbcfb9-90bf-4b5e-9eab-47de17d95efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d489921-30de-43c8-9d71-75f51e54ab88",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "92c56171-0f58-4b2d-a8ad-a4e02354c4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "39804ac5-d5e6-47a1-a410-9502400e29da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c56171-0f58-4b2d-a8ad-a4e02354c4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "728c3ee5-e9d8-4451-9fb0-a43cb132d873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c56171-0f58-4b2d-a8ad-a4e02354c4c9",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "036952fc-728c-44a3-ad83-587374d0d106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "59032b0e-cb65-45c2-95a0-5473e81c19ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "036952fc-728c-44a3-ad83-587374d0d106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17bdc8e6-1b0b-4734-b5f4-1425b2a99eb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "036952fc-728c-44a3-ad83-587374d0d106",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "876fc13b-31d5-48d5-9ce6-b30c05c54c25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "4fff0e24-8e9d-468d-b4c4-e83c4f077c22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "876fc13b-31d5-48d5-9ce6-b30c05c54c25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c3f2693-a4b6-42fc-bb53-cc8611050bb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "876fc13b-31d5-48d5-9ce6-b30c05c54c25",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "64bb48c5-a157-4d3d-b8b8-44645ab4ab04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "6d6bab26-3039-47eb-a387-f37db5a7987a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64bb48c5-a157-4d3d-b8b8-44645ab4ab04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a12b16a-4dae-4e42-8a22-e268291b0055",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64bb48c5-a157-4d3d-b8b8-44645ab4ab04",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "df12089c-7bf0-4f0e-853d-6d7d8a306f15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "6307033a-e566-4575-abbc-7ede026040a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df12089c-7bf0-4f0e-853d-6d7d8a306f15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6368549e-2412-45d2-bc86-f51ca307b3fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df12089c-7bf0-4f0e-853d-6d7d8a306f15",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "5e7825b6-db0e-4c23-b9d1-fd081922dbc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "6f4213e0-edc0-4eb6-bc71-4154a2b36426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e7825b6-db0e-4c23-b9d1-fd081922dbc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2843205e-d8cf-4f24-a12f-cca535c837c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e7825b6-db0e-4c23-b9d1-fd081922dbc3",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "f13cbad3-d9b0-4ea9-8653-d5568d14b33e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "035ef66d-d04d-42e9-920a-e4832cb8726c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f13cbad3-d9b0-4ea9-8653-d5568d14b33e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f18510bd-eb10-43dd-a24a-8829416ff174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f13cbad3-d9b0-4ea9-8653-d5568d14b33e",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "0b67460d-241c-4f09-ad3a-1524e47f982a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f66cf108-3540-465c-b418-d15e71b49ff1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b67460d-241c-4f09-ad3a-1524e47f982a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7636c5e0-ac60-45b4-ba35-24bf7962ab8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b67460d-241c-4f09-ad3a-1524e47f982a",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "d9b05eea-36fd-4f26-9003-40b1d42423a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f46201bb-b50c-4a8b-a743-6fd59535ebcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9b05eea-36fd-4f26-9003-40b1d42423a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "413e3307-c50a-415b-a0d4-9927e0878a25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9b05eea-36fd-4f26-9003-40b1d42423a6",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "4663613c-3f8c-4dac-baa5-7b8bb228171b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "47038a3b-5019-4c32-8f0c-7b00ef477f31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4663613c-3f8c-4dac-baa5-7b8bb228171b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5222630-cc17-40aa-801d-2e6de1f720b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4663613c-3f8c-4dac-baa5-7b8bb228171b",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "5da8cca2-b150-44df-9a89-4619e03f7d07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "f11bff8e-a698-41e2-9262-b7c149708e12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5da8cca2-b150-44df-9a89-4619e03f7d07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8dd351c-3101-4888-a476-5c24ab60bdc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5da8cca2-b150-44df-9a89-4619e03f7d07",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "3bd9657f-404e-4e43-9c10-775b6a31688f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "364feccc-8edc-41b8-9d72-75f6d8fd5f61",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3bd9657f-404e-4e43-9c10-775b6a31688f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a2f895-dd08-4191-8631-5384b6e6d63c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3bd9657f-404e-4e43-9c10-775b6a31688f",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "2df2beef-b2df-4ded-874f-b5f9421420fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "ab6bcb9b-d2bc-4ca8-9dba-4f453d79e07a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2df2beef-b2df-4ded-874f-b5f9421420fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46c78b7f-8ffa-4f28-bf84-5ca82d2eb537",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2df2beef-b2df-4ded-874f-b5f9421420fa",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        },
        {
            "id": "2254ea17-2848-4fa3-aba6-060232fe366a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "compositeImage": {
                "id": "60841131-db16-45fa-9d26-75eafb5b48fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2254ea17-2848-4fa3-aba6-060232fe366a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f61915ea-8116-48b7-9755-0a7a8d091365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2254ea17-2848-4fa3-aba6-060232fe366a",
                    "LayerId": "100419bb-7335-4c80-ae9b-b885a0107ef8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "100419bb-7335-4c80-ae9b-b885a0107ef8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "96f982de-d0ed-45e1-91be-3f2eed97d9d8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}