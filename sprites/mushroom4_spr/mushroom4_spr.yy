{
    "id": "fe8460db-8dd3-49c3-8f03-f5009112ab80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mushroom4_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 0,
    "bbox_right": 26,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23145b2a-2790-4641-a9f5-cb74b39df4f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe8460db-8dd3-49c3-8f03-f5009112ab80",
            "compositeImage": {
                "id": "3f060aeb-2711-4ddd-bfd6-d1ce657b2741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23145b2a-2790-4641-a9f5-cb74b39df4f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aecf0e68-ce7a-451d-8483-4a9a7d87c075",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23145b2a-2790-4641-a9f5-cb74b39df4f3",
                    "LayerId": "fb526aa7-f570-4a36-b86a-63591df35595"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "fb526aa7-f570-4a36-b86a-63591df35595",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe8460db-8dd3-49c3-8f03-f5009112ab80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 13,
    "yorig": 10
}