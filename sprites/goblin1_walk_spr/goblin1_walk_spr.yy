{
    "id": "c31fd46a-a84f-4c0c-82a4-51cd9589fe66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin1_walk_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c71a8e8-1100-43e8-916c-e5c0e24dfcc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31fd46a-a84f-4c0c-82a4-51cd9589fe66",
            "compositeImage": {
                "id": "e18148e3-7ac7-4568-abae-7b19181f89e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c71a8e8-1100-43e8-916c-e5c0e24dfcc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136a832e-e613-4489-982c-83b46e21966c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c71a8e8-1100-43e8-916c-e5c0e24dfcc1",
                    "LayerId": "86b14fc8-3014-4b70-9f97-8f15fa98bdf7"
                }
            ]
        },
        {
            "id": "e0b3549e-0990-4f1b-a5fc-bfc730094715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31fd46a-a84f-4c0c-82a4-51cd9589fe66",
            "compositeImage": {
                "id": "1fbe6f76-5d6f-43a5-952e-73f361bf42e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0b3549e-0990-4f1b-a5fc-bfc730094715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b48fb6f7-2ae2-4f59-abdb-143967dca45f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0b3549e-0990-4f1b-a5fc-bfc730094715",
                    "LayerId": "86b14fc8-3014-4b70-9f97-8f15fa98bdf7"
                }
            ]
        },
        {
            "id": "92daacbe-3fcb-44fd-b4ef-7d02e5935bc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31fd46a-a84f-4c0c-82a4-51cd9589fe66",
            "compositeImage": {
                "id": "1f374d82-96f0-42e3-a90f-60f077477e82",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92daacbe-3fcb-44fd-b4ef-7d02e5935bc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "974a2c99-e044-4731-9b12-bf829e606370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92daacbe-3fcb-44fd-b4ef-7d02e5935bc9",
                    "LayerId": "86b14fc8-3014-4b70-9f97-8f15fa98bdf7"
                }
            ]
        },
        {
            "id": "f58821ee-aa78-48d2-8047-e5fa15b9083f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c31fd46a-a84f-4c0c-82a4-51cd9589fe66",
            "compositeImage": {
                "id": "9cc77b7a-178c-4301-94a4-b9c73ad36aab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f58821ee-aa78-48d2-8047-e5fa15b9083f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33c22f47-4b9d-47aa-8694-878e0afee087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f58821ee-aa78-48d2-8047-e5fa15b9083f",
                    "LayerId": "86b14fc8-3014-4b70-9f97-8f15fa98bdf7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "86b14fc8-3014-4b70-9f97-8f15fa98bdf7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c31fd46a-a84f-4c0c-82a4-51cd9589fe66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 60,
    "yorig": 188
}