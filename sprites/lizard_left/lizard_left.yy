{
    "id": "183eaae4-6938-40f4-b1de-695b5ef56818",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lizard_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 9,
    "bbox_right": 56,
    "bbox_top": 48,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8aeba064-e9bb-419e-a4fd-b6be9effe383",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183eaae4-6938-40f4-b1de-695b5ef56818",
            "compositeImage": {
                "id": "07e4c7bd-be6a-4929-ac70-a76cc12a8166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8aeba064-e9bb-419e-a4fd-b6be9effe383",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd6971fa-4c0d-4f4d-9837-54e7d8e54c49",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8aeba064-e9bb-419e-a4fd-b6be9effe383",
                    "LayerId": "c4298dd3-a8e5-4101-a3bf-1e4fa5ae88b5"
                }
            ]
        },
        {
            "id": "18f568bc-f7cb-4885-8e49-c6599aa6c1c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "183eaae4-6938-40f4-b1de-695b5ef56818",
            "compositeImage": {
                "id": "7d1e92a1-f430-4cd3-9a4e-8b5dd2fc15b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18f568bc-f7cb-4885-8e49-c6599aa6c1c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1513aa0-2d80-46d1-875c-62321d8ac0e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18f568bc-f7cb-4885-8e49-c6599aa6c1c7",
                    "LayerId": "c4298dd3-a8e5-4101-a3bf-1e4fa5ae88b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "c4298dd3-a8e5-4101-a3bf-1e4fa5ae88b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "183eaae4-6938-40f4-b1de-695b5ef56818",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 77,
    "xorig": 39,
    "yorig": 4
}