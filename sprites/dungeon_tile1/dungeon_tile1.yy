{
    "id": "8554fc03-4762-4feb-8e74-0dcedb6010b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_tile1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66adc9d2-2ea7-4846-b06c-01426c626650",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554fc03-4762-4feb-8e74-0dcedb6010b5",
            "compositeImage": {
                "id": "a83ad85c-ede6-42af-893f-fc5551216d35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66adc9d2-2ea7-4846-b06c-01426c626650",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7368ed7d-2bee-41b5-ba74-dd8b99b40021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66adc9d2-2ea7-4846-b06c-01426c626650",
                    "LayerId": "c13507cd-26e3-4b62-8f50-84d03bd28b45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c13507cd-26e3-4b62-8f50-84d03bd28b45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8554fc03-4762-4feb-8e74-0dcedb6010b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}