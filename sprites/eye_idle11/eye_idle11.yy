{
    "id": "3f4a1757-dc00-4979-a018-b4a46edd127c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "53f06e0b-4c28-4804-8af6-8f8d5e19d013",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4a1757-dc00-4979-a018-b4a46edd127c",
            "compositeImage": {
                "id": "30a6b346-1ad3-4941-b381-ddee2b128410",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "53f06e0b-4c28-4804-8af6-8f8d5e19d013",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd2b7897-471a-4fc5-8750-c065966008cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "53f06e0b-4c28-4804-8af6-8f8d5e19d013",
                    "LayerId": "a51dbb88-69e0-4d43-89e1-cfaf359e8e79"
                }
            ]
        },
        {
            "id": "2fb26635-e3a9-4f30-a67b-8d4b9a3b0d10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4a1757-dc00-4979-a018-b4a46edd127c",
            "compositeImage": {
                "id": "597aa292-079a-4cc9-9d71-98ef736528ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fb26635-e3a9-4f30-a67b-8d4b9a3b0d10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "306bf8c6-5401-4f8b-8127-39de3624663f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fb26635-e3a9-4f30-a67b-8d4b9a3b0d10",
                    "LayerId": "a51dbb88-69e0-4d43-89e1-cfaf359e8e79"
                }
            ]
        },
        {
            "id": "b802bf35-6099-4ab2-8e96-5498b8806219",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4a1757-dc00-4979-a018-b4a46edd127c",
            "compositeImage": {
                "id": "5e9e51da-9d51-4c24-9f1e-0f991f193b69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b802bf35-6099-4ab2-8e96-5498b8806219",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb08e902-ad17-4dc3-a0f3-6c1bf62403ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b802bf35-6099-4ab2-8e96-5498b8806219",
                    "LayerId": "a51dbb88-69e0-4d43-89e1-cfaf359e8e79"
                }
            ]
        },
        {
            "id": "6f5a68c4-d560-454d-9dc6-94d475afa12f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f4a1757-dc00-4979-a018-b4a46edd127c",
            "compositeImage": {
                "id": "4a08ce43-5331-4a75-8b8e-d0e126de3fa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f5a68c4-d560-454d-9dc6-94d475afa12f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de17a39-6449-4009-9a19-1071837f36cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f5a68c4-d560-454d-9dc6-94d475afa12f",
                    "LayerId": "a51dbb88-69e0-4d43-89e1-cfaf359e8e79"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "a51dbb88-69e0-4d43-89e1-cfaf359e8e79",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f4a1757-dc00-4979-a018-b4a46edd127c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}