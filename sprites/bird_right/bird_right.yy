{
    "id": "70fbeae3-22e1-482e-9d9d-2018220ba43b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc7fcb8f-6678-444a-836e-56a3d0881eeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70fbeae3-22e1-482e-9d9d-2018220ba43b",
            "compositeImage": {
                "id": "cd629114-7e88-4a03-bd85-bb852c1a64d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc7fcb8f-6678-444a-836e-56a3d0881eeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad805dc0-1a26-4fc5-aea5-3a01329d769c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc7fcb8f-6678-444a-836e-56a3d0881eeb",
                    "LayerId": "19101781-f62c-42a8-89fb-2a6567ee6688"
                }
            ]
        },
        {
            "id": "49bea114-a342-46bd-a408-537222ae70ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70fbeae3-22e1-482e-9d9d-2018220ba43b",
            "compositeImage": {
                "id": "b815f4b9-c276-4565-aae7-0034c99d1606",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49bea114-a342-46bd-a408-537222ae70ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "867230c0-50e7-4c9b-9320-381c765f7013",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49bea114-a342-46bd-a408-537222ae70ac",
                    "LayerId": "19101781-f62c-42a8-89fb-2a6567ee6688"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "19101781-f62c-42a8-89fb-2a6567ee6688",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70fbeae3-22e1-482e-9d9d-2018220ba43b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}