{
    "id": "0e0204ff-d9c3-46a9-89c9-259b2d80a172",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle19",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "835d7ab1-9872-4509-b0f8-25624cc97ad3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e0204ff-d9c3-46a9-89c9-259b2d80a172",
            "compositeImage": {
                "id": "5971711d-4cd0-4f3d-b133-5ac78098816a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835d7ab1-9872-4509-b0f8-25624cc97ad3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d634ea8-776e-48f5-8323-8400c9264d2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835d7ab1-9872-4509-b0f8-25624cc97ad3",
                    "LayerId": "43be20f7-367f-4d2e-afca-3e88cdbcf4fe"
                }
            ]
        },
        {
            "id": "4f7623a7-5829-4ab4-91de-10fa0e1ddb87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e0204ff-d9c3-46a9-89c9-259b2d80a172",
            "compositeImage": {
                "id": "f1f6485e-bf29-4508-b1de-4c1793a67fc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f7623a7-5829-4ab4-91de-10fa0e1ddb87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91fb2547-dc62-4bd3-a63a-c86e49b18936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f7623a7-5829-4ab4-91de-10fa0e1ddb87",
                    "LayerId": "43be20f7-367f-4d2e-afca-3e88cdbcf4fe"
                }
            ]
        },
        {
            "id": "205973cc-1e8a-4f2a-8572-c782634ad1b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e0204ff-d9c3-46a9-89c9-259b2d80a172",
            "compositeImage": {
                "id": "fa3cfa30-a455-4ec7-a7b1-b04de3fbb3bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "205973cc-1e8a-4f2a-8572-c782634ad1b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c81de665-049a-4026-80fe-48ec71a15a51",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "205973cc-1e8a-4f2a-8572-c782634ad1b0",
                    "LayerId": "43be20f7-367f-4d2e-afca-3e88cdbcf4fe"
                }
            ]
        },
        {
            "id": "b48117ae-7923-473e-8f50-05f54fb736f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e0204ff-d9c3-46a9-89c9-259b2d80a172",
            "compositeImage": {
                "id": "4711be0f-6e68-41fc-b3e2-9a6d3daeecf6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b48117ae-7923-473e-8f50-05f54fb736f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f48ff900-7017-402d-9c52-725e917fd0ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b48117ae-7923-473e-8f50-05f54fb736f0",
                    "LayerId": "43be20f7-367f-4d2e-afca-3e88cdbcf4fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "43be20f7-367f-4d2e-afca-3e88cdbcf4fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e0204ff-d9c3-46a9-89c9-259b2d80a172",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}