{
    "id": "aaa46fb7-1378-49e5-9eb1-d403eb1ec5b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54d29d30-ee75-4299-8165-4b4f405e1f68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaa46fb7-1378-49e5-9eb1-d403eb1ec5b0",
            "compositeImage": {
                "id": "607fa685-9643-466c-9b1e-f89d8b7ec6a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d29d30-ee75-4299-8165-4b4f405e1f68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e0f5cc-8329-4f56-94b6-43dac4aec0b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d29d30-ee75-4299-8165-4b4f405e1f68",
                    "LayerId": "6fc88b5a-3f68-4d7a-8fba-3ccf20294923"
                }
            ]
        },
        {
            "id": "eb02a81e-d1bf-4c32-abad-ce8c008a5b98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aaa46fb7-1378-49e5-9eb1-d403eb1ec5b0",
            "compositeImage": {
                "id": "394e0d22-1197-4ab6-89a1-fd2285e04227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb02a81e-d1bf-4c32-abad-ce8c008a5b98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "698b7a4a-2e59-44f7-bd2a-a0f230693d2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb02a81e-d1bf-4c32-abad-ce8c008a5b98",
                    "LayerId": "6fc88b5a-3f68-4d7a-8fba-3ccf20294923"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "6fc88b5a-3f68-4d7a-8fba-3ccf20294923",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aaa46fb7-1378-49e5-9eb1-d403eb1ec5b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}