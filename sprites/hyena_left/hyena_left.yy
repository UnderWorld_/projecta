{
    "id": "adcd095b-0dc5-4291-ba31-dc79b590221c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hyena_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72dc0d59-1239-45a2-b83b-f57b3c5400ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adcd095b-0dc5-4291-ba31-dc79b590221c",
            "compositeImage": {
                "id": "2fdec469-3a9f-4ad3-97a2-937f036c855d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72dc0d59-1239-45a2-b83b-f57b3c5400ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49b6c327-d021-48dc-b7ac-de325eb379c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72dc0d59-1239-45a2-b83b-f57b3c5400ae",
                    "LayerId": "07049800-15df-4b9e-9107-3ad5ce85c2e1"
                }
            ]
        },
        {
            "id": "c1709748-60de-4b6e-83b7-96732d512e81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adcd095b-0dc5-4291-ba31-dc79b590221c",
            "compositeImage": {
                "id": "159acd45-7f18-4599-9316-94c71017c6f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1709748-60de-4b6e-83b7-96732d512e81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c44d8340-7db0-4c44-9c38-7fc6bfda932d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1709748-60de-4b6e-83b7-96732d512e81",
                    "LayerId": "07049800-15df-4b9e-9107-3ad5ce85c2e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "07049800-15df-4b9e-9107-3ad5ce85c2e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adcd095b-0dc5-4291-ba31-dc79b590221c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 54
}