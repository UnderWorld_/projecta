{
    "id": "c3364e30-3733-4e73-b1e4-074b20fbdb47",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin2_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "213260d7-f63a-445a-8626-256b91f87a58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3364e30-3733-4e73-b1e4-074b20fbdb47",
            "compositeImage": {
                "id": "7573a8a6-f113-42bd-8b7a-f55ae75e3313",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "213260d7-f63a-445a-8626-256b91f87a58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1f3f223-e86c-4340-99fb-29d57af738f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "213260d7-f63a-445a-8626-256b91f87a58",
                    "LayerId": "63fb504c-8f95-4913-9f9e-389f9297b341"
                }
            ]
        },
        {
            "id": "c73f4edf-f468-4eca-89ad-38919b2e6587",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c3364e30-3733-4e73-b1e4-074b20fbdb47",
            "compositeImage": {
                "id": "8ca9590a-08c2-4124-93cf-8674464d4997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c73f4edf-f468-4eca-89ad-38919b2e6587",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63a6934c-1b43-45ac-ab35-073854b6195c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c73f4edf-f468-4eca-89ad-38919b2e6587",
                    "LayerId": "63fb504c-8f95-4913-9f9e-389f9297b341"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "63fb504c-8f95-4913-9f9e-389f9297b341",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c3364e30-3733-4e73-b1e4-074b20fbdb47",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}