{
    "id": "9b470c60-fc32-4cf7-a4de-392591aef5c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bunny2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 16,
    "bbox_right": 175,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "48d3f3cb-d5ed-4912-9ac5-a86214e89f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b470c60-fc32-4cf7-a4de-392591aef5c1",
            "compositeImage": {
                "id": "cb786a29-03dd-4eb7-8d72-0548bc78713a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48d3f3cb-d5ed-4912-9ac5-a86214e89f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f220b75f-9e35-402b-8e85-b5b035338005",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48d3f3cb-d5ed-4912-9ac5-a86214e89f45",
                    "LayerId": "36dca77f-6f31-4b8d-a170-c3ae83017ca0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "36dca77f-6f31-4b8d-a170-c3ae83017ca0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b470c60-fc32-4cf7-a4de-392591aef5c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 103,
    "yorig": 206
}