{
    "id": "0fd7e91f-7d34-4a57-b3e7-16ef6b84fa39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right19",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9fc84432-6885-4a34-8fb2-b73aed273323",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fd7e91f-7d34-4a57-b3e7-16ef6b84fa39",
            "compositeImage": {
                "id": "31f7d425-d131-47e5-8eaa-13b01f1e50bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9fc84432-6885-4a34-8fb2-b73aed273323",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc10231a-5d94-4a0b-b18f-72d69981f6a5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9fc84432-6885-4a34-8fb2-b73aed273323",
                    "LayerId": "58e44341-129e-4464-8e02-faff50f2e3dd"
                }
            ]
        },
        {
            "id": "5ea32b9f-6a33-4b8d-82ee-5f0cc97c424d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fd7e91f-7d34-4a57-b3e7-16ef6b84fa39",
            "compositeImage": {
                "id": "16196772-980b-4d01-9abd-e2a1168b0673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ea32b9f-6a33-4b8d-82ee-5f0cc97c424d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b83556b0-fa68-47cb-9ad6-30c4d0ddac25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ea32b9f-6a33-4b8d-82ee-5f0cc97c424d",
                    "LayerId": "58e44341-129e-4464-8e02-faff50f2e3dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "58e44341-129e-4464-8e02-faff50f2e3dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fd7e91f-7d34-4a57-b3e7-16ef6b84fa39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}