{
    "id": "d1d84e74-71ed-4aa9-9fd3-0b4d68ffe317",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin1_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cc7dcf1b-aedc-4dc8-959b-96f5c2febd01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d84e74-71ed-4aa9-9fd3-0b4d68ffe317",
            "compositeImage": {
                "id": "3c21722f-0e7a-4d44-81ea-1d751cda226e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc7dcf1b-aedc-4dc8-959b-96f5c2febd01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "940e9fd4-9d3e-4c1f-bb6e-6f1ab726c23c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc7dcf1b-aedc-4dc8-959b-96f5c2febd01",
                    "LayerId": "61ca3d1a-4aee-4052-b293-fd675e18e5ff"
                }
            ]
        },
        {
            "id": "25a35a66-fdbc-4204-9272-22427c6772b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d84e74-71ed-4aa9-9fd3-0b4d68ffe317",
            "compositeImage": {
                "id": "ce24ce3f-1e92-42ce-8278-23cd7c26eb77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a35a66-fdbc-4204-9272-22427c6772b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08264bd8-e363-4e05-8fab-823d09b5295b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a35a66-fdbc-4204-9272-22427c6772b0",
                    "LayerId": "61ca3d1a-4aee-4052-b293-fd675e18e5ff"
                }
            ]
        },
        {
            "id": "e9b5da6f-fc6e-4369-98a8-d3fbc1281498",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d84e74-71ed-4aa9-9fd3-0b4d68ffe317",
            "compositeImage": {
                "id": "251a630c-3255-4c15-a14c-7083eaf4a98b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b5da6f-fc6e-4369-98a8-d3fbc1281498",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "223579c9-0b4c-49a8-9c6b-5e273cbd7d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b5da6f-fc6e-4369-98a8-d3fbc1281498",
                    "LayerId": "61ca3d1a-4aee-4052-b293-fd675e18e5ff"
                }
            ]
        },
        {
            "id": "322b979e-46d0-4669-8e14-4f3247c7bca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1d84e74-71ed-4aa9-9fd3-0b4d68ffe317",
            "compositeImage": {
                "id": "21f3bec4-7c3c-4dfb-bdb4-e872899cbb6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322b979e-46d0-4669-8e14-4f3247c7bca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb06a68-487f-4f88-8b26-81e85fe17f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322b979e-46d0-4669-8e14-4f3247c7bca5",
                    "LayerId": "61ca3d1a-4aee-4052-b293-fd675e18e5ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "61ca3d1a-4aee-4052-b293-fd675e18e5ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1d84e74-71ed-4aa9-9fd3-0b4d68ffe317",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}