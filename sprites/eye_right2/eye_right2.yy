{
    "id": "260722d4-def2-4288-a8d2-031d806810a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d579634-2631-4d2f-bb89-3e53c09b06c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "260722d4-def2-4288-a8d2-031d806810a9",
            "compositeImage": {
                "id": "bfa7c971-1f02-4e72-8759-5f98b4e9ece1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d579634-2631-4d2f-bb89-3e53c09b06c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff8a0576-bbf4-474c-aae8-969704ac4bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d579634-2631-4d2f-bb89-3e53c09b06c8",
                    "LayerId": "afd56fb5-0f62-4c73-8415-30ca90fdae17"
                }
            ]
        },
        {
            "id": "fcc630ca-2725-4d4d-80a7-dc5601b3e5c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "260722d4-def2-4288-a8d2-031d806810a9",
            "compositeImage": {
                "id": "aa1d7d57-d3ab-49ed-bf41-30cf6add5c7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcc630ca-2725-4d4d-80a7-dc5601b3e5c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b307839-179a-4986-8c74-941d9736a572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcc630ca-2725-4d4d-80a7-dc5601b3e5c4",
                    "LayerId": "afd56fb5-0f62-4c73-8415-30ca90fdae17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "afd56fb5-0f62-4c73-8415-30ca90fdae17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "260722d4-def2-4288-a8d2-031d806810a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}