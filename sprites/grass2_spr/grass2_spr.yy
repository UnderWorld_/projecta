{
    "id": "3ccf30af-db6e-4748-93f6-9081b1d9cf36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grass2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 0,
    "bbox_right": 56,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3375b3b5-ec8e-45e3-b70f-0abb3d3dd55a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ccf30af-db6e-4748-93f6-9081b1d9cf36",
            "compositeImage": {
                "id": "51e9c17c-3d88-46fb-a7ca-daa1124c2535",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3375b3b5-ec8e-45e3-b70f-0abb3d3dd55a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c362ee0-8230-4c6e-b547-f9bf1d9929db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3375b3b5-ec8e-45e3-b70f-0abb3d3dd55a",
                    "LayerId": "eadcae2e-1236-47e5-a8ac-6c999fd8ab70"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 56,
    "layers": [
        {
            "id": "eadcae2e-1236-47e5-a8ac-6c999fd8ab70",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ccf30af-db6e-4748-93f6-9081b1d9cf36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 0,
    "yorig": 0
}