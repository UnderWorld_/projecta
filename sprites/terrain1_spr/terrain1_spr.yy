{
    "id": "22ff01a2-1844-4318-86b6-354ee140609b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "terrain1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 0,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c9a87530-8f8a-4c00-8017-aa9f19100dd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22ff01a2-1844-4318-86b6-354ee140609b",
            "compositeImage": {
                "id": "a41c8d30-3b76-48a0-ac31-6bda241e655d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9a87530-8f8a-4c00-8017-aa9f19100dd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50db647-e255-482b-9fa4-96131f05f037",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9a87530-8f8a-4c00-8017-aa9f19100dd0",
                    "LayerId": "5aa7fc13-b961-4328-b626-e6698a8521d4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 75,
    "layers": [
        {
            "id": "5aa7fc13-b961-4328-b626-e6698a8521d4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22ff01a2-1844-4318-86b6-354ee140609b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 75,
    "xorig": 0,
    "yorig": 0
}