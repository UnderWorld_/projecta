{
    "id": "05420c45-7a47-4803-85c1-9745b0781eb9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "map1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 430,
    "bbox_left": 0,
    "bbox_right": 301,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cadcd6f-be2b-4480-ab86-aa8650a5397e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05420c45-7a47-4803-85c1-9745b0781eb9",
            "compositeImage": {
                "id": "490653dc-9e24-434f-8fb0-e4aca7cf161d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cadcd6f-be2b-4480-ab86-aa8650a5397e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed40091f-f17f-4e5d-a490-a4d52627bcff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cadcd6f-be2b-4480-ab86-aa8650a5397e",
                    "LayerId": "2439120f-97f8-4a86-944a-326c0bf0e453"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 431,
    "layers": [
        {
            "id": "2439120f-97f8-4a86-944a-326c0bf0e453",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05420c45-7a47-4803-85c1-9745b0781eb9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 304,
    "xorig": 0,
    "yorig": 0
}