{
    "id": "a7ce3ecd-a0ba-4b0d-b811-ff614b009fbe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 11,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c948978e-3f76-41ef-a55a-0939ca8d007a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7ce3ecd-a0ba-4b0d-b811-ff614b009fbe",
            "compositeImage": {
                "id": "c6b06c87-cf72-41d8-af5e-4e63a7649bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c948978e-3f76-41ef-a55a-0939ca8d007a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40ef3e61-b9fc-4e58-8913-3f5fadde4efb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c948978e-3f76-41ef-a55a-0939ca8d007a",
                    "LayerId": "19f7a407-4d69-4ab4-80a7-827a4851980f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "19f7a407-4d69-4ab4-80a7-827a4851980f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7ce3ecd-a0ba-4b0d-b811-ff614b009fbe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}