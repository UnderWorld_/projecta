{
    "id": "6af0558e-3bb4-4fb6-8f0a-3d77010bfbac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "water_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6201a231-2e2a-4f6b-9dab-7f93194f744a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6af0558e-3bb4-4fb6-8f0a-3d77010bfbac",
            "compositeImage": {
                "id": "bf60c35b-3c2f-4e38-b1a1-f4f1bae8a4b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6201a231-2e2a-4f6b-9dab-7f93194f744a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "497de232-9566-49a6-b91b-874608526241",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6201a231-2e2a-4f6b-9dab-7f93194f744a",
                    "LayerId": "5952eb27-6974-4d67-a966-bc744bd9dedd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "5952eb27-6974-4d67-a966-bc744bd9dedd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6af0558e-3bb4-4fb6-8f0a-3d77010bfbac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 96
}