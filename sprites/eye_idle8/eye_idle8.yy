{
    "id": "ecf23e57-0056-451c-bee3-8d58fe6556a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7a92b0b-58a5-4fed-be80-1ec2fad0149c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf23e57-0056-451c-bee3-8d58fe6556a3",
            "compositeImage": {
                "id": "e6d635a6-80f1-45a7-8176-1cafcb8d4623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a92b0b-58a5-4fed-be80-1ec2fad0149c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fcab5be-b710-4eab-becb-4467cd468501",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a92b0b-58a5-4fed-be80-1ec2fad0149c",
                    "LayerId": "82faa11a-8dd4-49ec-8c49-40211219dece"
                }
            ]
        },
        {
            "id": "7b4ba3ad-9dbf-4634-bbc9-6bae9c06d1f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf23e57-0056-451c-bee3-8d58fe6556a3",
            "compositeImage": {
                "id": "c751477b-d75c-4881-92ab-fe4b7b9d1d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b4ba3ad-9dbf-4634-bbc9-6bae9c06d1f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8807932-8069-4354-b802-1a3eb377db1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b4ba3ad-9dbf-4634-bbc9-6bae9c06d1f8",
                    "LayerId": "82faa11a-8dd4-49ec-8c49-40211219dece"
                }
            ]
        },
        {
            "id": "1f3c7439-658a-4f27-9206-b82fbb3e13fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf23e57-0056-451c-bee3-8d58fe6556a3",
            "compositeImage": {
                "id": "714548a4-09c0-4af1-b8f1-607c41eab477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f3c7439-658a-4f27-9206-b82fbb3e13fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d65ba1e2-57c7-4ccc-bedb-e60a9ca79343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f3c7439-658a-4f27-9206-b82fbb3e13fe",
                    "LayerId": "82faa11a-8dd4-49ec-8c49-40211219dece"
                }
            ]
        },
        {
            "id": "92f38250-836e-4eb3-842f-cb241aa62969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecf23e57-0056-451c-bee3-8d58fe6556a3",
            "compositeImage": {
                "id": "201367b5-42fb-4250-b923-673e697f5bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92f38250-836e-4eb3-842f-cb241aa62969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f3f1c7f-78b0-4eeb-a4b3-23ae040c6c6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92f38250-836e-4eb3-842f-cb241aa62969",
                    "LayerId": "82faa11a-8dd4-49ec-8c49-40211219dece"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "82faa11a-8dd4-49ec-8c49-40211219dece",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecf23e57-0056-451c-bee3-8d58fe6556a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}