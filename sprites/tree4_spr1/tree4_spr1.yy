{
    "id": "15c051eb-0030-47af-8758-b034d5adf07f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "tree4_spr1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 12,
    "bbox_right": 20,
    "bbox_top": 70,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3d19d0c-9d55-4c85-ae3a-1796c63c2306",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "15c051eb-0030-47af-8758-b034d5adf07f",
            "compositeImage": {
                "id": "df8d9384-6b15-4824-b20e-5312ff74408b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3d19d0c-9d55-4c85-ae3a-1796c63c2306",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0fd690-7247-47af-844f-43aa24a2689c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3d19d0c-9d55-4c85-ae3a-1796c63c2306",
                    "LayerId": "9490c240-2ac8-4f7b-9d8d-510a45df2c6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "9490c240-2ac8-4f7b-9d8d-510a45df2c6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "15c051eb-0030-47af-8758-b034d5adf07f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 27,
    "xorig": 12,
    "yorig": 86
}