{
    "id": "e4bafe4b-af1a-4fa3-9116-c65659997a74",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair1_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 5,
    "bbox_right": 74,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6947351a-d481-413f-a34e-832ba7d21a8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bafe4b-af1a-4fa3-9116-c65659997a74",
            "compositeImage": {
                "id": "72e931c6-ecfa-4020-ab9c-c200b357b747",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6947351a-d481-413f-a34e-832ba7d21a8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c3b6ee1-47cd-4988-9547-9f74a937d929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6947351a-d481-413f-a34e-832ba7d21a8d",
                    "LayerId": "a6123657-78d5-45e9-b858-adf457ffdca4"
                }
            ]
        },
        {
            "id": "a23c2093-a444-4149-a3cb-919e8310fac8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bafe4b-af1a-4fa3-9116-c65659997a74",
            "compositeImage": {
                "id": "9776279d-5d0c-4791-a140-4b49ba0afaa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a23c2093-a444-4149-a3cb-919e8310fac8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9560185-7718-4847-a889-fe37981ce350",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a23c2093-a444-4149-a3cb-919e8310fac8",
                    "LayerId": "a6123657-78d5-45e9-b858-adf457ffdca4"
                }
            ]
        },
        {
            "id": "c3c65694-94cd-445c-ac23-3328f535d682",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bafe4b-af1a-4fa3-9116-c65659997a74",
            "compositeImage": {
                "id": "d756707f-ab48-466b-bef8-32ec7f585d9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3c65694-94cd-445c-ac23-3328f535d682",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8cc8f8a-b574-4a64-a917-77ba6c3aee25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3c65694-94cd-445c-ac23-3328f535d682",
                    "LayerId": "a6123657-78d5-45e9-b858-adf457ffdca4"
                }
            ]
        },
        {
            "id": "7498a949-a4c4-48eb-8660-8709df004c72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bafe4b-af1a-4fa3-9116-c65659997a74",
            "compositeImage": {
                "id": "de111dfd-629e-4878-93ab-a9ec250d412a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7498a949-a4c4-48eb-8660-8709df004c72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef51d030-3771-427d-8a24-b0b0671a861c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7498a949-a4c4-48eb-8660-8709df004c72",
                    "LayerId": "a6123657-78d5-45e9-b858-adf457ffdca4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "a6123657-78d5-45e9-b858-adf457ffdca4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4bafe4b-af1a-4fa3-9116-c65659997a74",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}