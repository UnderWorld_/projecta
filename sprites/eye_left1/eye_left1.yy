{
    "id": "03c6b33e-7794-4afe-8d36-7de952c85628",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b58f498d-78db-4883-8b7f-9af2a9fb9207",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c6b33e-7794-4afe-8d36-7de952c85628",
            "compositeImage": {
                "id": "5db0560d-1c6d-4477-a244-3aa9ada81f46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b58f498d-78db-4883-8b7f-9af2a9fb9207",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f68da97-0310-4b78-a347-dbf5c418b2b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b58f498d-78db-4883-8b7f-9af2a9fb9207",
                    "LayerId": "5eaccb43-4124-44ea-b375-8aa5b843ed62"
                }
            ]
        },
        {
            "id": "52e0c3e9-ee0c-4dc9-bd45-bf3137066b38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03c6b33e-7794-4afe-8d36-7de952c85628",
            "compositeImage": {
                "id": "7d481507-20ff-460e-9475-df8dcd0fa184",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e0c3e9-ee0c-4dc9-bd45-bf3137066b38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9aab7664-364d-4db4-8325-d08d97415638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e0c3e9-ee0c-4dc9-bd45-bf3137066b38",
                    "LayerId": "5eaccb43-4124-44ea-b375-8aa5b843ed62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "5eaccb43-4124-44ea-b375-8aa5b843ed62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03c6b33e-7794-4afe-8d36-7de952c85628",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}