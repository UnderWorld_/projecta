{
    "id": "423f58fc-73a2-4fd2-9ece-8fde4fac7039",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "n_player_walk_left_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 56,
    "bbox_right": 85,
    "bbox_top": 44,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e93a61f-ebad-4d2c-bd92-a672a5c04118",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423f58fc-73a2-4fd2-9ece-8fde4fac7039",
            "compositeImage": {
                "id": "68fa4d6c-deeb-4181-b206-87e84f17d194",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e93a61f-ebad-4d2c-bd92-a672a5c04118",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6b78da4-6b6d-415d-ab4f-296bc9a614d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e93a61f-ebad-4d2c-bd92-a672a5c04118",
                    "LayerId": "0b171f96-941e-4e2c-9169-89d1790bde48"
                }
            ]
        },
        {
            "id": "2e8ca8bf-cdd0-4262-9e34-0064715606fd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423f58fc-73a2-4fd2-9ece-8fde4fac7039",
            "compositeImage": {
                "id": "856172d0-f5c6-420b-b775-54584ad43acf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8ca8bf-cdd0-4262-9e34-0064715606fd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "516874cb-3acd-4f1b-872b-8401365b0161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8ca8bf-cdd0-4262-9e34-0064715606fd",
                    "LayerId": "0b171f96-941e-4e2c-9169-89d1790bde48"
                }
            ]
        },
        {
            "id": "6b9bfd51-d7cd-4b16-bf23-d8454c7f56d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423f58fc-73a2-4fd2-9ece-8fde4fac7039",
            "compositeImage": {
                "id": "64799f85-2a8b-4945-b6ee-600a5ac9b964",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b9bfd51-d7cd-4b16-bf23-d8454c7f56d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8a245c3-c3b1-4102-86e0-6d094a164f72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b9bfd51-d7cd-4b16-bf23-d8454c7f56d9",
                    "LayerId": "0b171f96-941e-4e2c-9169-89d1790bde48"
                }
            ]
        },
        {
            "id": "261c0e41-e694-4f06-bbcb-8d47121ff1cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "423f58fc-73a2-4fd2-9ece-8fde4fac7039",
            "compositeImage": {
                "id": "8c198278-d049-42f6-8652-cfa09fcf3608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "261c0e41-e694-4f06-bbcb-8d47121ff1cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "241056c9-7f7a-4d15-ba78-954d8e606b38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "261c0e41-e694-4f06-bbcb-8d47121ff1cc",
                    "LayerId": "0b171f96-941e-4e2c-9169-89d1790bde48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "0b171f96-941e-4e2c-9169-89d1790bde48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "423f58fc-73a2-4fd2-9ece-8fde4fac7039",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 85
}