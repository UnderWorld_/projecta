{
    "id": "69218b53-cc60-40d7-a0c7-41d7691f7b43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_idle_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 35,
    "bbox_right": 103,
    "bbox_top": 34,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6fd002ca-4a9b-404d-94eb-8425450656ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69218b53-cc60-40d7-a0c7-41d7691f7b43",
            "compositeImage": {
                "id": "8b8f050e-3cf8-40fb-aa1c-9d779033f97a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fd002ca-4a9b-404d-94eb-8425450656ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f1f1970-783a-4718-a8b2-d0461e84a19b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fd002ca-4a9b-404d-94eb-8425450656ae",
                    "LayerId": "948da2ee-95be-4f10-bba8-d794c63e4e75"
                }
            ]
        },
        {
            "id": "a962fa88-61e5-47a1-b9c1-bbbe474ab8d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69218b53-cc60-40d7-a0c7-41d7691f7b43",
            "compositeImage": {
                "id": "f3b3a0ba-d5ee-46e3-885a-ec54f93931fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a962fa88-61e5-47a1-b9c1-bbbe474ab8d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f1e39b8-bf6c-4ad2-b3e7-75e79c0f92d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a962fa88-61e5-47a1-b9c1-bbbe474ab8d2",
                    "LayerId": "948da2ee-95be-4f10-bba8-d794c63e4e75"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "948da2ee-95be-4f10-bba8-d794c63e4e75",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69218b53-cc60-40d7-a0c7-41d7691f7b43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 37,
    "yorig": 47
}