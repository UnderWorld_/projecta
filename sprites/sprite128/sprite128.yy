{
    "id": "5211161b-f889-4e65-a6be-ad2291e68c27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite128",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 165,
    "bbox_left": 131,
    "bbox_right": 260,
    "bbox_top": 120,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e9df13b-c712-416d-868c-40cd29162744",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "b2587561-efc6-4c47-8771-0f9d827063a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e9df13b-c712-416d-868c-40cd29162744",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5f145da-acd5-4d86-9e96-066971260337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e9df13b-c712-416d-868c-40cd29162744",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "32f6aa59-7283-494f-9bff-ed891e4d29c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "bf400a73-1849-4c61-9e51-bd94d808574e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32f6aa59-7283-494f-9bff-ed891e4d29c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2bad24e-d29c-4f09-8615-7219b0508f39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32f6aa59-7283-494f-9bff-ed891e4d29c9",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "5750a813-4ab9-4972-bd74-6a5540d08245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "9a572f99-96d0-4078-9087-94949f3e38d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5750a813-4ab9-4972-bd74-6a5540d08245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29408ed5-a07b-4d07-94ca-3476281bc71d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5750a813-4ab9-4972-bd74-6a5540d08245",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "8c6491e3-a9fb-40b6-bc8c-ac27f5e2e1c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "8e481236-deb3-4bb1-8e08-e284b8cd2290",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6491e3-a9fb-40b6-bc8c-ac27f5e2e1c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27303ccf-bb75-48bc-ad34-ab35a021b962",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6491e3-a9fb-40b6-bc8c-ac27f5e2e1c1",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "d17cdd59-120f-45f8-b271-db504816b11d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "0e9aa852-f690-4885-ab94-ff5fb718b43a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d17cdd59-120f-45f8-b271-db504816b11d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8da410ca-9fe3-4b5a-ab7c-1a34ea5b7632",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d17cdd59-120f-45f8-b271-db504816b11d",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "9b03df8b-a2a3-423a-93d2-6a4a48d3ead4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "f8e34b1c-5bd1-4c10-8c58-a36b5cf4578c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b03df8b-a2a3-423a-93d2-6a4a48d3ead4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5b5ee7-4faa-4272-bf32-df9856213221",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b03df8b-a2a3-423a-93d2-6a4a48d3ead4",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "e6b3fd6b-8978-4474-8a5e-af140d5b39e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "6c9e97ba-5a6d-4c15-9adf-84be29722ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e6b3fd6b-8978-4474-8a5e-af140d5b39e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ddf2398-cd6f-416a-9c0d-9d2dc21b0eaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e6b3fd6b-8978-4474-8a5e-af140d5b39e7",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "81bd4fc1-c1d3-445b-8040-d54df2ddc95d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "ebd16c68-cb4b-4768-8a07-2df9c9c1e082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81bd4fc1-c1d3-445b-8040-d54df2ddc95d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef3adb05-2e4c-4796-b425-b500dae05708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81bd4fc1-c1d3-445b-8040-d54df2ddc95d",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "381eb834-f056-4fe8-a6ca-18265d73b242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "dbb0a98c-7063-4fe0-a7a6-5c5303fb3bb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "381eb834-f056-4fe8-a6ca-18265d73b242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29eaa1be-a297-4806-b86e-b529d19381d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "381eb834-f056-4fe8-a6ca-18265d73b242",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "3be302ad-4852-48d2-a71d-d980c14ca2e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "135fbd25-e2b0-4056-95f6-61b0231e198e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3be302ad-4852-48d2-a71d-d980c14ca2e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72c0b1ab-24c7-4a90-bab5-d7070c2df199",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3be302ad-4852-48d2-a71d-d980c14ca2e8",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "566bec21-cd9d-4735-a18f-6b86f96ad353",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "28ab8d98-1dfa-4d20-9b83-56f71a95bf44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "566bec21-cd9d-4735-a18f-6b86f96ad353",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fcfd51cf-fe2d-42fb-9a05-5ee6dfca5d11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "566bec21-cd9d-4735-a18f-6b86f96ad353",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "0aece512-a5e3-4444-be11-7276497d5734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "71e1232d-8e78-4fec-9b2c-97343f3396b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aece512-a5e3-4444-be11-7276497d5734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffcea975-bb2e-4849-bfdf-9eab23f9ba3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aece512-a5e3-4444-be11-7276497d5734",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "a8f0e998-b51f-45b9-bc4f-593c04d0e657",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "876d565b-5b05-45c2-b1a1-9b8e56a4e071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f0e998-b51f-45b9-bc4f-593c04d0e657",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "afa65df4-43ec-4f13-9835-1c874fce5107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f0e998-b51f-45b9-bc4f-593c04d0e657",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "35d64d4c-fb61-425b-8636-7a1b0c2affc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "9c5e429c-f70e-4d98-aa9c-6b23740d220f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35d64d4c-fb61-425b-8636-7a1b0c2affc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8fcdad0-14a5-4414-af4e-17e1defa2639",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35d64d4c-fb61-425b-8636-7a1b0c2affc2",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "f1e7f7cb-90d4-4b79-b7e9-4aa2723761ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "bb6051f2-8393-498b-9f84-3a7fc789164b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1e7f7cb-90d4-4b79-b7e9-4aa2723761ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a163eb3e-2581-46ba-a53b-e700ec9d996c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1e7f7cb-90d4-4b79-b7e9-4aa2723761ec",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "cd9f701f-0637-4d84-a14a-742b6d8fb131",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "7eea7b45-68e2-4561-81ab-fa914ad1ef29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd9f701f-0637-4d84-a14a-742b6d8fb131",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8259c54c-89db-4ad4-8195-dbe55246d112",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd9f701f-0637-4d84-a14a-742b6d8fb131",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "0b263e46-e150-4d10-940f-cbd7263f9a79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "0176c03d-ba59-472f-9950-eca6818fd359",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b263e46-e150-4d10-940f-cbd7263f9a79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d54b41cf-48cc-48d2-85a1-6a52af2b69d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b263e46-e150-4d10-940f-cbd7263f9a79",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "1c4555c3-815f-49c2-932c-419cddb88ac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "eaa99763-fd6d-44ee-befd-837f02094ca3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c4555c3-815f-49c2-932c-419cddb88ac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73bc9ffe-41c1-496a-8bc6-00d016e38910",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c4555c3-815f-49c2-932c-419cddb88ac7",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "44bdaa3e-86fc-4aac-8f40-9ca4e7e1b686",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "e860f188-36ef-44f9-9c13-91060d345896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44bdaa3e-86fc-4aac-8f40-9ca4e7e1b686",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f909eb91-c0ab-45a3-a67c-f014e0b3e0aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44bdaa3e-86fc-4aac-8f40-9ca4e7e1b686",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "83e04771-8f6a-46d9-9975-e79b272eff65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "fddff1a2-fbbf-4623-8c7b-9013cdababb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e04771-8f6a-46d9-9975-e79b272eff65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4669ad2-b0fe-4377-97b3-ced05fe17125",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e04771-8f6a-46d9-9975-e79b272eff65",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "91c51b61-0c3e-4822-82fd-2fcd8f73099c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "9daed140-a762-4367-b413-6f951808b667",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c51b61-0c3e-4822-82fd-2fcd8f73099c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c123468-624c-40c7-8dda-c2207bdc12cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c51b61-0c3e-4822-82fd-2fcd8f73099c",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "14126460-514c-412b-80ea-490acc69b3a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "a2e14c24-8be5-4c28-81db-d30350ecaa73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "14126460-514c-412b-80ea-490acc69b3a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0edb164e-3390-43d6-a941-c64011303515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "14126460-514c-412b-80ea-490acc69b3a1",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "9496546f-70c5-4df8-9809-ea949d45d81e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "4b04b2e8-3592-452a-8011-0753626a544f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9496546f-70c5-4df8-9809-ea949d45d81e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dc916f9-3810-44b1-b699-893dee690082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9496546f-70c5-4df8-9809-ea949d45d81e",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "97e7a757-2a20-4753-8cc3-9f35d4424270",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "7f8eda3d-040d-4f2c-9d5c-095dd4628800",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e7a757-2a20-4753-8cc3-9f35d4424270",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8edb50cc-9f94-4da8-b7fa-5422f2c77aed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e7a757-2a20-4753-8cc3-9f35d4424270",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "5091133c-9672-489e-a9fa-7235b4ef493a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "3417005b-96e4-4d5e-aac1-e00df60373a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5091133c-9672-489e-a9fa-7235b4ef493a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f8c8510-94e3-47cf-b63d-f5a3c3fc711b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5091133c-9672-489e-a9fa-7235b4ef493a",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "ab3e4621-7d96-4ff6-8fc8-a31ca33064f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "565dfb6b-ae9a-49ab-82b9-13ba22f1751c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab3e4621-7d96-4ff6-8fc8-a31ca33064f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "981a632f-b736-47a5-bb93-dab408b99996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab3e4621-7d96-4ff6-8fc8-a31ca33064f0",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "d9a9e81e-507a-4bd1-ae2d-092246a73e1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "ccfc7ea6-bbdb-433d-a9c4-567e7eb66152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9a9e81e-507a-4bd1-ae2d-092246a73e1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b13796-a22b-4985-a562-13188dcc4a76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9a9e81e-507a-4bd1-ae2d-092246a73e1b",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "2b6fd119-d44d-4c7e-a0cc-95f34058a6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "210a8b07-afcf-4e0c-9de3-af723fb26030",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b6fd119-d44d-4c7e-a0cc-95f34058a6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "436017eb-2699-44a2-9fb2-a77a24afb072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b6fd119-d44d-4c7e-a0cc-95f34058a6b5",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "930d75f8-5821-41d8-a355-09c6b07f0a78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "3c4a117a-5abe-45aa-ae2a-2382194d88fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "930d75f8-5821-41d8-a355-09c6b07f0a78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32955a24-4017-4793-82ab-526e1dfc90b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "930d75f8-5821-41d8-a355-09c6b07f0a78",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "9af63892-e3ce-40b4-80d5-6b99164d07a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "d4bf99ef-8be8-4a66-8d0d-48c4f34adb15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9af63892-e3ce-40b4-80d5-6b99164d07a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25404bf7-0f3e-45ed-be74-5747fa99da38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9af63892-e3ce-40b4-80d5-6b99164d07a7",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "df5680a2-fd03-42b2-9436-3da86fc0ae8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "16b21266-3ac9-4fb7-9ae1-4ae6080d1a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df5680a2-fd03-42b2-9436-3da86fc0ae8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e597495-7eaf-4973-ab97-9eb6a0ba7f71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df5680a2-fd03-42b2-9436-3da86fc0ae8c",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "d0cf7a08-13a7-42ac-9282-d9a5b09914e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "5ec7ab40-99ef-4a3f-8705-cc8df1e284e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0cf7a08-13a7-42ac-9282-d9a5b09914e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a04f27b-e703-40a9-aadb-9e2dfd917cfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0cf7a08-13a7-42ac-9282-d9a5b09914e8",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "ca4d1a04-1c22-4371-b848-aa3b69528828",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "6c2aaadd-8585-40e5-ab39-bde0c2788565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca4d1a04-1c22-4371-b848-aa3b69528828",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52a9fb48-5e92-4281-b456-ba457d0b8961",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca4d1a04-1c22-4371-b848-aa3b69528828",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "57559d82-9655-47d6-ac65-47d93268398a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "8917f092-578d-44f4-81ad-9dc7310d8ef0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57559d82-9655-47d6-ac65-47d93268398a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29164b42-8fe6-4120-8366-fe2f97cdfe5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57559d82-9655-47d6-ac65-47d93268398a",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "5a6ca7c1-acb2-434c-b15c-aa4dcbed5fa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "b1435aaf-28cc-4c16-a3b5-5fc3ec44c1cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6ca7c1-acb2-434c-b15c-aa4dcbed5fa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98aca34b-5cb2-4166-a86a-ab720964fa89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6ca7c1-acb2-434c-b15c-aa4dcbed5fa1",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "8fdf53cc-ceb0-4393-bdf4-24405cc53d3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "27c22143-9545-49af-811a-082fe4d72406",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fdf53cc-ceb0-4393-bdf4-24405cc53d3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4575a39f-e586-49ad-9343-4e773bf1c787",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fdf53cc-ceb0-4393-bdf4-24405cc53d3a",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "fc37e0ff-044a-40c1-a1c2-fe6e22c0184f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "9d9ae41f-0ebe-4bf5-84d2-5975a9a57e9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc37e0ff-044a-40c1-a1c2-fe6e22c0184f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aaaf170-110c-42b6-93b4-3a0879336839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc37e0ff-044a-40c1-a1c2-fe6e22c0184f",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "45254708-107b-4e87-bb12-5e3cb0450cd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "d4f936f3-1291-454c-aebf-4615322965a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "45254708-107b-4e87-bb12-5e3cb0450cd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df9c762e-dc25-46cd-8206-475be1aaf533",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "45254708-107b-4e87-bb12-5e3cb0450cd8",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "b3a97c38-8677-4635-8100-c07785ae3f50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "7a912f48-b232-42e5-bbac-7de97fa2fddc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3a97c38-8677-4635-8100-c07785ae3f50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eff67bd0-05fd-49ec-ac41-e52540faef0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3a97c38-8677-4635-8100-c07785ae3f50",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "74960b4e-4e42-4a40-8bba-c2c253683757",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "452520ee-05cc-49d3-9607-477c4bfb0521",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "74960b4e-4e42-4a40-8bba-c2c253683757",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3b9d998-5a21-4454-941c-37d488591a8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "74960b4e-4e42-4a40-8bba-c2c253683757",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "9c2c4628-ce53-420a-a12e-dafe9053228f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "bf83704b-caaf-4386-96a9-92a09749a07e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c2c4628-ce53-420a-a12e-dafe9053228f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5024b992-b876-41ae-befd-5440905192d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c2c4628-ce53-420a-a12e-dafe9053228f",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "bde74e66-338d-4821-903a-4d832b90cc77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "9dbbe52e-61fb-4a83-9419-24e48f37cdbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde74e66-338d-4821-903a-4d832b90cc77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bd17af2-aeae-4bbb-b8b9-9376621c047c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde74e66-338d-4821-903a-4d832b90cc77",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "050aacfc-53c4-42f0-86db-8dcafdedc234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "fd16bfbf-e2bf-4dd2-9fa8-be889ed79a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "050aacfc-53c4-42f0-86db-8dcafdedc234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f5a3711-c83b-47d4-884c-8db56fecd9f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "050aacfc-53c4-42f0-86db-8dcafdedc234",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "36a1e8df-4c47-4a29-98dc-32ca853d9dfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "87e3c8e5-5768-4081-8a7f-4cbf0dfec064",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36a1e8df-4c47-4a29-98dc-32ca853d9dfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78a94610-ef98-4669-af09-5618bb55dc33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36a1e8df-4c47-4a29-98dc-32ca853d9dfe",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "2d9914b9-31b6-4872-9d94-d91f32f705f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "1cb3accf-eb98-44d4-882e-6a2ea4f4d458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d9914b9-31b6-4872-9d94-d91f32f705f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f361d28-079a-48ac-bc02-2c7035d97d8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d9914b9-31b6-4872-9d94-d91f32f705f7",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "33c49b4e-6dcc-4339-8f1b-26a3c238d107",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "9eb1cf94-8572-4a28-b14a-f0d411efbe02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33c49b4e-6dcc-4339-8f1b-26a3c238d107",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b3cdf79-1033-4ed4-8cb8-17320bb9c275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33c49b4e-6dcc-4339-8f1b-26a3c238d107",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "703fb569-274d-42ce-97a5-4469aebf26b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "f953d128-2fd2-4811-8b82-b2e44832fc6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703fb569-274d-42ce-97a5-4469aebf26b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f821b328-8fad-4c49-992a-cd8a44349c0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703fb569-274d-42ce-97a5-4469aebf26b6",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "e9b8c954-d106-4155-923a-e2999e3ddad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "0963d039-41f9-43ae-97e4-b11fcf1052cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9b8c954-d106-4155-923a-e2999e3ddad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce8bf22e-9fad-44dc-b09c-6208eaf321ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9b8c954-d106-4155-923a-e2999e3ddad8",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "44791888-841c-48bd-8767-3435f5794d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "a9710ebd-dfdd-4210-9cbc-32b42f70ed48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44791888-841c-48bd-8767-3435f5794d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78c50e4-77ad-4f4d-8ee8-33b0ad22fc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44791888-841c-48bd-8767-3435f5794d9d",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "0bb02d53-48d8-4a93-bbb2-3cbdc2754732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "da2846f0-ce5b-45b0-83fe-6cf9f030bbf0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb02d53-48d8-4a93-bbb2-3cbdc2754732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46fba9a5-8e7f-42e2-ab99-6fdf39eb471b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb02d53-48d8-4a93-bbb2-3cbdc2754732",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "638ea3b4-c200-4f29-9cc0-1f23a54bfb8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "a72d747e-d52b-402f-a5ee-79064a4f69e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "638ea3b4-c200-4f29-9cc0-1f23a54bfb8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad830b90-a4f5-4592-9e14-6e8a178410fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "638ea3b4-c200-4f29-9cc0-1f23a54bfb8e",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "c908d772-5949-40cf-ba5d-693ea3f49829",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "4206560d-b1d7-4156-bd25-1db12cf277d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c908d772-5949-40cf-ba5d-693ea3f49829",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c80c26-c923-45b1-b7e4-37a8b9f9f203",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c908d772-5949-40cf-ba5d-693ea3f49829",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "687f82ca-af31-496b-b896-86fb8f83083e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "2a501ad6-6f4f-4013-8697-09899efa2c98",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "687f82ca-af31-496b-b896-86fb8f83083e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19390c52-64f2-497f-8165-b369c2cd2b3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "687f82ca-af31-496b-b896-86fb8f83083e",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "bacf2400-0ee6-45fc-bd38-213438952db1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "2873e5a5-1662-4b14-a71f-6493d4bc056b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bacf2400-0ee6-45fc-bd38-213438952db1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "999f0eaf-6658-4fc2-b8d8-84a962acacb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bacf2400-0ee6-45fc-bd38-213438952db1",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "c8e6bf82-a5a1-41b1-8cae-a938f834776a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "8fcf2fcb-4cb9-4ccf-b04b-519c02bfe169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8e6bf82-a5a1-41b1-8cae-a938f834776a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c2fa691-25b8-4245-ac57-6f3b5d01f4ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8e6bf82-a5a1-41b1-8cae-a938f834776a",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "091ae9b6-dbbe-4feb-9429-4ce4fabea003",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "3693a425-9cd7-4e09-8678-5f460933a11e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "091ae9b6-dbbe-4feb-9429-4ce4fabea003",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06ee6eba-dc7d-41a1-a74e-43f9678405e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "091ae9b6-dbbe-4feb-9429-4ce4fabea003",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "48e36cd2-8e92-4bd3-8aa7-7b42d0bd5256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "8bc84bc4-b2cb-4e61-af33-0915c7006fd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48e36cd2-8e92-4bd3-8aa7-7b42d0bd5256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "694330b7-e583-4989-9dba-7b4a72760be7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48e36cd2-8e92-4bd3-8aa7-7b42d0bd5256",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "edaac8c0-7533-402e-b8c9-17634d4d2590",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "63dc55c7-bc0a-43f0-a82c-3a8ccc63c083",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edaac8c0-7533-402e-b8c9-17634d4d2590",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c9830ff-75a3-45a3-9ac7-49ace0f5f857",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edaac8c0-7533-402e-b8c9-17634d4d2590",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "eeecd879-423f-42d8-91db-e3890b2fd061",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "500f1249-6335-4d31-a5ad-f94899e5fa0e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eeecd879-423f-42d8-91db-e3890b2fd061",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d0d3ce06-b7b1-4afa-a355-a9affc888a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eeecd879-423f-42d8-91db-e3890b2fd061",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "0d65cdbd-0511-4275-a0fc-10c541ba67fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "ce9c2868-ebdc-47f7-b985-2ac4ca98db6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d65cdbd-0511-4275-a0fc-10c541ba67fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8140cd8e-984c-4f95-a8a0-c4beeb7df293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d65cdbd-0511-4275-a0fc-10c541ba67fe",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "8d3b4ae2-c052-49d0-99ba-38984c2edc72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "afdd327e-4740-4d2f-ade2-276881d64567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d3b4ae2-c052-49d0-99ba-38984c2edc72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1033369-118b-4010-9022-98a54832c418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d3b4ae2-c052-49d0-99ba-38984c2edc72",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "434660d1-11d5-45b7-a0e3-dacd2069ec57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "1ae7836c-7ac8-484f-859e-645a625db865",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "434660d1-11d5-45b7-a0e3-dacd2069ec57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3536e78e-0fb6-48f0-92d0-b40957c72bda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "434660d1-11d5-45b7-a0e3-dacd2069ec57",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "6c3323a0-ca2c-4e03-aa3c-afeb9adfe50d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "7b85ccbb-2958-4ec0-bdf6-37129d68dedd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c3323a0-ca2c-4e03-aa3c-afeb9adfe50d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ee75ea0-b0f5-45e1-be7c-f4e4e7d5b255",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c3323a0-ca2c-4e03-aa3c-afeb9adfe50d",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "deb09488-71db-4b5e-8a41-122229f7acb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "05124550-3e76-41cb-937d-1085d5fb2edd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "deb09488-71db-4b5e-8a41-122229f7acb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67e7fbb1-e548-4bfe-a3dc-f4eb27b8c6b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "deb09488-71db-4b5e-8a41-122229f7acb8",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "4647784f-b954-4f44-80f6-60597e010f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "7a784253-3317-4665-a7fb-1b47c38d6507",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4647784f-b954-4f44-80f6-60597e010f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8699bd84-ad20-45fb-80f4-df3fa78a983e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4647784f-b954-4f44-80f6-60597e010f4b",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        },
        {
            "id": "d5dda473-f12c-4271-8e13-e19e85bf472c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "compositeImage": {
                "id": "08cbbb17-cb83-47b4-899d-cd65f2bc2c57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5dda473-f12c-4271-8e13-e19e85bf472c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90322ac9-bb0e-4583-adbd-f148c868c832",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5dda473-f12c-4271-8e13-e19e85bf472c",
                    "LayerId": "e7db5442-d094-414e-aaf5-1abddb384b84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "e7db5442-d094-414e-aaf5-1abddb384b84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 248,
    "xorig": 124,
    "yorig": 240
}