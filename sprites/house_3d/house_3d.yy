{
    "id": "ee76d032-ec1c-4287-9b8d-178426a2370b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house_3d",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 114,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4b0722e8-3a32-4042-a5c0-91d3325f1ea7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "d3492130-5847-4bb3-95b6-3705c0d4cc29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b0722e8-3a32-4042-a5c0-91d3325f1ea7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04920dee-db71-49c7-9605-a272e34b4683",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b0722e8-3a32-4042-a5c0-91d3325f1ea7",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "2da8c610-6c9c-46c4-81f3-60e31d9b857b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b6eecf8a-631b-43d9-9d3b-1dcb3e6d838b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2da8c610-6c9c-46c4-81f3-60e31d9b857b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8de3154-1186-46cb-a40a-3a520d12697b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2da8c610-6c9c-46c4-81f3-60e31d9b857b",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "bf2a27e4-2c27-45e0-91ad-469c1a609a39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "a3315685-17b1-43f8-837d-5a9aa647a043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf2a27e4-2c27-45e0-91ad-469c1a609a39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3f551e9-d8a5-4711-846b-0e5f7d609568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf2a27e4-2c27-45e0-91ad-469c1a609a39",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "bb97c9d4-25ce-4b44-a138-eccae2094ce2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "e3900359-830a-4b58-ba39-6bb82bccfb45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb97c9d4-25ce-4b44-a138-eccae2094ce2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc1a5e93-479b-435e-a53e-337f0838e446",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb97c9d4-25ce-4b44-a138-eccae2094ce2",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "04a33282-eb44-4cfc-9031-c4d30b495489",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "5e90a2a1-9da8-45c6-a3f5-3641dd5a6a7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04a33282-eb44-4cfc-9031-c4d30b495489",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cca5ba04-a72d-4b48-96b4-a8efc8792365",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04a33282-eb44-4cfc-9031-c4d30b495489",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "3619ae69-f5f5-4446-9c76-f6a2652ac16b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "a0b92c19-9d5b-49e9-a8b2-b8577e2dd207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3619ae69-f5f5-4446-9c76-f6a2652ac16b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a12dbc-ca12-4713-8c45-dee18c6d433c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3619ae69-f5f5-4446-9c76-f6a2652ac16b",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "20e99976-2f1e-473b-b545-2c5147bfb2fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "44c6b472-e066-4a05-b683-972cf11e1684",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20e99976-2f1e-473b-b545-2c5147bfb2fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2806516a-f6e4-492f-8b52-e6b4cb43a27b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20e99976-2f1e-473b-b545-2c5147bfb2fe",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "fc245f27-d675-442a-a63a-4f83600cd9a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "24550216-987d-48e8-b6c0-4eaeaa8b5cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc245f27-d675-442a-a63a-4f83600cd9a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7d4c891-f383-462f-8da3-317b26e16057",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc245f27-d675-442a-a63a-4f83600cd9a8",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "2d4a6f86-e854-4fa0-b7f5-cc6b378a8090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "4f3a3557-04d8-46aa-ad72-e35b9b315485",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d4a6f86-e854-4fa0-b7f5-cc6b378a8090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6036d08-56c9-4e33-82ae-52d33e758480",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d4a6f86-e854-4fa0-b7f5-cc6b378a8090",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "48f6a244-5f94-4d8a-bd14-f29c6b198b63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "7b9a0054-9136-420a-b8e4-f7e9c954de12",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48f6a244-5f94-4d8a-bd14-f29c6b198b63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b131227-d320-4dd1-8ef7-88e2a210254d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48f6a244-5f94-4d8a-bd14-f29c6b198b63",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "2324a9ec-0d23-459b-92e3-da6e1315dfc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "cf65f884-9d62-46a5-9398-b71209529974",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2324a9ec-0d23-459b-92e3-da6e1315dfc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f951123-9f73-4db4-8fa5-a5a0788aa349",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2324a9ec-0d23-459b-92e3-da6e1315dfc0",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "6d45dce0-c10c-4622-bb2c-e7d5a4a75c21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "6bd892fd-08ba-4639-9ce0-50293f1c235d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d45dce0-c10c-4622-bb2c-e7d5a4a75c21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d4e86de-10d3-4ac2-94e8-4efca1fc42cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d45dce0-c10c-4622-bb2c-e7d5a4a75c21",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "e95ec48f-4a89-4fb8-b207-0c5cd9a0a660",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "640bbace-9187-4212-91c1-0555b5546e26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e95ec48f-4a89-4fb8-b207-0c5cd9a0a660",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9263ffc9-14eb-4e93-a9ca-6da408a66d60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e95ec48f-4a89-4fb8-b207-0c5cd9a0a660",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "ce01e1b8-586d-475d-8a38-245fd7c7bbc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "c7ce5ae8-194f-40ca-978d-af4b15b81ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ce01e1b8-586d-475d-8a38-245fd7c7bbc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc9d592d-5182-41fb-b7fc-ca008fb6d1bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ce01e1b8-586d-475d-8a38-245fd7c7bbc1",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "872de64f-5080-4922-91e2-b6f24d50e1b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "9ce3643f-a0a4-4cc0-b390-e355df1bd5b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "872de64f-5080-4922-91e2-b6f24d50e1b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d731e890-2054-41dd-ad4e-77ac4c785ed3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "872de64f-5080-4922-91e2-b6f24d50e1b6",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "2398a5b2-795c-4745-a1fe-e62eecb1a0f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b1ae2d54-c38c-4478-aed1-aab0c4a744ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2398a5b2-795c-4745-a1fe-e62eecb1a0f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "598d7bb4-9b05-4a39-adda-a96407e40add",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2398a5b2-795c-4745-a1fe-e62eecb1a0f0",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "8b2893c2-4359-44a3-bb0c-e6bbd5fc3487",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "4c52feba-2439-47f1-afa7-e021d9f474e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b2893c2-4359-44a3-bb0c-e6bbd5fc3487",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c4ccfb-abc7-49ff-aafa-b68333906397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b2893c2-4359-44a3-bb0c-e6bbd5fc3487",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "4bf7e3e2-64af-4d1f-8fc4-b08041548e76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "be896448-538a-4f4e-8377-4dc555b426b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf7e3e2-64af-4d1f-8fc4-b08041548e76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0de37a0b-e01e-4493-8fad-bd69faee2ed7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf7e3e2-64af-4d1f-8fc4-b08041548e76",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "033c9d50-5586-42f2-b07e-cd00d722d5ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "0ab0fe71-248e-4dcc-b126-99cdf9ac2f0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "033c9d50-5586-42f2-b07e-cd00d722d5ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54073959-089b-4084-a55a-b8ae128a0646",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "033c9d50-5586-42f2-b07e-cd00d722d5ec",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "97f905f4-ffab-48a9-af72-1c7f5de78191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "c65fa009-7b56-4269-a59b-d0b22ca7f2cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97f905f4-ffab-48a9-af72-1c7f5de78191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad30292-5a89-4696-8e9b-19acc37342bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97f905f4-ffab-48a9-af72-1c7f5de78191",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "861c21bd-0872-4a63-8baa-06cf86378218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "64b20423-bf0f-4130-adb9-b50677f70bcb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "861c21bd-0872-4a63-8baa-06cf86378218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3f7de3-afe7-484e-a1dd-542f148b17e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "861c21bd-0872-4a63-8baa-06cf86378218",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f8f26415-7a70-4039-83a7-bf30c569951a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "ef0dccc7-dde5-4278-9c80-92d52da39b78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8f26415-7a70-4039-83a7-bf30c569951a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aefcb846-fa05-4786-a121-4ac902f1386e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8f26415-7a70-4039-83a7-bf30c569951a",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "35f20cee-49da-4976-ad89-a66d14cffc82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "bb78cede-1329-4760-a84a-8afc86744444",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f20cee-49da-4976-ad89-a66d14cffc82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88e99ca-d9be-4bac-b176-dc186642722e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f20cee-49da-4976-ad89-a66d14cffc82",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "cac3bf22-bb41-489e-8a60-b77c2bb574d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "046f304b-3a88-4fd2-81d9-0b5197638ed5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cac3bf22-bb41-489e-8a60-b77c2bb574d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4d74b9a-1974-45fb-8481-f4d09bcdb1ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cac3bf22-bb41-489e-8a60-b77c2bb574d4",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "bdff7ccb-2277-4bc9-b32d-ef577dc0e6ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b3cdbd92-9ab0-4bbd-887f-13702080b465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdff7ccb-2277-4bc9-b32d-ef577dc0e6ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a6223b1-8ecb-4c8e-bcac-25649e4ef0be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdff7ccb-2277-4bc9-b32d-ef577dc0e6ca",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "4c85ec81-7f1c-40e9-a567-a4430c474ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "638579cd-12e1-41bf-a434-9832cce75310",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c85ec81-7f1c-40e9-a567-a4430c474ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fd0f670-fda6-4a31-babf-e9f384addfe2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c85ec81-7f1c-40e9-a567-a4430c474ac3",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "b10827df-4864-48a6-bc64-d60ef58429ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "42b211e8-09bc-48a1-8944-13b993731426",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b10827df-4864-48a6-bc64-d60ef58429ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac96e58f-221a-4dcd-acdd-0115c55cc2ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b10827df-4864-48a6-bc64-d60ef58429ac",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "e47aabe7-d8e4-4014-a9f3-4cc013c5ea92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "019e780d-ae63-4284-9b80-112cf0587599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e47aabe7-d8e4-4014-a9f3-4cc013c5ea92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "695db366-fdd3-4dec-8c7f-2b6c9c4917b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e47aabe7-d8e4-4014-a9f3-4cc013c5ea92",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "634aad7f-9182-456e-ab00-2ac240b6cb81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "18b755f5-15c3-4e05-915a-c52873d9a051",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "634aad7f-9182-456e-ab00-2ac240b6cb81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b640f67a-2af2-464d-89f1-87ee651dd718",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "634aad7f-9182-456e-ab00-2ac240b6cb81",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "749a33f8-79b2-4d2b-872c-33638a27e43d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "d9033cd4-1ded-498c-9310-93a989e45cbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "749a33f8-79b2-4d2b-872c-33638a27e43d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a13daff-c56d-425a-8155-8428d727fab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "749a33f8-79b2-4d2b-872c-33638a27e43d",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "67863fae-d50d-4b60-9ab8-0edeb32abeca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "00c08e4a-65db-47d8-891e-286699cbc8c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67863fae-d50d-4b60-9ab8-0edeb32abeca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a100116a-9c82-4e03-ab4c-32e9dae25669",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67863fae-d50d-4b60-9ab8-0edeb32abeca",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "948d14bd-4673-4d84-b921-a7f112bcba1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "c2a52ba8-9cbd-4152-aec0-41fa0f12a67e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "948d14bd-4673-4d84-b921-a7f112bcba1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a003faa-6b26-45af-8a5a-0be917450ceb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "948d14bd-4673-4d84-b921-a7f112bcba1e",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f36240f0-bdab-4567-acc1-b421c45bc731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b13b5559-a9a5-4242-8044-1c040faf4e7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f36240f0-bdab-4567-acc1-b421c45bc731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "892e158a-fe63-4bb4-800c-838eda48627d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f36240f0-bdab-4567-acc1-b421c45bc731",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "d98eb12e-3372-4d43-9644-884aa740f982",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "1a114603-a7bd-4d3b-92d0-179ddfeb06eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d98eb12e-3372-4d43-9644-884aa740f982",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d6da240-f975-4b7f-b4c9-61897158d2f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d98eb12e-3372-4d43-9644-884aa740f982",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "108c57d0-e5a8-4e3e-8328-9b0d9522d79a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b30bceed-c712-4150-bdec-6b6fa4f904eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "108c57d0-e5a8-4e3e-8328-9b0d9522d79a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "186d9b72-629f-4b69-86c7-b529cb7047e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "108c57d0-e5a8-4e3e-8328-9b0d9522d79a",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "388ef820-3c2f-4dd9-9810-b399f7ffc125",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "627caba5-8d94-4157-a8c3-53d3e7e71ebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "388ef820-3c2f-4dd9-9810-b399f7ffc125",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8c63957-c67a-417a-abad-c75f075acca5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "388ef820-3c2f-4dd9-9810-b399f7ffc125",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "ad3d25ff-f871-41b8-8ee5-42eb2c82bae5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "fe72f963-2ff3-44e9-83fc-ebe20c85a89c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad3d25ff-f871-41b8-8ee5-42eb2c82bae5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e86db391-efaa-4cd0-bdbf-8bfa79959e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad3d25ff-f871-41b8-8ee5-42eb2c82bae5",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "38877176-b100-4cf1-bc0f-a1225e1943a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "01297dd9-f3ec-4c48-b2d0-2419c6167e29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38877176-b100-4cf1-bc0f-a1225e1943a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "432b3c04-e6db-4f02-a5b2-839982ed88dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38877176-b100-4cf1-bc0f-a1225e1943a0",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "27ddd87a-f3a4-4fd7-b669-5f43e7f33138",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "5bd820ae-90bd-41ad-aa06-860f8c34b31a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27ddd87a-f3a4-4fd7-b669-5f43e7f33138",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c6a9721-d72a-4be6-9215-85c6ed4281cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27ddd87a-f3a4-4fd7-b669-5f43e7f33138",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "d15f039e-b503-4924-83bf-307684fa0793",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "4123577c-79c9-4d34-9fee-0767fe6d527c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d15f039e-b503-4924-83bf-307684fa0793",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc52ed29-f1db-4b5b-b5ee-fc64e10728b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d15f039e-b503-4924-83bf-307684fa0793",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "49b2ee35-de07-4fee-bbc0-cdc9565da6d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "4ebb4fd7-8ed3-458f-8b56-17324020d573",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49b2ee35-de07-4fee-bbc0-cdc9565da6d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "063475ba-4170-4dbd-b098-4dbd6b8888be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49b2ee35-de07-4fee-bbc0-cdc9565da6d5",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "3e2a13bb-87ea-4ddd-af99-7dc0cb50ca92",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "c02cddba-a9d7-479d-8de5-640b31601725",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e2a13bb-87ea-4ddd-af99-7dc0cb50ca92",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01583fbb-f0c4-41ba-8264-fbd6e41711ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e2a13bb-87ea-4ddd-af99-7dc0cb50ca92",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "b8bd2ac9-880c-46f3-8efe-f26f840b50cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "5a83da90-e9dc-45c0-bca2-bce69805323b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8bd2ac9-880c-46f3-8efe-f26f840b50cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a882508-2f13-4fb6-b927-17b190d49065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8bd2ac9-880c-46f3-8efe-f26f840b50cd",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "086d1e81-9f21-46e5-bfa1-dd53e798cbff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "f252df5f-38a0-45f5-b8e8-41001e7b22d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "086d1e81-9f21-46e5-bfa1-dd53e798cbff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af5ea069-52cf-44c2-8563-650b66fecb08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "086d1e81-9f21-46e5-bfa1-dd53e798cbff",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "09319e3a-61fe-4ce9-8b4c-1ac7f2e541bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "884d47b1-6658-4403-ab7f-059ecc2c7ce1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09319e3a-61fe-4ce9-8b4c-1ac7f2e541bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0a5cb96-200c-43f6-9d0a-212654005a3a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09319e3a-61fe-4ce9-8b4c-1ac7f2e541bd",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "a1bfbba5-77b3-4684-8e49-8d78701466b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "f39fcea4-ddc2-43cd-a5ab-a8d2c84a87fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1bfbba5-77b3-4684-8e49-8d78701466b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4faeb64f-678e-48a0-a537-3e74bd420ffc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1bfbba5-77b3-4684-8e49-8d78701466b8",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "7237eae6-d086-4b08-bd5e-73e8f1cec4e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "2bb882f4-dc52-440f-97fa-4288a5e2fb57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7237eae6-d086-4b08-bd5e-73e8f1cec4e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8657fab0-00c5-4434-b913-db08b99b0838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7237eae6-d086-4b08-bd5e-73e8f1cec4e6",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "4e7d0964-408f-486f-8ed8-957a35695394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "36d32c54-76b1-4e34-9bd6-acf2fb8d36af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e7d0964-408f-486f-8ed8-957a35695394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f56455a6-4667-46d3-a28e-a6decf46b10b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e7d0964-408f-486f-8ed8-957a35695394",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "0fc8ec15-4b61-41cc-b080-9010cf04c6d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b69ae67c-902d-4441-9843-8c56abe90c1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fc8ec15-4b61-41cc-b080-9010cf04c6d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c40b05f2-c98a-425c-92f6-c13b485ae844",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fc8ec15-4b61-41cc-b080-9010cf04c6d6",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "5d7b2494-328d-4733-aa3a-294e38264136",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "42d10a2a-689c-4de4-b963-35fd17b1f02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d7b2494-328d-4733-aa3a-294e38264136",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac6b7480-9711-49a9-8e3b-74fc35fe5968",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d7b2494-328d-4733-aa3a-294e38264136",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "7756a3f6-28d4-4506-9611-7d17fa56339c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "51cdb071-a117-40a0-8940-ff8c78f70505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7756a3f6-28d4-4506-9611-7d17fa56339c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f580c679-1884-432a-a14b-03dfb155d540",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7756a3f6-28d4-4506-9611-7d17fa56339c",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "51cf0c9d-ad06-493b-bc94-edb4902bfb9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "76ea27c4-98e0-455f-9fd5-3c04553de0ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51cf0c9d-ad06-493b-bc94-edb4902bfb9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8130d1-2fdd-4aac-83c7-d92c95fdee58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51cf0c9d-ad06-493b-bc94-edb4902bfb9f",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "351c2cf2-aa32-4c83-9fcf-c32ee5612f88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "15b2ab3f-4e02-411f-aa39-38a721eae702",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351c2cf2-aa32-4c83-9fcf-c32ee5612f88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8dc92a0-04b3-4cfd-b6a9-fcd52c8df40b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351c2cf2-aa32-4c83-9fcf-c32ee5612f88",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "a09bb425-dcff-4b04-b023-93dcf816eb91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "5fb40ea0-21bb-4e5e-a10d-7d2c99b91dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a09bb425-dcff-4b04-b023-93dcf816eb91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60343008-e70c-4eb8-98be-fac07308370b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a09bb425-dcff-4b04-b023-93dcf816eb91",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f159083c-a57a-4400-bd69-3052c8f863b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "24d88d61-aa4d-44d6-aad1-9ff1d91e2058",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f159083c-a57a-4400-bd69-3052c8f863b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c399d2a-3de6-463b-865b-6021b936cc28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f159083c-a57a-4400-bd69-3052c8f863b8",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f9d2e861-3dd1-413a-8a4d-52e5f3b4b8e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "9be01913-c7d2-4071-a87b-2c9808e38843",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9d2e861-3dd1-413a-8a4d-52e5f3b4b8e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba5478c8-afe6-4744-bdb6-f6a602377352",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9d2e861-3dd1-413a-8a4d-52e5f3b4b8e9",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "5a765879-ff0e-45a6-8021-2f350a27c0fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "e5d0fe30-1024-413d-ac7e-8465014c5171",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a765879-ff0e-45a6-8021-2f350a27c0fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38da4e22-3b55-4b23-bdd4-799c03ca1364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a765879-ff0e-45a6-8021-2f350a27c0fe",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "7bad2458-85cc-49ef-850b-a83d596d779d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b1258db3-9530-4f2a-b559-d64caf149733",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bad2458-85cc-49ef-850b-a83d596d779d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1257705-f9d9-457e-9a15-e5609433790f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bad2458-85cc-49ef-850b-a83d596d779d",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "92d91b3d-39b8-48bb-84b7-3e921a8eb0c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "629d1a9a-9a80-4371-8fab-49ad9d7d24c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d91b3d-39b8-48bb-84b7-3e921a8eb0c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "abbd7dc0-8156-4683-b75b-743272ec973e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d91b3d-39b8-48bb-84b7-3e921a8eb0c3",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "21cf3a82-c072-4239-a150-c77c4e7d2dc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "89826e0c-e9ef-460b-baca-3ef010196c9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21cf3a82-c072-4239-a150-c77c4e7d2dc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c269335e-60e3-4f07-ba85-fd772904e513",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21cf3a82-c072-4239-a150-c77c4e7d2dc3",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "bc44a49c-16c5-4a36-baae-e5bd2d1facae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "7da7424e-8878-4cf5-8b91-4eebab7349d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc44a49c-16c5-4a36-baae-e5bd2d1facae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "984fc776-f752-4892-b2f9-14b5b60337ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc44a49c-16c5-4a36-baae-e5bd2d1facae",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "ddeb3ce9-a0cd-4886-814f-4bfbdccbd9d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "d07ec939-b009-4b3b-8428-272e84772bb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddeb3ce9-a0cd-4886-814f-4bfbdccbd9d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9d7a638-0bb8-49fb-b518-742bd00862fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddeb3ce9-a0cd-4886-814f-4bfbdccbd9d2",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "eb95868b-025c-4202-8fbe-42b3ee005d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "e9295fee-7139-41ad-834b-645187696c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb95868b-025c-4202-8fbe-42b3ee005d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be5aed2b-1958-4f50-a0b5-1cceacee402e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb95868b-025c-4202-8fbe-42b3ee005d41",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "08f36d0b-cc74-40a4-ac80-a6c929d351c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "9f803aa9-59d1-43ef-b1a3-557beab67a00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08f36d0b-cc74-40a4-ac80-a6c929d351c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c698c2a-818f-424e-8134-c89974c247e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08f36d0b-cc74-40a4-ac80-a6c929d351c6",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "71cace01-dec5-4196-8cc1-06e4237a9015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "645e638f-5627-4ced-8e3e-7e0ea6032307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71cace01-dec5-4196-8cc1-06e4237a9015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9b8cb3c-206d-4e6f-9f9c-eb13423d66c0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71cace01-dec5-4196-8cc1-06e4237a9015",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "a88e6711-8297-46a9-85a9-847c7fd89480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "5664a0e8-3912-45b8-8656-67bb8a9de977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a88e6711-8297-46a9-85a9-847c7fd89480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b6910d7-faad-4fde-8669-1afbc84fe63b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a88e6711-8297-46a9-85a9-847c7fd89480",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "c5e787fb-b90f-448b-a551-5238d980c63a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "0734eeb3-3c6f-47b7-ba1e-3339491df0af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5e787fb-b90f-448b-a551-5238d980c63a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "803b06a2-d06c-42c1-a908-7204e94efe5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5e787fb-b90f-448b-a551-5238d980c63a",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f0b88d62-0533-428c-adbe-fe78d01d6130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "9c321fd3-d6a0-4d18-aa02-3843b7b9f304",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b88d62-0533-428c-adbe-fe78d01d6130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e717c0b-8cf4-4e51-a5f1-8815dc3c9bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b88d62-0533-428c-adbe-fe78d01d6130",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "47e89b42-729a-4166-96e1-8b17fc63c4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "14aa903b-12d4-4de7-8f46-38fb473c2ca0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e89b42-729a-4166-96e1-8b17fc63c4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a6eecab-8c34-4840-bcae-8e58d8c0cb5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e89b42-729a-4166-96e1-8b17fc63c4c9",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "5985a74b-f22d-4916-b16f-077e14b6bd0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "06f1dca5-de81-4436-89ca-44c424053bb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5985a74b-f22d-4916-b16f-077e14b6bd0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d61d7d8-ab7b-4edf-ad09-70612118a74f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5985a74b-f22d-4916-b16f-077e14b6bd0a",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "0a2c8908-2301-4ead-9632-142a39f4c411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "d174728b-ed0b-454b-99b1-5da6c81dec76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a2c8908-2301-4ead-9632-142a39f4c411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bb13a0c-0314-4554-8a3c-9cb3bb566a38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a2c8908-2301-4ead-9632-142a39f4c411",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "0485efc3-7c5a-4e2a-b478-4a8a14a19169",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "31756c80-f2f8-49b0-9655-762e194b7f7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0485efc3-7c5a-4e2a-b478-4a8a14a19169",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5be6b0f-0dd0-45f4-8383-36baa2392605",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0485efc3-7c5a-4e2a-b478-4a8a14a19169",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "1ccaa120-6318-4b21-b7f5-5c4c73cb217a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "6a0aa589-9759-4627-80b1-f4583b7acfe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1ccaa120-6318-4b21-b7f5-5c4c73cb217a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6981030-0c93-4f64-a329-a887d743abc8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1ccaa120-6318-4b21-b7f5-5c4c73cb217a",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "10af45e5-3cd0-40c3-8f6d-6ada666d3001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "720560a8-9d22-4fd0-b73e-f091232930ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10af45e5-3cd0-40c3-8f6d-6ada666d3001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "850f8694-1ea3-40a9-8bfc-f9e6826ee11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10af45e5-3cd0-40c3-8f6d-6ada666d3001",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "00aa4c04-ae4b-4862-9165-181fd5a902c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "28323af6-d94f-4c18-87a4-a3b626258854",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00aa4c04-ae4b-4862-9165-181fd5a902c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0573a917-456e-45d0-a5dd-592a1342faf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00aa4c04-ae4b-4862-9165-181fd5a902c2",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f7275ce3-853a-4a74-bd8c-4aa223afa666",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "eaec984c-93d7-4ae4-b290-b7b93a76c92c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7275ce3-853a-4a74-bd8c-4aa223afa666",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cce20d9e-3cd2-469f-8351-dc9b60b254a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7275ce3-853a-4a74-bd8c-4aa223afa666",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "80503b0f-99e3-4cf3-894b-bac1781fb302",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "3463cc1a-b072-4624-82d5-53f0cc32d3d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80503b0f-99e3-4cf3-894b-bac1781fb302",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4faaab3e-a98f-44bd-ac78-2a244f841553",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80503b0f-99e3-4cf3-894b-bac1781fb302",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "32015ba8-7139-4398-b804-0c42ce0333d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "6a0cc993-1275-4a59-9a27-a2f3e68c7da6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32015ba8-7139-4398-b804-0c42ce0333d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55190f23-5af1-4820-88a3-b20a12f8a89c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32015ba8-7139-4398-b804-0c42ce0333d7",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "402abbc2-43b9-4e37-b4f2-e2f9c2ee2bc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "1647ddf6-bd2e-4d64-8fbc-61f092f42e20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "402abbc2-43b9-4e37-b4f2-e2f9c2ee2bc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d1e4e72-d2c6-444d-a0d0-cb4250e92a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "402abbc2-43b9-4e37-b4f2-e2f9c2ee2bc8",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "6550162c-b151-44cf-871e-6e16f26ed295",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "5e21448d-6333-4605-8036-5f60fd7bac79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6550162c-b151-44cf-871e-6e16f26ed295",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "050119a1-7b8a-459c-9074-c4cf8a7a2785",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6550162c-b151-44cf-871e-6e16f26ed295",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "3f182eee-7a91-40da-ab27-e23ed9aa5220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "1acff452-210d-4181-8b81-c01416ad1caf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f182eee-7a91-40da-ab27-e23ed9aa5220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cc48fcc-35b2-41d3-ac44-4c116d7a4d26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f182eee-7a91-40da-ab27-e23ed9aa5220",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "a4de9f62-96a0-418a-8a29-e5f979aa921c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "72cae931-a8fb-47eb-85ec-175000ec4a9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4de9f62-96a0-418a-8a29-e5f979aa921c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "089df39b-d234-41a8-adda-e1fe4dbcf393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4de9f62-96a0-418a-8a29-e5f979aa921c",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "d6b2db4c-72d7-422c-9cb9-c829be388a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "b60d7146-afa0-4165-a6d0-e2aa4527a703",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b2db4c-72d7-422c-9cb9-c829be388a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab4fa874-ceed-41d6-a5e5-36f81cf5b133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b2db4c-72d7-422c-9cb9-c829be388a06",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "8c1f9386-a399-4452-bdab-fdce315daa14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "71f0b7b6-281c-4283-a9a8-9ccd1c7caaa2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c1f9386-a399-4452-bdab-fdce315daa14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73d7094-8985-45b7-8237-0bc6660df5be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c1f9386-a399-4452-bdab-fdce315daa14",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "f5ea695d-7b23-4a2c-9c73-f1d6565f0a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "132bfd7c-d537-445b-ab42-f2e07d158884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5ea695d-7b23-4a2c-9c73-f1d6565f0a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "125dfead-d7f6-4516-9b06-48abb800d49e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5ea695d-7b23-4a2c-9c73-f1d6565f0a2e",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        },
        {
            "id": "86e7e8e3-75ed-4322-87cc-c293d790f11a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "compositeImage": {
                "id": "24b92c20-575f-49dc-af47-f164ae9e9d24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86e7e8e3-75ed-4322-87cc-c293d790f11a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b61c75-362f-46be-abd8-ac5b27c535f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86e7e8e3-75ed-4322-87cc-c293d790f11a",
                    "LayerId": "58b4be19-1144-46b8-8b99-c020e9193588"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "58b4be19-1144-46b8-8b99-c020e9193588",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}