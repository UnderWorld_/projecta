{
    "id": "9a34560e-a9f1-461f-8662-8e2572c6f42f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left19",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7582c19-6b21-4c90-81be-84b86f991abc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a34560e-a9f1-461f-8662-8e2572c6f42f",
            "compositeImage": {
                "id": "1e2f3ac6-006a-46c4-b65f-86cdac70a4f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7582c19-6b21-4c90-81be-84b86f991abc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c692037f-046a-4597-bcd1-748fc68906f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7582c19-6b21-4c90-81be-84b86f991abc",
                    "LayerId": "c34001a1-f334-4158-97d8-1a142017cacd"
                }
            ]
        },
        {
            "id": "2caa2a6a-7149-48fc-a0ae-b652366cf1ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a34560e-a9f1-461f-8662-8e2572c6f42f",
            "compositeImage": {
                "id": "e23f6122-39db-4e81-a83d-b6bb916b9d13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2caa2a6a-7149-48fc-a0ae-b652366cf1ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58854f70-f831-41d9-8031-786edbbb1336",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2caa2a6a-7149-48fc-a0ae-b652366cf1ae",
                    "LayerId": "c34001a1-f334-4158-97d8-1a142017cacd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "c34001a1-f334-4158-97d8-1a142017cacd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a34560e-a9f1-461f-8662-8e2572c6f42f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 0
}