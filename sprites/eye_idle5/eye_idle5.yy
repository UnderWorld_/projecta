{
    "id": "a63e12c9-5666-4dd7-bb47-abe58f987d48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "77e24790-5870-43bd-9ffd-518f61071c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a63e12c9-5666-4dd7-bb47-abe58f987d48",
            "compositeImage": {
                "id": "eb236aa6-13b9-4be2-8d8f-e2bc304b9866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77e24790-5870-43bd-9ffd-518f61071c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fc94c0b0-700b-41a6-bc8e-94a0caff461b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77e24790-5870-43bd-9ffd-518f61071c8a",
                    "LayerId": "8f889dc1-1b3d-44c0-aa07-ecd3ad2e9980"
                }
            ]
        },
        {
            "id": "ace6aeb5-291c-4bd1-8793-2da0b26dce8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a63e12c9-5666-4dd7-bb47-abe58f987d48",
            "compositeImage": {
                "id": "93579a70-1fa8-439d-81b2-9feefeed7433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ace6aeb5-291c-4bd1-8793-2da0b26dce8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca18f9b-7c07-42f3-8b1e-3e130036db82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ace6aeb5-291c-4bd1-8793-2da0b26dce8f",
                    "LayerId": "8f889dc1-1b3d-44c0-aa07-ecd3ad2e9980"
                }
            ]
        },
        {
            "id": "c9e05a45-9c2d-4810-8d1f-e26506b1a91d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a63e12c9-5666-4dd7-bb47-abe58f987d48",
            "compositeImage": {
                "id": "5166bb5d-5530-4ae7-b936-a7b5b7df5e43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9e05a45-9c2d-4810-8d1f-e26506b1a91d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99d65423-a1f2-4565-aafb-7edd87092f5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9e05a45-9c2d-4810-8d1f-e26506b1a91d",
                    "LayerId": "8f889dc1-1b3d-44c0-aa07-ecd3ad2e9980"
                }
            ]
        },
        {
            "id": "12dd7d04-680a-4ccf-8e1b-7c41845f905e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a63e12c9-5666-4dd7-bb47-abe58f987d48",
            "compositeImage": {
                "id": "5065ccbf-56a9-4ee3-99a0-0e592e070ec3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12dd7d04-680a-4ccf-8e1b-7c41845f905e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "930c3405-c917-4db3-b2e6-696a9ceed38e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12dd7d04-680a-4ccf-8e1b-7c41845f905e",
                    "LayerId": "8f889dc1-1b3d-44c0-aa07-ecd3ad2e9980"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "8f889dc1-1b3d-44c0-aa07-ecd3ad2e9980",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a63e12c9-5666-4dd7-bb47-abe58f987d48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}