{
    "id": "4a2c0e50-17f0-4180-a3d5-9c69b2dd6574",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rhino_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 24,
    "bbox_right": 231,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81f991cc-bfef-4e15-89a3-6ce9ba7e98a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a2c0e50-17f0-4180-a3d5-9c69b2dd6574",
            "compositeImage": {
                "id": "83038669-efe5-4686-a746-03cb58e77adb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f991cc-bfef-4e15-89a3-6ce9ba7e98a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9804f28d-0131-4bf7-875a-f6d0ff430de3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f991cc-bfef-4e15-89a3-6ce9ba7e98a7",
                    "LayerId": "1814d7bc-b9b7-4129-a323-20f4eb393774"
                }
            ]
        },
        {
            "id": "fdd9fcf1-3251-44cc-a9d1-a063a8bc2896",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a2c0e50-17f0-4180-a3d5-9c69b2dd6574",
            "compositeImage": {
                "id": "bb861433-b2bf-4645-8b27-d8d92db71d38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdd9fcf1-3251-44cc-a9d1-a063a8bc2896",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f63e5715-e5ec-4a33-ac81-f1f821f4d9a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdd9fcf1-3251-44cc-a9d1-a063a8bc2896",
                    "LayerId": "1814d7bc-b9b7-4129-a323-20f4eb393774"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "1814d7bc-b9b7-4129-a323-20f4eb393774",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a2c0e50-17f0-4180-a3d5-9c69b2dd6574",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}