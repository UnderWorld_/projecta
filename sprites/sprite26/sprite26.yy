{
    "id": "5e68d913-3f10-42ea-ab90-be213fca35c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite26",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0c1f378-edaa-41bd-9ede-0ea61d5227f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e68d913-3f10-42ea-ab90-be213fca35c5",
            "compositeImage": {
                "id": "e1515aae-a152-42d3-97e8-d0c5f400aa43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0c1f378-edaa-41bd-9ede-0ea61d5227f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "634c8d3a-781f-4343-9bb4-7d09c440f7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0c1f378-edaa-41bd-9ede-0ea61d5227f5",
                    "LayerId": "1ee72cb1-cd56-48f2-94e7-56eec38dd3ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1ee72cb1-cd56-48f2-94e7-56eec38dd3ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e68d913-3f10-42ea-ab90-be213fca35c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}