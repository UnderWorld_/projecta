{
    "id": "7bb2dd4b-aa92-440c-9fa0-99f87ab0e124",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mouse_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ad67f0f-b6fe-4311-9135-061acd000d81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7bb2dd4b-aa92-440c-9fa0-99f87ab0e124",
            "compositeImage": {
                "id": "6c7ced7d-b6c2-4f8a-91f3-06490159a520",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ad67f0f-b6fe-4311-9135-061acd000d81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98377627-4650-486b-9e4f-c42a23aedb5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ad67f0f-b6fe-4311-9135-061acd000d81",
                    "LayerId": "c88639a1-4732-440f-87cf-dbcdea1131ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "c88639a1-4732-440f-87cf-dbcdea1131ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7bb2dd4b-aa92-440c-9fa0-99f87ab0e124",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 3,
    "yorig": 4
}