{
    "id": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "v_tree3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad19705c-ce05-4089-96d4-f522f462fcf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "3e25b2e4-5d95-4947-a1de-da7566473f3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad19705c-ce05-4089-96d4-f522f462fcf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72067f19-b935-4766-ae43-077bcc4709e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad19705c-ce05-4089-96d4-f522f462fcf3",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "acb6900e-b396-48ee-9d71-0cfa202688f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "7ae6a2d8-b02b-4bd9-aca6-838138767276",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acb6900e-b396-48ee-9d71-0cfa202688f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "242bbfc7-7af0-4d9c-855d-088f8ed54fc2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acb6900e-b396-48ee-9d71-0cfa202688f5",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "ba0d91c1-a419-4ff6-883d-e2a31f19899c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "dad1b946-b045-4e13-837f-8d1698544d92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0d91c1-a419-4ff6-883d-e2a31f19899c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f6b9058-1f52-429f-a7ba-fa45a91421a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0d91c1-a419-4ff6-883d-e2a31f19899c",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "60c51708-03ff-4aee-9dd1-a6eb000bd3a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "c84eaa7a-8414-42db-9afe-34ea0aaaad69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60c51708-03ff-4aee-9dd1-a6eb000bd3a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f129fe6e-0c45-4a02-b067-a1900b516606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60c51708-03ff-4aee-9dd1-a6eb000bd3a9",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "6e2480c6-26b7-4185-8edb-6cf0f9ddde80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "3a5b535e-ebb6-4c62-b34d-e4eb58e4f2d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e2480c6-26b7-4185-8edb-6cf0f9ddde80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d44e7588-70a2-46c6-b1dc-f1bdd27902f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e2480c6-26b7-4185-8edb-6cf0f9ddde80",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "dfaa3d90-330b-4296-90cd-1510bd515387",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "96312dc0-f902-456d-b725-52affef4353c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfaa3d90-330b-4296-90cd-1510bd515387",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80c811a2-a884-4eb4-b7cf-9654cd708b67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfaa3d90-330b-4296-90cd-1510bd515387",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "3a2a4192-5082-4c77-85a0-5da098fb7c1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "43078657-4ddd-41aa-8d35-b55d59fc8f14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2a4192-5082-4c77-85a0-5da098fb7c1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fee6038f-da77-4308-9a06-d2c71ad72ddb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2a4192-5082-4c77-85a0-5da098fb7c1a",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "97e85fbc-7797-4e7b-8e9d-25977e8881d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "464dff42-e6c5-41ba-9805-0c4b50a2236e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e85fbc-7797-4e7b-8e9d-25977e8881d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6230d1b2-d50d-460b-a411-1db13b7c27b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e85fbc-7797-4e7b-8e9d-25977e8881d4",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "50bc2ceb-6d46-487a-a2a4-c177fbef3291",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "10ecd99a-fbe8-47f9-baba-4c708970d15c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50bc2ceb-6d46-487a-a2a4-c177fbef3291",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d979a2e5-3689-4518-81a5-abca987f7aba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50bc2ceb-6d46-487a-a2a4-c177fbef3291",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "cee76c99-fc53-4040-a1d2-d7c7b8779e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "37724d38-88db-4f1f-9823-04b857c7d5e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee76c99-fc53-4040-a1d2-d7c7b8779e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a40451dc-3184-4ae8-944f-64cd128a4c15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee76c99-fc53-4040-a1d2-d7c7b8779e2b",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "6363af0c-2bfd-4eab-ad11-bf07e752d5f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "c4e2eedc-caa7-477c-8ce3-a4df3b016977",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6363af0c-2bfd-4eab-ad11-bf07e752d5f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3e13460-4a7a-42cd-bef4-1bc56940fa7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6363af0c-2bfd-4eab-ad11-bf07e752d5f0",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "a2c01c98-e9ba-4407-8f28-e03fc332a30d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "44ceae73-e71f-4cae-b482-57f501f7391e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c01c98-e9ba-4407-8f28-e03fc332a30d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89919a08-6067-4be6-b339-9818029e9287",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c01c98-e9ba-4407-8f28-e03fc332a30d",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "307de3fe-fe9f-475a-931b-d1429a302e67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "aaf1ee65-8b8f-4b70-98ca-49d2782e7515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "307de3fe-fe9f-475a-931b-d1429a302e67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bad4b5b-032f-48b9-8119-78c127045c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "307de3fe-fe9f-475a-931b-d1429a302e67",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "2404a819-e8b3-4223-85bf-1c00db23dbdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "dc27e30f-6e38-4ce0-965c-47b70aac9a6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2404a819-e8b3-4223-85bf-1c00db23dbdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa9441dc-7183-4519-b437-e802393495a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2404a819-e8b3-4223-85bf-1c00db23dbdd",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "fa57dc4a-bba5-494a-8521-ab74e81c6ff8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "b03ddb61-aec8-4d91-840d-9761dde4d8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa57dc4a-bba5-494a-8521-ab74e81c6ff8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0132c145-3e35-4c13-a73a-86ebc9af0c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa57dc4a-bba5-494a-8521-ab74e81c6ff8",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "63375015-a641-42c0-9049-ec4aafec390e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "54f9392c-884c-4ec1-a683-f97f17e2e0f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63375015-a641-42c0-9049-ec4aafec390e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a29172e2-bb5a-43e5-8797-6f1b74a4ffd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63375015-a641-42c0-9049-ec4aafec390e",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "4a10bbe9-1976-4c86-931d-cb367b3b6540",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "2e8d9e18-0392-4af2-a9c7-c1474e388d59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a10bbe9-1976-4c86-931d-cb367b3b6540",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "441ade71-d3f7-4ed2-b3f9-6b5522845790",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a10bbe9-1976-4c86-931d-cb367b3b6540",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "f6c81546-28a8-4a9c-bf11-3f3efa49f7f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "47b8024f-6fc0-4ade-913a-be00916ae795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6c81546-28a8-4a9c-bf11-3f3efa49f7f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a68c7c9-3a26-40e7-97a0-aac7a7de6568",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6c81546-28a8-4a9c-bf11-3f3efa49f7f7",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        },
        {
            "id": "8927cd4e-aba1-4b51-a22e-e0e44bbf4ba0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "compositeImage": {
                "id": "1efd0309-2307-4268-9a11-e14237d3222b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8927cd4e-aba1-4b51-a22e-e0e44bbf4ba0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d27ddb0-8527-4305-a881-f75ebcf84b76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8927cd4e-aba1-4b51-a22e-e0e44bbf4ba0",
                    "LayerId": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "f1c5df13-eb7c-4638-b0da-45fb36e1b8c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 4
}