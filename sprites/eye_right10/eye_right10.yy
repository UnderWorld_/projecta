{
    "id": "a5a19666-1f83-4612-af5e-9e537eddfe1a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21eb2463-5695-474a-8d51-145e6ac337f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a19666-1f83-4612-af5e-9e537eddfe1a",
            "compositeImage": {
                "id": "03cf3f1a-981c-491b-b0b1-ac320b34570c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21eb2463-5695-474a-8d51-145e6ac337f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e196fda6-66f1-4fd1-9d20-0a331f3a1bc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21eb2463-5695-474a-8d51-145e6ac337f9",
                    "LayerId": "55e10a68-7b65-45b8-b75b-8fd1ef07961a"
                }
            ]
        },
        {
            "id": "3af1612c-97d9-479e-a13b-55998e306756",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5a19666-1f83-4612-af5e-9e537eddfe1a",
            "compositeImage": {
                "id": "ea9dcc99-6a5f-451b-8a54-42013e37f760",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af1612c-97d9-479e-a13b-55998e306756",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e07dd183-1446-4aeb-827d-ebf4ab10b9a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af1612c-97d9-479e-a13b-55998e306756",
                    "LayerId": "55e10a68-7b65-45b8-b75b-8fd1ef07961a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "55e10a68-7b65-45b8-b75b-8fd1ef07961a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5a19666-1f83-4612-af5e-9e537eddfe1a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}