{
    "id": "cca83fc9-68b7-470e-b371-4600fccf3a53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 895,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "060ea496-99e4-424a-83c4-82a4a693fab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cca83fc9-68b7-470e-b371-4600fccf3a53",
            "compositeImage": {
                "id": "6eba1c7a-f769-4669-81a2-d5ef3e8b9690",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "060ea496-99e4-424a-83c4-82a4a693fab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e401903e-5bbb-40f1-8aa6-3a340892e357",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "060ea496-99e4-424a-83c4-82a4a693fab0",
                    "LayerId": "95b2682e-9ab7-451a-aaae-f1cec61b7b02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "95b2682e-9ab7-451a-aaae-f1cec61b7b02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cca83fc9-68b7-470e-b371-4600fccf3a53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}