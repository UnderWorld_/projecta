{
    "id": "e596014a-0fcf-4cfe-b2f0-f7e165b372fb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair3_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 94,
    "bbox_left": 0,
    "bbox_right": 79,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ad6b458f-302b-4b4b-8859-3ad74ec69113",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e596014a-0fcf-4cfe-b2f0-f7e165b372fb",
            "compositeImage": {
                "id": "559509e1-faf6-44b4-a756-d2fe4d1fe79e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad6b458f-302b-4b4b-8859-3ad74ec69113",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f89557fa-8b2a-4c04-aad3-961286ba6f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad6b458f-302b-4b4b-8859-3ad74ec69113",
                    "LayerId": "a25172ec-d279-4dc5-9a70-52b717993983"
                }
            ]
        },
        {
            "id": "0365311d-b8b7-49a2-ba75-66acb0864642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e596014a-0fcf-4cfe-b2f0-f7e165b372fb",
            "compositeImage": {
                "id": "313a0704-4488-4703-af4e-a043171e6a57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0365311d-b8b7-49a2-ba75-66acb0864642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "862c9e69-3c98-4acb-ad96-b4a3dea00a21",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0365311d-b8b7-49a2-ba75-66acb0864642",
                    "LayerId": "a25172ec-d279-4dc5-9a70-52b717993983"
                }
            ]
        },
        {
            "id": "fde6d535-761c-4af9-a8e7-e9628ce77c9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e596014a-0fcf-4cfe-b2f0-f7e165b372fb",
            "compositeImage": {
                "id": "512dddb4-371d-4827-9ded-5315eb2cd79f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde6d535-761c-4af9-a8e7-e9628ce77c9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0538c29d-4a2f-41af-b379-13cfcdd59eac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde6d535-761c-4af9-a8e7-e9628ce77c9b",
                    "LayerId": "a25172ec-d279-4dc5-9a70-52b717993983"
                }
            ]
        },
        {
            "id": "49934ce1-651d-4901-9427-08e0c8a13ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e596014a-0fcf-4cfe-b2f0-f7e165b372fb",
            "compositeImage": {
                "id": "e39851e0-dc6c-4d24-a1aa-8c36994f5580",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49934ce1-651d-4901-9427-08e0c8a13ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a64502d1-f631-48f5-b6a8-64f148a39f9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49934ce1-651d-4901-9427-08e0c8a13ae7",
                    "LayerId": "a25172ec-d279-4dc5-9a70-52b717993983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "a25172ec-d279-4dc5-9a70-52b717993983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e596014a-0fcf-4cfe-b2f0-f7e165b372fb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}