{
    "id": "92d1e077-d68b-4da7-8d00-5d188d9f07be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardright1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fba772f5-2222-465a-b57c-2c05223e1b72",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1e077-d68b-4da7-8d00-5d188d9f07be",
            "compositeImage": {
                "id": "66d0758a-f6bd-43e4-8f15-2499b2ae07e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba772f5-2222-465a-b57c-2c05223e1b72",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9452b1af-00c7-441b-90b8-8bef2e975193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba772f5-2222-465a-b57c-2c05223e1b72",
                    "LayerId": "6db317f8-dfaa-44b3-ae33-b62fc3d02a48"
                }
            ]
        },
        {
            "id": "691a7934-f7d7-43fe-9927-f6ccf0eaeb21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92d1e077-d68b-4da7-8d00-5d188d9f07be",
            "compositeImage": {
                "id": "0f6a3572-5ac2-443f-b8ff-6c6c0f00d738",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691a7934-f7d7-43fe-9927-f6ccf0eaeb21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3db3778-71c4-4dc7-86d5-612fe9f15e01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691a7934-f7d7-43fe-9927-f6ccf0eaeb21",
                    "LayerId": "6db317f8-dfaa-44b3-ae33-b62fc3d02a48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "6db317f8-dfaa-44b3-ae33-b62fc3d02a48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92d1e077-d68b-4da7-8d00-5d188d9f07be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}