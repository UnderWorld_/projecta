{
    "id": "0f707998-859f-49bd-a8bf-ce969723ecfd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "fox_walk_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8cee13b-6e07-421d-97aa-d986642f43ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f707998-859f-49bd-a8bf-ce969723ecfd",
            "compositeImage": {
                "id": "0a4e3559-9379-4722-9804-97b67f28e3b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8cee13b-6e07-421d-97aa-d986642f43ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8710bd24-7cef-42e9-a81c-8aa1db890c8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8cee13b-6e07-421d-97aa-d986642f43ef",
                    "LayerId": "7f2b74b5-853d-44e7-a557-e206164530f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7f2b74b5-853d-44e7-a557-e206164530f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f707998-859f-49bd-a8bf-ce969723ecfd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}