{
    "id": "197f5c0a-5dbc-4777-bf9a-a235789e402c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "16df3681-9481-4905-9eeb-4ca438faacaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "197f5c0a-5dbc-4777-bf9a-a235789e402c",
            "compositeImage": {
                "id": "c9dc9e6e-e7a2-470f-b7a7-634c6559f467",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "16df3681-9481-4905-9eeb-4ca438faacaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64a862d4-9374-466a-8493-a2d80917a1c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "16df3681-9481-4905-9eeb-4ca438faacaf",
                    "LayerId": "363e580f-1db3-4ecb-8508-6abf39f21097"
                }
            ]
        },
        {
            "id": "f773eb7c-7045-498d-9eac-ba054a56db56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "197f5c0a-5dbc-4777-bf9a-a235789e402c",
            "compositeImage": {
                "id": "2a243309-5062-47e8-9f5a-2e18d8f065fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f773eb7c-7045-498d-9eac-ba054a56db56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b928fc17-ff7a-497a-b7eb-e6f40e0631c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f773eb7c-7045-498d-9eac-ba054a56db56",
                    "LayerId": "363e580f-1db3-4ecb-8508-6abf39f21097"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "363e580f-1db3-4ecb-8508-6abf39f21097",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "197f5c0a-5dbc-4777-bf9a-a235789e402c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}