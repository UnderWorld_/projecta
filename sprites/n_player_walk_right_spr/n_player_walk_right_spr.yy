{
    "id": "5af86791-d767-4f88-bd2b-c7bf5e4235ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "n_player_walk_right_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 123,
    "bbox_left": 82,
    "bbox_right": 114,
    "bbox_top": 42,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "348e451c-26fd-4235-838e-10b8974ede45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af86791-d767-4f88-bd2b-c7bf5e4235ea",
            "compositeImage": {
                "id": "ea5b50c8-82d5-4f71-96ea-d0dffe2f7f52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "348e451c-26fd-4235-838e-10b8974ede45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "986fc212-829a-4639-b6e8-9429650a1131",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "348e451c-26fd-4235-838e-10b8974ede45",
                    "LayerId": "e9de6f73-8bbb-4b26-8fb2-c81787b42fd2"
                }
            ]
        },
        {
            "id": "b1bca61f-d18b-4991-8cbd-02c3305fed14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af86791-d767-4f88-bd2b-c7bf5e4235ea",
            "compositeImage": {
                "id": "a8ceaa06-011e-4142-b6d0-8c422efdbfd2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1bca61f-d18b-4991-8cbd-02c3305fed14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "becfb04a-56d5-4e09-bc6c-4dbdbe14cd80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1bca61f-d18b-4991-8cbd-02c3305fed14",
                    "LayerId": "e9de6f73-8bbb-4b26-8fb2-c81787b42fd2"
                }
            ]
        },
        {
            "id": "eb335be0-367a-4bb6-b1eb-5d73d81c581a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af86791-d767-4f88-bd2b-c7bf5e4235ea",
            "compositeImage": {
                "id": "e23bfe30-7a47-4898-890a-5e10e84a2f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb335be0-367a-4bb6-b1eb-5d73d81c581a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "243e182a-4c62-432b-a461-64b6a624844c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb335be0-367a-4bb6-b1eb-5d73d81c581a",
                    "LayerId": "e9de6f73-8bbb-4b26-8fb2-c81787b42fd2"
                }
            ]
        },
        {
            "id": "bca33344-4c0e-429a-b3a2-008694d07cd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5af86791-d767-4f88-bd2b-c7bf5e4235ea",
            "compositeImage": {
                "id": "7162c0d2-9410-48af-ac0d-c5cca4401aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca33344-4c0e-429a-b3a2-008694d07cd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80bf75c4-4429-4e56-8d8e-d27be6f96a13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca33344-4c0e-429a-b3a2-008694d07cd3",
                    "LayerId": "e9de6f73-8bbb-4b26-8fb2-c81787b42fd2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 170,
    "layers": [
        {
            "id": "e9de6f73-8bbb-4b26-8fb2-c81787b42fd2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5af86791-d767-4f88-bd2b-c7bf5e4235ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 84,
    "yorig": 85
}