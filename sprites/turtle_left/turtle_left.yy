{
    "id": "82298c14-c9dd-461d-9301-038d46ef5549",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afa91eac-c6bb-480d-80a8-d9daea19a082",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82298c14-c9dd-461d-9301-038d46ef5549",
            "compositeImage": {
                "id": "205791bc-e70b-4f40-8d4b-7953d1203af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afa91eac-c6bb-480d-80a8-d9daea19a082",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "43a4f617-33f4-417e-ad72-0c46a1f5a370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afa91eac-c6bb-480d-80a8-d9daea19a082",
                    "LayerId": "205d901c-1224-410c-8eca-a59c980af1d0"
                }
            ]
        },
        {
            "id": "835ea008-a4f4-493d-aa14-1d9b4e685037",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82298c14-c9dd-461d-9301-038d46ef5549",
            "compositeImage": {
                "id": "4305d346-dca2-4490-ac6a-415d9f6fbd47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "835ea008-a4f4-493d-aa14-1d9b4e685037",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60fcd310-cdce-4fb0-bb54-5313dce93a20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "835ea008-a4f4-493d-aa14-1d9b4e685037",
                    "LayerId": "205d901c-1224-410c-8eca-a59c980af1d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "205d901c-1224-410c-8eca-a59c980af1d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82298c14-c9dd-461d-9301-038d46ef5549",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}