{
    "id": "ede6847f-b793-4a04-9a21-20f3876ef39f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "text_box_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e34d40d5-50fb-4851-9e45-e6994103d7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ede6847f-b793-4a04-9a21-20f3876ef39f",
            "compositeImage": {
                "id": "76833c93-1659-4d99-b4f7-cd6b0eae2ad5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e34d40d5-50fb-4851-9e45-e6994103d7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b0bd87e-34c0-4577-b684-792cad305a5a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e34d40d5-50fb-4851-9e45-e6994103d7c1",
                    "LayerId": "54874d00-e8b4-4f55-b788-8f50fe1c95a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "54874d00-e8b4-4f55-b788-8f50fe1c95a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ede6847f-b793-4a04-9a21-20f3876ef39f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": -50
}