{
    "id": "574dc409-92d2-405b-a655-bbdc840c7ffa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7a16bff-ede6-4f29-90f9-61b36c9d7ec0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "574dc409-92d2-405b-a655-bbdc840c7ffa",
            "compositeImage": {
                "id": "b3b198d5-9902-417e-b6f8-0f2f157a8d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7a16bff-ede6-4f29-90f9-61b36c9d7ec0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3599997-14db-4b0f-bfac-cead19165077",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7a16bff-ede6-4f29-90f9-61b36c9d7ec0",
                    "LayerId": "b8b2a89e-9efb-443a-b318-2ff6d9fc9c1c"
                }
            ]
        },
        {
            "id": "5568efbc-f345-4508-8962-91dc086bce6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "574dc409-92d2-405b-a655-bbdc840c7ffa",
            "compositeImage": {
                "id": "50b1f256-236f-4306-91cc-13852d0763e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5568efbc-f345-4508-8962-91dc086bce6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4ddb92d-30ef-4d38-82d8-df49adc0bc48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5568efbc-f345-4508-8962-91dc086bce6b",
                    "LayerId": "b8b2a89e-9efb-443a-b318-2ff6d9fc9c1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "b8b2a89e-9efb-443a-b318-2ff6d9fc9c1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "574dc409-92d2-405b-a655-bbdc840c7ffa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}