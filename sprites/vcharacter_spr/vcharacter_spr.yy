{
    "id": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "vcharacter_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26c87940-a398-4ceb-b1a8-943ddc4745b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "14ac51df-d61e-4d38-8f99-cf702b0a1f5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26c87940-a398-4ceb-b1a8-943ddc4745b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd42b6b3-a3cb-4e53-a001-e00e8aaa5948",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26c87940-a398-4ceb-b1a8-943ddc4745b5",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "e533fb88-7028-4bca-8748-05a3ce270d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "3341544f-206c-4653-b566-bdbe6f5bedc1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e533fb88-7028-4bca-8748-05a3ce270d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6af203ed-86b3-4f16-9957-6e18e05fbdcf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e533fb88-7028-4bca-8748-05a3ce270d4f",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "918967f4-b724-46f9-acb8-5b6b125a4515",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "75335ebb-8f06-4946-94cf-b6be9485bbb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "918967f4-b724-46f9-acb8-5b6b125a4515",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52ffa1d7-6900-4847-ac56-4735134af91a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "918967f4-b724-46f9-acb8-5b6b125a4515",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "3a2e40e5-2db1-42dd-9bdc-4c320ff8fb75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "98e25787-74ba-4ee1-a51b-9321724b9818",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2e40e5-2db1-42dd-9bdc-4c320ff8fb75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "445496e7-8877-4fac-a823-aa5a71248e36",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2e40e5-2db1-42dd-9bdc-4c320ff8fb75",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "d9388d05-5a88-4d5c-9649-1a249c934d55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "ac3e1efd-ed05-4520-8991-9d9cadead7f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9388d05-5a88-4d5c-9649-1a249c934d55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07e446b9-724f-49e5-850f-6701a1b747d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9388d05-5a88-4d5c-9649-1a249c934d55",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "097991a2-fe68-4838-8b5b-35a6ed364485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "753bca10-abd2-4ccb-8bc4-c79728766d77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "097991a2-fe68-4838-8b5b-35a6ed364485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e5dba14-f4fa-4dd7-bcf6-931a69718f28",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "097991a2-fe68-4838-8b5b-35a6ed364485",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "cf670c63-2eb6-4581-acad-40e435ed1842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "b9d2da86-0c36-452b-ac3f-e403dbfc8b91",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf670c63-2eb6-4581-acad-40e435ed1842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1613c09b-41c7-4704-9e52-4a788e89dfb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf670c63-2eb6-4581-acad-40e435ed1842",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "4dd6f6a1-051a-4393-a640-b8374fa2a5c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "0785fc46-9b3f-4a92-985b-4e49f8fce3bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4dd6f6a1-051a-4393-a640-b8374fa2a5c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec3068c-79ff-433e-a8e9-aaba4299a248",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4dd6f6a1-051a-4393-a640-b8374fa2a5c9",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "42bf95df-8d5e-4d17-a415-357ddfee2615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "8c9451f5-4277-4c8b-abc7-5474b7e72b16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42bf95df-8d5e-4d17-a415-357ddfee2615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66185e01-1570-4779-b3b2-b672bb7d5f53",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42bf95df-8d5e-4d17-a415-357ddfee2615",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "bcdd42c0-fab6-4079-8303-746dae950b5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "03444146-294c-432d-a8d8-ac14dc92b452",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcdd42c0-fab6-4079-8303-746dae950b5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65406beb-f798-46c8-b5a7-258f93fce912",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcdd42c0-fab6-4079-8303-746dae950b5f",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "f7cc3c00-691c-45a4-9407-f812297a3b59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "a7b2d321-14e4-4454-8943-e5d282a9eaa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7cc3c00-691c-45a4-9407-f812297a3b59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7c9018a-836d-4074-87d8-a7a2896736b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7cc3c00-691c-45a4-9407-f812297a3b59",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "ea1bfef3-6154-469f-9dba-94db1e498fef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "1770d1e8-cb35-42bf-8d2c-579e760b5b45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea1bfef3-6154-469f-9dba-94db1e498fef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "077858ee-5824-4c43-913d-185f55f025fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea1bfef3-6154-469f-9dba-94db1e498fef",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "769cf1c9-c86b-4587-bc37-59665f48326c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "23790301-4d3a-462f-be5b-bf3030467d52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "769cf1c9-c86b-4587-bc37-59665f48326c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff2949b-74f2-435e-b084-7005353a79ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "769cf1c9-c86b-4587-bc37-59665f48326c",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "fb26be54-14e1-40b0-9ba4-cb342bd2a54e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "24724f2e-ae7f-4b8a-85b2-b2f43fda04e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb26be54-14e1-40b0-9ba4-cb342bd2a54e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed4dddd9-0c01-4ce2-b6a3-4e4301c35f99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb26be54-14e1-40b0-9ba4-cb342bd2a54e",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "8a380ec8-987b-4b33-bd03-e3bf203862b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "0ebc8139-487d-4d54-a1e4-0ee56383c476",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a380ec8-987b-4b33-bd03-e3bf203862b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41ecd4b9-9860-407e-8f14-07232743e156",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a380ec8-987b-4b33-bd03-e3bf203862b4",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "4c8173b3-56b0-4328-b7c6-2c2874e92a9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "3786f0b9-6dac-4671-8efb-5dacbc437307",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c8173b3-56b0-4328-b7c6-2c2874e92a9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9f7ce21-d3c5-4e9c-897d-033d48f5d41f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c8173b3-56b0-4328-b7c6-2c2874e92a9a",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "578ac875-e503-462a-bdce-ffb623581922",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "23f9880f-6875-4f58-af68-3c49383afb40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "578ac875-e503-462a-bdce-ffb623581922",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a63ff84-3873-4562-bd43-9cb693663e96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "578ac875-e503-462a-bdce-ffb623581922",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "586461b3-4edc-4a17-99c0-2a9df62803e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "a34326a3-cf2a-4990-821f-a8e929f702d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "586461b3-4edc-4a17-99c0-2a9df62803e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "946f373a-1d5d-4009-9425-62a83eb06402",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "586461b3-4edc-4a17-99c0-2a9df62803e9",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "7bbd9f0f-55bc-4dc1-8694-5d9fb0c928a8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "5224a8e7-4379-4dc2-81be-d40389840ae4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bbd9f0f-55bc-4dc1-8694-5d9fb0c928a8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b20295c8-245d-4e84-9e1f-316d66a1cacd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bbd9f0f-55bc-4dc1-8694-5d9fb0c928a8",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        },
        {
            "id": "e255d7fb-2656-44d4-8a0c-c50a61c9e33f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "compositeImage": {
                "id": "2c89c356-e5bc-4490-9b3c-1895c83eaac5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e255d7fb-2656-44d4-8a0c-c50a61c9e33f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe05bf8a-5e25-47fe-9e41-f6cf255a1507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e255d7fb-2656-44d4-8a0c-c50a61c9e33f",
                    "LayerId": "1e7728cf-b00e-4f33-a299-8b8c380d09bd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 21,
    "layers": [
        {
            "id": "1e7728cf-b00e-4f33-a299-8b8c380d09bd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8e0b3b0-6f78-4204-94f1-7460b27204bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}