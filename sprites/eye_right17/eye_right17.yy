{
    "id": "4b8cec8e-f55a-4a6f-9199-af1a4c02cb75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3b9e05a-c76f-4d76-9a95-031bde3edd5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b8cec8e-f55a-4a6f-9199-af1a4c02cb75",
            "compositeImage": {
                "id": "d3531e93-f360-4afe-b5e4-4b504106ab7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b9e05a-c76f-4d76-9a95-031bde3edd5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbccb6d0-17ab-401d-a6ce-49856cbb3dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b9e05a-c76f-4d76-9a95-031bde3edd5c",
                    "LayerId": "4b461bee-17ff-47d0-bd04-03cc7af9993e"
                }
            ]
        },
        {
            "id": "f2af6ee4-ef57-42e0-a963-b87d62600970",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b8cec8e-f55a-4a6f-9199-af1a4c02cb75",
            "compositeImage": {
                "id": "d26d1324-332c-4812-96dd-7753c90dfda7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2af6ee4-ef57-42e0-a963-b87d62600970",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6b9e3ca-f7d9-4fd2-b1d7-c34d03ee550b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2af6ee4-ef57-42e0-a963-b87d62600970",
                    "LayerId": "4b461bee-17ff-47d0-bd04-03cc7af9993e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4b461bee-17ff-47d0-bd04-03cc7af9993e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b8cec8e-f55a-4a6f-9199-af1a4c02cb75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}