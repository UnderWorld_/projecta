{
    "id": "85f51d93-c63a-4511-992b-5924f99e9f06",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird2_fly_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76a8935f-efad-4277-85bb-7a286914a3c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85f51d93-c63a-4511-992b-5924f99e9f06",
            "compositeImage": {
                "id": "38268580-d3b7-474c-b947-fa782d9b5468",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a8935f-efad-4277-85bb-7a286914a3c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87cb88d4-6f9e-48bc-b36f-15f2ea8db2fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a8935f-efad-4277-85bb-7a286914a3c1",
                    "LayerId": "0fa9bc5c-e030-4128-8844-877082c7d5cf"
                }
            ]
        },
        {
            "id": "d6176313-648e-43d0-9cb0-273f09d5bbf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "85f51d93-c63a-4511-992b-5924f99e9f06",
            "compositeImage": {
                "id": "33aed697-b98f-4a42-b00d-299346aedf78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6176313-648e-43d0-9cb0-273f09d5bbf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9236a023-3d48-4ee8-9f09-bdd03c45f92a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6176313-648e-43d0-9cb0-273f09d5bbf1",
                    "LayerId": "0fa9bc5c-e030-4128-8844-877082c7d5cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0fa9bc5c-e030-4128-8844-877082c7d5cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "85f51d93-c63a-4511-992b-5924f99e9f06",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 27
}