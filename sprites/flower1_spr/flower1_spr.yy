{
    "id": "0801f49f-34b6-4c05-bf79-4f5af1480f07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flower1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 8,
    "bbox_right": 25,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81d9957b-4a44-4347-8595-4ab4a4945ac6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0801f49f-34b6-4c05-bf79-4f5af1480f07",
            "compositeImage": {
                "id": "8e926d0b-a682-4da0-8e73-e5e7b77a9c46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81d9957b-4a44-4347-8595-4ab4a4945ac6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "770be51a-cb97-405c-bed9-8c527af991d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81d9957b-4a44-4347-8595-4ab4a4945ac6",
                    "LayerId": "8297d517-8991-45ce-a9f9-d3e293569969"
                }
            ]
        },
        {
            "id": "edd08093-2ab2-493c-a77e-cadf2407d97d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0801f49f-34b6-4c05-bf79-4f5af1480f07",
            "compositeImage": {
                "id": "8bb93b8b-e62c-4ccb-aeab-1439bbd5124c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd08093-2ab2-493c-a77e-cadf2407d97d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fd7e5ec-724e-4b56-b3b7-af839ce092e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd08093-2ab2-493c-a77e-cadf2407d97d",
                    "LayerId": "8297d517-8991-45ce-a9f9-d3e293569969"
                }
            ]
        },
        {
            "id": "ba1288c4-45cf-49b2-8884-3766f7d8b0e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0801f49f-34b6-4c05-bf79-4f5af1480f07",
            "compositeImage": {
                "id": "677335cb-eecb-49d1-bb66-8b8e109298bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba1288c4-45cf-49b2-8884-3766f7d8b0e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d810fa0f-e14a-4b97-8aa8-b6846db02b7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba1288c4-45cf-49b2-8884-3766f7d8b0e6",
                    "LayerId": "8297d517-8991-45ce-a9f9-d3e293569969"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8297d517-8991-45ce-a9f9-d3e293569969",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0801f49f-34b6-4c05-bf79-4f5af1480f07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 0.01,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}