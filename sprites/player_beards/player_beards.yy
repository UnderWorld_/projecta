{
    "id": "ca432737-ed24-4bfb-a784-fc11dd188942",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "player_beards",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 68,
    "bbox_left": 0,
    "bbox_right": 54,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "597755a5-f244-4628-aca7-beba80a59c51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "compositeImage": {
                "id": "2e6459d3-2b58-44fd-8b33-bbeea7d5685d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "597755a5-f244-4628-aca7-beba80a59c51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba1911e1-39af-451b-8021-53abd47748b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "597755a5-f244-4628-aca7-beba80a59c51",
                    "LayerId": "4440431e-1af1-4386-84f6-bb0c1c651b8e"
                }
            ]
        },
        {
            "id": "46f36821-5573-4878-867a-c93a830d8eb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "compositeImage": {
                "id": "3699a863-c16e-40e4-aaf1-c9f38a656d69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46f36821-5573-4878-867a-c93a830d8eb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0070e37c-cfe4-4fca-943c-343519f2121c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46f36821-5573-4878-867a-c93a830d8eb3",
                    "LayerId": "4440431e-1af1-4386-84f6-bb0c1c651b8e"
                }
            ]
        },
        {
            "id": "9d94e092-25a5-465f-a98a-2c0de9a484ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "compositeImage": {
                "id": "16481e2c-9617-4daf-ae47-d90f25529ee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d94e092-25a5-465f-a98a-2c0de9a484ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07bf21f1-1b6b-4b72-8ad7-d24b23d32c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d94e092-25a5-465f-a98a-2c0de9a484ec",
                    "LayerId": "4440431e-1af1-4386-84f6-bb0c1c651b8e"
                }
            ]
        },
        {
            "id": "22882aee-e4cc-434f-a3e7-553600d1b77e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "compositeImage": {
                "id": "2c1a2f28-0bf4-4b1a-8e99-1b3a3bd48472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22882aee-e4cc-434f-a3e7-553600d1b77e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e89db4bf-6f8d-41f9-9f6a-9c7147398913",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22882aee-e4cc-434f-a3e7-553600d1b77e",
                    "LayerId": "4440431e-1af1-4386-84f6-bb0c1c651b8e"
                }
            ]
        },
        {
            "id": "8e387739-9569-4781-80da-344c43b12a09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "compositeImage": {
                "id": "0d4eef8e-5ba8-4345-8c35-19b1352cdcba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e387739-9569-4781-80da-344c43b12a09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24fc0bf-8326-469f-b52e-edd0e99134bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e387739-9569-4781-80da-344c43b12a09",
                    "LayerId": "4440431e-1af1-4386-84f6-bb0c1c651b8e"
                }
            ]
        },
        {
            "id": "94287016-5221-4426-9a18-74541cbd9d51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "compositeImage": {
                "id": "361eaf98-a781-4744-bcb8-828dd1d90f75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "94287016-5221-4426-9a18-74541cbd9d51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a493f1ad-f6e5-4de9-8c41-18398a9191c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "94287016-5221-4426-9a18-74541cbd9d51",
                    "LayerId": "4440431e-1af1-4386-84f6-bb0c1c651b8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4440431e-1af1-4386-84f6-bb0c1c651b8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}