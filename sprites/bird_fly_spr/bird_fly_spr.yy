{
    "id": "acf896f7-fcd2-4323-945b-2a3af83c197c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bird_fly_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a99c19e8-50cf-4f97-9d2f-f6400520ed25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acf896f7-fcd2-4323-945b-2a3af83c197c",
            "compositeImage": {
                "id": "1f2f242c-8b86-4c70-b79e-9c440147468c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a99c19e8-50cf-4f97-9d2f-f6400520ed25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23e5e0ff-a781-426a-a45b-bcedb5e95c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a99c19e8-50cf-4f97-9d2f-f6400520ed25",
                    "LayerId": "24c79a50-bd18-4de8-b609-cf7ec9bb2b28"
                }
            ]
        },
        {
            "id": "dda273b8-c8d8-4786-9ce1-f5412d4bd328",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "acf896f7-fcd2-4323-945b-2a3af83c197c",
            "compositeImage": {
                "id": "9b58412d-71a2-49c9-b578-34d7dd1eff38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dda273b8-c8d8-4786-9ce1-f5412d4bd328",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27a14d0d-c90b-4146-9eae-b5685c82933a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dda273b8-c8d8-4786-9ce1-f5412d4bd328",
                    "LayerId": "24c79a50-bd18-4de8-b609-cf7ec9bb2b28"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "24c79a50-bd18-4de8-b609-cf7ec9bb2b28",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "acf896f7-fcd2-4323-945b-2a3af83c197c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 21,
    "yorig": 28
}