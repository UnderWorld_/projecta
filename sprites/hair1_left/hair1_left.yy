{
    "id": "199e6b48-7a39-49ce-ac82-938540949c86",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair1_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 70,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e344cda-c403-4c5b-858f-04660115681e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "199e6b48-7a39-49ce-ac82-938540949c86",
            "compositeImage": {
                "id": "e065674b-73be-4161-8653-d6ab7404970c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e344cda-c403-4c5b-858f-04660115681e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2fa31a4-23c5-457f-ade9-b944f2cd5bf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e344cda-c403-4c5b-858f-04660115681e",
                    "LayerId": "530999bf-bdb3-4fa8-934c-7ee4dba4e1d3"
                }
            ]
        },
        {
            "id": "d8467617-b7c0-4989-bbec-67943f9641a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "199e6b48-7a39-49ce-ac82-938540949c86",
            "compositeImage": {
                "id": "c046e0e3-cf20-44bf-a606-d6545982cb1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8467617-b7c0-4989-bbec-67943f9641a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ebaa188a-b8d4-43ed-b511-456df71a4541",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8467617-b7c0-4989-bbec-67943f9641a6",
                    "LayerId": "530999bf-bdb3-4fa8-934c-7ee4dba4e1d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "530999bf-bdb3-4fa8-934c-7ee4dba4e1d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "199e6b48-7a39-49ce-ac82-938540949c86",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}