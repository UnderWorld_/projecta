{
    "id": "8982f23c-c302-4b06-9215-050aad36b9b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardright3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 10,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac8a838d-7f4f-4a74-917a-df0facc5572c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8982f23c-c302-4b06-9215-050aad36b9b0",
            "compositeImage": {
                "id": "0a031406-1904-4d19-af86-3bc67e2f90c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac8a838d-7f4f-4a74-917a-df0facc5572c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d12e5d14-6f79-43db-9133-7760468f47b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac8a838d-7f4f-4a74-917a-df0facc5572c",
                    "LayerId": "4a7683a0-1ae1-41f4-855f-dee3cc9d1cd3"
                }
            ]
        },
        {
            "id": "89cebf43-5274-42f5-aa87-db3514b3f6f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8982f23c-c302-4b06-9215-050aad36b9b0",
            "compositeImage": {
                "id": "de43d8e7-98d6-4260-8836-b8a18378ee07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89cebf43-5274-42f5-aa87-db3514b3f6f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e95ed726-ddd5-4924-aa88-5f04676e665f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89cebf43-5274-42f5-aa87-db3514b3f6f7",
                    "LayerId": "4a7683a0-1ae1-41f4-855f-dee3cc9d1cd3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "4a7683a0-1ae1-41f4-855f-dee3cc9d1cd3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8982f23c-c302-4b06-9215-050aad36b9b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}