{
    "id": "f5870948-3c71-4789-a426-a6884a0c2e07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hair2_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 10,
    "bbox_right": 69,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ba74a99-873b-484b-ad73-702f57d7bba1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5870948-3c71-4789-a426-a6884a0c2e07",
            "compositeImage": {
                "id": "26716ca2-a94e-48f5-a16b-64f1f26febbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba74a99-873b-484b-ad73-702f57d7bba1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c60f439-4fb1-4a92-9234-eca4b8044df3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba74a99-873b-484b-ad73-702f57d7bba1",
                    "LayerId": "8f98799e-4700-460c-b7c9-12b41fcd7765"
                }
            ]
        },
        {
            "id": "36ceb42c-a25b-44d6-a61a-1d8c6b6b00d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5870948-3c71-4789-a426-a6884a0c2e07",
            "compositeImage": {
                "id": "138f35bf-bb71-4160-a9e7-f78fa3736fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36ceb42c-a25b-44d6-a61a-1d8c6b6b00d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ebfe1d4-ddd0-4875-9b86-438b4435e5e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36ceb42c-a25b-44d6-a61a-1d8c6b6b00d2",
                    "LayerId": "8f98799e-4700-460c-b7c9-12b41fcd7765"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 95,
    "layers": [
        {
            "id": "8f98799e-4700-460c-b7c9-12b41fcd7765",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5870948-3c71-4789-a426-a6884a0c2e07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 80,
    "xorig": 40,
    "yorig": 51
}