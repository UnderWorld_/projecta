{
    "id": "91726857-4478-4b7f-827b-a14c4735de48",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "goblin2_walk_sprTESTR",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 207,
    "bbox_left": 42,
    "bbox_right": 162,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5edec9b9-ca0f-412b-a0fb-9c12ae9f3204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91726857-4478-4b7f-827b-a14c4735de48",
            "compositeImage": {
                "id": "2cb9fd22-daf4-46fa-a13a-ae6baa6a4cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5edec9b9-ca0f-412b-a0fb-9c12ae9f3204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04fd4d88-f61b-4aab-b03f-db20bc402471",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5edec9b9-ca0f-412b-a0fb-9c12ae9f3204",
                    "LayerId": "f362c314-f7f6-4a36-81ef-f37df496ddce"
                }
            ]
        },
        {
            "id": "d91198a8-b445-4156-83a5-bb826c900e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91726857-4478-4b7f-827b-a14c4735de48",
            "compositeImage": {
                "id": "432068a3-a07e-40c6-bdb6-6eb80e229bab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d91198a8-b445-4156-83a5-bb826c900e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a57ed5d5-aa77-4674-94df-cd31983ffc2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d91198a8-b445-4156-83a5-bb826c900e2d",
                    "LayerId": "f362c314-f7f6-4a36-81ef-f37df496ddce"
                }
            ]
        },
        {
            "id": "d2669745-799d-4375-82f3-2a156e893e8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91726857-4478-4b7f-827b-a14c4735de48",
            "compositeImage": {
                "id": "b8c03142-c405-4bb5-9005-025c3982f5d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2669745-799d-4375-82f3-2a156e893e8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbbf04eb-b1f2-4a50-b183-01660c6314bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2669745-799d-4375-82f3-2a156e893e8c",
                    "LayerId": "f362c314-f7f6-4a36-81ef-f37df496ddce"
                }
            ]
        },
        {
            "id": "170ec06d-c4d3-47b1-be55-a89a73420281",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91726857-4478-4b7f-827b-a14c4735de48",
            "compositeImage": {
                "id": "3c085016-9898-4cb6-a3ae-548e1b8e749a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "170ec06d-c4d3-47b1-be55-a89a73420281",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21f0e0e6-354d-48e3-80a1-d8088da3a281",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "170ec06d-c4d3-47b1-be55-a89a73420281",
                    "LayerId": "f362c314-f7f6-4a36-81ef-f37df496ddce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 112,
    "layers": [
        {
            "id": "f362c314-f7f6-4a36-81ef-f37df496ddce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91726857-4478-4b7f-827b-a14c4735de48",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 5,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 65
}