{
    "id": "fdd071d0-c914-4da2-857f-01213e537407",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mushroom3_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 39,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "643b9e75-4d86-48b5-84b8-4f065583cbc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdd071d0-c914-4da2-857f-01213e537407",
            "compositeImage": {
                "id": "8889cf05-165a-4e31-b96d-c1afef1cfd2e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "643b9e75-4d86-48b5-84b8-4f065583cbc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36718889-1461-43b4-b6fd-4ffc2aeac0b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "643b9e75-4d86-48b5-84b8-4f065583cbc8",
                    "LayerId": "4dbc3a77-a37c-451f-8da6-702703378fec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4dbc3a77-a37c-451f-8da6-702703378fec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdd071d0-c914-4da2-857f-01213e537407",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 40,
    "xorig": 20,
    "yorig": 31
}