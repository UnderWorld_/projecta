{
    "id": "0e9310cd-f7e3-4f44-8517-c2952cb261c0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right15",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fba8ec64-b779-42b1-9075-e8da80927f71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9310cd-f7e3-4f44-8517-c2952cb261c0",
            "compositeImage": {
                "id": "ece5170d-92bf-42c5-a0e2-4fee4d12c38b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fba8ec64-b779-42b1-9075-e8da80927f71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e02dabf-396f-4415-833e-bf98382c990a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fba8ec64-b779-42b1-9075-e8da80927f71",
                    "LayerId": "08e6b37d-38af-4aa2-a96c-6b180d55ed74"
                }
            ]
        },
        {
            "id": "39b76815-54e6-450a-96dc-39510f65925d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e9310cd-f7e3-4f44-8517-c2952cb261c0",
            "compositeImage": {
                "id": "f4b2810b-a935-40a1-bfda-8b5e6bcffa7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39b76815-54e6-450a-96dc-39510f65925d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75cdd8f8-5859-4749-b618-7e0503e1ad41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39b76815-54e6-450a-96dc-39510f65925d",
                    "LayerId": "08e6b37d-38af-4aa2-a96c-6b180d55ed74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "08e6b37d-38af-4aa2-a96c-6b180d55ed74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e9310cd-f7e3-4f44-8517-c2952cb261c0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}