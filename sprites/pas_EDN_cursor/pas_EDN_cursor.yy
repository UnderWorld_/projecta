{
    "id": "5a726e62-2c63-45dc-841e-a751577ab881",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pas_EDN_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99779cb9-731a-45c6-b67b-54b8319ead58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5a726e62-2c63-45dc-841e-a751577ab881",
            "compositeImage": {
                "id": "b965b488-a114-4ef8-9f3a-27e297227f17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99779cb9-731a-45c6-b67b-54b8319ead58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5a533b0-3f4e-406a-907e-ad56a744b6bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99779cb9-731a-45c6-b67b-54b8319ead58",
                    "LayerId": "a0497141-c550-4f55-9ed1-bea79499cbb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "a0497141-c550-4f55-9ed1-bea79499cbb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5a726e62-2c63-45dc-841e-a751577ab881",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}