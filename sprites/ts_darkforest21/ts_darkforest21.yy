{
    "id": "1b61d093-0807-4680-9877-2894cd7d1ae4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_darkforest21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e589d0d-1ef2-4bc4-ba57-9e875a6b8162",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b61d093-0807-4680-9877-2894cd7d1ae4",
            "compositeImage": {
                "id": "c71e5796-ccda-4a91-b3d5-d9593ed27b3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e589d0d-1ef2-4bc4-ba57-9e875a6b8162",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5044eaf-a5f3-442e-a728-504bac1cbe24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e589d0d-1ef2-4bc4-ba57-9e875a6b8162",
                    "LayerId": "796245b7-09f9-4e5c-8729-982529e4f69b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "796245b7-09f9-4e5c-8729-982529e4f69b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b61d093-0807-4680-9877-2894cd7d1ae4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}