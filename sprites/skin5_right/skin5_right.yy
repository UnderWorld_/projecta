{
    "id": "3373cddc-830a-43f7-bd45-5dd51f615af4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin5_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "275be4b3-dc3e-4fb8-9339-a6abeb0dadbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3373cddc-830a-43f7-bd45-5dd51f615af4",
            "compositeImage": {
                "id": "6f978459-1e72-4231-b766-489ff12f0448",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275be4b3-dc3e-4fb8-9339-a6abeb0dadbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c33365f-82bb-41d2-95a8-acdb56ddb803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275be4b3-dc3e-4fb8-9339-a6abeb0dadbf",
                    "LayerId": "71d1d0ff-924f-4aba-a2ed-50dc6e21329f"
                }
            ]
        },
        {
            "id": "b41c3d76-7b03-4ad2-8a37-4c0865e62dd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3373cddc-830a-43f7-bd45-5dd51f615af4",
            "compositeImage": {
                "id": "215778ac-ba2f-418e-9c90-06d9dc9f6d74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b41c3d76-7b03-4ad2-8a37-4c0865e62dd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f7511e0-5d59-4ee0-891d-21003f4ee3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b41c3d76-7b03-4ad2-8a37-4c0865e62dd6",
                    "LayerId": "71d1d0ff-924f-4aba-a2ed-50dc6e21329f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "71d1d0ff-924f-4aba-a2ed-50dc6e21329f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3373cddc-830a-43f7-bd45-5dd51f615af4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}