{
    "id": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite67",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 39,
    "bbox_right": 142,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4a603b9-22ba-4237-a0c0-485c7565f611",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "36484930-71e9-4212-a601-d4799d45a1c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a603b9-22ba-4237-a0c0-485c7565f611",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fd11dfa-4fc2-4719-ad8c-8703ad889622",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a603b9-22ba-4237-a0c0-485c7565f611",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "cfb67f12-81fb-42d7-88d0-99449a9f95a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "ea58f58c-baca-4ef3-8b68-a156d930f7ea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfb67f12-81fb-42d7-88d0-99449a9f95a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bb9439-e557-4e62-be97-91124b7ce621",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfb67f12-81fb-42d7-88d0-99449a9f95a6",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "ff2224f6-2c42-441a-906b-c5598c3b33b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "27b3338f-19ee-429b-b1cd-0d7faa7ebe7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff2224f6-2c42-441a-906b-c5598c3b33b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de33d010-f4d0-4f87-a210-35585f1b7601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff2224f6-2c42-441a-906b-c5598c3b33b3",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "ea835b63-61ae-4926-8df2-f61eac04e71e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "277d1c59-abed-479b-8ed9-e079fed1ad07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea835b63-61ae-4926-8df2-f61eac04e71e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b09e8896-fb78-482b-be08-b0872097fe91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea835b63-61ae-4926-8df2-f61eac04e71e",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "a2b24dab-9679-4a63-9d04-6ad7666a639f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "de2498b1-542c-4a7c-8e43-fc70af01085c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2b24dab-9679-4a63-9d04-6ad7666a639f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24cbd045-25ec-43a7-881d-5f6b95e902d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2b24dab-9679-4a63-9d04-6ad7666a639f",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "e855c565-856e-466a-a335-b2922d6f6112",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "bba3e9e1-8959-4e4d-bdb8-bbf331f6989d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e855c565-856e-466a-a335-b2922d6f6112",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "506a3d0f-f37b-4e3e-8d60-d2059b744226",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e855c565-856e-466a-a335-b2922d6f6112",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "398b66e0-a444-4608-9bc4-436363f167f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "bd381a7c-0cb8-4d18-b77f-70b7292a8e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "398b66e0-a444-4608-9bc4-436363f167f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "075b3d9a-3ec3-4709-bf54-5de71887d8be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "398b66e0-a444-4608-9bc4-436363f167f5",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "a1f7ee98-df39-42d0-b3b0-c85bbb933d1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "73335831-2e78-4baa-82fc-30585bbe6dd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1f7ee98-df39-42d0-b3b0-c85bbb933d1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03f2253c-a0bf-4eed-9faa-ed138335c076",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1f7ee98-df39-42d0-b3b0-c85bbb933d1f",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "bb41fce8-8fe4-4b5b-8f7a-70f8cc286b52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "b6c438ee-7e95-46a5-9b5f-04b183c1f486",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb41fce8-8fe4-4b5b-8f7a-70f8cc286b52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ec0460-3698-4a4b-ae02-9a5ae1283612",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb41fce8-8fe4-4b5b-8f7a-70f8cc286b52",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "382c6203-c68a-4dbd-8b5a-2debac2ef00b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "908d1108-d919-4448-94c7-9e6fc4bc791c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "382c6203-c68a-4dbd-8b5a-2debac2ef00b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3603ea2-35ac-4343-86bf-c8496d0e7550",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "382c6203-c68a-4dbd-8b5a-2debac2ef00b",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "c4b13ef9-7402-4736-83af-ed20873e5233",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "12447f41-2351-4c98-8182-5e14b084c1d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b13ef9-7402-4736-83af-ed20873e5233",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "723693a8-ae0e-49fa-8868-ad654cf7b291",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b13ef9-7402-4736-83af-ed20873e5233",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "0a5cd6af-6eae-47c1-80a5-05818bc18b5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "08a650c2-6477-44c5-86eb-bd7770ccdb2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a5cd6af-6eae-47c1-80a5-05818bc18b5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf9cc5c0-bb53-4e7d-9c91-bae9cf9346e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a5cd6af-6eae-47c1-80a5-05818bc18b5c",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "9c27a9e7-9e28-47a5-b2fe-b56cf1a0754c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "7919cb9f-6d89-4e7b-ba23-888a1786922a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c27a9e7-9e28-47a5-b2fe-b56cf1a0754c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ef0a096-98eb-4e17-8d5e-dbb22ba6042b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c27a9e7-9e28-47a5-b2fe-b56cf1a0754c",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "70aadff4-6ff2-4659-953a-1c24b6eb749a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "24ef562f-1077-41ed-b582-6e85d82c7f83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70aadff4-6ff2-4659-953a-1c24b6eb749a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "031de317-1b0d-4de5-a93c-8700f39a6d6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70aadff4-6ff2-4659-953a-1c24b6eb749a",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        },
        {
            "id": "b045d758-d6d2-4f88-8cfd-f688655cb629",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "compositeImage": {
                "id": "943e80ba-2502-4364-8938-45ff3cc324af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b045d758-d6d2-4f88-8cfd-f688655cb629",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b8fef0d9-2d62-4eb6-9a4d-dedda1bd37a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b045d758-d6d2-4f88-8cfd-f688655cb629",
                    "LayerId": "6e9fac04-3a69-4884-b179-aeca55c113b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 150,
    "layers": [
        {
            "id": "6e9fac04-3a69-4884-b179-aeca55c113b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c5732a9-9ebf-4530-88a6-f499fd1b8514",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 150,
    "xorig": 0,
    "yorig": 0
}