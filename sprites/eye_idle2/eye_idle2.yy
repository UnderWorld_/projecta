{
    "id": "d2dc839a-f4b3-492f-816a-8c161cac0f04",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_idle2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6c61ca6-3cfc-45e0-aecb-a82d24c9de7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dc839a-f4b3-492f-816a-8c161cac0f04",
            "compositeImage": {
                "id": "4282e57a-b124-47ed-bf39-21550bcc8105",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6c61ca6-3cfc-45e0-aecb-a82d24c9de7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df49f74e-f20c-47a6-bc2f-1bdf60c73932",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6c61ca6-3cfc-45e0-aecb-a82d24c9de7c",
                    "LayerId": "f2325ad1-5449-4488-ad2c-ad52917bdbd1"
                }
            ]
        },
        {
            "id": "6cbf7eea-b395-46d2-a39f-ef4250884ead",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dc839a-f4b3-492f-816a-8c161cac0f04",
            "compositeImage": {
                "id": "19824e23-1721-4636-8c9c-bc0e740de165",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cbf7eea-b395-46d2-a39f-ef4250884ead",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67da077d-7114-4110-8416-96d86e5625f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cbf7eea-b395-46d2-a39f-ef4250884ead",
                    "LayerId": "f2325ad1-5449-4488-ad2c-ad52917bdbd1"
                }
            ]
        },
        {
            "id": "33202ac7-3b6b-49ab-af81-b9c4ea368c09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dc839a-f4b3-492f-816a-8c161cac0f04",
            "compositeImage": {
                "id": "dbcf97c1-d86d-40fc-bead-22620382938c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33202ac7-3b6b-49ab-af81-b9c4ea368c09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddd5245c-bb44-4273-a921-637c0c107938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33202ac7-3b6b-49ab-af81-b9c4ea368c09",
                    "LayerId": "f2325ad1-5449-4488-ad2c-ad52917bdbd1"
                }
            ]
        },
        {
            "id": "334d1a42-0ad7-4c1b-a2b8-30af6efb8ea4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2dc839a-f4b3-492f-816a-8c161cac0f04",
            "compositeImage": {
                "id": "6c3298c3-9062-4c9b-b2c5-6a7243e01f3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "334d1a42-0ad7-4c1b-a2b8-30af6efb8ea4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc1f5ca4-168d-46ae-af60-f82c1bc03ac2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "334d1a42-0ad7-4c1b-a2b8-30af6efb8ea4",
                    "LayerId": "f2325ad1-5449-4488-ad2c-ad52917bdbd1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f2325ad1-5449-4488-ad2c-ad52917bdbd1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2dc839a-f4b3-492f-816a-8c161cac0f04",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}