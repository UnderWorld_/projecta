{
    "id": "67b4b1bd-86eb-4f08-8a9a-faa43d70f5c3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bigtileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5259,
    "bbox_left": 0,
    "bbox_right": 9679,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19dbf9c0-1d1e-485c-9248-16239618f097",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67b4b1bd-86eb-4f08-8a9a-faa43d70f5c3",
            "compositeImage": {
                "id": "d3303400-6cd0-494a-a131-92dbbcee4a2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19dbf9c0-1d1e-485c-9248-16239618f097",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d456750-1de7-46f7-a1f0-70b374625c8a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19dbf9c0-1d1e-485c-9248-16239618f097",
                    "LayerId": "96175d43-3e38-4d8e-8497-b009b9dc59ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 5260,
    "layers": [
        {
            "id": "96175d43-3e38-4d8e-8497-b009b9dc59ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67b4b1bd-86eb-4f08-8a9a-faa43d70f5c3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9680,
    "xorig": 0,
    "yorig": 0
}