{
    "id": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dog__CS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6389e929-1d06-4ffe-a89c-f3e16674f69f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "335227b0-c325-417f-a906-0bf0b6692325",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6389e929-1d06-4ffe-a89c-f3e16674f69f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f71965ad-16d5-45f0-86aa-56ec0f65013f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6389e929-1d06-4ffe-a89c-f3e16674f69f",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "545dee64-f870-4406-b5c2-163fcebbf9b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "983e1c93-bfd5-4096-a45f-a612f1e42552",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "545dee64-f870-4406-b5c2-163fcebbf9b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f47c68d-1fcd-4dce-b547-35266a36fe18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "545dee64-f870-4406-b5c2-163fcebbf9b2",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "c3ed6388-ac1e-44f2-bdcf-56dfebffbd08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "a949a30e-db25-4936-a860-e66d53e9dffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ed6388-ac1e-44f2-bdcf-56dfebffbd08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3950c350-1824-4903-959b-5e2cdf83298f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ed6388-ac1e-44f2-bdcf-56dfebffbd08",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "fe373410-46dc-42ba-9fcf-b4bd592ea79c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "bc3667d2-2993-49f9-867d-6dca4867309e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe373410-46dc-42ba-9fcf-b4bd592ea79c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5888af8e-57dd-43e0-988d-d5edea0e6c9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe373410-46dc-42ba-9fcf-b4bd592ea79c",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "ffa024ab-7231-4252-beb6-afcf3598f25f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "d4cc64ea-8021-4f6d-ada7-69e8d4ffab70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa024ab-7231-4252-beb6-afcf3598f25f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4de77368-736b-4ebe-abe7-70f6c068f79e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa024ab-7231-4252-beb6-afcf3598f25f",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "cd4d879e-9356-42f4-8644-2e8ddc8b87e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "e75d743f-f99d-44a2-b3cd-240abf1267a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd4d879e-9356-42f4-8644-2e8ddc8b87e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638f7104-c4d2-4c29-91fb-44145965d941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd4d879e-9356-42f4-8644-2e8ddc8b87e5",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "c195c4f8-270e-49b2-ba85-6ecf498596fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "b5a212fc-e3b1-4e84-83f5-12bb6b6bbb17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c195c4f8-270e-49b2-ba85-6ecf498596fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30de9416-5fbe-4815-ac5f-d15c8f4e78a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c195c4f8-270e-49b2-ba85-6ecf498596fe",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "b9b9a518-261f-4f9f-9d00-ba854ba70fc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "e45d304c-32c9-4453-babe-12e0c812e1e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9b9a518-261f-4f9f-9d00-ba854ba70fc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5488e999-5991-4f38-96c4-6ed000c6f7e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9b9a518-261f-4f9f-9d00-ba854ba70fc1",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "ee02001b-7f9a-40d9-bc4e-f6df5c31bae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "c5984fa8-0076-48a0-8eaf-feb5de6c3598",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee02001b-7f9a-40d9-bc4e-f6df5c31bae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23d0771a-31f3-41a8-87c9-54f37cfefc44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee02001b-7f9a-40d9-bc4e-f6df5c31bae7",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "e887b31c-9503-4a91-b499-25d0e0811056",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "038e9ee3-e740-490c-9e89-952e6a5a7874",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e887b31c-9503-4a91-b499-25d0e0811056",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7919bf3c-d251-4364-a19d-acef11ba5654",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e887b31c-9503-4a91-b499-25d0e0811056",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "8c766700-d118-4067-863c-3e7514a6e67d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "e43cc41c-47ef-40d6-a3f7-dcd0e418834e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c766700-d118-4067-863c-3e7514a6e67d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab0dde8-7762-4efb-b122-a4b5b8289c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c766700-d118-4067-863c-3e7514a6e67d",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        },
        {
            "id": "3d0c31ce-b5b3-48c0-b19d-f23262f39e51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "compositeImage": {
                "id": "f1483dd2-0608-46b4-9309-b2b2cd25fe27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d0c31ce-b5b3-48c0-b19d-f23262f39e51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51704e11-365a-4bcb-9e70-44197a4262b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d0c31ce-b5b3-48c0-b19d-f23262f39e51",
                    "LayerId": "22e2ab34-f8a4-447e-b4b5-254c28beab67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 87,
    "layers": [
        {
            "id": "22e2ab34-f8a4-447e-b4b5-254c28beab67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 43
}