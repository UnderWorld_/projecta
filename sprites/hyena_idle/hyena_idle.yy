{
    "id": "c20dbebb-75a0-4084-aba6-ceef13372eca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hyena_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 107,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76ab3a78-dfc8-492a-8093-0baaba37a00c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c20dbebb-75a0-4084-aba6-ceef13372eca",
            "compositeImage": {
                "id": "44bc4ded-6909-4229-b681-0011946caf36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76ab3a78-dfc8-492a-8093-0baaba37a00c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "546ca1ad-1698-4711-8e9b-91edda530fdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76ab3a78-dfc8-492a-8093-0baaba37a00c",
                    "LayerId": "302ce065-d044-4e85-93c4-dc6c8ebb9c9b"
                }
            ]
        },
        {
            "id": "c76507f8-710d-4a2e-bc08-5de9bdbfad82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c20dbebb-75a0-4084-aba6-ceef13372eca",
            "compositeImage": {
                "id": "dac6d33b-4606-4775-b763-0a56b300f227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c76507f8-710d-4a2e-bc08-5de9bdbfad82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9dc8ab85-ddaa-4161-9a1f-24959ce67ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c76507f8-710d-4a2e-bc08-5de9bdbfad82",
                    "LayerId": "302ce065-d044-4e85-93c4-dc6c8ebb9c9b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 108,
    "layers": [
        {
            "id": "302ce065-d044-4e85-93c4-dc6c8ebb9c9b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c20dbebb-75a0-4084-aba6-ceef13372eca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 54,
    "yorig": 54
}