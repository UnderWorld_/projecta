{
    "id": "6915f9f9-1de6-48f0-9a6f-e54d9eab6f1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "background_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac5cb1d9-ba3c-456e-84d6-eb2b5d4a0ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6915f9f9-1de6-48f0-9a6f-e54d9eab6f1d",
            "compositeImage": {
                "id": "1f714d25-1079-46fa-9a64-8f381401296d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac5cb1d9-ba3c-456e-84d6-eb2b5d4a0ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c5cdff6-b8a1-4ac0-8d86-6c066e81095b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac5cb1d9-ba3c-456e-84d6-eb2b5d4a0ad1",
                    "LayerId": "ef51d3d7-8a39-40e5-9bc4-2965d5b18fc7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "ef51d3d7-8a39-40e5-9bc4-2965d5b18fc7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6915f9f9-1de6-48f0-9a6f-e54d9eab6f1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}