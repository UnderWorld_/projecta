{
    "id": "0996523f-667f-418e-9ca6-9f937834e325",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd3974cc-f09b-4379-a8e5-8cf0e9ed4b37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0996523f-667f-418e-9ca6-9f937834e325",
            "compositeImage": {
                "id": "cc378bc1-3645-44f9-9db2-d154277e84d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd3974cc-f09b-4379-a8e5-8cf0e9ed4b37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50213e1d-5cd6-4f06-8e50-a3b8d37e7a6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd3974cc-f09b-4379-a8e5-8cf0e9ed4b37",
                    "LayerId": "61ffc4c1-20fe-4bfa-ac38-a370a8cfec24"
                }
            ]
        },
        {
            "id": "1a5691cc-1e8a-4165-8295-3da9a560eb23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0996523f-667f-418e-9ca6-9f937834e325",
            "compositeImage": {
                "id": "4de2d269-32bc-4910-98e2-c767d7fa4df8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a5691cc-1e8a-4165-8295-3da9a560eb23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37edbc80-f09c-4813-8ed9-723930662fca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a5691cc-1e8a-4165-8295-3da9a560eb23",
                    "LayerId": "61ffc4c1-20fe-4bfa-ac38-a370a8cfec24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "61ffc4c1-20fe-4bfa-ac38-a370a8cfec24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0996523f-667f-418e-9ca6-9f937834e325",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}