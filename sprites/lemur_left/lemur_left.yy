{
    "id": "a5b55813-562b-48e4-a4c3-d4ec0dcd1229",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lemur_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 71,
    "bbox_left": 0,
    "bbox_right": 71,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5b81c1d-81b2-4b3d-be4b-7f7c6e5a0b7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5b55813-562b-48e4-a4c3-d4ec0dcd1229",
            "compositeImage": {
                "id": "7b3d8e65-00d6-4656-97be-b61126737ce2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b81c1d-81b2-4b3d-be4b-7f7c6e5a0b7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "600bf007-7223-4ad3-987a-0a1b057a2237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b81c1d-81b2-4b3d-be4b-7f7c6e5a0b7e",
                    "LayerId": "fab7797b-5f0e-4c99-9c5e-5a8baf133b4a"
                }
            ]
        },
        {
            "id": "29b5960c-0dbb-4ba3-9a4b-0babaef7badf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5b55813-562b-48e4-a4c3-d4ec0dcd1229",
            "compositeImage": {
                "id": "520db25c-0f5f-41e1-99f3-7f9bf7485222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29b5960c-0dbb-4ba3-9a4b-0babaef7badf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150a132e-3246-45dd-97f7-a0770055daa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29b5960c-0dbb-4ba3-9a4b-0babaef7badf",
                    "LayerId": "fab7797b-5f0e-4c99-9c5e-5a8baf133b4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "fab7797b-5f0e-4c99-9c5e-5a8baf133b4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5b55813-562b-48e4-a4c3-d4ec0dcd1229",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 72,
    "xorig": 36,
    "yorig": 36
}