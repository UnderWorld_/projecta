{
    "id": "ac2c0c58-a5be-406f-92fa-a306c2cd4bed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ts_forest2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 46,
    "bbox_right": 239,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83e82e0a-b0f1-40ec-aeaf-b5b8c22b1ea6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac2c0c58-a5be-406f-92fa-a306c2cd4bed",
            "compositeImage": {
                "id": "53b31672-d821-4050-8f6a-cf098fe793e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83e82e0a-b0f1-40ec-aeaf-b5b8c22b1ea6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d740cc9e-ab3b-4a28-b3b0-87adc743fb2b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83e82e0a-b0f1-40ec-aeaf-b5b8c22b1ea6",
                    "LayerId": "aef83118-22b9-4313-be09-1cf0498a4cc2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "aef83118-22b9-4313-be09-1cf0498a4cc2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac2c0c58-a5be-406f-92fa-a306c2cd4bed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 240,
    "xorig": 0,
    "yorig": 0
}