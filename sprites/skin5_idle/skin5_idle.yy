{
    "id": "9c81628d-1223-4ce9-967b-1fb86c373d96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin5_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89faaeb1-9a4e-4688-ada3-c921b77c56d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c81628d-1223-4ce9-967b-1fb86c373d96",
            "compositeImage": {
                "id": "def237a8-8755-4df2-bc75-ec563a26f9f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89faaeb1-9a4e-4688-ada3-c921b77c56d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e81bf850-9dc4-4a92-b31e-2531a47820c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89faaeb1-9a4e-4688-ada3-c921b77c56d6",
                    "LayerId": "08aa38b5-0c61-419b-8906-9ff92b158cb3"
                }
            ]
        },
        {
            "id": "9579b9b5-9fa3-496b-bce3-f6190f9dfeba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c81628d-1223-4ce9-967b-1fb86c373d96",
            "compositeImage": {
                "id": "ecae8dc7-3a13-40e0-a613-dc344717f0a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9579b9b5-9fa3-496b-bce3-f6190f9dfeba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfd9d694-e6a2-41de-a219-a8943c9ff9b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9579b9b5-9fa3-496b-bce3-f6190f9dfeba",
                    "LayerId": "08aa38b5-0c61-419b-8906-9ff92b158cb3"
                }
            ]
        },
        {
            "id": "c9c746b6-a736-43f1-b03d-2b628a163564",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c81628d-1223-4ce9-967b-1fb86c373d96",
            "compositeImage": {
                "id": "64774384-1941-41ff-92a2-3e2c94e8794b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9c746b6-a736-43f1-b03d-2b628a163564",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9baabb1c-926c-4628-bbd8-d7b6089688ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9c746b6-a736-43f1-b03d-2b628a163564",
                    "LayerId": "08aa38b5-0c61-419b-8906-9ff92b158cb3"
                }
            ]
        },
        {
            "id": "5d21dbf0-f9d9-40ad-85b4-068a9deaf5c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c81628d-1223-4ce9-967b-1fb86c373d96",
            "compositeImage": {
                "id": "54f03148-656b-4f84-839c-259ad6fbc77c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d21dbf0-f9d9-40ad-85b4-068a9deaf5c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88075a82-a5de-4e87-90b4-059308fada1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d21dbf0-f9d9-40ad-85b4-068a9deaf5c6",
                    "LayerId": "08aa38b5-0c61-419b-8906-9ff92b158cb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "08aa38b5-0c61-419b-8906-9ff92b158cb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c81628d-1223-4ce9-967b-1fb86c373d96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}