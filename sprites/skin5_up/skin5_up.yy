{
    "id": "041a96d5-e97b-4230-bad8-4ff4a9325a3a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin5_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c53aa6ca-953a-4446-ad34-4f04865fcde7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041a96d5-e97b-4230-bad8-4ff4a9325a3a",
            "compositeImage": {
                "id": "1db54a0f-250d-48f9-8b5d-851a1ceecbf4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c53aa6ca-953a-4446-ad34-4f04865fcde7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de721be0-5e3c-47fe-8813-21d78db1d2a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c53aa6ca-953a-4446-ad34-4f04865fcde7",
                    "LayerId": "15a79345-2956-40b2-83dd-d4b1e1d3b02e"
                }
            ]
        },
        {
            "id": "22d9690b-e224-4f90-9024-78612f07f5e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041a96d5-e97b-4230-bad8-4ff4a9325a3a",
            "compositeImage": {
                "id": "a8063b45-43ac-4aac-a7c5-a564796db0ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d9690b-e224-4f90-9024-78612f07f5e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a489c77-ba56-4031-94bd-d35cc26fc57f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d9690b-e224-4f90-9024-78612f07f5e2",
                    "LayerId": "15a79345-2956-40b2-83dd-d4b1e1d3b02e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "15a79345-2956-40b2-83dd-d4b1e1d3b02e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "041a96d5-e97b-4230-bad8-4ff4a9325a3a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}