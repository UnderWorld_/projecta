{
    "id": "0f63e72e-fc3c-4c9f-bad7-44b69b752ab0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "nackground_character_creationscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b1e85c4-cb7a-423b-b4df-2b12d8d0e13a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f63e72e-fc3c-4c9f-bad7-44b69b752ab0",
            "compositeImage": {
                "id": "fab76747-1fb4-4f45-a102-b198a998bb7b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b1e85c4-cb7a-423b-b4df-2b12d8d0e13a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "705b177a-4e90-4e11-bac2-5fd054f44aa8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b1e85c4-cb7a-423b-b4df-2b12d8d0e13a",
                    "LayerId": "32b83b28-c792-4a36-a8ce-f2acc4846d64"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "32b83b28-c792-4a36-a8ce-f2acc4846d64",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f63e72e-fc3c-4c9f-bad7-44b69b752ab0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}