{
    "id": "af988983-8c47-48c5-9260-68e4af3a47c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beardright2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 30,
    "bbox_right": 44,
    "bbox_top": 39,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c4a5b7a-6e13-4173-bdb1-669be62ddafa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af988983-8c47-48c5-9260-68e4af3a47c8",
            "compositeImage": {
                "id": "329ff5bc-ed14-4617-b48f-aadeb5ff9b90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c4a5b7a-6e13-4173-bdb1-669be62ddafa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7db120f7-a56f-4b7c-90d9-cc1793e95bef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c4a5b7a-6e13-4173-bdb1-669be62ddafa",
                    "LayerId": "56c44253-1fbf-4113-9f9f-339aa6b242b7"
                }
            ]
        },
        {
            "id": "473c29f1-4b78-4620-a1d4-3d98b11a8b8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af988983-8c47-48c5-9260-68e4af3a47c8",
            "compositeImage": {
                "id": "6dd3fb42-1a3f-480b-a344-6eef5e7b5b7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "473c29f1-4b78-4620-a1d4-3d98b11a8b8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6045d8f7-508f-4742-80b5-164ee18133f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "473c29f1-4b78-4620-a1d4-3d98b11a8b8a",
                    "LayerId": "56c44253-1fbf-4113-9f9f-339aa6b242b7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "56c44253-1fbf-4113-9f9f-339aa6b242b7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af988983-8c47-48c5-9260-68e4af3a47c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}