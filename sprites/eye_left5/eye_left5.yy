{
    "id": "24a1d226-34b7-41fa-ac27-f0b35ffb9db3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6e4f2b53-64bf-4830-9475-4f95028c7710",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a1d226-34b7-41fa-ac27-f0b35ffb9db3",
            "compositeImage": {
                "id": "0528f13e-4978-4c49-8e73-f952d10fffac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e4f2b53-64bf-4830-9475-4f95028c7710",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6c01ced-cf37-414e-adf5-2920ea623392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e4f2b53-64bf-4830-9475-4f95028c7710",
                    "LayerId": "7edcab36-730c-47db-b45b-36e1b7b4ab57"
                }
            ]
        },
        {
            "id": "03b08c7f-b56a-444b-b546-9ded75b17b1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "24a1d226-34b7-41fa-ac27-f0b35ffb9db3",
            "compositeImage": {
                "id": "c63be741-ecc3-4ce6-a3ff-e33116b52be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03b08c7f-b56a-444b-b546-9ded75b17b1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16884b5c-dd16-4ffb-a65f-ba7f8907f98a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03b08c7f-b56a-444b-b546-9ded75b17b1f",
                    "LayerId": "7edcab36-730c-47db-b45b-36e1b7b4ab57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "7edcab36-730c-47db-b45b-36e1b7b4ab57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "24a1d226-34b7-41fa-ac27-f0b35ffb9db3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}