{
    "id": "9979e0cc-d07f-4a32-a5d8-cb82d3f9bf83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "lion_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 20,
    "bbox_right": 154,
    "bbox_top": 65,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eea69985-b70a-490c-81bc-e093586b0637",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9979e0cc-d07f-4a32-a5d8-cb82d3f9bf83",
            "compositeImage": {
                "id": "8cb691ab-46b5-4fb2-9009-e934fecb0350",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eea69985-b70a-490c-81bc-e093586b0637",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c8b8631-c542-46b7-935a-57f8dfdc11c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eea69985-b70a-490c-81bc-e093586b0637",
                    "LayerId": "fc5cac94-1cc3-47d9-8c18-cdeeb73da27d"
                }
            ]
        },
        {
            "id": "2ae1e03c-90a2-4bc4-94e7-eb27082e7ab8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9979e0cc-d07f-4a32-a5d8-cb82d3f9bf83",
            "compositeImage": {
                "id": "05b489df-548c-43f2-9d3b-520c1eb96e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae1e03c-90a2-4bc4-94e7-eb27082e7ab8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbc25aad-ff6a-4ec6-a800-7bbf0da27118",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae1e03c-90a2-4bc4-94e7-eb27082e7ab8",
                    "LayerId": "fc5cac94-1cc3-47d9-8c18-cdeeb73da27d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "fc5cac94-1cc3-47d9-8c18-cdeeb73da27d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9979e0cc-d07f-4a32-a5d8-cb82d3f9bf83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 80
}