{
    "id": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "dungeon_wall2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe2f0a6b-2320-4ab0-8aee-3fd29ef71811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "267037c4-0259-47db-9668-bbc6e50b39b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe2f0a6b-2320-4ab0-8aee-3fd29ef71811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16a06044-f25a-484a-a01e-53f7ee69401b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe2f0a6b-2320-4ab0-8aee-3fd29ef71811",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "fb57ae3e-83ea-4a84-bc1f-15e2d9becf0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "9a672a35-287b-441f-9d04-0f03f62ec911",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb57ae3e-83ea-4a84-bc1f-15e2d9becf0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992bfac2-a454-4bb3-859b-2c2617e3c796",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb57ae3e-83ea-4a84-bc1f-15e2d9becf0a",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "41679bf8-de1b-44d6-beaf-4ed35e382adb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "83df26ae-ef88-44be-bb2c-a2e05cbb054f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41679bf8-de1b-44d6-beaf-4ed35e382adb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05244d4d-7dcf-4a65-bcaa-50960c5e899a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41679bf8-de1b-44d6-beaf-4ed35e382adb",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e45a2af6-704b-4afe-9eca-3b606ffc3a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "f3671402-c939-4ab3-835a-cbdd7e369c8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e45a2af6-704b-4afe-9eca-3b606ffc3a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73731c27-f6fb-4acc-8e14-a9d994354dfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e45a2af6-704b-4afe-9eca-3b606ffc3a35",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d6482836-1dd9-4862-bdc2-304b3ead631c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "dc43cd62-2110-41e7-87b4-cd0e724a7e9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6482836-1dd9-4862-bdc2-304b3ead631c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f8388b1-0f4a-4959-923b-213751e450c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6482836-1dd9-4862-bdc2-304b3ead631c",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "f38e2b94-0dae-4638-8d5c-d7ab1889d81c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "d4d8f303-614f-47a7-90a3-663c0250cb29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f38e2b94-0dae-4638-8d5c-d7ab1889d81c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c633c2cc-20b0-49b9-a47b-1517bc013d6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f38e2b94-0dae-4638-8d5c-d7ab1889d81c",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "4fb7ff39-696f-48cb-866b-5149adb6c53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "f727d27b-4b59-4e16-b113-f2fb452a77bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fb7ff39-696f-48cb-866b-5149adb6c53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e2657b9-6559-4657-b0b3-155340a41ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fb7ff39-696f-48cb-866b-5149adb6c53d",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "82a48666-d1ca-46fd-9e1b-526747db3e27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "47cefaf1-df7c-4db3-8254-9bd770003301",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a48666-d1ca-46fd-9e1b-526747db3e27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b8c7de-8785-49f5-9c31-81e5f44ccd13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a48666-d1ca-46fd-9e1b-526747db3e27",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "b8c1036e-861b-4954-a22b-4d6f47c383d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "52f8b807-010e-41e1-8edd-a6c2cfcbff65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8c1036e-861b-4954-a22b-4d6f47c383d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e46a00f-ab52-4cf4-ae30-4cbd7ec2d26a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8c1036e-861b-4954-a22b-4d6f47c383d2",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d6e28712-2775-4e8f-8f98-f1ea48b8f3c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "d08626d8-7e19-45bc-b340-d8ace2f27d93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e28712-2775-4e8f-8f98-f1ea48b8f3c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0f90dc-8499-4d24-8477-102875b208eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e28712-2775-4e8f-8f98-f1ea48b8f3c8",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "dc407077-f384-4183-90c7-02bc04ba9680",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ea330865-13fb-439a-8797-f09aedc74191",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc407077-f384-4183-90c7-02bc04ba9680",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89537ecb-2e6d-4e46-9191-ccb2e8044bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc407077-f384-4183-90c7-02bc04ba9680",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e440ad56-427d-462a-b821-74c0e26ea78d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "a9b07aac-3874-4542-ad92-6ba1f9d709de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e440ad56-427d-462a-b821-74c0e26ea78d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1418613f-b452-489d-a65a-53d9c7643d02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e440ad56-427d-462a-b821-74c0e26ea78d",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "490deaeb-2458-46ae-a62a-a94e57085287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "572ce52e-fc1e-4923-a205-400539e20b1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "490deaeb-2458-46ae-a62a-a94e57085287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84baf220-9867-490f-af06-0b47779145e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "490deaeb-2458-46ae-a62a-a94e57085287",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "34c495bc-6b40-4cad-82d1-c77e59bfae8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "90396ebd-b6ee-491f-b8bc-a9402b69754d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34c495bc-6b40-4cad-82d1-c77e59bfae8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d390346-fce3-40d1-9039-4d3f291b9ec8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34c495bc-6b40-4cad-82d1-c77e59bfae8f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "bf4455ea-a6db-4f3e-af01-6baf25e919e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "be65632c-efdd-462c-b369-b5f12e093664",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4455ea-a6db-4f3e-af01-6baf25e919e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c630904-b9f1-4cc3-93d5-ac7f18cb05a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4455ea-a6db-4f3e-af01-6baf25e919e6",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "0a4385aa-007c-4612-adae-afcb7e7880e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "8987200e-17fc-4ffe-b68e-a4e37e7aec59",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a4385aa-007c-4612-adae-afcb7e7880e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c11d485-ddac-489c-ac85-810010fac805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a4385aa-007c-4612-adae-afcb7e7880e2",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "98ed7fed-be6c-480d-af65-fe0d9538ae71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "5b8ad867-bda6-46e2-b5b6-b43ffffa17f8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98ed7fed-be6c-480d-af65-fe0d9538ae71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c49b2c4-6db4-4ca3-8274-98aa779c31de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98ed7fed-be6c-480d-af65-fe0d9538ae71",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "aa548566-2237-4410-a8c7-5d6565e04db0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "c189bffd-e9c3-4388-9189-040df978c0e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa548566-2237-4410-a8c7-5d6565e04db0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "190b892b-5826-454f-93b1-7907de6f2ad7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa548566-2237-4410-a8c7-5d6565e04db0",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "1bc0dc90-bd6a-423e-a74f-f757b7e01080",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "0f50769a-fc43-4fcb-b4d0-1908248d992c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bc0dc90-bd6a-423e-a74f-f757b7e01080",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df8eb56c-4fb6-48ed-ae32-b7a7f296ba79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bc0dc90-bd6a-423e-a74f-f757b7e01080",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "7329458d-b34f-4c2a-96c8-74eab03bfb4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "1cf5a10a-0f06-448e-b467-3e75194d8d19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7329458d-b34f-4c2a-96c8-74eab03bfb4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aacf2bbb-9057-48f7-a171-66f458d98c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7329458d-b34f-4c2a-96c8-74eab03bfb4f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "01578509-e6d8-439c-9a3b-cbf5657eefb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "02dc7435-2d31-4c47-844e-225188f3eec4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01578509-e6d8-439c-9a3b-cbf5657eefb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7ad9268-8693-4c20-8d97-09ddac55ab73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01578509-e6d8-439c-9a3b-cbf5657eefb0",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "830e96c0-c1ff-466b-994b-37ed3c425103",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e0e41c31-6d09-4d83-9e11-5336972ab7b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "830e96c0-c1ff-466b-994b-37ed3c425103",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e5c3c2-beaa-4b0e-840b-78d8b100f901",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "830e96c0-c1ff-466b-994b-37ed3c425103",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "4e6a40f7-0378-4c37-9eee-5870fb6a32ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ec1c93c2-c83a-48fa-959b-733176cd2af9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e6a40f7-0378-4c37-9eee-5870fb6a32ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfe527c6-68bf-4024-b35c-e964a5ebda56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e6a40f7-0378-4c37-9eee-5870fb6a32ff",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "51a9a562-b68c-4127-ac15-9d1e663deba4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "1af6ce27-62b0-4c58-9161-8a14fc2100e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a9a562-b68c-4127-ac15-9d1e663deba4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f754a1a7-f99f-4f18-93db-15a805373666",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a9a562-b68c-4127-ac15-9d1e663deba4",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "03f6ee0c-78b0-4395-9ee1-974c45bbbfcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "3d42937f-d19b-4fff-803d-70e3b33f71fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f6ee0c-78b0-4395-9ee1-974c45bbbfcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99101cdd-772b-4d2c-a3a2-d292224511f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f6ee0c-78b0-4395-9ee1-974c45bbbfcc",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "af738fb0-a5ae-45fe-8dd0-1540919925e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "6e0dc080-c214-4e88-8180-f7856a700b8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af738fb0-a5ae-45fe-8dd0-1540919925e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2b5d71c-7178-408c-926d-8257c15b49fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af738fb0-a5ae-45fe-8dd0-1540919925e5",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "daba0252-7623-450b-bde9-428575fff7ce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "611d83d7-fc37-45fa-b78c-aa35be2ee51c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "daba0252-7623-450b-bde9-428575fff7ce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a599680d-5485-4cad-98e0-8fbd51ff7a0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "daba0252-7623-450b-bde9-428575fff7ce",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e7b86e97-7455-4518-b9a4-405c7180364d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e6f1432b-6ea3-4a8c-be38-8ec876c5648a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7b86e97-7455-4518-b9a4-405c7180364d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "493c2b71-7d83-44f5-8557-4b19ada317d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7b86e97-7455-4518-b9a4-405c7180364d",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "01dd85bc-52ee-4a2b-944c-84533af590cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "932d514b-36f8-4a80-ba02-38077586c441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01dd85bc-52ee-4a2b-944c-84533af590cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be40818e-d7d9-4ceb-95fc-6f59fefba140",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01dd85bc-52ee-4a2b-944c-84533af590cb",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "ecc49c81-f80e-4b6c-bc18-77b8fa544955",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "92a3cd54-7c63-4dfb-93ee-74b014271034",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecc49c81-f80e-4b6c-bc18-77b8fa544955",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "548e5ada-60b8-4d52-83be-21db0079d061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecc49c81-f80e-4b6c-bc18-77b8fa544955",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "51fe86ea-d36b-4dc5-ade7-40129854c931",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ee91e873-9c12-4873-9b50-a9c4d6c60ea8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51fe86ea-d36b-4dc5-ade7-40129854c931",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "671cb8b8-1b32-455a-8f12-cba41e3a8b44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51fe86ea-d36b-4dc5-ade7-40129854c931",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "3c15d328-f783-4e87-bd04-ddc1bcc55198",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ddad255f-d03f-450a-b756-b4b82615a166",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c15d328-f783-4e87-bd04-ddc1bcc55198",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7758039e-d987-40f3-8911-1e698a0a31ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c15d328-f783-4e87-bd04-ddc1bcc55198",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "2887f5f2-4571-4ed3-8340-e96c1bb4eff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "1163829d-6dd3-42bd-a745-abe9ad135a5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2887f5f2-4571-4ed3-8340-e96c1bb4eff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7994b2ff-63d3-4473-94fb-47c63cb56373",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2887f5f2-4571-4ed3-8340-e96c1bb4eff4",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "24e777f1-00b4-4dd5-bf64-666f349a6171",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "d404b936-0393-498e-9317-b7e7dd4aafff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24e777f1-00b4-4dd5-bf64-666f349a6171",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc51b8ac-03c7-4802-81df-31cfd8a31a14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24e777f1-00b4-4dd5-bf64-666f349a6171",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "5b0aeadc-d723-4350-ad67-f02e3d42c76f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "a6aacd82-0889-443e-874b-6ecc4394e2d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0aeadc-d723-4350-ad67-f02e3d42c76f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "552c200a-ea91-4e29-960f-761f42803048",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0aeadc-d723-4350-ad67-f02e3d42c76f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "9a1314fc-c8b1-487a-97de-8224a449cd93",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "a6791b61-0955-4c05-94b6-e6d4c5b7bd2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1314fc-c8b1-487a-97de-8224a449cd93",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74cf836e-718e-4fc4-8a19-3c5269061729",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1314fc-c8b1-487a-97de-8224a449cd93",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "3d5dca65-ffa7-43f1-b98c-4adb246ae859",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "a4647fe8-4193-4176-b0ba-ad54bc005cc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d5dca65-ffa7-43f1-b98c-4adb246ae859",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b02b7c4-72b2-41d7-9c2d-406835595889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d5dca65-ffa7-43f1-b98c-4adb246ae859",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "fefe50af-fac1-4361-bd26-e8730fc79e59",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "48c85ac7-c4a8-470c-9b15-e30fbf324b0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fefe50af-fac1-4361-bd26-e8730fc79e59",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658a8b84-c1f4-4404-b490-70691aa324cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fefe50af-fac1-4361-bd26-e8730fc79e59",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "acfea5be-0a17-49ac-a7dc-fee51392fd9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "be921b69-1489-4481-827d-bfc9ccaed65d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acfea5be-0a17-49ac-a7dc-fee51392fd9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb1bfe42-c076-4d49-bdbf-a65a4941085f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acfea5be-0a17-49ac-a7dc-fee51392fd9f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "5e6031cc-8b98-4b5b-a1c3-f906923d75eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "5b60d752-a038-4e37-a178-d42504712a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e6031cc-8b98-4b5b-a1c3-f906923d75eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68a20519-b0b6-499a-a55b-67c6943fb17f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e6031cc-8b98-4b5b-a1c3-f906923d75eb",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "6520abc1-4896-4855-8afa-f5b4a18de8cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "14b26aa1-c8dc-4932-9aac-e5fada24381e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6520abc1-4896-4855-8afa-f5b4a18de8cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cd8cbb5-894a-4e84-b980-b91726e15710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6520abc1-4896-4855-8afa-f5b4a18de8cf",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "95b057ad-04a3-442c-a917-b6fed4988123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "9c9fc768-9d11-4183-a8f6-953ad797bd72",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b057ad-04a3-442c-a917-b6fed4988123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa948aad-5cce-4b26-a635-25938696fb87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b057ad-04a3-442c-a917-b6fed4988123",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "7d557c2f-22fe-4e87-b590-d650812a6f27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "565455e7-e673-46dc-ab45-b26f335079fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d557c2f-22fe-4e87-b590-d650812a6f27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d336ff60-932e-4bcd-887c-1812bfd3a4ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d557c2f-22fe-4e87-b590-d650812a6f27",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e2299be6-d4ec-4987-9ee4-d4d1398d3d53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "0842c893-781b-47bc-a1ad-ab704ee86222",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2299be6-d4ec-4987-9ee4-d4d1398d3d53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d16e0cf-e780-4de4-ba86-6520de064f93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2299be6-d4ec-4987-9ee4-d4d1398d3d53",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "10362b13-53d5-43f5-ad62-5580d6ffaae6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "f37a530d-7c31-4b90-8d06-8e82e8f9f4d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10362b13-53d5-43f5-ad62-5580d6ffaae6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ca0dc9b-c7a1-4da5-9552-ba0a53761867",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10362b13-53d5-43f5-ad62-5580d6ffaae6",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "3cbc8676-4d70-47a5-9a3c-484e1eb78f56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e19cc934-6ab5-44d6-b1d2-3fdc6b514d50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cbc8676-4d70-47a5-9a3c-484e1eb78f56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8787240e-88db-4dc7-bb7b-7a574ea29a61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cbc8676-4d70-47a5-9a3c-484e1eb78f56",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "be4469ae-d1ff-4076-9dff-26d747b4f4d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ffce8318-0c09-4138-bf7e-0f5e2c46e855",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be4469ae-d1ff-4076-9dff-26d747b4f4d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7efc76d2-7315-43e8-a49d-9cedc8493361",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be4469ae-d1ff-4076-9dff-26d747b4f4d6",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "32f17a04-4f5a-4c55-a2c0-2eeada92645c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "41cf29e7-46bb-4b91-92da-5978f62c319a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32f17a04-4f5a-4c55-a2c0-2eeada92645c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02215db9-43fa-4575-b76a-fc9f84744f4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32f17a04-4f5a-4c55-a2c0-2eeada92645c",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c2d97827-4dbb-435c-af9f-c50ea3835dbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e61a1790-9857-45fb-9a28-f29e1415025b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2d97827-4dbb-435c-af9f-c50ea3835dbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "676fd0a5-0a15-407a-9994-0ebcdac19508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2d97827-4dbb-435c-af9f-c50ea3835dbc",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "85d72e0e-2ccf-479f-950d-ce0b64701716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "3872d16a-206a-4b5d-8216-bac6377abfda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85d72e0e-2ccf-479f-950d-ce0b64701716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85cf8a9a-4544-46b9-8189-6e9302fdf8ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85d72e0e-2ccf-479f-950d-ce0b64701716",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c7824f20-8421-40cf-8977-21cae3cb6252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "9e9b050e-9f3e-45fe-b37e-723f4d5aad57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7824f20-8421-40cf-8977-21cae3cb6252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aedc790f-687a-46dc-bb76-7b124a57294b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7824f20-8421-40cf-8977-21cae3cb6252",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "6247f3dc-4beb-46bf-a3ed-a38dde445b1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "54f35195-96bb-4736-96f0-066993134103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6247f3dc-4beb-46bf-a3ed-a38dde445b1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25559ffb-a1c3-47e0-8cf8-961542ba7753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6247f3dc-4beb-46bf-a3ed-a38dde445b1e",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "72233900-d0dd-4cb3-b191-2a00ad669d30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "3405dbac-1a9b-49d2-9c08-25002e6cd380",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72233900-d0dd-4cb3-b191-2a00ad669d30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bb0fdc-5e45-45dc-b61f-76b7d134ec60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72233900-d0dd-4cb3-b191-2a00ad669d30",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "9888985c-3158-4535-a03b-0ff1c8affe66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "b157a4c3-11f2-483f-9d0d-24fe92aa6ef9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9888985c-3158-4535-a03b-0ff1c8affe66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bd5ed8a-e9f9-477d-beca-f3205a470fad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9888985c-3158-4535-a03b-0ff1c8affe66",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "caa232ea-2974-46f5-819c-fe90f515df73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "345dd93b-0310-46ce-9052-d69e13bd9726",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caa232ea-2974-46f5-819c-fe90f515df73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22d44e32-97f5-48fe-8aca-5f4614c3b7f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caa232ea-2974-46f5-819c-fe90f515df73",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "4bdb2be3-382c-4b21-9d65-d7bd4ef13c90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "2a009e74-6c52-45ee-9301-58ecac33f082",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bdb2be3-382c-4b21-9d65-d7bd4ef13c90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19c2fca5-0538-43c9-9613-2af057a958e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bdb2be3-382c-4b21-9d65-d7bd4ef13c90",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "7db06258-2f61-49a0-8f0e-455aed8b6258",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "96e2af82-994c-47b1-b57e-f2898dd6fe13",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7db06258-2f61-49a0-8f0e-455aed8b6258",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b49b7e12-3ea7-48e7-ae3c-219539e4cfef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7db06258-2f61-49a0-8f0e-455aed8b6258",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "90f7f9c4-71e0-48e2-ac0f-352af38ff49b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "d489f39a-8d03-4ea9-9132-a8987a40f2c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f7f9c4-71e0-48e2-ac0f-352af38ff49b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0993fe81-fec2-46ff-a0f1-a90843aefd3c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f7f9c4-71e0-48e2-ac0f-352af38ff49b",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "664b5e17-e585-46b9-954b-19c8c509cfce",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "727382fb-0453-42e1-b899-fafd1490bdeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "664b5e17-e585-46b9-954b-19c8c509cfce",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc0d2bb-2cc5-4b0a-af16-4b9d69cdaaf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "664b5e17-e585-46b9-954b-19c8c509cfce",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e660c670-82b8-49cd-87e7-7f7d5156218e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e86af117-6dea-4bc1-8213-283dea8f0999",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e660c670-82b8-49cd-87e7-7f7d5156218e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60bb532b-42ad-478b-9286-1352a04e90fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e660c670-82b8-49cd-87e7-7f7d5156218e",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "f9f80492-9353-4c0b-834f-52dff7a7f161",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ab394311-ac89-43c6-b345-31ff9ff09929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9f80492-9353-4c0b-834f-52dff7a7f161",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "982594ae-5ae1-4f44-aaba-1e74d471afb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9f80492-9353-4c0b-834f-52dff7a7f161",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "97eb95eb-54a9-4b95-9b82-c7df09f6a8c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "844f626e-970e-4ba6-a28c-d65e5a863da9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97eb95eb-54a9-4b95-9b82-c7df09f6a8c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53efa495-9bfa-4a6c-909a-2d68d5f5d606",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97eb95eb-54a9-4b95-9b82-c7df09f6a8c9",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "376d2f89-6836-4202-bb1b-20ec53b2275e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "3dbf2336-27bf-4303-afe3-b350bda009ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "376d2f89-6836-4202-bb1b-20ec53b2275e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c50215ae-b67f-4c45-a42e-fc7159d408d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "376d2f89-6836-4202-bb1b-20ec53b2275e",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "190324a0-6e9d-40b7-8e9e-22d631a636af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e7c48380-bc43-4f42-b3be-aa8ed0517bb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "190324a0-6e9d-40b7-8e9e-22d631a636af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e96053c0-7976-4dfc-8423-576b184303dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "190324a0-6e9d-40b7-8e9e-22d631a636af",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c269615c-cf82-40db-b398-894291ff343f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "37789e01-b555-4c3c-b25e-2bd4c4aa41a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c269615c-cf82-40db-b398-894291ff343f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919e1785-bfc5-4b84-93cf-8335b97448a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c269615c-cf82-40db-b398-894291ff343f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "fb2d13e0-60f4-487d-acaa-18160006215e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "bfe8c44a-50f6-4d4c-affc-a89e1a9642c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb2d13e0-60f4-487d-acaa-18160006215e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8b3303-1c8b-459d-a709-2ea234294992",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb2d13e0-60f4-487d-acaa-18160006215e",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "ca6d2cd9-4de9-4a3e-b830-8d13511364f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "bbe925cf-bbc6-4231-9f8a-1c49f7c4e463",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca6d2cd9-4de9-4a3e-b830-8d13511364f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9d4af75-9fb6-4a7d-a084-116d36198f16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca6d2cd9-4de9-4a3e-b830-8d13511364f0",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "32e614d4-a6ef-4c59-af25-734a8ff15e4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e3af9b9c-e201-4cf7-95d2-482918f53037",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32e614d4-a6ef-4c59-af25-734a8ff15e4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbbab137-ac59-45d6-99f2-bab0cc96e19a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32e614d4-a6ef-4c59-af25-734a8ff15e4e",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "ccb1997b-386b-4a13-b902-1f04ec171857",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "2c69ab54-00cb-45d2-827b-e94a98611353",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ccb1997b-386b-4a13-b902-1f04ec171857",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "150dbb3b-b457-4985-8427-648bdf49bc52",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ccb1997b-386b-4a13-b902-1f04ec171857",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "a2c6df33-45fb-4b45-a5b6-c60ecd7f0d8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "10b3a462-e568-4539-bb7e-c27b42e0af57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2c6df33-45fb-4b45-a5b6-c60ecd7f0d8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c2f9526-4845-47c3-a1ad-230db48f73c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2c6df33-45fb-4b45-a5b6-c60ecd7f0d8e",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "aaf3280f-37f9-49f8-99c3-9dc171f8473a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "894ee8a9-72b3-4c58-bea0-8059ecac1ea2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaf3280f-37f9-49f8-99c3-9dc171f8473a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d5dc6d-e27f-4455-a80c-23450eeb700e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaf3280f-37f9-49f8-99c3-9dc171f8473a",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "2fee068b-19e5-4a63-ac8a-d1882274583a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "a331fa64-a575-4d08-a5dc-8d7f6853aa1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2fee068b-19e5-4a63-ac8a-d1882274583a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2725cdd3-ce58-4ee9-985e-d77ed1511bdb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2fee068b-19e5-4a63-ac8a-d1882274583a",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "747e73d1-f6d4-49ae-adbe-3bb6fb6cacdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "99759b76-35c7-405f-9d4f-b25696c2cb76",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747e73d1-f6d4-49ae-adbe-3bb6fb6cacdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab06df48-cbe0-4336-8335-08e7332ed80c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747e73d1-f6d4-49ae-adbe-3bb6fb6cacdb",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "368fe31a-f7e9-458a-b9b3-bbe9de5da5a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "f3471940-1f78-4d48-aeec-3c5925c6548d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "368fe31a-f7e9-458a-b9b3-bbe9de5da5a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05adf1eb-36a0-4f4a-93da-58df0f8b7994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "368fe31a-f7e9-458a-b9b3-bbe9de5da5a2",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "39e44a42-64b2-4a60-b5b6-8480b50d48b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "1f2f7b95-7991-482a-a1ec-98467a4e5940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e44a42-64b2-4a60-b5b6-8480b50d48b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "166e3d01-9e51-4aad-8110-da99b211b87c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e44a42-64b2-4a60-b5b6-8480b50d48b8",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "76a2a5e7-c58e-4953-b987-7453b2b38d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "cbed9374-2bd8-426d-b99a-a4f56442ea9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76a2a5e7-c58e-4953-b987-7453b2b38d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bac3f2cf-7b6a-4ceb-b2c8-3180d1be187c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76a2a5e7-c58e-4953-b987-7453b2b38d78",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "4bf4afc9-101b-4bd6-ae64-a8e52e0368c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "0fb3f989-b160-464d-a40e-63ab84e0ec25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf4afc9-101b-4bd6-ae64-a8e52e0368c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17fb04aa-46a9-4544-b934-0ca944b99ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf4afc9-101b-4bd6-ae64-a8e52e0368c8",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "9a09ae1f-a23c-42dd-98c7-265acfa9e4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "bb84efaf-ca77-4b77-aa9a-91e90edd50ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a09ae1f-a23c-42dd-98c7-265acfa9e4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfdca4f4-6a1e-4279-9134-1e44f5dd7d72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a09ae1f-a23c-42dd-98c7-265acfa9e4c4",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d8b4fb53-6689-4877-865c-7eb9857ddaea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "be43120b-4550-4f91-a33b-69d6423fbcbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8b4fb53-6689-4877-865c-7eb9857ddaea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ac084543-63a9-4721-bf49-0e04944fa266",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8b4fb53-6689-4877-865c-7eb9857ddaea",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c278401b-c210-401c-a1c9-698880b4b88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "8193a20b-f7e2-4eb0-8c07-0a22ef0362e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c278401b-c210-401c-a1c9-698880b4b88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e1bc05-d700-4a45-a5b9-40255e3fdd78",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c278401b-c210-401c-a1c9-698880b4b88f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "a8f0ad86-9380-426d-93c4-262a1bfe0299",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "581425d4-0dd0-494b-a0df-5c4fa71aff9a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f0ad86-9380-426d-93c4-262a1bfe0299",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c433e4b-0adb-40d1-b52c-bf9c725cf37f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f0ad86-9380-426d-93c4-262a1bfe0299",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d23d6f38-d680-48dd-9d67-2bcfc4a9c27a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "dbd1b896-a5a8-4600-a4e9-53b9828cb36c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d23d6f38-d680-48dd-9d67-2bcfc4a9c27a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b667a57e-bbe5-4b3e-9683-b1e8aac725c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d23d6f38-d680-48dd-9d67-2bcfc4a9c27a",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "491f8162-d413-4310-99eb-0bfaeed5f8e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "105a4a71-de70-45a9-9804-01dea2c8b75e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "491f8162-d413-4310-99eb-0bfaeed5f8e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c64b488d-a0cb-4a02-aeff-dfd284f9d3e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "491f8162-d413-4310-99eb-0bfaeed5f8e2",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e2983b76-a385-43ee-86fc-cda1b5b4e6f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "d142c915-d7c5-451f-be46-f1d66df0246c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2983b76-a385-43ee-86fc-cda1b5b4e6f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3d85833-2631-42e7-bb59-8c60af9c6ea6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2983b76-a385-43ee-86fc-cda1b5b4e6f2",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "59b65ca9-4744-4242-b842-b280389bc772",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "bba64d14-5b73-4963-a4f4-b2a218883323",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59b65ca9-4744-4242-b842-b280389bc772",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "136accff-ef2b-4a07-8f18-8456470b2453",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59b65ca9-4744-4242-b842-b280389bc772",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "8ce49915-0d58-41de-aa28-1ebed29bf80c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "affb4ab6-ffa5-4b04-bc12-54ea760bcf95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ce49915-0d58-41de-aa28-1ebed29bf80c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0cb89b88-6229-4467-ab8c-b5d69dce39a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ce49915-0d58-41de-aa28-1ebed29bf80c",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "705887ef-b6f2-43c3-88bb-49b9cd7134e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "cfbba24f-16bc-483c-8f91-f4df68bca109",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "705887ef-b6f2-43c3-88bb-49b9cd7134e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94a41fe6-2424-44e0-837f-07397ba8f3ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "705887ef-b6f2-43c3-88bb-49b9cd7134e5",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "a7ddf230-11c5-43ea-914d-ff25873f3f1b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "7297e635-7b6e-4de7-a7ab-9b83efe362e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7ddf230-11c5-43ea-914d-ff25873f3f1b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bbf75682-2db5-44b6-b47b-bcb9d128a586",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7ddf230-11c5-43ea-914d-ff25873f3f1b",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "0c1c824a-b8ee-4e68-ba62-ed443fc47af7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "2e130122-716e-4c08-b36a-6614f7ff33b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c1c824a-b8ee-4e68-ba62-ed443fc47af7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77eebf4e-8df0-4b54-8f9c-f697456c4026",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c1c824a-b8ee-4e68-ba62-ed443fc47af7",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "ec5b1946-e82e-4d9f-ad66-7f07317b8efe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "f76fc39f-42e6-4a6c-b8db-fbc4fdd11e93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec5b1946-e82e-4d9f-ad66-7f07317b8efe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52e5601c-3a11-413d-8ed0-45b4d64c10b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec5b1946-e82e-4d9f-ad66-7f07317b8efe",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c9bfc7ec-dc48-492d-b691-7ed1abcefc31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "91341d4f-ec13-4641-adde-7977456dea21",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9bfc7ec-dc48-492d-b691-7ed1abcefc31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33d2e3e3-6529-4940-b006-38b56d872106",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9bfc7ec-dc48-492d-b691-7ed1abcefc31",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "1f041f90-ecab-4d06-b233-13f6c51bd500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "54f9d60a-4aea-4176-af23-08cf25fc1103",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f041f90-ecab-4d06-b233-13f6c51bd500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ede2f783-395f-4919-958d-2e617d686d63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f041f90-ecab-4d06-b233-13f6c51bd500",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "9dc41db2-2b93-49a4-a0bc-180dbe690075",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "bc2fdbf8-5dea-44dd-b9b4-52224dd16ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc41db2-2b93-49a4-a0bc-180dbe690075",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9724e810-2576-458b-9c30-367be6491dab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc41db2-2b93-49a4-a0bc-180dbe690075",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "997a17d2-35e6-4e48-b7f5-50aaa66ef372",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "572c9bd5-cccb-48bd-ae19-aca33da1f048",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "997a17d2-35e6-4e48-b7f5-50aaa66ef372",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a528bdc8-9604-4230-a208-aa19b268d315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "997a17d2-35e6-4e48-b7f5-50aaa66ef372",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "43d79943-0038-448c-9da7-42c9f9bccf29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "c516dee3-7767-46d9-95b5-53a40db027fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43d79943-0038-448c-9da7-42c9f9bccf29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2315762b-8bdd-4c76-b807-0eefb8692ce0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43d79943-0038-448c-9da7-42c9f9bccf29",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "51a19050-8d77-4130-8456-c47dbb757c38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "c552c910-d17d-431b-ba8e-79e2d1a3b7ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51a19050-8d77-4130-8456-c47dbb757c38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16db0f4d-127e-4d21-9f1e-343751f7d38f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51a19050-8d77-4130-8456-c47dbb757c38",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "104c875e-a92c-487d-ba26-97e5f19a4b4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "49506cdc-e0de-4535-8db6-65b5bb126bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "104c875e-a92c-487d-ba26-97e5f19a4b4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33e99856-148d-4890-aa78-dd176312a315",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "104c875e-a92c-487d-ba26-97e5f19a4b4c",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "3f3047fb-645f-4649-840c-32432d1cd715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "37ecbb11-39b1-48e9-bf4b-819c018dac23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3f3047fb-645f-4649-840c-32432d1cd715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6060fd58-1a74-452c-8a0b-a45a12cbf9df",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3f3047fb-645f-4649-840c-32432d1cd715",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "3cf768c7-9a4d-4320-85b6-06cdd456f6c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "212dce14-e01f-45bc-b073-7a0dee4fe231",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cf768c7-9a4d-4320-85b6-06cdd456f6c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f88821eb-a782-4e11-9aa0-d42dde2c684a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cf768c7-9a4d-4320-85b6-06cdd456f6c7",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "ac545fd2-cd96-4775-968e-e56aeebe7b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "53268af9-ad16-4e55-ad82-013707a7cb15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac545fd2-cd96-4775-968e-e56aeebe7b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33a83657-e853-4849-b76e-3fcc05f60a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac545fd2-cd96-4775-968e-e56aeebe7b86",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "80268901-1a6c-43f8-845e-2acaac7fa041",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "83fae001-a390-48c7-a0f4-59f84e520c6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80268901-1a6c-43f8-845e-2acaac7fa041",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36319639-9643-49c7-8902-16113b5ae917",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80268901-1a6c-43f8-845e-2acaac7fa041",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "1638790e-a6a9-48e3-8ccc-133aa6912a69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "441f6e4c-c6c1-4d12-9cb6-4af6fa28ef20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1638790e-a6a9-48e3-8ccc-133aa6912a69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c1587ac-2f54-4c1a-b582-dd43239d8bf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1638790e-a6a9-48e3-8ccc-133aa6912a69",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d4a819ad-8030-4334-bf55-e04100d4ab8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "f82224ed-df49-4530-b433-c9e7088875bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4a819ad-8030-4334-bf55-e04100d4ab8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9063212c-30d8-4df8-9833-e7aa6e85a858",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4a819ad-8030-4334-bf55-e04100d4ab8b",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "ff6f3e4e-51fa-4716-8aef-78a9eb0889d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "440d72f9-f67b-43c4-89ee-4669d159f1c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff6f3e4e-51fa-4716-8aef-78a9eb0889d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6824203-9a7b-411d-ab73-be572a73eaa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff6f3e4e-51fa-4716-8aef-78a9eb0889d9",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "cd0c5646-a680-4ce8-b9f2-fd99878a4cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "470d09f5-d0fc-49fa-b3ab-7055204c0560",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd0c5646-a680-4ce8-b9f2-fd99878a4cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aff29d88-4587-4ce8-890b-03707b750198",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd0c5646-a680-4ce8-b9f2-fd99878a4cbe",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "e4cf7db1-d7a2-4af0-b7f2-fed1c1c4cc08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "053452e9-5efb-4adb-bf63-362b0d105858",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4cf7db1-d7a2-4af0-b7f2-fed1c1c4cc08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfa9f30f-c508-4716-8b95-2875f33e97a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4cf7db1-d7a2-4af0-b7f2-fed1c1c4cc08",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d441a67f-4580-4631-ad04-8637fe9485ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "9ae66159-6b15-49ed-800e-e30fcc8007e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d441a67f-4580-4631-ad04-8637fe9485ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cfa4beb-2cca-4d69-a601-62c85479ec1e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d441a67f-4580-4631-ad04-8637fe9485ed",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "20b63444-8641-4a26-9e15-0dba6e28ff69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "3180b952-cf36-4ea8-8e78-f932a009038a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20b63444-8641-4a26-9e15-0dba6e28ff69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae0fca10-cbb6-4008-a294-a1dba38f9d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20b63444-8641-4a26-9e15-0dba6e28ff69",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "3ae5b4fe-65c6-4864-a455-62da23ca23bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ed1a18c1-60e7-4b18-bd1b-185cef858ccb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ae5b4fe-65c6-4864-a455-62da23ca23bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4244c69c-9c2a-46d5-8a4d-9dc1e3e8910a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ae5b4fe-65c6-4864-a455-62da23ca23bd",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "4859733e-42e7-4cad-abe6-1cc73759189f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "214ed8ba-b904-4f5b-9f25-375df04abf5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4859733e-42e7-4cad-abe6-1cc73759189f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de75dc1d-0233-4fa8-bf8d-3f991a4d0ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4859733e-42e7-4cad-abe6-1cc73759189f",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c729de9b-dea7-45e8-8c36-1e513d89dcd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e6cf4e9c-2d21-49e0-8c93-a53f5a4a3672",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c729de9b-dea7-45e8-8c36-1e513d89dcd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0b7f13c-ec95-4c9c-8deb-0656e1129b62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c729de9b-dea7-45e8-8c36-1e513d89dcd0",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "590e39fa-f593-4d6d-8fef-c2f181710053",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "31856254-9a53-4168-a3f2-d47388fd0893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "590e39fa-f593-4d6d-8fef-c2f181710053",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "794b0fd1-5918-44dd-a8bf-91deb644434f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "590e39fa-f593-4d6d-8fef-c2f181710053",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "906c3661-8d91-4afc-ba48-768fc0e6be8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "0881bf30-f004-496f-9e1b-b14fafca6c5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "906c3661-8d91-4afc-ba48-768fc0e6be8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2453b6b-da63-4267-baa4-2a5f57b36495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "906c3661-8d91-4afc-ba48-768fc0e6be8a",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "1deb2f9a-d5ac-4fc9-858b-9341ec2b6944",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "cc61e092-fb09-4849-b231-78e00f247833",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1deb2f9a-d5ac-4fc9-858b-9341ec2b6944",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eee76f72-6fe1-42f7-aa9e-edae49cc258f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1deb2f9a-d5ac-4fc9-858b-9341ec2b6944",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "55edae66-2e30-434b-b968-3480d9541252",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "86b916e1-82e7-41a3-8252-ffa6cb544eb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55edae66-2e30-434b-b968-3480d9541252",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a4b84e0-80ee-4350-a3da-828f3395c335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55edae66-2e30-434b-b968-3480d9541252",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "cfae1d0f-24f5-4e98-8d9d-0fe3efe40432",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "ed93e83b-2320-4b0a-9ff1-e1a34238dca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfae1d0f-24f5-4e98-8d9d-0fe3efe40432",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d84b1c3a-490c-41c9-97ba-4c521dd98356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfae1d0f-24f5-4e98-8d9d-0fe3efe40432",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "848e7f40-29e9-4733-bd9a-94d01fa95c9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "edc7b69b-c44c-4001-935c-c80b18a4a07a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "848e7f40-29e9-4733-bd9a-94d01fa95c9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99caec2-7b8b-4602-a5c1-cd1f1464689a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "848e7f40-29e9-4733-bd9a-94d01fa95c9a",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "50318d31-287d-4277-98a3-4c2e7782b670",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "21e17cc7-8d86-4612-9ce3-2c8ec0d14266",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50318d31-287d-4277-98a3-4c2e7782b670",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26340baf-c5b6-482f-8f10-618317d40a2c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50318d31-287d-4277-98a3-4c2e7782b670",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "68649aad-d8ec-47cd-9421-fd189c271628",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "8e50b40a-97bb-4dd8-8bcb-339da2e4c2fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68649aad-d8ec-47cd-9421-fd189c271628",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a92fcc96-76d8-4e5c-bcb2-3cebe3a90836",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68649aad-d8ec-47cd-9421-fd189c271628",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "a1bc0578-748a-476d-a340-da34bbd6f64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "c3578823-8a98-4590-b8d1-9c551a6205ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1bc0578-748a-476d-a340-da34bbd6f64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14352ab3-9f79-4bc7-9ef8-2245bda7a983",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1bc0578-748a-476d-a340-da34bbd6f64b",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "8daf3ca5-9ddd-4597-a120-c90c84a0a0aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "2c5aaec5-1591-40e4-8574-6a0cecd2e38e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8daf3ca5-9ddd-4597-a120-c90c84a0a0aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5d95cb9-1324-48ac-b8a9-a3d37b50d147",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8daf3ca5-9ddd-4597-a120-c90c84a0a0aa",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "c8975926-ecd9-4bd4-906a-4d7ade2468e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "3af5e3c4-9ba8-4012-bf8c-ff10b29bf8c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8975926-ecd9-4bd4-906a-4d7ade2468e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "783db7aa-e774-4fa4-9341-e408b8d7b9e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8975926-ecd9-4bd4-906a-4d7ade2468e9",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "aebde705-9262-46d7-9777-8e258791d204",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "38f93791-9a08-479c-9025-a88518f70996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aebde705-9262-46d7-9777-8e258791d204",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eb9bd61-b879-4ef7-ab87-257b6957cb3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aebde705-9262-46d7-9777-8e258791d204",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "b514e427-dacf-4b58-b325-58afefc59854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "60331ecb-13ee-4b35-8f3a-2348ea89c284",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b514e427-dacf-4b58-b325-58afefc59854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ebd731a-1193-4335-89ce-e3e97de64356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b514e427-dacf-4b58-b325-58afefc59854",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "d352ae3f-7a3e-4b50-8006-c2e0e69bbc06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "53ccfd84-e00d-4eff-a8ad-dd58ead31571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d352ae3f-7a3e-4b50-8006-c2e0e69bbc06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b3face-28ff-4571-9cb6-797160a46ce8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d352ae3f-7a3e-4b50-8006-c2e0e69bbc06",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        },
        {
            "id": "7156b4b3-f037-4c89-9a73-a67c47145fbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "compositeImage": {
                "id": "e92cd5f6-ab4b-4662-8ff2-0ec919befa28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7156b4b3-f037-4c89-9a73-a67c47145fbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a22fb51-e599-4587-932b-257a7f693bbf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7156b4b3-f037-4c89-9a73-a67c47145fbd",
                    "LayerId": "ecdcc978-bfee-4743-87f0-87cfc1e485fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "ecdcc978-bfee-4743-87f0-87cfc1e485fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 63,
    "yorig": 12
}