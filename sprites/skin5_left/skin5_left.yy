{
    "id": "e78d383a-41fd-4002-924d-6ef4da35bc45",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "skin5_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1e0256b-aeb8-4ca7-b3d8-bbbb364d9db4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e78d383a-41fd-4002-924d-6ef4da35bc45",
            "compositeImage": {
                "id": "c963d82d-41f5-4c9a-a214-8acebc255101",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e0256b-aeb8-4ca7-b3d8-bbbb364d9db4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7da7361-2149-4c6f-9e58-cd4d45dbc86c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e0256b-aeb8-4ca7-b3d8-bbbb364d9db4",
                    "LayerId": "b54326ea-9604-45a1-8295-157e80bda9a3"
                }
            ]
        },
        {
            "id": "19484109-2931-4e3f-900e-ca3cead43e71",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e78d383a-41fd-4002-924d-6ef4da35bc45",
            "compositeImage": {
                "id": "64a6ec5c-0a5a-4598-ae97-a09c97466126",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19484109-2931-4e3f-900e-ca3cead43e71",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4495ad32-a1bb-4574-9301-760a0fd4ae97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19484109-2931-4e3f-900e-ca3cead43e71",
                    "LayerId": "b54326ea-9604-45a1-8295-157e80bda9a3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "b54326ea-9604-45a1-8295-157e80bda9a3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e78d383a-41fd-4002-924d-6ef4da35bc45",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}