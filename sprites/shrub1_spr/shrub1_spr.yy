{
    "id": "88c3310a-ada0-456b-b0d3-5c21f4174f8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shrub1_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76457797-df2b-4062-96bc-c80118805126",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "88c3310a-ada0-456b-b0d3-5c21f4174f8a",
            "compositeImage": {
                "id": "74c7d5ee-9df7-47a9-a6e2-d0fb713bea80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76457797-df2b-4062-96bc-c80118805126",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "587b0233-54f9-4c94-91a8-9b13617d1fb5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76457797-df2b-4062-96bc-c80118805126",
                    "LayerId": "4f49fec2-5c6c-4a2b-b9c0-ebf3e63ae5b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4f49fec2-5c6c-4a2b-b9c0-ebf3e63ae5b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "88c3310a-ada0-456b-b0d3-5c21f4174f8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 7
}