{
    "id": "a0e0452f-4343-43ca-9642-c53f6cce007c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rhino_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 247,
    "bbox_left": 24,
    "bbox_right": 232,
    "bbox_top": 112,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0b39289-5fe4-46a0-9ad7-ea0d14e106dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e0452f-4343-43ca-9642-c53f6cce007c",
            "compositeImage": {
                "id": "4a75a7fa-9152-4976-af54-4bb0d2a5fdc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0b39289-5fe4-46a0-9ad7-ea0d14e106dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b880bf9d-ede0-474a-ab88-cdc5d85a2615",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0b39289-5fe4-46a0-9ad7-ea0d14e106dd",
                    "LayerId": "ec281500-d204-42b2-af19-c3dff0758a0b"
                }
            ]
        },
        {
            "id": "77f49624-e6bb-4c9c-977f-241c63782497",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a0e0452f-4343-43ca-9642-c53f6cce007c",
            "compositeImage": {
                "id": "808a45aa-43c2-40cc-8cb3-dc9c5f7e45f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77f49624-e6bb-4c9c-977f-241c63782497",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "722a8ce4-7349-49c9-9874-e13314045982",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77f49624-e6bb-4c9c-977f-241c63782497",
                    "LayerId": "ec281500-d204-42b2-af19-c3dff0758a0b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "ec281500-d204-42b2-af19-c3dff0758a0b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a0e0452f-4343-43ca-9642-c53f6cce007c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}