{
    "id": "36d20439-fa5f-47f6-ae70-41130ba1233f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "turtle_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4685298d-8194-4a75-bc64-caa863b808cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d20439-fa5f-47f6-ae70-41130ba1233f",
            "compositeImage": {
                "id": "4eb64445-1d65-4a9a-9266-9c443478f940",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4685298d-8194-4a75-bc64-caa863b808cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5285061-e495-42ba-9c0f-4376836ab133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4685298d-8194-4a75-bc64-caa863b808cc",
                    "LayerId": "a72ceb2f-0edf-43c8-8dc6-2b29d651dc83"
                }
            ]
        },
        {
            "id": "61516ed5-327e-4a77-9476-21e97df5d369",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36d20439-fa5f-47f6-ae70-41130ba1233f",
            "compositeImage": {
                "id": "de5badb7-1046-4532-b5d6-03d408d5a9dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61516ed5-327e-4a77-9476-21e97df5d369",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09a65177-b6a2-4127-b921-2182b4319bfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61516ed5-327e-4a77-9476-21e97df5d369",
                    "LayerId": "a72ceb2f-0edf-43c8-8dc6-2b29d651dc83"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a72ceb2f-0edf-43c8-8dc6-2b29d651dc83",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36d20439-fa5f-47f6-ae70-41130ba1233f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}