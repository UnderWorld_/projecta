{
    "id": "e0af289f-833f-4b11-b797-4aca2e7891f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beard5_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 73,
    "bbox_left": 5,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a52e136d-ab2b-4e5f-8225-e4016b34975b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0af289f-833f-4b11-b797-4aca2e7891f7",
            "compositeImage": {
                "id": "b856ad39-ca00-4dc3-939d-55ec55eeb8f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a52e136d-ab2b-4e5f-8225-e4016b34975b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750a0291-fc66-4802-9e29-f628f971bf55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a52e136d-ab2b-4e5f-8225-e4016b34975b",
                    "LayerId": "3ace892d-6107-4c0a-adcf-88f177e6f02b"
                }
            ]
        },
        {
            "id": "9a1cf397-b8cf-4c56-82a5-2176f14b2ee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0af289f-833f-4b11-b797-4aca2e7891f7",
            "compositeImage": {
                "id": "4d1358bc-7650-46f3-9206-9397adb0978f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a1cf397-b8cf-4c56-82a5-2176f14b2ee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d29b595-2241-4137-8f83-0f6a9d88a212",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a1cf397-b8cf-4c56-82a5-2176f14b2ee7",
                    "LayerId": "3ace892d-6107-4c0a-adcf-88f177e6f02b"
                }
            ]
        },
        {
            "id": "c6d2cf62-6838-4f0c-99e2-49b3ace76b55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0af289f-833f-4b11-b797-4aca2e7891f7",
            "compositeImage": {
                "id": "b0ab6a6b-8b72-43bd-8e31-0203696229fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6d2cf62-6838-4f0c-99e2-49b3ace76b55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e363fae-7b85-431e-b961-7348439dc418",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6d2cf62-6838-4f0c-99e2-49b3ace76b55",
                    "LayerId": "3ace892d-6107-4c0a-adcf-88f177e6f02b"
                }
            ]
        },
        {
            "id": "bfd5899d-e790-49e6-aad5-08bf84413674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0af289f-833f-4b11-b797-4aca2e7891f7",
            "compositeImage": {
                "id": "4f3e6de2-4976-4ec0-9873-d322dab31611",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfd5899d-e790-49e6-aad5-08bf84413674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "800bd696-0a47-4965-9b80-1d0f694282cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfd5899d-e790-49e6-aad5-08bf84413674",
                    "LayerId": "3ace892d-6107-4c0a-adcf-88f177e6f02b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "3ace892d-6107-4c0a-adcf-88f177e6f02b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0af289f-833f-4b11-b797-4aca2e7891f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}