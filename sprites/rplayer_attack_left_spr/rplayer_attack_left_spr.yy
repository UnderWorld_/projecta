{
    "id": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "rplayer_attack_left_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 119,
    "bbox_left": 38,
    "bbox_right": 89,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9c513fd-35f1-4108-83e0-023edbc10a07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "61ee844c-aa01-4ce5-a9a7-c1b0b43a33b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9c513fd-35f1-4108-83e0-023edbc10a07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b73c8800-a7eb-471c-8774-380a6d24dd8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9c513fd-35f1-4108-83e0-023edbc10a07",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "1c9bb4eb-fbf6-493e-9a0c-092d932910cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "c245bdb5-f696-48d3-b4ca-5c2049f3c522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c9bb4eb-fbf6-493e-9a0c-092d932910cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d186328a-8bfc-40b0-afe9-84dc338e14ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c9bb4eb-fbf6-493e-9a0c-092d932910cf",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "40c04b18-42a4-4d52-a7e3-c72e1914ebf5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "aba39b7a-039c-41ed-ac27-169256989b22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40c04b18-42a4-4d52-a7e3-c72e1914ebf5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5eb382b0-c84e-41a4-8e3c-9538b535eba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40c04b18-42a4-4d52-a7e3-c72e1914ebf5",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "abe0dce9-dc22-4a36-9439-105e8a049642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "2f228d0f-7a06-4be9-bd79-5fcc9f6f01fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abe0dce9-dc22-4a36-9439-105e8a049642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1df9361-da92-4627-a206-98f32a5f1edc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abe0dce9-dc22-4a36-9439-105e8a049642",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "a0620e04-fb2c-4462-8fc9-e9d3612a712c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "5286824d-259f-4fe9-a076-34831c47f031",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0620e04-fb2c-4462-8fc9-e9d3612a712c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "527b9525-24a7-467c-863a-766dc515e875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0620e04-fb2c-4462-8fc9-e9d3612a712c",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "d2ff490e-47ce-4202-a039-d1cac25c934e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "13742a2e-e916-433c-965a-657f8937ac23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2ff490e-47ce-4202-a039-d1cac25c934e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0262940e-1db6-4d0f-9f48-0bef2ad54db1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2ff490e-47ce-4202-a039-d1cac25c934e",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "15a85e68-b1d6-48bd-a1b0-9432d670afbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "f321eeba-d0b2-43a6-b769-b433742d7069",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a85e68-b1d6-48bd-a1b0-9432d670afbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672d513b-9131-4b3e-b34a-d7a25d630572",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a85e68-b1d6-48bd-a1b0-9432d670afbf",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "86b37285-bedf-414f-b073-d1bae31b4fb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "a0fcac7a-508c-41d6-91b7-4e5693fabfd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b37285-bedf-414f-b073-d1bae31b4fb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fad25261-e34b-4a94-94ce-c2f888ec6d56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b37285-bedf-414f-b073-d1bae31b4fb7",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "6fbe5c73-db71-4c6c-b82d-c57fe6dff52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "cea6e281-822c-4718-812d-37afc7b29264",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fbe5c73-db71-4c6c-b82d-c57fe6dff52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "faa12396-971c-4a85-82c7-67c6a8fc3dfa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fbe5c73-db71-4c6c-b82d-c57fe6dff52e",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        },
        {
            "id": "7a883f4e-bcb2-466c-aa53-c6dc863b14d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "compositeImage": {
                "id": "91bb947c-a824-4af5-8b90-b182ced23701",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a883f4e-bcb2-466c-aa53-c6dc863b14d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e3c131c-5b83-460e-8284-142aa206fda8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a883f4e-bcb2-466c-aa53-c6dc863b14d2",
                    "LayerId": "aa1313a3-3f52-400b-bce7-ab6291b83a62"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "aa1313a3-3f52-400b-bce7-ab6291b83a62",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d29ecad-6e37-48e1-96ce-7a2e73416930",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 66,
    "yorig": 66
}