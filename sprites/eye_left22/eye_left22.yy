{
    "id": "6b5fe6bc-1273-450f-b982-153f3e31d07d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd9ec904-9af7-4289-9c83-2417d7fc077d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5fe6bc-1273-450f-b982-153f3e31d07d",
            "compositeImage": {
                "id": "58a1feff-dcf2-467f-93a9-4200d19cebe9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9ec904-9af7-4289-9c83-2417d7fc077d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "090e95e7-1fcf-460d-87f0-15efe29dbcda",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9ec904-9af7-4289-9c83-2417d7fc077d",
                    "LayerId": "556d619b-93af-4d34-bf67-6341bb79fe08"
                }
            ]
        },
        {
            "id": "9a7ea336-4a2d-45e7-9592-1f8887beee8b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b5fe6bc-1273-450f-b982-153f3e31d07d",
            "compositeImage": {
                "id": "66b88de1-d855-4842-ad12-31040abf599e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a7ea336-4a2d-45e7-9592-1f8887beee8b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad95fec8-2fb8-4646-a66d-5172be66caa4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a7ea336-4a2d-45e7-9592-1f8887beee8b",
                    "LayerId": "556d619b-93af-4d34-bf67-6341bb79fe08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "556d619b-93af-4d34-bf67-6341bb79fe08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b5fe6bc-1273-450f-b982-153f3e31d07d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}