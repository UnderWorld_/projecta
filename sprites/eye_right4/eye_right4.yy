{
    "id": "1c144ea3-7268-4ebb-8af3-3ea1e72cb7c7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_right4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 25,
    "bbox_right": 49,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "269643fc-bbaa-454a-be84-1752a8d2eb9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c144ea3-7268-4ebb-8af3-3ea1e72cb7c7",
            "compositeImage": {
                "id": "e4da9b0c-cee1-4500-937f-8dea9cde7fef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "269643fc-bbaa-454a-be84-1752a8d2eb9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74401959-69d0-42e0-baad-d20655b1f53c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "269643fc-bbaa-454a-be84-1752a8d2eb9b",
                    "LayerId": "bb07e5a4-652b-4dbb-9d55-1f5388c43815"
                }
            ]
        },
        {
            "id": "309caeeb-0434-4c10-ab67-59eb19e47eb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c144ea3-7268-4ebb-8af3-3ea1e72cb7c7",
            "compositeImage": {
                "id": "3d814a5f-500e-494f-9a4a-47376bee3bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309caeeb-0434-4c10-ab67-59eb19e47eb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e641167-3375-401b-b514-f4a83755caef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309caeeb-0434-4c10-ab67-59eb19e47eb5",
                    "LayerId": "bb07e5a4-652b-4dbb-9d55-1f5388c43815"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "bb07e5a4-652b-4dbb-9d55-1f5388c43815",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c144ea3-7268-4ebb-8af3-3ea1e72cb7c7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}