{
    "id": "2f54033a-b131-4b55-8656-fd1b146ac5fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "eye_left11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 10,
    "bbox_right": 34,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d9275b84-9183-47e4-aea9-7006d7cb5c7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f54033a-b131-4b55-8656-fd1b146ac5fa",
            "compositeImage": {
                "id": "932fa39a-45fc-40eb-8c04-e8722275038f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9275b84-9183-47e4-aea9-7006d7cb5c7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82084b4d-3ed1-4717-a2d6-08c65b735442",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9275b84-9183-47e4-aea9-7006d7cb5c7a",
                    "LayerId": "f767463d-7e84-424c-8370-467fe9d2ba6b"
                }
            ]
        },
        {
            "id": "f172f6ed-3b9d-4646-8381-a504d82befd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2f54033a-b131-4b55-8656-fd1b146ac5fa",
            "compositeImage": {
                "id": "153c0005-ccd1-4a7b-8010-932568223347",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f172f6ed-3b9d-4646-8381-a504d82befd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b4d1413-01d6-4e4d-a5eb-68a64c95f919",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f172f6ed-3b9d-4646-8381-a504d82befd9",
                    "LayerId": "f767463d-7e84-424c-8370-467fe9d2ba6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "f767463d-7e84-424c-8370-467fe9d2ba6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2f54033a-b131-4b55-8656-fd1b146ac5fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}