{
    "id": "e1034b97-c3db-44df-959e-59c69eefe709",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "gobrit2_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 17,
    "bbox_right": 51,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "faa8bb7d-2459-47bf-817e-42106a791ac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1034b97-c3db-44df-959e-59c69eefe709",
            "compositeImage": {
                "id": "7cc23179-4a6d-4aa9-acde-10a21e13c14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "faa8bb7d-2459-47bf-817e-42106a791ac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5f49d6f-e063-45f5-873d-f00d11362843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "faa8bb7d-2459-47bf-817e-42106a791ac3",
                    "LayerId": "009baa6f-3091-42d3-8e94-64419dcd9d2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "009baa6f-3091-42d3-8e94-64419dcd9d2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1034b97-c3db-44df-959e-59c69eefe709",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 55
}