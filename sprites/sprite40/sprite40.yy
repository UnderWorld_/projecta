{
    "id": "10330c79-5e56-41a2-9702-4e48558a2203",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite40",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "70735512-42ef-4a6f-9995-c9fb6136a2bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10330c79-5e56-41a2-9702-4e48558a2203",
            "compositeImage": {
                "id": "fbe50882-432c-4dfe-ae54-c6106d11b876",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70735512-42ef-4a6f-9995-c9fb6136a2bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ded671b4-557a-4fd7-9010-35a30e603ea9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70735512-42ef-4a6f-9995-c9fb6136a2bc",
                    "LayerId": "a410f1e1-65cd-4999-959a-1ae1c4cb971e"
                }
            ]
        }
    ],
    "gridX": 350,
    "gridY": 350,
    "height": 768,
    "layers": [
        {
            "id": "a410f1e1-65cd-4999-959a-1ae1c4cb971e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10330c79-5e56-41a2-9702-4e48558a2203",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}