{
    "id": "7c0c9f9a-75b3-4fb3-a8d8-f526f3a2e38b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "beard4_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 5,
    "bbox_right": 54,
    "bbox_top": 35,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "62f8969a-6c6a-4b16-82f0-41bc81c73efa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c0c9f9a-75b3-4fb3-a8d8-f526f3a2e38b",
            "compositeImage": {
                "id": "47f6627f-44be-4129-96cf-debb7761469f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62f8969a-6c6a-4b16-82f0-41bc81c73efa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80dee37a-4e69-4534-97e0-cdbb1701b27e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62f8969a-6c6a-4b16-82f0-41bc81c73efa",
                    "LayerId": "dc6ac17b-f8e1-4c48-a9a7-2910c9421256"
                }
            ]
        },
        {
            "id": "ea6fd769-7fe2-4e65-a2e3-69b38a3c5f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c0c9f9a-75b3-4fb3-a8d8-f526f3a2e38b",
            "compositeImage": {
                "id": "addca943-47b0-4f6a-9e81-83e45e132c79",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6fd769-7fe2-4e65-a2e3-69b38a3c5f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "919608d2-6475-4463-a016-2093acb0e589",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6fd769-7fe2-4e65-a2e3-69b38a3c5f5f",
                    "LayerId": "dc6ac17b-f8e1-4c48-a9a7-2910c9421256"
                }
            ]
        },
        {
            "id": "65a92ec2-8887-41a2-970c-b2e65d46139a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c0c9f9a-75b3-4fb3-a8d8-f526f3a2e38b",
            "compositeImage": {
                "id": "dcdf79a6-bf66-4693-87f2-2b20202f63ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65a92ec2-8887-41a2-970c-b2e65d46139a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95faf746-be78-46ef-978e-8d796b894f1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65a92ec2-8887-41a2-970c-b2e65d46139a",
                    "LayerId": "dc6ac17b-f8e1-4c48-a9a7-2910c9421256"
                }
            ]
        },
        {
            "id": "8765611f-f931-4cfc-a0a2-056118bc3ea5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c0c9f9a-75b3-4fb3-a8d8-f526f3a2e38b",
            "compositeImage": {
                "id": "2198a68f-7609-4f85-8f3e-e097dffb4771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8765611f-f931-4cfc-a0a2-056118bc3ea5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d47025f-14cb-40d6-999c-4cb11ff08cbe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8765611f-f931-4cfc-a0a2-056118bc3ea5",
                    "LayerId": "dc6ac17b-f8e1-4c48-a9a7-2910c9421256"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 85,
    "layers": [
        {
            "id": "dc6ac17b-f8e1-4c48-a9a7-2910c9421256",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c0c9f9a-75b3-4fb3-a8d8-f526f3a2e38b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 42
}