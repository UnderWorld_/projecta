{
    "id": "a8cf189b-196b-4f45-81ea-2a4d2cd54506",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "mushroom6_spr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8c3055f-f27e-4221-80c2-ece79649262f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8cf189b-196b-4f45-81ea-2a4d2cd54506",
            "compositeImage": {
                "id": "30ff8752-effe-462b-a789-92b77bce6a8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8c3055f-f27e-4221-80c2-ece79649262f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26aba3bf-6d75-412e-b394-8c2156fee2f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8c3055f-f27e-4221-80c2-ece79649262f",
                    "LayerId": "e6fe0669-4bde-4163-a609-78c5148922e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "e6fe0669-4bde-4163-a609-78c5148922e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8cf189b-196b-4f45-81ea-2a4d2cd54506",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 13
}