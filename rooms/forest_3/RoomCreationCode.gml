  window_set_fullscreen(true);
audio_stop_sound(menu_ambient_sound);
audio_stop_sound(dungeon_ambient_sound);

/*



var size = room_width*room_height;
var terrain = size*0.004;
repeat(terrain){
 var tx = random(room_width)
 var ty = random(room_height)
 var strength = perlin_noise(tx,ty, 40, 1)
 //strength = strength*(strength/255)
 if(random(255)<strength){
	 switch(round(random(1))){
		 case 0: 

		 break;
		 case 1: 

		 break;
//		 case 2: 
//		  instance_create_depth(tx, ty, 0, ground3_obj)
//		 break;
//		 case 3: 
//		  instance_create_depth(tx, ty, 0, ground4_obj)
//		 break;



	 }
 }
	
 
 
}



var size = room_width*room_height;
var water = size*0.0003;
repeat(water){
 var tx = random(room_width)
 var ty = random(room_height)
 var strength = perlin_noise(tx,ty, 41, 0.1)
 //strength = strength*(strength/255)
 if(random(255)<strength){
	 switch(round(random(1))){
		 case 0: 
		 instance_create_depth(tx, ty, 0, water_obj)
		 break;
		 case 1: 
		 instance_create_depth(tx, ty, 0, water_obj)
		 break;

//		 case 2: 
//		  instance_create_depth(tx, ty, 0, ground3_obj)
//		 break;
//		 case 3: 
//		  instance_create_depth(tx, ty, 0, ground4_obj)
//		 break;



	 }
 }
	
 
 
}
*/
randomize();


//var biome = irandom_range(0,1);
var biome = 0;






//if(biome == 0)
//{   //forest

//var size = room_width*room_height;
//var trees = size*0.0004;
//repeat(trees)
// {
// var tx = random(room_width)
// var ty = random(room_height)
// var strength = perlin_noise(tx,ty, 40, 3)
// ////strength = strength*(strength/255)
// if(random(225)<strength){
//	 switch(round(random(4)))
//	 {
//		 case 0: 
//		 instance_create_depth(tx, ty, 0, v_tree2_obj)
//		 break;
//		 case 1: 
//		  instance_create_depth(tx, ty, 0, v_tree1_obj)
//		 break;
//		 case 2: 
//		 instance_create_depth(tx, ty, 0, v_tree2_obj)
//		 break;
//		 case 3: 
//		  instance_create_depth(tx, ty, 0, v_tree1_obj)
//		 break;
//		 case 4: 
//		  instance_create_depth(tx, ty, 0, v_tree2_obj)
//		break;
	
//	 }
// }
//}

var lay_id = layer_get_id("Tiles_1");
var map_id = layer_tilemap_get_id(lay_id);
// show_message("height: "+string(room_height/tile_size))
var tile_size = 48;
for (var ty = 0; ty < room_height/tile_size; ty += 1)
{
  for (var tx = 0; tx < room_width/tile_size; tx += 1)
  {
    var tile = tilemap_get(map_id, tx, ty);
    
    var px = (tx + 0.5) * tile_size;
    var py = (ty + 0.5) * tile_size;
    // show_message("px: "+string(px)+" tx: "+string(tx));
    var strength = perlin_noise(px, py, 40, 3);
    // show_message("strength: "+string(strength))
    var tileID = 0;
	var tileID = irandom_range(1,4)
    tile = tile_set_index(tile, tileID);
    tilemap_set(map_id, tile, tx, ty);
  }
}



/*var size = room_width*room_height;
var flowers = size*0.001;
repeat(flowers){
 var tx = random(room_width)
 var ty = random(room_height)
 var strength = perlin_noise(tx,ty, 40, 3)
 //strength = strength*(strength/255)
 if(random(255)<strength){
	 switch(round(random(2))){
		 case 0: 
		 instance_create_depth(tx, ty, 0, flower2_obj)
		 break;
		 case 1: 
		  instance_create_depth(tx, ty, 0, flower1_obj)
		 break;
		 case 2: 
		  instance_create_depth(tx, ty, 0, flower3_obj)
		 break;



	 }
 }
}
*/
//}


if(biome == 1){  //dark_forest
	

	
	var lay_id = layer_get_id("Tiles_1");
var map_id = layer_tilemap_get_id(lay_id);
// show_message("height: "+string(room_height/tile_size))
var tile_size = 48;
for (var ty = 0; ty < room_height/tile_size; ty += 1)
{
  for (var tx = 0; tx < room_width/tile_size; tx += 1)
  {
    var tile = tilemap_get(map_id, tx, ty);
    
    var px = (tx + 0.5) * tile_size;
    var py = (ty + 0.5) * tile_size;
    // show_message("px: "+string(px)+" tx: "+string(tx));
    var strength = perlin_noise(px, py, 40, 3);
    // show_message("strength: "+string(strength))
    var tileID = 0;
    if (strength > 0)
    {
      tileID = 2
    }
    
    tile = tile_set_index(tile, tileID);
    tilemap_set(map_id, tile, tx, ty);
  }
}


}