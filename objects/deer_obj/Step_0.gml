///This is run from the creature object step event
//It will set behavior and mood, and the target IDs (not the actual x, y coordinates which are reset in alarm 0)
//Speed is set in script set speed
//The only exception is the wandering/returning, which are set in alarm0

//First reset mood
var Health_Percent = Health/Base_Health*100;
if Hunter == true {
    if Health_Percent >= 100 {Creature_Mood = "Normal"}
    if Hunger > 0 {Creature_Mood = "Aggressive"}//Dangerous if hungry
    if Health_Percent < 75 {Creature_Mood = "Cautious"}//become defensive if the prey fights back
    }
    else {//Non predetors react differently
        if Health_Percent >= 100 {Creature_Mood = "Normal"}
        if Health_Percent < 80 {Creature_Mood = "Aggressive"}//Get mad if attacked - this should reset after a while... (new alarm?)
        if Health_Percent < 70 {Creature_Mood = "Cautious"}
        if Health_Percent < 40 {Creature_Mood = "Frightened"}
        }
//Next bypass anything that will reset itself in the alarm
if Behavior_Mode == "Eating" {return false}
if Behavior_Mode == "Dying" {return false}
if Behavior_Mode == "Attacking" {
    if irandom(100) > Health_Percent && Creature_Target.Behavior_Mode == "Attacking" {
        alarm[0] = 1;
        Behavior_Mode = "Fleeing";
        return true;
        }
    else {return false}
    }

//Now evaluate all other behaviors:
var Reset_Movement_Delay = -1;
var Closest_Player =  instance_nearest(x,y,OBJ_Player);
var Player_Distance = distance_to_object(OBJ_Player);//Will add in a player stealth factor in the future
//Will crash if all players die
x += Pursuit_Dist;//This is a total hack to make it ignore itself!
var Closest_Creature = instance_nearest(x-Pursuit_Dist,y,OBJ_Creatures);
x -= Pursuit_Dist;
var Creature_Distance = distance_to_object(Closest_Creature);
var Player_Sighted;//Can the creature see the player
if !collision_line(x,y,Closest_Player.x,Closest_Player.y,OBJ_Rock,false,true) && Player_Distance < Pursuit_Dist {Player_Sighted = true}
    else {Player_Sighted = false}
var Creature_Sighted;//can the creature see another creature
if !collision_line(x,y,Closest_Creature.x,Closest_Creature.y,OBJ_Rock,false,true) && Creature_Distance < Pursuit_Dist {Creature_Sighted = true}
    else {Creature_Sighted = false}
var Current_Level = Entity_Level;

switch Creature_Mood {//Mood adjusts pursuit level
    case "Cautious": Current_Level -= 1; break;
    case "Frightened": Current_Level = 0; break;
    case "Aggressive": Current_Level += 1; break;
    }  

if Behavior_Mode != "Fleeing" {
    if Creature_Sighted == true && Closest_Creature.Behavior_Mode == "Pursuing" {//Change to account for being attacked
        if Current_Level < Closest_Creature.Entity_Level {//Other creature is bigger
            Creature_Target = -1;//Get rid of other goal
            Behavior_Mode = "Fleeing";
            Creature_Threat = Closest_Creature;
            Reset_Movement_Delay = 25 + irandom(35);//Need to reset target with a slight delay
            }
            else {//If other creature is at the same level and attacking them, then fight back
                if Closest_Creature.Creature_Target = id {
                    Behavior_Mode = "Pursuing";
                    Creature_Target = Closest_Creature;
                    Reset_Movement_Delay = 20;
                    }
                }
        }
    }
switch Behavior_Mode {//Determine need to reset based on old mode
    case "Wandering"://Then evaluate targets 
    case "Returning":
        if Hunter == true or Creature_Mood == "Aggressive" {//Check if they should be chasing someone and reset
        if Player_Sighted == true && Player_Distance < Creature_Distance && Hunger > 0 && Current_Level > Closest_Player.Entity_Level {
            Creature_Target = Closest_Player;
            Behavior_Mode = "Pursuing"; 
            Reset_Movement_Delay = 10;
            }
        if Creature_Sighted == true && Current_Level > Closest_Creature.Entity_Level && Hunger > 0 {
            Creature_Target = Closest_Creature;
            Behavior_Mode = "Pursuing";
            Reset_Movement_Delay = 10;
            }
        }//End of predators or aggressive creatures
    }   
if Behavior_Mode == "Pursuing" or Behavior_Mode == "Attacking"  {//If they are already pursuing, check for distance to attacking
    if Player_Sighted == true && Player_Distance < Creature_Distance && Hunger > 0 && Current_Level > Closest_Player.Entity_Level {
        if Player_Distance < g.Melee_Dist {
            Behavior_Mode = "Attacking";
            if !alarm[1] {alarm[1] = 5}
            }
            else {
                Creature_Target = Closest_Player;
                Behavior_Mode = "Pursuing";
                }
        }
    if Creature_Sighted == true && Closest_Creature.Entity_Level < Current_Level && Hunger > 0 {
        if Creature_Distance < g.Melee_Dist {
            Behavior_Mode = "Attacking";
            if !alarm[1] {alarm[1] = 5}
            }
            else {
                Creature_Target = Closest_Creature;
                Behavior_Mode = "Pursuing";
                }
        }
    }
    
if Behavior_Mode == "Pursuing" && point_distance(Home_X, Home_Y, x, y) > 2*Pursuit_Dist {
    if irandom(100) > Creature_Relo && place_empty(x,y) {
        Home_X = x;
        Home_Y = y;
        }
        else {
            Behavior_Mode = "Returning";
            Goal_X = Home_X;
            Goal_Y = Home_Y;
            }
    }

if Health <=0 {Behavior_Mode = "Dying"}        

//None of the above apply
switch Behavior_Mode { //This is the switch from a combat to peaceful mode
    case "Fleeing":
    if Creature_Sighted == false { 
        Creature_Target = -1;//No goal
        Behavior_Mode = "Wandering";
        Creature_Threat = -1;
        }
    break;
    case "Pursuing":
    case "Attacking":
    if Player_Sighted == false && Creature_Sighted == false {//Targets all out of range
        Creature_Target = -1;//No goal
        Behavior_Mode = "Wandering";
        Creature_Threat = -1;
        }
    break;
    case "Returning" :
    if point_distance(Home_X, Home_Y, x, y) < (Pursuit_Dist/2) {Behavior_Mode = "Wandering"}
    Goal_X = Home_X;
    Goal_Y = Home_Y;
    break;
    } 

if Reset_Movement_Delay > 0 {alarm[0] = Reset_Movement_Delay}