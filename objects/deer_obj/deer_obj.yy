{
    "id": "950e9249-0f53-4c5f-9382-6213e5fae44b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "deer_obj",
    "eventList": [
        {
            "id": "4cf03d5d-977e-4cde-9f57-6ecef6bc06b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "950e9249-0f53-4c5f-9382-6213e5fae44b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27bcb68a-e9af-4286-a748-a7bbe2961dd6",
    "visible": true
}