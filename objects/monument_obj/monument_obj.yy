{
    "id": "ad126886-9f13-4d43-8bf5-c9201c704f5f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "monument_obj",
    "eventList": [
        {
            "id": "22593a29-d566-4987-8535-10e182cfacaa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ad126886-9f13-4d43-8bf5-c9201c704f5f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd77a1e6-041d-4a19-acfb-18ca2ac5de26",
    "visible": true
}