{
    "id": "e2767f4b-001b-41b4-8649-8b0c67147eee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gobrit_obj",
    "eventList": [
        {
            "id": "719c2094-1403-4cc9-85c9-d1b2096ac6f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e2767f4b-001b-41b4-8649-8b0c67147eee"
        },
        {
            "id": "77007029-96b3-43f6-a455-e33b948cc679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e2767f4b-001b-41b4-8649-8b0c67147eee"
        },
        {
            "id": "5d1bc42f-cf54-4ada-82e2-c1fcb68dd3c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e2767f4b-001b-41b4-8649-8b0c67147eee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1034b97-c3db-44df-959e-59c69eefe709",
    "visible": true
}