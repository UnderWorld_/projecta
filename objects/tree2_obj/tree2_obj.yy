{
    "id": "3a8eb937-ccda-48ac-a118-23843cf21796",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tree2_obj",
    "eventList": [
        {
            "id": "c637e95d-6fb8-43f6-ab71-e0a9bd90a02d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a8eb937-ccda-48ac-a118-23843cf21796"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9cdb2402-6f1d-44a3-bd58-a8b2c6e49f87",
    "visible": true
}