{
    "id": "6f49eeeb-5f43-46ac-84dc-34ad619fcd92",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_rock1_obj",
    "eventList": [
        {
            "id": "a653be77-8438-4d25-818b-658820a8224d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6f49eeeb-5f43-46ac-84dc-34ad619fcd92"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a1d24423-2bf7-4105-8b89-cc070412b44a",
    "visible": true
}