{
    "id": "54d9de2e-aaae-4c9f-800d-29a7fe20958e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_tree1_obj",
    "eventList": [
        {
            "id": "542a791e-21ef-4981-b3d8-aefdc6f8ee1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "54d9de2e-aaae-4c9f-800d-29a7fe20958e"
        },
        {
            "id": "ec19fec3-fc84-4f86-89e3-4605df806da6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "54d9de2e-aaae-4c9f-800d-29a7fe20958e"
        },
        {
            "id": "bbfd6364-eab0-49ca-bf79-b70ad9f6a709",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "54d9de2e-aaae-4c9f-800d-29a7fe20958e"
        },
        {
            "id": "898311ca-d191-4aeb-94f1-43f0b2f94402",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "54d9de2e-aaae-4c9f-800d-29a7fe20958e"
        },
        {
            "id": "ad8ed1fc-7600-4f54-890e-30240111d980",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "54d9de2e-aaae-4c9f-800d-29a7fe20958e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ad4d5998-766d-427a-b735-b61dce25aa3d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "27e1f300-1f0f-4f95-a115-330ac5dac0df",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "f42a2c4f-337b-4765-a2ec-5f5adceab72a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "ce2c4f91-5ccb-4e2a-9de4-c5180ffe1085",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d95c9471-fc37-46f0-ada4-9c88d3a27d28",
    "visible": true
}