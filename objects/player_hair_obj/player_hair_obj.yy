{
    "id": "6e13215d-953d-4c92-819c-39caa61f51f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player_hair_obj",
    "eventList": [
        {
            "id": "f3261fa5-b618-4503-9f8a-1b70f8893cc4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e13215d-953d-4c92-819c-39caa61f51f1"
        },
        {
            "id": "528e7ff4-046b-4ae1-9c98-6164c633bfe9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e13215d-953d-4c92-819c-39caa61f51f1"
        },
        {
            "id": "fc92d16b-0864-4b59-a24d-9d8241b0279a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6e13215d-953d-4c92-819c-39caa61f51f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fa96e6b6-fbdb-44e5-81e4-a00372a20ac1",
    "visible": true
}