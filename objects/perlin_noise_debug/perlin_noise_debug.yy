{
    "id": "79f97501-3bdc-49d9-92d1-170dd6f4a374",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "perlin_noise_debug",
    "eventList": [
        {
            "id": "4164e4f2-7358-4873-aa52-d6ae8f10f8de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "79f97501-3bdc-49d9-92d1-170dd6f4a374"
        },
        {
            "id": "75ffa3de-155d-4289-91fe-fc80204cacc5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "79f97501-3bdc-49d9-92d1-170dd6f4a374"
        },
        {
            "id": "e6be72cb-5f04-4c49-a2e6-d9c413690ea5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "79f97501-3bdc-49d9-92d1-170dd6f4a374"
        },
        {
            "id": "57115e9f-cfc4-40c8-81db-bb2484af3b99",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "79f97501-3bdc-49d9-92d1-170dd6f4a374"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}