var jump = 10;
draw_set_color(c_white)
var tx;
for (tx = 0; tx < room_width; tx += jump)
{
  var ty
  for (ty = 0; ty < room_height; ty += jump)
  {
	var strength = perlin_noise(tx,ty,octane,persistance)
	draw_set_alpha(strength/255)
    draw_rectangle(tx, ty, tx+jump, ty+jump, false);
	
  }
}
