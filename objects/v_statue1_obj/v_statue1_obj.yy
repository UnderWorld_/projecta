{
    "id": "64b0ea5f-960e-40a0-9691-743c610abc50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_statue1_obj",
    "eventList": [
        {
            "id": "41368e20-4e65-4df4-8dd3-573adbd1a420",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "64b0ea5f-960e-40a0-9691-743c610abc50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6d7dccaf-899d-44db-9b9d-9681cc276783",
    "visible": true
}