var dir, knock;

knock = 30;//knockback distance

dir = point_direction(x, y, other.x, other.y) + 180 mod 360;//gets the direction from your location and turns it the opposite direction
//"mod 360" wraps the code to 360 so if the value is 361, it changes it to 1. It's not necessary as most of the functions will accept larger values but it is typically a good idea to do this anyway.

//move the object the distance and direction specified
x += lengthdir_x(knock, dir);
y += lengthdir_y(knock, dir);
hit = true;
hp -= 3;
audio_play_sound(hit_f_1,7,false);
var change = choose(0,1,2,3,4);
		  switch(change)
		  {			  
			  case 0: audio_play_sound(e_hit1_sound,7,false);break;
			  case 1: audio_play_sound(e_hit2_sound,7,false);break;
			  case 2: audio_play_sound(e_hit3_sound,7,false);break;
			  case 3: audio_play_sound(e_hit4_sound,7,false);break;
			  case 4: audio_play_sound(e_hit5_sound,7,false);break;
			  //case 3: audio_play_sound(attack4_sound,7,false); 

		  }
		  
		  part_particles_create(global.partSystem, x, y, global.ptBasic, choose(3,5,7,9,11));