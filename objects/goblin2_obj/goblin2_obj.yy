{
    "id": "f94eac70-5ce3-4862-bff2-24a72417ac62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "goblin2_obj",
    "eventList": [
        {
            "id": "d8037866-14cc-43b1-b3ff-6a81e8cd99a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        },
        {
            "id": "dd2b4b15-cac0-4b73-b933-b63ca8479f64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        },
        {
            "id": "3270ef2a-05bb-4fd3-b6cc-d06357f230f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        },
        {
            "id": "599739ae-92d4-48c2-94a0-9fb3b1e237cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1a4ea01a-2c24-4e6e-8bba-87fa5d84454b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        },
        {
            "id": "0f7e1a57-8dc4-450d-a4bf-e23763b9d8cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "530f5969-822e-47d7-899e-d29e69ccb778",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        },
        {
            "id": "d902699f-4290-4057-a04d-687911613df4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        },
        {
            "id": "1552b375-e446-4db9-b41b-e26646f8662c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f94eac70-5ce3-4862-bff2-24a72417ac62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d491b1f8-a1ca-4a6b-9210-023063f3a8d3",
    "visible": true
}