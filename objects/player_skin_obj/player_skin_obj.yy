{
    "id": "8e22a76d-bbb7-463b-b372-84cd22846eb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player_skin_obj",
    "eventList": [
        {
            "id": "d510dc19-a3c4-496f-afda-6e60c8d810ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "56278d8b-2aba-41ba-aa1a-86b503da1e49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "98ca8fa2-2b57-45c2-867f-2eac41daee28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "8ad09958-51a6-4a5e-a4e3-bbc0b0f64fac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "5c76ecb6-0078-4484-b280-b52ab2da258a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "a96a129b-7dc5-4767-b035-1b0fd3e972d0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 9,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "93a1b3bc-f3d9-4930-b4d2-1336c9bff44c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "a4ccd390-57ba-4f3c-93d7-3313075edeb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "47997b8f-ba7b-49eb-ab33-00fb59236a02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "9141d7f9-c5ea-4a41-b8cd-672ca6921770",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "3b85e81a-a1a7-468f-937b-b40bc3241624",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        },
        {
            "id": "0fcd316b-6741-47f6-ae69-3f6ada9d6dea",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "8e22a76d-bbb7-463b-b372-84cd22846eb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
    "visible": true
}