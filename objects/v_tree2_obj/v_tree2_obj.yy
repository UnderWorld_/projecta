{
    "id": "62c161a0-117a-4b29-af63-89010876b97c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_tree2_obj",
    "eventList": [
        {
            "id": "3166c5e8-70c0-4e26-b532-43ac4da8c684",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "62c161a0-117a-4b29-af63-89010876b97c"
        },
        {
            "id": "ead3d611-9e86-4a5c-ae27-143ad908c25d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62c161a0-117a-4b29-af63-89010876b97c"
        },
        {
            "id": "fe4c31f8-2721-486e-b40c-f5a35717b84f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62c161a0-117a-4b29-af63-89010876b97c"
        },
        {
            "id": "d64b4c01-5baf-455f-9470-51d64673aec6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "62c161a0-117a-4b29-af63-89010876b97c"
        },
        {
            "id": "42857080-7844-4ebd-9e9b-2ce6f90f5d07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "62c161a0-117a-4b29-af63-89010876b97c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "41d3c5f2-71e8-4d71-962b-ff65b4fd6a9d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "75a7437a-69c0-4829-a784-0e5d8974cf77",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "555a59d2-3d93-46a8-a580-c22992314679",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "aacf9914-6673-4e4a-8c3a-a58809e211ce",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d95c9471-fc37-46f0-ada4-9c88d3a27d28",
    "visible": true
}