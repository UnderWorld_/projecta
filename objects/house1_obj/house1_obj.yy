{
    "id": "775b110e-40be-4cbd-a79c-a1121695c6fc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "house1_obj",
    "eventList": [
        {
            "id": "e6301cf5-78b1-4c33-8951-490d4c83b9e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "775b110e-40be-4cbd-a79c-a1121695c6fc"
        },
        {
            "id": "2758e790-c4a4-43eb-8373-8df4f2b38023",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "775b110e-40be-4cbd-a79c-a1121695c6fc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "11016ee0-de7a-4332-8e25-51e34a203bb2",
    "visible": true
}