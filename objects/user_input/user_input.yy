{
    "id": "9bd1e42d-25e4-4ef9-a5fd-2a5ae46084eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "user_input",
    "eventList": [
        {
            "id": "ce594c62-ab35-48dc-8561-69a4c0646922",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9bd1e42d-25e4-4ef9-a5fd-2a5ae46084eb"
        },
        {
            "id": "eb118d4c-6f9a-420b-a232-cc7c2528d5db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9bd1e42d-25e4-4ef9-a5fd-2a5ae46084eb"
        },
        {
            "id": "c768906c-8e1a-4628-b940-a0e74b20c9d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "9bd1e42d-25e4-4ef9-a5fd-2a5ae46084eb"
        },
        {
            "id": "48f81967-84f6-4acb-91bd-c01bd82176fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9bd1e42d-25e4-4ef9-a5fd-2a5ae46084eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}