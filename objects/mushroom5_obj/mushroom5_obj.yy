{
    "id": "d1437b6d-4d70-406d-a7b4-a504bad475c0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mushroom5_obj",
    "eventList": [
        {
            "id": "a2bb5c19-b0c0-4705-931a-9317b424e8ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1437b6d-4d70-406d-a7b4-a504bad475c0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bb9b28bc-723c-4cca-98ce-289d7ae3f49e",
    "visible": true
}