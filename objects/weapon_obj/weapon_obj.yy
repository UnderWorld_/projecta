{
    "id": "d0a83b66-1c4f-4ec0-8c46-9683d930b372",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "weapon_obj",
    "eventList": [
        {
            "id": "8a207eaf-c8e8-4d82-a1ea-997f1e52e54f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d0a83b66-1c4f-4ec0-8c46-9683d930b372"
        },
        {
            "id": "86d60b94-bc4b-4ddf-bcaf-99d102b3d736",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d0a83b66-1c4f-4ec0-8c46-9683d930b372"
        },
        {
            "id": "279a0f0b-5c61-4f17-bd78-261fe9ef528e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d0a83b66-1c4f-4ec0-8c46-9683d930b372"
        },
        {
            "id": "7bd43348-ce78-4b20-8d2c-55443efb2fee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d0a83b66-1c4f-4ec0-8c46-9683d930b372"
        },
        {
            "id": "3fd268e5-73a7-47c8-bde1-a5eb78e9d861",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d0a83b66-1c4f-4ec0-8c46-9683d930b372"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48210e7e-e9f7-41e3-a476-d469aac467b3",
    "visible": true
}