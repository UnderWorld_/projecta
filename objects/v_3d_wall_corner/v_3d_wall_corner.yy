{
    "id": "18c662d9-c320-4709-a6c6-64abad13cd65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_3d_wall_corner",
    "eventList": [
        {
            "id": "b5da7da8-5a99-45ff-8e43-29162d45239e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "18c662d9-c320-4709-a6c6-64abad13cd65"
        },
        {
            "id": "a0075ef6-a74b-47ab-b44a-7587c4bbb7aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "18c662d9-c320-4709-a6c6-64abad13cd65"
        },
        {
            "id": "1bc24796-526c-4c2a-a240-030d954cfa1e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "18c662d9-c320-4709-a6c6-64abad13cd65"
        },
        {
            "id": "d70b2a17-5b08-45ee-86a0-8b221fc83ca2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "18c662d9-c320-4709-a6c6-64abad13cd65"
        },
        {
            "id": "c712e522-4fe8-47ae-9d21-e9795e43a2e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "18c662d9-c320-4709-a6c6-64abad13cd65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "7c7798bc-ed17-4244-aedb-a67e3776b05c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "d565c98f-86a4-4de7-ba4c-1a29bfcd27ef",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "aa7963dd-d1bc-41a8-a0b4-e7a46142a322",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "12ed69d9-c822-4707-86d2-a23010ec7ec4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "189e01f8-be5a-4d0f-a816-928fe717a725",
    "visible": true
}