{
    "id": "6e752b1a-c46e-4f9a-b3b7-ff583ab04c3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_speech1",
    "eventList": [
        {
            "id": "0bd62673-a641-462f-a00b-087713e77bde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e752b1a-c46e-4f9a-b3b7-ff583ab04c3c"
        },
        {
            "id": "9c074163-bb48-47a4-ab29-2d28fa293d63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "6e752b1a-c46e-4f9a-b3b7-ff583ab04c3c"
        },
        {
            "id": "9a6a3f4e-2a0d-41be-8e9a-6c81740f7ad5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "6e752b1a-c46e-4f9a-b3b7-ff583ab04c3c"
        },
        {
            "id": "f20d99ba-634b-4e48-a102-15d957250cfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e752b1a-c46e-4f9a-b3b7-ff583ab04c3c"
        },
        {
            "id": "bc90ddc4-3890-45f7-a0c2-ba22e52de9c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6e752b1a-c46e-4f9a-b3b7-ff583ab04c3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}