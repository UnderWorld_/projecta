{
    "id": "57ebda33-c130-4d16-91f2-73aa3894815d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "campfire_obj",
    "eventList": [
        {
            "id": "4a94a8cf-5f67-4d2b-9484-c3e4531f299c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "57ebda33-c130-4d16-91f2-73aa3894815d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8fa0fc9e-d0c8-4009-9538-69457f0e2b4e",
    "visible": true
}