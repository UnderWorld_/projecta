{
    "id": "5f7e29b4-0c0d-485b-b6c6-6f9667a2dbe3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mushroom3_obj",
    "eventList": [
        {
            "id": "4b2787f7-f269-4113-9505-47c952ac3936",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5f7e29b4-0c0d-485b-b6c6-6f9667a2dbe3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fdd071d0-c914-4da2-857f-01213e537407",
    "visible": true
}