var target_x = player_obj.x;
var target_y = player_obj.y;
if (mouse_check_button(mb_right))
{
  target_x = mouse_x;
  target_y = mouse_y;
}
var target_dir = point_direction(x, y, target_x, target_y);
var target_dist = point_distance(x, y, target_x, target_y);
speed *= 0.75
motion_add(target_dir, target_dist*0.05);

var retract_dir = point_direction(x, y, player_obj.x, player_obj.y);
var retract_dist = point_distance(x, y, player_obj.x, player_obj.y);
motion_add(retract_dir, (retract_dist*retract_dist)*0.0005);

//this is cahnges the zoom based on scolling but you can set it how ever you like
zoom_level = clamp(zoom_level + (((mouse_wheel_down() - mouse_wheel_up())) * 0.2), 0.5, 0.75);

//Get current size
var view_w = camera_get_view_width(view_camera[0]);
var view_h = camera_get_view_height(view_camera[0]);

//Set the rate of interpolation
var rate = 0.2;

//Get new sizes by interpolating current and target zoomed size
var new_w = lerp(view_w, zoom_level * default_zoom_width, rate);
var new_h = lerp(view_h, zoom_level * default_zoom_height, rate);

//Apply the new size
camera_set_view_size(view_camera[0], new_w, new_h);

var vpos_x = camera_get_view_x(view_camera[0]);
var vpos_y = camera_get_view_y(view_camera[0]);

//change coordinates of camera so zoom is centered
var new_x = lerp(vpos_x,vpos_x+(view_w - zoom_level * default_zoom_width)/2, rate);
var new_y = lerp(vpos_y,vpos_y+(view_h - zoom_level * default_zoom_height)/2, rate);

//global cam angle
//camera_set_view_angle(view_camera[0],global.z)