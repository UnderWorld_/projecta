{
    "id": "680edd9a-931a-4ae6-9688-8a1806ef7bf9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shrub4_obj",
    "eventList": [
        {
            "id": "24bd1c5a-e795-4154-b75e-48b096ffe2a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "680edd9a-931a-4ae6-9688-8a1806ef7bf9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e6541b9-6af8-481d-ac8e-b680dbac3e31",
    "visible": true
}