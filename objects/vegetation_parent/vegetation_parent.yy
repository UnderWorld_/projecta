{
    "id": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vegetation_parent",
    "eventList": [
        {
            "id": "433fb24e-4798-4a0d-a9d8-dcfc9fd3446b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26"
        },
        {
            "id": "89cbb6c0-e2b4-4542-a0b3-a61bf8edcccb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26"
        },
        {
            "id": "9baed9f2-ee62-4dbc-871a-fe1571eae443",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "27407fa5-8982-476b-8165-8725b4fe7568",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}