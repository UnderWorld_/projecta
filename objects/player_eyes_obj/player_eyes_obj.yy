{
    "id": "c40b05c2-eaa2-44d3-ac9e-839e4d94b82b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player_eyes_obj",
    "eventList": [
        {
            "id": "d3e382d1-3078-4fc1-b915-5477cc62df1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c40b05c2-eaa2-44d3-ac9e-839e4d94b82b"
        },
        {
            "id": "c1867f8a-7e2d-4e1b-8dab-7175dfa8edb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c40b05c2-eaa2-44d3-ac9e-839e4d94b82b"
        },
        {
            "id": "a28135ff-ffb3-4c96-b152-9e8974fb9445",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c40b05c2-eaa2-44d3-ac9e-839e4d94b82b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "84d8a955-c4f7-41a0-940a-8b6c476444b0",
    "visible": true
}