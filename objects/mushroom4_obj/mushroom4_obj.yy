{
    "id": "3bb43d29-21ef-49a5-b5a1-49a43857abac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mushroom4_obj",
    "eventList": [
        {
            "id": "8f659bd4-21c6-4d39-a705-0c539e961060",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bb43d29-21ef-49a5-b5a1-49a43857abac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe8460db-8dd3-49c3-8f03-f5009112ab80",
    "visible": true
}