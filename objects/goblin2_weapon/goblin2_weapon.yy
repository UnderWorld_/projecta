{
    "id": "429c6d92-77d8-4917-9f74-273f7bc57840",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "goblin2_weapon",
    "eventList": [
        {
            "id": "3647e41d-a0d1-40b5-a10f-b3366d93c0bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "429c6d92-77d8-4917-9f74-273f7bc57840"
        },
        {
            "id": "1aaa0ede-92ef-4f1e-901a-fd08cf91f8e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "429c6d92-77d8-4917-9f74-273f7bc57840"
        },
        {
            "id": "594bf5fd-8a76-45be-83d9-14acaad63d03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "429c6d92-77d8-4917-9f74-273f7bc57840"
        },
        {
            "id": "60ec447e-2f3c-4fbc-b688-e62b260194c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "429c6d92-77d8-4917-9f74-273f7bc57840"
        },
        {
            "id": "f68baa17-8400-4657-a958-5bca675bc2bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "429c6d92-77d8-4917-9f74-273f7bc57840"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b619901-dc53-4777-ab05-534e36a5ac3a",
    "visible": true
}