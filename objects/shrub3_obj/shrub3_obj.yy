{
    "id": "792d2fa8-d9d3-47b2-91a2-c3b1dbe33216",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shrub3_obj",
    "eventList": [
        {
            "id": "113f260a-4633-4cbd-aa68-26d064f495e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "792d2fa8-d9d3-47b2-91a2-c3b1dbe33216"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f17b2d22-9432-4a69-ab02-980a762e4ad4",
    "visible": true
}