{
    "id": "077dd868-d794-4c7c-83b7-308487aae7de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "flower2_obj",
    "eventList": [
        {
            "id": "47d26eb1-5196-446e-984d-a9203ac64cc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "077dd868-d794-4c7c-83b7-308487aae7de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07d9dd56-78b2-48fb-b36a-3ffba6b9cd30",
    "visible": true
}