//write your messages in an array, starting at 0, like so
message[0] = "Eku was everything";
message[1] = "from which created the plains of existence ";
message[2] = "through flow's refined eku essence, the first being was created";
message[3] = "Ka";
message[4] = "eons passed";
message[5] = "Ka populated the plains of existence with land, water, vegetation ";
message[6] = "wasnt enough, Ka created the first sentient beings mirrored in his own spirit";
message[7] = "each with a different balance of Flow and disruption";
message_current = 0; //0 is the first number in our array, and the message we are currently at
message_end = 7; //6 is the last number in our array
message_draw = ""; //this is what we 'write' out. It's blank right now
increase = 0.25; //the speed at which new characters are added
characters = 0; //how many characters have already been drawn
hold = 0; //if we hold 'Z', the text will render faster

message_length = string_length(message[message_current]); //get the number of characters in the first message