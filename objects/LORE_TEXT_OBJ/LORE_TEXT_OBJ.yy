{
    "id": "5d315b74-c389-4a6c-9463-8c97c66ade85",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "LORE_TEXT_OBJ",
    "eventList": [
        {
            "id": "6cd4e50c-b199-4960-bb8f-978711fc2273",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5d315b74-c389-4a6c-9463-8c97c66ade85"
        },
        {
            "id": "ca2212b0-b09d-48fa-a254-a8d711eea69c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5d315b74-c389-4a6c-9463-8c97c66ade85"
        },
        {
            "id": "48f44fde-6740-42f9-8447-84e7bfebd9f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5d315b74-c389-4a6c-9463-8c97c66ade85"
        },
        {
            "id": "512edd17-282b-4044-b971-c9b50afd6d90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "5d315b74-c389-4a6c-9463-8c97c66ade85"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}