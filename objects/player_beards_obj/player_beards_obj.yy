{
    "id": "f063692a-78b2-47f2-912c-9d386cdfa240",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player_beards_obj",
    "eventList": [
        {
            "id": "5afd5650-6e65-44f8-99f2-7b41ce173bfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f063692a-78b2-47f2-912c-9d386cdfa240"
        },
        {
            "id": "85b40ad3-8bf9-488b-be67-c1208f358801",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f063692a-78b2-47f2-912c-9d386cdfa240"
        },
        {
            "id": "5fc296c9-c430-4b02-95b4-016586b1dcfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f063692a-78b2-47f2-912c-9d386cdfa240"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ca432737-ed24-4bfb-a784-fc11dd188942",
    "visible": true
}