{
    "id": "27407fa5-8982-476b-8165-8725b4fe7568",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ai_prey_parent",
    "eventList": [
        {
            "id": "79990bb5-f292-4565-8d21-e03b300be2f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "27407fa5-8982-476b-8165-8725b4fe7568"
        },
        {
            "id": "15470919-cf3c-494d-a243-55901bdbecd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "27407fa5-8982-476b-8165-8725b4fe7568"
        },
        {
            "id": "821c4172-9895-4c02-943e-986c38812b00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "27407fa5-8982-476b-8165-8725b4fe7568"
        },
        {
            "id": "c72ce653-1157-409e-ad72-5633cbc2bd86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "27407fa5-8982-476b-8165-8725b4fe7568"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27bcb68a-e9af-4286-a748-a7bbe2961dd6",
    "visible": true
}