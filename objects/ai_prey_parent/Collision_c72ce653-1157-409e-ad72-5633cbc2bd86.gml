var dir_away = point_direction(other.x, other.y, x, y);
while (position_meeting(x, y, other))
{
  x += lengthdir_x(1, dir_away);
  y += lengthdir_y(1, dir_away);
}