 //hunger -= 1*0.1
 
 
 if(state == states.idle){
      
	  
	  #region Idle
	  
	  counter += 1;
	  
	  
	  
	  spd = 2
	  if(counter >= room_speed * 2){
		  var change = choose(0,1);
		  switch(change){
			  case 0: state = states.wander;
			  case 1: counter = 0; break;
		  }
	  }
	  if(collision_circle(x,y, 64, player_obj, false, false)){
		  state = states.alert;
	  }
	  
	  if(hunger = hunger*0.5){
		
	   state = states.gather;
		  
	  }
	  
	   sprite_index = fox_idle_spr;
	  
	  
	  
	  
	  
	 #endregion
	 
	 
 }
 
 
 
  if(state == states.wander){
      
	  
	  #region Wander
	  

					my_dir += random_range(-20,20);
					x += lengthdir_x(spd, my_dir);
					y += lengthdir_y(spd, my_dir);
					counter = 0;
		  }
	  
	  //  if(hunger = hunger/2){
		
	//   state = states.gather;
	//	}
	  if(collision_circle(x,y, 64, player_obj, false, false)){
		  state = states.alert;
	  }

	  
	   sprite_index = fox_idle_spr;

	  
	  
	  
	  
	 #endregion
	 
	 
 
 
 
 
  if(state == states.alert){
      
	  
	  #region Alert
	  my_dir = point_direction(x,y,player_obj.x,player_obj.y)
      direction = my_dir+180
	  moveX = lengthdir_x(spd, direction);
	  moveY = lengthdir_y(spd, direction);
	  x += moveX;
	  y += moveY;
	  
	  if(collision_circle(x,y, 64, player_obj, false, false)){
		  state = states.flee;
	  }
 else {
		  state = states.wander;
	  }
	  
	  sprite_index = fox_idle_spr;
	  image_xscale = sign(moveX);
	  
	  
	 #endregion
	 
	 
 }
 
 
  if(state == states.flee){
      
	  
	  #region flee
	  
	  
	  
	  
	  
	  if distance_to_object(player_obj) < 128
{
	my_dir = point_direction(x,y,player_obj.x,player_obj.y)
    direction = my_dir+180
	  moveX = lengthdir_x(spd, direction);
	  moveY = lengthdir_y(spd, direction);
	  x += moveX;
	  y += moveY;
	  
	   sprite_index = fox_idle_spr;
    
} else {

   state = states.wander; //This is assuming you want the enemy to stop when the player isn't near
	  
	  
	  
	  
	  
	  

	  
	  
	 #endregion
	 
	
		
 }
 
 if(state == states.gather){
	 
	 
	 
	 #region Gather
	 
	 
	 
	 
	food = instance_nearest(vegetation_parent.x,vegetation_parent.y,vegetation_parent);
	move_towards_point(food.x,food.y,spd);

	 if(hunger >= hunger*0.8){
		 state = states.wander;
	 }
	 
	 
	 
	 #endregion Gather
	  sprite_index = fox_idle_spr;
	 
 }
 depth = -y}