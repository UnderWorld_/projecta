{
    "id": "e0bb2b72-c9da-44b3-a90a-dc1fbd52ce8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_3d_pillar",
    "eventList": [
        {
            "id": "b9076ceb-b726-4125-a4b6-9fffc79066d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "e0bb2b72-c9da-44b3-a90a-dc1fbd52ce8e"
        },
        {
            "id": "8a46fba2-8d9f-4578-9e49-d5fba1576a98",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0bb2b72-c9da-44b3-a90a-dc1fbd52ce8e"
        },
        {
            "id": "7b2ad780-d3ce-4679-8ea3-ed639c77acc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0bb2b72-c9da-44b3-a90a-dc1fbd52ce8e"
        },
        {
            "id": "1195b160-0c9c-46c7-83b2-3a8a4af4c15a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "e0bb2b72-c9da-44b3-a90a-dc1fbd52ce8e"
        },
        {
            "id": "86599578-91b4-40e3-862a-52da8a735f3a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "e0bb2b72-c9da-44b3-a90a-dc1fbd52ce8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "b6cb6889-9d17-42f7-9ad9-893718e58af4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "bfd85a23-0cf6-4dbb-8408-26fc90e82d9f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "75e8a12f-db11-40af-81d3-dbc7012c1557",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "255b6749-e454-4e13-83e1-4edfc873b40d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8a19ed8-811b-4832-8d51-77116ceec389",
    "visible": true
}