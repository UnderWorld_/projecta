{
    "id": "7f5acfa8-3da1-43c1-b507-6545af94f0cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "dungeon_door_obj",
    "eventList": [
        {
            "id": "862c4be4-78c4-487e-a90d-46700c06023e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7f5acfa8-3da1-43c1-b507-6545af94f0cd"
        },
        {
            "id": "cf73e2d2-5f9a-4fd9-9dcb-81d805e45687",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f5acfa8-3da1-43c1-b507-6545af94f0cd"
        },
        {
            "id": "28d9df03-b6f4-46e9-a3d1-00ae2af99c32",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7f5acfa8-3da1-43c1-b507-6545af94f0cd"
        },
        {
            "id": "fc1f27f6-9a02-4697-9dcf-fb8dfa0c7b44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7f5acfa8-3da1-43c1-b507-6545af94f0cd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5211161b-f889-4e65-a6be-ad2291e68c27",
    "visible": true
}