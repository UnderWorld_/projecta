{
    "id": "1a4ea01a-2c24-4e6e-8bba-87fa5d84454b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "dmg_collision_obj",
    "eventList": [
        {
            "id": "682d04c2-7781-4ffe-84b2-9d78f7c5e524",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f94eac70-5ce3-4862-bff2-24a72417ac62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "1a4ea01a-2c24-4e6e-8bba-87fa5d84454b"
        },
        {
            "id": "9f1efff7-c3dd-4f03-97dc-2fd1ed2c3a30",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1a4ea01a-2c24-4e6e-8bba-87fa5d84454b"
        },
        {
            "id": "9849b4f4-d2f4-42f9-9822-cbe3cfce9294",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "1a4ea01a-2c24-4e6e-8bba-87fa5d84454b"
        },
        {
            "id": "ae0266c3-8cb7-4373-ace1-dd84e341cbe8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1a4ea01a-2c24-4e6e-8bba-87fa5d84454b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "77dc3bb8-07e9-421a-9a5f-ac0e5de887fb",
    "visible": true
}