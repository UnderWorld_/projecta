{
    "id": "4f6c4696-ac4b-44ee-817c-0aab9d114101",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "creation_pet",
    "eventList": [
        {
            "id": "15dd512f-a3bb-4890-9b58-59df3460d3df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "55a961de-6cbe-459c-913f-5fc4de4c05b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "28ef1e50-8b0b-4a60-a9be-c5f7753b4884",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "6d841f7f-1547-4040-8d59-e5a75fa3de28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "97412915-069b-4c22-a2c3-a1a2aed3dfe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "73a0c6df-397c-4fe3-8a8a-6a225d63208b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "646dae5a-5e84-4fd7-9e75-00b735bae407",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "2faa1890-beaa-4587-965b-3fcb24b493f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "327c12c2-07c4-4646-9493-289c49dd250b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        },
        {
            "id": "a6d29ad8-05d3-4af0-a3e5-f273b7cbf2e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "4f6c4696-ac4b-44ee-817c-0aab9d114101"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3a2e7a4c-03e5-4cd0-8867-d04be1ca2069",
    "visible": true
}