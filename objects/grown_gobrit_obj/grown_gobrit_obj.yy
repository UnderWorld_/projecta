{
    "id": "d1780267-13d1-4e7b-ba82-4797f5bc62d6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "grown_gobrit_obj",
    "eventList": [
        {
            "id": "b18f4090-ed72-4079-b531-5ebb14626e1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d1780267-13d1-4e7b-ba82-4797f5bc62d6"
        },
        {
            "id": "7f330664-0f85-4fca-b6fd-3884ee6bdad3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d1780267-13d1-4e7b-ba82-4797f5bc62d6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6e22ebb-2ac7-4dd4-8f15-a6ca391f6862",
    "visible": true
}