{
    "id": "8a3024ac-9ffc-4bf3-8666-02a9c1f4084b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_speech",
    "eventList": [
        {
            "id": "5fd596c2-6bb2-4d06-9f60-60ddf13617ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a3024ac-9ffc-4bf3-8666-02a9c1f4084b"
        },
        {
            "id": "20406423-9191-4e56-b3e4-4ce13979d4d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8a3024ac-9ffc-4bf3-8666-02a9c1f4084b"
        },
        {
            "id": "231a7226-314f-49c5-baba-1dbca348691d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8a3024ac-9ffc-4bf3-8666-02a9c1f4084b"
        },
        {
            "id": "25dd467c-4d9e-4bb5-8953-c112dcb34713",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a3024ac-9ffc-4bf3-8666-02a9c1f4084b"
        },
        {
            "id": "c85a05b2-594e-49af-aee3-d94535fe4f4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "8a3024ac-9ffc-4bf3-8666-02a9c1f4084b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}