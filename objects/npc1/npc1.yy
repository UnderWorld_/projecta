{
    "id": "5098a0cc-1400-42e9-b220-15d6675d504a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "npc1",
    "eventList": [
        {
            "id": "49b69020-73de-4e6e-bf99-7f3fbf776ed9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5098a0cc-1400-42e9-b220-15d6675d504a"
        },
        {
            "id": "7b045d05-fb48-4e90-ac48-1140f0b6974c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5098a0cc-1400-42e9-b220-15d6675d504a"
        },
        {
            "id": "4cf8139e-5b8a-467b-a7b2-72e972e1e765",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5098a0cc-1400-42e9-b220-15d6675d504a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8c281671-f3d9-4c19-8f99-7e59bd041907",
    "visible": true
}