if (surface_exists(surf) == false)
{
  var _cw = room_width;
  var _ch = room_height;
  surf = surface_create(_cw, _ch);
  surface_set_target(surf);
  draw_set_colour(c_black);
  draw_set_alpha(0);
  draw_rectangle(0, 0, _cw, _cw, false);
  surface_reset_target();
}
else
{
  if (surface_exists(surf))
  {
    var _cw = room_width
    var _ch = room_height;
    var _cx = 0;
    var _cy = 0;
    surface_set_target(surf);
    draw_set_color(c_black);
    draw_set_alpha(1);
    draw_rectangle(0, 0, _cw, _ch, 0);
    gpu_set_blendmode(bm_subtract);
	
	
	
/*var lr = 100;
var lg = 0
var lb = 0;
draw_set_color(make_colour_rgb((lr / 100) * 255, (lg / 100) * 255, (lb / 100) * 255) );
*/
draw_set_color(c_white);
with(torch_obj)
{
draw_sprite(light_spr,0,x,y);
}
with(player_obj)
{
draw_sprite(light_spr,0,x,y);
}
    //with (light_obj)
    //{
    //draw_sprite_ext(light_spr, 0, player_obj.x,player_obj.y, 1, 1, 0, c_white, 0.65);  //Player FOV
  // draw_sprite_ext(light_spr, 0, torch_obj.x,torch_obj.y, 1, 1, 0,c_red, 0.80);  //Torch Light
   // break;
    //}
  }
  gpu_set_blendmode(bm_normal);
  draw_set_alpha(1);
  surface_reset_target();
  var strength = sin(6.28319*time/day_length);
  if (strength <= 0) 
  {
    strength = 0
  }
  draw_set_alpha(strength*darkness);
  //gpu_set_blendmode_ext( bm_, bm_inv_src_color);
  draw_surface(surf, _cx, _cy);
  var darkness_scope = darkness;
  with(torch_obj)
  {
 draw_sprite_ext(light_spr, 0,x, y, 1, 1, 0,c_red, strength*darkness_scope*0.25);  //Torch Light
  }
  /*with(player_obj)
  {
 draw_sprite_ext(light_spr, 0,x, y, 1, 1, 0,c_white, strength*darkness_scope*0.25);  //Torch Light
  }
  */
  //draw_sprite(light_spr,0,torch_obj.x,torch_obj.y);
  
}
