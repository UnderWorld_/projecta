{
    "id": "325d2aeb-d1af-4cd4-b9d6-8668d5ad59a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oLighting",
    "eventList": [
        {
            "id": "984e5bff-a72a-45eb-b5ed-c4931fcb077f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "325d2aeb-d1af-4cd4-b9d6-8668d5ad59a8"
        },
        {
            "id": "4cd9bd34-4413-4d24-93ae-a59dab6ee1fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "325d2aeb-d1af-4cd4-b9d6-8668d5ad59a8"
        },
        {
            "id": "bf1ea4ed-4a62-4601-934a-3fb281e032ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "325d2aeb-d1af-4cd4-b9d6-8668d5ad59a8"
        },
        {
            "id": "bc2435f1-670d-42a9-b413-5ee6260aa06b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "325d2aeb-d1af-4cd4-b9d6-8668d5ad59a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}