

menu_move = keyboard_check_pressed(vk_down) - keyboard_check_pressed(vk_up);

menu_index += menu_move;

if (menu_index < 0) menu_index = buttons - 1;
if (menu_index > buttons - 1) menu_index = 0;

var i = 0;
repeat(buttons) {
	if (unfold[i] == 1) i++;
	if (i < buttons) unfold[i] = min(1, unfold[i] + 0.02);
	if (i + 1 < buttons) unfold[i+1] = min(1, unfold[i+1] + 0.005);
}

if (menu_index != last_selected) {
	var change = choose(0,1,2,3,4,5,6,7,8);
		  switch(change)
		  {
			  case 0: audio_play_sound(menu_switch1,7,false); layer_background_blend(back_id, c_blue);
			  break;
			  case 1: audio_play_sound(menu_switch2,7,false); layer_background_blend(back_id, c_red);
			  break;
			  case 2: audio_play_sound(menu_switch3,7,false); layer_background_blend(back_id, c_green); 
			  break;
			  case 3: audio_play_sound(menu_switch1,7,false); layer_background_blend(back_id, c_purple);
			  break;
			  case 4: audio_play_sound(menu_switch2,7,false); layer_background_blend(back_id, c_orange);
			  break;
			  case 5: audio_play_sound(menu_switch3,7,false); layer_background_blend(back_id, c_aqua); 
			  break;
		      case 6: audio_play_sound(menu_switch1,7,false); layer_background_blend(back_id, c_fuchsia);
			  break;
			  case 7: audio_play_sound(menu_switch2,7,false); layer_background_blend(back_id, c_lime);
			  break;
			  case 8: audio_play_sound(menu_switch3,7,false); layer_background_blend(back_id, c_silver); 
			  break;
		  }
	
	
	part_particles_create(particle_effects, menu_x, menu_y + button_h/2 + (button_h + button_padding) * menu_index, box_flash, 1);
}

last_selected = menu_index;

