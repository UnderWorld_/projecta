
var i = 0;
draw_set_font(font4);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

repeat(buttons) {
	
	xx = menu_x;
	yy = menu_y + (button_h + button_padding) * i + 256 * (1-unfold[i]);
	draw_set_color(c_white);
	//draw_rectangle(xx - button_w/2, yy, xx - button_w/2 + button_w, yy + button_h, false);
	
	draw_set_color(c_black);
	
	if (menu_index == i) draw_set_color(c_black);
	
	draw_text(xx, yy + button_h/2, button[i]);
	
	i++;
	draw_text(150,1000,"Version : 0.041.1");
}