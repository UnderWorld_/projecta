{
    "id": "9d07f972-e679-4202-939c-6a1fe0c24b2b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "torch_obj",
    "eventList": [
        {
            "id": "5f5710f1-ab46-432d-8d8e-268941b717cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9d07f972-e679-4202-939c-6a1fe0c24b2b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0e5d0e9a-0b43-4fac-a1bd-090e113c1554",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "61186641-9c33-4b06-859b-6f83ba2af575",
    "visible": true
}