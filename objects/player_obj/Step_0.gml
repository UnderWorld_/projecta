// Inputs
right = keyboard_check(ord("D"));
left = keyboard_check(ord("A"));
up = keyboard_check(ord("W"));
down = keyboard_check(ord("S"));
attack = keyboard_check(vk_space);
close_game = keyboard_check(ord("R"));
var _Cam_R = keyboard_check(ord("Q"));
var _Cam_L = keyboard_check(ord("E"));
sprint = keyboard_check(vk_shift);

// Camera rotation.
global.z += (_Cam_R - _Cam_L)*2;


if(global.RESETCAMERA == 1)
{
camera_set_view_angle(view_camera[0], 0)
}
else
{
camera_set_view_angle(view_camera[0], global.z);
}



// match beard animations speed with player animation speed
//player_beards_obj.image_speed = player_obj.image_speed;

//var dirX, dirY, dir;

// Reset the direction
//dirX = 0;
//dirY = 0;

if(sprint)
{
	spd = 5;
}
else
{
	spd = 3;
}

if(close_game) 
	{
		game_end();
	}
	
	// Movement deltas.
var _DX = right - left;
var _DY = down - up;

if(_DX != 0 || _DY != 0){
    // Get direction.
    var _Dir = point_direction(0, 0, _DX, _DY);
    // Include camera angle.
    _Dir -= global.z;
    // Move player.
    x += lengthdir_x(spd, _Dir);
    y += lengthdir_y(spd, _Dir);
}
	
	


if(left)
{		
            	
			sprite_index = player_left_animation;
			player_beards_obj.sprite_index = beard_left_animation;	
			player_eyes_obj.sprite_index = eye_left_animation;
			player_eyes_obj.image_speed = spd*0.25;
			player_hair_obj.sprite_index = hair_left_animation;
			player_hair_obj.image_speed = spd*0.25;
			image_speed = spd*0.25;
			player_beards_obj.image_speed = spd*0.25;
			face_direction = 2;
			
	
}


if(right)
{		


			sprite_index = player_right_animation;
			player_beards_obj.sprite_index = beard_right_animation;		
			player_eyes_obj.sprite_index = eye_right_animation;
			player_eyes_obj.image_speed = spd*0.25;		
			player_hair_obj.sprite_index = hair_right_animation;
			player_hair_obj.image_speed = spd*0.25;
			image_speed = spd*0.25;
			face_direction = 0;
			player_beards_obj.image_speed = spd*0.25;
			

	
}

if(up)
{
			
			sprite_index = player_up_animation;
            player_beards_obj.sprite_index = beard_up_animation;
			player_eyes_obj.sprite_index = eye_up_animation;
			player_eyes_obj.image_speed = spd*0.25;		
			player_hair_obj.sprite_index = hair_left_animation;
			player_hair_obj.image_speed = spd*0.25;
			image_speed = spd*0.25;
			face_direction = 1;
			player_beards_obj.image_speed = spd*0.25;


}

if(down)
{			
		
			sprite_index = player_left_animation;
			player_beards_obj.sprite_index = beard_left_animation;
			player_eyes_obj.sprite_index = eye_left_animation;
			player_eyes_obj.image_speed = spd*0.25;	
            player_hair_obj.sprite_index = hair_left_animation;
			player_hair_obj.image_speed = spd*0.25;
			image_speed = spd*0.25;	
			face_direction = 3;
			player_beards_obj.image_speed = spd*0.25;
}

	//IDLE CODE
	if (x == xprevious && y == yprevious) {
		
	//if face_direction == 1 {
	//	sprite_index = player_idle_up_spr; 
	//	image_speed = 1;
	//}
	//else {
	sprite_index = player_idle_animation; 
	player_beards_obj.sprite_index = beard_idle_animation;
	player_eyes_obj.sprite_index = eye_idle_animation;
	player_eyes_obj.image_speed = spd*0.1;	
	player_hair_obj.sprite_index = hair_idle_animation;
	player_hair_obj.image_speed = spd*0.1;
	image_speed = spd*0.1;
	player_beards_obj.image_speed = spd*0.1;

	
}
//Attack animation

if(attack && weapon_cooldown < 1)
{
		var weapon = instance_create(x,y, weapon_obj);
		instance_create(x,y,dmg_collision_obj);
		//dmg_collision_obj.dir = face_direction*90-global.z;
	    //dmg_collision_obj.image_angle = (face_direction*90)-global.z;
		
		weapon.owner = id;
		weapon_cooldown += 30;
		//weapon.dir = mouse_angle;
		//weapon.dir = point_direction(x,y,mouse_x,mouse_y);
	   // weapon.image_angle = point_direction(x, y, mouse_x, mouse_y);;
		
		var change = choose(0,1,2);
		  switch(change)
		  {			  
			  case 0: audio_play_sound(attack1_sound,7,false);
			  case 1: audio_play_sound(attack2_sound,7,false);
			  case 2: audio_play_sound(attack3_sound,7,false);break;
			  //case 3: audio_play_sound(attack4_sound,7,false); 

		  }
		
}	


	//Weapon Cooldown
		weapon_cooldown -= 1;
	if(weapon_cooldown < 0)
	{ 
		weapon_cooldown = 0
	}
  	
	
	
	// //Idle/shadow Code
	//if (x == xprevious && y == yprevious) {
		
	////if face_direction == 1 {
	////	sprite_index = player_idle_up_spr; 
	////	image_speed = 1;
	////}
	////else {
	////sprite_index = player_idle_spr; 
	////image_speed = 1;
	////}
	

			//sprite_index = player_idle_animation;
			//image_speed = spd*0.1;
			
	
	
	


if (hp < 0)
{
room_goto(menu_rm);	
}





velocity[@ X] = clamp (velocity[@ X], -spd, spd);
velocity[@ Y] = clamp (velocity[@ Y], -spd, spd);

x += velocity[@ X];
y += velocity[@ Y];



