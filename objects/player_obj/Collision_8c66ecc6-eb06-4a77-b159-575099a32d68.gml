var dir, knock;

knock = 2;//knockback distance

dir = point_direction(x, y, other.x, other.y) + 180 mod 360;//gets the direction from your location and turns it the opposite direction
//"mod 360" wraps the code to 360 so if the value is 361, it changes it to 1. It's not necessary as most of the functions will accept larger values but it is typically a good idea to do this anyway.

//move the object the distance and direction specified
x += lengthdir_x(knock, dir);
y += lengthdir_y(knock, dir);