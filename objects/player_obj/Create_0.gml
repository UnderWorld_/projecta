//stats

hp = 50;
arrowcd_ = 2;



//Skill base values

melee_dmg = 0;
projectile_dmg = 0;
area_dmg = 0;
// attributes
strength = global.strength; 
Agility = global.agility;
Toughness = global.toughness;
Endurance = global.endurance;
Recuperation = global.recuperation;
Disease_Resistance = global.disease_r; 
Analytical_Ability = global.analytical_a;
Focus = global.focus;
Linguistic_Ability = global.linguistic_a; 
//exp values
base_experience = 0;
level = 0;
//Combat Skills
Armorer = 0;
Athletics = 0;
Blade = 0;
Block = 0;
Blunt = 0;
Hand_to_hand = 0;
Heavy_Armor = 0;
//Magic Skills
Alchemy = 0;
Alteration = 0;
Conjuration = 0;
Destruction = 0;
Illusion = 0;
Mysticism = 0;
Restoration = 0;
//Stealth Skills
Acrobatics = 0;
Light_armor = 0;
Marksman = 0;
Mercantile = 0;
Security = 0;
Sneak = 0;
Speechcraft = 0;

//
global.freeze = 0;

// shader outline

////outline thickness
//linethickness=4;

//upixelH = shader_get_uniform(shd_outline5,"pixelH");
//upixelW = shader_get_uniform(shd_outline5,"pixelW");
//texelW = linethickness*texture_get_texel_width(sprite_get_texture(sprite_index,0));
//texelH = linethickness*texture_get_texel_height(sprite_get_texture(sprite_index,0));

// Macro

face_direction = 0;
#macro X 0
#macro Y 1

// Inputs
right = keyboard_check(ord("D"));
left = keyboard_check(ord("A"));
up = keyboard_check(ord("W"));
down = keyboard_check(ord("S"));
attack = keyboard_check(vk_space);
close_game = keyboard_check(ord("R"));
// Movement
spd = 2.5;
fric = .1;
acc = 1;
can_shoot = 0
velocity = [0, 0];

instance_create(x,y,camera_obj);

instance_create(x,y, view_collision_obj);
instance_create(x,y,player_beards_obj);
instance_create(x,y, player_eyes_obj);
instance_create(x,y, player_hair_obj);
instance_create(x,y, pet_obj);
//Current Weapon
weapon_cooldown = 0;

global.RESETCAMERA = 0;
global.z = 0;
z = 0;
//z_motion = 0;
//z_gravity = 1;
//z_jump = 10;


//body

//sprite_index = player_idle_animation;

if(global.skin_color = 0)
{
		player_left_animation = skin1_left;
		player_right_animation = skin1_right;
		player_idle_animation = skin1_idle;
		player_up_animation = skin1_up;
		
}

if(global.skin_color = 1)
{
		player_left_animation = skin2_left;
		player_right_animation = skin2_right;
		player_idle_animation = skin2_idle;
		player_up_animation = skin2_up;
}

if(global.skin_color = 2)
{
		player_left_animation = skin3_left;
		player_right_animation = skin3_right;
		player_idle_animation = skin3_idle;
        player_up_animation = skin3_up;
}

if(global.skin_color = 3)
{
		player_left_animation = skin4_left;
		player_right_animation = skin4_right;
		player_idle_animation = skin4_idle;
		player_up_animation = skin4_up;
}

if(global.skin_color = 4)
{
		player_left_animation = skin5_left;
		player_right_animation = skin5_right;
		player_idle_animation = skin5_idle;
		player_up_animation = skin5_up;
}

if(global.skin_color = 5)
{
		player_left_animation = skin6_left;
		player_right_animation = skin6_right;
		player_idle_animation = skin6_idle;
		player_up_animation = skin6_up;
}

//beard
if(global.beard_type = 0)
{
		beard_left_animation = player_beardsempty;
		beard_right_animation = player_beardsempty;
		beard_idle_animation = player_beardsempty;
		beard_up_animation = player_beardsempty;
		
}

if(global.beard_type = 1)
{
		beard_left_animation = beardleft1;
		beard_right_animation = beardright1;
		beard_idle_animation = beard1_idle;
		beard_up_animation = player_beardsempty;
		
}

if(global.beard_type = 2)
{
		beard_left_animation = beardleft2;
		beard_right_animation = beardright2;
		beard_idle_animation = beard2_idle;
		beard_up_animation = player_beardsempty;
}

if(global.beard_type = 3)
{
		beard_left_animation = beardleft3;
		beard_right_animation = beardright3;
		beard_idle_animation = beard3_idle;
		beard_up_animation = player_beardsempty;
}

if(global.beard_type = 4)
{
		beard_left_animation = beardleft4;
		beard_right_animation = beardright4;
		beard_idle_animation = beard4_idle;
		beard_up_animation = player_beardsempty;
}

if(global.beard_type = 5)
{
		beard_left_animation = beardleft5;
		beard_right_animation = beardright5;
		beard_idle_animation = beard5_idle;
		beard_up_animation = player_beardsempty;
}



//eyes

if(global.eye_color = 0)
{
		eye_left_animation = eye_left1;
		eye_right_animation = eye_right1;
		eye_idle_animation = eye_idle1;
		eye_up_animation = player_beardsempty;
		
}

if(global.eye_color = 1)
{
		eye_left_animation = eye_left2;
		eye_right_animation = eye_right2;
		eye_idle_animation = eye_idle2;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 2)
{
		eye_left_animation = eye_left3;
		eye_right_animation = eye_right3;
		eye_idle_animation = eye_idle3;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 3)
{
		eye_left_animation = eye_left4;
		eye_right_animation = eye_right4;
		eye_idle_animation = eye_idle4;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 4)
{
		eye_left_animation = eye_left5;
		eye_right_animation = eye_right5;
		eye_idle_animation = eye_idle5;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 5)
{
		eye_left_animation = eye_left6;
		eye_right_animation = eye_right6;
		eye_idle_animation = eye_idle6;
		eye_up_animation = player_beardsempty;
		
}

if(global.eye_color = 6)
{
		eye_left_animation = eye_left7;
		eye_right_animation = eye_right7;
		eye_idle_animation = eye_idle7;
	eye_up_animation = player_beardsempty;
}

if(global.eye_color = 7)
{
		eye_left_animation = eye_left8;
		eye_right_animation = eye_right8;
		eye_idle_animation = eye_idle8;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 8)
{
		eye_left_animation = eye_left9;
		eye_right_animation = eye_right9;
		eye_idle_animation = eye_idle9;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 9)
{
		eye_left_animation = eye_left10;
		eye_right_animation = eye_right10;
		eye_idle_animation = eye_idle10;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 10)
{
		eye_left_animation = eye_left11;
		eye_right_animation = eye_right11;
		eye_idle_animation = eye_idle11;
		eye_up_animation = player_beardsempty;
		
}

if(global.eye_color = 11)
{
		eye_left_animation = eye_left12;
		eye_right_animation = eye_right12;
		eye_idle_animation = eye_idle12;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 12)
{
		eye_left_animation = eye_left13;
		eye_right_animation = eye_right13;
		eye_idle_animation = eye_idle13;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 13)
{
		eye_left_animation = eye_left14;
		eye_right_animation = eye_right14;
		eye_idle_animation = eye_idle14;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 14)
{
		eye_left_animation = eye_left15;
		eye_right_animation = eye_right15;
		eye_idle_animation = eye_idle15;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 15)
{
		eye_left_animation = eye_left16;
		eye_right_animation = eye_right16;
    	eye_idle_animation = eye_idle16;
		eye_up_animation = player_beardsempty;
		
}

if(global.eye_color = 16)
{
		eye_left_animation = eye_left17;
		eye_right_animation = eye_right17;
		eye_idle_animation = eye_idle17;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 17)
{
		eye_left_animation = eye_left18;
		eye_right_animation = eye_right18;
		eye_idle_animation = eye_idle18;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 18)
{
		eye_left_animation = eye_left19;
		eye_right_animation = eye_right19;
		eye_idle_animation = eye_idle19;
		eye_up_animation = player_beardsempty;
}

if(global.eye_color = 19)
{
		eye_left_animation = eye_left20;
		eye_right_animation = eye_right20;
		eye_idle_animation = eye_idle20;
	eye_up_animation = player_beardsempty;
}

if(global.eye_color = 20)
{
		eye_left_animation = eye_left21;
		eye_right_animation = eye_right21;
		eye_idle_animation = eye_idle21;
		eye_up_animation = player_beardsempty;
}
if(global.eye_color = 21)
{
		eye_left_animation = eye_left22;
		eye_right_animation = eye_right22;
		eye_idle_animation = eye_idle22;
		eye_up_animation = player_beardsempty;
}



// HAIR :3



if(global.hair_type = 0)
{
		hair_left_animation = hair1_left;
		hair_right_animation = hair1_right;
		hair_idle_animation = hair1_idle;
		hair_up_animation = hair1_left;
		
}

if(global.hair_type = 1)
{
		hair_left_animation = hair2_left;
		hair_right_animation = hair2_right;
		hair_idle_animation = hair2_idle;
		hair_up_animation = hair2_left;
}

if(global.hair_type = 2)
{
		hair_left_animation = hair3_left;
		hair_right_animation = hair3_right;
		hair_idle_animation = hair3_idle;
		hair_up_animation = hair3_left;
}






 pd = point_direction(x, y, mouse_x, mouse_y);
 mouse_angle = angle_difference(image_angle, pd);












