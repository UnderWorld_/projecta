{
    "id": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player_obj",
    "eventList": [
        {
            "id": "b37e3393-2574-4520-88dd-4a4671e8ee37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "b8e207bc-e2c7-4791-83a8-572af1ca53ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "ae0d4f10-b0f5-4e64-ad6f-caa14a7f0ebf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "08049772-1aa4-46f1-ad55-3ea0c273659e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "8c66ecc6-eb06-4a77-b159-575099a32d68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f94eac70-5ce3-4862-bff2-24a72417ac62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "02e8f837-55c9-4b37-9170-7c23976b33b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "778457f2-689d-4ed2-93e4-0c256f028371",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "531e727e-aead-4500-90d8-fb51339f2d5e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        },
        {
            "id": "31c6da4f-cba4-4260-9d64-a37623b9dac3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "9a045811-8768-4f1e-959b-344b69ae5f65",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "14e5e620-7d60-4286-9567-25f7c035e2ec",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 0
        },
        {
            "id": "6e72766f-0855-4697-998f-a9594d07ce89",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 160
        },
        {
            "id": "1d7ed558-61ed-4872-a049-ab9a6472ab85",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 160
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "256340d3-275a-4aee-9ade-ce1d7e7afa29",
    "visible": true
}