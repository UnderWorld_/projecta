{
    "id": "d1dd73fb-cf64-4ac9-a277-c799060d31af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tp_dungeon1",
    "eventList": [
        {
            "id": "e4dbdd3d-1e8e-47e7-ae1c-fc52f6d6caaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f59a76f7-a875-4756-9c5e-ce5dbb62f99d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d1dd73fb-cf64-4ac9-a277-c799060d31af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a2ad964-b592-4a6c-90c3-1c191c96fe6a",
    "visible": true
}