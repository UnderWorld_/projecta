{
    "id": "9e38b8e5-b416-4d7c-a92f-bc6608cfa71d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "flower3_obj",
    "eventList": [
        {
            "id": "6f72e23a-4dee-4e92-b84a-d4a860afd690",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e38b8e5-b416-4d7c-a92f-bc6608cfa71d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "655f5b75-f2d9-4c64-b16d-35f17f1418a8",
    "visible": true
}