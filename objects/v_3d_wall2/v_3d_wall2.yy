{
    "id": "9badd95a-e0f6-4e8d-bf66-32d6f5c69270",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_3d_wall2",
    "eventList": [
        {
            "id": "a76d941e-4eeb-4f5c-afd9-c4615c894d55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9badd95a-e0f6-4e8d-bf66-32d6f5c69270"
        },
        {
            "id": "7bf1e984-b56d-4795-bfad-6e3a01d7fe9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9badd95a-e0f6-4e8d-bf66-32d6f5c69270"
        },
        {
            "id": "37c9db83-4972-4e06-adc4-1d0e5e7236dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9badd95a-e0f6-4e8d-bf66-32d6f5c69270"
        },
        {
            "id": "9c90aa94-06b7-4424-a88f-667f21fb3071",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9badd95a-e0f6-4e8d-bf66-32d6f5c69270"
        },
        {
            "id": "9f9b7950-df35-43d4-9399-5716f29d1198",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "9badd95a-e0f6-4e8d-bf66-32d6f5c69270"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "18ba4492-4aa3-4252-aa58-5ca518b287b4",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "4c495576-f1b5-40fa-979b-75da3b250baf",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "64937394-a6eb-44f1-b103-ba186c7b20b3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "6c29c25a-eb13-4e8d-8de5-1afaa02101ff",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e8e4b3e0-01d4-44f8-ac5e-218751e2c494",
    "visible": true
}