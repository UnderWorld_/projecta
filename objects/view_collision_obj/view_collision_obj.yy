{
    "id": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "view_collision_obj",
    "eventList": [
        {
            "id": "97113d71-0d8d-4514-b5bb-97e2fdf9fcc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3cd93ead-9c9d-412a-8703-4029ba172c6d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "725ac854-5ab5-4535-994c-efbb31cf7d08",
    "visible": true
}