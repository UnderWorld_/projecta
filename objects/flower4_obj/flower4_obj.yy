{
    "id": "9c8e36c7-9829-4199-a24e-f33e07ef1530",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "flower4_obj",
    "eventList": [
        {
            "id": "64f0cfb3-77a7-469c-a669-1818142248f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9c8e36c7-9829-4199-a24e-f33e07ef1530"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb88a4f0-a79f-4673-bd4e-529b1a9fc3b6",
    "visible": true
}