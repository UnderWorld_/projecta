{
    "id": "acd68b50-2cae-4764-ae7c-f607a7705d19",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_3d_ground",
    "eventList": [
        {
            "id": "cb648017-d1e9-42f5-8287-e7c71e429329",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "acd68b50-2cae-4764-ae7c-f607a7705d19"
        },
        {
            "id": "f592dc0f-11db-450e-b3ce-ba7bfc5b7daf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "acd68b50-2cae-4764-ae7c-f607a7705d19"
        },
        {
            "id": "8e550050-aa5b-40bb-b01a-0dcf9ba4ce3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "acd68b50-2cae-4764-ae7c-f607a7705d19"
        },
        {
            "id": "009a2902-e5c1-4adf-8fcb-6ba8379ffa57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "acd68b50-2cae-4764-ae7c-f607a7705d19"
        },
        {
            "id": "81df3143-a9c5-42bf-a9c0-f6ffa85cac96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "acd68b50-2cae-4764-ae7c-f607a7705d19"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "83be85e9-7d74-46e4-8a90-d58a53743085",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "8dac44c2-439c-46d4-8b9f-640c69f6d900",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "7a0ccbea-681b-4db6-84f4-34a30cd443a8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "feedd849-e11b-474b-934d-4234bc6b9a2d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "004fc0bd-4de4-4bba-9470-76453a1541d0",
    "visible": true
}