{
    "id": "b7c78de6-ece1-4871-abf3-91df040d251e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "goblin3_obj",
    "eventList": [
        {
            "id": "32779c0c-99ac-4e60-838b-6773bb3b3f03",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b7c78de6-ece1-4871-abf3-91df040d251e"
        },
        {
            "id": "8f8ad37d-f10b-4139-87c9-0b53790636f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b7c78de6-ece1-4871-abf3-91df040d251e"
        },
        {
            "id": "a57315f5-5d2c-42a3-9edf-43052bce2c83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b7c78de6-ece1-4871-abf3-91df040d251e"
        },
        {
            "id": "39db745a-071b-426b-a661-639d03ff15e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b7c78de6-ece1-4871-abf3-91df040d251e"
        },
        {
            "id": "a302e4bb-c42f-45c7-8455-57d4ffe032e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "b7c78de6-ece1-4871-abf3-91df040d251e"
        },
        {
            "id": "cb398119-10b7-43cd-b785-6c9ff273cb28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "b7c78de6-ece1-4871-abf3-91df040d251e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f94eac70-5ce3-4862-bff2-24a72417ac62",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d3052b27-a03c-4940-b19b-9fc2fbf05a5b",
    "visible": true
}