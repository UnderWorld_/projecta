inventory = array_create(5,-1);
show = false;
padding_x = 64;
padding_y = 64;
position = 0;
inventory_speed = 0.1;
depth = -99980;
image_alpha = 1;
enum item_type 
{
	 weapon,
	 health_potion
}

item_type_count = 2;
item_type_name = array_create(item_type_count);
item_type_sprite = array_create(item_type_count);
item_type_buy = array_create(item_type_count);
item_type_sell = array_create(item_type_count);
item_type_description = array_create(item_type_count);

//Sword
item_type_name[item_type.weapon] = "Long Axe";
item_type_sprite[item_type.weapon] = halfaxe_spr;
item_type_buy[item_type.weapon] = -1;
item_type_sell[item_type.weapon] = -1;
item_type_description[item_type.weapon] = "An old worn Axe";

//Health Potion
item_type_name[item_type.weapon] = "Health Pot";
item_type_sprite[item_type.weapon] = halfaxe_spr;
item_type_buy[item_type.weapon] = -1;
item_type_sell[item_type.weapon] = -1;
item_type_description[item_type.weapon] = "A simple but effective potion";

