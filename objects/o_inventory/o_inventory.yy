{
    "id": "9e75053e-bb29-4880-a1c6-7ebee1b467ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_inventory",
    "eventList": [
        {
            "id": "79291e3f-b942-42db-998c-f6958250a1a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e75053e-bb29-4880-a1c6-7ebee1b467ed"
        },
        {
            "id": "6749cfae-44b7-4ea2-8839-b130fb6b3032",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e75053e-bb29-4880-a1c6-7ebee1b467ed"
        },
        {
            "id": "1bbc9397-54d8-45fe-86b7-b6f9e61e9b31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9e75053e-bb29-4880-a1c6-7ebee1b467ed"
        },
        {
            "id": "8290e91d-8212-446e-a78d-bfbdd5ac8cbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 9,
            "m_owner": "9e75053e-bb29-4880-a1c6-7ebee1b467ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}