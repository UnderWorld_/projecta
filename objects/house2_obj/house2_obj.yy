{
    "id": "aff991c1-e078-4096-92df-cc6e6373ff63",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "house2_obj",
    "eventList": [
        {
            "id": "c1d2a08a-8336-4868-8bb3-f1640e5ce59a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aff991c1-e078-4096-92df-cc6e6373ff63"
        },
        {
            "id": "ffca8ea2-9733-4058-a6ba-8af9e023ef55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "aff991c1-e078-4096-92df-cc6e6373ff63"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "66b9c13a-10a2-4ccb-b7b0-a11e6f2b2356",
    "visible": true
}