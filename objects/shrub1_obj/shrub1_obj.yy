{
    "id": "a8ea4bc8-177e-46c9-a879-4581f6b391f4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shrub1_obj",
    "eventList": [
        {
            "id": "0684d6e7-90d1-4c05-ae30-75bb0ec879d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a8ea4bc8-177e-46c9-a879-4581f6b391f4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "88c3310a-ada0-456b-b0d3-5c21f4174f8a",
    "visible": true
}