{
    "id": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_blockpar",
    "eventList": [
        {
            "id": "9c7d99b1-4119-4207-a8b1-ecb19b876b75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0fccde37-7a04-45f2-a79d-2e6413a93149",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}