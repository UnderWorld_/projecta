{
    "id": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "weapon_objORIGIN",
    "eventList": [
        {
            "id": "f501c11f-51a8-42c3-8aae-c98af75fa585",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e"
        },
        {
            "id": "38d3c0ef-aedf-4db8-a6eb-03d5f2fe9cc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e"
        },
        {
            "id": "d851dfb7-1d41-477a-a538-b3ef6c5449e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e"
        },
        {
            "id": "4ab02ec7-360d-4050-8b06-855e2ee432a5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "94607a3d-bb56-4061-bf86-62f4aa8a9b1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "48210e7e-e9f7-41e3-a476-d469aac467b3",
    "visible": true
}