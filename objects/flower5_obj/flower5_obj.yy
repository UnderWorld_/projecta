{
    "id": "c9699b58-09c9-4384-bb78-a3e21953b831",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "flower5_obj",
    "eventList": [
        {
            "id": "b76c09e8-bddf-4abf-8b98-2dd0671e9a70",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c9699b58-09c9-4384-bb78-a3e21953b831"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "746d6927-a1c4-42db-be7f-7fcd39a7cf92",
    "visible": true
}