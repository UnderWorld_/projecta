{
    "id": "eadbb037-08b8-4888-b34d-a7857d762a6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mushroom6_obj",
    "eventList": [
        {
            "id": "a9159adf-78f6-49bb-90e5-f408e8907b3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eadbb037-08b8-4888-b34d-a7857d762a6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a8cf189b-196b-4f45-81ea-2a4d2cd54506",
    "visible": true
}