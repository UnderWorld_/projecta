{
    "id": "767ec481-8159-48f7-90ff-45cffec7a1c8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bird1_obj",
    "eventList": [
        {
            "id": "3cbb6696-487e-4a70-9fe9-613f0e274d5c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "767ec481-8159-48f7-90ff-45cffec7a1c8"
        },
        {
            "id": "3dfed95c-e4f3-4fcb-8559-3f21815b1dd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "767ec481-8159-48f7-90ff-45cffec7a1c8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "acf896f7-fcd2-4323-945b-2a3af83c197c",
    "visible": true
}