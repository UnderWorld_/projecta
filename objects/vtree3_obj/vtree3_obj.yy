{
    "id": "320576fd-36dc-481e-981a-ffc8b0619814",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vtree3_obj",
    "eventList": [
        {
            "id": "4d591864-1258-46da-a7f2-1ced3fc43e2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "320576fd-36dc-481e-981a-ffc8b0619814"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6a23ff20-9d5a-4efe-ae25-a798d12a3d38",
    "visible": true
}