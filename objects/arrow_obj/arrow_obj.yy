{
    "id": "530f5969-822e-47d7-899e-d29e69ccb778",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "arrow_obj",
    "eventList": [
        {
            "id": "588cd6c4-0277-41f9-9f2f-e3bc22883372",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "f94eac70-5ce3-4862-bff2-24a72417ac62",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "530f5969-822e-47d7-899e-d29e69ccb778"
        },
        {
            "id": "7c427df2-5b3d-494e-a079-57b5d014f0fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "530f5969-822e-47d7-899e-d29e69ccb778"
        },
        {
            "id": "7e92bccd-c29f-4ba2-9d78-5b5f65648791",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "530f5969-822e-47d7-899e-d29e69ccb778"
        },
        {
            "id": "1d0558bd-aee7-45be-9f9d-b121e7d11040",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "530f5969-822e-47d7-899e-d29e69ccb778"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ed0e2b69-ce4b-42ca-b3dc-a8e28a3bc435",
    "visible": true
}