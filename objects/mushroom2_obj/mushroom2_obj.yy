{
    "id": "4985a9d1-1e2d-4e61-a6d9-7db327128521",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "mushroom2_obj",
    "eventList": [
        {
            "id": "8a9eab5b-cc85-46b9-b1b6-a7eedefa8b69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4985a9d1-1e2d-4e61-a6d9-7db327128521"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9fa55b60-91fe-485e-9f7d-0717f3acf1f7",
    "visible": true
}