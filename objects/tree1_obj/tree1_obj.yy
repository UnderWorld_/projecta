{
    "id": "5cef3df9-06b9-4fdc-bc76-c53fc3a408a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tree1_obj",
    "eventList": [
        {
            "id": "33077492-0e01-436c-9daf-678c287ddd16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5cef3df9-06b9-4fdc-bc76-c53fc3a408a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e0cbefa5-68a3-417c-87bc-6be46c874866",
    "visible": true
}