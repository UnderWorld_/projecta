{
    "id": "d206cf16-0b07-4712-b74c-7b36ad440991",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "tree3_obj",
    "eventList": [
        {
            "id": "481b6f6b-28b4-4a7c-98c8-1d8e6f794c08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d206cf16-0b07-4712-b74c-7b36ad440991"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00943bf8-20e3-4807-8d4f-b6420d4c4784",
    "visible": true
}