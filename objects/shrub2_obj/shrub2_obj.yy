{
    "id": "3aa9e3d3-a68c-4ffd-bc4e-428f863d8e72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shrub2_obj",
    "eventList": [
        {
            "id": "63f90c65-8f56-4259-9e32-7d53c5e5ec3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3aa9e3d3-a68c-4ffd-bc4e-428f863d8e72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58c4cc72-3b38-4323-8f49-08cd569809cf",
    "visible": true
}