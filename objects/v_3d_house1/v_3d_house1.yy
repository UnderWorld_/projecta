{
    "id": "07290ed4-a7b2-4e83-b01a-1d492a3ef227",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "v_3d_house1",
    "eventList": [
        {
            "id": "5a1d0814-6c19-498e-82c6-63a68f674b93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "07290ed4-a7b2-4e83-b01a-1d492a3ef227"
        },
        {
            "id": "ddb539c7-b880-403d-811e-f3ec3d1ff9ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07290ed4-a7b2-4e83-b01a-1d492a3ef227"
        },
        {
            "id": "d0c12aea-f96f-424b-8135-eee42aa4a2d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "07290ed4-a7b2-4e83-b01a-1d492a3ef227"
        },
        {
            "id": "88368253-1806-4df6-b833-a5436278d3ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "3cd93ead-9c9d-412a-8703-4029ba172c6d",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "07290ed4-a7b2-4e83-b01a-1d492a3ef227"
        },
        {
            "id": "23a49e40-6321-4ca9-84e4-a1337b50e347",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 7,
            "m_owner": "07290ed4-a7b2-4e83-b01a-1d492a3ef227"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "e081dfe8-f6cd-4e0f-a7fc-38ccb638e1b3",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 1,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "c6bf55bc-242b-4db9-a653-000fca8dc8ab",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 4
        },
        {
            "id": "41334f34-ac60-4093-99b8-111c59a79153",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 4
        },
        {
            "id": "2b6d4f34-124a-44c3-af29-64c6fb7a8039",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 26,
            "y": 30
        },
        {
            "id": "cb1d91d3-fa16-4409-b7ab-5c770cc76b67",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 4,
            "y": 30
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ee76d032-ec1c-4287-9b8d-178426a2370b",
    "visible": true
}