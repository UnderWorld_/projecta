{
    "id": "ef47e26d-75be-4498-b098-42134aa93de2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bird2_obj",
    "eventList": [
        {
            "id": "9a560068-cabd-4668-ae46-ecc789d27e64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ef47e26d-75be-4498-b098-42134aa93de2"
        },
        {
            "id": "23fbab14-1a5c-4f45-b407-cc05d58d2f8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef47e26d-75be-4498-b098-42134aa93de2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f25994ab-2b06-4031-88d3-f65904bfc91e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "85f51d93-c63a-4511-992b-5924f99e9f06",
    "visible": true
}