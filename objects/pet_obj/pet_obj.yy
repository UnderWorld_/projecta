{
    "id": "ce420bd1-31a2-43d0-b5d6-5c009180b6ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "pet_obj",
    "eventList": [
        {
            "id": "b4894891-d3c3-44a9-beed-cc6db5909235",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ce420bd1-31a2-43d0-b5d6-5c009180b6ed"
        },
        {
            "id": "87397c9f-42ab-461c-ab7f-c170b0a79833",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce420bd1-31a2-43d0-b5d6-5c009180b6ed"
        },
        {
            "id": "739b0a9b-c066-4fb9-9758-0b139897b1c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ce420bd1-31a2-43d0-b5d6-5c009180b6ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "316538a0-1afa-4c8c-a0f1-b2d55933d628",
    "visible": true
}