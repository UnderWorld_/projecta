//Follow Player
ds_queue_enqueue(followQueue, player_obj.x);
ds_queue_enqueue(followQueue, player_obj.y);

LAG_STEPS = 60;
if (ds_queue_size(followQueue) > LAG_STEPS*2) {
  x=ds_queue_dequeue(followQueue)-image_xscale*4;
  y=ds_queue_dequeue(followQueue)+2;
  //sprite_index = lizard1_spr;
}


if (x == xprevious && y == yprevious) {
	
	sprite_index = pet_idle;
}


if (player_obj.left)
{
 sprite_index = pet_left;	
}
if (player_obj.right)
{
 sprite_index = pet_right;	
}

image_xscale = 0.5;
image_yscale = 0.5;