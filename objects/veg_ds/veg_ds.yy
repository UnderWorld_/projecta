{
    "id": "423215fe-c24e-46b2-858f-72a4cfeae34d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "veg_ds",
    "eventList": [
        {
            "id": "14a4ed51-48ff-4aba-bf1c-f24a0753a986",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "0b6c78b4-9c5a-4031-abb6-00e7397d0a26",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "423215fe-c24e-46b2-858f-72a4cfeae34d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a5d7bfc1-63da-4288-bee2-3cc9bffe60ac",
    "visible": true
}