{
    "id": "c6fe5d19-c7d3-4752-aa58-1483ad2bd739",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fo_EDN",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e1dee6f8-53e9-4cbc-9b2b-6a54f9d3eab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 16,
                "y": 88
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "169718db-6764-413e-b230-fa61bcdb4cb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 14,
                "offset": 2,
                "shift": 4,
                "w": 1,
                "x": 17,
                "y": 106
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "3423b074-786d-4e8a-b660-733769c52424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 8,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 101,
                "y": 88
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "3ca384b8-5c16-4331-a331-b9761076fd6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 100,
                "y": 36
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e886c76b-3757-4ea0-8ba5-017ed3e82d45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 55
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "455439e1-a631-44d7-938f-3ca468be690f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 14,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9770de37-d1f0-40de-a1c6-da5299339559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 56,
                "y": 20
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9b95c818-868b-4236-ba43-cb1da2aa9f97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 8,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 20,
                "y": 106
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "20380223-3c42-45e7-a595-b4789d932389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 78,
                "y": 88
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d052ea30-889d-464b-ac8e-86c5fb78a215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 73,
                "y": 88
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "713c9e50-62d8-400f-9175-8d88b05166b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 8,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 107,
                "y": 88
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "5362643c-4db6-4b37-b611-4d672a024899",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 66,
                "y": 72
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e15dd4ff-b9f1-45cb-b163-59f9f879c292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 118,
                "y": 88
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b5fc5e50-94b4-495f-99cb-bdfa1e97c1b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 11,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 90,
                "y": 88
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0683e4fe-b952-4f5d-a9d7-23f89e6d30df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 11,
                "y": 106
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9a8c87dc-2840-4832-b44b-801d9faad0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 28,
                "y": 88
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "0368eafa-8f73-47da-8df6-a250a9d893de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 72
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "1aae8049-e20b-4bc6-9d53-a124548ccdd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 14,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 96,
                "y": 88
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2306f6aa-1213-4757-a854-6d27ecb91daa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 91,
                "y": 55
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b20d163e-a23b-47fd-a31f-20160fac41d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 55
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e9dd5abe-ceab-485a-997b-f85943e40fa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 72
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0fe45a24-aa45-4236-86fd-197b7dc7183b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "49e3c8b6-ea35-4664-9323-3c2b8fed7695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 19,
                "y": 55
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "6e365c55-2590-489e-9504-d7646f635f53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 72
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2d2c14bb-ce1d-4eba-9aa6-2536ea7c52fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 51,
                "y": 55
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3eb66d69-e5da-48c9-9236-0a2a0f9498bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 115,
                "y": 55
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ecbec842-423e-49b1-8abb-7cbac465a338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 14,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 14,
                "y": 106
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d8ff2b76-97ca-4004-a9ca-940376a1adff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 1,
                "x": 124,
                "y": 88
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1a9fea48-e904-47f1-8a45-f86dd7690db1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 50,
                "y": 72
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1f9b2252-26f7-4bce-b4bc-b9180fbce86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 11,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 88
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a22fad7d-7b7f-4e83-96d2-6d7ef810a495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 58,
                "y": 72
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "60488df3-dadf-41fb-bbde-85020454cff5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 107,
                "y": 55
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e1fb0af1-bce4-46b3-badf-0e0e3ad3b9f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "714a3c54-258d-4c2b-a332-aa3d71f720e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "151f128e-6d6b-429a-95ea-cd9137dbdaec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 118,
                "y": 36
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "052581e8-24ad-4789-b337-732cfe0d282a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7abf8649-1325-4592-b2a1-68ce119a9438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 86,
                "y": 20
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "edaca6c7-5579-4e4f-973e-b2e38d023d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 64,
                "y": 36
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1351ecae-da66-4648-aff6-4d1f21097bc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 109,
                "y": 36
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "be19ce6e-fc7d-4e9d-9329-1b0c6c3d44e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 20
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ef556ce2-9b1f-4fcc-ac12-988435877805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 106,
                "y": 20
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6c6e05f6-06f4-4adc-a2e2-fbbb945fa7ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 8,
                "y": 106
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "31f20bc4-155f-4210-93c3-ff9484e474e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 97,
                "y": 72
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5e66ebc4-4c55-4d5b-a3b2-ff72ed771ac5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 36
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "1250a2fb-727f-47f2-9b7a-62620d909226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 72
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "1bce7138-01b1-4482-8bef-409090cc349b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 20
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "24d7be01-fc55-4120-8f5e-24df7a6cfbf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 36
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "054e559f-5e8e-4143-863d-914bc6de59f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a84b3591-44ea-4e65-8134-9e9d49d45d8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 73,
                "y": 36
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a0b9ea80-b4c4-4676-b6e4-90cc5fb29cdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 15,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "14288c28-4261-4b34-aa9f-1d0de712a0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 116,
                "y": 20
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2c7002cb-b3ad-4aa7-a9c9-efe7dccea879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 14,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 76,
                "y": 20
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "ad6c96ed-92c9-49d4-806c-fecbe2e9ec2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 2,
                "y": 55
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8635b273-eaed-4e6d-b310-483aa9609728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 14,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 36
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4fe9a5c7-3269-490f-9c1c-b240b0f72528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "13ad1ed0-1ebd-44c1-968c-50cdb2b2f5fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 14,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "9b64fe66-e612-4b38-b44f-85882d552a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 14,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 24,
                "y": 20
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1c94f4d0-8d0c-4eb9-b150-3e155623d8c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 66,
                "y": 20
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "65c1451b-38d5-4add-b592-9aad3db746e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 14,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 46,
                "y": 20
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "24245e5b-aa3a-444c-9f35-caabe4b9612f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 68,
                "y": 88
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f0fc8b2b-3a46-4070-af43-dc4be3d2136e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 40,
                "y": 88
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e46eb27f-6bb4-40eb-ad17-addf07e328f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 63,
                "y": 88
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "d67b03c9-fbb9-4b95-8cbe-a15050ff11b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 83,
                "y": 88
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c50ab017-acff-4ef6-9050-d3e815b9b492",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 91,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "63fbda4f-55a6-48cc-8a40-41e1c4468827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 6,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 113,
                "y": 88
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "4d1ca034-ab81-406c-a9c8-f4a7e0715844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 67,
                "y": 55
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "05a32213-8f5d-4393-8f95-a40b9dbe452d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 75,
                "y": 55
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "162fca49-890a-4fc6-9711-3bffb0176c98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 43,
                "y": 55
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e1ad69d3-470a-40b7-a0b6-d3f555f70f2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 99,
                "y": 55
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "64be21e2-27fd-4bac-8521-5985a6b492a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 59,
                "y": 55
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "9cc493ac-6085-4744-b31a-97a63b9e40a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 52,
                "y": 88
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0eb001c8-cae1-4478-bcc2-d45eec11503f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 56,
                "y": 36
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "270d503b-770e-40d4-afc6-c230704af800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 104,
                "y": 72
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "43fcd4c3-6302-4be3-8208-095891cedcde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 5,
                "y": 106
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "77ee1fa6-f30b-4bd2-90e4-f3a4228cb179",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": -1,
                "shift": 3,
                "w": 3,
                "x": 58,
                "y": 88
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ba03ace7-9524-45ae-b3d0-cecf7ed2ca6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 83,
                "y": 55
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "15621c56-7659-40be-ab2a-2d7d3cadac67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 14,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4f155fbf-7d2d-45bf-9bd0-c7d1ea49fe5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 14,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "73aa4213-f196-4086-b424-417b4e5365b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 90,
                "y": 72
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "df40716c-a8b5-4b97-b649-3d91e0c0dcc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 72
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "7be6ba61-b246-44e6-8ff2-58d637a36a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 48,
                "y": 36
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "9f6edd6f-90e7-4792-863a-b93f22e8edf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 40,
                "y": 36
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "c75ea491-00d2-438b-a5ed-0386d58bf30c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 14,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 34,
                "y": 88
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2ecc1174-664e-427f-baa6-56804bb045f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 35,
                "y": 55
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e4c4342b-7113-4161-9390-a181db6e233e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 14,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 46,
                "y": 88
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9cb1bc10-14c5-45b1-807e-d27973220c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 14,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 74,
                "y": 72
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "4dc2b7f8-abe2-43d3-9dad-c7c930ec13cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 96,
                "y": 20
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "62f82fce-fb13-41f5-9789-7ea855c8b30a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 14,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1905c0ec-2e91-4384-8739-1b0a34b6de93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 14,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 91,
                "y": 36
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "18c17b3e-b82f-4ee2-b33d-fcb89d03b8ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 36
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "8d1bddfe-e9ff-4ce4-9090-f0b8d7fb4e39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 14,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 82,
                "y": 36
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "80503901-3b9e-4f5f-bc7a-c864722bc397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 10,
                "y": 88
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "83d7f0af-6421-4540-856c-673a39cc5140",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 1,
                "shift": 3,
                "w": 1,
                "x": 121,
                "y": 88
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "016db7d6-f874-4cf2-b65a-48f840221c07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 22,
                "y": 88
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a9277a90-5b3b-4db0-a181-40c47563dbb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 10,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 72
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "2a80d8c1-3ebc-42d4-8672-31d25d809e51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 10,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 81,
                "y": 72
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}