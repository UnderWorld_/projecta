{
    "id": "22be6637-5712-4a81-ae4f-23fd36be25ef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font411",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\font411\\BreatheFireIi-2z9W.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bulky Pixels",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "d958f9e5-a8b7-45fd-9c21-757a36f4febc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6c03bdef-cb2e-475a-9bcf-fcd2d349065f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "f44afc82-e6b1-4a9a-a867-a3115059711c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 241,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "1cb1476b-753f-441f-a8dd-913fb468bcfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 226,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "34096ef2-c054-4a17-841a-487b44e9e800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 215,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "56b8e9f7-2578-4eef-841c-d975d5fc21db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 195,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "340e1ac6-29e8-444f-be77-b28c0787b3b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4d819306-97ef-4156-8b9d-7d8b88691afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 175,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5ce1e97e-e10b-4f71-8e0a-09b361d5080c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 166,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "2f322970-783f-4022-8d96-2785dab0b957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "b2002a97-9cb1-4c4c-842e-e79d17b7762b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 8,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "ba61bab4-989d-41de-86e2-620b7faf1e60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 145,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ceca9422-aca2-48f5-8a24-90140cbea9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 129,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1b23b450-0442-4dc8-b002-7cb4ef41a944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 119,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "dfb9c4d8-90fb-4a7e-a16e-7a045d299d2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 113,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d9a80bef-050f-4627-9650-e22258ca9d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c526f17f-e53d-4988-bf27-7594b60cbffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 92,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4f094213-c408-437f-b9c3-dd8795c52cd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 85,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3ab5b595-62c4-401e-b556-c2243ac5cbc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 75,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c3a9c8c5-affd-4b83-b391-0a73ef6f7e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 65,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "68b9db83-1c19-412f-aa08-63a57513461c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 55,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ab3a9e06-1114-4603-8d19-a92016937f40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 135,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a1ba39bf-a241-44db-be47-73f9371a2aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 23,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "35bbab7c-0d2c-446f-b5e5-b72f0364925f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 33,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "ffcb2725-ec60-4633-afa7-976109a00c67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 43,
                "y": 59
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b3a43e35-0ee1-4dc9-9d3d-430648ee3143",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 26,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ecfbaf13-8f56-4c11-b2d3-240fdfa7eeff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 20,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e0131c2b-0f4e-4477-bdf7-7d22b687ba28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 14,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9e0102aa-aa34-4245-9b74-633ee7be4904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "93c2be9f-730d-43cd-9c42-9a45ba9d710c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 238,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c35a10c9-1804-4d08-938e-314b8ae48214",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 226,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3b79aa01-3939-47d3-8f72-21ef7cab5330",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 217,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "634814c9-463f-437f-b104-b1986b53934a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 202,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6483d92a-f287-4de6-8df0-175894590082",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 190,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3fdc93da-6132-41bc-86d9-a9dc2a4f99a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 176,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1656ee72-4e01-4d1f-b083-fde71c0a0813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 166,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7a943677-130a-4429-9b12-f0b7a8a1f4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 154,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e3299003-60f0-4374-a185-5c7a0bb5c6d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 144,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "beff683e-b41a-4b5d-a2f2-077bd0b57710",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 134,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "700ef678-ccd1-4505-94ce-1848ea0ff474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 122,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a5569be3-5cab-41af-afd7-68fa60b93bd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 110,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b20afed0-7a29-4718-ab3b-365e839a931b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 104,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "71091dde-c91c-435b-9f75-6eb12d61b1d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 94,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3ef82565-7d0b-4587-8d62-f8d04cf2c5b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 82,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "db7c086a-2a92-41b4-8ac6-7bbaa19aee55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 73,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "10c7e46b-0915-4878-af28-63c070cf2983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c8cab4c3-c81b-4935-b66b-61dc333fc482",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 43,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "e98e89e8-498c-4660-92ad-165adca7d934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 31,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bf22f5fd-09d5-4d17-9644-47604d760b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 19,
                "y": 40
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "43f2c12b-9a0a-4ed3-8073-2709ea13e25e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 9,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5d801f3e-814b-4027-85be-17f2c842208c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8050f9eb-40bc-4705-b14f-aeaf3c53567b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9acf9ce2-988d-4579-9292-106fd4e49025",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "d013fe36-87d4-4369-aecb-d35a2c12456a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6e937fbd-0cf5-4b79-8e73-c091e94ac0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e594a43b-0278-4cdd-a9d4-e6aa41cd309f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "368001a0-a5be-42e6-a8fc-93ed112df255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "31e9196c-659b-4e1b-a2f0-f346388f3156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "45c99065-6ad0-443a-aab3-583d22879c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9c06b73f-8ded-4c4c-bd08-06b4ca5e531f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "5316755c-28df-48b2-b1ac-10a0139b611e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "48164bbd-9454-46bc-8d60-33616befd1fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f367ff87-1863-4b7e-936b-4ee4f10ace16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8fb575ec-205a-46cd-a5de-91bc8eb08556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5eb42ede-a1ea-4d09-a2d3-8cba8ebf85cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "3472adf0-f483-4c2b-81b4-9f8a4db71213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9a12c6db-05ba-4dbd-941b-89662394295c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "e6ab5132-3d93-45f2-9345-12ac82cdfd8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cdfd1ddd-0d73-4823-b56a-8bbf0f30dfb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "ff1aedaa-8ca7-40be-bba5-7de948ccf29a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "73c8496b-6c65-4e7a-b239-650c9b3ded34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7e634fad-43a5-4993-90be-b635952608af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 23,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "250163e5-b7bd-45be-9541-414a75758178",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 132,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3274a7d9-5e4c-40f6-8cb4-d5aa99de82de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 35,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "2f195e63-867e-4ddc-8e33-c4730c2d5ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5852ed90-228f-4a26-855c-195a113f1d1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 237,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "25d6b7e9-0cee-491b-bcc8-4c7f970c74ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 230,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "88765a9c-b000-4332-82b2-be2619ef10ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 212,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3c7555ef-663d-476d-a16c-4823c27db21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 200,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "dacdcc05-0eba-4439-9cfe-7bc5ee87e298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 188,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "62d9ffe4-4f3a-456e-a069-99d3d44119d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 176,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c6aed105-1697-43fe-9b00-33483aa47385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 164,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "17916f4f-f3fe-41f7-aaa4-02caedfa4c73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 154,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "219258f1-b4af-4f5c-bd3d-5b02e4fb1d6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 9,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ccdfa1c3-29c1-4c18-a6d0-9f9670ba0804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 144,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "f735d5ff-ff23-4a1b-a860-c01e3a95ce8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 122,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "14a7d43a-4248-4741-88a2-5976bd8ce930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 112,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "95acbc60-52bb-48c5-9aa4-d3e01c7dadeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 94,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "515f3b7c-3732-43ab-9e0a-7e86aecbcd26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 82,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "08e89578-6374-45c4-bcdf-c2f1dbe4e1f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e0ed8a60-95cd-4e1a-bb62-9c50a3018598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 60,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3bde286e-5c26-4c4f-9814-e3428d87098f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 54,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "4760b3db-c33b-4d2d-9af2-48ca354c09c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 47,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5302bcd5-af6b-4421-a46c-9344fd8183fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 41,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c7919d09-26f5-407c-9a5f-e4a3498a3e3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "25dbea2c-d1b0-4060-b362-78a3de5d57d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 54,
                "y": 78
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "cc7b5395-16d9-442a-bf67-c9bce9ac1f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 40
        },
        {
            "id": "12a87cb5-c9fe-46cf-a2f4-ab74fe5b8a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 41
        },
        {
            "id": "679de785-3c44-4795-9659-789b696c3fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 47
        },
        {
            "id": "c40540a9-ad7b-499d-a16b-619975175f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 49
        },
        {
            "id": "16bdb345-a76c-4f53-8c9a-711a7c1bcfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 51
        },
        {
            "id": "628261bf-e823-48b8-b717-4a7fb99a4658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "bce06a73-c244-4431-a1d8-22b3aa9c9d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 55
        },
        {
            "id": "f32c9cba-76ff-46aa-b478-b50b2741531b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 58
        },
        {
            "id": "f07ab633-b2a2-40ff-b1b1-cb44a6226284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 59
        },
        {
            "id": "bffd1a4f-5136-4cd6-af8a-c2be9b7b82a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 74
        },
        {
            "id": "aa97961e-42ab-46b3-b85c-0275c561f391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "94b6527c-fa60-4ead-987d-2f564c3b3347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 87
        },
        {
            "id": "559fac0d-2824-4d85-9b2d-222f03806d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 90
        },
        {
            "id": "3ca76003-10e8-47bf-82d3-ce330f12ad87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "4109625d-71ab-4903-ad7f-c201d4fbca0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 100
        },
        {
            "id": "51edb0cf-71e2-4ea4-adce-b41916ef927f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 103
        },
        {
            "id": "721c9f86-8a1d-4c09-8ea0-d4adf0c6c2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 106
        },
        {
            "id": "e41f87bf-ff2d-49f2-a447-24b1bfa40afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 119
        },
        {
            "id": "b75c05fe-b017-45a8-903f-4b86901aa5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 894
        },
        {
            "id": "7b41980f-022c-4f92-9b22-c2bc82080d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 40
        },
        {
            "id": "67ff9928-0b72-420c-8d04-26b452f98bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 44
        },
        {
            "id": "4ca6b4b8-b14c-4a0b-a64a-70e530d54a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 45
        },
        {
            "id": "fd0a90f8-c7b0-461a-8938-39a55241c8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 46
        },
        {
            "id": "9d9eae64-2e2f-4200-a937-0fb377c97aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 47
        },
        {
            "id": "36d751f9-7d79-41d7-a5dc-75d5383fab02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "efb328cd-edcb-4d8e-a430-438fd064df1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 55
        },
        {
            "id": "69e42df7-e3ec-4df6-bdc4-7d3cdf10757b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "4998e9db-af92-4d12-b521-956d6d2c5ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "a2bc72e8-43ef-4bce-b09b-b7bd0e10c5cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "a8ae0874-cbd2-4c6e-9a2e-038ed6fb8dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 106
        },
        {
            "id": "c02c14fc-6df4-4829-a357-54f3abf3c3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "c890753e-39f5-4df5-bea5-2912362d51d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 173
        },
        {
            "id": "73959601-771c-4769-bc6e-223eeb9b83ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 8208
        },
        {
            "id": "9c6bfdfe-a48a-4688-b681-eab98871745a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 40
        },
        {
            "id": "0e38c36c-39f4-431f-8304-d6529d3bef80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 41
        },
        {
            "id": "209cfb83-6064-4b1c-a21d-58ab63e66f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 47
        },
        {
            "id": "081bc094-3c42-4477-a820-71ddedb87107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 49
        },
        {
            "id": "2e74be7e-afb8-4a32-958a-61c804a26e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 51
        },
        {
            "id": "e98b112d-bfca-4872-84c6-d5177b5c0200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 52
        },
        {
            "id": "bbb4a7e5-a518-4bf6-a918-927f0096012c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 55
        },
        {
            "id": "d66e52b3-6ba4-443d-ae75-70d51da37262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 74
        },
        {
            "id": "49f9cc89-3904-4de6-81f7-1274a55cbe69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 84
        },
        {
            "id": "7900fc04-9e7d-4b74-9705-a861d99fe780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 87
        },
        {
            "id": "cf405d98-4c69-4552-977b-cb54d4aed9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 92
        },
        {
            "id": "0bceff09-e294-48d9-8434-23028abccab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 100
        },
        {
            "id": "ab3df8f4-7897-4fa3-88c6-9661180fd0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 103
        },
        {
            "id": "680fc8b8-fdd8-484b-a9e3-02c7d72a21e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 106
        },
        {
            "id": "006c6083-76b1-436d-a0cf-56b1dba2b3b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 119
        },
        {
            "id": "fff45bb2-0216-4133-a70c-219de2d090e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 40
        },
        {
            "id": "941358a9-28e9-4924-93b0-887bb18b1d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 44
        },
        {
            "id": "34fb153d-1dc8-4592-8495-267cdab8a9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 45
        },
        {
            "id": "063f2883-4a42-4d88-94c9-92c55e19ac98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 46
        },
        {
            "id": "549ec4f4-a5d8-4e23-8d06-37961b1afdb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 47
        },
        {
            "id": "ee3dfdd9-3865-4b09-abe5-85cba5e3e413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "f1dcf0e1-45ac-449b-930a-a82ad1512700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 55
        },
        {
            "id": "23ffbaa9-4c4a-4e54-9e99-8b6fd5a46cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "6cd5cb2d-34af-4376-870c-1b7158064a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "03a57b50-d7e4-4927-8eee-563299226c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "ac8d64b1-4487-44e7-b697-3220fb79c8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 106
        },
        {
            "id": "dece75b4-b647-401e-a7f2-0b6a1d99e9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "a10cc4ca-bc5a-45d9-8a65-ce534ce4ecb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 173
        },
        {
            "id": "ee3d8a49-ac3b-4329-9af5-d7109f5fd675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 8208
        },
        {
            "id": "92552105-953d-49d9-ba2d-2e6fa2f4a9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 47
        },
        {
            "id": "01cda7ff-5374-4df9-a97d-43415f844608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 51
        },
        {
            "id": "35ce04b3-ba0b-49ba-af7c-9481895a4e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 52
        },
        {
            "id": "77343793-e9c2-480a-91aa-0776c218a840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 55
        },
        {
            "id": "f2eeccf4-5b32-4474-9b7f-0709eabdb975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 58
        },
        {
            "id": "101e8983-0e5e-451d-a60b-b5da76c547f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 59
        },
        {
            "id": "50ea02f6-7acf-4062-a660-1fa8b6ccb6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 74
        },
        {
            "id": "2cd56013-0a0b-46e3-80b0-13931a8273b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 84
        },
        {
            "id": "8c66be26-146b-41be-9457-905ec3f540f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 87
        },
        {
            "id": "c84039e0-30e8-4914-ae4b-75f988cc9618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 92
        },
        {
            "id": "92c7ede6-1128-4025-834f-97d910d4bda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "bd15d9f6-c91c-41f1-a2b1-0cb728ddb96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "f49cf74e-e627-4891-9c5a-deb3279f2a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 106
        },
        {
            "id": "0e731ae5-6cb6-4a74-ac45-1566017e2c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 119
        },
        {
            "id": "8e70db1c-987e-43b5-a6f7-8870ee11fbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 894
        },
        {
            "id": "eddd0412-c564-4092-b80e-7e75b41de995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 34
        },
        {
            "id": "13458cfd-89a0-4181-b838-2211e5668675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 39
        },
        {
            "id": "3070bf61-b3ff-4908-a5be-3f27dca8a4f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 40
        },
        {
            "id": "eff6487a-6250-4cfa-bf5d-e2e2301fc0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 41
        },
        {
            "id": "33d767f5-1b60-400a-91d0-5f693e8e33a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 44
        },
        {
            "id": "2d310720-d0da-4543-a76b-c592985baf5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 46
        },
        {
            "id": "50f39aae-b420-407b-bcdf-1f54d99d60ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 47
        },
        {
            "id": "ede8e9b4-3bfd-4625-81ac-81a558875126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 49
        },
        {
            "id": "2330475b-e016-42ac-933e-e88d7f604e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 51
        },
        {
            "id": "b2149a9c-a6f9-4f44-b4f8-50a438a20d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 52
        },
        {
            "id": "759dc3a6-6a0f-476f-bb1d-7f4564bf2860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 55
        },
        {
            "id": "ba0aa4db-b43d-47d6-a014-14d919bdbcb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 57
        },
        {
            "id": "276f9ece-d657-4dac-9083-434bd2d0957d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 63
        },
        {
            "id": "4f3bb1e9-97b3-428a-bff4-781f7a1a83da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 74
        },
        {
            "id": "450b8370-eded-437d-b1b5-6895c23aaa02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 84
        },
        {
            "id": "b5062a30-0928-415e-b553-62cad354ee43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 87
        },
        {
            "id": "5e3bba05-a834-4056-9c47-b125f168728e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 90
        },
        {
            "id": "416fc75e-aecb-4996-b8f8-530792e15d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 92
        },
        {
            "id": "e3831ebc-8d51-413f-a1a8-1bed5f3d7f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 100
        },
        {
            "id": "7836fe0c-b4e0-4214-a8d1-5db97645dbe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 103
        },
        {
            "id": "d01eabde-f0e1-44b9-85f1-1321c51195d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 106
        },
        {
            "id": "65dc1457-973a-45be-a8ee-8120b5dc62dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 119
        },
        {
            "id": "589dd641-898c-44d4-bd83-c8c1f3538c8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 34
        },
        {
            "id": "ace25af8-fa7c-44e2-912d-5c38134a1e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 39
        },
        {
            "id": "48dce237-32b9-4d31-a824-c3bc4251c20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 40
        },
        {
            "id": "8fe14655-b16d-4ad2-883c-b5088cff5483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 49
        },
        {
            "id": "9ce96a66-ab68-4b1f-b6a6-933fed41662f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 52
        },
        {
            "id": "383af299-63c6-459f-8fdf-97ac0bb2f37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 55
        },
        {
            "id": "435251dd-b6a8-4b65-a3fd-c733c7dc3e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 57
        },
        {
            "id": "950f4065-803e-4bac-9a0c-80e88006fef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "e64b33de-5344-4e56-a31d-0bb765341328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "3be7dce5-5d97-4c27-bb1a-b9f4a8a97dc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "2d518131-93f8-4017-ae37-f9a664cea245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 92
        },
        {
            "id": "b3114d81-55ef-4b4c-a209-c098286d9b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 103
        },
        {
            "id": "e431a5a5-18fd-473f-8093-ef6484a9a3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 113
        },
        {
            "id": "b80ced29-9ea0-41b3-8640-8f0da7cec0e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "6f3f0de5-302a-411b-9efa-764fa7c2c8a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "1111133f-48aa-497a-808d-b7b7984d19b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "58a9f4b5-5312-4272-aaaa-2c11ac36916d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 45,
            "second": 34
        },
        {
            "id": "af0a35e9-80d8-4d02-b3f5-b3cca489e040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 39
        },
        {
            "id": "a9b193a3-7f6b-451e-864f-c603852dda98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 41
        },
        {
            "id": "bea820d2-f096-4d18-9c65-c9c6c6ee0bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 49
        },
        {
            "id": "267bc152-eb1e-4d7e-83b8-b709f90153c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "5211f66c-9040-4a08-9a25-589f835fe1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 52
        },
        {
            "id": "53d7765b-5044-4604-afe3-590fb9a542ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 53
        },
        {
            "id": "3832cc3d-6feb-41fe-8b44-160183e414e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 55
        },
        {
            "id": "3ebdfb17-22b7-4df0-b17b-45e373f71910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 57
        },
        {
            "id": "bd32467f-2afa-480c-b5d7-ebb3fedda5ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "9740009f-5efd-4533-b942-ec74be2d2095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "38bce0d5-2903-49bc-b51e-55a1a19f4009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "9c7b51ac-749f-4152-9e78-3fe280390197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "d7aea0e3-bc06-4756-a5d2-866cc29da55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 83
        },
        {
            "id": "2598cc85-2867-442e-8711-d427792a364c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "57a30284-71d1-4fb6-a71b-68dad701334b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "5f49a230-0d17-4f1b-bf1e-a73b8f28d26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 92
        },
        {
            "id": "7e98822b-1f43-4f36-b251-41a361960f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 93
        },
        {
            "id": "c22fe9bb-f640-4fd4-9bc6-bb1c3d4cdca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 106
        },
        {
            "id": "80acb205-73e1-4e9a-b733-155692c009ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 115
        },
        {
            "id": "141ae6c4-99a5-49bc-a8b8-d10acb8fdf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "54bef3da-c216-4ac4-8f60-85da71a4f9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "cca23522-0189-4c96-93e9-3ef6f349082c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 894
        },
        {
            "id": "beb98459-a4f0-4bb4-9ff4-cd8c6452deba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 34
        },
        {
            "id": "fd7274ab-c4e6-47b2-99be-4d683de91baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 39
        },
        {
            "id": "2fa18224-8afc-48c2-a0e9-b09ccadf42b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 40
        },
        {
            "id": "9274418b-d7e8-4921-9780-ab6cac77bd32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 49
        },
        {
            "id": "43d32aa7-e3b5-4759-b41a-7df088daa5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 52
        },
        {
            "id": "28ccc0e4-920c-49a2-91db-ef86cbd59ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 55
        },
        {
            "id": "98bc47fe-ff53-48a5-8452-150f10f82d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 57
        },
        {
            "id": "eb0ebd4b-eec6-4815-b8ad-beb55535e987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "b1cd7667-1fb1-43f5-94e2-c9dc2780e75e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "12056d3f-7132-46dc-a8b2-716c95af1999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "6dcad5a7-5837-41bf-8c9b-d7bd6a36e1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 92
        },
        {
            "id": "c8a0d826-24af-47d9-8109-3b21c5f12cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 103
        },
        {
            "id": "a5d1445d-9da6-4fc0-a0c0-c482e1b665ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 106
        },
        {
            "id": "1fc3a8ed-0956-4e3a-87e1-68ed4b8800ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "ae95e37f-9c5c-485e-b557-24c16e3e282c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 119
        },
        {
            "id": "81e46171-28ff-45ce-bf0c-8f54d6483c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "858a1e25-2dfd-4b55-a8f0-197ab3e23381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 44
        },
        {
            "id": "42556493-4310-49b5-9ee3-85fb63f5c54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 45
        },
        {
            "id": "ede58714-6c27-400c-b63d-4d4ffb64a9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 46
        },
        {
            "id": "c867f06b-2596-4c3d-8b62-0dce275c7bf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 47
        },
        {
            "id": "ec989d2a-d52d-45d0-9fd6-b033456f26fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 51
        },
        {
            "id": "5f013004-9b56-495c-9c63-234bdeb56c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 52
        },
        {
            "id": "5365a048-a5e9-46a0-b8cf-66cb53ff05bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 55
        },
        {
            "id": "6c243545-1bb3-4ae0-90fb-ebefaf48199b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 58
        },
        {
            "id": "251ed6b5-89db-4ffb-986c-e2411051a198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 59
        },
        {
            "id": "571d86f9-bf22-4cfc-a6ff-ed699fb1dcc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "562e6779-8f93-4b1e-afff-47e473b00889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 84
        },
        {
            "id": "b5c96df0-de6a-445e-a8b1-8ae9830fd0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 87
        },
        {
            "id": "a5bda37a-6822-48a2-897c-ab33ef99082e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 92
        },
        {
            "id": "1f875718-7cd7-4fab-a6f5-10cf01cc4b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "64aa8562-3ce9-4827-9891-1fae0c999241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "92d14c2d-4b55-4246-8dad-c5f4b0c24f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 106
        },
        {
            "id": "8855142a-7bd9-43fc-8520-3f7cc9c1a50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 119
        },
        {
            "id": "f2bf3b2e-36ed-4b60-a365-3082e8f663da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 173
        },
        {
            "id": "321b78e7-3a60-4779-a7f3-34090fd6de05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 894
        },
        {
            "id": "1a23a15d-36cf-4361-82bf-3c3a1640e348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 8208
        },
        {
            "id": "ebd81434-3f66-40f8-a34d-7f04964feff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 47
        },
        {
            "id": "156f7283-55d8-4012-b2b5-23eba54ef58b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 74
        },
        {
            "id": "c1ef064b-40a7-47bd-9960-b38326f31230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "feafb338-4418-4843-a99a-77ace2d02fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "9e369b53-8feb-4d57-95d0-3d00fcfb7979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 92
        },
        {
            "id": "9c8de5d2-c939-4562-a929-6c511e00781b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 100
        },
        {
            "id": "e9ed29a0-8e58-4e90-882c-956cf1db205b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 103
        },
        {
            "id": "312abbe9-c33a-4f0f-aa89-f249aeeaa153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "1358be7f-6573-4620-a54f-c9d27165fb16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 119
        },
        {
            "id": "e46b2dc1-f90c-4960-8e14-49a92508b42b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "b60b684a-d283-42ca-8b64-90c4e3e9fb9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "1f34aef1-badf-4341-8bc0-51d202988712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "3fc06875-cb8e-48ae-b2e2-e3532ce8b831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "72453a8f-0ae7-44af-928d-f4520e693335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "c601aa9e-5102-4bdf-a2b7-193ca2eaca18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "14ba2b09-6f0b-4469-8e99-655329398538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "c72d4bab-d263-4d82-8a3a-964767af9c58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "43f51c27-89b0-4e54-8fe7-fb1a6d6ce143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "eaa550b6-20e2-40c6-8ec0-f4a6c0399894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 40
        },
        {
            "id": "c1115344-4026-4010-aaad-51845aa53781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 41
        },
        {
            "id": "606aaf9d-f4a6-47f7-ac91-36224d8c60d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "7936282b-922d-48d9-9274-388434b62b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 47
        },
        {
            "id": "934db38a-915e-48e3-8169-33081549c5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 58
        },
        {
            "id": "07567d54-ad36-4036-b39c-f19f2d787b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 59
        },
        {
            "id": "b080c5cf-bb83-4e70-9bf8-b450edfc44c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 63
        },
        {
            "id": "f6096dc4-937b-4010-ab05-fa55691c3dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 74
        },
        {
            "id": "57136f02-5fd0-40c3-b1ad-60ec17abae01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 84
        },
        {
            "id": "e76c5352-075f-4fa4-9f63-6c62243f99d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "2014cd10-0506-4547-8936-6a476b718ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "d4196e75-927d-458d-ae80-8152d17717f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 92
        },
        {
            "id": "d3d95c95-8862-4673-b385-02edd191ff7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 100
        },
        {
            "id": "7816787a-f779-499d-ad8d-199932c57588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 103
        },
        {
            "id": "6184b0f8-bd10-469b-bb83-89c33e6f8f96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 106
        },
        {
            "id": "07149656-c426-4926-ad67-d806e2195838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "073ebed7-e133-4838-8f1c-0a63b046af24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "05cf577c-a853-49f7-88c5-06e8c808409e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 173
        },
        {
            "id": "3c9f203e-d6c9-4237-b237-81821a94404b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 894
        },
        {
            "id": "e2e1c75f-44f1-4c35-a505-927fa1aabfef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8208
        },
        {
            "id": "05b9bc5b-f970-4a9e-8621-d36927cb28fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 47
        },
        {
            "id": "723e2133-32da-4772-83ff-f20b585c6bea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "f355d686-4630-4729-9356-b69cec4e73bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 84
        },
        {
            "id": "92aa74f7-ef74-4797-9cda-63068fc1287b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "39bf35c8-ce38-48aa-a4c3-c950bad8267e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 92
        },
        {
            "id": "6789ec7b-505f-4e30-af56-bc1a450f66b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 100
        },
        {
            "id": "2594d115-2738-41b9-9e19-f738cf8fee27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 103
        },
        {
            "id": "4680b877-b083-4362-bd8d-0207b95830ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "63fcfb1a-1843-4191-83fc-8bde65a8f6e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "1f897be2-1d27-49f4-ae0c-7e8caa95680d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "7efd56c6-4314-4728-91c5-fbc7307f29ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "418a7c10-dbfe-4e66-ad5d-0d52d4164dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 40
        },
        {
            "id": "e19e9b79-c6e6-4bb1-979c-8f04eaec3ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 41
        },
        {
            "id": "c64749d2-3b96-484d-8781-da196b5c0b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 47
        },
        {
            "id": "01880b66-65e7-4a43-9a12-50aafd08cee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "e821f8bb-1cfc-4150-98f9-64ebba60701c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "b02e9260-22ef-4f47-a12d-7348983c2a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "68ff032e-9702-4bd0-8cc0-1e6326a8381b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "6c02e50d-3576-4bd9-be9c-31a3e2551af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 90
        },
        {
            "id": "56402941-be62-4e86-a547-ab6f7579d339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 92
        },
        {
            "id": "cbafb320-374c-466a-977e-9f373c6f343b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 100
        },
        {
            "id": "b2b8f738-fce5-4529-bcdb-341645ed52a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 103
        },
        {
            "id": "deba8951-d1c1-46ff-8c9f-7d8d64317b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "2b9bace2-1697-4dd8-9d6a-81360edcb576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "6c91a5fd-a231-4a8e-9624-a6cef81c7b8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "400c39ea-97d9-4ad1-bde8-c3c6c6f0217b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "e681c064-82bc-42e5-9072-1c2aca92b73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 40
        },
        {
            "id": "d74f6fc1-5cf2-4bb0-a4ab-07c3a3a4c443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 41
        },
        {
            "id": "ddb45462-5511-4dba-8e4f-c4f0b4330976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 47
        },
        {
            "id": "c3222f64-6c28-4626-beb3-9e9bdedf2b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "afdbdefd-533f-4d05-8975-87d69dfcd0bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "8b3e2d61-561c-4e95-a97c-2b566c3bba24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 63
        },
        {
            "id": "11a4453c-8033-411b-9c0c-a57f1ba8357b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "42566f14-76ef-4ee6-8909-f3c77e98ae94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "49eb811f-71d5-4bfe-9a95-96481180c7b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "de60fad0-00a9-4836-bf53-b8c02363f18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 90
        },
        {
            "id": "9e4ba1dd-bfab-404d-90b6-786afba9bf99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 92
        },
        {
            "id": "c7a4d804-e68f-4cc7-8638-6516900b36cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 100
        },
        {
            "id": "03721d4d-5560-42f9-9fcc-93cd006a661b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 103
        },
        {
            "id": "ef61333a-20a1-496a-931d-a77603123132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "5f6e3135-bd0e-4a50-ac4b-b08e3442c93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "76500906-3d88-416e-be13-0da060e4c383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 894
        },
        {
            "id": "62095fb3-8101-4d50-ac4c-5a2ebb7a0862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 34
        },
        {
            "id": "b66bdc07-ce85-4fa9-9c47-2e96f28f8185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 35
        },
        {
            "id": "4a9db6df-1867-4521-87bd-4d9684533f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 39
        },
        {
            "id": "e2d79e7b-6ca6-4b0d-b9e3-48501510a80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 40
        },
        {
            "id": "281feff0-16e5-4020-8651-7d581abd51cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 41
        },
        {
            "id": "fe0b919c-45df-4167-aa80-30e85a48341e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 47
        },
        {
            "id": "f6b9767c-5b56-4efe-80f2-808f3122eb36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "6d204348-1e92-46dd-a477-e6de8ac98031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "d9f961ee-a0e5-449d-ad12-433c157291a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "595ecc50-2625-44f4-99bd-63aa2407756b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "c45f10fd-e0b1-4da9-a039-e5d440a73a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 83
        },
        {
            "id": "8a18123c-52fd-4a66-9146-716aaff4a39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 84
        },
        {
            "id": "c1226527-0c4a-47ac-9dd2-be87f411b12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "99e57182-a5b2-4b8d-b60c-6d697dc07e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "d3055d2e-bac9-451e-9b35-064ef5fabea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "c1cdabea-0a8b-4c3f-bcda-b486271fd030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 90
        },
        {
            "id": "b37f5f01-6704-42b6-8130-bdc7402013a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 92
        },
        {
            "id": "555fc1bd-a791-41ae-b761-2de979dfc741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 93
        },
        {
            "id": "3b819609-36fa-4302-a441-feffd27bb11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 100
        },
        {
            "id": "788604a3-01bc-4fb5-929c-1b47a386add8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 103
        },
        {
            "id": "c32d235e-9bf7-46e2-a131-5b4d7ba6604f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "0927b849-91ba-463a-bc0a-bfff2a3bea8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 113
        },
        {
            "id": "cf74eca1-ce20-475c-9f06-e6e98f9fba99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "3177836a-3710-42bd-81e4-b949e318b763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "d9d41577-84e3-4883-8af2-19960499b1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 894
        },
        {
            "id": "1388bc74-5190-486b-9f0b-af041460e764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "efe709ab-4d61-48a2-8918-83f1daa8454c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 74
        },
        {
            "id": "09751878-32c6-4442-af97-3e6056200fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 84
        },
        {
            "id": "462f2599-a9fc-4038-b8f6-dce19a86bdb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 87
        },
        {
            "id": "6ab4f7ed-c743-4cab-885d-22efc281e3de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 92
        },
        {
            "id": "c757d8d7-1b58-4bce-9b71-aa3e23ad83c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "24305bed-6a1a-4fe7-a477-fba3db834ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "0a82d64a-d1eb-4e92-861e-5038d63aec04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 106
        },
        {
            "id": "0497594b-ad08-46ab-b43b-7c0f4cfb47d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 119
        },
        {
            "id": "ff5211a0-1725-486f-865a-0d1772e2d8fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 47
        },
        {
            "id": "13a1c96e-7e24-42aa-a386-3c4a9563afd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "09006665-010e-40f0-b6fc-c1a50817240c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "d1058afd-e5c1-40f7-bb7f-57cfc89ab57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "a50599ea-290c-4a6d-ad42-3b1e9cc95cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 92
        },
        {
            "id": "e381961a-2adf-441e-9433-95aaa3eaae79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 100
        },
        {
            "id": "35b74df5-1003-478e-b023-5e6ea25d6e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 103
        },
        {
            "id": "518298dd-d7a5-44ba-9849-72fefb18ffd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "309da8ed-769c-404a-93b2-90d15fabb49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "f425ab60-f373-478f-96d1-f178c50d8dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 47
        },
        {
            "id": "6e13c44c-f918-44ea-90f5-5a22bae7904e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "cb2c6695-f6fa-489b-b2c9-e22f06bfb84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 84
        },
        {
            "id": "4aa0722c-ea3e-406f-bd9c-7f75c1235ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "2e8e8de8-2e3f-4e4e-8b77-b606dd565127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 92
        },
        {
            "id": "9a65980a-01a6-43f6-9fcc-2c9a3c6b5e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 100
        },
        {
            "id": "a117814f-fc59-45c7-8f2f-83c3521d414b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 103
        },
        {
            "id": "b605cf45-a8ea-4cb9-abd7-fbd55e47c408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "5b203c6a-96a4-4e01-8cbb-3424d521153c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 119
        },
        {
            "id": "c4939f32-16e5-4518-95bd-f8158dcb2ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 41
        },
        {
            "id": "e9159ce1-402b-44d4-9bcf-f87e58e2ee75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "0699f332-0b1d-42e4-988b-e7b77b9cf450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 51
        },
        {
            "id": "98fc254a-5b44-4416-955a-8e8a7bb05cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "59565748-ea2a-4011-ba39-421c3ced617e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 55
        },
        {
            "id": "02bd4517-a5a5-4494-8ee4-8fbc48fb2edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "f2690020-89b4-4bc5-81c0-5396bb0fa921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "21e9edf6-4c6d-4fd7-9423-5f186b383150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "1075bccf-8329-4c53-8479-ea2cfaa5eff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 92
        },
        {
            "id": "629c1ace-99c3-45d1-87ce-bbcde2ebef85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 106
        },
        {
            "id": "c8d15b5f-0a77-43c4-af31-75eb587d200c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 119
        },
        {
            "id": "584684f5-a614-4575-9da1-87f4e5472068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 173
        },
        {
            "id": "6cbb30d0-26c7-4322-a946-17412c3b3c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8208
        },
        {
            "id": "7e5ace00-4749-4ed7-9746-89b91f9d1b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 41
        },
        {
            "id": "dd56a2b2-2b5c-4082-b115-4c99d096511f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "33b8efe9-c14e-4464-969f-0325b38a39b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 51
        },
        {
            "id": "0c68c463-6d34-4c4f-8782-9e17074344e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "9ee6bf6b-1eab-40e1-8596-cd67db036aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 55
        },
        {
            "id": "89a4351b-97dd-400c-b9fd-6325662ed4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "531f012a-4a97-4e44-838f-e079e14c8c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 74
        },
        {
            "id": "89a6c630-c311-43a0-aa2e-0135240ee1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "9ea17f65-8cbf-4b85-bdf6-14ee53577bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 87
        },
        {
            "id": "0f9f0211-33b5-4c77-98a0-3d0ca75f8959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 90
        },
        {
            "id": "b1000f74-6f4e-49e1-88fc-f0f08640e891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "09755cde-c1f5-402d-86e1-3ca99edf7d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 106
        },
        {
            "id": "4219e7ff-faaf-45e0-8870-102fb218e8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 119
        },
        {
            "id": "13d1e1fa-a882-4d27-a278-9ce33401fdd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 173
        },
        {
            "id": "47ce5c82-3e54-408b-9376-2e77b7b921ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8208
        },
        {
            "id": "202692db-bf22-4e2c-af35-865c700e864e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 44
        },
        {
            "id": "55ba2b3f-b187-49be-b169-3045232ecca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 45
        },
        {
            "id": "c7966367-1a67-429c-b92b-73d7bc1b32e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 46
        },
        {
            "id": "afb0f2c2-60f4-4f0d-91ab-c929017c4d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 47
        },
        {
            "id": "00f0cec9-2605-45a8-9c06-a3ddb8c1a9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 51
        },
        {
            "id": "39f96ad6-50cf-4781-8182-902fceba6335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "6172d7c8-8e1f-4c3e-886b-4a2b0ed8d347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 55
        },
        {
            "id": "a5040405-67a4-4865-9e0d-8711b1f3385c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 58
        },
        {
            "id": "0a85a3ed-c177-42cb-95e6-09cad9780b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 59
        },
        {
            "id": "a5a3f749-3085-41e2-ba4d-15d56764940a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 74
        },
        {
            "id": "fc911ebb-0c9f-471c-9704-a16d07515482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 84
        },
        {
            "id": "25f23ba4-3cce-4a01-92f0-c833740b8b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 87
        },
        {
            "id": "72fea824-833c-4e5a-aace-e7480416bab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 92
        },
        {
            "id": "2aac8ede-c23f-4efd-ba48-bc316a7cc716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "cf12abef-e99c-4eb3-a4b5-dda940db8f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 103
        },
        {
            "id": "a4ca2034-47bc-4c7b-bb41-a0feb9383913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 106
        },
        {
            "id": "cfe85b39-8689-4c90-90e6-74a28090ea4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 119
        },
        {
            "id": "fa8907f2-0ad0-43ad-8206-738b67c7fba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 173
        },
        {
            "id": "185f3090-65f5-4708-9c80-bcdbcc353638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 894
        },
        {
            "id": "3a0cd21c-4e94-4e88-bff8-9a4590fd1f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 8208
        },
        {
            "id": "597d28ec-595f-4da1-afc1-acde74b1c15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 45
        },
        {
            "id": "935448ba-bf64-451e-8566-5072710f2458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "4df4ca55-5406-4a84-94b1-1888c990993a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "f146dd9b-ed08-4fa7-8dd2-38e7b5a6384f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "2503468b-5b61-4703-ae48-cfb889c4dd69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 52
        },
        {
            "id": "07436bc8-6dfc-43f4-a3ed-76527c934d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 55
        },
        {
            "id": "5d1f1c49-835a-4991-89f8-73ad931cf28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "c55b213c-21cd-46d2-ab2f-68295542524a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "240c4184-6f3c-4cbb-9729-81822b0a95ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "b14e8b3c-2c6d-4876-9ee4-34a35761dbb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "e940f18c-80f7-4fa0-b075-304d83eee157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 100
        },
        {
            "id": "6e6efe8d-0c79-4e22-948b-e0cc40536cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 103
        },
        {
            "id": "2d28be6a-5947-4add-ad8d-7788e0560814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "eaa01349-7860-4b98-bbce-651797b7a5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "89f8b73a-3339-4ed0-96f3-6e5a64be9b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 173
        },
        {
            "id": "be30eeca-7acb-4ee5-bcbd-43f55f72288d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8208
        },
        {
            "id": "c8b62155-bed8-4d76-a42c-e32f485572d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 47
        },
        {
            "id": "2cc042a7-6a29-448e-975e-31be49aeaff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "33962b06-8165-4c1b-96a0-2a35ac6d250d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "d11ff3a2-7764-4361-995a-53a0405bed5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "17050b27-bede-4a05-b5d2-a2ff4f2eda98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "3c0d2c57-f5a1-4ba2-a3a4-b0b6523b3b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "7c71975d-3275-4c6a-a4a1-5afd36be5e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e2742049-5ec7-4c1d-8f8a-0a1411221f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "d4ac470b-6f19-4d0d-a9a4-b2fb75117862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 100
        },
        {
            "id": "fadc2762-16e8-4a85-b448-dce108992234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 103
        },
        {
            "id": "d2967971-b488-4b7d-bf68-ef8c1ce4850a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 106
        },
        {
            "id": "05f38f88-e623-471c-9576-a3b2b9ed9329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "931ba9ff-0a38-4cce-b153-41370da79634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 34
        },
        {
            "id": "6a6740df-38d0-40dd-ae2a-eb4be97d5b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 39
        },
        {
            "id": "232caa58-ff70-47ac-80ec-c030d534b19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 40
        },
        {
            "id": "1702ae4e-6dbf-4d75-9109-c399f7ae1c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 41
        },
        {
            "id": "57879777-c441-4be7-83e2-fa4e970e029b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 47
        },
        {
            "id": "6a1c7793-3dee-4f87-ad43-3e6da6ca7028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 49
        },
        {
            "id": "4b594081-fb9c-42fc-a632-366a94a30d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "bd4ea2ee-516a-4cde-ae43-e24e0bc63ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 51
        },
        {
            "id": "de4e2d8b-70d0-4ebe-9f23-4b2117298db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 52
        },
        {
            "id": "e3e1b849-94b9-4889-bd72-ed76f5cbda22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 55
        },
        {
            "id": "a9739a40-9bad-47b7-aa13-067602a60b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "4d7d95c1-f079-464b-b810-655eaf714cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "b337ccbd-68fe-406d-a713-7322d951881e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "48d57986-4dc2-4c5a-9758-a58f46a354a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "7d371acc-d138-40f3-b797-3faf3176ed9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "9e4ff0c1-3abd-49c8-bccd-291ef6708bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "aae34662-fdd3-407c-bfc3-7ca9d7fe4a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "49bf54ad-fd63-4329-96b9-125d43d71354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 100
        },
        {
            "id": "86ea93b8-2a28-4fe1-979b-64e1387be9f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "5950f561-1f15-4588-a79a-b2401f2bf461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "e8833774-751e-4888-bd4b-4d6240592439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "6b02e240-102b-411d-92d0-e23c50964c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 40
        },
        {
            "id": "88deabf3-80fe-49a9-abd2-b2553a8aaf9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 41
        },
        {
            "id": "27bbcdab-a3ab-4de6-acb1-4e255f6aa999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "287875c4-c45a-4645-b988-76e6a92a3033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 47
        },
        {
            "id": "77fbfd42-86d2-4bd3-8580-3107bc506063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "549d595f-901b-4f42-9abc-f1f0a8595e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "c52eb9cf-f7d6-4a6f-9336-54560de7b89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 52
        },
        {
            "id": "a6b18345-b7cd-4f9d-98c8-664073db96d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "3c39d55c-8ef0-4aab-9011-4dde20336a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "4096ce2e-2d6a-414e-b3ed-36639507746c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "9506d190-1507-4a34-83a0-4465982bdc92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "c0ade6a6-2208-4caf-9199-c1c4ea169070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "becff093-6864-4558-bb62-1237c77d5b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "344332a8-105d-4c11-8647-bd65ad089282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "fb61f910-a6e6-42ee-b043-4ec11d60ef62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "32081e0d-a9bb-4379-8ac2-0bd8b4d1aa07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 90
        },
        {
            "id": "0aea82cb-b575-4359-991d-df0deb6eaab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 92
        },
        {
            "id": "5e2f829e-d1fa-4823-b8e9-5e3b3a9b833c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "92d079c0-3b5b-4613-9d9b-26bdb734294e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "6ca76f6b-9171-4cd3-ade3-ba756a733549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "c6b28375-9b5e-4a57-8103-f91a114459bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "dfd733f4-871a-4b4d-84b4-53540810515d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 115
        },
        {
            "id": "1b7d5e9c-2931-4c49-9fe9-d22c25661751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "10ce3d51-d92b-429f-b74f-64058028e89d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "b9d8d596-858f-40dd-9b05-ce69774ffac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "26d252bd-67f0-4a37-86e3-29c15c19360b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "d8d98c3d-7198-440d-9d1a-c8ae9bbd0114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 122
        },
        {
            "id": "5791faf2-6375-40a4-b27a-75cc566347ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "66c44403-f143-4d08-876b-f01799370f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 894
        },
        {
            "id": "3aa3dea1-3b38-4172-bb75-845cfeb68e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "901fd487-5e9a-4d01-8321-3696507bb36d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 34
        },
        {
            "id": "8ac2faf2-6bf9-476d-a616-634c0042865d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "06b3bf84-d751-4a8d-87e1-64710f467af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 40
        },
        {
            "id": "129fb0a0-f002-424b-a99a-ee6bb8dab781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "b89c718a-0a01-47dd-b912-20e82adf3f96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 47
        },
        {
            "id": "3ee80af5-8eed-4aa3-b92f-786c65c0c3c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 49
        },
        {
            "id": "bcae264d-b2d4-4f63-8d0f-a68b52f62e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "3081bf57-d143-4297-b37c-2f352f781611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 52
        },
        {
            "id": "413d94ae-3dc4-49f2-ba18-5eef9f89147e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "511e60a7-fcdf-40f8-a6bd-2c0cb04d82d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 57
        },
        {
            "id": "b780b75e-e788-406d-948a-b40bd33534b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "c068dbfb-468b-4a53-9794-b13c8de687a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "7fa348e1-8d87-4f4f-b39f-a4054d1d5040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "7f602309-a317-4686-83bf-1765fdf2a713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "1afd6af9-d28f-4282-ba1c-67744f5c1e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "bfbe3ee5-a28a-4f8b-bb81-0358099ce691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 92
        },
        {
            "id": "67868154-bb02-47d1-abc9-c4ec5b1b59ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 100
        },
        {
            "id": "d38be074-aa4f-4917-94a4-617cd6218272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 103
        },
        {
            "id": "50e65ee8-38f9-4a14-aa42-47c40e0de291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "fba52c8d-aba4-4a50-a9cc-3fc6fb27770c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "7e8a13b6-278a-46de-ac1f-b97bd34968db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 47
        },
        {
            "id": "af9ff0ac-b811-412b-93b3-3622ce47b635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 51
        },
        {
            "id": "d6c5e9f6-c854-427e-9370-c640a12a325d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "463c8531-7ea2-4c3e-8482-6134204d50af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "957a6233-5c5f-47c3-9159-84d4117c2330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "34cb7e3b-5c82-4102-9bcc-842c309b2492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "42c851aa-e346-4255-90bf-d04ae9f04214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "e973319b-9713-4a5e-9077-c02fe4fbd0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "07947b79-90d0-4d39-8b1e-65a6929886be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "fb2b6a5c-965d-4903-95db-d4f8867d33e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 92
        },
        {
            "id": "fb862c1f-bbb7-4564-8d68-d34b059cf954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "7ae1d9f0-ac5d-4b47-8967-59a3c66aa524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 103
        },
        {
            "id": "a2bf73dd-d2ef-40a7-bdfb-43b0c1a8206d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "dec62c99-8416-4b0e-86eb-3eb75607b4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "6b57827b-79d5-4b61-8308-aa930f295a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 894
        },
        {
            "id": "5c29b2d9-77db-4c85-88f6-301fb832532d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "722ef245-9cb8-40a9-9e62-32f2927479c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "92b3e8e0-06c0-47e9-a5ce-53016a6e50e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "b4829551-43a8-48f8-8f7a-4488dfa3aa3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "87f31de4-16f6-42a2-ab50-9ad7b5d05cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "ffa877e8-fa57-421c-bf33-77fc82fa561a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "e9025a0c-b668-4926-9be9-81cc410967ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "7859b2a5-4a2f-41f6-9c53-a4ae0d2ee969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 55
        },
        {
            "id": "a866d9ca-cdb1-4b62-962c-34aaf3dc00d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 56
        },
        {
            "id": "e80d1324-6235-43e4-9895-ea4ff9f962b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "f511715f-2be9-4a57-b7f7-c4c5be97fbef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "ba5b87d7-1b75-498a-abf6-a92cb07ddf03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "a4792e7d-6a1f-40ab-960e-6842fc9c9fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "98c75d47-d134-4885-8549-e428cc8e1109",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "ca3ff82d-a9c9-415e-a72d-ed3665689524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "3c7c83fc-6124-4f1c-8f48-8f2a76677228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "a1076b66-3e7f-4828-aa88-78f3890c7726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "bf96ca70-ced4-4d1a-be69-8e852e6406b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 118
        },
        {
            "id": "620be864-e753-433b-b8ea-d99f64ad4759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "b7a7f782-34a5-4f0d-8845-dccb1a6904fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "2f3fd7ac-ad08-4dd8-887a-6a43b05983c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "511f3469-add1-4f6e-b47f-3b18cac91e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "6a4d7a0b-f539-4e8b-9df0-44ff1489a87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 894
        },
        {
            "id": "2c400059-e673-479e-b31c-5305689b8bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8208
        },
        {
            "id": "2cb703f7-17a5-4441-989e-7a8377388b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "e68a41ca-5d92-46a6-89ec-d1a7ae3a2e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 34
        },
        {
            "id": "c6a964f7-d250-4e25-8b5a-b68fee2d887f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 35
        },
        {
            "id": "66a6196c-ff55-4eb1-976b-75cc87b43b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 39
        },
        {
            "id": "6f2c928e-ca21-4410-b8ce-8fef3208ca95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "8929d4ca-8fba-4410-b3e8-f1e31ab097e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 41
        },
        {
            "id": "959a95ba-cebd-4ca7-98a5-877f262d0631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "deb48809-3586-40b9-9802-540a4a8c4c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "43bfc1e9-2ad2-4a9d-8c7e-efd94e8967f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "42e73df0-06d6-4510-bbf4-41e00e172d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "2d4c8caf-a459-4130-ae98-f7bf7a6256cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "731fceb6-c529-47b8-bc1e-7e49b5923f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "195cb7d6-a673-4a87-83c5-634559ccf59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "d01115fe-f41b-45cc-add1-7d5d24dc4969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "b66de951-21ac-4d94-bd63-5eb3839d6a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "75335269-90c8-4984-9ad7-a57dfbd04501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "343662ba-2d1f-45a0-bc5f-405fb1544d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "91f66fc9-bb5e-431e-96a3-a626214d35bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "099c8790-5de7-4cae-bb3f-5565e673db34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 64
        },
        {
            "id": "2c5b1857-b2e9-441a-b144-88c44297b33c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "7c371a1d-1b93-46ee-a1ab-27275796ddfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "19e5cada-7379-416f-958e-80e81ccfb439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "f815e187-3e58-4b64-8773-7c41aa8ed8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "72defb2b-8110-4909-9f3a-1bf3924d0225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "3921b22a-9637-4b86-94d1-b3664b535bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "7f085c6d-788c-4933-a236-d254f38114b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "34deccd2-bb62-45a7-b3d7-c066565ba00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "d5fcee04-fa22-442b-88dc-c7aae96de47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "fcce9753-7787-4ed3-9ec2-2b350b7e4f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "dbbbfd0a-f1e6-4e61-a529-d56425f73e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "3b50e318-7a30-4c11-a412-6b42d76f3c25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "8fe002bf-7852-4a21-adde-638d2bf2b855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "bb6f4150-668b-49cf-8d88-3aae3c9948aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "26a97e87-5cbe-4fd3-b7d7-d4b352a1bfa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "595bdb04-51a6-435a-8cde-731119668558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "db004ad9-c1e1-4416-9c33-cbbc21f9b1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "592ada4e-b963-4c03-bef3-ff8ad4ef888b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "1e425718-9a01-4d16-af44-ba8747b45a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "2e8ca259-6983-434f-840b-b76d999056ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "cc1d2374-2ca6-471e-9725-a8a68500f7e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 894
        },
        {
            "id": "a40c7a3e-73ce-494c-8cd8-7e8290a150c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "6955ae09-a964-41f4-b7fd-929cb597634a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "27643260-a076-42ae-90b0-079b9d8bf879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "79a448a3-6569-4f31-acfd-43e17e37071d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "9ad8c1af-d053-4c6b-bc0a-24eed8b91ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "fd5445d2-3af0-47ea-9a5d-432bdb928e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "924d62db-e39a-407c-8bb5-94e71dff7901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "93b54cd2-b656-4000-a9a2-21434f5a3166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "ecce6e68-2aff-4852-bc1f-a7734c161d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "06acee67-8dcd-43dd-96f9-0fcf62796ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "3d1239a5-4ede-4dff-bfaf-2889b8354d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "1beaec57-570c-44bb-a6c5-e1207a153f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "8076de62-5859-47ee-9fbf-5acd74ab7628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 47
        },
        {
            "id": "ec2fd2bf-6f09-4465-affe-a36e66404759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "b7503532-3c98-45f6-a023-34d96b742ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "68ef5859-8504-426a-a642-8e215d0140d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 55
        },
        {
            "id": "5bc6a727-e464-46f3-8d5f-ddae1907f0dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "27c56a5f-90f7-4be5-9878-060b570858a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 84
        },
        {
            "id": "8255a0fa-5820-4ce1-a961-2e9c45adca77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "44f91051-4fe0-451d-b8c7-780af06b8801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 92
        },
        {
            "id": "15faa94a-90a9-46ab-94f3-0800e0d61413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 100
        },
        {
            "id": "e5062873-81f5-416d-bac6-9f4136ced197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 103
        },
        {
            "id": "1b7a537b-78e4-40ff-b6b2-51f7ccdd6222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "f386e5ab-677c-4b70-ba71-782509925048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "94a59113-d4d3-4eea-9b1e-0628ad2a33e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "431c52d6-a585-40f8-9962-1b131374b5d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 45
        },
        {
            "id": "75dad763-9e48-4111-a8fd-cc1906526f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "b180ab93-0d71-406e-8403-9f53932e2cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 47
        },
        {
            "id": "b0e05951-5e3e-4d96-9b97-df749c670575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 50
        },
        {
            "id": "3fe6d259-a05e-4b25-ab2f-74aca40274f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 51
        },
        {
            "id": "4e766259-d75f-48c5-87f3-465dd8695640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "ca383f02-7725-4960-b9c4-eaf71be5e080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 55
        },
        {
            "id": "ed957e57-d5d1-4b0c-9aec-b646c16d56fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 56
        },
        {
            "id": "9d23a6e5-ff24-4ad2-8881-7744d3f3c582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "80a68e4c-1299-49ff-8917-8b2b99b34160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "e454a2cc-7c67-4c04-bb63-fd9c6e0ae311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "53940712-a53f-44a2-8224-265de20429b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "69bf79f4-8cef-4408-a34a-2f67679a1e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 87
        },
        {
            "id": "db430066-4d8a-4953-8a3c-7e1d6b278a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 92
        },
        {
            "id": "60fc91a2-7ba8-4336-912f-c8defb796369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 100
        },
        {
            "id": "ac950edd-56c0-4922-948d-17221b162b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 103
        },
        {
            "id": "6a64055c-b777-41aa-9ff0-4e2d9fa0275a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "b813ead8-77f0-479f-bb9e-b46d9cf2bf37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 119
        },
        {
            "id": "cc6538ec-8e1d-420d-9b47-f946a648016c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 173
        },
        {
            "id": "8d82e162-2e53-45c4-bcc9-8cc4bb510cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 894
        },
        {
            "id": "f4faa893-00c8-443a-83df-79d6772b9103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8208
        },
        {
            "id": "f1e16a08-6bda-49e6-92a5-fbb6e46f1bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 34
        },
        {
            "id": "e033db9b-a1b7-4068-b1c1-d8818dfed846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 39
        },
        {
            "id": "e31ea977-80af-4cf1-aade-7c0f7c52ec57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "316192a0-b4f2-405b-8fe9-5505d9618b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 41
        },
        {
            "id": "4ac74b43-c9a5-4a8f-9138-9c9e130aeec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 47
        },
        {
            "id": "be1d4092-9dd6-4236-98dc-2f4dfe7ff6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "870863f3-6e58-40ad-82e4-d4fd651e2b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 50
        },
        {
            "id": "782444d9-773b-4c53-8fe4-aa23ee9b8255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "0b604e6d-0310-4efe-aff2-78a8ac745d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "0d80ead3-327f-424c-8c60-0cc54d99fded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "bc1d55e8-e4ed-46b2-9879-e86c5e499de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "1c950cda-40c0-4398-adf3-aac08db66dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "1e3e04a3-41ff-449a-93db-c67a4cc5eff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "1fb725bb-4428-465a-9575-2fede10d2e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "d5fc8570-c29f-4d6a-bbe7-4c0198f482a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "69449aba-d842-4263-8197-b16a75fc6b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 90
        },
        {
            "id": "17ec640c-df93-4755-85be-cc86b1a745e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "a11d19d2-39c3-4901-8f30-88d6002f18b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "3508d3fb-a4ef-40f6-9438-05436c2acf58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "9f7d6452-61bc-40c9-859e-806428c6bcfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "5ace409c-d00b-4316-b8be-7b8fe8bb3f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "e93a4983-a6ee-43a0-b2bd-024ee64ed8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 34
        },
        {
            "id": "47661f10-9522-4111-8cc0-478c5e5589bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "a0d71974-7219-48cf-b8dc-d1a73c1521cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 40
        },
        {
            "id": "3362b110-2dea-482f-9c3d-0d988db5a65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 41
        },
        {
            "id": "04e41980-faf0-4f8d-8f50-12d9a33f3898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 47
        },
        {
            "id": "d2785e9b-83ef-411e-b3b0-e8b19e199f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 49
        },
        {
            "id": "f3088aa4-52a1-48e1-994e-d0eac175b7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 51
        },
        {
            "id": "95d02aac-6bfe-4528-9faf-f7ef467a52bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 52
        },
        {
            "id": "23b7c761-c866-4d2b-a8c9-c647a48e05eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 55
        },
        {
            "id": "959dde06-7db5-4774-82c9-a37772716396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "c41b12f3-6503-4c01-8d34-f32fc2634fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "48bc1df0-ba82-4aa0-a1cc-06968a1f8d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "67dbd7cf-be6f-467f-afbc-2a5f48131379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "1eeee62d-9240-4ef2-9af1-f0761bed54ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "4502a0ff-d482-4855-b8d4-6dfdc2cc89fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 90
        },
        {
            "id": "2015592d-29cd-4404-861c-bad437031649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 92
        },
        {
            "id": "15d5826d-734e-4f8f-85f5-c2a3a909c0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 100
        },
        {
            "id": "5d0d3706-f155-4a46-b6d1-054a691bceb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 103
        },
        {
            "id": "6b3cfca5-6353-46e3-a594-7ea67a410706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 106
        },
        {
            "id": "96446bb3-72bf-4900-b104-84ab79037639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "40a54e04-57e5-461c-84f4-66f62803231a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "766ec260-146c-46d8-b282-e709d4af060c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 35
        },
        {
            "id": "4c647b10-4c5c-45db-b5ba-af99d6713111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "c9626849-e12b-4a32-9a40-f3202fd2a0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "98279d49-3ca1-4ccb-bad3-a710ec8d7b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "799dabc1-acb2-4d31-a7c7-85833fb8d9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "09739866-b86c-491d-9169-0a3b78fdb3b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "f9d6bcf4-0efd-4f59-893a-80074dee9b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "32ff28fe-7b18-47f3-91e4-f874282bc217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "08daead9-eb8f-4e7d-8363-37f8a4c941e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "aab6ccd6-8f86-4a01-ab63-7efc3aa6dfd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "2a2b31c4-7dc7-4f60-8526-77eb213d1881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "cf54eb83-992d-49ff-843c-bf1daf04cde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "8946a94a-74b5-4a7a-9dac-75fb5d6a940f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "ee6d6aea-8209-486a-9bfa-d96768f69c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "ddb40a8e-f551-4ee6-b67f-2efb86cc71a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "e99bc4ac-5581-4381-8407-25f36f445c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "23378778-a25e-4021-b9bc-8a4e34b7cdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "6ac9d3c4-4eae-4954-8bf3-d009ff49c4a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "453c94ac-5553-4494-a45c-8173cf1228ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "f810431a-1630-4d02-af07-6fc3380028c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "4d72e7db-27f2-4c77-b616-144ecedaf744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "7f530906-e35d-4f1a-818a-3ef7767f29d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "808f08a1-3736-4e99-a50f-83a7810a7b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "00fdc0d4-6d12-4ffc-85f1-d6a54afc6909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "875dd0d3-2a08-4969-9262-e25ddf569ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "c3daeb72-2301-4966-9426-46ee444ced74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "b93da817-a735-4c00-8939-2ebfc23f9e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "a168b339-d3ac-48b6-98b4-66604dacc5a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "fcee81c5-4581-4baa-b4ef-ca48f76d77ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "b4bc76ed-6092-49e3-a7f0-1c8ab4e09f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 74
        },
        {
            "id": "4c6a58fe-b9d2-433d-a16f-dc22741d69c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "abb844d0-d2bb-4a00-8dd0-6daca8500a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "e121739f-21e7-46e4-9ce3-19c6e3424409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "331d3b62-dbb0-4ed7-b379-addc6e14b608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "7fb348fe-f0f7-4333-b146-ecfbfdb4ee39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "ce7b6070-4385-49ea-be09-4059fb52579b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "a18138eb-2384-4323-9220-233a4fcd4a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "007f3017-f6fe-477a-9925-c530914d39c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 47
        },
        {
            "id": "1708c738-dca5-46b8-ab79-f0a75a72f4be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "5e900f83-5585-4781-a40a-4d01efc77fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 52
        },
        {
            "id": "187fe5cf-db0d-4455-9620-da8eea1d20ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "411da0d4-cfe9-4791-ac16-bec41a737b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "35dfe405-197e-48e8-8136-7d496c70f6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "b579a6f1-7e07-4705-b27e-860d063842fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "8357d1e1-4fb6-4274-977c-3cfd791e3a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 92
        },
        {
            "id": "d0ac3549-ba08-49e5-96c4-24870e928d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 100
        },
        {
            "id": "261d8f0d-68c0-4ce0-846a-95e921274299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 103
        },
        {
            "id": "ff86d0c9-cd79-417d-95ab-daaae1da2fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "1296c128-823a-4600-92ad-096c9f6ccd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "ff13eaaf-e012-4d62-b0f2-c3e481d6232d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "d8714fc9-ae3b-4369-9ac8-6dc2bcb53c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "e9e312ce-6f03-452b-8902-058a354aac42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "4b504a6e-9fd5-45a3-ac5c-d46fe745d83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "68937686-a9bc-4186-b6a5-f6b639688a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "7e584dff-9948-4872-9562-9850fb5c599d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 55
        },
        {
            "id": "832bd7d5-c8c7-4ec1-8693-42d387963d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "c1ac729b-4c17-493c-8eae-dd0920d04ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "2494277a-d303-469b-b812-3b4011d38c99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "27d623d6-3a15-46c0-ad6e-a0d905eb5b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "def63d2f-fd47-4022-a0bf-7687823ab522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "a405e682-7453-4013-9ff2-c00099986fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 92
        },
        {
            "id": "3091181a-74f5-4cac-9e5a-706647048740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "06679777-f9a8-4d22-bffa-6f7352f7f1af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "b8d0314e-b9cb-4e57-acb8-59847322549e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 106
        },
        {
            "id": "a955d550-c098-4e55-85cb-b1a394860671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "4b150414-ca9a-45b1-9a53-ddf71f049fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 894
        },
        {
            "id": "6877a328-8c70-47cf-abc1-ccc9f68ca4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "9f9724d5-71c7-4335-8bc7-e80e422f2c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 34
        },
        {
            "id": "8bd614b8-5a60-47fe-8117-9b53ab08624e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 35
        },
        {
            "id": "d39238a3-85de-426d-ad84-b51818a90b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 39
        },
        {
            "id": "54b93f9c-93fa-4b4d-9f3d-9693703763c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 40
        },
        {
            "id": "8290194e-13e0-43f7-a388-180f4d300c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "4e3cb79e-d98b-42de-aaad-a2bfc3cf6173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 45
        },
        {
            "id": "07f94e7f-e09e-4f1d-aeca-cda1f3e1f64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 47
        },
        {
            "id": "0873daba-34f1-4627-b086-e6e772a10853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 49
        },
        {
            "id": "40cd1613-ee8d-4bd3-8d8f-0944047ae7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "47822e0b-cb29-4a9e-8e2b-fd71b69806f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "c493ff8d-75d1-48a0-900d-9a9780958ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 52
        },
        {
            "id": "3b29bca0-2fcb-4eac-8196-a92ecacad0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "d6db4ddf-e6fe-42f0-9323-fa6dd936d307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "1a13058d-6f73-4f15-8a4f-f4393833795b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 56
        },
        {
            "id": "0d76fd9a-b4c9-473b-9375-326220342f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 57
        },
        {
            "id": "c91d86be-7bd1-4e8f-a0ce-54c5b67dc5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "f0149f95-92cc-4f6b-ae81-dd8760f8557a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "8aa9a772-2ef6-4d23-b2f2-f8b2c60bdd0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "d2ab2fa4-d7ae-4e2f-9f7d-4d9809b1ad23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "8bf3a1bd-5a22-4859-9a74-109ed56cc4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 74
        },
        {
            "id": "a872dde3-7d20-41cd-b5e7-0602c2851947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 83
        },
        {
            "id": "dd97bc3d-a6ed-4947-80be-ef527927c3d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "6e424a57-5ded-4837-ab9b-1775503ad97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "e5696eef-eae7-485c-aa7c-2929bb2af687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "88339e93-1eaa-4628-a1c6-8d9ef100aa17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "60e99a77-1977-402b-a9d1-b565c49ba373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "378c9fc3-dc10-4f1f-8f08-2032073475a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "f0df72d6-7bb5-4c1d-baf9-625d6f727d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 92
        },
        {
            "id": "6d685a0f-8d1c-4e45-a790-ee2a4ab5a338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "5a941c1a-4537-4882-955d-c8ad63c0135a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 100
        },
        {
            "id": "6bc52de3-60ab-4422-909e-d6e72bff0d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 103
        },
        {
            "id": "d8b40b01-4dd6-4279-97d0-a6bb5e3ba7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "0dc34e1c-04a0-46ac-aae8-87ee5cf4a684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "116f5f26-8c35-4b1c-ac49-056d4a161b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 113
        },
        {
            "id": "b3bc2005-56f0-4535-898b-06dc99f0baea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 115
        },
        {
            "id": "ca117937-6bf8-420d-9091-8524502c2d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "f75dd22d-eeb6-4cee-8246-63c3c9ed4bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "d7b87af3-c8dd-48b4-98f6-a661afa96672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "173988e2-71c1-499a-9bec-2ff90cd46111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 121
        },
        {
            "id": "7a02e817-2161-4956-91c0-2eb1f8ad740b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 122
        },
        {
            "id": "e67020d4-3cd7-42c3-a33a-16264381e319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 173
        },
        {
            "id": "52d7bf88-8329-4b3a-bacc-5981acee216e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 894
        },
        {
            "id": "fc43efa7-6eb4-47fa-a00f-aad4dc2102aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8208
        },
        {
            "id": "3bab988f-e0f3-4f39-87c1-141cc4de8e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 34
        },
        {
            "id": "37d2b76f-343d-4779-b489-0fee66c39d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 35
        },
        {
            "id": "2c0e0945-cb07-4119-b688-fb623148c38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "46b9e835-ac16-4cf9-86ed-c8c75ba534ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "3384fe51-81ae-4828-9033-0f750a1169f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 41
        },
        {
            "id": "6ce50bab-fa25-4db8-bcd4-031138c76a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "b2dd481b-8959-40d7-9e79-868f61a21077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 49
        },
        {
            "id": "cb52800a-8050-41f9-adbd-6f1f88ecdb7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 50
        },
        {
            "id": "42d029fa-97f5-47ad-96ba-1a68693b081f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "aaa9b73c-1a82-47e7-ad8b-b83b46b90116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "81866fe5-8b3e-47fb-8f32-08543d05f8f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "80f72e4a-6234-4f58-bfee-f6cc06e9b36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "85d56eed-8242-4cda-9b8a-60cded9a617c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "9f37adba-c48d-4e05-a41f-6de745eb8215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "2a425581-938d-43c0-bcd4-764b5883af61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "505c7d8b-29df-496a-970f-9a05e7bcab24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "aa6332d3-77dc-4639-845a-6bc080d37826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "0921657f-5b87-4fd0-8a5b-cf4a0ad4f41a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "79caaac5-58d8-4d5d-92b1-8f97efa90ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "e351cddb-87db-4754-bc00-2b92663c398d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 88
        },
        {
            "id": "5b3bb775-56b5-488e-acc5-eea9406ba1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "e528697d-de59-4667-8500-c3c5de163970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 90
        },
        {
            "id": "7e81f30e-0522-4c43-aa50-ca47db33480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 92
        },
        {
            "id": "92c71aa2-fff8-4f72-bb3b-fcadb1f36326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "f1582563-c936-4d6e-9370-93a7f28de4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "790a9c78-0f44-402a-b1ea-892bd17f86b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "be5904a0-66e9-4519-93e8-6687735d0b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "c067a707-cf15-482f-b84a-2351453f3591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "c095b8ab-93da-4568-86b7-f3e4c7dd1d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "83b3380d-63aa-49ae-8cdb-8d7b1fcdb597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 120
        },
        {
            "id": "aa4d2200-955a-48c9-9b2a-96238bbb8d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 894
        },
        {
            "id": "e5f81ffa-b70d-4f98-a2f8-bf29a32e3185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 33
        },
        {
            "id": "dba3fa2e-7139-4ddc-8c73-ba411d37da3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 34
        },
        {
            "id": "ef06cdd0-092c-4f70-b35a-fa633c36cd86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 35
        },
        {
            "id": "e3b6e72b-56e7-42fc-83a0-f673f59b8a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "55c9b1fc-90ef-4769-90d1-98fbe6250b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 40
        },
        {
            "id": "b75d3e2f-a0e5-4a26-9999-403bb3df6a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 41
        },
        {
            "id": "dac24412-fb4b-4992-a4ab-9ebe02669cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 47
        },
        {
            "id": "dbab95d4-182c-41b4-9c3c-34943891ae4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "97c57333-14fe-4c2e-b2c9-c64c0c94944a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 50
        },
        {
            "id": "3a8d7cd8-3ce9-458e-9ea4-ff1e9c7691c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 51
        },
        {
            "id": "2e3ca11e-154c-4aaa-b85b-c89ed72d4bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 52
        },
        {
            "id": "6aa10678-5879-48b5-a795-26c4877708da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 53
        },
        {
            "id": "08f1fff2-4f3d-474b-9167-4b364cb9bfe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 55
        },
        {
            "id": "05b32421-e306-4322-9fd4-451dbb94be44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 56
        },
        {
            "id": "713547c6-9254-4e7b-afee-4354d790b4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 57
        },
        {
            "id": "f98314d0-6095-4d23-9942-dc23cbec4eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 58
        },
        {
            "id": "b82b917c-ac23-471e-9c23-428564767d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 59
        },
        {
            "id": "b78f1d8d-ac2c-41d2-a318-2d641d879a13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "ee2dc580-cb86-4b66-8222-12983b88717b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "6669e985-1320-4762-9ee0-1274d696a3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "3ba28ea2-fe3c-4931-bde5-f5d82c3bce4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 83
        },
        {
            "id": "a4780ad2-c82b-4dbc-99f6-838ec673f959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "7f7231f4-effc-40e8-8b94-5f6dee7debc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "7d5f2fb3-baca-4f39-8298-bc69de0e9c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "fa4bbd29-548d-40e9-ae0a-c160c2e79378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "41a8824f-c339-46ab-be3f-44ac965caac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "d1704028-a103-488d-9f94-9b869b395eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 90
        },
        {
            "id": "c7c7aed6-1ccb-4e9c-a324-5d767ab3e5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 92
        },
        {
            "id": "e712aa7f-0bb7-4b50-9e7b-2c8a238ae4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 93
        },
        {
            "id": "edc81419-2851-4dc7-9b99-b2fa844705c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 100
        },
        {
            "id": "af8225d0-e527-452e-ad27-e3a38c818351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 103
        },
        {
            "id": "8b25664d-3a31-45a1-b4f7-e055e5782957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "2e4e9a85-2c9b-4dc5-aad3-a247d6f9ea8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 113
        },
        {
            "id": "e2802d44-7d27-4957-82f3-29b295c0e55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 115
        },
        {
            "id": "753dbd73-d196-4701-b413-7ed58bdc1ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "f4f63eb0-10ba-405c-86f1-4230ff26b081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "d9cc2f9f-47de-466a-b349-1b6383be6e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "dffbab9d-3eda-42d7-b0a6-411f2d913d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "269a9ab4-ab63-4475-b348-9d7a8904636c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 122
        },
        {
            "id": "5229bc87-829e-4e2b-97ce-162a2a69bf3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 894
        },
        {
            "id": "7ed3b580-a5a1-4081-a561-22c5030d4878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "fad6d4f0-db63-4d08-a95a-4f816da24c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "de8b8456-532f-49b4-8129-a351922a2f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "be501182-def3-4be7-af05-03b617c3f1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 47
        },
        {
            "id": "c076d773-8e6d-4b35-a293-a51f631b3070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 50
        },
        {
            "id": "05e4fb85-b0ec-409b-b01c-fa8b1a0a7aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "2f5edf28-0eea-4512-b5d0-d9208b62ce88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "f3634a10-bc22-43a4-aa69-b9826476e8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 55
        },
        {
            "id": "41525d14-1bfc-4726-8bf1-d718741b086f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 56
        },
        {
            "id": "0786cdc3-6e80-4bb3-9c28-5642cef8a160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "8526cef1-2f9d-4625-bdcd-5e40ccf54104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "8ec5a4a3-5357-4c09-bc36-8af6fdf9a6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "8791d80e-288c-47a1-a6cf-a6847864c638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "f561da41-a1d5-45e4-848b-a2cf1862c0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 87
        },
        {
            "id": "40003c94-95d9-42d0-8dfc-2cd85c780288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "51e804a7-24c4-4881-aba2-8e459487c53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "11a9e9bf-53e2-4ef9-b2ba-f8d209309fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "dc1c1f3b-4632-49c6-84ac-122bb20dc6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "e64f977e-9f38-4b3e-b362-5a2a932e3858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "28d53107-4d49-42a5-b67f-ac6ceacc046a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "eff51a98-a551-40e8-a11d-e98ff219ae70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "047add7f-ee64-4706-8e1b-eb5121fa958f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "d4b77f47-111d-45b6-9c52-06df2be1e3c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "fd3a9e5e-b796-4cb1-90c9-b303a499db02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "ae4e0521-8556-486e-a5a2-f6d391b1f3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "2bc64324-0105-46d0-89e3-5805db95ef70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 34
        },
        {
            "id": "1e9183c2-d00d-4235-a47e-a763e30330b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 35
        },
        {
            "id": "31c0adea-b885-4c8b-9a96-93e1c0fb19be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 39
        },
        {
            "id": "ecbd23d1-571e-45f5-bd85-55e432252828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "406185b4-5092-4a99-aada-b32816d3afed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "4d21b5d2-e770-40f5-b083-502d8b99f48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "e4f26168-ed5e-4076-ab76-79e1d11ea2d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "b2060617-3eab-4d02-89e3-0ddfecf687e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 49
        },
        {
            "id": "8d6292e3-9d58-4609-8549-a00361b5739d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "41e0f421-d3a3-4075-9a8c-6af918bd1b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "5d5b81da-35a7-425f-bfe0-aba60b72073f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "4628c9be-b33c-4e2b-b53c-36f03d697758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "31003110-002f-428f-994d-4680353d6399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 55
        },
        {
            "id": "225fa972-8f06-46c7-b84f-e157422a3f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "80310317-de32-46cd-b834-b37ecee30870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 57
        },
        {
            "id": "41a48765-f4f9-4342-86ec-091c4c25886e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "5a4fc40c-6680-4fc2-bf0a-28bbbadd41c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "88c0c7be-9bd0-4483-9c08-6b51e2115c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "1ef2abdc-8f20-4c48-bcaa-4de9a8b9e139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 64
        },
        {
            "id": "ac4f9c7a-76d2-41be-89ae-912122b7cf35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "d2383e58-8e22-451b-b5cf-3754503241c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "ef0da1bc-5ff7-47d2-a6c8-863e83d9efb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "ae91f200-fdca-483a-b29c-12ba388cbf9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "b04d13bd-be1c-4969-b836-6fd407cef1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "1b5a966a-3e2b-415c-bd32-b9cbae9c29cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "52b01ea7-472e-4d12-bb25-401c0f696f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "e5ea9cb4-61d8-4703-869e-e7b197673bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "ac673820-3ba5-433b-8c6c-88442e3b508f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 92
        },
        {
            "id": "b37a3b3a-6cb7-4cd5-9525-b906c8546119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "4d304e5b-eab4-44dd-8d9d-a2468218aad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "ca2595ce-51d0-4132-b34a-88cb1f959230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "6c2f29e6-59b6-4a60-9eeb-8338315f11a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "63eee8f6-8aa3-4a87-8c64-ecb14734deac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "d2aa4083-e24c-4b12-8409-32876587aef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "7317bb7d-7bd1-47c8-810f-0e7d70da1c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "bdcaa7c3-b1c9-4f00-a20d-3c8af92e4a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "fe45f840-42e3-42d5-ac6a-031f06e28b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "7cb690c5-7d51-4055-9727-414ebb2390c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "bfb790b4-47c7-463c-adc2-e1320bbfc193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "ed1b73a4-0770-4f79-b946-1608b562f261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "e4e2379d-3e76-47b5-968f-514ce319436c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 173
        },
        {
            "id": "75a3bd3d-790c-41cb-9ddf-7f0f3fc69963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 894
        },
        {
            "id": "a5f6eb42-b84b-43f2-974a-5c8beae86c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8208
        },
        {
            "id": "996d05d7-d225-4d63-8f83-f8704e3e4bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 40
        },
        {
            "id": "897b2ed7-9894-4ab3-aa97-5680fba711c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 41
        },
        {
            "id": "a2983a80-c178-449f-b0ce-e7efd25dbd49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "973e4751-ef4b-47df-b2da-f1bd8b03203f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "e95d28a4-0124-4c95-896c-41dda79eea8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "8c85a6a6-7a5d-4e30-abd5-4d86e1db8918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "9f8e56e5-d2ed-4d6f-bf19-e5d402c765cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "af69b299-25e5-40a6-b684-9487376fb03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "2630756e-bf15-45e3-8e3d-bc34db61543b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 55
        },
        {
            "id": "291d52c2-a36a-42a9-a985-095cb3c6775a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "17c29719-df19-4da1-8ca0-bfbe3d318a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "b6fc668c-ede2-4837-9120-6411aeb6cafa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "0f897cf9-131f-4467-b875-4f232bc48b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "39a3dd9f-4558-4a43-975d-d9e64ad6dd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 84
        },
        {
            "id": "c898b3ec-3c5f-4658-832a-50e0ea011cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 87
        },
        {
            "id": "9edc2f97-c106-43af-ac12-2a1456051ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "e08de67a-9430-48b5-bd08-0329809d6e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 92
        },
        {
            "id": "3f90fe8b-01c7-4a0c-9b76-41689f70ab28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "bd927667-0207-4e2a-966b-09793a83625d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "742b1503-1831-4369-a5d2-4059c6a40429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 106
        },
        {
            "id": "f38479bb-6fdd-4ae5-96c0-ab35b2eee9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "a976e41e-ed5c-4298-b631-f89687a7ffe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "cb09ef2a-b4a4-4286-8104-af74b4566403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "6b226d6d-6c15-4aac-8f49-e7a62fd95c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "f1f38ad7-fdcc-4cd1-91fa-fc50da794735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "c2562c2b-6dd8-492e-9ac1-d495b974c730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "f4632409-4647-4073-af20-4e5ef77031c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 55
        },
        {
            "id": "435db868-dee6-42fb-8602-545a09c0e1c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "43a46efc-0df8-443f-9117-fb547c0e429b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "ce983d8e-f34c-4275-bb63-12b2c4c70bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 87
        },
        {
            "id": "7320f1dd-dbe6-457d-9464-02d599c575ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 92
        },
        {
            "id": "aa7b7ad2-0719-4a64-b31f-3583f5d63869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "d3b3a3df-dbbe-4671-b925-0b4db4905497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "5857a9c1-0ac2-4e26-a063-c4f7019eb2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "883161e5-3fc2-4b63-b20e-3d9d9f1a7015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "7907c1ae-b3ba-4bd5-9013-4d5620ea3f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 35
        },
        {
            "id": "dfc22c99-77b2-4f1d-a36b-8383b03e329c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 40
        },
        {
            "id": "a8eaa2c0-92b7-4f1d-b550-853f66479788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 41
        },
        {
            "id": "6f2b24f4-b8f5-41cc-ae68-101a2d40019f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 47
        },
        {
            "id": "1239dab0-b9bd-4cee-8295-0e37edac5781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "28b4ebbc-d2c5-472c-b100-9223b77b8f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 50
        },
        {
            "id": "4c75144f-055a-40b6-a1f3-f0afabb748f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "034d325a-7165-4ff2-8aea-1196ca06576b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "338d0aba-451e-4409-a761-59677196d9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 55
        },
        {
            "id": "4d21a634-e6bf-4719-b3bf-8248d9b3d8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "7afb45af-bad7-406f-b1cd-701173477f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "7ab3376e-a1fc-4e24-be17-17f7f74ce48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "7d396cf5-74c7-4c02-9f7e-1284329d33d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "8cfee696-cd4d-4b88-898d-e4638016266c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "c7131775-d7bb-4cc2-92ba-2cb1ce9be0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "cc679874-5f67-40e7-9c2e-76fd510d7806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "f5198421-caa6-4698-a1ee-12798a005e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 90
        },
        {
            "id": "7b0e9d89-8e23-4407-a94b-64da5bd38b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 92
        },
        {
            "id": "a89094b3-2767-4720-8665-3e8ea69a3086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 93
        },
        {
            "id": "f1208e70-99a8-4499-a884-4f7bb1d0820f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "e035e2c9-b2e1-41fd-aa2f-281b10b033a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "66f9e1a2-7edb-4463-ba70-2e7ca41dae16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "cbbec5d6-a28a-478f-892f-4b4bee28c7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "59bd3187-2aee-4b19-b76e-c96a51888aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 115
        },
        {
            "id": "38a109c1-b529-4505-a9c8-50eb5a07d4b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "1d6d58d2-074f-4d1e-846b-909c91a55186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "ae007631-b1b6-4f46-9078-31fd46abdfc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 120
        },
        {
            "id": "2fa87034-ccb9-44a8-905e-9149255b3277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "fba638d1-e1e6-4100-9a9d-a477735c5548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 122
        },
        {
            "id": "43abde39-fdd2-4c3b-884f-3e0ca08a6b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 894
        },
        {
            "id": "f4a815b1-f1fe-4851-8a1e-451f2aeb968a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "0ba6692a-62de-41fd-a8c4-c31253164060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "658951de-298b-41d7-9a9c-a02273cbdf49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 52
        },
        {
            "id": "ff42b9c2-989f-4e1b-bab2-5082e30f8f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 55
        },
        {
            "id": "162c49fb-95c2-4fcc-adc2-5019136e194e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "8c667303-50bf-4a38-9ef3-0a653b1d6622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 84
        },
        {
            "id": "6f69f65b-e636-44e4-a353-6592607f8024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 87
        },
        {
            "id": "02716c7e-fdd9-4f9e-ae49-843b4cc803e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 92
        },
        {
            "id": "8a919a45-8be2-4189-9191-123dc716f4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "41660921-8b8b-43c2-b6ad-24f729b7e0f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "19b48bd3-1a52-41d0-8975-0e292ffbab53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "d66ec06b-afc0-40aa-a728-bcbdd937554c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "64ee6122-b8f1-4994-b342-d19bff6c7409",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 40
        },
        {
            "id": "f5a5fb49-3d9c-479b-b0e3-2a6002ecbc71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 41
        },
        {
            "id": "e47537c8-8cec-437b-8d18-36ba01b7f683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "e9720b78-8e0e-47f0-8519-ad0531d383a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 47
        },
        {
            "id": "270a78a1-c626-4bfb-ab10-6b8f05bde2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 49
        },
        {
            "id": "2f249e03-d006-4fad-9116-4d71037ed331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 50
        },
        {
            "id": "bc2fedd0-02ce-49ba-a7f8-24c35d699432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 51
        },
        {
            "id": "bca79700-1b81-428a-805c-f8084f431bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "f6e4747f-a141-499c-b4f4-567f7241e97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 55
        },
        {
            "id": "e7702ec1-65ca-4d52-9c89-4467b238e332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "a6954a19-d07d-4f59-b3b7-fa56ff9ac3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 58
        },
        {
            "id": "9629b765-07f0-4173-a08d-9b41e31c16de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 59
        },
        {
            "id": "2ba287d8-2d56-44af-b32c-756d8a90986b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 63
        },
        {
            "id": "031e0cdb-30cc-4a17-89df-7573938b0791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 74
        },
        {
            "id": "d490431b-7927-4af3-8ac8-23a24dd91f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 84
        },
        {
            "id": "62134602-f951-4cc8-bb88-8afa118b2b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 87
        },
        {
            "id": "1cd9f649-c693-460d-b9bc-87c153d94e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 90
        },
        {
            "id": "455d4b35-9bc4-440f-acf3-c73289b8949c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 92
        },
        {
            "id": "f71451c7-539d-4461-99cf-5338c8211bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 100
        },
        {
            "id": "b806ee5f-dd59-4063-b181-95d00caad3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 103
        },
        {
            "id": "9026c13d-25b2-45e8-9aa4-60c8bd7b53d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 106
        },
        {
            "id": "1a6c9cd4-42b2-46cc-a578-217826bb3395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 113
        },
        {
            "id": "5ddba228-fef9-4aed-9780-2ebc8557b082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "7cbe1c2c-decb-4f8d-9388-a1e20496fa10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "a060ef5e-9b95-4e9a-b1f2-4c1b4f979fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 120
        },
        {
            "id": "59ad1a56-9d32-4a5b-8055-fd9f7f4ba6b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "be0a04da-4b4f-4db6-9824-6bf9302a2970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 122
        },
        {
            "id": "8a030c27-0d2c-42a0-9f48-a13a24f19dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "f121405c-a443-4493-9626-98a92dd74881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 894
        },
        {
            "id": "76922e47-ef22-48be-b46d-6bfc4dcc29ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "a4b4776a-03bd-4220-89d9-2c6b9a174496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 34
        },
        {
            "id": "7421bb0c-4b71-4f0f-9c13-84e9fe267bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 35
        },
        {
            "id": "af313500-4cb9-47d8-aeed-4ecadee7d3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 39
        },
        {
            "id": "ecc567a2-eb69-4803-bb35-2661fa5058f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 40
        },
        {
            "id": "4defd58a-db90-4ead-8e18-0fa297772f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 41
        },
        {
            "id": "e82f05ba-4c22-4bc9-8262-105552e0d1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 47
        },
        {
            "id": "cbe442fd-77f3-49ab-bea2-f6ea2d0be647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 49
        },
        {
            "id": "100c667e-6032-4a4e-baf3-3db4b68eea82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 50
        },
        {
            "id": "b3d33c40-fab4-43fd-9e3c-d1c23036ad8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 51
        },
        {
            "id": "3af10762-3553-44ff-80d6-06277d7db251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 52
        },
        {
            "id": "9cf09bef-cabb-48d4-b322-e7d0272f94d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 55
        },
        {
            "id": "9bc96767-b7e8-41d9-b21a-6462e46971df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 57
        },
        {
            "id": "703bc3a5-dc0a-4daf-bffe-3dd2e0953523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 63
        },
        {
            "id": "b47eb3f9-34cc-4320-b22b-66641ce7968e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 74
        },
        {
            "id": "70ebf53e-66f8-4108-8f01-2a0e274bb368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 83
        },
        {
            "id": "373f63a8-f538-4df8-ac6f-ec8a9d8e8aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 84
        },
        {
            "id": "4518995d-5364-4545-a121-8bad20a71e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "280d5e76-5829-448e-8395-73ff1be6215a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 88
        },
        {
            "id": "06157ee3-ef98-4682-94d7-99ebf9329e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "92e91150-c247-486f-bb29-f5ea37784c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 90
        },
        {
            "id": "b424925f-0d59-4d37-9248-4727a58fd004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 92
        },
        {
            "id": "90c2c4e8-5127-41ed-9874-302d7ffbb3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 93
        },
        {
            "id": "3dd98930-c305-4e0d-9b18-71709dccc14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 100
        },
        {
            "id": "5658f9e6-0073-4298-888f-43d5366a7aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 103
        },
        {
            "id": "b29686b1-0254-4792-a3dc-dc3d181286cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 106
        },
        {
            "id": "32a75282-9767-4d7b-8ea3-0a5fb771ef9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 119
        },
        {
            "id": "97e5d1e1-0309-4510-847c-a9e869c30aab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 47
        },
        {
            "id": "1efd0373-ecf5-4c04-9104-e304e1103bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 51
        },
        {
            "id": "7c943b41-4d6b-4446-80a5-460136348e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 52
        },
        {
            "id": "b365c214-d7e2-465f-92ed-199cbdd39497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 55
        },
        {
            "id": "078f810a-4337-4e70-a881-49dc77039a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 74
        },
        {
            "id": "9aa55ed5-8681-4bbd-8eb4-67dac8219efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 84
        },
        {
            "id": "ff41edb5-26a5-4b3f-a550-a0d0217d13d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 87
        },
        {
            "id": "2b9b9206-e5f3-47f7-9c13-66aef981b0ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 92
        },
        {
            "id": "f8fa3b94-b58c-41e9-ad08-61f9f55c8205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 100
        },
        {
            "id": "1dbbcfca-3755-436e-a027-638f9fddf640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 103
        },
        {
            "id": "c6e8a315-f532-464c-85bc-f3ffe740e7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 106
        },
        {
            "id": "158f4de5-e19a-4d72-b361-19a5df95e396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 119
        },
        {
            "id": "e7f6c605-ed58-4419-854b-8ecbc790911f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "f444bc14-754c-409a-abb2-11d9cebb0c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 34
        },
        {
            "id": "96cbe659-2463-49a8-be7d-03b3faa7238c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 35
        },
        {
            "id": "8297e146-00e8-41b8-99db-90df9ff59c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "edf40489-3674-4193-92c4-61ffec766f1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 40
        },
        {
            "id": "c7e4cc2a-7990-4c78-ba0f-44e877309a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "943eb0c8-f18b-4446-a214-6f249efef778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "2f0338e8-48cf-43dc-8637-707d2a7f396a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 47
        },
        {
            "id": "3d462eda-feb6-4caf-bac9-42b0627de040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 49
        },
        {
            "id": "9f3268ad-1111-4596-b54f-823c119ace2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "18b4a534-a2d0-4ca2-8ab9-fcfbf98936c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "6ddab14e-02c8-480f-ba41-032360a40572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 52
        },
        {
            "id": "c216f114-edb8-405b-b6f6-b486d0462051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "ce2cf1e0-58c2-43b2-b787-4c3ae2de4be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 55
        },
        {
            "id": "4046a996-70a1-4e7f-88dc-a710b303a3f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 57
        },
        {
            "id": "ca59e35f-6193-45e1-aa29-6cae6a3b1595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "af9a202a-9a75-430d-9861-03a4e37fffa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "29abe3ed-0b65-4bc8-94ff-554984d2357e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "00257d10-1c32-4746-bb86-3d039eab53f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 92
        },
        {
            "id": "a9f20147-6c31-4785-aa41-7775a3e6d34f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "2fb54a7b-1b5f-442a-a25d-042e8f62ac6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "1430f0f5-5a50-4105-95cb-2c4fcc6d16d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "0defde9c-1d5c-487e-9e06-65307414f0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "4ba6d1ff-8a92-4ce2-b275-63e6b151b6d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "0b9ee6e1-1bb3-4ca1-8fd5-5223424219be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "279337dc-8175-4890-ad9c-6e877a8a1c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "32b9c188-7d3e-4deb-a793-08fa07720360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "ec2a0036-a6e7-4e14-a236-511ac0f71331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "019e0b26-3c9a-46d4-b744-482e7508c880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "fe95095b-62c0-49ed-b6a8-651c9dfb3c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 173
        },
        {
            "id": "0dfa197f-0227-420b-884b-efb4792bc919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 894
        },
        {
            "id": "3a558772-2b56-4980-96fd-158b42c233d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8208
        },
        {
            "id": "14098cc2-59d7-4c0c-9268-7f080ef92965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "dcd78e76-5de1-4396-857b-4ee23e616ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "150f6783-b629-465c-aded-ac65ed2715d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 35
        },
        {
            "id": "95ad70e0-12fe-48c3-b66a-23aa94417e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "164d5940-edbb-46d3-9394-af564d6696fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 40
        },
        {
            "id": "e584a011-d24f-4ff9-8f9d-d71e92cc2444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "30935146-d176-44d6-8967-353687323133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "800c9a3f-753e-4366-961f-de2770ea915c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 48
        },
        {
            "id": "6dc74819-efac-4301-89b5-f71b4a7f8188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "e9d699e3-5fcf-4bb6-9807-6355c9df5c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "c8ac1b65-dd36-4216-8dc5-b756b89d7dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "4afb55a3-2647-4f19-bc15-31c36f4f2dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 52
        },
        {
            "id": "2cd2ec81-1246-4709-8b3d-6a000b31d82e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 53
        },
        {
            "id": "e8d9b134-6047-49ae-9c53-3560bf65d639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 54
        },
        {
            "id": "5bea67ad-b778-4e9c-a1ce-de6ff17a1c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "cd507cb6-df55-4dc7-a098-a5e6ef81342c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 56
        },
        {
            "id": "5356c4dd-039c-4318-aff3-13447a4089d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 57
        },
        {
            "id": "b1a6dc32-f90d-4b50-9c36-be1cd54acf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "1a3e232c-93a1-4dd9-ba09-3a47ee6e98a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "d6452c48-cc6f-4d14-951e-b47e8321c923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "1de35f91-8a05-4cab-b0ae-468954d81901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "b78d9052-1453-4c1a-958c-dfbda448e5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "0849d9f2-c8b8-495f-8d49-f9993825f91e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 100
        },
        {
            "id": "6703f971-94bc-4e05-8725-3710578feeed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 102
        },
        {
            "id": "eb558807-5aac-4b78-bde9-af3d5c20c087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 103
        },
        {
            "id": "e114f3ff-3379-4e97-b630-29cba9185f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "5100a0c6-6533-4c38-82d3-9913a011d372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "a587cb23-644f-4eea-b4c4-b7a602331575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "b0bb2501-119a-438d-8ea7-5579e180ed58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "298c0b17-2f20-4a64-8f3f-8b1a2e01a067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "9eda3fed-8713-4836-86c6-9b5aac018041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 113
        },
        {
            "id": "b7b928dc-b5c2-4585-af4a-d00ee9d0b13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "fb0cd639-7f01-4837-8166-c298fe072872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "0e186c09-a96b-4d33-9d99-9b88c05f4302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "1624e8f1-bb2c-4109-9076-3bfa0b0fb64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 41
        },
        {
            "id": "9193fa30-cc62-474d-ac94-08fe6550dc0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 45
        },
        {
            "id": "fb0fb735-4cc4-4413-8af1-f32ddc9110c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 51
        },
        {
            "id": "8f0099c1-b467-40c4-bb0f-b191f09bfc85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "9b6826f8-abc7-48e4-abd2-721167597e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 55
        },
        {
            "id": "45d2aa64-5434-4adf-9569-271e0731deb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "801b1ef5-42a2-49d8-b192-4947183980f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 92
        },
        {
            "id": "235ed644-86e1-4cce-9f64-c4ff3c8947e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "c04e5e06-7cbd-42c1-80bd-b4cec61dd316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "a88b9953-6883-4003-a107-0b6129b516bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 173
        },
        {
            "id": "d74fd78f-1c3f-47b8-a9e2-bb6b55296762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 8208
        },
        {
            "id": "383ffaa5-0c22-4a01-8b49-d9af0836f3d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "a15ccaa5-d3ca-479c-b1e3-3a3a4121bb8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "b3128d3a-636f-4ff5-b488-f2d8e0836b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "c3e59777-e4a4-454c-8f15-a8f5e1267184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "dd5b7ac9-7687-4fcf-b14f-4b0d71fb6217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "aa53395b-6234-4bd1-a9f8-1b7031bdb2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "2fc54ac7-4770-48ab-914e-ee7b81095036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "8c95244a-8ee3-47b4-8b59-628347b84c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "6785fa86-391c-4869-9a74-b3d606553c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "1961d93c-d94c-4a35-8f5c-8f0be61435e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 35
        },
        {
            "id": "7b6b08ce-c892-4863-8dd0-4e67acb415ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 40
        },
        {
            "id": "37a4f49e-6d95-4ec7-b383-19ce414fd0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "4ade1f8a-2931-4df8-ab77-c43763fc2e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "68663c94-78d1-4022-a556-ea92cafa432e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 47
        },
        {
            "id": "c79e9192-d51f-4baf-acf0-0a0a3e2c7039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 49
        },
        {
            "id": "e04b92c6-7bee-4b9d-9a9a-a24084e6b102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 50
        },
        {
            "id": "7593d2ac-b2df-4580-98e2-14d9ceffb0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 51
        },
        {
            "id": "579793eb-e3bf-4e5c-81bd-c90356c4fbcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "597ebf49-2b1c-4168-be08-da09bd572fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 53
        },
        {
            "id": "933d2dbd-ae04-4fd3-8279-4cf002f46d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "6f6c3166-2697-48f2-aa9d-01a3f83a1655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 57
        },
        {
            "id": "c65853fd-b04e-4c14-8568-87fad478a647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 58
        },
        {
            "id": "cfd17b7c-5738-428c-a5cc-58cafa7a0e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 59
        },
        {
            "id": "ac531012-c3e4-4eaf-ba05-467e44ffd812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "c249cd8e-546c-4615-bec2-4a9ed30c89e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 92
        },
        {
            "id": "b6cc96be-7af6-4e1a-9e78-5180ead9b5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "e122bacd-8b04-4fd9-901b-8ca0ca3df04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 100
        },
        {
            "id": "0bf101f2-8600-4846-9341-39d25fccd4c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "d328083e-2942-4a50-9804-89255e39fc78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "7c5dcc76-cd29-4fe7-85d2-a00f8ed35ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 113
        },
        {
            "id": "0e63639f-8c30-4fa7-af6c-468632e015aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 115
        },
        {
            "id": "70a22326-9397-42f5-9eb6-abeef4851963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "216dcd55-4fd5-4e8e-bfaa-ee8eaf3d22c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "4eca09c7-3b22-4cf7-9173-27586357e4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "fcb83b31-ca37-4a01-91a5-d3896b736bb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "d6cf7e2c-863c-445a-96ec-302443a3ea53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "9cd12216-1108-48a0-9820-927798750e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 173
        },
        {
            "id": "f02b00c2-a75d-45ce-81ce-8921b9e8efbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 894
        },
        {
            "id": "976198e0-6cab-4a63-b5c3-f86ec930970e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8208
        },
        {
            "id": "8e8ca747-252e-4b54-931b-7aeefae83c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 34
        },
        {
            "id": "021e9b2b-c585-4fdb-b251-cf557989fd91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 39
        },
        {
            "id": "1d28f6c2-f27f-41f9-acd8-0ba5b503fb22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 40
        },
        {
            "id": "9d1f3f20-76f0-458e-966c-a039b359e8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 41
        },
        {
            "id": "71d2e53a-31fc-4783-8ada-03e360b47045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "0f7729b4-1160-4086-909b-43bd497b379a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "af30614a-bcbb-4a48-a5be-6815c449efee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "8e8e1733-06ca-436c-aba4-3c188d766208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 49
        },
        {
            "id": "9943e408-3294-4fd1-a7dd-35f961e88be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 51
        },
        {
            "id": "ce415b9b-1514-4b73-91b2-ca5b259ded29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "ed6d5539-306b-42db-9714-cc1a1ff21932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 55
        },
        {
            "id": "bbdf7259-8519-4ad5-b81a-6799158ccdba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 59
        },
        {
            "id": "f8cd6447-3348-4614-b440-2b8516ce0850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 92
        },
        {
            "id": "0e29269d-f35b-4e98-8fcb-d5d7538a1518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 93
        },
        {
            "id": "9e1eb220-dc79-432d-b883-2f045713d6ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "baa24c8e-face-4e54-82dc-5ae3b41151f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "fd989a66-fe2c-4197-9dc5-d23bcc3edde5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 106
        },
        {
            "id": "b7cf8be3-4942-458c-9d83-d0907829cb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "72301752-6f1e-4782-8abe-a17d5cd0c57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "04685e5f-ef7f-4307-9645-d4dc3dcb3954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 894
        },
        {
            "id": "38425fef-e8fa-4c6f-a228-d5e3d0338547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 47
        },
        {
            "id": "b8facd8b-716f-4b43-add4-ff376782820b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 51
        },
        {
            "id": "036ce339-6e3a-40ce-b985-1269aa8dab40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "3e8e22d7-3707-4205-8282-fd3100fc817b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "7eee49f9-dd7a-45b7-95fd-99aa1143b2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 92
        },
        {
            "id": "f7e71680-2518-49bd-9ce5-2d640f592480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 100
        },
        {
            "id": "302deca3-3b77-4366-a2d7-118782e12183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "3b908a97-71c9-4c84-851c-ffbdb6abf4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 113
        },
        {
            "id": "25fe5ca3-42d1-4568-aae7-29e92865f7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "e2c11080-f10b-4917-a294-ef4cfd57b716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "194cb264-9615-4897-bc6b-9fb6840bf181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "d9f168f3-d47b-4022-868a-fea2765d8661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 35
        },
        {
            "id": "18722d7c-3748-41e5-b195-a24eac002af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "d234c48c-0091-47ea-bae5-f6d703f32934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "3e1d08d6-5f07-4488-9498-e23138840aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "062f655c-ec10-46b4-9ba6-4c2dc6f6a214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "ae908cf6-fe4f-423b-8f3f-0fa9899b225e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "cc3b1bad-ed19-438a-b81d-35e0a4cac0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "e5ddc236-757f-4dfd-bd3b-a4fbaccf2719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "7d75c059-e37b-4cdf-9d0a-2a0955ce9e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "a5260f68-48f5-457a-9742-9fb063109d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "7f1caa8a-877c-4a1f-96c3-70ab451f0207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "35f0582b-47fe-4aca-802b-2500e73b1436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "c6ed967c-bfe4-448e-9436-787ec79bc5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "faefa847-52bb-4e5c-a184-05fd0545afc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "d7c9f608-5aa1-4b8e-a899-2be369f5601c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "bd6bce3d-1890-4f9c-bc37-bb78d8c93e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "e7d8592f-bf5b-4e98-9645-7bf4c121fb59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 64
        },
        {
            "id": "f02f76b0-2fc7-40c0-b4d1-6f058a27a012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "f2014bc2-692c-499d-b016-042ca6054211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 93
        },
        {
            "id": "320b0f42-fed3-449e-8425-318e5564fa72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "4e7aef12-0c07-4f5a-9efa-683153675d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "397fb9f1-a1ce-4ef4-ac23-b7baf95a9d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "829600c6-d5cd-4eb4-80f4-6a164a739956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "e5bca373-e03f-4e8a-8e97-e68e8e70f37f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 104
        },
        {
            "id": "efc6a771-3017-49bc-96d8-d9bf76ba4ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "2bb2562f-51c5-4b01-9c3a-5a3c8cf45d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "5e647fce-354c-4a3c-b04b-d240e9f5da58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 107
        },
        {
            "id": "1ea87c9e-ecfa-4ee8-8664-b9a3baae3c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 108
        },
        {
            "id": "fe8160a6-8ba6-4b3c-b1d0-5420e87df3aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "a585bd0f-9169-4363-b636-d1c8660e0978",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "d703a3f6-f184-41c7-b864-05110bcc6cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "3aa71a69-98e4-4a45-a174-873f2ed6ef49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 120
        },
        {
            "id": "d0c0a661-fcad-401c-a38e-f2127b9c6885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 40
        },
        {
            "id": "14985d06-ef07-4d48-abdc-2f7c2cb090a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 41
        },
        {
            "id": "f9fc9bc2-4777-4f65-a620-de8539e96016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 47
        },
        {
            "id": "e11770bf-efa7-47bf-a0b2-cb4736d7b417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 49
        },
        {
            "id": "58e7c9a3-a999-4317-99f8-2d0e1bef4011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 51
        },
        {
            "id": "358783f3-6c7f-4285-aadc-f9ed3fda392b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 52
        },
        {
            "id": "e96a8b65-7c33-473f-bb9b-77c1a2aba407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "5fca3b5c-1280-4507-9b21-d2722e0953d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 92
        },
        {
            "id": "26113aac-415c-4705-b075-74e6743534fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 100
        },
        {
            "id": "86c4b722-fb8f-4a7f-930e-7828a0a3031f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 103
        },
        {
            "id": "401983fd-fb31-493b-900b-65dfefe1a940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "8ea6d74a-a135-4eb8-9093-1e3b5b50f1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 119
        },
        {
            "id": "de8fad60-5b2c-43f3-a558-9d359687069f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 34
        },
        {
            "id": "a5ed0c09-5f59-49e0-a182-6f852469f1f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "ad61b66e-6509-40ad-9c06-5e397191c650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 40
        },
        {
            "id": "41cb251b-d515-4a33-8089-7dbe198686ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 41
        },
        {
            "id": "96585220-f342-4c3b-a6ef-0d61cb990b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 47
        },
        {
            "id": "9fdbb6b4-f613-4560-9801-e670e94be05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 49
        },
        {
            "id": "fd426f0d-7791-41c1-9b4a-b94a1ed83d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 51
        },
        {
            "id": "31390b09-e7f1-4f97-89e9-2458e121df32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 52
        },
        {
            "id": "d3cb5e76-3058-4f84-b13a-34c22af320a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "b603dd41-6409-4966-a3b8-edfa7150cbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 58
        },
        {
            "id": "8c8766a3-899b-4b2e-988e-eb8d3cc33b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 59
        },
        {
            "id": "5ef7436f-0f3b-4bbf-a4eb-bfbea53cd35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "a685ba8d-d477-44a4-a2cc-8030323b1e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 92
        },
        {
            "id": "94b81362-d881-4957-a121-e3e91c484f91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 100
        },
        {
            "id": "a3f67b2b-07ad-4585-bdaf-43b38a6f540f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 103
        },
        {
            "id": "91131876-ba52-43e9-92ea-f6b6e8194ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "923cb8c5-7dd8-40fb-a8b4-cdda193e07ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 113
        },
        {
            "id": "efe78b80-e34f-4252-ac83-d8a0258745c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 119
        },
        {
            "id": "6cd90e50-ac47-4f2a-b51c-46b426be7035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 894
        },
        {
            "id": "28acb975-de37-4ba6-b947-75ac352a6d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "1ec3717c-f8b1-45dd-93d9-ae9d08bfdceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 34
        },
        {
            "id": "47ccdbce-7c18-4a05-b074-1923c3b30a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 35
        },
        {
            "id": "87a356c7-4a58-4aec-8b12-c5744a0e91e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "ffac8f98-0c9b-486d-bd26-0eaf5a1afd5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 40
        },
        {
            "id": "3050e422-28ae-4806-8365-b8da36f739a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 41
        },
        {
            "id": "f7f36b50-f914-4fd2-a1db-24cd7cf3392b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 47
        },
        {
            "id": "ffcd870a-f9fd-402b-8562-4ee7e7023411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "a365ae43-de89-4498-9306-b96e66f8562b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 50
        },
        {
            "id": "d89c673d-4875-4f09-8168-9689a39b8158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 51
        },
        {
            "id": "2f473f39-86ae-4330-ad15-6e8375d0bee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "ea62971e-97bf-4d0a-b3b5-963bf53ab63b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "ea691eb1-5a7c-408f-a13f-3735e15d723d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "056962b9-8ac7-411d-9ad1-5d73e551864f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "f621ade5-e7e9-472e-8519-2221a7f203b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "72163a48-dc3e-4b38-a914-6bd7fb3f4895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "65d31cd6-f581-4d50-a98a-eedd51205aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 64
        },
        {
            "id": "8471fc30-0e0c-4d3b-9fa3-bebf8aa354dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "94fa7e16-ae16-48ec-baf6-dda39d0283be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "92a88a74-b18f-424c-b8a1-46e91afdf5fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "cf8e99a8-879e-415a-9a9e-6feb9a5cba05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "17f30ba0-e1e6-48e8-9fc8-2c7e937e2776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "5f6dc2e2-c5d8-4801-9630-8058061a6e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "f8d8afc1-a40a-497a-b8e3-68e3899cd28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "1c0f064c-9205-47a1-8de8-f7a6538e8900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 33
        },
        {
            "id": "9f4b895e-0fda-4d4e-83ef-ba27b4bfd59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 34
        },
        {
            "id": "492e80dd-b9cc-49b9-a527-0048f60b820b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 35
        },
        {
            "id": "6c87f53e-d47e-46dc-af40-4fa6f0e5ef9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 39
        },
        {
            "id": "c9f7f2d3-dade-46b4-803a-9874cbee5c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 40
        },
        {
            "id": "4b7dfd3f-5ba0-45b4-b8e5-5a501de192e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 41
        },
        {
            "id": "c9d30d0c-39f6-4eb2-87d9-98dad3b591a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "79093184-f713-463f-98e6-913aaf50f97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 47
        },
        {
            "id": "baacca93-bfa3-4a66-bbd7-b2ee46c90d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 49
        },
        {
            "id": "c902ffa9-74e4-45ef-9e2c-83aac628cc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 50
        },
        {
            "id": "2d60c1d7-596e-425d-b522-cc9a1a8f48b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 51
        },
        {
            "id": "8327dcba-7989-46ae-bb25-f185c99413cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 52
        },
        {
            "id": "45fd9af7-5d01-4a68-bbf2-720305c6e898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "c5109bee-8b18-4a9a-a098-5a87c156e8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 55
        },
        {
            "id": "7a2da0ce-f123-473c-a17b-db00420b6a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 56
        },
        {
            "id": "32f017f2-7004-4196-9d84-0df0e7888a60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 57
        },
        {
            "id": "5cbe7be8-ab9d-4a9e-a199-92c0bf650bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "c7b453ef-8871-44cc-b935-63159da347ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "04251d51-dcc9-4364-9954-fa97b18b0c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "f74774d7-4790-41c2-98f9-2446f5cba72a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "805d8ec3-3be7-4c16-a007-b15f3d3c05c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 92
        },
        {
            "id": "4530c320-0cc3-4885-a72a-71a2bdafe582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 93
        },
        {
            "id": "a300257e-0022-44da-ad40-3680385668a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 100
        },
        {
            "id": "30e1f7d5-d073-4864-b21c-1dc50b895827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 103
        },
        {
            "id": "ddb7261a-7ed9-4ff0-a75f-5594b0064690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "58250cd5-4dfe-4126-be0d-d03a7fffde8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "9cc404b6-2a6e-4978-8fc9-898f9839df71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 113
        },
        {
            "id": "9a79b009-9e3f-41be-8093-0f7df6e011f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "ffaeda9d-b654-4d41-8867-b64437c552f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "a50d3ce7-0481-4ee1-918a-bd4be30db92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "0e46d3e5-0002-4969-8de7-73c397022cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 120
        },
        {
            "id": "889e2b1c-4112-4485-967a-4c3e820bcd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "bfca8552-8f5a-452b-9cd7-b1b7a3b2fa9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 122
        },
        {
            "id": "22082cc7-4efb-481a-ab6f-f203745213d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "25534045-8d95-403b-88c4-de9fa0ced079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 894
        },
        {
            "id": "2c8b6b66-0716-4627-b2e1-2cec29c11adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8208
        },
        {
            "id": "8a3476e8-0292-4b97-ac9a-169d97f32958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 33
        },
        {
            "id": "cb246000-8393-4b61-8120-b9325672e967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 34
        },
        {
            "id": "a80ccbca-6265-4172-8053-ef1fe3b7f8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 35
        },
        {
            "id": "e0edbca5-0022-4e5c-a329-14d84ba4ca75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 39
        },
        {
            "id": "bf0b4e45-cefd-4d4e-ba9e-caca92bd1d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "6aff9059-d36a-43ab-a8ae-9fc7bd7672ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "b5338e4b-8e05-4b14-81ad-827e6404a97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "271f9094-590b-4c97-944c-0788d6164a8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "0bfa8414-bdfb-45a9-a06d-92c896e339d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "a2808981-e83d-4d3c-90a8-fe18937d228c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 50
        },
        {
            "id": "1f60c87f-b943-4bc0-8b07-45bc466c58c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "f14639ef-777a-4fed-a4ec-370164168292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "a8feed9e-5e9d-45a1-a4d1-d586bbe4bed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "db3ca784-5b79-459e-99f8-1b0cfab846e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "da84fd33-8883-43d4-9671-c80e2cebed6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 55
        },
        {
            "id": "34738d9a-ee06-4b76-b955-0253c599ed0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "ac8f5126-8ac2-4adf-bcc7-263791801424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "df51237a-8aa4-4abc-85cf-7b0ccb95c578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "c2d8c57d-d474-4416-a9c8-bda9cb2ef5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 64
        },
        {
            "id": "12dfe212-59a1-4458-9e40-8e932ff301e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 92
        },
        {
            "id": "617c1de5-2c33-4339-b8cc-cee1e638cb3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 93
        },
        {
            "id": "63156c19-e424-493e-b6f0-6bfd86128727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 98
        },
        {
            "id": "155aa75d-51cc-4595-9c9f-9d6502e3ae83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "5f0f7943-edc4-4f36-a595-7ac05026db85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "77eb5135-9207-4047-99c9-86de7f8b3478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "3daa2abe-9379-44e0-8a89-fd047b2a0074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "6a6f73ef-9aeb-4ed5-85c3-7ee22a1fc736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 104
        },
        {
            "id": "e2831d00-6a32-43c9-b7d4-cbef8e1fcf2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 105
        },
        {
            "id": "770816ac-4278-46f0-81b9-65058eff6b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "2e036fa5-f0e7-474d-a2cb-07ff798cc969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 107
        },
        {
            "id": "ea31f578-a474-4b77-b5ca-72cfaf3d2811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 108
        },
        {
            "id": "a9c96716-19ce-4a5c-84b8-e33dbc91a7f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 109
        },
        {
            "id": "e30b402c-86f9-4bb8-b75a-978b1004663a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "d0440ef3-d4ac-4705-87c1-bdb049bd5e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 112
        },
        {
            "id": "c772c820-14e0-4b58-a5e3-dd263e29b31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "3f4d2e57-3a2e-4ac4-b044-49ddcf28b328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "76a9b8d0-9cca-41f6-8599-54a2ada8e0a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "ac709f4e-3086-4778-b447-c6bc96a73f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 119
        },
        {
            "id": "fcaaec32-ecf1-4782-bcfb-4ba518d305a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "bd6d86b6-f750-40d6-bcf3-96d3d9f77693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "c9bd0821-f4f9-4e24-b91a-27c8e2ac0c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 35
        },
        {
            "id": "03e753d8-8d94-401e-9ebf-cdc6b6b93c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "77b7f8bd-efcc-4488-ad74-424ccd177189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "12b6a994-e20e-487f-85d6-04e21097269d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "912bad42-889a-4db0-bda4-a3ca9588c95a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "607f0555-4938-4db6-9f20-b3649967f26e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "9b3a8956-7124-4ad2-ab50-89cd85e8a4f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "2ad09318-2a43-4bdf-be1c-7118eff5d5aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "31377394-8dfb-4c69-8219-e3d12c1bd1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "fcef7df0-e3aa-470b-b0d9-3e9b34aa38be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "cd72790a-b714-42b3-85b0-4e9c1aebc849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "cb2257c2-b25c-4ca3-8149-fe02ecf62ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "35ffb42a-3bb2-4cdf-8483-4813f9f3da97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "7bb96854-d0ed-4e59-953f-1e1bc7d158b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "e5aea271-8db2-4f31-9f20-98a5e27cdc5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "65112170-c33a-4ea0-a520-8bdd4eaf27e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "77da6f5d-17ce-4d30-b056-64fbb9d7168b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "b9f7c0bd-8498-4f56-8cd5-aca6a570a478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "2eb7a29d-579a-4f09-a7b2-6aecb28ffb26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "14f4e96e-682b-4767-bf51-ac5c648b6aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "2ac88c98-d28b-4ae5-848d-719c63011e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "a7fc2f2e-33f7-4f2e-bb8f-c58c8f77e1cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 120
        },
        {
            "id": "e0ff1950-5e31-479c-a647-38e7dc5917a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "2d5af0e8-c7bf-4822-8f21-4ff48e167c24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 51
        },
        {
            "id": "3caf969d-bdc8-4836-a914-cfc956d07c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 52
        },
        {
            "id": "2132cffa-12cd-4e5a-a159-264a7918f89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "f887fe3b-f079-4ee3-8f7a-6c48c55fd8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 92
        },
        {
            "id": "e283a83e-9ee9-45e7-ae52-6c35c1e595dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "cfb2ac19-eea8-454b-9be5-896b229c2fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "23b7b6a7-dd2b-4466-b94c-6dc214ff9186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "d3ce0260-951b-4a23-b68a-a865f6836d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 35
        },
        {
            "id": "7e793a1a-184f-476b-9ba7-df49e30a8179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "40612558-b53b-44c9-9bf3-e7baa60f5991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 40
        },
        {
            "id": "38a81b76-97d5-4713-ba59-84cdc51baed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "74ac86bf-eca4-43bf-8e7e-75d80cb0c196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "3c6008c0-cbd5-41e5-8250-d28d80d715c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 47
        },
        {
            "id": "6d81857d-fc84-4923-9ff8-87bca91aaefb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "4524add2-3164-4add-bad3-678f7b0bc07b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "b7fc010a-fbd5-4686-a37c-5989a04b3d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "605e750e-b9b4-4c48-baf5-f2a1ea376415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 52
        },
        {
            "id": "ad35f61a-bb4c-4910-b299-de00dc5d4b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 53
        },
        {
            "id": "3e75343e-18c1-4ed0-b335-a73108879d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "e2d52884-5452-477b-97d5-67d5be68bd76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 57
        },
        {
            "id": "4b641093-2705-43c5-8a7b-0f6568d7edc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "16cbe856-9ae3-4a8e-a48d-c7adffb975d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "4bab48a1-7fc2-4b61-9a57-2cbdc01bf76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "e2896dda-0f33-4e36-8d74-e5465fc930e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "a52c6949-15e1-427b-9104-e54648552926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 100
        },
        {
            "id": "1627eb85-aa3b-4648-8c7f-fe59de59353c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "2339b81e-2715-4856-809d-a9a4cd8a432f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 103
        },
        {
            "id": "52eda427-acc3-40e4-83a9-9f2fdbdfcb6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "b0939f55-31ff-44be-9217-5795416c7088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "c3d296ee-8b1e-4c9f-9f92-1145c4f61e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 113
        },
        {
            "id": "1f433372-08de-4a05-8735-2d930699e475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 115
        },
        {
            "id": "43ef6374-43c0-46d0-a02f-93954fe37b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "c33bd639-bc8f-42b1-b120-1667a513d487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "30d8f3cd-bb16-4d46-a064-f857fce05a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "e942700f-a280-4807-bcea-c5210ba3b338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 894
        },
        {
            "id": "7e1c88c4-50ed-4ea4-a3a0-71358261a700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 41
        },
        {
            "id": "646640d3-af20-4659-ab07-a848e044f90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 51
        },
        {
            "id": "9f4a6b15-f870-4755-a06a-9699558b2e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 52
        },
        {
            "id": "a357eab4-a20f-4e02-b95e-24b5a71534a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "7916d00a-a668-4a13-9bf3-31d3dc9f12d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 92
        },
        {
            "id": "39be816c-9d49-40d3-a079-c1aa160184c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 106
        },
        {
            "id": "9016416c-1f9c-485e-b239-8aeff56a17b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 113
        },
        {
            "id": "207424be-a5e1-4dd7-82a0-84e510ddc573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "7e61f7c1-1a9c-4089-b0e7-0466404e7134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 33
        },
        {
            "id": "58f733ee-5bad-456e-99b7-e3d992426387",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 35
        },
        {
            "id": "61079fc1-26b8-428c-87b3-aca829e5b182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 40
        },
        {
            "id": "bf4a1a97-7812-4e6c-9fa7-e6a611df5e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 41
        },
        {
            "id": "ce1203a9-ee81-48c9-a2c3-ac552db90c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "96198b5f-ff1f-4d15-b7de-298319c220f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "d5dae18e-8d38-44a7-bb18-27a754d59883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "dd960a4a-ffe1-47cb-bb6e-127858f5a0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 49
        },
        {
            "id": "3f2bd686-770a-46cd-938a-9b6549a8a822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "4b94d76f-f351-4bb5-8aa8-544970d7093b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "af3766d5-5c0a-4bd0-9264-1ac843e7fc27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "2e379cbf-e2ed-4a86-9dc4-c346466de666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "b6d9231a-52e7-4e3b-9d45-2895526e4e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "e4aa53be-d302-47de-989d-bbeffb7fe744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "2fb221ff-f530-4565-9c5b-72f0ce313983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "28968f92-3a8f-42aa-a3f2-7b46832a55c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "f1516851-c854-4256-9e06-cad83e8258e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "d19e76d1-a048-48b3-8ebd-11a2396f9613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 92
        },
        {
            "id": "f4eec8c3-aed2-47b8-bbd0-315114416c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "37e33430-bab3-487f-a6b6-3c7c2641bc89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "177b8dbf-5494-43ac-8235-10777e7f1f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "5941a7d9-b95c-465d-bc2a-371e992b6fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "cb0d588b-d258-44bb-826d-0bf240a6ef1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "979cc01d-45cc-4387-833d-8139739dfcec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "c7f9e620-4b52-4495-8d2a-642b7cffcf88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 120
        },
        {
            "id": "43b6ef3c-68db-4c29-9411-1a42e41e0dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 121
        },
        {
            "id": "63d89f03-2576-4749-bce1-1f18269e095f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 122
        },
        {
            "id": "7c7f8485-cd72-4715-a4cd-7ad00d67cea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 894
        },
        {
            "id": "d7378a7e-b53b-4641-b68b-8f7c6030aaee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 33
        },
        {
            "id": "24f6be35-fce6-495a-bac4-4976f83b693d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 34
        },
        {
            "id": "928070e5-5c98-4875-a671-e0ec2eacc1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 35
        },
        {
            "id": "414980c1-d02d-45aa-aa3c-7dc8a8a92c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 39
        },
        {
            "id": "bac7169e-5409-418f-986e-ebd8ee21152f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 40
        },
        {
            "id": "4140ca84-c201-4c18-8e52-7e3818eb3bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 41
        },
        {
            "id": "eaf09509-6a7d-47cd-b2e9-6ebf123ee52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 47
        },
        {
            "id": "b956870e-02f4-46c6-ab3c-2cd7649a051a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 49
        },
        {
            "id": "f881dc8b-e697-423a-ac5e-6ef3a81a1144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 50
        },
        {
            "id": "aa92cdb5-472d-4d44-a70d-06bdf7eb3e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 51
        },
        {
            "id": "18ea2867-7bdd-411b-a0d8-7d42f4bc40a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 52
        },
        {
            "id": "0a50ba22-ccbc-473d-967a-dccb621d203f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 53
        },
        {
            "id": "160bbc54-78c5-4dae-9223-0594b34381b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "3a553364-840a-4c85-98a7-9b8f1be8639b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 56
        },
        {
            "id": "3842829b-7351-486a-b2ce-7f9c1062662d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 57
        },
        {
            "id": "7d1948b9-14b4-4531-9f09-6ce7c2174b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 58
        },
        {
            "id": "0ab200f7-8fb4-4df7-b6e7-007f86c89547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 59
        },
        {
            "id": "14f9eae0-a181-479b-9c04-b47e7c34cff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "fe0f3e21-8013-47ae-ae0a-eedd4ee02b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 92
        },
        {
            "id": "6616be01-2616-497f-ae8d-8a3b5bc71c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 93
        },
        {
            "id": "93041217-3b21-4282-bd41-4e4d8a43b9d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 100
        },
        {
            "id": "ff6f0ed7-bb3d-4aaa-8cd0-017ddf6fcb2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 103
        },
        {
            "id": "9d0c9da1-e2fc-464d-b4da-6df24713b9bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "8aa8bae7-1a56-4b47-b1bc-5451492bcf14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 113
        },
        {
            "id": "d9fe7f8b-5815-492e-a1aa-4b0adef4e67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 115
        },
        {
            "id": "7bc5ba84-560b-4a12-b5a5-d5ffc0dd9c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "6e7af898-74fb-4610-92bc-e78a6daf60e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "3ad82bbe-69cf-4386-956c-71311a4b3f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "b88560aa-fa5f-49bb-8fc9-b795c4a4e335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 121
        },
        {
            "id": "f51d6ade-ce21-4202-8035-2763089a51a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 122
        },
        {
            "id": "20a6f57a-4300-4429-9da1-ac82388ae9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 894
        },
        {
            "id": "3a8e8784-047e-4add-a577-fbbaf83367e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 34
        },
        {
            "id": "fb6a6e93-3746-4eb0-85a2-5dab47978a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 35
        },
        {
            "id": "fb06aa49-b755-47db-8d25-9e6c008ac819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 39
        },
        {
            "id": "85a81e2f-243f-4cf8-9793-7f52d9ff7338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 40
        },
        {
            "id": "399e49c1-8488-419e-a8c4-eef156022aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 41
        },
        {
            "id": "85857780-f23d-4f4c-8b3f-431a765669ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "c1285cef-b693-41ea-9e5c-012201ccec85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "8ca67123-7b2a-427b-80c7-f1f8993ea29c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 49
        },
        {
            "id": "0f29826f-f376-4a45-b302-654fb7165dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 50
        },
        {
            "id": "c2abf9ef-ac73-40a7-822a-ab1582e84699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "aebf57ba-666e-452b-8791-d05c67a4c5ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 52
        },
        {
            "id": "ae37ad08-1e57-491b-b15c-19b6de44fa3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "2375e3a5-2c7e-48d8-95d2-9e022db368a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 57
        },
        {
            "id": "9390b477-98c9-4ff5-982c-b4bfedf4d628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "6e161879-6b1c-4794-95e2-10832509a6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 92
        },
        {
            "id": "b8badbcb-f0dc-4051-96dc-22886ab3f7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 93
        },
        {
            "id": "e3bdae5f-7d3d-4859-b87d-6ca06559b92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "9e8d42b4-2550-45ad-9097-c71b40296015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 103
        },
        {
            "id": "cc2f93f9-e58e-4ce5-b4cd-9a6f9fa03cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "e02bd4db-b14f-4c5e-9515-ec42acc4728b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "044b59e4-a394-40be-9833-2e2de9439356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "75be7424-f76a-47ff-9d6b-8dabd8bd1d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "99af1688-bb86-4ded-8255-138df2581118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "b31187d3-02e7-4dc9-88e2-9078afe69ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "53f882a7-422b-4e08-b7e3-db1d23103332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "aee04b4e-99a2-4835-9aa7-ee3c2fa0f280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "59cc617b-5aae-4918-a21b-03f6669ed251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "38d878b4-9ec3-482d-84fe-1e5b4ed7a9d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "8c722bde-fb97-4a1f-8a85-fb6d208900e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "9f61e0a2-2aa4-4208-8cc9-b1c513c58ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 41
        },
        {
            "id": "a2c110cc-56c1-403c-bcad-c349e9a93efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "6a111436-4278-4e41-b332-eec08ab62249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "2c51a03b-ef5d-4428-a126-2838684927ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "1ac598f1-b98d-42ca-b864-94565434a3ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 49
        },
        {
            "id": "f50142f9-5f8f-49b1-94ac-e4d838a35a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "f4a0e182-6228-4b0f-8e61-67492ce167c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "126fc6fe-f195-453a-b73a-a15aca18dc58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "e6bc6185-72ab-4fa4-9d71-1f66752b1d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 57
        },
        {
            "id": "69998f3c-0899-4945-b89a-00dd5f032cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "34de5825-3a6c-4998-9d18-fc1dc87ae495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "3cdad74f-b2bf-4540-afb2-590905a6bc27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "113fb513-623b-42d7-a13e-d0b363e2a59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 92
        },
        {
            "id": "17f329b9-7666-4ba9-925b-c2d7c3b06577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "f1a98ddf-74d3-4b62-befb-3be448a7c504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "f2f738c9-adce-4177-8150-610e00c4509e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "6e24c11c-2624-4c6d-9d97-e4554b913faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "2e3363cb-a362-4e3a-9aaf-985cd2a0e560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 120
        },
        {
            "id": "b556121c-a530-413c-b1d8-60b41a33eb67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 121
        },
        {
            "id": "abf8d45c-53ca-4efa-b9c3-ae0c5b0ddb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 894
        },
        {
            "id": "e2a0aea6-9ced-4488-a7b4-5a31ad251087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "bf18c18d-1299-451f-944c-f2cb5400751d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "110cc476-23bd-47a0-b0a7-ef67a2f206bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "c349570f-6926-414f-95f0-c8cad54b8692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "f7da14b7-80f4-4f2b-84ad-23c1c773783c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "e378c77c-4294-417e-a0d9-5bd7f7a90f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "bc0bbc98-698d-4ac9-b28c-8665758be448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "8253f767-c575-4c92-bc10-1eeaaa60ba6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 35
        },
        {
            "id": "39e7f431-8d0e-42c9-9df5-31cab81ca4b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 40
        },
        {
            "id": "9af1716b-f06f-472d-af2a-55c62a2df20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 41
        },
        {
            "id": "97ec44ea-6eeb-465e-aae3-849b3972c90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "f89a560d-0cf2-43b5-a3a3-8169a4a10fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 47
        },
        {
            "id": "961c2fb9-cd02-4448-a86a-17a09fa5ec29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "f733073e-70dd-4306-81c8-8659e2e88319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 50
        },
        {
            "id": "20d583f1-3da0-425d-8492-322aea595999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 51
        },
        {
            "id": "6d01a19c-7bcb-4d50-954e-ad011c2fd5d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "236960b0-9124-44d7-98bc-9ceec8fe8132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 53
        },
        {
            "id": "65551bc6-e621-4fe6-b478-c7ba95f72e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 55
        },
        {
            "id": "41a5c449-5017-4367-bc91-74209c64d5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "43c25dce-9b77-4bb9-bdbc-11e6f7e61cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 58
        },
        {
            "id": "2490933a-6f96-4bdb-8834-f4407de4ac3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 59
        },
        {
            "id": "440d6c6d-da2c-45cf-820c-a7ed00882dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 63
        },
        {
            "id": "3e0ff3ce-4836-45ec-9fbd-7b377bd648d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 92
        },
        {
            "id": "1120f770-e0ef-444c-b548-cfbe141f826d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 93
        },
        {
            "id": "fddcee70-2087-4514-b7dd-e0f39fa70e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "98ff4e27-2d3b-4cd0-a8a9-28c14efca772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "e0de53fe-ffc3-4d8a-9265-143f0ef17ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "2065234a-1e5b-4f8a-8d14-0b8a80764842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "b2e35991-1b3a-4479-bfa6-c00d8f828fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 115
        },
        {
            "id": "17239875-e608-429e-9d3e-a695b5381690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 118
        },
        {
            "id": "29a20eaf-e81a-4d4a-a4b4-ecb041ab2983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 119
        },
        {
            "id": "b8ddd3c1-67e5-4406-ae69-7227439e84ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 120
        },
        {
            "id": "71e9d988-d66b-4f15-87e0-3cbeae22822b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "389aaac8-ec3b-4d68-b396-9d6a1df0c96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 122
        },
        {
            "id": "76a56e03-e812-4762-8f4e-7ebe0218fd75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "c88e5307-61ea-4317-8c14-26213faab88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 894
        },
        {
            "id": "4431dfbe-25f5-406f-ba9c-26361f46a5a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 8208
        },
        {
            "id": "2f4b8d6a-8055-45a3-9281-40fc8355f0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 41
        },
        {
            "id": "941572b5-94d1-431d-a0c0-92c53bc34d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 51
        },
        {
            "id": "858cb8dd-eb6b-48d8-a974-f2844a2b8360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "9b84bc99-6dc6-4194-bb07-2adaa87925f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 55
        },
        {
            "id": "2c9c1f1b-cef2-41c1-852c-abeafd10395f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 92
        },
        {
            "id": "88b80f86-fea1-4c4c-bb04-bb2345f5b063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "e694351e-c53c-4118-b9b1-e13ec2e76463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "91175cce-01c1-4dce-9e83-b64e7e6012c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 119
        },
        {
            "id": "2f5d82ee-fc47-49d0-b057-d8cb29c32527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 41
        },
        {
            "id": "ba46d391-a9a7-4619-81e6-fac01f39ef2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "2be980d8-7797-4416-9536-209d1220a1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 47
        },
        {
            "id": "019dfe42-508c-4aee-b14a-44075117ea28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 49
        },
        {
            "id": "144c2d77-5606-46dd-afcd-df6a8ca63351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 51
        },
        {
            "id": "63dd4641-1266-4b45-8eb6-b673976b8436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "16f77e58-20e6-4ba4-9a85-6742332c8c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 55
        },
        {
            "id": "1dd466aa-6fbe-488c-a4f4-1ce6484b5c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 57
        },
        {
            "id": "3fbbafbc-0549-4df3-9d50-580cc5f36af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 58
        },
        {
            "id": "13f89d66-3ad9-47f6-a4aa-37f26dca2961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 59
        },
        {
            "id": "251aca98-bfcf-48f6-86a9-2f42f96b666f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 63
        },
        {
            "id": "225101ec-fb2d-41cb-a65f-abc660bc8301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 92
        },
        {
            "id": "ea2d7ee3-f6e5-43f8-818b-998aba360168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "2e5df0a0-3c62-4465-ac3c-b5741d7a0eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 103
        },
        {
            "id": "ae3b60c9-8401-4dcc-b632-2604cb2a4490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "797a0e77-ba48-4b0a-a4f8-41f5e2913d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 119
        },
        {
            "id": "9ef33ca8-39ee-4703-9668-a0c8aa15c1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 120
        },
        {
            "id": "8d72dc4a-df86-43d9-89de-185a315c8fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "33c66c8b-dac3-4d59-8304-5514b938b4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 173
        },
        {
            "id": "394cf0f8-4d73-4176-bd15-a0e1db05fbbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 894
        },
        {
            "id": "0d0a4896-34fa-4124-a310-71994b3a4ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8208
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}