{
    "id": "22be6637-5712-4a81-ae4f-23fd36be25ef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font411",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\font411\\BreatheFireIi-2z9W.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bulky Pixels",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c544977d-11f6-44e0-92da-4d68904ede6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "aa6f4db7-e1ba-4d0f-9fcb-844a8973714c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "210d8c15-347d-499d-9d62-d799bd13432b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 241,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "91ff2292-c3a2-4cc5-9ee7-29a658796713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 226,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "03fd4eef-0622-430f-9c4d-221d0be7e545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 215,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "e73735cf-da37-419a-b452-f2a1055ff85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 195,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b66119af-9974-41bb-9ee7-c7777de80845",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ab5ea01a-4b04-45e3-85ba-8d6fefc0ae30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 175,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "0a2275b7-6097-40e1-b46f-a29c880e7b23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 166,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ae824dc1-7a81-4fad-b4e7-17cda923055b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "feeb7411-f380-4c57-bf0f-100172f8e33b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 8,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7fb79ab8-a62d-46de-ad63-d76fb20a82b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 145,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8728c50b-74eb-40eb-a6af-d43e7a6fd4b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 129,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f301cea4-b434-4d47-9833-58e00c5f478d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 119,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "77b31575-f597-4730-8448-f965a3675195",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 113,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "47f100ad-fdf3-4ba8-80ac-6826c56396f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c709ea98-f48f-42e6-aae3-b85f239a0767",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 92,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4362fe32-a27c-4696-978e-17612288b003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 85,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2c94c6fa-a807-4786-88bb-419013ead71a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 75,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a1a36167-ff9a-41c7-93b9-31d2db1b3971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 65,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "79d69532-df94-48c3-b80f-a2661550e23d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 55,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a9c885e0-031d-4aa6-9a0a-cfe84b222fc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 135,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "7d9aa4af-5dc9-46e0-b238-a4fb1cdd28f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 23,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "f0ec1493-e278-4d95-b049-c53edb0df38a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 33,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "30f751d8-201e-4de3-b39b-281396d7864c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 43,
                "y": 59
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3b24b87e-5695-4473-a9a1-b6f16ebda224",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 26,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "eb85ce99-9c9b-4878-b976-1f234dd34e1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 20,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "8207c91a-4943-4fbf-95a9-7ddad7f8e33a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 14,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7681430e-d3b2-4c58-9a45-4e1ce21f2368",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "85bf7c87-ed66-442e-9d7e-3b470f8abf88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 238,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bc823946-ca41-4405-84bd-fee7dafa9b84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 226,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "87b9f067-383b-488b-b7ea-11f1fef2f985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 217,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0510c374-e794-4eed-af78-84e49cd924a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 202,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6d10d6fc-60e8-4bd7-9b80-111e751d8aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 190,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "255e0ba1-4e4f-4f37-b18c-c2b78afabdbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 176,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "0c6889fc-257c-4d35-95ef-49fe6c01f7c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 166,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "252d2a98-86dd-46e3-9b3c-e11ed7b9a053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 154,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "832b9245-89a7-43c5-be7f-ca7bef3c4129",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 144,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4bc50ce8-b63e-4c83-b39a-5fbb8e5aac6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 134,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "9d5e3c19-4f74-4c9e-9a23-55a5e19c9618",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 122,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cff2f974-6363-40aa-86e8-a521d9be136f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 110,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1bdf58d2-9664-411d-b609-52b0d42081ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 104,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "57f9e1f4-c798-4cea-b6c3-384d7f580d7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 94,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8af52694-3f68-43ab-a772-e3e5ad317c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 82,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "92cf4849-fd8f-49e0-8f4d-22bdeb224455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 73,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9967957d-a83a-4063-8180-7111e5de419a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f2854367-efa8-4ec0-ae48-35b3d6c87031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 43,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "7830925b-3aca-4e76-832a-f68a6ada52c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 31,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "37a4cabe-ea9c-4f9f-b009-390919841925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 19,
                "y": 40
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "8c9527d5-f4a9-4afe-ac50-ecc2062a5ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 9,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2f24b60c-9672-4055-b841-8f2469bbce43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "11c39106-e093-4325-9dd2-1c4cddb7e29d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "35aef5ad-447e-443b-b3b2-b599a5d33b5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "6b7cd071-52f3-4a50-aedf-be21ca4d713e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "3184c570-77fe-4520-b22c-42020f92be72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ad98b703-bfd8-4fe9-84c1-a862ad54a9da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "89af543c-ec6c-47f9-a36e-7d430e5aa4f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7ed76096-d6a8-403a-be30-bce65f08302a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4fcb5596-456c-4c09-ba2b-77e4748efbb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3704149b-e592-4d65-909f-f4b14b80522a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "527e26cf-1ba1-49de-8498-fb1d6f21850e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ac78459a-937b-4006-902c-76f093fe5802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b80dd4aa-e2cf-43ff-9aeb-23f1da328d22",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "23357ef6-ae39-4228-96ae-768c6eb7983d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a9551325-5cc8-48d8-bc24-7f2f2d2de066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "45d720c9-c1bf-4daf-b15b-296b90d0f77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "b10e246c-ae8f-4e24-b76c-fab704857933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "58637d5d-56b6-494e-9745-75ebe0961916",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f1f2794c-6f53-48b7-8aff-92a37b3701a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "9d4e39cb-9203-4e48-9381-fe4811f30350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "436e10a3-6f12-40be-b2a1-1237b8633367",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e5c4aac0-3dcf-4932-98b8-08d7a88443f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 23,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "9acdf186-a072-4b6a-bda7-134fb11d84aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 132,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "56589c95-2791-4c53-86b1-4a4c4d4d36bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 35,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "80c4a2f7-365e-4044-ad04-af979d52309c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "a90cb1ec-8095-4610-9974-0367618c4f07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 237,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ee7b9fb2-65c1-4b14-82ad-fe1e2a19d893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 230,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd0a4c00-b033-4e55-adaf-82ec39f0873f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 212,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1a308d40-497d-4815-ad22-d8f7910fec17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 200,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "b49d6605-d564-43e7-8bd1-f9c12f084b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 188,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ee0f7e92-f637-482b-9ee6-61e5a8f2446d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 176,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d9a4e751-3e89-4cc4-bdfa-6e60cc54989e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 164,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b8bd3a0a-407e-414f-bd8f-10ce5beb2312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 154,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5cdff074-ac9f-46cd-ad21-e9759726391e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 9,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1638a7bd-3abe-4996-8654-24d99f59b986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 144,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "9c4e9f75-9346-4174-bf29-fd710eb04ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 122,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0f22b742-a4b6-4023-8ad1-225d09a5a0d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 112,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bfb8ebc8-d8c8-48c0-8afb-26b4c4432e6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 94,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "805c1492-79c5-47c3-b2f3-bde59d784c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 82,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "11dd259f-e931-4d3f-95eb-3b548b4affe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "01b4fe68-a08d-4924-80e3-e1cccddad779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 60,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d637622d-89ca-4500-a810-c3c3f42ea499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 54,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "9ab2ce5d-4950-43a5-a9c9-f6f30a432736",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 47,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "da0806de-118d-47c0-8f6e-0aacadde3979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 41,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "816ee881-b589-426c-9c3b-5940cc58d264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5782965b-369a-401a-9b2d-d2f7620407d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 54,
                "y": 78
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "58e09f11-9c15-43af-bab2-c70a02b6964c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 40
        },
        {
            "id": "1e035569-eb5f-4ac8-ae85-8903a399dd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 41
        },
        {
            "id": "c796aa38-d478-40a1-9e26-2b18c7043a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 47
        },
        {
            "id": "a9ffec20-ef8f-4775-9542-8bcfa0ebcf67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 49
        },
        {
            "id": "bfb4960d-072e-4798-955a-821f03146b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 51
        },
        {
            "id": "4397df8c-9200-4ec0-a06e-983f874e43d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "d7a176cf-5f1a-41f3-b2d9-1ec42e870cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 55
        },
        {
            "id": "fc1b1b8f-aa61-450c-95f0-785d606feda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 58
        },
        {
            "id": "a3a2df0d-dc34-44d9-8707-0d66f6c72356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 59
        },
        {
            "id": "24b76558-6681-417d-8fbc-6fec8c088306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 74
        },
        {
            "id": "89f5ba51-6c75-4b9e-b922-365d3758bd16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "92553735-cb59-4700-b9dd-46b70ed5d34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 87
        },
        {
            "id": "43f6d8a5-ba04-426b-811a-5c5f18d931fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 90
        },
        {
            "id": "71a76b16-05a7-4a01-995f-da840911ed3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "3c8f6670-748e-4ff6-ac73-3a36100da9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 100
        },
        {
            "id": "6599c8e3-2a49-41d3-bcff-e571268a6004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 103
        },
        {
            "id": "678477c9-11a5-4a4e-9eb3-7363b226c534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 106
        },
        {
            "id": "bffdc5fd-0d85-4409-b6ce-04a0c9c63334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 119
        },
        {
            "id": "22099c09-35b5-461f-815c-44161fb79a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 894
        },
        {
            "id": "44350561-112e-42b0-933f-c3fad7322c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 40
        },
        {
            "id": "801f74cd-5fac-4b0b-96e3-a2dfddf16ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 44
        },
        {
            "id": "6071b9cc-e016-426a-befb-d8a6b46e3656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 45
        },
        {
            "id": "658bb599-232d-4564-9b0e-d60da2e49d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 46
        },
        {
            "id": "6e0c520f-d751-4b1c-b38e-dbbdddd6e0bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 47
        },
        {
            "id": "c6d9c77b-96ad-4728-9b52-3d743b3b7dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "9de8bf25-286b-4a14-9be3-2646bf39ca43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 55
        },
        {
            "id": "a0420a93-92fd-484f-ab50-ff51e6b698e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "b5f3776b-18a9-4079-9fdc-ee1bedd05b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "2fcfcec8-0390-4b13-8029-81c3e7eae4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "a6a4185b-3df3-4a32-a47c-bcf8c6fe9c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 106
        },
        {
            "id": "6f7b6dd6-0069-4baf-89c3-3596282e0b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "4b7a2eb2-b31c-46f5-8e7e-be229a646eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 173
        },
        {
            "id": "530f0b50-3a1b-403d-98ae-d3364da29489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 8208
        },
        {
            "id": "24e81cd7-86e7-4620-b173-3a9f5822c53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 40
        },
        {
            "id": "5211165d-026f-4b8e-85ed-f17f5e7ccdae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 41
        },
        {
            "id": "e109ea6d-c0c9-42d3-af3a-906b5b5f739c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 47
        },
        {
            "id": "c47f8f7d-f113-412d-9883-e0dd15b151cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 49
        },
        {
            "id": "6c4afc1b-bdcf-44f0-a190-9fee2413a532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 51
        },
        {
            "id": "45b889fd-407b-4f3b-a0f9-54af72867524",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 52
        },
        {
            "id": "b666c05a-2746-4dde-ad04-13fa06198ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 55
        },
        {
            "id": "0fd79558-d7b1-4c41-a598-c553241a7ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 74
        },
        {
            "id": "432a92ed-fc06-4547-891b-41ed35eb4a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 84
        },
        {
            "id": "661ccc89-7eee-465b-bc01-cd769a7234d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 87
        },
        {
            "id": "98e92c83-cc65-4516-9689-5243dacfbdec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 92
        },
        {
            "id": "a92f95ee-427f-4d61-958a-71742b90d77f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 100
        },
        {
            "id": "bc014edd-9097-4173-b9ae-b065c18cc04b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 103
        },
        {
            "id": "d71d9d93-2364-4f94-819c-4d1953799864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 106
        },
        {
            "id": "2425f663-3e9e-4c30-bbe5-4778d939ac03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 119
        },
        {
            "id": "e3b49c25-9e66-4e28-a876-21b25bd69c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 40
        },
        {
            "id": "43371cc6-14ff-4a1d-a118-593361b770dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 44
        },
        {
            "id": "82239a53-bd5d-4560-929c-fe8be5992ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 45
        },
        {
            "id": "e91ce307-f545-4e40-bb9e-8613ee1def35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 46
        },
        {
            "id": "3bf34fbf-8ced-47af-a9b7-06bdb7cb3b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 47
        },
        {
            "id": "7eccebce-337f-427c-9473-2d2ae0c71c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "4e0b9fe1-a3a3-41bd-a90c-aff49e60ed2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 55
        },
        {
            "id": "e21bcd89-da45-4827-92e5-a8964be8e0a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "f487b3a9-ea30-430a-b881-50014122b3cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "696d5682-91cb-4b5b-8542-158b79426a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "3cc50ddd-8f86-434a-b04e-3099bc671830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 106
        },
        {
            "id": "9d61c641-6432-4f4c-aa22-a753cefc34d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "eff94258-a18a-4d90-beb5-8cb7d48324a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 173
        },
        {
            "id": "678d8f14-fe45-4d45-9338-c5b002316cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 8208
        },
        {
            "id": "73790fb7-e610-4c0a-9fca-fd3886762693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 47
        },
        {
            "id": "1b5d843e-c7a2-448c-bfdd-0bfccc1333c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 51
        },
        {
            "id": "8fe3b2f0-d826-4249-b24f-0bc580f92ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 52
        },
        {
            "id": "86818ebf-d362-4e2b-ba01-3f56ab018dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 55
        },
        {
            "id": "1e1ba96c-40b5-446e-b74f-51ccbb70189d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 58
        },
        {
            "id": "d14f4c3d-2275-4a77-8a2b-b3fa747bed9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 59
        },
        {
            "id": "19885ec4-a0cb-46fe-8e6d-27e05e6c2b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 74
        },
        {
            "id": "53348505-9bc8-42c0-a5ec-4421fd727d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 84
        },
        {
            "id": "7e383930-c389-4bec-ad19-172d3e1c8c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 87
        },
        {
            "id": "9eeb7a66-989a-4419-ae97-a40dc9cade54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 92
        },
        {
            "id": "f18e2ba6-54e5-4794-9b94-4eb60a6ba7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "15d5088f-47a5-42e7-a0ab-6c802b7aa733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "1d3bf047-38a7-478a-9e25-59deaee784a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 106
        },
        {
            "id": "f52bd372-bb3c-44c2-8502-3c4847d97097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 119
        },
        {
            "id": "486fe696-999d-4bd2-84e8-125bdcef035d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 894
        },
        {
            "id": "3ff0a03e-944a-4196-b787-2dffa91a0152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 34
        },
        {
            "id": "ed38145f-6ddb-4a13-ba4c-bd49c23db68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 39
        },
        {
            "id": "69096821-eb56-4025-ac78-a70c2e068d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 40
        },
        {
            "id": "ebab28ff-38c7-4f59-bd61-c7cfb44e1215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 41
        },
        {
            "id": "2e5830bb-d745-4cb8-b199-b72fbe450c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 44
        },
        {
            "id": "2e2ba313-c782-4837-9156-64f2a3a39088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 46
        },
        {
            "id": "f78e730f-5d82-4d05-b8ec-e6cbfb583169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 47
        },
        {
            "id": "a8df7051-521e-4214-adda-85e043d11676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 49
        },
        {
            "id": "68383c10-2a6d-4a8c-9498-d5c692586aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 51
        },
        {
            "id": "ef181085-a762-4610-afe4-265d913f3a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 52
        },
        {
            "id": "e15012f9-1e8c-4e51-8a3d-0995ea7a5334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 55
        },
        {
            "id": "5e74eeb0-14a5-4015-8f7a-24285f05d57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 57
        },
        {
            "id": "e02149da-7113-46df-b21b-60137aae611f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 63
        },
        {
            "id": "f0fb8d5d-dd7f-49e0-ae00-8a3957c5c101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 74
        },
        {
            "id": "68429f4f-e184-4570-8121-dc2eb79bb0b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 84
        },
        {
            "id": "57a33ab1-172e-4a3e-b0e4-fa72206a6650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 87
        },
        {
            "id": "cd7374cb-ba9f-4017-8c67-4e34bb212558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 90
        },
        {
            "id": "56d04843-3fef-4922-ada8-d1eadf2d0bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 92
        },
        {
            "id": "9c7c8e7e-e769-4153-ac24-b9c3bf043ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 100
        },
        {
            "id": "2c024be8-edcb-437a-810c-f7305d930672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 103
        },
        {
            "id": "c08901ec-5c43-4abf-9995-71361deddb5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 106
        },
        {
            "id": "4c39a501-acc9-48e0-802e-aad6898592f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 119
        },
        {
            "id": "b482e18d-1f07-42d2-be9a-c15f0fce37e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 34
        },
        {
            "id": "002f2dff-9d92-4e18-a051-237586b1282c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 39
        },
        {
            "id": "e45ac442-8de2-4984-ae28-6848a6a36aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 40
        },
        {
            "id": "b8b64d2f-8bf6-4c7c-b498-b78652304ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 49
        },
        {
            "id": "85cd8bf5-1843-4459-bb4f-4773ada9ee28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 52
        },
        {
            "id": "a655d577-e5c9-4058-b9e4-0ea5d96ec443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 55
        },
        {
            "id": "73b0520a-a8be-4a40-b5be-f3e4c705a402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 57
        },
        {
            "id": "edd8bca2-8fb5-4671-9952-f480aaeb2504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "01326e2a-3aa7-48a0-a315-6d25f6b634a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "2492043e-3c90-4ff8-bdce-5a68a4c768ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "79f2c642-d51e-4513-b99f-5c183cab2170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 92
        },
        {
            "id": "4d024415-1b16-44bd-97e7-ecd652c3a29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 103
        },
        {
            "id": "a0edcfdf-1617-4dbc-ad90-c37f1ce7d4b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 113
        },
        {
            "id": "d5eb59d2-6291-45a1-87d3-191155fc24dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "9f88a182-b157-4088-82d7-042903b825e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "223c6f1a-e524-4213-aea0-8ae7e7e7f15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "5084a761-7c4a-4fc8-8846-126bcd6100c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 45,
            "second": 34
        },
        {
            "id": "1a211153-600b-4567-9770-b9e7cc192c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 39
        },
        {
            "id": "be356a0b-bb53-49df-91f3-005530fa18ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 41
        },
        {
            "id": "f41c3ec8-17b4-4027-898c-98725edef1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 49
        },
        {
            "id": "1fd8402d-dab0-4fc4-928d-dab6207028d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "fe43223f-493a-4f81-a9a0-4e15a4c32678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 52
        },
        {
            "id": "af123792-a565-416e-a843-9dc10cadd9b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 53
        },
        {
            "id": "334b76f1-553d-411e-b195-a75f3f6921b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 55
        },
        {
            "id": "9fdb6218-197a-4b86-9b83-721c4d680546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 57
        },
        {
            "id": "54fe9834-739c-4a59-909b-2a5d51d2a8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "79f98697-5050-4373-8fa9-7157dc13eb8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "15d76c99-dd74-42ae-8001-3b0a18292b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "9b31046c-2622-41fb-9947-7d933345c6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "e2143669-2d4c-4f22-9bab-2b6380ab3269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 83
        },
        {
            "id": "7c643608-8a30-4586-9563-0142282fc910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "bbc5eb3f-b1e1-4343-816c-19e8c6cc557e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "fe34cee6-6da7-417a-bc24-25fb648a1002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 92
        },
        {
            "id": "7ac927ba-c567-49fb-9332-a97d02cbde5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 93
        },
        {
            "id": "5f0fac1f-f421-44c3-b2e7-a1d2d6d3fc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 106
        },
        {
            "id": "7ae03a95-ece4-4054-868f-81a7c3513b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 115
        },
        {
            "id": "16d94c37-b4e6-43ae-89e2-fab930343208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "365553c2-e899-4f96-a7e2-8c05c3137216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "8f77f3fa-edac-498b-980a-4867730a5769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 894
        },
        {
            "id": "9f6b6ce5-aca0-41cc-9c99-d0f3d0a2bf83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 34
        },
        {
            "id": "26ab5c9a-e5b7-4754-ad22-0e483035d82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 39
        },
        {
            "id": "c75ea720-8190-4ff6-85b5-cebc6dbbf047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 40
        },
        {
            "id": "921a18b0-2c6d-46ea-9e95-f382ab05019f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 49
        },
        {
            "id": "63a74098-6031-4c94-83c5-6aae6423937d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 52
        },
        {
            "id": "7f189072-1d7f-4795-b971-ad1c8ee0100d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 55
        },
        {
            "id": "ddd3155f-c1d5-4bd5-918e-7557f97f15cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 57
        },
        {
            "id": "7cba481f-b68f-4b97-9e3e-1d426e084c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "cfe5e4ea-e8e6-4449-be2e-79739061bcd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "04d67c43-7339-473c-8697-1291f9009a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "cfaf4f91-b3ae-4e80-a773-c03abc50585f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 92
        },
        {
            "id": "912aa688-f8a4-4443-8350-18880d4c4eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 103
        },
        {
            "id": "702faafa-c2ca-43ad-a1cd-d63b916334f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 106
        },
        {
            "id": "a2db250b-2392-4b97-915a-75358dad77d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "e0a55355-dd0d-4cd7-82c1-ffb6be8ca54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 119
        },
        {
            "id": "35005487-3e32-4d4c-94f9-f65de1dcef17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "013ce62b-00ba-4981-b498-643bb2c3be48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 44
        },
        {
            "id": "38c0b7d9-b625-454c-88d9-1e649b29768d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 45
        },
        {
            "id": "8f71be0d-f6d5-4b96-ac5b-4a42d20bedcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 46
        },
        {
            "id": "33168913-ca5e-40b7-86c9-43c50ea1cac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 47
        },
        {
            "id": "ec9ed887-95c2-4f34-9113-4e06ab6bc3e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 51
        },
        {
            "id": "2d39f568-b184-4deb-8750-6c4a20485634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 52
        },
        {
            "id": "2a25cfc0-d49c-464b-81fa-211fbdf545c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 55
        },
        {
            "id": "b0229f62-ef8a-47e6-8c58-8c5cb1cf107e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 58
        },
        {
            "id": "a06ffc62-82d9-48b7-962b-7ea266b8b398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 59
        },
        {
            "id": "55bba011-a94d-45e1-8bb8-7d3cef5539aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "e5dc0a42-5fbc-46dc-8b4e-807eb08c0fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 84
        },
        {
            "id": "103f3c41-f36d-48e1-a388-ff5dd6c87e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 87
        },
        {
            "id": "9c06034c-cb5d-4cf7-81f1-48ae33984ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 92
        },
        {
            "id": "35b050f7-910c-41f4-bc57-ad5a53485cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "540be4c3-a711-448d-b85b-1a531d704da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "344e2b1d-6271-441c-b8ef-b468f3d7499b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 106
        },
        {
            "id": "37c36e5e-9a25-4e05-a1d9-90534fb7a31f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 119
        },
        {
            "id": "d153d90e-560f-49cb-9334-5d4c5342c406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 173
        },
        {
            "id": "fa827218-1f88-49d4-9454-e70ef0ce563d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 894
        },
        {
            "id": "0e446383-eda5-4e19-a646-7590647ed31c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 8208
        },
        {
            "id": "b13c6a72-edf9-4f9d-927c-18f41e5f9b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 47
        },
        {
            "id": "54548bf9-debc-4637-956f-3e102e9b75cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 74
        },
        {
            "id": "b6f2971e-9a9d-4a86-8b51-917e69912ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "1b04f8d9-7c74-4bc2-9421-129f8197caae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "041b8f45-273a-4dcc-abc9-813584b73df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 92
        },
        {
            "id": "f30fb96b-5864-48d8-bf21-616bca8e689a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 100
        },
        {
            "id": "609a7f17-6724-4bf4-9617-7501be9fdea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 103
        },
        {
            "id": "07ae7415-b170-4420-b258-60b84e50d45d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "23ad1fb2-0f2f-488d-968a-fd3ead6e3aad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 119
        },
        {
            "id": "d084ce9b-310b-49d2-a177-ba7bc1b06459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "4b8f8cb9-9da7-4b84-a7e5-4bae18327717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "fa40974c-bd7b-49fa-85e7-c6563fc30d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "5760ef6f-01dc-4bde-a342-57b4b8992753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "dcac5d74-8a5f-4b7a-8667-97a4476abd8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "b740c2cb-8f3d-41aa-8b9d-ea20c4c95269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "fc5c6c0c-7c14-4230-b7b9-5cda9058ab48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "bb260510-6119-4097-99f6-65eba644d00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "30ea23d0-26aa-445a-a327-fd8cc2709a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "14f41141-de4b-49f0-a90e-150db1ba8da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 40
        },
        {
            "id": "23c14f86-ea93-4ead-92ab-38ef7639e1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 41
        },
        {
            "id": "8c068bcd-6b45-4715-a32b-f057d1852d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "96e58f84-30c3-4ed3-a255-cba454462f65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 47
        },
        {
            "id": "231c26d5-ea46-4338-bddc-7d41eeb35d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 58
        },
        {
            "id": "681d669b-9fb4-4963-9534-bca44a725674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 59
        },
        {
            "id": "b9a23c29-a1bd-403a-9ef5-a08ba6cdfc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 63
        },
        {
            "id": "1bf7fa79-f78b-4c17-9538-a31589a492a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 74
        },
        {
            "id": "d051a620-0fc3-4f27-abb3-bfb208a799e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 84
        },
        {
            "id": "1871d531-172d-4766-9dff-ef3611353a4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "850da7f1-d91f-4413-a899-d892da13be90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "dd9f6ffb-270b-421c-891f-7f14e219f0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 92
        },
        {
            "id": "2774805d-0783-4b19-8039-9c86e6c16b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 100
        },
        {
            "id": "841d2e59-d7d5-4b89-b95c-cd8cd94e349e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 103
        },
        {
            "id": "3e7e816f-035b-476e-a35e-8be160a0d981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 106
        },
        {
            "id": "c877be96-c968-4bef-8ef3-1c472e8b884b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "a544378b-071c-49c1-9a6c-4cb1e715b832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "b6506fdc-2de9-44f0-8c24-631607e4a4a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 173
        },
        {
            "id": "460b5b13-f819-4081-81d6-4e8fda0ffce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 894
        },
        {
            "id": "8ca5a31b-a1fc-4dd1-92fc-477b6905ee64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8208
        },
        {
            "id": "eda754b8-44bd-4c5a-be23-ec262d549a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 47
        },
        {
            "id": "31762bcb-a4f7-487e-9980-2edaf36ee928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "208330ac-cca2-424c-84c6-3dbe418df247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 84
        },
        {
            "id": "61de1e2f-4709-4067-8d7d-3fe627398080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "98abd855-7c9a-4912-9368-2d738e1d0adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 92
        },
        {
            "id": "cbb29d1c-3e74-4a08-86a2-d10d1bf9c78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 100
        },
        {
            "id": "679c13b7-d414-473e-965b-32346cb3d5fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 103
        },
        {
            "id": "b153e189-8e9a-4277-a0c2-4d17362f48ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "92ae64f7-68e6-494b-81f5-2078b997a1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "eb706e95-cf15-41f9-bcbb-8acfe8443e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "9e9e1b7e-f9dd-4c85-bb96-7306c4e75edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "5c44c497-a0fd-4034-bd78-f04a97eba70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 40
        },
        {
            "id": "a1d2677d-56af-4b39-9258-d7ee28ecbfb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 41
        },
        {
            "id": "57d1245e-aa3a-4743-b5f4-dea2508c8c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 47
        },
        {
            "id": "fa06dcc4-f861-4ee0-bdd8-d10d1238cbb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "e3bf088b-6ac6-4061-be98-f2f60ac21352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "60a5dd6e-28e2-4db4-b003-4df3d9798642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "2b3b45a6-7cb5-405a-aff2-da1d88f042d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "61cf0ee9-bdbf-4981-9e39-c0369780ed4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 90
        },
        {
            "id": "480b685e-38f3-4859-a48a-7a2b76d3de19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 92
        },
        {
            "id": "21339128-b4fd-43ee-b93c-032372ad9653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 100
        },
        {
            "id": "e5c7742b-fdef-431c-9c20-dd6d68f93423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 103
        },
        {
            "id": "d6a364b1-bbc9-45aa-a434-6019bc461f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "b0e5a1d6-188c-4bda-8aee-e5e24f50ff08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "cf42da93-1598-4436-8d35-e994e6b4ec07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "56784ec9-314f-4ecb-80ca-01ea17822035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "6903a73c-4b74-43d5-80e7-08aecda26588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 40
        },
        {
            "id": "5224140c-1721-41e3-8682-96c60248aa04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 41
        },
        {
            "id": "b9973ef0-7801-4910-998b-ec2225d25066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 47
        },
        {
            "id": "12d96773-461d-41b0-90b6-0bd2101a9383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "ce017836-4912-4997-ad84-1ece7a9b7870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "05c69fb0-53ed-411c-b3e4-f499070d691f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 63
        },
        {
            "id": "75864df4-1449-4e58-af4c-180e48ef74ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "89b399d4-a863-43a4-812e-757293faf229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "e361c2a1-28dd-44f6-9fde-24adf64808ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "0c33694b-91ba-4911-b18c-42f7f07baa22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 90
        },
        {
            "id": "fc6c5f93-28a3-42bf-84a0-80839e3b2a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 92
        },
        {
            "id": "d1c58063-279c-40f4-b22c-9b59161a278c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 100
        },
        {
            "id": "2a049758-0989-41c8-b9de-73a268e8037e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 103
        },
        {
            "id": "2a7e1dcd-f45c-42fc-a8fa-85401340360b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "a557d511-3767-4b26-8f3f-625d05ee1b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "f44e054f-d052-429d-ac51-3b2fa3c9a197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 894
        },
        {
            "id": "8bf90909-cc03-4f41-9517-410b4bd44d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 34
        },
        {
            "id": "151cff51-6d2d-41d3-bb8a-d12142c3c1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 35
        },
        {
            "id": "e8a0d69d-db07-4038-b979-9f9fe87985af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 39
        },
        {
            "id": "4c41c000-1911-45f9-ae20-64aadb7bb5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 40
        },
        {
            "id": "818b5f9b-9f57-4f70-af3d-09f19856bfc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 41
        },
        {
            "id": "2425fb54-bfd4-4c03-b2ed-9f321a99a5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 47
        },
        {
            "id": "feb1f295-a97a-410d-a890-68f4d00af000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "70e162a3-a629-443e-b876-abd998ca46ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "2b410a69-7509-4339-8ab0-59259e976a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "c6d34807-790a-42de-87c7-c98722093170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "c6440f89-32b4-42bf-a45e-d3e36356a706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 83
        },
        {
            "id": "78e49b35-0ed8-4d66-9355-f3cb965f2bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 84
        },
        {
            "id": "b2e60caf-60da-46c5-8fca-3b2f91004e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "bbb2d80a-4657-4be3-88ba-f6f59cac1448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "1a7b130b-eff3-4e19-ad7c-11909a135a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "c66dd87c-7091-4e5f-8f94-5d1cf5a27cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 90
        },
        {
            "id": "85c151c6-f3b8-4671-b43d-0364bf347f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 92
        },
        {
            "id": "894ad9ec-ca07-41ed-805b-d2be8aa01dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 93
        },
        {
            "id": "eb7be50b-40e0-44f9-a995-4766b2d3a698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 100
        },
        {
            "id": "15f68693-10fb-45f2-8780-c9bdffe6bcc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 103
        },
        {
            "id": "dd025655-70ca-4523-9058-c24773f66f38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "e5ae93b3-edd9-4c82-a7fa-e10233fa67c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 113
        },
        {
            "id": "36b2e176-cad8-4c36-83c3-1bae36bfdaa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "f691c110-5030-4fbc-bc72-6b952d02934e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "72741c0f-449d-43ff-9eaf-fa4359804805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 894
        },
        {
            "id": "0b8b30d1-0131-41a8-8092-e1127f14c805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "1798afa1-1d2e-4477-b0e0-8d1fc6782216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 74
        },
        {
            "id": "f7caabb4-ebe4-42a4-95c5-3ba69ec087b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 84
        },
        {
            "id": "356b761f-a710-4a60-bbe1-ed6cae3071fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 87
        },
        {
            "id": "d44f3f2e-e8bb-4e79-88c0-81ee08633f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 92
        },
        {
            "id": "874dc72b-9fca-4d1e-a243-e1aa372a73a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "613b78b2-c77b-410d-8a79-39bdbe98e1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "cd5738ea-11aa-4e8b-a27f-d1e50a094035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 106
        },
        {
            "id": "8272fc27-f92a-4bda-8c51-b4454561ae16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 119
        },
        {
            "id": "03bc2f4f-21e3-4dd8-85b6-0c0601aca9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 47
        },
        {
            "id": "6424c95b-719c-4049-bfdc-d711a0978f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "64ac3cf0-09f1-4b48-afde-cd3ff0bcf6c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "76a310c3-66d7-4e43-84ac-20ed9eb0b570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "6785b53d-ecc2-421c-815d-d16e5714dc7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 92
        },
        {
            "id": "fc128e0a-fb40-4ed1-9b07-957e26929105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 100
        },
        {
            "id": "2f9834f3-b970-410e-ac0b-df109a746cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 103
        },
        {
            "id": "b7fef0ab-1490-4d3a-a6d4-ce87f01de104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "77aebd6c-1f92-448d-a3c6-5c2d21ae6c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "4c78eebe-93be-4f51-94e2-5d3feefb7e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 47
        },
        {
            "id": "997155f1-31f8-44d9-8109-0a934d013f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "b0d01d4e-08d3-4cc8-9756-c66015c0ccb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 84
        },
        {
            "id": "b4c811c9-5746-42ea-a47d-20182cdeff74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "8afa66b7-18fc-40ab-b9f2-295cc42f245c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 92
        },
        {
            "id": "958b2ce0-fa46-4008-a026-e2c62e475070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 100
        },
        {
            "id": "ee0d742e-5cbf-4b2d-bce1-ecfbc336ce0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 103
        },
        {
            "id": "9dcbe01b-2f2f-4fa2-a123-58b18ac89685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "8b80ae94-2b1b-4407-a90e-92864f03ca28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 119
        },
        {
            "id": "17770a58-a8bc-477f-b221-a0673fb993e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 41
        },
        {
            "id": "4536f8fa-2e70-46f2-a2bd-5b8a08b00416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "efcc9ce5-936d-4be1-a40c-edccf7ff1876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 51
        },
        {
            "id": "01689c6c-b7ae-41a3-9a64-af845a31f5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "af844941-b2c2-4c24-ade1-669adbead0de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 55
        },
        {
            "id": "93bd12c5-59af-454c-b412-6352983cc628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "990862b1-083b-46d4-8f20-5a3583ef36aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "0bf5ad25-fb0f-45fe-aa23-3e956e38c41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "cba3284c-000e-4bdc-b7ad-373489a36493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 92
        },
        {
            "id": "01e3adea-c577-41e8-9bd1-2014778d2b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 106
        },
        {
            "id": "f4769b42-9943-4109-a1f7-ed80604907b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 119
        },
        {
            "id": "bf61dd25-640f-47e2-b7f7-8dce0d0a9b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 173
        },
        {
            "id": "72aacb35-74db-4139-89e2-fb1b74f73302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8208
        },
        {
            "id": "33b6e615-a303-4c1e-bac6-d642ea53596c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 41
        },
        {
            "id": "a1172852-f2fc-4930-8842-18cc168a07f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "f226e3f8-b6f3-48b0-87d3-fb2eea39f574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 51
        },
        {
            "id": "128bf86c-0258-4074-935a-eb125e5e60ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "2683eb40-01d8-47e4-9e56-e2850a37d01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 55
        },
        {
            "id": "1049b369-1270-4884-afab-c1cf71d6d473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "1d626a48-de64-4891-a845-ece7b99edaee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 74
        },
        {
            "id": "c6e39565-ed16-4098-aaae-0f44cca0c103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "43625b07-8b67-452e-817f-bb3aa90085e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 87
        },
        {
            "id": "afae68a1-c447-421e-8f24-1086ff7c151e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 90
        },
        {
            "id": "1f10b28d-8edb-4263-b727-95d98cf6b9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "ea4d0800-3f62-4070-be9d-6a60016c826d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 106
        },
        {
            "id": "07e8721f-d952-45f7-b899-1a632c0d65a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 119
        },
        {
            "id": "b7e274d9-cb66-44b2-ad16-3f6e0fb92507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 173
        },
        {
            "id": "84126cfc-2725-4494-b9c4-d2159de163ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8208
        },
        {
            "id": "f341fcfd-03e2-4aa1-97fc-e6d7bc0fc0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 44
        },
        {
            "id": "0c0e29cb-872b-48a1-ae6e-7043bc5ed363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 45
        },
        {
            "id": "d9b19d7e-3539-42e4-a673-d9188e3ed73d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 46
        },
        {
            "id": "127e0a80-1889-4283-a618-e85ff24b4842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 47
        },
        {
            "id": "a6197045-6960-44ee-b3a4-6dc4c8315e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 51
        },
        {
            "id": "4c2f0626-665f-45a4-b88e-80cc441fb167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "020eee57-8c10-407c-a86f-29c2632ff967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 55
        },
        {
            "id": "ff52893c-8ca0-4019-adaf-2a93eddc93a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 58
        },
        {
            "id": "11f3b9db-8760-4ae2-830b-15bdb72f02e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 59
        },
        {
            "id": "1626fd7e-302b-4635-a1db-4fb1a3758051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 74
        },
        {
            "id": "e7ca2eb6-a6e4-47d7-9866-7db63eebdedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 84
        },
        {
            "id": "b9b82064-3781-438d-9a7c-ef2115e0d74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 87
        },
        {
            "id": "d204f294-8497-43f4-a028-3ba2f6c8afd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 92
        },
        {
            "id": "151efe58-2536-4693-a074-80962ad396e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "114013be-73a8-44f2-84be-788e5b7a34ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 103
        },
        {
            "id": "e00e8712-aea5-44b9-ba12-97a012835df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 106
        },
        {
            "id": "23c656a3-c9de-40a6-a73d-ed5d95135c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 119
        },
        {
            "id": "4d01370f-dee4-48c4-9eed-400770b577a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 173
        },
        {
            "id": "68cd537f-c2bb-41b5-809d-e0db2cae578d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 894
        },
        {
            "id": "f0ab5f8c-1501-4edc-a176-8caebd2d892b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 8208
        },
        {
            "id": "69bb81f5-c3b0-4693-ab0c-fda728d80b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 45
        },
        {
            "id": "f16cca5a-cbe5-4c12-b11e-75433ed739eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "39c02464-0591-4564-89e6-a68f9595b893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "2e86e2ee-2a8d-4aeb-9ac7-4be11e815aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "c2609f8e-ca31-4a43-98f6-c9422ea107ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 52
        },
        {
            "id": "dc97802b-1350-46a4-bec6-7278f6638b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 55
        },
        {
            "id": "d78bb77c-ec0e-4214-aee8-052bff05b325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "f6b394e5-5f0a-4626-86c6-5d19db69ec72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "0698827b-1d55-44ec-ba91-9617793754c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "7f874aaa-cea3-48ef-81c2-2c8c7355b34d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "2ab7d312-0f4b-4fef-a3d9-42d5922adb3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 100
        },
        {
            "id": "a26c4d51-ffd5-4ad4-90fe-f7eaaf612136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 103
        },
        {
            "id": "67422c6a-562f-4385-80ce-50681ca573f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "508b555f-aee9-48a1-aff2-7eabed03efca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "07bff354-938b-43ff-bfee-d2df57ad53bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 173
        },
        {
            "id": "faa96c55-a826-460e-ba94-7aa088dbdc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8208
        },
        {
            "id": "1e6229af-0de3-42eb-aa3b-950c6918788b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 47
        },
        {
            "id": "5c725894-b2bc-49fb-bf65-f16688cab822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "07061218-93bd-4623-96ca-0677a9f2e900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "6d92d8b7-ef0e-4b05-9d45-66dfc84c3213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "500c81de-ddd0-40f9-98a2-c98aba22be9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "5cac6da7-0704-431b-87db-39077bf8615e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "38bdd311-c818-4ef8-a667-94e405e9c731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e6da3ce4-4fe7-462c-866b-2cde7097ae93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "1645dc42-cf47-4788-82c3-03d0612387d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 100
        },
        {
            "id": "3961af1e-2eaf-4f89-b7ea-f1815c1ece05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 103
        },
        {
            "id": "de5cacd5-6f85-43bd-8ca7-9ce881070b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 106
        },
        {
            "id": "3026c95f-addd-44b5-baf4-0540cbed4ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "2fcb6d8e-3776-4af0-b613-817ca22a48a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 34
        },
        {
            "id": "ca86793f-3b86-4444-bcc3-520efb4c8204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 39
        },
        {
            "id": "ff6366b6-822d-4cc0-b56c-e58afc53133d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 40
        },
        {
            "id": "9ddfbc8a-4d5d-4e56-b8da-bba2c1fc460f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 41
        },
        {
            "id": "e7b49f07-c4c6-4469-bdeb-773d24d1187f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 47
        },
        {
            "id": "4ba9ff14-5f31-4d31-a913-e61b32c8ee62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 49
        },
        {
            "id": "94dda942-d593-4c3b-9edf-b43580cf6e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "475151b1-4d38-4c30-84dc-7eb8f203a09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 51
        },
        {
            "id": "4da1b3d1-3e8f-4d5a-87ee-77d0123f4e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 52
        },
        {
            "id": "e140ffd4-90dd-4787-94bf-f0eb02bd0da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 55
        },
        {
            "id": "4914c6eb-0d83-44fd-a87e-520be64e9b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "d4eab91b-9643-4c71-bf8c-10adceccdd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "1d57049a-a095-4b5d-9e71-69b0168f3740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "0930d8ae-c8ac-4419-b3ba-ce1425401523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "72e7b1b1-9194-48e2-a1b1-c54ec11715c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "27be777e-1a36-4320-81a2-81e12b335198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "51082fe3-c482-4c77-934e-f6f9f28d5f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "e5972f3b-6f52-4c01-b0ac-d8a9723f891c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 100
        },
        {
            "id": "5fbb6b3a-44d3-4ed3-88b5-ce0be4950c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "19e0f3b3-c05b-473a-a549-4cb4b78be44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "f49f0da1-f10b-4609-8f4b-2c0aee7836ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "6d2c9d8f-937f-4e3e-b93e-75bd94b88f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 40
        },
        {
            "id": "3a9687cc-6a7d-44c5-9f06-dfaebecacf8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 41
        },
        {
            "id": "8c417400-fc2f-4f17-ad3e-6aa2a8ae1bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "743888ea-1a7d-41c1-a829-8b31aa401308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 47
        },
        {
            "id": "b16c8260-60c1-46ac-b492-895c682cb491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "95134b24-5fe4-4638-b651-95a69dcd8857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "05cc9569-66b2-4c54-b378-890a1e970fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 52
        },
        {
            "id": "1cda19ad-181c-4e43-9a90-46e06225ad3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "f49dc533-7a91-4838-b964-8fd14aa24092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "8e28046f-1664-4b76-8aa5-9ee3c5f609af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "09d8b2aa-1c4c-4975-aec9-fc5072f29a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "f2f5f6ad-5a77-4ad4-8aae-36f40d0e619a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "c253b138-f83c-43d1-9d05-2253afcb0611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "7ebd1647-f165-4709-a1b0-7b1044668937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "a691f162-3b72-41c4-8eae-d9d163cedc7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "3bff5541-e9fb-4379-8120-f6aaacba5647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 90
        },
        {
            "id": "8388d8f3-a2bd-4073-88fb-2796deaa4626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 92
        },
        {
            "id": "c392058a-09e3-4536-a9c9-93928820c383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "b207ae38-840f-4a4c-bb6b-770003a50d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "4192182e-bc32-4bd3-8f86-32805c9db9b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "fcc985ef-6b58-4ecc-8d6e-eb051c119aa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "7d3662f7-eead-4bac-b586-7f75631188ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 115
        },
        {
            "id": "be66696a-572f-432a-9d8f-2b67337fe4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "5a5be682-e7dd-488a-a34c-6e741c4d15e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "be4463ef-982e-4357-87b9-39f5b59d8cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "62e7652e-4f6d-429f-adc5-430ae211d807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "78facfc0-394e-482f-b2e4-83172221eadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 122
        },
        {
            "id": "175f2466-a44b-4606-8032-9af073a70358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "aa442f35-791e-4bae-8fe0-489835de6e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 894
        },
        {
            "id": "1e18e1a1-2bdf-453e-ad24-e47cdf31d2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "5a4fbc5d-340c-4108-9624-e3ae0ab6ad9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 34
        },
        {
            "id": "9f204812-dc09-4b76-8c08-b619ae123136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "a40f3902-6344-4b5b-b2f1-51871a119262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 40
        },
        {
            "id": "f021584a-2edb-4491-bacf-7b5c47acf82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "e6b3eceb-f3ac-4861-b25b-18741e400d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 47
        },
        {
            "id": "91a6b639-ce84-48d4-a39b-f9b2aefe50cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 49
        },
        {
            "id": "f8ca63b5-338f-45f9-bddd-fae0a697f7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "662ac89b-328d-4f11-b5e7-5818cd0ca009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 52
        },
        {
            "id": "ed8a62d3-db85-4c4c-a28c-6e52ba1eebd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "89571370-35a5-47ef-bc38-4b2fc3d2824a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 57
        },
        {
            "id": "ad5fbbb0-ec09-4d3c-b51b-6c021a593c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "3e16f6cb-64dc-4e25-a9fc-b5c0b353f8aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "47112317-156a-4ef0-a90b-b8b50e96ef47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "0d1247d5-6416-4c11-9a14-8ec5b197b653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "610c05b4-1caf-428e-a547-5adb14e59085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "c575b1d7-66ee-49dc-ab79-423037dafa34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 92
        },
        {
            "id": "86d830d2-fa87-4ca7-bf86-84c18c689af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 100
        },
        {
            "id": "102f7666-6604-4d13-9f1b-be0df299e8bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 103
        },
        {
            "id": "2e076783-5b4f-4362-9666-7d65b048f6bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "a2ae800f-64bd-4817-a551-b9b73f532718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "785d1f75-c4dc-4e8e-a5c1-880ab0e32f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 47
        },
        {
            "id": "e7f90a0f-d046-40ba-8eb0-0f07df786382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 51
        },
        {
            "id": "ebda81b2-2b28-496c-a936-9d7a2a8caa35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "30ac90e7-f92a-44ca-8329-f8732e0d16bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "8d093c02-ba0d-4118-a9e3-4f626d4ca7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "7553ab9d-32a9-4337-8592-e683b2dcfe7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "1bb41837-4e72-4b54-86bb-763411750f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "adaa0c6f-3a79-4406-80dd-834eb6d9b5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "29358d0e-7eec-4bd5-9830-5e7813a05634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "b550684e-736d-4f13-85c0-0e317869de60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 92
        },
        {
            "id": "67008fea-4cc0-4815-ae72-39f63b568b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "1d748d04-a01a-469c-b0e2-249f99bbbe9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 103
        },
        {
            "id": "30c27ecb-14dd-47a8-b47b-04660dad7d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "4f2fa4a8-ff16-4440-b426-309f432bd8d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "a9133c7e-73fe-4062-9c4d-f6d634f83c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 894
        },
        {
            "id": "f5bc06da-12a8-4eb6-8d8e-0b896521fdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "a3b0afc7-9f9b-440f-b3e1-32fc6275bea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "50da2922-15e8-4899-ac72-0a4a5f06f840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "0a01ee6b-9718-489f-b25a-a4e84f0f9e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "e6598df1-0569-4a1b-b1d6-f17bf0a79612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "415cba20-2ef3-45e1-b997-3822dc1ddf57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "dead31f9-59a3-4fe2-9845-95885c0bde91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "cf1aa9c2-dfb6-43fa-a88c-9f77db6c6c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 55
        },
        {
            "id": "0f0ff647-efe7-4b78-a059-8604713e8ea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 56
        },
        {
            "id": "f9346aef-36a5-40a7-a672-d06a286439d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "dfe4b1ef-db98-4004-b705-ec5359d40918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "9a58c5be-eee0-414b-a4c0-ffc78e8c503a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "7131fd18-53c3-4871-b92d-c11b20265240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "4e6d635a-132e-4e10-ac94-6706be611686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "76ef90b7-4211-43d8-819d-a16637c9dd6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "45146757-dbfd-45b5-ba9d-2a4d217cc69d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "d97e28d2-08be-4040-b5af-b65fbbe0af6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "43391e1f-9a56-4759-96ea-03f4d6321d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 118
        },
        {
            "id": "3ee5ee04-63d0-4465-8a8c-6806f7ed8cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "8d8be227-04c9-4767-93c2-67c6ae43ad07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "35e2ff31-672a-49bb-888a-ac4401592856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "1956b7ec-c9da-40f9-9e8a-4d9123d3f038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "35075f54-c7f6-41e6-b9c1-ef6f2a076af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 894
        },
        {
            "id": "79b1ef43-2f3d-4051-91ba-033246c5506b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8208
        },
        {
            "id": "b0410d1d-e2fe-42fe-90c4-e8c573d2f8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "4b7f73d4-c0ae-48b3-8e30-4df2fa0bcbaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 34
        },
        {
            "id": "0d04620c-0589-4ac8-bbe4-6d5ab1a8a764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 35
        },
        {
            "id": "8499c585-dcf2-4634-9766-8bb345b1067c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 39
        },
        {
            "id": "1fb99831-ffcc-4218-8138-b5816fc88c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "21d49f6a-c6cb-405b-9a7e-dd42ff43f7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 41
        },
        {
            "id": "9bb93a7d-d262-4a1d-bb00-6b7dd7f2b87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "1b161a82-f525-429c-aae8-350c42d70532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "0cc82186-0837-4d93-89fc-84824b27f268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "f8af6439-b8e0-4732-9528-e6e62c183135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "2a690a63-29b8-431d-bffc-998e00c8494a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "6aac5b06-3b45-47fa-9df0-5b46fc99b2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "63f9c65b-1f9a-41db-9ac1-6c263cde239c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "d32bfbfd-1f79-4054-bdbf-8264977606cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "fa9d5db1-08f3-4938-80cf-34b44c2b405b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "96982d8b-1eb0-4496-a8ee-1a0bb657f04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "878c1799-a728-4127-ac49-57b66e16a12c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "967cdee7-38b0-4f16-9a74-9fd4d913520f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "0a82d466-9f22-4d69-9791-042ff8f229ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 64
        },
        {
            "id": "b929b113-5325-4b9b-a011-3db8ed526b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "194f1865-6fdf-4aee-8b84-8caeecf2312a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "28b0a768-aa47-4ee2-83f2-e05f0e06133d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "299e70cf-2ce6-4d36-b32c-2d2c4f8b0a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "2cf924d1-83e4-4d3e-94c1-14eb534bd962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "938ff252-4a3b-4cca-95f5-8c1258f3cb34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "fb68d340-913c-4d87-b6bb-c860f62d56d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "1af5d702-8bcc-48ab-99e3-8399c1f91b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "23d30cc3-ecc3-4ea3-b252-b04cc45233d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "92f302c4-7bf6-4787-ba60-a0f8e3f7132d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "f898e572-bbcf-4875-b336-b547fa67b841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "4ec0b0eb-eeb4-4518-945e-969a5b435173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "66857637-3050-441b-8c76-a65a5e918cae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "9f7adc3b-babe-4809-8e71-5d80633f7487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "1e85b4fd-b9fd-4ca0-af86-c609c1994e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "abdd6b58-0d53-49a2-978e-d18b9b81f5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "9652c42b-12f5-4548-8ff2-68cc2a1c1f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "214a6bb0-ef64-42a6-8a02-de257ed33be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "da93f640-376a-4892-b883-f9db4fcda23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "71b02f22-d50d-40d0-8cdf-2c58625dafb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "46981719-60e2-41dc-88bc-d4bf92523222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 894
        },
        {
            "id": "a21b9532-3584-4eaf-b0e5-8e01d0406c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "fdde9620-49d7-4cac-928e-494305c17cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "6cd8f8db-fd24-4c49-93f8-48e44b86b477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "28ea4840-c2a5-477e-8a27-8edb261f3a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "20c63776-7d9b-4055-8d29-6ee21c7b76f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "4be89a7d-6259-46f4-9466-4333c443a279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "ef434544-9014-4113-8015-695daa146244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "cbe78e1c-1818-4243-bd21-4041a9cd1dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "5d406d83-7ff2-4520-aaac-5cf0fa008889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "6eb4c7c4-2ce2-4edb-ad3f-e22238f2186e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "87eeb8e8-18cd-4547-953a-33f57412c172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "3b9a63e1-5080-4a94-b9d2-04bf62a0f482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "7e4977cd-1a36-49b1-8fb7-77dd3fa80dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 47
        },
        {
            "id": "77fdd9b2-cf97-4bc3-bdad-9fef46e39aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "31505373-a738-4444-8259-f6f0a476e336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "1482b3f0-6d7d-4e09-b759-58bfff1fc21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 55
        },
        {
            "id": "5f37b6c1-7113-4037-9a59-810ab97ee1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "c84e7c5a-f512-45f9-a5e7-19a9a518ba85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 84
        },
        {
            "id": "c87a02ee-3c81-45a3-aa05-2077eb37483d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "ab7d0283-3712-4e70-926b-3d39b346a131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 92
        },
        {
            "id": "4c71ab63-64b8-4bce-93ae-2b811167ffb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 100
        },
        {
            "id": "a21b28a7-1bf8-45a8-bcc2-cfcbb2a3c34b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 103
        },
        {
            "id": "ee3ee687-f0ee-432f-8670-bb2b339f4e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "1003a414-1eab-4a37-87a7-da02b9ced8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "3221f69a-aeac-4955-9dba-f4c9aa31f777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "5d2593bf-95ad-4419-b974-bb1f48c6bb18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 45
        },
        {
            "id": "e36f83da-0c62-4f45-bee9-09b1a4c1c7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "641c3fce-d780-45e2-9cde-e56263d67af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 47
        },
        {
            "id": "2e520f23-0f93-462f-9c51-d56bc2f6e1e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 50
        },
        {
            "id": "ae823180-1c3a-4016-8e0c-e68c83d200c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 51
        },
        {
            "id": "c8af5e59-e8d0-4447-9f91-e315eb801635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "06dea9bc-1204-4a64-90bd-2e874067c9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 55
        },
        {
            "id": "526b7780-edfb-43c5-8a9c-d18101917677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 56
        },
        {
            "id": "11688453-7cd1-49ba-b785-180f1f9cbbf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "e69ff7b4-269d-4fac-945b-b12604aae92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "75446018-cc6e-4e64-b73a-ba49364a3c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "a98cbdb2-52ef-4141-9d77-74fccd733062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "693c8b97-01fa-4564-91b3-f7a3135fb5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 87
        },
        {
            "id": "e7da466b-58d4-4905-8dcb-72b60a7c5c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 92
        },
        {
            "id": "cfa19840-11a5-4da3-a7b0-3d74bac9f345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 100
        },
        {
            "id": "98e5b816-015b-474d-b5aa-fc6c98cfb35e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 103
        },
        {
            "id": "41259c08-dee0-4939-b76b-b70c8a7ccc87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "becedf74-a264-4cdc-8d74-1cae40b7741d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 119
        },
        {
            "id": "1138f718-1b46-46d8-9a85-fc8824e96462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 173
        },
        {
            "id": "b03c8070-7e9f-4ade-a367-3d269a8e103a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 894
        },
        {
            "id": "be15defe-4554-4b62-b00e-4d24dc249eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8208
        },
        {
            "id": "3321f2e9-677d-4454-8e09-5c80b1da06c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 34
        },
        {
            "id": "812417cb-7306-418e-8b8e-c3bd571ce755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 39
        },
        {
            "id": "da08adf4-a0c0-4deb-b1a3-1992aed319e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "037c2082-6f7f-49dc-954b-6791582cdfdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 41
        },
        {
            "id": "e412eab4-cb9f-4038-bf5d-1f13d602bdbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 47
        },
        {
            "id": "d0ae12bd-e4ca-4059-a454-d9c653d65d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "d6a0e11a-bcf3-4276-9604-ebfb8cae00e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 50
        },
        {
            "id": "c41f0245-f02c-4c54-9424-f60bd84e887f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "f6e01d81-16ae-4c63-8946-6ef1fb150997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "742787eb-b7c2-4fa1-89fc-31a697894ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "22d65683-10db-4d06-9471-d6ff0f73f88c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "b2ac5d03-58c3-4913-b113-33159d1770c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "466a8695-71d3-49f2-a37f-4beea27268b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "4c6bbed4-d5dc-46a2-aeb6-f59e4665cd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "85d538a1-4ef5-4b38-ac84-d274379023b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "5b6e1991-417a-4592-9828-28f634ca32e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 90
        },
        {
            "id": "ca06db9c-ce99-45b1-93b5-2046cc8fc40d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "48743e50-315d-4a96-aa4f-23612b5468f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "fa914756-36bb-4c87-8d99-16194795061c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "3d2ed3c2-46fe-4a91-8afc-4205b9a6c728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "9a5d6b6f-38e3-4001-a45c-5c185d616d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "20d2c226-7968-4f09-b172-4abb24688600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 34
        },
        {
            "id": "d98f2005-cb5c-463d-924a-438e17f0c960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "cc7638df-e73e-4333-9714-551737e1a546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 40
        },
        {
            "id": "e5004072-1c59-45cf-b5fd-3c60a5a79a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 41
        },
        {
            "id": "6960ed0b-6eea-48fc-b5af-6867f6c6a081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 47
        },
        {
            "id": "34a2be46-367e-40fc-822e-eabbc15cdcea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 49
        },
        {
            "id": "394a10f4-e285-4b3b-b633-85e1d133fede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 51
        },
        {
            "id": "150df05f-687d-405e-b6d1-deb162821657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 52
        },
        {
            "id": "02a4799f-7113-4fa1-8fd2-fd5ac096feef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 55
        },
        {
            "id": "de552898-0cbd-46ac-96e8-a991ead5bc6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "f477e7b3-df08-413e-bba7-fc2a5d3fdf1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "f3ac5c1a-45bb-4104-9db0-0aea18f1c7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "d7b0f6d3-09e6-4953-8f03-6aaa9d949c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "8411faa6-fe7a-40fb-b212-e957d0e21443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "b69e20e6-70f0-4972-b271-71a04e5ffa12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 90
        },
        {
            "id": "f23c7799-b3f7-4043-9c2a-38628d869234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 92
        },
        {
            "id": "6c0da1f9-3710-470d-9e96-bd04f14dab2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 100
        },
        {
            "id": "a844c1a1-e6db-4926-80f6-ab5f502f57b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 103
        },
        {
            "id": "8065a347-dd90-4be8-b011-5ed0d7fa5f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 106
        },
        {
            "id": "f805ceb0-3a47-4248-b7fe-ee3db48e77fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "8a025597-2607-4a10-b15c-ca7b5e6b1918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "70c97c01-4899-47a9-bfa5-5ad0e90652d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 35
        },
        {
            "id": "61b41c9c-4a00-49c8-9690-0a6fcbd24202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "bcc9c5f5-daf0-4de3-90dc-b23a2d677b17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "2853fc01-be3a-4ea6-ab8d-b7b31038ba14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "933c58fa-36a6-4243-b4cd-b7de71b50fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "e7b68cf1-21bc-4e6d-8606-5e05ed72ca88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "57dec21d-3334-40b8-8055-a0c3c61d97ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "7c0d5768-88dc-45ba-92ce-9f39e0d11059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "3e7838fe-d5ab-4bd5-b5ef-38292fed7e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "2dd88bd9-7afe-4c9a-b4b9-4c0ce2e8f5fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "f6e84302-38db-4453-9145-3eab409dfb16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "133204d9-0591-4a72-91b0-813abc67b1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "c86b98f6-6393-447c-a744-b48a148520bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "4e7091a3-c771-4ae2-a587-a0da24f90562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "3d65f38d-9c92-447a-b130-8d737a566087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "710f6bda-31da-4296-bf3b-408967994517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "c2e31d4f-6e50-4539-ace6-3697a7dfd610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "d640e4a3-377d-4a9e-991a-313fe29462f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "2a1ea598-eeee-4014-8d43-02aade0369c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "c73def91-0f98-4e92-9b20-24421f46ba82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "5c7a575e-26a2-492d-b01b-a7340456d849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "996fa575-8d0e-49e5-83fc-2a49f20bbfca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "e0266b30-cd6a-4207-8d33-572e5129a2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "6661e0b7-fdb6-44a6-aa0e-d0ca2f002cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "f87be124-2f03-4cf8-8015-13d323425870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "3cf0a400-0e39-44ec-86a1-12b1bd7f1ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "052c37ea-8820-433c-9ca7-8b67b0de3189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "4620d9bc-b1de-46c9-90b3-37d3449a546f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "a136f5c1-7927-4618-9c78-faf88a3f4e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "04c9f7c9-944a-4523-9673-a1debfbb7aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 74
        },
        {
            "id": "17e4e4a5-0c6e-4477-8c4c-fc92738e92e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "817562d8-2269-44b2-a692-1ac7ddaf5e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "e0a3b0b5-7ac1-4c7a-85e6-66885112d8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "cbc060b0-cbf4-47b8-97a9-08a2010668e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "3e70cccc-ebde-426d-8e0f-8cea1fe4ab3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "dc6c5cac-10ec-4efb-b2b9-f8c00a1cf7fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "2f9f1378-b995-486d-9504-59043c603e7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "d1588652-d182-4e77-ad2c-3f5d7391d971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 47
        },
        {
            "id": "8ad2982f-5d9f-4d8a-8871-76323e24640d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "83f6bd9a-14c9-44b6-ba8c-01e5ec10f593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 52
        },
        {
            "id": "a647db30-f982-447f-9123-f952fc1cc4dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "2449abeb-f800-4419-9923-e82ea704ec47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "bda25da1-5e5a-440b-9915-815528ca6018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "91f591f8-4549-4d48-a18b-182563c2c7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "55a17bd2-adf8-4e01-8255-a79d61c45244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 92
        },
        {
            "id": "d1938669-f3ab-47d4-89be-522882743157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 100
        },
        {
            "id": "dfe8de0c-ee35-4019-a09b-354fcebf04fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 103
        },
        {
            "id": "2a8a1e0b-81ec-4944-9dbf-110b720569ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "f8174018-5049-48d5-9869-d5ebffeb778b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "131bcc10-df5e-42f9-b815-d611ec63d5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "4dc90d7f-2e68-4fda-8cda-9ec9f8dc62a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "79afb3a5-44b4-40a2-b364-86b3549bfd47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "c456449b-cdb8-4913-a436-0dd2ca771f5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "5e6dde6e-552b-4f42-88bb-088aa9b6dd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "a8c6eb1c-e018-47ed-ab63-060df2fd75d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 55
        },
        {
            "id": "df9c0fac-361e-4243-abab-7efe18e8fa37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "26678da0-b22e-47bc-90aa-5547e584346a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "c5a1aef1-2b17-4ffc-a460-ace3f36c033c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "8ace6ad1-5dce-4a1b-aa19-97ee23fc3c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "d6efe7b0-2470-4b50-9d42-ecfd24632ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "bd2719a0-f6db-4290-817d-7841dff05d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 92
        },
        {
            "id": "d159aa6e-5130-46a2-bf02-c4ca72c4ccfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "11d06750-339d-4344-a259-5dfb91e0fb43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "6b266ada-da5d-440a-a1b3-21e078be45e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 106
        },
        {
            "id": "63c0c637-2294-46d9-a020-f6b399873431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "4f659261-24e8-44f3-a2b2-e053edfcf35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 894
        },
        {
            "id": "7edf202e-0948-48fe-a400-b01e3c05ef10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "23e6f292-85fc-4c9c-a373-40aed6cf6723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 34
        },
        {
            "id": "cdd2ce61-3953-4f3b-b9d1-4ba887259916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 35
        },
        {
            "id": "c97d5459-edf8-4b2a-85b9-5759a0185a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 39
        },
        {
            "id": "a6a98cd0-c99f-43ab-9ca6-02cda326eb34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 40
        },
        {
            "id": "3096e9b7-d3f5-4891-bc16-121dbab8e3e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "d9086b1a-eec4-4477-b4ee-3cdc7050854e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 45
        },
        {
            "id": "2aca0837-ed84-4d5a-a3e8-b108522aee77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 47
        },
        {
            "id": "611bcf15-780a-4a2e-8172-c52595bb8795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 49
        },
        {
            "id": "0661ea96-4085-462d-b1b3-e30c627ea531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "a91d779d-2f5d-4ac7-a1c4-f2a6bdbbf793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "29bdc6e3-46f4-43b4-ac6b-106ed374d06d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 52
        },
        {
            "id": "d33818a5-f604-49d7-992c-cabce8e141ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "8ff9d86b-85c8-453d-9452-879e3c81be06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "acb701c8-2835-434d-aea3-4acf5723391a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 56
        },
        {
            "id": "fe1e61f4-696d-4aad-832d-23b338fd7454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 57
        },
        {
            "id": "a2695539-afb3-43cf-a476-488241c4158c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "5168a4e5-7182-48d4-aafe-ece94aec240b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "bd441562-e155-41d3-9b96-f9f04bc10e82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "161e19cb-c5af-4ca6-a04a-19a8c93ca4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "89eeb569-ec75-4f5d-8f40-ceffc0510fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 74
        },
        {
            "id": "63006aa9-eac4-4499-85c9-954af25bb686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 83
        },
        {
            "id": "e489dab5-d6c4-4b6b-a563-9e546a5cf366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "000af94b-11d9-4d6d-9840-87bd45ebe10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "b3fd567d-b65d-42e8-964c-aa94cdbe24ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "2e40cdf4-3ad7-489a-a3eb-2c2158d6ad9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "8f370284-e2c0-4a40-be29-48c5c36fa90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "b1330ebe-b1c0-482d-a8ef-0b55bb6e254a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "caf1aa81-0a5a-4543-9cd3-cf1a385effee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 92
        },
        {
            "id": "beb0de58-2b65-41df-8231-5fb78d775a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "9956218e-8ad6-4991-83a2-75182ef59193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 100
        },
        {
            "id": "80b348e5-a1d4-4fe4-858b-132e277651d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 103
        },
        {
            "id": "c2bc6e3e-55ed-4378-b6ce-66ef16db59bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "628ce506-0c6c-4782-800a-325ab74de2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "9dae6ecc-9329-44cf-98c3-859e4aee0389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 113
        },
        {
            "id": "21cfb041-2169-4875-b1d2-90eb5269aa0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 115
        },
        {
            "id": "724668be-a747-41c9-948c-275031a876f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "eac938d0-4f3f-424e-825d-655b9d32f138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "dba86510-0bd0-4c60-8250-f9d41daa0ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "7eecae5c-0d65-430e-b1b3-8f26f6225d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 121
        },
        {
            "id": "aae7eeb8-9c66-4c74-99fd-04df59dcf222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 122
        },
        {
            "id": "56b1cade-aabd-41f9-93a7-0608a36203ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 173
        },
        {
            "id": "6f2f0c33-2538-4e02-a102-b3db747965da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 894
        },
        {
            "id": "5859ca3c-43d0-4fac-8702-7983a42f3486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8208
        },
        {
            "id": "f6363c2f-b0d5-44cc-a93c-6a8346944f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 34
        },
        {
            "id": "f7cbc1a8-f4fc-417c-820f-6a22ad323973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 35
        },
        {
            "id": "30f48d98-fc7e-47b9-bc98-cb8c990dc126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "9b3faa8e-1bd7-4f29-99ba-aaf96f3d36bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "00fba6ab-bf87-45cf-81e4-6fd679117ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 41
        },
        {
            "id": "eb1ff5df-8e1c-4aff-92e4-4b8564f51c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "52215af4-d6e9-4259-9ad3-07a3048a0f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 49
        },
        {
            "id": "357a5fa3-ddef-4a3c-89bd-1c95946e24db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 50
        },
        {
            "id": "3ac52b8d-ed56-4665-a375-92d88a71e548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "04d12e19-41d7-427e-b7d9-447a5a0e3216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "b2a13aee-a204-403e-aba5-ce9bdec0afa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "19a2a09d-ca24-4271-91f9-e020c2b5bf0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "70c8006f-9ed4-4753-8943-e5f2b365a3fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "17c59db3-7f2a-45e9-b65c-ba870764f820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "bf6a51eb-64db-47ee-9722-d3be3e966a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "970a15b2-eea0-450c-b909-0b02b1018032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "d03f00a2-6630-47bc-afb8-bf71d041d3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "98ea834c-b61b-420f-8a80-ad877999a2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "58673032-71a1-4abd-8518-5bdbd670f8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "5815415c-8eb9-4ac9-96dd-bdbc32dc27f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 88
        },
        {
            "id": "56360b8e-56cd-4f91-943a-3efa809da7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "a3dc61cf-f3c2-4707-8fde-0f89a3a0a762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 90
        },
        {
            "id": "2c3203ba-8f00-4432-9211-eef74db3d961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 92
        },
        {
            "id": "9de6e339-31c9-4820-96c3-beefeae7d499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "7b269400-e965-4f6f-9137-35f0df2d98aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "b17c0a6c-2530-414c-aa55-627a3ad52aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "8e811e45-d07c-4e2e-a6b9-babbed3c967f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "002ca65f-17b7-473f-85c8-bea902259de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "f097f2d4-9f2d-4100-afb6-daa92708ec74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "a105ef9f-d65e-47a7-b77a-cd1343697cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 120
        },
        {
            "id": "f64172bc-6fef-4aca-b061-bcb5cab8f768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 894
        },
        {
            "id": "485d0a58-a836-4120-b67a-ecfbaea00d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 33
        },
        {
            "id": "8bfca4a8-ecad-4fc8-bc0a-49ec26ca3ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 34
        },
        {
            "id": "0961af67-f0db-402e-bf66-c333631dabb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 35
        },
        {
            "id": "517b22f8-b6c7-4bc3-9623-95ae0dd365e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "560ea057-e6e7-4ece-81b0-e6215c16e967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 40
        },
        {
            "id": "d8896f14-23cd-4072-a1a0-434b0b615110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 41
        },
        {
            "id": "b517a1d3-814e-4fd9-883a-001bdbf8d2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 47
        },
        {
            "id": "000b374a-8540-4b53-8690-e826d863c958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "1d8ca69f-4e9c-4788-95b1-e3e5b1a28464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 50
        },
        {
            "id": "226e8a85-3dc6-40a8-a9c2-5ed6b23abbab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 51
        },
        {
            "id": "1525de35-7579-403d-acab-90e6a9dac807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 52
        },
        {
            "id": "b2ede2ec-23b2-41e6-be12-31039e5c05e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 53
        },
        {
            "id": "5e7793b2-8757-4084-b5f7-2ff323b6c2d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 55
        },
        {
            "id": "86d89be8-503b-46e4-ad8c-4a5025622361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 56
        },
        {
            "id": "9113d572-57e9-43a5-86c1-0c47b6cc8a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 57
        },
        {
            "id": "9ac6625b-939f-4b2d-b6dc-3979a54200d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 58
        },
        {
            "id": "b0e50d4a-fb9b-4709-9b1a-e391decc44c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 59
        },
        {
            "id": "a4274c94-5562-4429-a5d1-8485a2a6319c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "7c73037d-b29e-40a6-9c6e-ec7fd1de03e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "ce2e74fe-7e81-415f-a361-a1af09a0e16b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "66157a87-a336-44b3-ab6e-0a276b4a810b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 83
        },
        {
            "id": "72e4a10f-0d75-4403-89c1-3e230cdc22e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "1b7b5d23-70f6-4e61-af41-0e86b374ee2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "4c10ebc4-49ac-4a26-bfd7-aaa2cab0d010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "82b6e8f7-041e-494c-993c-1df91e26b039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "bf287694-6060-4f51-b97f-5f12ce10b0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "1348464a-ffa7-4317-8977-f72068967136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 90
        },
        {
            "id": "b9f5b151-0d80-4178-9850-58beac9bad08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 92
        },
        {
            "id": "2e5883a4-62d8-45b9-b633-0c188ae03b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 93
        },
        {
            "id": "dd1ea887-fde1-4cab-82e4-1c8d1d748dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 100
        },
        {
            "id": "4e4c1678-90e1-45a3-923a-1b76938896b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 103
        },
        {
            "id": "7d016097-a60d-4012-8946-d15bee8d0756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "1cf94fd3-6e4a-45f1-8f83-6048f6b0e53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 113
        },
        {
            "id": "742e6b09-c8e9-4fb3-9473-1bde710a63b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 115
        },
        {
            "id": "ce07ec4c-5691-40ef-9e21-e828d5fd50a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "0c859d74-40cb-4ed0-ad0b-713f495c1fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "13615229-a8c2-4a0a-8a22-a0a2e48a41b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "fa8e4a82-654c-4067-a499-301d4b558f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "d5fec25a-f86e-4658-87c7-18874ab4522b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 122
        },
        {
            "id": "4a2c09e6-427f-4554-b14e-3b1b1c7c8685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 894
        },
        {
            "id": "640ecfcb-a757-4b59-898c-219310e6cad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "3549c421-77fd-4676-941f-1a236203cffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "a972129a-3dbc-473b-8427-7a10b532d8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "55a41b04-b5d5-4877-8bd3-2546a2eada85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 47
        },
        {
            "id": "1f4962ab-26a8-453a-b592-2d10dd0d11b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 50
        },
        {
            "id": "7207de20-1898-4e1c-9aff-c7396e3527e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "4c797705-95eb-4b9b-bcd8-5470c40821a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "3b86ad92-a2b5-4656-9c95-4e093ae997b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 55
        },
        {
            "id": "a4e9802c-6eba-470f-a5ea-5ae7c4e43d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 56
        },
        {
            "id": "0bf6e85e-baa2-4775-bc69-ff0158a941d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "688118f4-433f-432a-b6b0-25d041afdbd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "51d39ac2-0c77-40bc-b96f-9fe508998993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "d8a3b073-465e-4093-890a-3dce1206265a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "efdbfe58-ab76-421a-a7f2-72f8468df651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 87
        },
        {
            "id": "4ce3fb33-0289-496d-b873-042546cfcb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "9eea7a48-8ad3-4487-903e-87ba328a2354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "4c7cb19c-9334-4ab1-9aa6-21fd213e4533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "9f3a07bd-444f-4d16-87c0-eb6168db4436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "1ebf2191-3480-4b45-82f0-e48db70e5f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "b047c2c2-f76a-44b3-aaed-3aad6afc3db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "562069aa-b96f-4ee3-8785-aa2395c2fb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "d895adec-49f5-4786-8f99-c56d666c1d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "ba16b54d-8362-4c30-b838-33e47696f8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "4bf7dd54-267e-4eb9-8831-4a5068cc7c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "c22411fb-2669-443f-89c8-4cae5723579a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "cc9a3d18-7462-4194-bcb2-8b3f0d82c3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 34
        },
        {
            "id": "81f12d5a-4a37-4b0b-b9f9-ec2d8d0baac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 35
        },
        {
            "id": "110ddf5f-1504-456b-9d42-2ac9d97d6cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 39
        },
        {
            "id": "3bf06f8b-778f-42da-9b7c-74ff2cc4832f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "b23bf348-a3db-4f49-9354-9b93e1f43f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "0448f0af-7e3c-4b59-b532-ff89f3c6ea2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "968e27e4-023f-457e-9aad-11cf158f5e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "b2e62e17-b8e8-401b-8ac6-98d1abaf178a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 49
        },
        {
            "id": "d6c05132-87ed-49d5-a3a7-586cc9a0378d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "bec2929a-6f55-490e-98f7-0d1fbb59a83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "28b36dec-be87-426a-a237-8d943bf9cac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "4f6e7533-9a04-458b-8873-c6779c1b5e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "c3143eb6-adbc-4edc-a0d6-847a9d9f9fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 55
        },
        {
            "id": "863f77ee-5025-40cd-b343-e533960bcf82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "25ba9414-5e96-4dda-ad71-941ede3a42f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 57
        },
        {
            "id": "c0270f4d-8ce0-4553-80b9-c1cb9f2f27e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "70b1a053-ff73-4c92-be20-da4fb5f2c33d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "60199f87-2f1f-48df-b1cc-6cafb87b7f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "71f16a79-b496-45fb-b975-0abdf7314af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 64
        },
        {
            "id": "594b930f-9378-4054-81c9-3a691c9c6880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "90f323b7-8b0d-4173-854e-4556e12a06d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "174492d4-d8f2-4e6a-b405-dca0bc59b36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "cbcd869b-99b1-449d-a137-df7f4c24a9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "325a2b09-24be-4d51-b91f-12bf3de1a61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "1c672142-5007-4453-89be-ea0b1e97351e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "1991298f-157f-461a-ba04-9224c46e2bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "9a3d7d20-2040-4d9c-9063-cc356a233866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "08224425-5f32-4bc7-9ec3-8222de6fe021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 92
        },
        {
            "id": "d0458dc0-e2ba-49c4-985d-2b9a2be13293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "5857d3f3-87fe-46b2-9b5a-00203f12b0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "93dedd5d-bbb0-4839-8356-b94f5bd2e739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "4fce767d-b468-4eaf-a1d6-d59ce50da8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "dd5d8cc4-bedb-4ad4-8bea-1a6f43b747c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "983bd0d3-614f-4025-8af7-b518fe60bea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "60f4447f-1951-43b8-956c-f37e9cac9476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "aebffd65-7b01-4903-a715-6de3805e7f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "88b2591c-dfdb-434d-aa52-9d370dfd9d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "39af7258-239f-41df-8a9b-0deb399d2ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "2c12c4b9-c522-4137-a04d-102acfe1f138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "58888628-6267-4abe-be0d-660568a5de16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "0bb6c056-c926-4295-b39d-3d4a4e090fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 173
        },
        {
            "id": "ef4fb599-113a-4d4c-9e7b-80adce7213b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 894
        },
        {
            "id": "f1ed5291-eb95-437b-a821-597b53d5d42e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8208
        },
        {
            "id": "73b6ae1e-6c8b-4f9d-ada6-08df04a4d1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 40
        },
        {
            "id": "4bbcc466-23f0-4346-8c51-93a5b98a2c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 41
        },
        {
            "id": "5f9ac459-6901-44a8-9be5-448878fb8b14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "00f128cd-3e84-48f4-8375-a7514f05dc48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "730a723e-1d5e-42f0-822a-901ba8df9722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "bcfaddbd-fec0-447d-9bd5-30c3dfff324e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "289d214d-1b6f-43d9-8600-0c06e1da2ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "4876a07c-ac4f-4d1f-a7dc-8e2fc584a20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "bf53fd9d-25bd-4aa9-8241-629a0b3815f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 55
        },
        {
            "id": "2608f6c4-2c18-4300-8fc9-52f50762da32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "a387352d-2ac3-4776-9a4c-50539695ea52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "4afabe49-523c-4199-b910-ca862d6193d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "7f25d65b-68fa-4e6e-a59c-4f5da9b0a1f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "72d2a99c-5246-4108-8081-ce734fb19da4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 84
        },
        {
            "id": "70a67c3e-184e-4d12-baad-8d7497a92ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 87
        },
        {
            "id": "69c82e25-805c-49e5-8be8-4a749c6647e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "e273a429-ab0c-4702-b8e0-aea699a341ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 92
        },
        {
            "id": "ae4f4c12-086f-41b2-9874-d5e42b87cc96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "d6cd134b-4194-4822-8be4-87e6026d4121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "03124598-8aee-4e59-aa01-d0ff444975ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 106
        },
        {
            "id": "e09c3b4c-b565-4bc3-9086-d9c7be5f6dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "c540333a-777c-4183-b5e6-15b07b0012e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "97c9eec6-c1db-45c4-ac9a-7d19647efb03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "504fd2df-a2f8-43ab-8b5a-19ad9ae15c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "fedf6776-9069-4229-847f-1a9ff51e7bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "c29e10e6-2ae8-4a72-8209-bed613d32aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "41001eaa-6556-49cc-ba9d-1a9ee00266f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 55
        },
        {
            "id": "3005d57e-8e8d-4d53-8d00-9c47e4597439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "2abde6e0-f5c1-4ceb-b02e-c580a981c90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "0ab73a3b-3cfa-4219-ad3a-bde6c5592552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 87
        },
        {
            "id": "8e985c9f-1e91-4ea9-96cc-cefc8ecd78f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 92
        },
        {
            "id": "1815e99a-b254-498a-8138-ce56cf115106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "89af9e57-3cc5-4187-9a2d-202d231cfa9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "374a6ad9-8c1c-4661-b7c7-cf1d3159680c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "dfb46354-39cb-40c4-8991-a6592e4788ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "e3abfe0e-e59b-4b78-9f2a-c6c40e3a8346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 35
        },
        {
            "id": "1f6f718d-661e-40ed-bf95-b48f53a0bb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 40
        },
        {
            "id": "02a989eb-bd85-4c80-acbf-e7b56278a120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 41
        },
        {
            "id": "d3278c89-497a-48d4-93e7-b7bff411c20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 47
        },
        {
            "id": "823c85fe-06f4-4e43-a1c0-456c04520efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "1d0ef066-31a4-42bf-98f4-d89ddd2584ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 50
        },
        {
            "id": "68dc9d8d-5d13-48b6-9bec-12861257ced4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "109e3787-bf47-47be-b7b7-bda125256815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "7bd7c9b0-a50b-4ae2-98b5-7c3f48059708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 55
        },
        {
            "id": "eabe15b1-1080-419c-a0c5-2fce32c4fe52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "3a854ddb-5030-4af3-9ec1-f1a59525ea1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "36667e1f-aed4-44df-b8df-f1a50165fe2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "55a252ad-ded0-4997-90dc-d8c509eb664a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "cfd4da9f-b8e0-4e3c-b606-1dd331b5a71f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "d9047298-0512-4395-992d-2d820b60817d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "dd244d5d-4d18-4fe8-9624-017c708db0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "a01a0343-6b1b-4724-801e-01a036f66728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 90
        },
        {
            "id": "3ce7a608-ae07-41fb-bee7-918616973e6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 92
        },
        {
            "id": "41342e89-d7ca-4e30-bafc-2eac29ac2a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 93
        },
        {
            "id": "72780b12-426f-4718-9160-dac83d6f9325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "ed12b713-d2d8-406f-8b03-fb1ecf8d89a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "07b34c7d-6da3-49a8-b79c-f16ca49fae83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "50f45119-9543-4ebb-8b25-d1b502becc1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "136bd056-6a6f-4cb3-8fec-bcd4036b0d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 115
        },
        {
            "id": "9e6f959e-7f04-44db-a7e8-5954ef92558c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "0ade1abf-f8d7-45fb-9566-fc2dd3bf8d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "5d1262ca-9425-482c-b566-021e9bb1a492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 120
        },
        {
            "id": "37fbf90f-23ad-436e-b8fb-4439c4a6a821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "31ba1c17-28a7-4554-8a32-11e579d8f913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 122
        },
        {
            "id": "54bd74e8-1522-4bdc-95d3-6106517441ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 894
        },
        {
            "id": "86836f10-5a86-49e0-8c34-b5aa2f4cbd9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "14245fc8-9c56-402e-96b1-6e8166ceecb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "97246532-d31b-47c6-94b4-1303da78fb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 52
        },
        {
            "id": "90fe9780-8a4c-46d8-b5ed-9d58916d4e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 55
        },
        {
            "id": "1a4b043f-81b1-47f5-ba14-d01aa67bfb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "5661b36a-1aa3-46f0-8e38-44738699f01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 84
        },
        {
            "id": "1c7633bf-e331-4a2d-89a2-4353a0d7e7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 87
        },
        {
            "id": "3c424726-8ce9-439b-b4c1-e481196e713b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 92
        },
        {
            "id": "90a6ac3d-4641-4abc-8d40-aa20da9c8877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "1972c29d-d545-4a66-b1b0-12f16d7c7e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "e5cdc113-6cb6-47aa-817f-ac1da2d865cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "ae71481b-898b-4a87-bddb-0791a9c491cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "02f0ef29-247e-4906-9b90-e2e8a68cb541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 40
        },
        {
            "id": "d841474f-0330-4756-8747-f71796af3cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 41
        },
        {
            "id": "2bd6be41-a01e-41ca-8ef9-459430a61065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "7f9eb94a-ba5e-4b81-985d-88ae514d865c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 47
        },
        {
            "id": "a0c1956b-4cc1-4699-9d1e-6a8afd0a72a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 49
        },
        {
            "id": "bceee9e5-3ed4-449c-8ec0-9228371c8bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 50
        },
        {
            "id": "d8e0c31a-fce4-466b-b6d2-c6e9b54bc471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 51
        },
        {
            "id": "0da4cf69-4d7f-4a1d-b9b2-03f2d2ac3c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "41d3d136-4ecb-4aa6-9583-fb34de067a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 55
        },
        {
            "id": "369b067c-b320-472b-9718-02218aac2404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "b6d6142b-ed50-4743-927c-2a644c4db774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 58
        },
        {
            "id": "a8070ebd-d131-4b7a-b77b-d43322f62820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 59
        },
        {
            "id": "1fb34564-2a16-4cba-9a2e-b843a9c98aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 63
        },
        {
            "id": "6dc34260-4463-4805-8eca-212ff5494c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 74
        },
        {
            "id": "cc2077e3-f0ca-4c4e-8f53-ce37b6dfe2bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 84
        },
        {
            "id": "40f16203-521a-4672-a523-48e94419a17d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 87
        },
        {
            "id": "9e47a5c9-5b1f-4918-a9b3-75e69af3c734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 90
        },
        {
            "id": "ee10111c-6f88-48f8-8008-d61b8da34165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 92
        },
        {
            "id": "9031059b-15ce-4945-8a53-422fe292bf74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 100
        },
        {
            "id": "25b4a5d9-be10-4d7b-9e09-172ebe1d24e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 103
        },
        {
            "id": "4f8b6b7e-5534-47c3-8267-91c1d7500684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 106
        },
        {
            "id": "63854358-38d5-426a-b2ba-1d3443a65127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 113
        },
        {
            "id": "4fe5f411-7d1c-4480-88cd-828fa9da5128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "084d948c-0608-4bcf-b091-e535ec31ab0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "7f990295-b408-48dc-bd37-8376cb8b745d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 120
        },
        {
            "id": "1352cadf-ca77-4159-b43c-270553b22170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "868bf9a8-9c04-4b8a-9bd5-f94a5a7b631c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 122
        },
        {
            "id": "cd442364-5083-4b84-852f-eb64be51fb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "6e4ad163-99c4-4806-a641-2cf507a0c28a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 894
        },
        {
            "id": "e0b268f1-cb51-4736-8b3a-d8839f080562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "379de02f-567a-4949-9173-f95f17834898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 34
        },
        {
            "id": "06703553-73c1-48d4-8f2b-f29cac272842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 35
        },
        {
            "id": "6d7d5b71-5f09-4738-be9c-d66bd3d44cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 39
        },
        {
            "id": "2beee2be-f4a9-4617-87c4-0e3791e4e60e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 40
        },
        {
            "id": "744f0583-a2ba-4027-8d26-5479cdb35384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 41
        },
        {
            "id": "4fffdc88-e8a4-4118-a2a3-c20edd9ccb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 47
        },
        {
            "id": "79a142eb-89cd-4e3b-bc31-72408410c450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 49
        },
        {
            "id": "765e0bb3-0ca2-4a0c-a435-7c028e84d1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 50
        },
        {
            "id": "b448d640-3bdc-49c1-8834-ca8e0aae3868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 51
        },
        {
            "id": "1f465991-81ed-4264-b090-3b18afe68417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 52
        },
        {
            "id": "de68d504-4389-4a5c-9454-f2c616304b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 55
        },
        {
            "id": "cd195a72-5a6a-4749-89b2-fd0b93760db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 57
        },
        {
            "id": "68fbebaf-32cf-447a-b87b-1076dd38f54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 63
        },
        {
            "id": "95fb0c57-65db-40d5-8e50-63f8fa103b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 74
        },
        {
            "id": "8dea60b9-1bdb-4b61-86dd-4e993f9b81c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 83
        },
        {
            "id": "c726637e-983c-45f7-a708-828bac4fb618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 84
        },
        {
            "id": "e74108e5-bf2c-409e-8f9e-05826fa5a5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "9157b81e-741f-42d1-8e78-7fa2aa1a12d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 88
        },
        {
            "id": "8bc3afa6-ef22-42ee-8be3-4b8cb73e7d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "8106fb08-a392-4459-bfe7-3e0357dbecbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 90
        },
        {
            "id": "0256ae9d-20d3-445d-a68b-54af046772b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 92
        },
        {
            "id": "1664afde-017c-483a-8301-a3828cc980f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 93
        },
        {
            "id": "9ff24006-94ee-4af0-b00d-a64ea3dda4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 100
        },
        {
            "id": "adbd95d3-4984-4439-bb5f-2a31270bcfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 103
        },
        {
            "id": "6e54e390-ac66-4423-8ec5-348abf43d8ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 106
        },
        {
            "id": "e2bae95b-cc2f-4866-acef-171a2f8319cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 119
        },
        {
            "id": "bcd5f02f-973a-4465-9ea6-4e5531ec591e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 47
        },
        {
            "id": "53417854-59cf-4345-9681-db35b97dc8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 51
        },
        {
            "id": "1aef7889-eeeb-4863-b004-de3ea2935457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 52
        },
        {
            "id": "4c5bac85-9ef1-4b5c-9e83-9dc9ffb17289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 55
        },
        {
            "id": "8861ee37-259c-432a-bf45-ae60d5e9376a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 74
        },
        {
            "id": "27634322-3c14-4c16-8264-06e4a13206be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 84
        },
        {
            "id": "db262205-4ed6-43a0-a8cd-bc0e0017a5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 87
        },
        {
            "id": "1876a99f-d0b7-4220-a04b-bf5bd0b471cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 92
        },
        {
            "id": "e326bad1-e499-47d5-982b-c31063413b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 100
        },
        {
            "id": "9303fe04-d9d0-4dbc-a061-ecb807b40e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 103
        },
        {
            "id": "8ef2b5c6-1aa8-48e6-9407-3f4a7354fe4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 106
        },
        {
            "id": "13c47a03-39f7-4d5a-a6fa-75c78353c444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 119
        },
        {
            "id": "1398209c-5da4-4e19-911b-dc7617a781c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "1a003b21-2839-46c5-89e8-6c14c88beaec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 34
        },
        {
            "id": "3b5b9ff4-b1b2-4275-b6e0-d13d817851af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 35
        },
        {
            "id": "1f44262e-fb89-4de7-a577-30eb1fe189c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "7f0bf501-5f44-4976-98f1-720de565b96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 40
        },
        {
            "id": "1ab0753d-1dad-4fc2-a019-db84b317a976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "107a7532-ea41-4f1b-bab9-8800793bae4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "74bdc59b-02e4-4c4c-8e85-29918b4d8d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 47
        },
        {
            "id": "dc85f95a-8ac5-473b-86a5-ec511d58f196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 49
        },
        {
            "id": "70113a19-53cb-4a0b-9c14-5d4daeb27ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "68ff8755-0012-4d12-8c32-45636b68c60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "1e2d0bf6-6c8b-4e4a-8c72-87e631767255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 52
        },
        {
            "id": "e919a257-1ba9-495f-9dce-5fd643c02c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "e81277f5-2364-42ba-b59a-a9767a56c0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 55
        },
        {
            "id": "dc279876-8d47-4ce5-8ca5-2af11a7c133b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 57
        },
        {
            "id": "780cc678-15f7-4a40-9faa-2edcb72f7413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "370ff799-6ed4-4a51-a0e2-b5cabd112a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "77839d0f-80ce-4e33-bffa-1db0ead1b168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "d202f29d-c21a-4c5e-b067-1daa0b73961e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 92
        },
        {
            "id": "5c9d0536-a89e-4417-a0be-1ed519074b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "0c9d4cbd-fc9f-475c-9d67-a639cde0fe46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "9b3d2abe-8c70-4cb5-afea-e6741175b8d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "def316b0-7d0f-4f3c-86fb-29e681c11aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "bc439c14-76d3-4b0a-9358-488f6233da57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "036c5d07-6438-40da-a3d0-0efd37fe21c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "06d93611-0e7b-4dd8-bfba-3a829c284fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "66647a3a-b9d1-44f0-8c03-6c5a337eccdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "1e976262-9294-434c-9479-a3ebaa80f19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "ce27e6e0-b3d0-4a02-ae15-62e82a40ba4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "fb5437fb-4c2d-40f7-a589-f2a44475c92b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 173
        },
        {
            "id": "476f1fef-0ab4-4f02-a5b0-cc4269b5bb65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 894
        },
        {
            "id": "74a57039-b15f-458f-9595-3bd66e167288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8208
        },
        {
            "id": "b52df653-25e3-4a30-b524-715c2804f960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "bb38590d-4dcb-4947-a3b3-d9acb4ba2588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "aa1d3077-927f-4ff5-9c3e-ac952288aace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 35
        },
        {
            "id": "3195b07e-d27b-4256-86f6-10a958df3807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "1bff2bf0-4adc-45e2-9978-eadd3f78c7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 40
        },
        {
            "id": "467ceec1-4e5e-48db-8e41-309aade21508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "455c9a09-c4b3-40d7-ac00-e2cc51b108eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "0db74703-21c1-43df-a619-1b559cf210c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 48
        },
        {
            "id": "b6241314-9f99-4610-94bd-09224328f0de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "18ec5e40-24c3-4e43-b18d-bbc5e9d00375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "b1f1fea4-1204-44e5-8788-7730deb82fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "3d7391e1-1f62-4362-9a8a-7d85e536b6c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 52
        },
        {
            "id": "816ec623-4ff9-4cec-be10-4162f13517cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 53
        },
        {
            "id": "73ea6023-6872-4642-975d-0fa63f966688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 54
        },
        {
            "id": "8964a2c2-b4aa-421e-810a-000586d30682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "8ebb1487-d6b4-413d-b2a8-cffb7e322fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 56
        },
        {
            "id": "533d6739-da05-4077-a732-4458533ea730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 57
        },
        {
            "id": "79ddedcf-ce50-45b3-aea5-376b0b488cbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "506f99e2-ce09-49fc-8c2e-5a982abcebb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "a28569e4-fb35-4849-b43e-89da955ef611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "f9364908-ae6e-451c-8dde-e1ae90cbb0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "e3ec0145-47c7-43da-b861-a1dfea98a57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "81e76ffe-31f4-481b-8c82-3011a658aa2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 100
        },
        {
            "id": "a59e1aaf-6a4a-48d5-a0c5-228c0b3c9dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 102
        },
        {
            "id": "97fa8be0-0f62-45c4-9277-d48ea9f021a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 103
        },
        {
            "id": "88fcf8f4-138c-487f-87a9-03edeb3817bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "08bf6476-bfe9-4243-8d24-6da5093ade58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "36f36467-58a8-42b5-a0f8-704f2fbdfdb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "e1b15f22-2801-4d66-8ccb-2dd1a3fbd463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "41278940-c6c3-4b11-9a48-69a3552f0a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "f609592d-b49a-4e96-b7ab-1a4c7b20950b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 113
        },
        {
            "id": "9a265a12-df4d-4e31-b21d-471184b186b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "594fcd46-ee54-4553-b642-73f6844d7b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "0f8fa48e-f5d7-4f24-83dd-98a4caf01692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "c17a6b1b-ed9c-435f-9174-9b1431f6d0c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 41
        },
        {
            "id": "441fd448-f8f4-4b64-af82-44eeb6c66f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 45
        },
        {
            "id": "79fc8038-e51e-4bda-879b-46134ca83b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 51
        },
        {
            "id": "5c344da2-89e4-43c9-bb1f-bd9c410349fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "c191d3b0-5808-4405-8d1d-fc5c11d88287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 55
        },
        {
            "id": "e097bb34-8972-40a6-a676-4c96cf991650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "fd61b079-3980-442f-8223-5c82b4be6466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 92
        },
        {
            "id": "7f77fe4f-4ddc-4bde-a557-4a26e71315f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "8f1ef92a-d941-45dd-a4f4-346e7df25a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "e3325de7-4811-4571-8629-2e604014484b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 173
        },
        {
            "id": "f8103177-78c6-4b17-b31e-56cfbf66aef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 8208
        },
        {
            "id": "402de306-0646-4b35-a58e-171a9340e14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "c7cf877c-d64f-4d4b-8292-d63aceed1b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "2232a4c0-560f-4381-8a2d-e8e7cb0c895a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "6ccecca6-73b6-4d73-9387-7daf3386e91b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "e1e0dba5-bfb3-4531-b1d4-f0451b1262d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "1c9751c9-a48a-4f73-b2fe-759821e979d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "3f8b6f6d-d347-4a09-8e6c-a4bb07bed84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "5cf51f01-4bed-40aa-92c1-73c70778b3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "47c12ced-8dc3-4dfe-9717-d410067ebb3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "8a03558d-d951-4d95-98d5-dfe56a32f611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 35
        },
        {
            "id": "d1d93d25-e5e7-4f4b-a97a-de3cc4ebaf4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 40
        },
        {
            "id": "e2b20bc2-3ea6-4453-8aa6-97d413870c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "719c0be6-ef3f-49fd-b761-89675c0faaef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "56313cd5-03e3-4827-929a-c825137983c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 47
        },
        {
            "id": "ade94111-a60d-405a-b55b-b128a7d7090b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 49
        },
        {
            "id": "d8862e56-042d-43db-9e75-a5231eb668c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 50
        },
        {
            "id": "b47617aa-ae3b-4238-8fff-22d42575a0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 51
        },
        {
            "id": "e00820c0-7a1c-4a19-b82f-1dea474987b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "f5ffad20-82f1-40f7-bb24-1557291b4872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 53
        },
        {
            "id": "c563aa26-7dd6-49ef-8266-fba743311d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "f8a56b27-b849-4cad-b756-7bef0553de4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 57
        },
        {
            "id": "5d3a1fcb-0a80-484f-8476-cd138c6c8d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 58
        },
        {
            "id": "a8d866da-71d5-48e0-92c4-6592ed3e941f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 59
        },
        {
            "id": "4e40df5a-8024-4479-be3f-b243d849eafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "4ac9c42d-d9cb-4b63-b765-60599b63639b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 92
        },
        {
            "id": "ae5a738d-4a0a-4b8d-82d4-c3d2886bb72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "3ec934d9-6562-4e05-ba77-3151e49f2928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 100
        },
        {
            "id": "efc5d61b-1d13-4fc0-9bbf-6b6748d5ea65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "805daf7b-9374-4429-9aeb-8900afea2183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "52fea1db-7963-4bca-9e7d-78731e3fa304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 113
        },
        {
            "id": "1cbc61f7-f5ae-409a-9a91-6d1e2be2be9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 115
        },
        {
            "id": "813b16da-8095-4c7c-b7ef-8fce70c3c6ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "be04faf5-f907-4f9f-a823-31edded75376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "55a0a5ba-a12c-4104-8c05-dc00453a3d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "03e39259-eb17-46af-8fd4-6baf4f9d79c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "d5c3529a-6e28-42bc-9254-6b655c736812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "da68cce1-d93b-4785-99ba-e9ffe1804d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 173
        },
        {
            "id": "f679c7d9-e020-489e-a49c-a8b323702c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 894
        },
        {
            "id": "628fae93-59d8-4886-8283-cd0eb3ad9e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8208
        },
        {
            "id": "69740e65-8a83-4a48-a6dd-28d2c3769fb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 34
        },
        {
            "id": "f0f2caee-5516-4994-a6b1-5916035f519c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 39
        },
        {
            "id": "4e895047-0b68-41fd-a771-719d8ff6463d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 40
        },
        {
            "id": "96fbfaa6-d8ad-414f-adb1-2ae6ab49f414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 41
        },
        {
            "id": "01d5db97-fc0d-457d-9510-74b7b727bfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "14c1e9f3-6e86-48c0-80f3-bb45ac354a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "73f282f9-8aab-4eb3-a720-4d5fd60e90b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "696eb78d-b14e-478b-89c2-9adf2e5e4a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 49
        },
        {
            "id": "2acd030a-4f17-4a92-be94-aa9ae7a1d065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 51
        },
        {
            "id": "4f9c16cf-58d0-4258-97f3-18e40f2ef26a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "fda7919e-9dba-4de4-908d-b99d7c150a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 55
        },
        {
            "id": "7a7b8c37-7625-4512-9df2-080615c44884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 59
        },
        {
            "id": "a69922be-8de7-449e-bd10-e6105bdeb2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 92
        },
        {
            "id": "9fc92a4c-cf8e-4140-9fcd-baf8faba997b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 93
        },
        {
            "id": "583deced-1ea0-4b63-b80c-45d69843e81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "53fd40dd-97c3-4f36-9005-00f695931b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "072a6594-6277-44cb-b348-c5ac6e2e7bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 106
        },
        {
            "id": "5e531501-ca72-40a4-a556-09beb854520e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "deb0b111-283f-4b40-87e4-dbaaeab197c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "3c7c844f-69e5-4a02-885b-e5abc82887ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 894
        },
        {
            "id": "c06ecadc-f87e-4d0d-907f-2bf4cc64c6f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 47
        },
        {
            "id": "f15dc724-d244-4ba7-9621-98e40bac959d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 51
        },
        {
            "id": "5b709318-ca0c-4cad-9180-72d086357476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "851261fb-9d85-4f56-912c-d09b6ecd5811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "d61cdb6e-59e0-4522-9030-d558a2cadc9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 92
        },
        {
            "id": "81a2efe9-783d-47da-95c8-5a06b03c522b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 100
        },
        {
            "id": "23d306d4-2129-40b1-a2e8-b775da95a302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "b4b835df-e244-4f09-8af5-0e713a45f54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 113
        },
        {
            "id": "689e4d5b-b330-4e0e-9778-e6c556a199dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "e2699642-8d17-491b-be07-3ff64d99f96f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "fc713d72-6c10-462e-a0f1-6e2032ed7ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "ef14d6be-0844-44ec-9f42-632afa52356a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 35
        },
        {
            "id": "445e414a-ef2f-4ebf-a32e-dadf7a19078a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "bedc515e-8cd5-4c77-bdc3-79377da4aed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "95fd6478-d148-4e14-95eb-e200c31bad5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "9082e4a0-1525-4494-afcf-15818c30edbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "2a60713d-b552-4d37-b55c-8139cb18c5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "7ff33c4e-97af-422f-9f75-92715e117261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "f6a649ca-6175-4c68-84ae-3e0120fa08dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "398fe4ee-fd36-4bf0-b17b-e17d4efd00a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "b55582b2-e90b-492e-a227-5d3c731d8995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "0c7a2e3e-83ee-45a9-aebc-fcc8f1178053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "e1b54e71-b4be-4e18-b49e-e090f1d3e531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "f57ae668-b40c-4146-97b5-c5483596b155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "bd090777-0b4b-4c51-b625-547810468f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "62cba84b-347d-428f-a1d4-f7933d89c564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "2063bc2b-2cc9-4b72-ada6-af9ada174865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "bc183784-6dbc-4afe-98c6-b6775a468ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 64
        },
        {
            "id": "8d00ced8-5c47-4c06-b714-32bfb0e9b7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "52cb2275-d32e-4e66-ba08-1cafa3350e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 93
        },
        {
            "id": "1bd8e55e-8487-491a-af86-8d344f0db7bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "6d5fcf83-ce77-4d04-b6de-6992af182095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "4bbf83c9-a154-4adf-bab9-0adf6ca6007d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "e37ae4cc-7fce-4991-a9c0-dd81716db26e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "3877b612-7af7-4f3f-8d65-5a33fa86edbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 104
        },
        {
            "id": "5f526cf3-2151-417b-8790-c9794c694c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "4a4f76c3-2b88-4502-b514-4b3c1a5b0116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "f6e17716-e861-440f-9c6c-a2b743f43916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 107
        },
        {
            "id": "22030b31-337e-4e61-a242-74d22e4c787e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 108
        },
        {
            "id": "e8496382-9627-4326-8d5f-c16c1fa034e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "651c2fcf-a797-4fe2-8f23-c7cc309221f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "331a815a-1130-4c71-9d19-a653b5e2af97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "f0aee789-1216-493d-a4cf-75a3346d75e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 120
        },
        {
            "id": "78aaaa7b-f2c4-4e82-bfd1-f6f50578882d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 40
        },
        {
            "id": "f7abda11-c536-434f-bb6d-8837a83d857a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 41
        },
        {
            "id": "abfe987e-e2b9-4e99-b123-4233f3dee359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 47
        },
        {
            "id": "0372477d-e528-4e34-976c-d176501f664b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 49
        },
        {
            "id": "2bf1156b-25cd-4636-b8bc-8cede8516e68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 51
        },
        {
            "id": "f41c3553-f9bf-44a6-a0a1-e4376926bd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 52
        },
        {
            "id": "aa9560ae-0183-46c0-8c5b-71c5118c4c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "25cdd89a-62a1-4fd6-9903-cc8d089d634e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 92
        },
        {
            "id": "65029b28-11fe-4201-8755-eaf603407678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 100
        },
        {
            "id": "c45b554a-ff73-4592-afbc-4abff4e5645a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 103
        },
        {
            "id": "69d0778c-0950-4fb2-92f0-7b745c46bcf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "f356d541-2519-4221-9772-b0233ae15011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 119
        },
        {
            "id": "0aeec35a-125a-4c23-b6ab-d7159fde6f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 34
        },
        {
            "id": "205f89c0-6e22-4043-9d4a-fa246c4a7995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "e671a81a-d9ff-4d97-a9f4-9e1565fe6860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 40
        },
        {
            "id": "a2ab835c-bb89-429e-b41d-f75bdbdc800b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 41
        },
        {
            "id": "cb0e586f-bae5-436b-a0a1-429d0910550f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 47
        },
        {
            "id": "bac32d9e-501b-44eb-bd7e-edc4e83221af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 49
        },
        {
            "id": "95f817f4-9461-4035-bceb-8bbe382c3c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 51
        },
        {
            "id": "f26f46b7-bd66-46ae-8f85-2deecbae8447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 52
        },
        {
            "id": "d67798f2-7b3f-4c91-9b5d-51abe4db223f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "fec8b5e5-a92f-4741-abb5-76e26304e85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 58
        },
        {
            "id": "0dd07b44-b005-444e-8683-1c8ec3d8e622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 59
        },
        {
            "id": "651feb1f-381e-4691-8fc5-173e426b3231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "bb445fb6-5768-4081-b4e7-3f0374890948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 92
        },
        {
            "id": "e7a89a36-4770-42f0-a236-6f412cd2bfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 100
        },
        {
            "id": "dbc7be38-c768-4b8c-b826-c5b3830f3e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 103
        },
        {
            "id": "8a5cd9e7-b027-4645-8fbc-46bb48053cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "3716d595-f454-4c5d-8eed-dc2f2ce87950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 113
        },
        {
            "id": "c71e5db8-b6e6-4107-9f63-c0599771acdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 119
        },
        {
            "id": "f0ced632-3af9-46eb-979e-2d6cc685ed3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 894
        },
        {
            "id": "f639b12c-ad48-46dc-aab1-af9aacae4d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "3f023806-73cc-493d-b745-75c9f5c865cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 34
        },
        {
            "id": "2f030458-32d6-439b-a4d3-fc52dfac61ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 35
        },
        {
            "id": "b16a858e-4e64-493b-a92d-df4db26486ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "749bb645-be88-45fa-8c04-e97a26bbe146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 40
        },
        {
            "id": "6c97b182-3665-4f47-81ac-72c3330e2856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 41
        },
        {
            "id": "746e48be-e13f-46ad-9d81-d93f1dac6eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 47
        },
        {
            "id": "d3d60576-5f13-4ba1-be24-4f115dd47ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "3eca2735-c6ed-4e93-92cd-8ce50b219e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 50
        },
        {
            "id": "82c4fd69-876c-4873-bd01-971cc7dd63e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 51
        },
        {
            "id": "5d74aa3c-3808-4acf-be8b-dcd620a953b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "d2e55678-fbb7-4f8d-b20b-f6b26f24af58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "6ce324e0-ed0c-4c7f-9f94-6125e208ffd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "6fb7a587-0c1f-44e8-a665-c56415bbeedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "90a29e3d-adf8-4303-8216-967ebc2ca4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "b69764b3-34a8-4c8b-af80-105515412431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "1a62bd8c-1c03-4fd3-8b7a-aaad3d38a385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 64
        },
        {
            "id": "2f3d5e7e-e9e1-45ad-a1a2-a1809676e93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "a79d4609-a864-4ecd-a3d5-439e1d7d3e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "f1a26ee6-71c9-4fc5-b57e-feb017c75ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "873b9690-2175-48f7-b9bb-3bab2df66e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "9dab2bb7-0b16-4baa-9df8-e01b8bbcd0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "74f8c447-b225-4d8a-8c71-1d78a79b022d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "71847365-4c32-4edf-91d9-2fd7fb0412b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "41b5f602-cd1c-442a-adf7-fcc7f101699f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 33
        },
        {
            "id": "04f22e58-f351-446e-9936-0fdf4ef64e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 34
        },
        {
            "id": "11c97bb6-530f-4d3a-b05c-b16a5e782f73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 35
        },
        {
            "id": "b0470e18-1ef2-40d7-a57b-8e13ff576b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 39
        },
        {
            "id": "bbc933d5-2702-4911-bccc-ef8e1c4842ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 40
        },
        {
            "id": "ac334a64-f60a-40b6-b453-fdad6307d536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 41
        },
        {
            "id": "7bef13e0-51bd-4fc6-a6bc-7110404a33e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "f368ac91-0e03-43bd-b127-ac607d276941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 47
        },
        {
            "id": "8ed2c208-da78-4837-8f33-79a69d889102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 49
        },
        {
            "id": "46fe0fc1-c8f7-4db9-846c-64ee58ffa462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 50
        },
        {
            "id": "07649867-a913-4c4b-9085-9c2aa1dadcca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 51
        },
        {
            "id": "188a5e9c-67a4-4192-9807-211e411e3de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 52
        },
        {
            "id": "fa9f1207-2c09-4507-be16-2ffcf4ab7ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "81aa2cd1-637d-4106-b92c-58820c94d6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 55
        },
        {
            "id": "05f0b489-5434-40ec-83d3-d7b2ae72b535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 56
        },
        {
            "id": "6633bbff-0102-41c3-81f3-fcb00abb0a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 57
        },
        {
            "id": "320823d9-d0d8-409b-a308-6af85d847dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "8c5e503f-33a4-4e37-8775-bf3070b80892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "d7ba72d8-0d5f-47d0-9665-c05c90976f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "c00ea0c5-6cdb-40b3-a6e7-2c91ce05d541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "4baa55db-e9e7-429c-bf4d-f02a1c038710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 92
        },
        {
            "id": "2ca66c67-a990-42a1-94bc-56e96e51df1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 93
        },
        {
            "id": "82cc7530-38c0-4344-8e8b-2b5ea791768c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 100
        },
        {
            "id": "f697c5c6-cd44-4600-aeb5-c193536372f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 103
        },
        {
            "id": "89c86cdd-1005-4b79-889d-0f690438029a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "cb1d6a7a-711a-46e6-aa51-087127bdcfd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "c78ab040-b77b-4f8d-a877-9f31b7f3af23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 113
        },
        {
            "id": "5a140fff-e5f2-4a98-9067-f51abc9d4f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "b669688b-9ac0-4067-89ab-c4deeccca8d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "a4ccaf29-ab04-4e6e-9615-9f8eff5c76ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "9b8c7111-d876-49ee-a7c3-18869f70e5f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 120
        },
        {
            "id": "d5ee6977-54bd-4b9d-9e49-d8daf5f16947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "3a8790de-6510-4478-98c0-56dc4833470d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 122
        },
        {
            "id": "efeee88c-35ce-45e3-98ac-d45c06310407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "3a6e881d-0bdb-41de-9549-63720ec85124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 894
        },
        {
            "id": "5ac3896b-3db2-4f55-bbff-a95c8e6a3e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8208
        },
        {
            "id": "cea5db64-f751-411f-86ec-e488b2a772f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 33
        },
        {
            "id": "5f628737-0bc4-4128-af10-4b8b419f8acb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 34
        },
        {
            "id": "925c69b0-3ce6-435b-9fc7-6011dc6dcc3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 35
        },
        {
            "id": "9a81b3bb-c148-4b0a-9662-9c4cc1934602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 39
        },
        {
            "id": "08470a66-89e7-40aa-9fd8-a56d1a709b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "71106dc4-1730-46e3-8c82-148ca3e2404d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "6f2d6101-eda3-4092-a458-146a2bc3ccb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "a05ca7af-b3f7-4a60-89e7-9bf4caad35a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "7d41df01-ac03-4885-bf49-f588afd3e8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "620a5e60-6179-4131-a074-736b6067e4d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 50
        },
        {
            "id": "8515f495-565d-4d2b-9c1b-b432e4eed4c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "4f5c5f55-95d9-49bd-a0de-ff1690bcead1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "eb0db623-531c-4caf-9730-123581ea36f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "c65d07e8-6aed-4ebf-9dd2-eaf4474dcac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "4b22ca28-4cf2-4d90-9163-18460102a220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 55
        },
        {
            "id": "3c670df5-424a-4221-a495-e353ee432ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "b9305f74-9d44-44cc-9757-6956e7813ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "d9023bee-5478-47e6-9202-6709881c425d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "039b17cb-0f13-48f7-8d60-94af1490aeb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 64
        },
        {
            "id": "bb0eaef7-81a0-471a-a9c0-fd71a8166a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 92
        },
        {
            "id": "bd44fa49-ff76-4e9f-be8f-df6f6ff709a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 93
        },
        {
            "id": "ba87b46f-e320-43e2-8669-a9e44218a57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 98
        },
        {
            "id": "15bf2a09-1e54-4e2a-a54d-4ca018d03ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "f29ecbfd-aa7c-4ff7-97cf-10882b0f62fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "b0319692-8c98-4aee-990b-fc23e7915f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "d2141abb-9833-4865-9fa3-c71fc2339ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "b6c4ad50-ca9d-4235-b7e1-c1bd20e74d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 104
        },
        {
            "id": "e6cb514d-5ce8-4788-b154-3d0493acdef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 105
        },
        {
            "id": "ed14b925-aa0b-4f4b-9dd9-da20006c8c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "a68b2f8c-ba96-4947-abb3-aaa2b88a8a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 107
        },
        {
            "id": "cbda993f-1502-4ca9-a58e-07b5c6953453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 108
        },
        {
            "id": "ac90aad2-ea03-40d3-a5df-a310a69a8118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 109
        },
        {
            "id": "792741ea-d24c-4abf-888d-209cb3825481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "5ab35785-e90c-4ce8-ad3b-28e8c2b02d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 112
        },
        {
            "id": "a1fcadc6-3b75-42ea-9eca-7071904fa09f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "a40c7e80-faf8-4180-8efd-b79100b7c044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "db60a82f-0864-4950-8e52-27ef5cea7791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "53312b22-a415-4675-ab8c-fbbf431e3cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 119
        },
        {
            "id": "3e18d032-6031-43e8-a23e-beed118ae7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "89634685-2c13-4289-a2d0-61f9da43a0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "df31469b-3026-485e-a912-da8113d70407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 35
        },
        {
            "id": "3113ba29-8312-4e39-bf80-b2ad86a5baa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "ce159489-9796-48f8-8da5-0c629e0abd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "c03a337b-f4b6-46b5-ab1b-600ec3edc3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "d435c458-33ae-49a4-9aad-d7ef117e8213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "3cb7c24c-cc1c-41ea-ac8f-58fb0ed2b42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "a6986fa0-cfaf-472f-9391-352b44e44b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "c033fb94-aad9-4939-a76d-a6fa23b2e8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "568547fe-d767-4f2f-9cb0-94073fefa8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "93aa5df7-c0cb-4eb8-9336-91f2a7eca813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "457f5e98-8ba5-45ef-accf-d056f22b8951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "456e80d2-4bda-4517-b918-6245aacbb819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "8bf0c51f-571c-4f32-8eb6-cf79b5605330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "7400055d-9059-48c1-bf8d-527ef7bfd6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "5d7bf1c6-f9bb-400e-a978-5068efc098ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "7ba57e2c-d729-4b76-be1c-c48577a9cf15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "42834012-01d9-4870-b57a-da2b98071054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "8a8ce548-7e27-4af1-b3a9-2ef8603d6bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "72405b4d-dca6-4534-8bf4-99b003562b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "d195c838-97b8-4710-8480-bdd61848c9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "c5e90075-12a2-4178-beb1-6c51657cf026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "a7da1e2f-1462-48f8-b6e7-2daaebe89355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 120
        },
        {
            "id": "e7308364-d2fb-487f-983d-c48861d86cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "062d5306-c5dc-4692-8275-393fbbffcac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 51
        },
        {
            "id": "85d73ad4-cc80-4d0a-a54c-bbc1b2fd8884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 52
        },
        {
            "id": "72aca75e-b929-4c06-a698-ebf452dad926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "67972c19-f27d-49b2-872c-5a4471e94367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 92
        },
        {
            "id": "1c38acc5-dc33-40db-9c4d-e70f519325ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "65c53e32-09a1-4e2b-a1d5-4a94cc691034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "4803ce9b-ee21-4a34-99c7-417a3a3c6302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "de2008ad-4192-4ff3-a3db-0e1428c174a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 35
        },
        {
            "id": "1f107669-f7d2-4d4b-b36e-cb831c832672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "2c2c9aa6-d3f7-4feb-b9b6-ebc7677a9f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 40
        },
        {
            "id": "8cd0cf71-39b9-4501-8ada-811b1312793c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "9cb6549f-0d6c-4c1e-92a3-66710cdda94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "0798a0e1-a703-434d-b72e-452a7965158f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 47
        },
        {
            "id": "e0e2acf6-889d-4010-9fa2-3a03c506143f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "71991413-1818-40ee-8b99-b7bfa535f1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "b835391b-67f4-4eb6-a604-976fd793e172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "26d891fc-d89a-408f-a0d7-1c6b478a120c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 52
        },
        {
            "id": "d33e534e-b129-40e0-8482-f33dd7a75927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 53
        },
        {
            "id": "90703802-6533-4743-8ac3-c814480226bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "9abbeef3-4c2b-48ff-9f75-dcb4463da325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 57
        },
        {
            "id": "0b399d85-913b-4d5b-b511-3615796ff050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "b06f0348-d10d-4880-9f51-380ca19a8d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "56246f1e-c883-49dd-90d8-498045eb6e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "0cea2a3a-525c-43b2-9a43-1553a0744278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "039286ea-e685-4907-a803-ce22d3132f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 100
        },
        {
            "id": "e471ecc9-6e02-470c-a4f3-5c19666641f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "ef1baf51-9df4-4395-952c-0f68a600d7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 103
        },
        {
            "id": "501845bd-ce98-4afc-b91a-05fb3f297b5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "abf1d3e1-c58e-49a2-9ebd-680d0f2f06e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "19e9212a-8b3b-4c17-af21-aba31b37e4e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 113
        },
        {
            "id": "63f22b02-8399-4d43-a5a3-885acf3e6ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 115
        },
        {
            "id": "797b164f-d418-4a10-9309-c07ca37bd96c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "89a96e78-f9a7-4096-b4f8-0b195efc208a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "b76ae972-bc91-481d-82c1-a65b2e9ae768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "8095851e-e2a9-44f1-a1e6-394ec2e342e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 894
        },
        {
            "id": "31ab6e15-8c61-4b9b-83ca-4bcd62feed72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 41
        },
        {
            "id": "f7670390-d48c-4bbe-a89b-6542bd18bd2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 51
        },
        {
            "id": "5e85f62e-43dd-4ce8-89da-7061bcaca045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 52
        },
        {
            "id": "0bc37c81-ccae-4acd-9505-2496c2a1db43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "2d899b91-4342-49d7-924b-78ef1992cd2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 92
        },
        {
            "id": "4dbd96bb-d16d-45ab-b6d8-be41db3f054e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 106
        },
        {
            "id": "ec4e6734-f94d-40a0-89fa-71a99e2acb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 113
        },
        {
            "id": "6db153f3-a2e4-4bbd-9746-9e32aa7a38b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "ebe9a338-df92-4360-b2de-3e4aae683669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 33
        },
        {
            "id": "0e1d8b52-f582-473e-a043-5711b6d425c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 35
        },
        {
            "id": "7803d3fa-76a7-480d-99f3-ad93ae8161f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 40
        },
        {
            "id": "f0143935-af52-483b-984f-8701e7a1246d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 41
        },
        {
            "id": "41372c77-1321-41bd-b28d-9209930a1f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "fda5507b-0e90-47f3-a973-3b2a4929f0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "cc6c24c2-10b9-4224-8f26-85fca644b887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "efdb4368-67e5-4a32-b0ca-fbff66ff1e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 49
        },
        {
            "id": "c946efb4-167d-4f3c-b7f0-fb7b7723db5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "c072c4a1-a871-47e5-8f30-062fd43795ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "727efce3-02a5-4146-8252-ea337589ef1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "d7b9f97f-93af-4c13-8447-30cacd82118c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "4e69a93a-b8de-46b2-80c2-0a81f8a8c073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "078a5ab8-0c77-4f85-8d56-c69fe8fd22a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "57e1afc7-4a4a-4b44-a645-38899652fa5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "481eee93-80c2-4b97-b2e6-5bc720b69e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "23bd0a55-ee6d-40e9-a6b4-59b95400f953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "bf5038ad-3d10-475d-8868-1d1a9eefbf59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 92
        },
        {
            "id": "540bcea6-809f-47b6-8fa2-18490248182b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "75300eab-1ded-49f9-aed2-f4fb1db9111e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "8600db8a-416d-4171-9692-cd24eb108f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "317ec9f8-1e73-417f-a058-268668938921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "249fa910-dc8e-4f07-baed-92cc4e9498f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "30d5ec68-30c1-4021-b558-a3926f34daf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "423fb5e5-a747-446f-b046-20ff24ca3400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 120
        },
        {
            "id": "5e873a99-d3e9-4152-8b43-e88d7c979a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 121
        },
        {
            "id": "b0741352-beae-4a71-8fcc-502b62b096f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 122
        },
        {
            "id": "4201317e-aa01-4836-9aff-79e844a59e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 894
        },
        {
            "id": "78a1facb-f352-4da4-a312-31d71893bae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 33
        },
        {
            "id": "a1abda44-6cd0-454e-be39-acb083949e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 34
        },
        {
            "id": "346654bd-1b41-4501-b14b-b5b746e6b923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 35
        },
        {
            "id": "2978ac58-662d-454c-a4b1-edc39da51479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 39
        },
        {
            "id": "ab6f1f8c-5db9-4f61-b15c-673cdc8323a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 40
        },
        {
            "id": "d743b81e-7f6c-46a9-bd54-a34f39ba7dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 41
        },
        {
            "id": "a3d8fdff-1f51-47f4-8896-f3ab3022b3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 47
        },
        {
            "id": "2a910af8-8679-44f6-b546-21df3a03ac51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 49
        },
        {
            "id": "dbfbbcef-d155-4dfa-9ffb-3e80dfca2121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 50
        },
        {
            "id": "49cda9f9-1efd-427e-8e66-43578c601479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 51
        },
        {
            "id": "5a30f81b-54d6-4055-9a2e-88a7d7d74d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 52
        },
        {
            "id": "50235ba4-a28c-4255-a80d-a613e4df0923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 53
        },
        {
            "id": "ae436c3c-a268-4548-a15f-9ee6d3f8a95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "70c41054-f99c-4334-97d7-6c978fc6fe61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 56
        },
        {
            "id": "4581ce85-0338-4faa-9204-e9d5e73aa19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 57
        },
        {
            "id": "0308ba1a-128b-41f3-8190-27d8642cd1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 58
        },
        {
            "id": "14fbcc8f-ebda-48fd-b492-a5f24a37ac00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 59
        },
        {
            "id": "8b556698-a2b9-42dd-956e-d0a937bf384e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "eb87ba19-6e69-47e0-99bc-0d25998943fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 92
        },
        {
            "id": "d2aca4cd-7e53-44dc-9f09-3a5c6c51f068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 93
        },
        {
            "id": "05797d98-f947-47d5-b4eb-075ff866f9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 100
        },
        {
            "id": "aab778dc-163b-46b0-a286-35b759879477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 103
        },
        {
            "id": "b4bf0194-7bf6-4c60-88ee-200409a64268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "a5224b0b-03f7-4db5-a7db-22ed27b1e45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 113
        },
        {
            "id": "7623c3ca-077a-4514-964b-2e4dfd70d6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 115
        },
        {
            "id": "d539adff-f8ae-4fdb-a3a0-87bdbec99b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "c45bf8b5-99df-4e34-8a38-c02d232e3aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "3a8048f3-3513-4b32-adf6-19611a5e6d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "cf04c3b7-37fc-47ab-8fb8-d3560468b776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 121
        },
        {
            "id": "c8ad692f-46e5-48b9-bc82-1394213f2a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 122
        },
        {
            "id": "22ed8a73-1188-4a72-ac1d-261d6de8926a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 894
        },
        {
            "id": "dc22cacf-fd51-4757-a2ca-668332d99b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 34
        },
        {
            "id": "86fd2fbe-1b44-4216-830e-ef0f57036abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 35
        },
        {
            "id": "0273d90d-a9b3-4647-8bc5-6a719d85b345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 39
        },
        {
            "id": "ad320b5c-8f82-4ab6-a65d-e467126d4813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 40
        },
        {
            "id": "826a5d7b-a6a0-42b4-a93d-3d5000a3938e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 41
        },
        {
            "id": "07d08771-7371-45b0-aaf3-a9dd4ed69677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "c511ecee-092e-4964-b59d-95ac0b520cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "0ad595d5-8a28-43d0-8d32-abe1691c9997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 49
        },
        {
            "id": "53928454-20e5-4baa-afd4-fca3f8938bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 50
        },
        {
            "id": "e90d020a-5039-4593-996d-288d45025deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "aaf31406-cbd9-4062-8509-693d8cf8c1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 52
        },
        {
            "id": "2f0586b0-f5b6-4904-97b9-2ed2c8da3b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "b20acaa9-d1ca-4832-80b7-0240929182d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 57
        },
        {
            "id": "cd3c91cf-1096-4faa-a33f-147f1b1e8209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "5dac1b81-6ddb-4bcf-8f5e-a5cfd04359e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 92
        },
        {
            "id": "7dac9532-cf5d-40f0-84ba-01bdb150af28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 93
        },
        {
            "id": "e88eb54f-fadc-483d-90bc-413216d0388b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "98490eae-e4c5-4b1c-8d4c-157260fc5c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 103
        },
        {
            "id": "37f5bd92-566e-44ab-b718-c328f5a05f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "1ff55f6d-1b8b-4c45-aa63-57426e047ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "23b2d271-950c-40a3-ac2e-d6508a756150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "c88903d4-b82b-426f-be51-7a1d51d5c66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "7fa3d33c-df1f-41be-a3bf-69fc08cf2a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "1b99cd22-2f58-4c50-a98c-5e33d6c3ab63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "8d796c5a-d371-48d4-b047-8ba7f92ae1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "a809bda1-ed60-4823-ae68-1de3ecbec93c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "803df174-c8ef-4646-8630-fe24edf4d77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "fe2b7704-a15d-4913-b37e-29e226557aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "8f1ebd29-b457-44f5-b493-62cae320a6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "e19f1054-a383-4fbd-a12d-cdb12cbb038b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 41
        },
        {
            "id": "cf4e0627-bfa9-45e8-b0e1-78fbb1a1dea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "e4da4ecc-8a9b-4424-a32a-3f951d000723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "64105a3d-5909-4f8a-949d-932d137b14b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "718249a0-8711-4803-92ef-d7c26b840834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 49
        },
        {
            "id": "f988d25e-b1f8-4477-962f-7cb787fa3c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "0324fd5e-e17e-4448-af98-b11865ebde59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "dc1c6d6b-f768-4a48-ba57-4201f3b60860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "cf155b30-7a3a-4a49-bf37-bca46682bfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 57
        },
        {
            "id": "1ca090e9-18a9-4502-b726-22eed62a1a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "28e9effa-f850-4307-894d-6e523b05fde2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "42e93538-6ccd-41cc-a136-48a66cb52869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "c7f52504-93b2-4e50-bb59-940f132e41c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 92
        },
        {
            "id": "ca1d5c3f-70c6-4331-99c0-49f62099c7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "f190ec38-7f38-4e0b-8199-3806e15af8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "ad5a8e5a-beff-45a9-bd30-bfe96b9ccd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "1c91df0d-79a9-4911-91f8-d0ea598d2d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "6aa8cee6-51d5-4953-bb4f-45af26a66174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 120
        },
        {
            "id": "cfc6f027-cb6c-4786-ba83-085b6144d22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 121
        },
        {
            "id": "1df46425-d1a0-478c-a945-ffd5fa37ba33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 894
        },
        {
            "id": "2c973964-cb6a-4e3a-880a-c490fb49b4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "3802df97-fe3f-4a06-84cb-128426a38d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "5b28bca1-975b-4157-9c7a-0237003ce3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "40b6afe2-88d7-4bea-b9da-e3eee03a41ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "0e51edd1-3e17-48b6-b86e-bcf65265ae1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "b3759e8f-b653-4d8d-b801-bb9f07b8dc6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "80197ae2-d7b0-4c3c-b3ec-cf12d0e638dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "8cc6e441-5b94-4faf-ad11-b5e7e42eb7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 35
        },
        {
            "id": "b3691cd2-887e-43e5-909b-259f77aaf841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 40
        },
        {
            "id": "428680c4-5a1c-455c-b54c-772ca5fa9131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 41
        },
        {
            "id": "14317deb-9401-42cb-b7bd-7e2ffd80df59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "53a21e9a-20d7-47af-a18c-7e3f88f71343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 47
        },
        {
            "id": "c2ac263e-476f-42f6-9e53-174d17f4430c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "975bdc13-6cc7-4c59-b6d3-6d92c5520b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 50
        },
        {
            "id": "fb58daaa-3f62-4cc4-9c58-f97e240e2a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 51
        },
        {
            "id": "3586faf7-f951-49bd-978c-a081c915e4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "568e5387-df9d-4e3b-b94f-26b4d50d9643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 53
        },
        {
            "id": "ab0090df-9d47-4bc1-b335-6fd5ddf70ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 55
        },
        {
            "id": "a0bfcefe-ac84-4ae6-8699-b50e6c910d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "4322859f-fff7-423c-8f32-73ed475f142d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 58
        },
        {
            "id": "22d0b83f-17eb-4e78-af63-14dc8e0def6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 59
        },
        {
            "id": "2b5fff89-b28e-465c-8002-7f96c39fc209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 63
        },
        {
            "id": "7445ac5d-f441-4406-bac4-0214724e5361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 92
        },
        {
            "id": "5b22edc9-d817-460b-8e20-9c0cfed28ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 93
        },
        {
            "id": "470be6a5-d3c2-4c74-b655-8892fd70c5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "f3fefb76-22fc-46d8-9fcb-d0a35268fcd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "3bd6bffa-ce1a-445c-979f-f144cf69f391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "46f037a2-398e-4395-859c-c1641c47dfb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "0253ef83-0a15-4913-bf0d-d0413fb28080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 115
        },
        {
            "id": "2c1cc2fd-9f31-4f16-8458-26af2521de9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 118
        },
        {
            "id": "22a99c54-b63c-425f-8a0a-0ba8c3252c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 119
        },
        {
            "id": "be485aee-3ec0-4c74-b860-869ca4f5c1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 120
        },
        {
            "id": "5e7b9ffa-0abc-47d7-b14f-88f7ebbaac6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "13580ac9-26e8-469c-9a12-146d64c5be69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 122
        },
        {
            "id": "233ff15a-f240-490e-a6ac-4bbbfbdf2f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "8d5a0847-09bc-4535-ba29-bea5e868e466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 894
        },
        {
            "id": "a6cec9a5-3d8b-48f3-a810-d14d74626080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 8208
        },
        {
            "id": "5a991040-1046-4c69-bf76-03333d67d831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 41
        },
        {
            "id": "1db1c269-aac2-4c23-acf9-0e46e0221d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 51
        },
        {
            "id": "4bd381a5-da97-49c7-917c-36cbaa709dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "0446ba0e-1def-4e1c-9b61-225808828de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 55
        },
        {
            "id": "a51bf009-a686-4e52-a907-6d3d7256ba51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 92
        },
        {
            "id": "b2c84490-2762-43ca-ad0f-f3e36ff0dbbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "7f704d84-ad6c-4cc0-92ee-6c16829d62c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "ddeba34d-dc8b-483b-9914-35c777867e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 119
        },
        {
            "id": "0358423c-43d9-49d7-be22-4b55753d0db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 41
        },
        {
            "id": "8628a951-8626-496f-9b5d-43003198f018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "c4008424-4803-443d-b3c5-701cb57d5519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 47
        },
        {
            "id": "752d4f92-d985-41b4-9c0b-f69b889557b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 49
        },
        {
            "id": "6058c9c1-a936-4e3a-8529-7638116241df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 51
        },
        {
            "id": "4a0571a9-60e5-4344-b95f-6d155986a6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "7599f9ca-ad0e-4933-96b6-fc29cf2c2fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 55
        },
        {
            "id": "2df22018-b39c-42ad-aa33-eadf073cadef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 57
        },
        {
            "id": "9ef4c8d3-2b6d-477d-a635-ec2c30997d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 58
        },
        {
            "id": "490dde54-5fb4-4b58-bb4e-d5e2bb0f5030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 59
        },
        {
            "id": "366e314b-44ae-4ca5-82c1-ebd343666393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 63
        },
        {
            "id": "5f0787f5-ef9a-4628-8996-f95bef00b2dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 92
        },
        {
            "id": "3f75bfa8-1c4b-49d7-8c5d-7a36d8aab2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "99e0cea2-59ae-44ea-aca4-312b5f7593a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 103
        },
        {
            "id": "5d155baf-c483-4fc9-979e-f83c6b461812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "33d7930c-a21c-4686-a549-b8e98de4c5c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 119
        },
        {
            "id": "78c85c67-83bc-4c6f-8fbb-28c967a8f5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 120
        },
        {
            "id": "82c144e4-d4b6-4fef-b57a-eb6c84f65b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "9fd8a9b2-9ec5-4a5d-8f6d-f056e5e4b6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 173
        },
        {
            "id": "6774730c-149d-430c-9be0-154d3d8dc8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 894
        },
        {
            "id": "a7f6c367-2adf-4f67-9186-27ddfab4729a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8208
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}