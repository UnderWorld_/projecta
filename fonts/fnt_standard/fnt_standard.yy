{
    "id": "4129fffc-2477-44f2-9b71-ae1efdf87ab4",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_standard",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 1,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 113,
            "Value": {
                "id": "34b7b756-6397-4aba-a053-876394050974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 20,
                "y": 42
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6046466c-4f23-4314-8347-7acdb4ed1dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 34,
                "y": 2
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9b5eac3f-93ac-4138-a9fa-1485223074e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 192,
                "y": 81
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "62f4d15b-ee55-41f5-b3ea-351fd5881e2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 98,
                "y": 121
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3b5e190d-257f-4a19-a90d-6f8c0b7d06cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 340,
                "y": 81
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7cdec88b-1b7c-49bc-884f-989971977414",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "01d66237-3a77-45b7-904c-9955cc10b0af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 158,
                "y": 81
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "818146f4-6a0f-46af-a6b8-ea51a3683c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 301,
                "y": 42
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "02fd0245-1326-4e54-a1a5-c194d0e44afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 81
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "b08c8103-35ad-4534-9a29-42518bf052ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 243,
                "y": 81
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "b7a8b98d-28d0-4bd4-9379-a365bd73c291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 3,
                "shift": 9,
                "w": 5,
                "x": 105,
                "y": 121
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "dca639ce-524b-43b0-99fc-213507e504eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 433,
                "y": 81
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b2106129-f6ee-4253-b33e-950a57680081",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 243,
                "y": 42
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "71f12868-1bc8-48d5-a0cb-5a5f42677d09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 21,
                "x": 389,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e2bc8c8c-42c2-43dd-b633-1adbc8ba9a6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 314,
                "y": 81
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "af61531b-4e0f-4675-99d0-5a2ae2416f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 124,
                "y": 81
            }
        },
        {
            "Key": 32,
            "Value": {
                "id": "9378eac3-1c73-44c9-9a0b-8c0548fd5685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 422,
                "y": 81
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "08f3a862-155a-4e25-9ead-97e0f2720fd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 38,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 12,
                "y": 121
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c18e80f6-4217-4eea-ba15-2d300cc53d4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 56,
                "y": 81
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "84b18229-bae8-4540-b170-75c92d2e70c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "090ac794-c9ef-47b8-b69d-ae059a869167",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 486,
                "y": 42
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "80d2d5fd-2179-4b8d-a94c-30e4b9e1dd46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 32,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 396,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c6a730bd-8f0d-4260-bc31-da7d77461442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 3,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 81
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "54820614-599c-40ab-9c41-c138f8a642a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 3,
                "shift": 13,
                "w": 11,
                "x": 409,
                "y": 81
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "c3f09614-1544-4533-83a7-f918f6ba13a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 119,
                "y": 121
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7b7c2957-dad7-4fb0-bd3a-a3bc40beb46d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 2,
                "shift": 23,
                "w": 21,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2aa96426-d88c-4d6e-a207-89341c3a8f7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 32,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 414,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bfdb017f-bb55-4ffe-8f09-0b3a61959338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 84,
                "y": 121
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6d17c5b9-70f8-4ac2-9e30-28a2d7a81ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 2,
                "shift": 25,
                "w": 23,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ab7e8b3f-44ff-49fd-a560-72f9f22d38ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 38,
                "y": 42
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ff57c3f5-f28e-44b4-af0b-659ed309a3ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 445,
                "y": 81
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a94bc9a4-fcde-45d7-8bf7-15aa1b7e5b31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 59,
                "y": 42
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7428191f-54f8-4de7-aed9-c06c0e8fe2d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 34,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 283,
                "y": 42
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cc065e06-0410-4d2a-b93c-19d72fcee75f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2dde2959-2d35-4606-aead-1224cc72ffbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 38,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "5a484d5c-aedf-4376-9c5d-7a51fbef34d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 32,
                "offset": 1,
                "shift": 28,
                "w": 26,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d4ab8266-3001-4a19-a881-ac1ef97b29f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 377,
                "y": 42
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "048c3001-5da1-4529-be85-766d91c48d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 3,
                "shift": 22,
                "w": 18,
                "x": 183,
                "y": 42
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "067a7dd3-a2b8-456d-beef-b4438dd0ad05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 80,
                "y": 42
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bfc973f2-3bf2-4d7d-8723-7e749217a511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "0629f0bd-d5af-492e-b820-acbef8da4ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 38,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 481,
                "y": 81
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "447dc839-851f-42c8-be22-46af10e51b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "3ce0abff-a708-43b8-903a-07268a1500a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 27,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 296,
                "y": 81
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "963af01b-b77e-4908-b0cb-6bd5993a39df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 13,
                "offset": 1,
                "shift": 11,
                "w": 7,
                "x": 139,
                "y": 121
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "19e794d4-8e48-4cea-9802-84135e0c5b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 226,
                "y": 81
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "64e0bb6d-a5d5-480b-9581-7a6c0a32263d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 320,
                "y": 42
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bb4e4d09-0a23-4027-bb87-fb0aa12f29ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 203,
                "y": 42
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "1d22af4b-9036-48d3-a478-b60e3ff691e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 23,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 358,
                "y": 81
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "0dc855f2-d9f6-48f6-9631-19c4896b3b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 112,
                "y": 121
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "613e197e-9710-44ae-a99e-692a659dd609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 450,
                "y": 42
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ae2e987b-f898-4936-b1f0-a5364a56040d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 126,
                "y": 121
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "50e120ae-8c7d-45ee-b2a9-d0395469468b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 38,
                "offset": -1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 121
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "995aa0bb-0d53-4316-91b9-7eca04493da2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 318,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "e68e92e7-55bc-4013-8c4e-aefadf477a90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 396,
                "y": 81
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "6cd46afb-8e49-4030-a490-bf5acebf437e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "734b7974-0325-4817-89b9-3e1ce2e5894d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 223,
                "y": 42
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f300baa7-77d0-4228-8916-8c1cdac5be92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 454,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6bf5fd9f-0ae4-4fa0-ad0b-6f0e8fc55c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 358,
                "y": 42
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "32821be1-2273-4c61-822e-7e0144dc8c51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 38,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1f366b57-4e9e-43f1-b166-34456e768666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 32,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 107,
                "y": 81
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "bb02dc51-933b-4b37-8675-fa3e457101d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ab79cdd3-b8bd-46d4-9412-7e5e191effb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 148,
                "y": 121
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f8b4bc14-9e84-465b-9642-38ed23f25505",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 22,
                "y": 121
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fd74ab51-c016-4808-af15-8930c03d660e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 77,
                "y": 121
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "03e23c33-065b-41aa-9bfc-94ff634127a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 469,
                "y": 81
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "cb01f3d0-bc09-4325-b516-60d6716d2b07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 141,
                "y": 81
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f639f30a-6818-4fb0-96cc-dbd085ad4bd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 32,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 90,
                "y": 81
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "02d526a1-930f-4fe5-8a25-28e784c4f734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 19,
                "x": 122,
                "y": 42
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "7dab4bb5-e37e-4308-b4f8-c191a0123dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 260,
                "y": 81
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "42084417-b03b-4f98-bd25-c04e11b45552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "ad3509ab-3e0e-4e4e-be0c-5cf0e8c4b2a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 38,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "28248b84-9715-4016-b5a4-eb0a2af869f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 457,
                "y": 81
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "cdc8da6f-42a0-4216-b547-27b1d6415a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2863ed2a-b979-41f2-8494-ef362e738bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 38,
                "offset": 2,
                "shift": 11,
                "w": 8,
                "x": 501,
                "y": 81
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c9b68c1e-d3a0-458b-ace3-3fe00196aa9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "c940f899-8b87-4375-9f11-d5ff37b4ad1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 209,
                "y": 81
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "cf8766ce-c609-4714-9329-da71fdae5e89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 38,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 327,
                "y": 81
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "64ef3e09-280a-4a29-9129-64b3d942a03c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 22,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 377,
                "y": 81
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "ac7768ed-7c79-4024-9e7e-e3b7ced6313a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 175,
                "y": 81
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "066dedbb-f7c5-420e-bc54-0d96faf69e57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 81
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "2837ae87-bfb0-4e1c-b037-c7a6dc836aec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 29,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 278,
                "y": 81
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "40078ffd-59f2-45e5-8f9c-f11efe836e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 2,
                "shift": 11,
                "w": 9,
                "x": 39,
                "y": 121
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "cce5bc34-7cb0-458c-be76-fde598863daa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 50,
                "y": 121
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "da17dc9c-abf6-4c62-96e7-5251f701402c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 339,
                "y": 42
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f2cb6d31-8d26-4867-9b31-5876db1ad91b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 38,
                "offset": 0,
                "shift": 11,
                "w": 8,
                "x": 491,
                "y": 81
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f2341066-b0c8-4b17-a3d6-bc4ca3d03596",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 38,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 476,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c8b921a4-b19b-494e-beab-ee49f0d0e790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 263,
                "y": 42
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "46ac9e9c-bd2a-444a-ae5e-31d48d05e05e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 432,
                "y": 42
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "09c2c86a-94b4-4997-baac-d883519c09fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 3,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 42
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1e5f508b-fe86-4f7f-9708-44d618d06925",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 32,
                "offset": 3,
                "shift": 20,
                "w": 15,
                "x": 73,
                "y": 81
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e1cad957-d43c-470d-896d-bde8d96a2fdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 38,
                "offset": 3,
                "shift": 9,
                "w": 4,
                "x": 133,
                "y": 121
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "2f329702-0cad-451f-8b52-17a28c242405",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 63,
                "y": 121
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d856fcd0-136f-4eec-96cd-51655602c135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 163,
                "y": 42
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9fb2ade1-1a9b-4dea-bdfd-e4e2f8921254",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 3,
                "shift": 23,
                "w": 21,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "65675356-6317-489e-849c-7e700f0a9b7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 32,
                "offset": 3,
                "shift": 23,
                "w": 18,
                "x": 143,
                "y": 42
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "0ad33736-eba7-4acb-b79b-63416a10a7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 91,
                "y": 121
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 24,
    "styleName": "Bold",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}