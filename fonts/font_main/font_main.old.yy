{
    "id": "b3abdc6d-1ddc-469e-b88f-bcdd016625af",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_main",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Tw Cen MT Condensed Extra Bold",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6c168a98-21fb-44df-b985-efdfe6ca9f83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b839f60d-9d2d-4796-9c39-09a853d372df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 181,
                "y": 46
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "58a57373-b771-4cad-a618-210abb9aa191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 170,
                "y": 46
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9475fc77-c584-4d68-922f-89aeaa72c65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 20,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 156,
                "y": 46
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "930ad229-5b3e-4957-affd-094ed58b6a18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 144,
                "y": 46
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a5629877-aeb1-4dce-9b30-93180638c8ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 20,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 126,
                "y": 46
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "4056e3d4-0f25-40f0-a202-0142e9f2875f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 113,
                "y": 46
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "336442f4-3ca8-4728-8023-c40bf6c6181e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 107,
                "y": 46
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b31aca01-85b2-4a18-9b60-41463e2b7e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 101,
                "y": 46
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "310072d5-90b5-4fd4-b76f-8c93eaf44ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 95,
                "y": 46
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6403fe6e-01a2-4b0c-ae64-4099c31c7a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 187,
                "y": 46
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e62d1189-8533-4b16-9718-570bfe895b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 82,
                "y": 46
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "9a4b119e-3765-484e-a847-64950783bcd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 66,
                "y": 46
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "04fab337-1608-4073-aa4b-49655c3fc7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 58,
                "y": 46
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3fc8963f-8b48-4122-b2ff-dfa5393f3a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 52,
                "y": 46
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "c65b772f-63c2-4ffd-b6b9-f8559ffcb5fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 41,
                "y": 46
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b5e29dc7-416d-4120-88e3-fd8da4d746e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 46
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e9cc5a08-f356-4e98-9b84-36fd5faeeeea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 20,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 23,
                "y": 46
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "70c46754-f84d-4f01-b029-a4b91914cb9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 12,
                "y": 46
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "48101bb2-bf71-4961-9786-c96604d2ce20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 46
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "030d04a7-e41a-43f4-ad7e-347702211921",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 244,
                "y": 24
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b34a48ac-10cf-4450-a06a-497ad663c84f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 73,
                "y": 46
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "10de3257-57c3-4a2e-95d5-ff4cf49a5cab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 208,
                "y": 46
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "95a73f19-fcea-4d1c-859b-5deaa9eeeb42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 68
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "40429e7d-4421-4b89-bbfc-57d738b53641",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 219,
                "y": 46
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "b8c20b6c-e5ea-4b0b-a024-5cf59871c14e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 68
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e7f927f1-644e-475d-b277-bf4302e6c193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 175,
                "y": 68
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "deb24e21-7366-44d8-a1c4-710d4ec76e40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": -1,
                "shift": 5,
                "w": 6,
                "x": 167,
                "y": 68
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "112ea8d2-cbaa-4f82-9b20-291afe91103a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 154,
                "y": 68
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5f790ac7-5030-4ca7-b92b-915f23238e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 141,
                "y": 68
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "976b04dc-7cfa-4244-81b2-0708df699337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 128,
                "y": 68
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f4a24ec4-54c1-4603-9785-a7d97b327087",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 117,
                "y": 68
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "6a57df87-0965-4c2e-8c12-93f3a4cf87a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 101,
                "y": 68
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "0246ca21-e185-462e-9053-bf6bec0d5511",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 89,
                "y": 68
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "0dc7b636-9a0c-46eb-9f3f-778f7a362103",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 192,
                "y": 68
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "41e67e34-64dd-4bb9-a6a4-938ef8881ea9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 68
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "67bb9c29-4d3f-430d-ab3e-fb6a597dbf58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 68
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "d46bdc0a-7597-43dc-a62f-c99c291cd138",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 48,
                "y": 68
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "77f750f7-d8a2-4f03-9ae5-62834dfb3863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 20,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 68
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "10910dee-b3ba-4540-8b9c-bffd9f52529c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 27,
                "y": 68
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "267f7490-51cc-47a1-a657-796479c8760d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 16,
                "y": 68
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "beb1b4ae-9d07-4944-9b45-1cd3ccb38e84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 10,
                "y": 68
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "bd843510-20d9-41b0-afe6-ab8f3d792b6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "020d5842-b7d2-49da-af21-64f89068e927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 238,
                "y": 46
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "40712ac1-0466-40b3-8d83-23075a9adda6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 230,
                "y": 46
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3cde996d-b205-47c8-91d0-c01a2f81e311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 228,
                "y": 24
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "5c6b8507-306f-4805-8165-1fead8604c09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 196,
                "y": 46
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "a2619efe-1b41-40a4-8eb2-bc39f7ddf1b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 216,
                "y": 24
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8d85e14e-c09d-48f8-8114-b0a146d53041",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "6443ee10-2035-4820-a1ac-d172fa5c125a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a2aded9e-64e3-4484-a2c5-0c5f14738657",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 20,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f7f950ea-5ddf-4474-aeb4-07bcd21706f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "9cfe1b8c-d0eb-40bb-af13-ce6194bd6708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "f3c06e2c-0e06-45af-bb4c-7520fd265d07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 20,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "932db3db-f501-4110-b6e9-361856aaa150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d820f394-4450-463f-b90d-2a2ef180ca53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 20,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 137,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "aebb9c73-6706-40bd-a823-f2043c4c7d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 125,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8bba9c73-7c1d-49a5-9ade-23bfda42f6dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 20,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "83a8057d-96e0-4c34-97f8-a65086bf5709",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "654ca2c5-e459-487b-ae91-08b749853aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 107,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "eb83dbf2-7759-41f6-b68c-1e4ae781f2b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "76839333-b552-48c9-924f-1d84072256ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1d28bbb6-6b1f-4ce7-9a9b-ebd683cf0a42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 20,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 67,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "2bf969ed-0491-490f-aa3f-97eff42bdf5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 20,
                "offset": -1,
                "shift": 10,
                "w": 11,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "dcc747ad-2cf6-4486-8c91-fe24ef496056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c815bb7f-eddf-46e2-a89a-4621003b71cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "16d33e44-970d-49fb-829f-18af499a113f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2684bc31-1712-4e19-b0da-d821e33c09e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c0c943b2-ed6b-404e-948a-20cde49b796e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 9,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0f4ab3c5-3065-4743-8e0b-4afacdc89dc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "642b4545-c98e-4b0e-b2c8-dac5c4fb682d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c27df4aa-1f19-4821-b642-aed72e6cdaf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 24
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "544df8f2-1d9a-44d4-8362-2dfc19f4505f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 24
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d1bcdd94-b546-4fb0-b7fd-c35a73b4a24f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 194,
                "y": 24
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "dd71f551-562f-47f6-bfa6-bace9f64a9d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 189,
                "y": 24
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "80aa3a59-85c0-4f2c-bc25-1e0da841a432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 179,
                "y": 24
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bb57568e-489c-4630-a2ce-127d93d3a127",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 20,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 174,
                "y": 24
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "21651e6e-7877-4987-a75e-286720ca383f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 20,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 160,
                "y": 24
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "fc50770e-0df4-402b-a747-754d7dbdc5a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 150,
                "y": 24
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "454b60cf-33a8-4f42-b998-4ffe4c53c71d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 139,
                "y": 24
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "51503afa-dad8-400b-80bc-eca8a51c8b53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 20,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 24
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "7c87ed4e-4781-4792-b148-1dd198162258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 119,
                "y": 24
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "43be5c03-bdd2-4f35-a781-bcd7ec271c96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 20,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 199,
                "y": 24
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "05a175bd-1d74-45fd-9c7c-cf529f791805",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 24
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9bf97050-7e07-4dc3-b7c4-31ba20e2d5bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 92,
                "y": 24
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "af345c8b-92cd-4d12-8ff1-c0d2c4866b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 82,
                "y": 24
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d8997803-38a8-4638-8063-e2c74697dba3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 72,
                "y": 24
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "4a4fa210-4f6c-4f02-b315-c77bc928e933",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 20,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 58,
                "y": 24
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4965a5b6-ca01-4dad-a79f-5c3a2b0a0803",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 20,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 47,
                "y": 24
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "024e7ed8-9629-4daa-bb8d-b1594b748376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 36,
                "y": 24
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7e4ec3e8-3c1d-4ede-a417-051146135c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 20,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 26,
                "y": 24
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "472b6d71-4ad9-4063-8308-aec896350b87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 17,
                "y": 24
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8f8f602f-73ae-409a-92a1-445bb9b1846b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 20,
                "offset": 4,
                "shift": 11,
                "w": 3,
                "x": 12,
                "y": 24
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "83424315-4716-4b92-a3e1-f5fa24806003",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 207,
                "y": 24
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "612cac48-08a2-4971-beab-41b2a4a732f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 20,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 203,
                "y": 68
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "cfe47803-62ff-4910-860d-9a386015eb6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8217
        },
        {
            "id": "a86c4a87-0b4b-4da9-b2a6-a5a4274ac917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8221
        },
        {
            "id": "383b73ad-36c5-497f-9b52-591740f3036a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8217
        },
        {
            "id": "1cfbe78b-e923-48c7-8451-735c9e13eace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8221
        },
        {
            "id": "8cda8878-a7db-4bb2-a5ae-7ee8855cb23f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "fa18f645-e61d-43ff-90df-72d803ab41d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "ff040758-ecca-41e7-a7bf-404aa090734e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "fc4f7968-0eda-4bbd-ae02-f5f5b54834df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8221
        },
        {
            "id": "b11e29f9-f08f-44a7-b908-c75bfd76fda7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 44
        },
        {
            "id": "d951c414-f4d6-4413-b168-db473e5663ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 44
        },
        {
            "id": "2c2168f3-a216-44fa-af19-d00b14c3bed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 46
        },
        {
            "id": "9b57e2d4-309e-402a-8880-a513fc197205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "e6b84207-d57a-48b6-ade2-76b8cca305ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "3648fc4b-deab-4e21-83d7-be8c9733d381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "ca224b03-61aa-4d8a-9b7a-cd5d7163d1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "d18f1299-9427-4c68-a108-88e458f0c0dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "6fda6786-7028-4850-84d4-6d252db61252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "dab791da-b396-447a-b649-49d75cfdcc6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "93268692-0943-4c90-81ba-a91ef7b25a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8221
        },
        {
            "id": "309d5886-2b86-46fb-bb69-cc0a07c7fab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 44
        },
        {
            "id": "3094882d-f7e7-4376-a450-faf58e4a7d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "faaf8bb0-1fd2-433d-9e47-cab87a4db816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "8697f306-4bdf-4fc6-a5f4-536fdeed2cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "b4eb9b02-feb0-44c5-acb3-252115a771fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "08b55813-ab16-4413-9335-383a4cca740b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "afb0a119-573c-4a1d-94c6-9c5e67eae828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "d92015ea-4933-4783-a031-a4c388c71bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "a555574a-3403-4713-a337-8f43f01349fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "95d7316a-e9fa-4dc6-8eeb-bc075d51c316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "ffbd9a7c-7892-43b7-9c5a-233609f80f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 44
        },
        {
            "id": "6390ccbf-9a6a-495c-9e43-ffd430eba7d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "a4ea3608-c803-4ce9-83b7-2ef5646d1d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "dcd8cf73-67ce-429e-9f4d-50caa31edfb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "fe229ee1-7514-484c-86f2-dc52555eb555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "d948c9db-dcee-4a45-af34-838debaed8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "06c935d1-c641-41db-909a-36eaddb0ee65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "8d6f7ddb-ffc0-4fd9-a4dd-36ee992912e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "516a2f69-e5db-4520-879e-901bba265844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "830c4b82-e4a3-48e4-8f23-6c1e89e5b02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "df210c00-f8ef-4b6c-aa64-e785bac1d29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "bcb2f94d-3db0-4c75-b8d8-0855b22d9e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "5e0906f5-ddd6-4f8a-b665-8d1c1ca7bbf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "54ecc686-c44a-42bf-8142-d289b966089b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "45cd1a84-bd83-4390-a7a1-c16cbc543a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "e1489e03-d151-4c88-86a0-79dbf3c81055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "d48c3a40-34c2-4c42-a402-a2b41090e0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "833f6992-1774-48cc-b79a-bbf004be0caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "a7cf30e6-1b33-4864-9e1b-d55f009fb7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "553fc38b-24d3-40bb-9f30-436c48cfb20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "20b1a7f7-d167-466b-815c-17b500aef292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "043d47c5-3952-4a14-a9a8-129991969e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "4faeb68b-4c4b-49c4-a56d-adf88a755e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "c7775192-b253-4ec3-8649-db3ba69574ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "96f1f408-1862-423f-a8e4-4ba51a2da528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "66d9581a-783d-421f-9901-bcc6a6348da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "3c3af787-5969-4033-830f-2b5e2e00a238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "1eb23ada-0541-48ab-ae69-688985266b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "40a2be11-2da0-4acf-b054-bfb9d38c995e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "dfac561e-82fb-4b3b-ae6a-07d43c44f637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "c5566e0d-067d-48dd-a26c-3f75c7883481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "c0a7b7d1-82cd-41b3-9f5e-bc3f19174f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "c58c2d50-58c7-4cc2-8ad7-19091ee56e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "05ffbd7f-c863-4589-a112-6fef0fe92193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 229
        },
        {
            "id": "4883b4e4-f9bd-40c1-adec-8548747ee060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 44
        },
        {
            "id": "9dba6946-12f3-4578-8a27-b243a7428ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 44
        },
        {
            "id": "724e2f32-62b2-4e0e-8148-c3901a342253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 32
        },
        {
            "id": "2b3f2fca-d815-475a-9283-df44d2073f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 44
        },
        {
            "id": "b6e51390-a113-4af7-85a0-4f8c93d8835b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "d2ce8ccc-5189-4bdb-a15c-0fedc7bf157a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "b6073b21-ec08-4dac-996f-c2f55e0caeb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8217
        },
        {
            "id": "0f0f0de1-6cdf-4c97-8de4-930d4f29235f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 44
        },
        {
            "id": "ebc508e1-35c8-4cb6-8313-6cfecdbd8445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "4dec09b5-5586-47e8-98b4-6c7ebcfcf7fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "f33af324-eb79-4c24-aaf0-dabd14aba16e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 44
        },
        {
            "id": "7da5bc94-068f-4276-9174-1e4bf9230712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "3f97b930-66bb-4c42-86a5-07ecc2ee3c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "9e58d6c3-4bc0-4949-981e-38361d674b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "94db7309-7d8b-42ea-835f-ab220eea58a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "1497b138-3b46-4318-9677-1fd78aed8806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "88b2f9e1-82c8-410d-a3fa-c844d9eab61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}