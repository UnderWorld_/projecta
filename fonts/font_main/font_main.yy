{
    "id": "b3abdc6d-1ddc-469e-b88f-bcdd016625af",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_main",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Jupiter Crash BRK",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "12877860-2062-4e68-8e01-1690a82e5518",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "1d675cd3-67c7-4c0b-a5bb-7d40be4ed41d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 19,
                "y": 50
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e2f02759-8e18-4a31-b55b-a8aca75a8572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 12,
                "y": 50
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c744c395-293a-4de0-ae35-b7bc56bd02f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 50
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "155fb848-fb78-40c4-96bc-77683ccb6091",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 242,
                "y": 26
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2498df3a-45bc-4063-8cd7-d41ac429e238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 234,
                "y": 26
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1921c496-2493-4506-81b0-c69600254e2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 226,
                "y": 26
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e16cb40a-852d-4f21-a028-19aae9a4990c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 222,
                "y": 26
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e15c9ac9-aeab-4391-af6e-38b3667ec756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 216,
                "y": 26
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "226e2c48-a11a-4667-bc74-92957a9d8de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 22,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 210,
                "y": 26
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2f27cf53-9dcf-4a3e-aa69-ca5eaa950a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 22,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 23,
                "y": 50
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "259e4daa-88fd-4e29-a4bb-fa6394607ad4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 202,
                "y": 26
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "59c02205-5205-492f-989b-85e3ab04d23b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 190,
                "y": 26
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "73aa705e-df47-4ed1-b2f7-8c624eae2920",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 182,
                "y": 26
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "45f918e3-25e3-44d7-8bf0-f0f5bbe0d652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 178,
                "y": 26
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d9017492-e5f8-4b13-872f-76f8233ff62b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 170,
                "y": 26
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b5af123b-438b-40c4-b2e7-727bdd1fdbe5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 162,
                "y": 26
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "79faf0e9-efd4-4d12-aed3-bbc0751ac0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 158,
                "y": 26
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "cc09ced0-b8c8-4331-aa5c-0e25aebca72c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 150,
                "y": 26
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "dfc4470a-0fe7-44ac-9d5d-5c23a6955cba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 142,
                "y": 26
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f51a96db-a9d7-4ec9-ab58-2a71b4f266f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 134,
                "y": 26
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "60a56fe0-2e93-47b6-9251-3f65ba296c1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 194,
                "y": 26
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ab268e8a-2b91-4f16-b24a-aa30cfdc2b1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 33,
                "y": 50
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "969a06f2-a6b3-43df-9fe6-423d80303724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 41,
                "y": 50
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3c65dc6d-4134-4071-9108-7cda7ab65329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 49,
                "y": 50
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2be68a10-b63d-40cb-ad44-915f644934dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 219,
                "y": 50
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9f5ca655-7fbe-4fd0-a6d3-ae883f49322a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 215,
                "y": 50
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4a4f0791-9f8c-469f-a058-11be7d96ffd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 211,
                "y": 50
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5f3a599f-426e-41e6-9560-fe0ae8ddf3d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 204,
                "y": 50
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7100d6a7-844c-4fe4-b734-bd929deffc2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 196,
                "y": 50
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b8154e06-3eac-4f46-b4a3-c042887a60c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 189,
                "y": 50
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3c9df4fb-c0bd-471f-b3f9-41b6339b850b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 181,
                "y": 50
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "62306937-58d7-4621-8115-3007e415fd89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 22,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 165,
                "y": 50
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f3cfe978-ea04-4af2-bc86-d55a2c3b851c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 157,
                "y": 50
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "7af355ba-2b6c-4a76-8f8d-898e989efad7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 149,
                "y": 50
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "ffc4a840-3874-4384-8a9b-7a1f82c0b9a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 141,
                "y": 50
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "67031657-4030-42ae-a38e-ad1b49fbe6e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 133,
                "y": 50
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c3e442f4-004f-4adc-8278-4dd957681fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 125,
                "y": 50
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "b1451215-5b9e-4410-bc74-8baa337bcbbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 117,
                "y": 50
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4343dbed-1350-468a-a25b-26203ca909de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 109,
                "y": 50
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cbaf08d8-63c1-4fab-92df-19163a9581b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 101,
                "y": 50
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "37c5aba6-7d0f-4bac-9426-d663eff06f49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 93,
                "y": 50
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "84c2af9c-7ac3-4b50-8888-7e31ec9c3674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 85,
                "y": 50
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "98d9d587-fd6c-4b44-ae36-c3865e365624",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 77,
                "y": 50
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "57764ffb-0bc1-4cf5-ab25-5d4cde970190",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 69,
                "y": 50
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d16a581a-933e-467d-b08b-997d1861c322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 57,
                "y": 50
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "abbed89f-ab24-47bb-939d-541fa8013211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 126,
                "y": 26
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "79e21ecf-0282-45f5-bb25-f45d75368d3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 118,
                "y": 26
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "26ff70a3-425b-48e3-9cbc-d1d80710297e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 110,
                "y": 26
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "5edeb928-2e47-4392-9d23-07f7632d07e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0c6c0de2-65b0-4f7d-b92b-b8f23ba667aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "a3f658f5-40fa-425f-bdac-0a4c9cc5b113",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5c8da4f9-3dfd-46e7-b440-bf235bfca042",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "5fb5801f-8aba-4b80-a7da-cb704bc5f398",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a428435a-f532-465e-a125-29dfd9fb736d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "9c246420-962f-4b5e-b719-e80bae1aecc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "91523b4b-c48d-4431-bd5f-806401668980",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "02be7fbc-bf4e-4ab2-9bb7-5e68760a6ed3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "784b7e08-1069-43c5-8a44-b7035197f938",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3498816d-f6f2-4077-ae8f-ba48c176133b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "af18cd83-a036-49e8-9999-605b2e5e522f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3a77e03f-627b-4765-842b-b6f451721bde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "31f00893-a8b6-4235-9d69-7ca1e85fef54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "218ebda0-5ccb-4d05-b1cb-bc89f39db15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "93ca8762-8437-4a2c-84d8-663984cdd184",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 22,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "867671dd-9c72-492b-b13b-edbc051104d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "adcbda72-adbf-4927-aa9e-b4f8968bc93e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "bcc2e35c-3348-4d79-851a-ca068aa3f3b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "48f63b35-aef7-4cdc-8ff5-b8060152cb11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 19,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "12195050-e9cd-4fea-ad02-cb8a557406b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6f9fd957-4769-4d79-8da9-51086d24e27d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 79,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "3f7e58ba-96f6-4406-9424-2a92bd8e5695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f18d5963-524a-4495-8684-89b27e7a7e79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 18,
                "y": 26
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "8d7c6b57-14ca-46a8-9088-0eeb22123820",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e4801e2e-373a-4f41-b8a3-f3a812a5c0b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 98,
                "y": 26
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "12482454-00e3-4c25-ab42-9b8fb3df1a48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 90,
                "y": 26
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8b5248d1-cb36-4d1c-b2ef-411aacf54f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 86,
                "y": 26
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "efb1a905-f219-4308-9310-3d5b84292137",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 74,
                "y": 26
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "a9a79030-f0f9-4fd4-8b97-c69265b0bed5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 66,
                "y": 26
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "6cafe42f-8169-45f8-af79-5cf75e1720a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 58,
                "y": 26
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4c1819bb-68b9-443b-875b-05a88972c457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 50,
                "y": 26
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "57c73203-05a4-4359-a8f6-9bca930c1879",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 42,
                "y": 26
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "90a6b4cf-ea37-4f71-85e9-103809080197",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 34,
                "y": 26
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c4b6fa3c-bde3-47d6-b696-3e8a27a23a28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 102,
                "y": 26
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "82340622-4548-4bc0-be5c-05274e2843bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 26,
                "y": 26
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "23316b8b-dd2e-4901-9a0c-f0f60b5c14c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 10,
                "y": 26
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b3ea79e6-f668-4192-b0d2-f857723bda21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 2,
                "y": 26
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "14e6d52e-2f73-4035-8cc6-df509a9fdd9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "eee13294-e0a4-440d-9db4-fbd67ed54931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "9ca3b4a6-9fe5-4f0f-8aaf-a8bb00087c4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "6f1b4a4b-2ee1-456e-b6ae-4e144c2d49f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eafb8496-4887-465c-8a17-132a3f503d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "887c88b0-b130-4915-abd1-fc4d36693130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 22,
                "offset": 0,
                "shift": 3,
                "w": 2,
                "x": 205,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "02938001-79be-4d93-95cd-1e67dada67e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 22,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4427a1c7-6260-492b-ab34-6c6587127928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 22,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 227,
                "y": 50
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "597f0252-7ebe-4259-b625-e781eb37602f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 22,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 239,
                "y": 50
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 15,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}