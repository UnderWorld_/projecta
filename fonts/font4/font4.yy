{
    "id": "a47caaa9-7520-4dce-ac4c-54af4d907d78",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font4",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\font4\\BreatheFireIi-2z9W.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bulky Pixels",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "313e46f4-d265-4f1e-bcf8-c8bcc3f1d69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "38c4ac31-e151-4186-8e81-4c630df4e3ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "79b63fe6-1ac2-488c-80b2-2347eac6384f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 241,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "916f92af-cc01-4433-b39c-56da00920717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 226,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e5229327-0fa3-4ea6-957d-fc0877ee690e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 215,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c5f32b70-55d7-4de4-9d81-9134cbbd0cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 18,
                "x": 195,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e7216925-fb19-437f-9d62-84943aff6696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 181,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "34acaea7-a40b-47b9-a60e-ce3d5f72a6c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 175,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "26da1716-ae44-4935-a474-3296cacaeb21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 166,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "503356d5-3d09-44b6-a336-5aff286a2c41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 157,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "95296582-21b9-475e-a75e-9768192e86a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 8,
                "y": 59
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8bdc8733-38b5-4954-aa94-28f1561801e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 145,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "de26fffb-d73e-4c1c-9b7d-9dc6e67e5543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 129,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4eb7385c-dbdb-4f27-81d7-e3de8e4fc1c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 119,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f56afa12-7f62-4040-9a85-074db35ebb0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 113,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fe70250f-4b8b-46e7-8334-83e191c80299",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "faed02dc-a4c5-472d-805f-7b1badeac4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 92,
                "y": 40
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "4b73192d-9650-475f-9791-a177761f9189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 85,
                "y": 40
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "81f25377-4675-4213-bccb-e55fc4615dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 75,
                "y": 40
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "61c143cd-de58-49fd-81cd-5e6ffc999149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 65,
                "y": 40
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "88254240-0045-4d36-b709-62b273f3273d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 55,
                "y": 40
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "8260cc04-1c6c-4320-8095-4b3939efbcc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 135,
                "y": 40
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a1060b51-eaf5-465c-8c1c-8aee0ed7dba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 23,
                "y": 59
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8a6b4a36-e93b-4c6e-aa8a-5af5cab48784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 33,
                "y": 59
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0251ea50-aaf2-4d74-859e-0c4126839f8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 43,
                "y": 59
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "2530b09b-c65c-40e2-8d1a-fde3ffd1cfdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 26,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "3fece184-6a7d-4f8d-9ad9-1016ae1765f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 4,
                "x": 20,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7f7838cf-44ae-40e7-9a0a-d4e00dd45229",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 14,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "563766d6-dbb6-4b7c-9115-cd4140284693",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "3d786333-e6e3-4661-b848-5e4582b7e5ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 238,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f2ed5cc6-3a29-4540-a90c-50a10bb35987",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 10,
                "x": 226,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "64a0fac3-6942-4893-bf01-c420ef4bfb2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 217,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "58c137a8-4562-435a-8938-4610c2c2ea09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 202,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "eb2d2d52-d994-4c9e-9355-cf7246ab1bf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 190,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "725243ec-7bb7-489b-ac19-ba2703681454",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 176,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "851494df-21af-4250-be9f-91316cc48c2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 166,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9fbae698-77a0-46e7-b2f6-4708b1b1b4f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 154,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "08043dca-1561-47a2-9607-5708cd6f6fc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 144,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "1370e283-d01e-424f-b9c4-223f080966ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 134,
                "y": 59
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "39ffb93e-3d73-46c4-b493-83b1911acc6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 122,
                "y": 59
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "d313fc5a-b573-4ce3-be47-8ec4a23e4611",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 110,
                "y": 59
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9d4b584f-3f88-4481-a62c-c633851b3991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 104,
                "y": 59
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b84308f7-2073-4b28-a990-623eb549df58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 94,
                "y": 59
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "0f9b8a35-73cc-4783-aa26-24a037def490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 82,
                "y": 59
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "9c751650-43ca-4604-9db9-5d5dffd1a4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 73,
                "y": 59
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "6539820c-ea6e-4010-b068-406802293385",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ed9e23e1-0082-4f76-b56b-04d70a9e531c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 43,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "457d6b22-bf24-4552-995e-a1af9b836f79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 31,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "85239dfd-379c-436b-b918-4fb49941bd79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 19,
                "y": 40
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f5cd36d1-4118-426e-b998-90332f16327b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 9,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4cca9a66-d870-46c7-bde0-19eeefcdcd48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "648340d3-d7e0-4131-b84a-61528a614ea8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "5e2206bf-73c3-4f65-813d-db74319c773c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bd145f76-98a2-4623-840d-7dea29ab6315",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "19b05e56-e603-417d-be03-acdc3f530b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "8b0ecea7-529c-459f-9797-31db18838bf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "3b2d8f7c-bae6-456a-8fd0-a69f351ca97e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5f9cc5bb-ed29-4efa-b457-2c61e2a1d175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "de014c75-1fdd-4583-babb-dacdace7ee10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f979e10b-fe07-4359-a606-9d1490a71423",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "4467ff7a-739a-47ff-9bfe-1d3c78f3771a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2545dd84-111d-4a7a-9171-40b39ee01afe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1e8e92e8-1f00-4e5d-9700-d0b2916107fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "36bb6071-8109-4fac-9cfa-69347f3e1989",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4164330c-0907-43a9-944c-f68fed251d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "60ca7543-212a-4f0d-baa7-11fa066995a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4b24b076-52b5-4dc1-b4bf-41afa09ce598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "616f5cda-c72b-41de-82b3-7a3c46d5404c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f3d7df63-c879-4b20-b7e6-71865cad180e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "7795e5c3-e09e-4742-8d0c-7132fb619c16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "77b760a1-973f-40b0-b8fd-a9927b399270",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "f33f4ddd-383c-4ead-93ae-1ff4d59df7a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 23,
                "y": 21
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d30feba4-3bd6-41ec-83fb-b2810e1cf750",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 132,
                "y": 21
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cc2d331a-e4e0-4505-b914-be570d92d0ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 35,
                "y": 21
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8346982a-9d62-499e-adbf-a94c204e37dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2044b242-e94e-4da7-ba00-d3864a37c8d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 237,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "aabb6a7f-9099-4581-beec-07e79ef56f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 5,
                "x": 230,
                "y": 21
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "7702249b-b5ca-49c8-8fec-e5238025a6c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 212,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "87069dba-93a5-44dd-b0e1-af75026cf0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 200,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "1a1a50d9-0cb7-4446-9aec-cc5e05afcfd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 188,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2d0e8101-2160-4aa8-a070-f5de2c258e5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 176,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "1e9b7678-33cf-455d-b905-6876be752e83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 164,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0f06fcd8-50a6-4cdb-b4b8-36dfb4b4418b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 154,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a45a5658-5649-416a-b630-022d039253e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 9,
                "y": 40
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8503b8d7-b89f-42b3-8a49-f82788841c68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 144,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "24f8b758-1a7d-4a9e-b042-a93785da4c43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 122,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "968b2497-8256-4551-a8e2-fc7e0a5f1202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 112,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "b475fe87-01e4-46ce-8d20-860eb39346cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 21,
                "w": 16,
                "x": 94,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "7c9265c6-2600-43c3-bb1c-2b0b74a8af8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 82,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "ab3bafdc-31d2-4e76-8594-741a36538065",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 70,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f3feb2e5-ab4f-4d15-94ac-844bc8ca56f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 60,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "575ebfed-a150-4758-90a9-d044194e0a82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 54,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "2282a84b-a4a2-4649-acfb-68fea082b031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 47,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "4ebd1b9a-1d8f-4e98-b8b1-af2564cf554d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 41,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "37236387-0e60-49d8-9775-56b7470cfc34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 20,
                "w": 16,
                "x": 36,
                "y": 78
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "26af5c8a-0d03-4a60-8242-144645c89751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 54,
                "y": 78
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "9f1d889e-7122-4acb-b3ae-0f1dd3e8e67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 40
        },
        {
            "id": "221ffc72-478f-4475-8996-bf409a7a881e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 41
        },
        {
            "id": "0b7859c8-67a2-4deb-92e1-158eb356f422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 47
        },
        {
            "id": "33de06b9-75b2-45fe-90fa-9cbe8470c3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 49
        },
        {
            "id": "b882968a-ab0e-4c31-807f-71370a5e5b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 51
        },
        {
            "id": "525cbffb-2b6a-44fc-8cbf-42794380490c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "c47c923c-56bf-4fcb-8a2c-29bc2d761fa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 55
        },
        {
            "id": "165312bc-4647-432c-a490-ed90cd3f741e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 58
        },
        {
            "id": "f5f9026a-2aa9-4745-832b-86cea84aaa8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 59
        },
        {
            "id": "4c359928-06a2-433a-bb3a-86155c912331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 74
        },
        {
            "id": "a044ebf2-671c-4624-a632-437dd4617bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "8f285b66-ab06-47b5-8625-f616e7f90a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 87
        },
        {
            "id": "374cce9f-0c10-4b90-b146-84b3bc475ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 90
        },
        {
            "id": "c4b0e1d2-771e-45a5-a31b-073c6af450a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "f594b642-86cf-4f07-b3c3-169e178c57b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 100
        },
        {
            "id": "ec6f4d7a-cf3a-4ee9-b709-3715f8a2dbaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 103
        },
        {
            "id": "60a27cf1-1a84-4bc5-ab98-e0b26fb6ad30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 106
        },
        {
            "id": "811d64d8-955e-45fd-9457-342350841f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 119
        },
        {
            "id": "90262824-d250-420d-8d31-b5e0312e7229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 894
        },
        {
            "id": "7c7afc87-5a08-4f33-bd2d-46e79e66905b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 40
        },
        {
            "id": "e3052c29-f787-4079-90f7-942cb07da081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 44
        },
        {
            "id": "afc750e9-46d8-40a9-ad04-8f95392e7772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 45
        },
        {
            "id": "bf4ea34a-4e70-4335-b4a4-38021d546f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 46
        },
        {
            "id": "a4dcea35-61ae-441d-9b84-941097810b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 47
        },
        {
            "id": "4c569e09-170d-4701-a9fc-e06541b7379f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "887e3e44-8eea-4914-a3d0-8f5367e862d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 55
        },
        {
            "id": "45ac840b-632e-4346-bb82-9169d9e0bb13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "6dba1035-fd00-444c-be89-7278c6de0b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "84c0ee2b-06ab-4f93-abe6-cdee6713066c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "05e9205d-d666-4163-be8b-aae3b27e2307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 106
        },
        {
            "id": "2a205aaf-e98d-4dc9-85bb-9b82bce28775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "6808adf2-2538-4551-a162-5bc7cdb858c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 173
        },
        {
            "id": "233f07c5-3652-4422-9758-237a5fa1cab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 34,
            "second": 8208
        },
        {
            "id": "f90cbcc5-0e8a-4702-9e8c-8b8570404a4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 40
        },
        {
            "id": "22a9c173-c0c6-4c55-a36f-0f40b7741a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 41
        },
        {
            "id": "0c643f24-c508-455e-9150-b10f9bfac25c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 47
        },
        {
            "id": "b6f0ef2e-40e3-4ff6-abe4-556905d9b729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 49
        },
        {
            "id": "4830a1f7-af58-4e5c-a9e3-d6f386bb6d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 51
        },
        {
            "id": "b26aa465-086b-402d-a226-d080201c4464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 52
        },
        {
            "id": "24005fa1-d674-4b01-8144-642a837f2716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 55
        },
        {
            "id": "1ce02145-39f9-45ab-83af-91d3ea462f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 74
        },
        {
            "id": "9eba3963-6946-4d45-9c16-4bd03b7fafa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 84
        },
        {
            "id": "d8c6987b-fa66-4258-96a5-2cb1527333ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 87
        },
        {
            "id": "6b619487-380c-4e89-940d-af7572d9f8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 92
        },
        {
            "id": "709abe7a-f5c5-4d4d-8a1a-1d01749cea1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 100
        },
        {
            "id": "38b093ca-be30-4b49-bfc2-4374f5e1ea74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 103
        },
        {
            "id": "d9032966-4985-49eb-8f53-091153197a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 106
        },
        {
            "id": "8f4b1986-6b11-437a-9ca8-7b6940bcaee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 119
        },
        {
            "id": "584a00c1-1ce6-450c-b75f-511e27144721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 40
        },
        {
            "id": "d3ca9d35-62ac-4a3b-a8ed-d8992157dd97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 44
        },
        {
            "id": "c12dce9e-1f4f-4d3f-a8e1-e7d8ef3ccc14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 45
        },
        {
            "id": "bc97edc9-83d0-45dc-b8d4-afe0d45f0e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 46
        },
        {
            "id": "f2a4cddb-70c9-48f8-a28e-e9e823a76142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 47
        },
        {
            "id": "65b6becd-5a56-4df9-b262-067ce684fcd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "90db6ac1-3411-4557-ae1d-4873a91d1058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 55
        },
        {
            "id": "9a5a920f-84cc-4d3d-8001-488652793090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "19234571-7201-45e5-bbb6-7dda93809660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "723860c7-e6ab-4992-871a-00f3257d79d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "c44dee52-1522-4aa0-be6f-74258cc1a927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 106
        },
        {
            "id": "ed339d7a-c612-4cde-aea4-3f49cfe7a7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "1a553aa9-8202-4975-b78b-2475f1c5d162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 173
        },
        {
            "id": "b6e8f679-c551-41a1-8ebb-dbb5dfac9b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 8208
        },
        {
            "id": "ac8cff85-599d-4615-b6ec-fd6a7105376b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 47
        },
        {
            "id": "6fd82d12-b163-4dab-a39d-5616900e4a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 51
        },
        {
            "id": "b55a8cb8-bab4-4db9-8976-a083ed51645a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 52
        },
        {
            "id": "d8db0167-dfbc-45cb-b5fe-79deaea5669c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 55
        },
        {
            "id": "f12a4b66-62f5-47bb-86e7-cd6b76f1a620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 58
        },
        {
            "id": "3e356a9f-dd6a-4a79-9ec3-e41d57da06bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 59
        },
        {
            "id": "edf0af8e-5b49-4fcc-abb7-c21cd9d1d4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 74
        },
        {
            "id": "1599e91e-2dd1-419c-a8eb-c805faaec9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 84
        },
        {
            "id": "2e594e01-9035-4b07-85f4-29b6cfd4815f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 87
        },
        {
            "id": "832ef52c-e68a-43b9-a3c7-6f7c527809a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 92
        },
        {
            "id": "7115c06f-5e5e-4acf-be92-9f5bd2ee2549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "f724f07a-a6ff-4349-8e9c-3f99a19aaa92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "0774074c-6608-4251-8277-77bbdfe8c717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 106
        },
        {
            "id": "18b355e9-7e02-4f15-acd9-c8b5dee3a517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 119
        },
        {
            "id": "ac8528fb-6901-48d0-b8ed-8cdccb184331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 894
        },
        {
            "id": "e98e69e6-463f-4457-a03b-661b410e22f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 34
        },
        {
            "id": "de94d956-3807-4429-b877-0c0681d4779b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 39
        },
        {
            "id": "4990ec91-610d-4e79-bd51-31ef381a76bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 40
        },
        {
            "id": "e51ffe8b-3825-42aa-9239-8b45b625b371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 41
        },
        {
            "id": "6cecddac-d683-40a5-aecb-bd37a8ac543a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 44
        },
        {
            "id": "62e4e7e4-5c2c-4312-9896-6d1c065364c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 46
        },
        {
            "id": "e48911ee-35e3-4570-b28a-d6eb25616a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 47
        },
        {
            "id": "744cb9ed-853b-43fa-a1d2-e8f5f062b93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 49
        },
        {
            "id": "3b890b93-2fd7-457d-ba85-a17d1a35df21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 51
        },
        {
            "id": "dec4cf47-cf53-46d5-9ed6-8f9eba2a0cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 52
        },
        {
            "id": "8f2ccd2e-88b4-406e-a112-0155189c8c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 55
        },
        {
            "id": "7c33e837-92d3-480f-a52a-23038532d452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 57
        },
        {
            "id": "905b2efb-dfec-466e-9488-6c9c4fd60784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 63
        },
        {
            "id": "aaee374f-1431-4c8e-8401-3998a7a11df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 74
        },
        {
            "id": "e6ac3a9c-e08f-49ee-a132-4447133e4dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 84
        },
        {
            "id": "37a2f515-9c51-4e87-813e-417b5cda8fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 87
        },
        {
            "id": "34e1a893-5674-4f8a-9f2f-edd1c3b49c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 90
        },
        {
            "id": "7b162bfe-d83c-4939-90ef-cb29de372f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 92
        },
        {
            "id": "2f77ff47-8270-4948-8a99-bd229fa02e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 100
        },
        {
            "id": "432dc52c-da3e-455b-afd8-5a5900138dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 103
        },
        {
            "id": "82ec2fa8-8368-4efe-a3e0-8a599f57fac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 106
        },
        {
            "id": "cfd4b74a-5a6f-47e9-88c3-ddddde6c1c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 119
        },
        {
            "id": "ced03b23-14a8-4a52-ba2b-49fe7da54f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 34
        },
        {
            "id": "9112d1fc-dd84-43f7-afe4-d5a35ad76cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 39
        },
        {
            "id": "46a5e063-276a-439d-b9ff-6f70945a73a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 40
        },
        {
            "id": "d955f659-1089-4578-89a6-4fe06a765890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 49
        },
        {
            "id": "7a3278a8-3ab1-452b-bac3-13d8db0dfa23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 52
        },
        {
            "id": "5dca3697-4873-4bd3-b0fc-917f6959dc85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 55
        },
        {
            "id": "fb9847a6-6efd-4865-adf5-2b1dd098f9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 57
        },
        {
            "id": "88bc96c3-f39e-479e-9f65-d977b5b3278b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "d50c66bc-bbcf-4c59-9617-60d0e3d533a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "a7f65d62-e036-4202-909c-dabd7b0fc06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "d706e554-a5ec-4dff-abf2-b442acd98404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 92
        },
        {
            "id": "b45c2aeb-d542-4eec-8ec1-42890d7cb18d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 103
        },
        {
            "id": "19dca9b9-420d-4586-8eff-f9d563b0bf40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 113
        },
        {
            "id": "91b22dd0-55b0-4ef2-8501-ba2655104f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "bb42c211-07d4-4ecd-926c-7578c6b95a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "435be07f-32dc-41d8-9fd0-7bda93f404af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "414df505-66b7-4ef2-b8c8-89a3cad42681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 45,
            "second": 34
        },
        {
            "id": "f89142f7-ae3c-4e96-9501-719e0d75c96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 39
        },
        {
            "id": "9ebb5f18-bbce-4fce-afbd-7bbab4e2a35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 41
        },
        {
            "id": "90cb224b-7af6-4238-a13f-0cafd2c1e947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 49
        },
        {
            "id": "05d8dc83-44f9-49c3-8940-8cd20b5546b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "71ec2c08-2de7-4afd-9d5d-ad743e52fa67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 52
        },
        {
            "id": "b8450a7e-af7e-4034-951e-c82ec3e3dccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 53
        },
        {
            "id": "6953ca9d-bcb6-4a30-a1af-a6aaecd3757c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 55
        },
        {
            "id": "0e907103-fb37-416b-a5d6-f981391f0fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 57
        },
        {
            "id": "e2d1f013-c4d0-4aa6-bc67-10015a9e0ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "8e9d8a1f-ef2e-4f08-8b94-f6b603767a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "c3ca72dd-249c-4acd-adf6-5ea3dffa7afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "fc22ed39-976a-43b5-b195-50fa0c9a4fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "9f2ce57d-aa6e-4101-90d5-13a2be31a95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 83
        },
        {
            "id": "13c68d29-d2c0-4b36-9178-98940015a53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 84
        },
        {
            "id": "8c7e16b7-c88a-4386-9060-5292e3462507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "3cc6f13b-d887-4efb-a146-5d82dc487c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 92
        },
        {
            "id": "3065e552-5a12-4808-b37e-ab028eca6adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 93
        },
        {
            "id": "9abef308-cb35-4681-ba02-666d90c77a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 106
        },
        {
            "id": "1a620baa-7e6a-49c2-9680-3e5d80857b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 115
        },
        {
            "id": "02a3bc54-de68-4696-abfa-ab909570afdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "f9917860-67fd-4976-a0ee-6cfd711185fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "9ed6e64a-ed4e-415b-a437-2e87f32d31f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 894
        },
        {
            "id": "2825479d-27dd-48fd-b6de-a1052a7b5d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 34
        },
        {
            "id": "f09b1540-562d-4549-b53c-521b1d590435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 39
        },
        {
            "id": "d0a8fec0-2c31-4ab0-8794-30efc2b6a0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 40
        },
        {
            "id": "30e2f6bf-8c02-43cd-b64e-9de8c35e4991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 49
        },
        {
            "id": "9999486a-021e-4dd1-a3dd-2b65d456fd9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 52
        },
        {
            "id": "bbae59da-4ead-4aa8-bb11-40c6d1eface9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 55
        },
        {
            "id": "79a725f6-4202-4778-a840-a5d7dc78f1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 57
        },
        {
            "id": "98a93943-7099-4e30-8d01-8edb4e68a5ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "868b8c51-026d-4352-9c42-f54fb8f28c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "d5d75f76-de78-4b47-920f-fe1925595111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 87
        },
        {
            "id": "918e6976-3841-411f-b07c-fa2f7f0c7ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 92
        },
        {
            "id": "2355aeca-6264-4d81-bf67-ab148a582ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 103
        },
        {
            "id": "637d02e5-90a6-4706-b6ba-a0b0d4fad5f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 106
        },
        {
            "id": "a29b22e5-a0d0-48bd-b7d1-aac550d740b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "0b29f74b-0315-4aa9-ae86-50eba283c567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 119
        },
        {
            "id": "cfcf276a-bc99-4bcc-8dca-dab3a653f7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "34fdf04a-f9a8-4920-b22f-bf1875930864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 44
        },
        {
            "id": "43dcb0c0-00d2-48ed-b37b-9b9c5d909db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 45
        },
        {
            "id": "cd085051-b238-4914-8e43-ecbf76b0f2c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 46
        },
        {
            "id": "df286158-fdb9-40e8-8b5f-bd7233aafcc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 47
        },
        {
            "id": "57ae8195-a11d-4fd6-8cb1-a4541a9a3ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 51
        },
        {
            "id": "f9f9bfc1-0d1d-4726-b01a-8abbbd82c73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 52
        },
        {
            "id": "fd4df5eb-a100-4cb2-a083-b4b263c71e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 55
        },
        {
            "id": "0bdc22a7-12f3-46ce-82bf-3e8205ad7b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 58
        },
        {
            "id": "60230778-9ff6-403e-b678-d7804ec8ebd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 59
        },
        {
            "id": "6d9f98bb-f08c-4040-b10f-496aee3522e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "d62ee3aa-317b-41c8-acc6-da7ee5c9b0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 84
        },
        {
            "id": "9d973e2d-0c72-4dcd-aff7-f6db0ee3f139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 87
        },
        {
            "id": "e03f06a9-be8c-48b4-8715-76f5b7449417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 92
        },
        {
            "id": "2fae3fa1-f8a9-47bc-8326-7c9ad1d8145f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "3f5c6351-b009-4c1e-923d-798dfff03bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "b3c1a66a-76f6-4e50-b877-200b108cc0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 106
        },
        {
            "id": "b78372d8-dca9-424a-a3fe-4db58c2bbea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 119
        },
        {
            "id": "90d64918-65ee-4bfa-96db-06f02fa23d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 173
        },
        {
            "id": "042ffe25-659b-456c-afb5-7b57a38a91e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 894
        },
        {
            "id": "d27384ef-2412-4d70-bf2d-593f0c81eadc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 8208
        },
        {
            "id": "aaf5837d-6135-4de2-86c8-337d72be0446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 47
        },
        {
            "id": "54f2b9c0-2f84-47ef-b33a-c37bbbf76c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 74
        },
        {
            "id": "52615db9-e551-473b-9528-4598ae9915cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "d377c12e-8a83-4eeb-b936-3c664dc224af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "cf3a76b2-3b97-4395-acac-eea50fbe4a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 92
        },
        {
            "id": "09c9fb5d-513a-44e9-9487-cd012e6315e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 100
        },
        {
            "id": "1bef8d44-9f85-4bd2-9b51-bda9750a39aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 103
        },
        {
            "id": "07f22579-73f0-4d12-9d18-b88560b65b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "fc60725a-81bd-4a59-bc98-84327f7a4165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 119
        },
        {
            "id": "6891bcb4-e442-4e2a-a87c-5d3394d7ea6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "1e9c9884-2cef-45e3-8588-01601a91fbdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "17210ba9-6089-4b05-94a9-61485428698e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "a3d62b6a-1599-4fcc-b339-c5c7da4ab123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "071c257d-fc5a-4bd1-9fac-f29ff2c9f611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "ce31c666-154c-440d-8bb0-7f74298e18cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "e0863f69-b2db-45bc-9901-c997bfc26464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "84d0251c-df3d-438c-be48-b6f7b4b4838d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "f0326cbb-6c8e-4719-bc40-cd95b8a919ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "489af7bc-db82-4c43-9c76-fde85ec4649d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 40
        },
        {
            "id": "d3ea6ae9-4822-4d78-93d5-08ecef849f16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 41
        },
        {
            "id": "c48b5214-a921-4ede-af01-fca35681f97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "c53aed14-4043-4315-9fba-f1a0a23643a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 47
        },
        {
            "id": "a0228135-02f8-4b51-8626-e1e114607400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 58
        },
        {
            "id": "67f38cd2-db31-4b13-9163-692b03cdd110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 59
        },
        {
            "id": "f3491f3f-05ea-41e4-8b30-b6c18cfbeccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 63
        },
        {
            "id": "ec223c85-4b15-4909-93c2-8337ad05ad4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 74
        },
        {
            "id": "e7b6385e-5d60-4758-97da-7dc6480ca309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 84
        },
        {
            "id": "854fb155-019d-4ffc-8113-a959af72b443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "358662b7-9f8d-4fa1-8a24-bd2810b1eacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "674a7cb5-ba7f-42c3-a880-9d3186247ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 92
        },
        {
            "id": "e149eaf6-c092-44c1-9112-d597a672e855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 100
        },
        {
            "id": "be490837-6e28-47d2-9943-c267710ec683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 103
        },
        {
            "id": "49f5f43c-de59-4111-84b4-99ff712867f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 106
        },
        {
            "id": "6c22001d-c82e-4daf-a8b6-4b86cbb83bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "96aea11d-38c1-44ac-a957-f3376da39341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "f02782ee-4979-418c-b933-8440f96a3cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 173
        },
        {
            "id": "06ad1f55-7ed5-4b32-be78-85c8c133af07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 894
        },
        {
            "id": "dbbadc80-4642-4f26-9805-59d1874ecc53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8208
        },
        {
            "id": "b29791fe-acd4-4d92-860c-8cf0e890e2ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 47
        },
        {
            "id": "52d398ff-638f-4cb3-abe3-02429d4e5a1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "b99d9523-221d-4dcb-a511-242e03826eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 84
        },
        {
            "id": "3c688356-457f-422e-878c-0f134076006d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "beb30d31-e22f-44d5-b19c-5cf7e27e1d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 92
        },
        {
            "id": "464a618a-c0d9-4131-9679-058b1348fbb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 100
        },
        {
            "id": "b3d17f3c-9834-4e0f-84a3-e0b28678fa2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 103
        },
        {
            "id": "b07dd73d-d4d6-4450-bd21-392219d72b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "1c48699d-7982-4e36-b206-461b8eb821be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "de8305da-df69-479d-983d-3bfd6a14fcc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "3920f353-7284-4e76-8727-6882797a6012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "5a0391c0-c2bc-4a93-8c20-0c4c4195272f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 40
        },
        {
            "id": "3c4bb561-c647-4877-a682-7f8c25458313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 41
        },
        {
            "id": "2340977d-5dc0-4581-be58-f22df1eb2c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 47
        },
        {
            "id": "bfae603b-4beb-48e2-bd34-17b329a56b44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "71e22afa-7be5-4a56-b8ba-b5f70ac8193a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "a967c489-f9b1-4be6-b506-0f669befcbd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "02bd472c-fbdd-4daf-a85a-ed94754f2395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "b9624393-3faf-4faa-8450-27d59ceb62c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 90
        },
        {
            "id": "0351a866-44eb-4421-85b1-6ca3f53f896d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 92
        },
        {
            "id": "c46412bd-b4bf-4dc2-9293-e9486bae05c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 100
        },
        {
            "id": "eb4bb37f-1e17-4676-8fb0-19530cd7a1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 103
        },
        {
            "id": "82ad088c-4fe6-46de-9f29-92847983ed62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "cab18bbf-df54-42d3-af5f-12a587fd5dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "76d21319-e7b1-466c-a42f-d6f334b1eeeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "6ccf4b90-9d74-4d12-9b0f-65c108c0edb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "7c47e96c-4b7b-41ff-8024-a3d66e001d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 40
        },
        {
            "id": "811c8262-37b0-416c-bfda-fa99b79e37ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 41
        },
        {
            "id": "ba3d2780-3103-4bb1-9c1a-dc1eedbfda7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 47
        },
        {
            "id": "64711273-284b-4f96-8cc9-d373d6de9ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "f8c5a42a-be3f-412f-999b-26be9b291858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "faf7475e-18b3-4ad1-9ac1-f1e9a38a4cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 63
        },
        {
            "id": "e6c64f42-21e0-4511-b002-e75527ed7da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "c6bbbefd-0959-485a-800c-be4cbf176ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "7117c990-257a-477c-867b-0918ded7a441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "0b917d1a-8617-4b80-9611-b6f794ed2cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 90
        },
        {
            "id": "a7b02e26-dc30-41be-a4ba-4c900b7bfb3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 92
        },
        {
            "id": "0f8a6b09-1373-4763-9d42-5d36e682d0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 100
        },
        {
            "id": "37208546-a19a-4476-bdec-76c826715c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 103
        },
        {
            "id": "f4cc7ed0-acef-4c0f-ab03-6131e3845ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "0c57db6a-926c-45c6-b255-a2e8c1b9a2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "da459dc5-7799-44f8-8af2-732f4faedad7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 894
        },
        {
            "id": "dd6b95a2-f755-417a-a4a3-07f14a930326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 34
        },
        {
            "id": "965cee8b-b4ad-4af9-b95d-03374d4b2775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 35
        },
        {
            "id": "05dd38d0-3103-4557-a6aa-9a92c38b170f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 39
        },
        {
            "id": "a27f2cf6-d031-4415-8d1d-2871fb5f641f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 40
        },
        {
            "id": "1d623c42-ca80-46f3-b1c2-f0f221e6a9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 41
        },
        {
            "id": "7726a82a-aa0f-4ac0-81cd-c280a4ed9a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 47
        },
        {
            "id": "8c31644b-1e7d-42d2-a59a-809738632e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "757d5724-fae1-414e-b190-aefffddd470b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "be9010c3-5879-4353-9c03-4c6642a0f2f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "e9971b61-5d14-4305-a8f9-6d189f3595a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "e6641e2e-7473-478f-9919-5b2dda8cb228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 83
        },
        {
            "id": "3d7f81f7-9f70-4e1f-af87-b6282ee2fd15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 84
        },
        {
            "id": "26089af3-2480-4892-919e-a54a3b6f67ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "df0a3b3f-5f1a-49cd-ba81-e73baa66774b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "b113617a-399b-4df5-86ea-03e0d624e60a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "9c2472b9-f928-4c31-8a21-9af03c019d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 90
        },
        {
            "id": "b9a95e9c-a7e8-482a-a850-20e77491d414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 92
        },
        {
            "id": "6c2f4609-c08b-4f1e-8823-71f1a04db974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 93
        },
        {
            "id": "e10b1532-b0d1-4724-9426-79e6d106ebf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 100
        },
        {
            "id": "cd566fa0-3641-438f-8190-df8b68bacef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 103
        },
        {
            "id": "bd2f46f4-7762-4e58-86cd-bdcd40ea48ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "55e50308-f741-4778-948f-bffdda3aa59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 113
        },
        {
            "id": "67b0b599-37a0-4869-ba19-ef6f7597bdf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "b6956916-efc8-4284-84cc-134d4725dfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "a81a1dec-4ca3-45dd-a935-0d0b7055d747",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 894
        },
        {
            "id": "5d22c725-1d79-4847-ab65-1035426051be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "c74f068b-e9f9-4531-96f5-55488ca8059d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 74
        },
        {
            "id": "db5047f1-b6bf-4699-aeaf-21a1182c20b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 84
        },
        {
            "id": "18122683-11cc-4f37-9661-c099390742ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 87
        },
        {
            "id": "c23f615e-8b8d-4f9d-8c55-b9f6d931af09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 92
        },
        {
            "id": "3f7dd04e-8741-40fe-9c72-0c0c1cb82256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "8af396fa-723c-444a-95de-2fc708485b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "731b2fb2-816d-4bda-b10e-e832178a9948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 106
        },
        {
            "id": "757e1478-b1ce-4d8b-b4d5-6ccbcda77946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 119
        },
        {
            "id": "27c08669-1eff-46bd-90e8-4dde777f9c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 47
        },
        {
            "id": "c1fcd08f-8595-41b2-b00b-5164972d8af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "a141e892-50ea-42a7-962d-e909ba33f4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "fbbc245a-241d-4b74-b66b-ca24dbd1ae60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "3501f49a-e2c6-4b63-a3dc-1733c1942101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 92
        },
        {
            "id": "c84abe34-e527-4cfa-bd5f-f0997f31985a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 100
        },
        {
            "id": "8ebd81b1-f9df-4fcb-a2d0-1d371a7d0f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 103
        },
        {
            "id": "62eb496a-f7c3-414d-b5fa-298d4ff51718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "ed3983c6-ecee-45a0-9f6f-af79b4df07f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "557daa5f-6aa2-4860-8e2d-0e3482f4f809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 47
        },
        {
            "id": "45c014c1-0c84-4061-976d-9f55df2c291e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "5d3fc430-dc95-4e12-8eac-00161c079a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 84
        },
        {
            "id": "6a75b614-2272-421c-a051-9cd84fc420ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "a655c905-6f96-4a30-a98a-c9c28bfe2e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 92
        },
        {
            "id": "cf16cc18-dcb7-446a-be1a-9ae734a674d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 100
        },
        {
            "id": "7828af9e-76bc-474e-b352-83ca7aa8f3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 103
        },
        {
            "id": "a7028193-a6db-4c88-8849-4c347acc4f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "39785728-8368-4130-bc2a-ccf4ee482d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 119
        },
        {
            "id": "4f176806-1a3e-4601-940a-9ff12c3d76fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 41
        },
        {
            "id": "a058b924-07b2-4dfc-aac7-4676c26f7bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "b48854bc-2b2e-4bc8-b5d2-17c41e5f452f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 51
        },
        {
            "id": "7e860c52-fef3-4212-97bb-f2958532c327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "0466d573-b381-4afb-b9c8-eb5ff1974b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 55
        },
        {
            "id": "7d82ba7d-f7aa-4cd2-9aee-5df06c6c6ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "b5bcdc60-d5ae-45d9-8780-30c73dff8404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "e15328b1-d760-450e-bbfe-4ba1adbc79b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "e0f633a1-f57f-459a-8c0b-8226e4d22c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 92
        },
        {
            "id": "6ddf9e1c-d487-47da-8757-ab71e6ad4bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 106
        },
        {
            "id": "3531b908-15e0-4541-8fe6-86b7b09be9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 119
        },
        {
            "id": "16d7cf31-cfcc-4f23-bfb2-2a9c478dcb86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 173
        },
        {
            "id": "0794760e-770f-475e-8413-6225c6fda9bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8208
        },
        {
            "id": "f376f92a-b408-42d9-abf3-bfa24d90d2e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 41
        },
        {
            "id": "7096f18e-684d-4442-b564-c6b39a7259c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "29bc7cd9-d401-45d7-883a-340283b18a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 51
        },
        {
            "id": "503c11c5-ebe1-4995-aa66-05d4eeeb4d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "ac2a7634-ee37-4f66-882e-7f54a206b09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 55
        },
        {
            "id": "87d8244e-2ee0-401f-9467-aa30071432f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "673bc249-6978-46ef-a19f-57b7c32164f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 74
        },
        {
            "id": "9564c023-ce2d-413b-84cc-f74857facb27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "002c529b-f45a-4d76-b256-f49663b4f77c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 87
        },
        {
            "id": "28af2994-504a-482e-bd5d-c39144d35a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 90
        },
        {
            "id": "7140ba7d-f538-4cbc-bffc-0195e3830b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "1e826648-5fca-4824-9ae0-895566c9825f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 106
        },
        {
            "id": "ccf42bfe-2ab0-49d8-b335-34ac362f20e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 119
        },
        {
            "id": "e282d24b-1964-40b9-b172-0c04407ce64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 173
        },
        {
            "id": "15fae23f-0cb7-42b3-a6ff-d15f838fb9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8208
        },
        {
            "id": "54200256-13e3-4f1d-b6de-7e3c391e52c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 44
        },
        {
            "id": "d6d101d4-aa44-44e9-8631-f564ddd78471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 45
        },
        {
            "id": "39154909-aea7-4660-bf69-b7e2c26ed8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 46
        },
        {
            "id": "02e5e5cd-da71-4e8a-a089-956791fad340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 47
        },
        {
            "id": "662b0971-ffad-4c71-abd0-96a6faca370a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 51
        },
        {
            "id": "47379aff-be9d-4ac2-a08e-dd5bb67208ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "59e22eaa-79d5-4272-ba19-72581bd05de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 55
        },
        {
            "id": "f62dc0fa-be75-407a-9558-58be7711be18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 58
        },
        {
            "id": "8b791287-653b-412b-b55f-45de40ad3bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 59
        },
        {
            "id": "f3e76059-52cb-494b-ae0e-4ecacab8118a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 74
        },
        {
            "id": "0b01a997-cbde-4e2d-9a32-8cb3e3a26371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 84
        },
        {
            "id": "8f874723-f1c8-4142-8f50-3b2dafd813ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 87
        },
        {
            "id": "61238b26-1c9b-4d36-bda7-b0f9c1c9a0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 92
        },
        {
            "id": "59ab0f81-94b4-4959-a7db-e6f891c7fa53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "f9b49a2a-4eb2-4576-bc19-1e52b65f3c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 103
        },
        {
            "id": "cd8d337d-4add-4cc5-8681-6ce595c3101e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 106
        },
        {
            "id": "7234f9f4-8a5f-4268-8357-2da43feca6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 119
        },
        {
            "id": "5c9b5aa1-1c87-406d-995b-959d24bd28d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 173
        },
        {
            "id": "bd08c08b-c801-444d-bef7-693578205f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 894
        },
        {
            "id": "148c58e9-4998-44d6-87a6-d62c87c58001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 8208
        },
        {
            "id": "c0996336-bff6-480f-bc51-c6d93aabe785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 45
        },
        {
            "id": "30b85e84-bbfc-4426-b94b-34f6bbd34b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "6442adc6-7c65-42d8-b139-c591c630d865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "fcf8c641-eac6-40c1-aebb-71e71c0543dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "f42ff60c-a5e3-4c46-aebc-6876494790f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 52
        },
        {
            "id": "7cb0b685-fcec-4c9a-bd67-24a0017c14a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 55
        },
        {
            "id": "6aea325d-e0ef-41c8-a5b5-80fa5e381bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "d056075a-4421-412b-b3c4-37fc41276b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "1740a5bf-57be-4dd1-8b75-bb232acc4181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "c1ac33de-22bd-44cf-a795-2bbbbc1dff63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "248e86d6-6dee-453b-b33e-c9028cab6054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 100
        },
        {
            "id": "35b9a8a4-ebe6-4682-b185-2c6e495c90f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 103
        },
        {
            "id": "c3042d36-5427-42bb-9a7a-7187f53ccaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "018f0466-a867-445d-925a-d855d5a4f004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "9388f849-aa9a-47f0-a2e8-a843442c48e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 173
        },
        {
            "id": "ec47fa4f-9829-4993-9c5a-e5a3a0be1ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8208
        },
        {
            "id": "3a77427a-def0-4b2e-8ad0-8a56a37225e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 47
        },
        {
            "id": "7269a17b-755f-4b15-b97e-ba5d49427a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "88b3a735-0fbe-4bfd-847a-75352875b122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "bd0953b3-35a0-456f-ab0f-54e990eb23eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "12adabaf-1095-4031-bba5-1ccfc55b500c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "e6173e1a-9269-42b1-99ad-ae16662fd8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "c60b5efc-2a47-4f97-b5a6-75f7ac9e55ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "f167a997-d03b-4f7a-aa00-c8893670f045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "9acfe932-39e7-44db-86e0-4401911f500e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 100
        },
        {
            "id": "27ff9505-983c-45fe-beb7-f5e166c55c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 103
        },
        {
            "id": "e2a1ad2a-bd0e-49fb-ac66-18be117464d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 106
        },
        {
            "id": "d1781756-43de-4ecd-b881-20a01ec60a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "960fc593-cfb6-4c71-a9bb-c1658b135a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 34
        },
        {
            "id": "ebdf2dba-a4c1-4058-8d4a-2ac2f2e87892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 39
        },
        {
            "id": "889a3c1b-a2c6-412c-bf5f-19a9fb6bf9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 40
        },
        {
            "id": "0f96049f-0c1f-4b04-911e-1654d638eca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 41
        },
        {
            "id": "7459eeff-7f1b-4955-861f-83e98bb42621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 47
        },
        {
            "id": "dc589dc1-30f6-4aec-a8be-6d68a6108656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 49
        },
        {
            "id": "336c3507-d7e2-489a-88a3-682019c65434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "ea080e4c-0619-4861-8af0-093d634e3daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 51
        },
        {
            "id": "5860dbf2-e149-4656-ada7-4bab3acf940e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 52
        },
        {
            "id": "ccb2fa9a-2ac0-4648-8340-6dae86b75920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 55
        },
        {
            "id": "5fdbd56a-9f7c-4690-99d5-624a92ec5b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "f22b00ba-7178-4f15-94d1-0c1069efc772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "209da8a8-9d69-4306-adca-911584446981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "74cb61cb-372f-4253-97ad-c343d3f7f05e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "b3d4787a-2d48-4346-a728-6dedd31d87c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "02dadebc-9933-4d36-b8bc-75fbd6fd9645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "fadf2926-4127-4b5d-ac4a-0a824f741d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "5d849972-b666-4991-8d2e-59e60dcbb29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 100
        },
        {
            "id": "55175748-6619-4696-8e21-57c52ac3b5fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "d6977b3b-f95d-4c2f-845e-f665c84eddd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "586b2a4b-47c3-42be-8d0e-084ef56d711f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "47e43451-feef-43f7-8427-deb1ece99eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 40
        },
        {
            "id": "eccdb60e-bee9-4cdf-a5aa-c77c9b73e187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 41
        },
        {
            "id": "af6f787f-a1d4-41ae-aced-7fd216108750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "e1b8f449-41b6-4036-bef7-2e39e950e57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 47
        },
        {
            "id": "598aeb2f-ef75-45eb-bb9c-d156e5404926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "2ddf4187-d307-4b9d-b93b-d8bf1a46c088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "2d638102-1f6f-47fe-9dbc-50883a08b511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 52
        },
        {
            "id": "521c7510-68e9-4dbd-b92a-d9a59781b84b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "6637bcda-d433-4fa3-8ef5-a786b525f53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "6a95f585-5018-4605-b9fa-742c759d6216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "da4b5963-5835-48e2-87f3-d6fa2267c8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "2585e82a-0143-4396-bd0e-d7cc364bf7b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "43c78e83-ad20-47b2-8e33-cb43307dda63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "cd0add6c-6a72-4a25-a7e7-b3851383eafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "f2971f3f-3dfe-4bdc-9fb5-3b9b5047d9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "062e88e0-b6ad-4574-b67c-bcf5b9186a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 90
        },
        {
            "id": "c6553e72-800b-4b12-9310-8da5b5024f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 92
        },
        {
            "id": "2485cf10-910e-4533-9f9b-6863a85d98d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "0244f355-9b5e-46ea-bda2-fd422578af25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "f51b4815-638a-4220-b9e3-7e96f35d2079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "70a79744-cf83-494c-8878-bb40ec19d2c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "7f30e3ff-44ed-4b9a-b79e-96bf742db635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 115
        },
        {
            "id": "afe30ad4-b8ef-403a-a56c-ac36a2b04c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "17256cba-24f2-4243-9aaa-bd886c0dda5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "022db710-daad-412d-a335-76f649f6b1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "cf486c4d-07b2-48d5-8b3f-b1ae334bee21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "1621076b-a82f-40cd-8f42-b5c6273b8117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 122
        },
        {
            "id": "22707c02-f5fa-4c20-a17d-7acc8f9fc718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "b6065e73-93bb-4b25-a149-f79cf3b3c1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 894
        },
        {
            "id": "f8b771da-6374-4142-81ba-3c5ab817c44e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "c75395cb-143a-4a8e-afa2-e4668103d7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 34
        },
        {
            "id": "588f8cea-b3e3-4e67-9cb6-7dacbce0a602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "227b59b2-7c76-4cba-9bc3-459ac12f20ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 40
        },
        {
            "id": "0b8c59ea-6082-4736-ad8a-8e18a96f7945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "21844a9e-7afb-448e-93cd-1468cf9e74f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 47
        },
        {
            "id": "1a7accc8-d44f-4362-a3ae-6b86aab706aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 49
        },
        {
            "id": "5dcfc751-422a-44ce-9857-877394069a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "daa38917-5c6c-4c53-8c47-c3fe8eafb8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 52
        },
        {
            "id": "80fc8484-baa6-4650-9572-2cc80c1e0599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "4e4ed318-e4aa-4a79-9fde-e9744a066afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 57
        },
        {
            "id": "27537609-0ad1-4aef-9457-f823028def84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "9ec37644-2545-4e9e-8b73-acaa9e839679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "0039be5f-5a29-4632-a311-28ee5c8d6575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "5242b8a0-e31f-4267-b41c-e439d6242966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "1807f412-a2ef-446c-947a-848f50160256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "f0e511e5-ef7b-4a13-86f3-4bb58d052eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 92
        },
        {
            "id": "772773e6-9875-4874-b42c-0dce30e8700d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 100
        },
        {
            "id": "108b3a4b-0a70-4f69-9203-34c81c3fa400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 103
        },
        {
            "id": "58b41790-e43b-466c-ad27-152166437f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "a65747ac-7c97-43f8-9188-7108121e5eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "0ba5bc95-d3d8-409f-9519-6243fc8be666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 47
        },
        {
            "id": "458ceedb-2a7a-492a-ab85-557e067c4007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 51
        },
        {
            "id": "97c1eba0-b5d5-489e-9509-5eaeb351d4ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "08aaba67-5e8c-42fb-8116-287bf3dd1d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "504114d2-63ff-4f13-b7f8-784c2c87a153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "911239cd-c444-4e40-adae-0fe65e11c674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "dcb5c924-3b11-4e03-ae11-a039ebbd86b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "dba38947-ce92-4607-a9e2-7a8852a80da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "f36b743b-2c05-414a-a0c4-4ffe2e08badb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "28a1d8d5-beec-4643-bbd6-b9f0f7d36fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 92
        },
        {
            "id": "10d97ebb-2cfb-493d-83d4-80d1b3d5c7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "72c40a17-5ed3-4449-bed4-49858713d94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 103
        },
        {
            "id": "bdf85558-2a4d-4eb7-89f3-a1f7fe887ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "beb093ec-6d97-4df7-80f4-54cdb0ae90ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "92a7c191-ccb3-4133-af0c-0391d5546d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 894
        },
        {
            "id": "4a1b8a02-5d00-41e6-af2e-f703cb1e2f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "5cc32853-ee45-47d3-b433-493c412f5bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "bca464df-42da-45d1-8bb4-2b2bfe304c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "a997b8f4-0596-4923-97ac-f1502a5ef7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 47
        },
        {
            "id": "48db7621-cd58-4259-8da1-9bc0b7eab5bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "80a0c23d-0ac9-46b5-bf77-264c343dddb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "590f955a-dfc7-4d5d-97ac-bdda175fc7b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "c599bcec-02ac-46a4-99b7-a3f695a72680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 55
        },
        {
            "id": "2b84c9ad-d15d-4481-8a52-adfbe701c1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 56
        },
        {
            "id": "72e6cfbc-8e87-4f01-882f-f6384b9a036a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "8944166e-dbfd-40e9-bb0f-24da553cfc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "d1fa3b0a-6869-4c33-820f-49975244ef33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "4531f221-031e-472d-a25f-6977617f0d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "8761f609-cd07-4cd3-b069-6d901ef31b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "6c582ffc-0b07-479e-b4d6-e18cb3e69535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "51512298-9b8f-4a71-b963-92d35ca74d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "f49875fa-f7a0-47da-8548-f188328a3396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 106
        },
        {
            "id": "268c8b2e-3863-46fd-88eb-aa677b709807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 118
        },
        {
            "id": "75395924-5825-4b72-811b-806eaaf7fd92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "f00f1c11-0c7c-4aa2-b442-bfe8eb46691e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "b3a000e9-1306-4f22-ac6b-84ab98db92de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "9b848ab7-4c81-4c2d-8718-8476f4e16013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "7ab1848d-3814-48bb-af11-93c7918e5687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 894
        },
        {
            "id": "dc2d81f6-d854-43ab-801d-64ad074cfb56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8208
        },
        {
            "id": "9aa30b86-f7d3-4626-9be6-e95e48a2bea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "112fdd39-d0cb-4569-aa27-3f77bbc2821e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 34
        },
        {
            "id": "079cdfd8-75ed-4cf9-a0dc-3997237054a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 35
        },
        {
            "id": "3f7097fc-901c-4dca-a1fe-37a958b0f456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 39
        },
        {
            "id": "7d7bb264-de54-46c9-9829-a8b55988c75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "55c4bc7a-c390-4dba-a146-a016e8c192bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 41
        },
        {
            "id": "501e1225-8449-4f16-b373-e81f7754c365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "2b73276b-b2ae-479f-82b7-2c4651a2b6c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "b30bad99-d1bc-4a87-a89d-beb9a175e4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "2545de52-cf61-4e66-85f0-d1a1b496541d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "7eb5147e-95c6-4eae-9800-0cffc9c8aa1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "aeeb1186-0f19-46a2-9d31-0c675acb1d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "aee73308-ef38-44ec-a5f9-f560deace2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "2506df05-dd1e-4411-94b9-4b51fede5ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "5f0afe2b-cfd0-4fa7-a452-1229f030fd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "d8482669-7ffc-441e-8d6d-8659ae4aa72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "c90711b1-b794-4e23-a5ae-a8c39d927bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "cb7e4255-6ca4-4741-8f6c-cda63f2b7585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "77857288-52dc-498c-a5dd-9f88f039d170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 64
        },
        {
            "id": "bf30b9b7-84cc-48b7-a825-c45b16087afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "b7d4032c-8a79-4cb5-bfd6-75a0a6183470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "c1aad589-dbe5-46fd-b9c7-85b81a0b780e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "9866c340-cee5-4895-a865-a20807df8472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "e2769fb3-234c-4e32-b666-2cce93e16437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "9613c4d6-40a5-4cbc-af40-4dab9c365b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "f0c15863-e2c4-4cef-80ed-aab2dd9eca23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "53be7e60-fe66-48ff-af28-bac0bc3be30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "8d010c3d-22fe-498c-a336-45d6a55bcccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "2f281585-802d-43c9-be03-4a39a7880f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "1df700f0-3190-49ff-8db5-683a6e28ecaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "2153adaa-a5fe-48d0-94e3-9b9ee6642411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "9d3b3e40-050e-4b6c-b0b7-26b0ec77491a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "6da488f7-ea83-4583-a0f2-47547537bec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "59756bce-5a30-496f-80f0-499a1960d271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "4fa580ae-488c-4dcb-9c4b-75f08f9dd8be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "acc3efb6-b2f3-44f6-b9e0-dc24bbd28bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "b260b66d-95b6-44b1-a72d-c9c0d3599fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "76cfa8fb-f82d-4a0d-be60-396ec1b400ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "bdb3a7c9-266a-4ea3-adf1-5f02ae657275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "6b8bc9a0-9a03-402d-9a02-6208948cd2a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 894
        },
        {
            "id": "fa074169-b59e-4201-a394-c35707fc2b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "94f9936f-c25f-4580-83b1-a196b1deca4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "e5398df4-d1df-4329-b5c8-3fcebd6077af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "f0c68b2a-412c-4490-affd-036a27434cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "fd0cb220-c06d-4160-9e8a-75af66261c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "851f2f22-c764-46a7-a519-38e698c16875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "8558c8a6-3ca2-402f-85b0-4ca8cfbcfb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "917f33bd-adbf-4a36-abd7-39a934032dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "eea637b9-a0ac-40b2-a8c0-d7df2daf1afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "68dab49b-e5f6-4ea9-abfd-384cc1a6d9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "cc723526-605e-4f8e-b01b-4a8609fc5fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "27c385fa-f96d-40b2-ad11-87f93ff7413d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "e7e9c9fd-b5b4-4104-b9a5-c4838f3cc9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 47
        },
        {
            "id": "5ffd4acb-07b0-4fff-9d79-6dabeed7662d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "1ea4b6fc-be6c-4dcc-9570-6b49c65ffff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "ae955803-3deb-450c-9072-87979b697cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 55
        },
        {
            "id": "d62795db-e014-4ec8-925e-543596566163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "5700a4f6-a3a1-4d5a-948f-bf76f429896c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 84
        },
        {
            "id": "8c132ab5-41f5-4972-96d4-a7b2fa7a1577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "774718f1-38fd-4463-b2aa-ed5da9ae694e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 92
        },
        {
            "id": "44b0b806-0768-4730-8e47-5b7a837ddae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 100
        },
        {
            "id": "fc4c6132-3f78-4ed8-ae56-8022f8c3dd1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 103
        },
        {
            "id": "ecf345e4-d312-47b3-8ade-c8c85bf20425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "3d36d4b3-52ef-4943-bf0f-8178b8160bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "ca11a328-32b7-4339-b3be-88cdbc442881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "979844ec-7005-4020-baf2-3cc9f9307447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 45
        },
        {
            "id": "8cacd561-fd7f-49ca-a5d9-a00b842b7c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "fb0b8532-b500-4c82-b825-b36fe4253151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 47
        },
        {
            "id": "531e437f-62fc-415d-a96f-1e96ea6e25c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 50
        },
        {
            "id": "1d693946-1384-459d-a043-f403b2989d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 51
        },
        {
            "id": "489eb84f-be6b-4155-8d8a-c03e9547cf75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "6c053a43-fb37-49d3-8c7c-6915554ff312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 55
        },
        {
            "id": "d2580c6f-0673-4d08-a5e1-f29ce224baa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 56
        },
        {
            "id": "890e18d4-6a1b-4287-b623-e9cf736dfa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "0972788f-77d3-454e-b384-a42fdd6910e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "85cde999-87a6-49d4-b0b6-5a5ce7383d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "e4859b1b-7a49-4ef0-9435-2b2a572cce75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "bc8e9b71-6a0a-433f-9268-21e396cd6499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 87
        },
        {
            "id": "a2f73650-fa90-4a1d-b1f5-709d5ec76e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 92
        },
        {
            "id": "01430cde-5284-4949-918a-e7c5f1db64fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 100
        },
        {
            "id": "998c8298-5244-4bd4-9ac9-c49dbfd75ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 103
        },
        {
            "id": "3f22caa3-10b9-488b-bf14-cddc24aecc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 106
        },
        {
            "id": "90195bf6-a6c3-47fa-85b4-3e50df52d58a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 119
        },
        {
            "id": "a98f3439-f724-441e-8ca6-38aa3fbc5d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 173
        },
        {
            "id": "6204732b-e555-4c91-9fd2-485c73ff1486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 894
        },
        {
            "id": "189c48a4-73fa-4200-b339-4db01fb16885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8208
        },
        {
            "id": "54ec27f9-52e7-4701-8842-9d2f11b24559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 34
        },
        {
            "id": "243398df-fd4b-4342-a966-8ef00982b855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 39
        },
        {
            "id": "aa5cb919-f926-4fd6-b61e-acb577aa2ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "04aca82b-d797-465b-9f18-34ea3f9b77c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 41
        },
        {
            "id": "f6d4acb5-0dbb-4ce7-99a7-a746e93f01f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 47
        },
        {
            "id": "e7deb824-1dca-4b80-911e-2f0aabbfbadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "c8587a53-4cc1-497f-8143-f38054526954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 50
        },
        {
            "id": "b948a0fe-bc17-46f4-8849-35b4290a8764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "adf07da8-a4d6-4599-9065-933845b83625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "942b966b-ca3f-41af-bac7-0a506c9bdc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "98f10300-6c43-4361-96a3-85d60ecbb06b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "3367746b-e48c-4a1e-a13b-47a22d3ce727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "8b782994-d573-4a54-bf79-9f5eb9bae67a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "2139ba7e-8e14-4429-8cba-547bf07c4422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "25af27b5-1f57-44f1-a7f9-fc34f144807f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "53ebefe8-106c-48d5-bc77-5397f2f261f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 90
        },
        {
            "id": "b30eda94-9de2-4c35-b396-44db5a38544d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "f799870f-9192-4640-a9f5-87a5e827ac75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "bfebc214-c151-45f4-b2a4-d7f2305b2dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "3e6878d9-e1dd-4a64-adaa-f35c3581faba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "080d6c4d-4d27-40a1-a981-cec27624b6f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "684019bb-bdb6-429f-b622-28c855430038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 34
        },
        {
            "id": "7b8a1890-9527-447b-8027-f95e0862102d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "3b596d90-0121-4e5e-8feb-2824991f09d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 40
        },
        {
            "id": "f74e642d-3a36-43e6-a20d-ccd24403959c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 41
        },
        {
            "id": "d2514fad-3607-41ac-b3b7-8363e82edb79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 47
        },
        {
            "id": "0a98feb1-bd94-4c39-88b8-0e98fe7c125c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 49
        },
        {
            "id": "59212ab0-55ba-4ee7-97f4-fc77ff15cc0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 51
        },
        {
            "id": "250c9b39-d4e0-4c50-8956-2b22ae6d1740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 52
        },
        {
            "id": "231902f1-2c28-4389-af82-d1d52fae2ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 55
        },
        {
            "id": "b5f74295-bb5e-441e-982f-cee458afb53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "f9e96ece-de3e-4ce7-842d-df0441625a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "8c538a31-6802-4d24-b0a3-0ca3d5e26126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "5f1180a5-b00a-43af-a3db-2109af187dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "264c5972-e0fb-4deb-8532-ae4980b9d429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "a9d39b0d-98f3-4c4b-a99b-32cc557e8552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 90
        },
        {
            "id": "31327a04-78c5-426f-bb38-1899c367d628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 92
        },
        {
            "id": "4d1f1998-1db6-4dc2-8b45-7b0042a511de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 100
        },
        {
            "id": "7e4d71ed-38f3-4277-8b61-de27fc95bfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 103
        },
        {
            "id": "f689a8cc-96cd-4e4d-a7f7-589690a17072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 106
        },
        {
            "id": "8bf2f519-09da-466c-943b-fa9fd877ba7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "9a31e158-2dff-482a-b3a4-bb39e1fea717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "bdaff45a-340b-4d19-a629-560325e137c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 35
        },
        {
            "id": "295a0b9d-8c4a-42c6-9de6-5d650fccb8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "10c5c433-50d7-4c21-8a68-56f4c456d491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "54026bce-4839-4c80-bf4a-1b8b8146e0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "25790513-80a4-4e62-a5b0-4766933d0ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "aff45d88-a107-45e1-b15a-498d21ce3ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "aa5dcce3-ae18-4001-b33d-2db152ad3245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "c3522ac8-dbf9-44cd-89a0-776a376c80e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "7a09974a-ede5-4f18-852b-71bda6b2e3dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "c4e74e02-cca0-4bb3-ae52-906f36026e89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "37e78a43-46ae-432d-851a-3760f624651e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "377b11e9-142f-4d26-9ce2-196c1cf6a5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "881f4949-01a9-4c12-a418-7b5563fa9e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "d20f7b1c-c9d4-4e47-bc02-bd2960aae8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "0e1e1a20-ca79-4524-9bab-99ff4a050fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "dbc1d789-33c6-4d2b-8c0e-590a69cae6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "f92fc445-3b3b-41ea-b746-9cb1292105e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "4e996451-4c62-4588-8738-cd8416fe2cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "b3d18417-a429-44e6-8c62-6d5e337442d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "b30da1ab-e96e-466d-9799-aaae3f116356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "22589f16-214f-4e35-a1d9-a9b98cf2e0cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "e82a53db-4da1-4248-9c7e-606450bb81ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "786101c0-38da-4cd3-b8eb-12e6a557e348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "b5b4d7c7-c3fd-48c4-9b6b-32836c7eb6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "6479e1b2-69ce-46a3-91b5-2322f31d7152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "23f5d446-0371-494b-95cb-944875a6d468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "5700c6e8-8c23-4696-8ed9-412a2c4fbc5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "500de0e2-35d9-4816-9764-272249edb500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "c8be98e6-5ad7-4a93-9a9e-79c38d9fa39e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "7b533998-2c27-4c5a-8fbc-0b099bb8ec04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 74
        },
        {
            "id": "371c7464-193a-447f-a2da-4fd4e76248cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "6bd8506c-032d-46b8-8ed4-c24451074689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "b20e5004-8d39-4900-8ed3-12e36ae3c197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "77019914-eda7-4f8b-85f0-39a7144ad336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "d5114852-dcd1-4beb-a6a3-665a389391cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "cecb9f25-36f3-4c10-8a16-62ce0783fb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "85a60bac-89ab-4f5e-9775-3152ccc23c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "f7aa9be6-5ae6-4c6a-9b60-e292e2d82566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 47
        },
        {
            "id": "665647fa-ebec-4102-b486-76b3d55bbbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "84a5c80e-bded-4f47-851f-5c9b6c16d5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 52
        },
        {
            "id": "528c128c-ded2-47ec-8527-ffc447e7305a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "7e8220ee-5832-4aa4-9c37-7a7c1644a609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "5a3e2cc6-4f06-4b6a-8729-2b94a99f51fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "f2ef62e7-5bb0-4b2e-9a8b-054bb0ea32ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "3605b814-8619-433f-bba1-3eb5af1d76d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 92
        },
        {
            "id": "3e040968-a11e-4f35-bb56-d99bee389c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 100
        },
        {
            "id": "3c1da00d-d32b-481a-9ccb-7c7e250ba88b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 103
        },
        {
            "id": "3f4ced39-9489-41fc-8222-de88b52febf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "3e7f43c2-910a-4a7e-85ba-7f32904bc93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "b86b6517-b26c-4ba1-8c50-9dae6e86ff30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "b99d378a-9098-4e76-86fc-f58daecf96bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "ed473b84-7c26-4135-8d1e-d8942ac31ceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "a9232dfa-fa1e-4a32-b77a-fc22ee8da709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "c714c100-9798-4185-b082-b6439a08b7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "cae635da-46bc-485a-8d0f-a17178d516e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 55
        },
        {
            "id": "4e2fd287-25cc-4d3c-9b3b-5ab53922eee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "c78da8a0-c84c-4e8a-af7e-66e79d05cedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "f7c86e73-5a4d-4433-ab86-c2db9d8ddaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "7e8dbfdf-db60-45c1-832f-8e69129aa469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "68adb003-7363-4dcb-b80a-653cc97fdfd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "9c010b91-4060-4f4e-8c41-ad32d63a6cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 92
        },
        {
            "id": "d48ceafb-933f-43e5-baa9-dc276605bad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "9e348c7a-770c-4feb-a2ec-ef6f3a2526a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "bc3fc75c-8bf0-4916-bdad-f026404a023f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 106
        },
        {
            "id": "0e45c6bb-0ba2-431f-9a7a-d6bd52a1b5b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "af066018-309d-46c4-b103-cfff500a0b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 894
        },
        {
            "id": "f43963a6-f884-40f3-9065-39c3ae4f3a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "222a1d98-c8aa-409f-a5dd-1009aa169f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 34
        },
        {
            "id": "74944132-7645-45b5-bf55-29f2b33a2789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 35
        },
        {
            "id": "f7e4e751-c479-4285-ab17-13a99274cfd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 39
        },
        {
            "id": "dd6ddbc6-4ddc-4ccf-99dc-1338f4201958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 40
        },
        {
            "id": "81a59bfb-a0e6-4bb7-9a83-1fd62d1b45d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "fb7d3ebf-39a6-4309-b567-e8fa2f2c1e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 45
        },
        {
            "id": "4849332b-070d-4c75-a288-e4a749520a86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 47
        },
        {
            "id": "b84773e0-be6e-43dd-bfa8-ec7daa9e6ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 49
        },
        {
            "id": "44c80a28-08a0-44f8-be26-922071994aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "b9c67b76-f12c-4422-969c-f02ed1724c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "3137ee48-61ec-404d-9ccf-c4080557527e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 52
        },
        {
            "id": "95a32452-4168-4698-aefb-ce2365fbb0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "7222cff6-c44f-489b-a129-79ec0b840ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "65f20239-47fb-45b4-917c-047642881277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 56
        },
        {
            "id": "a81d8e71-a18f-4be2-bf17-c9f3635a2148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 57
        },
        {
            "id": "674b7bc4-62b8-449a-93f0-f3f5782f7c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "abd85675-5175-4c19-8356-31b0f259f6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "dd4bedd6-d67f-4b49-9fbe-969a73a8b7fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "ae681163-4583-476d-aaeb-01625c21babd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "ba785404-6af1-45e0-8ab0-d23029e59499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 74
        },
        {
            "id": "51e770d7-3cf2-4490-9b39-ac05f061cc7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 83
        },
        {
            "id": "dcbe2140-9ae1-41de-ae8b-d96c43f223c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "933bc017-b248-4d76-b4e7-01325246c7aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "953ab3c5-6a5c-4591-bc3c-d851b9820775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "b2d9f484-d0ea-47ef-ab24-ee443b3b63f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "5e7ee849-1e2d-4b1a-a63f-98d810fbe3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "20bdf906-7294-4501-93a1-a330b95fa948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "3fbd4f84-68a6-473c-a1cb-0393ac278f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 92
        },
        {
            "id": "115621a9-26c5-469a-bd1a-e82d3e45ba81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "4ad77331-42b0-4c0b-9184-fc275d0e7733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 100
        },
        {
            "id": "1b0fe13a-f7ce-4de8-ae2c-946709cb9c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 103
        },
        {
            "id": "1a3ef48a-72c7-4a3f-8afc-0c99e30b91b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "78ee9a3a-2db5-4dab-b7d3-d8ca110ac8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "6208ca28-c705-469d-91cb-1d020842c0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 113
        },
        {
            "id": "3af8b2b8-ba19-4dc7-938f-4ac37c30be6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 115
        },
        {
            "id": "a975b822-0fda-4b31-86d6-f9bba3cca985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "4502cc51-4bdf-4dd1-a44a-ca4f711196c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "838336f1-8766-40c8-80a8-248818c7a5cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "a7c9f28a-e5d1-479d-b03a-907d5c3bcfeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 121
        },
        {
            "id": "52b90ace-03c8-4cb4-ab27-952af91985e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 122
        },
        {
            "id": "3970ecdc-0e3e-4db9-b2ee-a3451ad0aa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 173
        },
        {
            "id": "3a5769d0-c75a-4541-b43b-851771de06f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 894
        },
        {
            "id": "9ea94cbb-331c-4a2c-904d-a3941c8681cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8208
        },
        {
            "id": "67988149-985e-4a07-b1db-981607203647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 34
        },
        {
            "id": "4001052f-814d-4adb-9e05-cfa74dd442f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 35
        },
        {
            "id": "81a29586-2164-46cd-9b20-deebd8afc725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "ed034787-0688-4c95-b71e-d594f688a9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "614b3f18-2221-4418-ae26-938e81e233ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 41
        },
        {
            "id": "c0eed5e9-b61b-4234-a0f3-cfa770ab89aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "4822f085-5723-4571-b820-be0fb293d8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 49
        },
        {
            "id": "aa9c73d7-1e45-478d-8e5f-93ce9beeb43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 50
        },
        {
            "id": "2e872289-73fd-4088-b689-fe86050cf4bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "8faab7ca-4395-4153-8629-dd010fef66b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "c0bbdb73-cacd-474a-a8b5-22f2a2af0b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "335a9d83-1597-42a0-8abe-99f6fdb52429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "35e6c34c-16ef-4c14-81aa-bcf17b1a5724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "7a72b13a-6303-464e-8ed6-d910f7ea4351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "a54a55be-180d-4b97-8bd1-acd0bf05f546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "59859767-6860-4340-be4e-021c321b73c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "b87d1db1-676f-40c7-b8a7-e5c63a3270dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "25931f5a-fb53-417f-ad31-f73fd4f59f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "1526cc39-9816-42b4-a73a-484d18bcee54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "feb1cf05-cf0a-44d6-a416-2353c3171c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 88
        },
        {
            "id": "8d6ace7e-f8e7-42db-817b-d42a513ce242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "4a15ef57-c12f-43d4-8967-0413b1d2a964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 90
        },
        {
            "id": "da20f437-df66-445f-9fb9-6c33c5cfb453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 92
        },
        {
            "id": "43a5f33e-47b3-4224-a038-55bc6812c1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "8de3ac45-c601-4dca-ab64-1c9172ae3b2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "83dff8f6-80c6-4b91-9141-cea81d10ca69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "92052a35-7096-4533-8f23-386d3348fac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "31c99f7e-ebe2-4ae6-80b8-6481ff873da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "641b9412-5baf-46b9-96fd-169ad16779d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "390454b0-22a1-454b-a350-401ce39062ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 120
        },
        {
            "id": "31c5d3de-52e7-4956-b46b-a2450231d597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 894
        },
        {
            "id": "2c451e1a-123b-4190-b6a5-a82959bfe56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 33
        },
        {
            "id": "f36b8d44-5832-4fbc-9802-fd4f76be7296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 34
        },
        {
            "id": "ed7bc9c6-801b-4bdd-af10-62960756f70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 35
        },
        {
            "id": "91d1148d-430c-45e6-9bbf-d88cc3d48df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "32bc76db-6954-48ad-a14e-e2e3634c6efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 40
        },
        {
            "id": "06c68aec-b2b1-48f5-a415-a562320a4ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 41
        },
        {
            "id": "9689dea4-bc62-4799-aaad-06691edb9e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 47
        },
        {
            "id": "af2ff1eb-148e-4163-b56e-8089a86e54e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "d3004610-0d8d-45b4-835c-512b733a7b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 50
        },
        {
            "id": "4575efc8-8b91-4837-ab79-97b4596c4e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 51
        },
        {
            "id": "c8c253b3-d341-48aa-9403-0a9eab4badd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 52
        },
        {
            "id": "63a43a42-5771-4fe6-8177-96267121c92a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 53
        },
        {
            "id": "ca68374c-917c-4517-9752-9d0b3e954d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 55
        },
        {
            "id": "efba2fad-df6d-4e5c-8d29-13c6f26e2c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 56
        },
        {
            "id": "d37434ed-1814-47d5-8a00-e9b08c5b9430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 57
        },
        {
            "id": "94beb312-5336-4fbf-9679-d89e3bafa3cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 58
        },
        {
            "id": "59ac760c-7a1e-4050-a06c-7f0a45012709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 59
        },
        {
            "id": "342bc0cb-5f15-4463-be20-ee33c187d3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "28808ad9-2189-48fe-aca2-6d025baeabea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "398caa49-2553-46af-9705-c49a6cdd8e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "fdb8f989-f874-4407-bfc4-63b70a252853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 83
        },
        {
            "id": "8fad1ffa-e959-4df1-93a5-1165ca24e5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "45d9600b-5a2f-4571-8267-74e6e43996c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "0be6cffe-f4f6-4627-82ae-e9441b3e527a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "f4cb56e4-471f-44f8-8192-645d5dfe9e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "dcbcd130-bc5e-4691-aa3c-e5ae638b404f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "1966b284-2a37-40a1-9f01-6e4d558e6776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 90
        },
        {
            "id": "2b3c4d02-7b60-4516-adc9-9c05d768e80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 92
        },
        {
            "id": "ff2d60d3-8c59-4121-b9f8-9deee1792e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 93
        },
        {
            "id": "ef2b55bf-985b-497f-b424-8608879b5e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 100
        },
        {
            "id": "72358131-a6d6-4ef6-a7ee-25200c6512f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 103
        },
        {
            "id": "c10f718d-eab1-46b7-a04a-d13d8697f335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "2e0025f4-74a0-4356-82b9-f15e44f4963d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 113
        },
        {
            "id": "88ad7a69-f96b-4e0a-b7fd-81c6068a120a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 115
        },
        {
            "id": "8341fc2a-16df-455f-854a-5ef7781d1284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "aa01e6dc-8a65-447c-b3e7-efa8d44c7ecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "2ccaaa96-6ae9-4db1-9687-40ef5a725f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "89dc0800-dc15-48bb-8a24-e43e4446fd72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "e27853c9-bec7-4986-944a-9420cf6a4dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 122
        },
        {
            "id": "8c4e226c-f8fb-4c66-81ae-30cf8197e571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 894
        },
        {
            "id": "1195e209-d444-4c68-b2b6-8a51a86e1532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "1bd694cd-538d-47d7-9f50-de5a3f0401fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "833440b0-105b-4ebd-990c-86fd8bd7177a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "d3621074-cce4-4d40-b12e-174b60958ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 47
        },
        {
            "id": "a8c49cab-83f5-4acb-a77d-40231409e0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 50
        },
        {
            "id": "05bd6d3a-7106-4242-81d4-77e644a0be3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "68a1c8e3-d59c-425c-8dbf-9521407e4024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "96be690f-425a-4f05-baad-288111398e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 55
        },
        {
            "id": "450e96fa-1a9e-4f58-8f9e-a16ba215e7eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 56
        },
        {
            "id": "ad710790-93a0-47c7-b5a7-6fbefc1c6c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "b446db53-123b-4830-84b7-3d968c8a35ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "c3801ed9-fec6-4f5d-b137-e690259bd665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "71a208b6-27a6-434b-8a8d-405db9647c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "d745963b-30d3-4762-86f9-8f5dc3c10a19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 87
        },
        {
            "id": "407cd6ad-6f91-46c5-9358-2affe87d0621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "84bd0db0-faf8-4c64-be9a-db5aa4a7df66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "38154ef1-b7f5-4cc3-b6ef-5fae314bf32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 106
        },
        {
            "id": "4814694f-1da0-4ca6-a3b6-1027aedffbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "81414323-7e6d-4721-b2c6-2bcf6b43463a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "e374af98-b445-4a9f-95fb-c6614da9a481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "81e56e52-0953-490c-b0fe-3a6c3da7ffe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "d166954a-98cc-4e47-80b9-8b6b46b1517d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "345f09c6-8bab-430f-b7f2-a3da075f4dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "5d30bd86-c825-4a9d-a8eb-984cc80d2601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "6256f2f6-a174-4562-874a-696f1d030896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "4a2803ba-94fe-4179-863d-465609618d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 34
        },
        {
            "id": "eeef703a-72dc-414d-b5a4-1d25d92a3b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 35
        },
        {
            "id": "44f5d0d0-33e4-4264-85aa-ba1ccfc6f268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 39
        },
        {
            "id": "b6ec6e05-2f93-41ce-9dee-fd953a2bc756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "587f6696-3415-493b-bf33-5b973109eeb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "cb771cfd-6470-40d8-91eb-bb2a9a649ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "282e12a8-f318-4443-8c3e-ded99419888d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "7f0d917e-503f-44ef-8cf6-d42eaa795593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 49
        },
        {
            "id": "abc48153-191c-4493-8947-8143bcbae4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "7256da3e-4f82-4b8f-b1ec-2de58aebbd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "315132c5-61a3-477d-958b-5c2ae97c984b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "1ae042ef-b7bd-469b-b139-34e7d8d6da18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "19b4c8eb-e707-4628-b317-5e930d9e483b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 55
        },
        {
            "id": "ac0f5dd3-8622-48c0-b688-e3e0bf77e673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "81e5d2c8-b621-4605-8892-2bd4c69c3e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 57
        },
        {
            "id": "83dd3549-97d6-4149-9918-f38825bad66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "19651750-a2cb-4890-9c0d-6c2ca8856a9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "9db83ac0-597a-42ee-a4ba-9a9188cd1813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "347e59c9-bb41-4f75-bd29-94c3c59bc383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 64
        },
        {
            "id": "45bce21e-9497-476e-8fa2-69391d7101c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "4d4f0e13-fbdb-43cf-8f89-20ddc1491e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "c7d86d16-db6f-4589-aae4-c3d0499ebce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "706d86cc-00a3-4e4a-814b-ec8010fc5ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "9acf5574-41ee-4060-ac5f-b45e9d57eedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "2c0658c4-c7a7-4186-bff4-98c2e34decef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "8af0ba10-1857-4208-90c0-1c669f5a4a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "a2ef4acc-2fcb-4180-bed3-0b3f1920c528",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "74d19462-e548-43e0-be2c-e548a5074597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 92
        },
        {
            "id": "3c28fbdd-11d7-4da0-a243-0548cb572c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "87788249-b7ec-4f84-b775-db742a49f38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "9b21e518-8864-4502-af7a-0974347d9763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "0615fca1-fd3e-497b-b4bb-647aca8211f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "58d30fdf-eb74-4141-827b-3b8170c65e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "d647f239-5a2f-4348-8969-0b46f2bb104b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "da4593d6-9ec6-4896-89b9-07feeb89d90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "80dd3ca7-10df-415a-bae3-420f50e26a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "6a91af86-36f0-4e13-ad10-3978dc42b9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "8499e2d0-20d8-4199-8e42-8c6fc68fdad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "a073e78e-5fb0-4d55-904f-e590eabe6a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "7837f337-f3c6-47e4-9493-2ac6382d77d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "149bfaaf-3d8f-4240-9757-8f33b0e6f068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 173
        },
        {
            "id": "d9b4f73c-a23c-4708-97b5-1319e2b9238a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 894
        },
        {
            "id": "3e7aaf3c-0616-4b70-b43c-8e268b158fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8208
        },
        {
            "id": "cf60cdab-42e9-4efe-8d79-8fdd8d0cad7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 40
        },
        {
            "id": "bf1c34fb-222a-4923-90fa-923c088a8b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 41
        },
        {
            "id": "e2631516-136e-4570-aa78-4dbf3f7225e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "9089716a-539a-421e-9b37-7034b9942404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "e824130b-b82a-406a-85de-51fec57be5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "5dcaf9e7-c155-4099-b458-bd0c63200b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "82e33b94-c9a8-4d25-b536-a0c1a156deb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "b7ded26d-8ac3-48b3-9371-a222cd06f7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "6eeea633-9ce5-4b90-a680-92e0c693e325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 55
        },
        {
            "id": "b16c2946-9ce1-4186-861c-ad2bd89640d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "08093e01-b54d-4003-8b85-5ba9773b30a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "b4787ab5-a82b-4884-96da-6f0c2761a969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "ef30ee33-4512-47cd-9bc3-a45f6ead93dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "9e551ae8-12af-42e4-8842-5aad26059139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 84
        },
        {
            "id": "11ef9463-3785-4b08-ad9a-c204afb68901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 87
        },
        {
            "id": "d4d9506d-c186-4c6f-9efb-fc232af5f2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "88a01277-f7cc-45a3-95e0-4f69ca656297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 92
        },
        {
            "id": "76077a75-91ee-47ae-a269-8d16c6b6bdf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "902fb8f3-c5f4-4bb7-b475-7ac35d3ee294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "a42b2ab2-89d0-46cb-bc60-96224a8cee65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 106
        },
        {
            "id": "9aa17780-0243-4517-9601-5bb405afe6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "c03c8d45-7cee-4c2b-8f47-2951671f7a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "ed480bb0-f649-461d-80af-ef742c0191e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "9b197b14-91bc-41e7-bd05-96720214df85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "52a68a8d-d69d-463a-b1b8-07068402ff8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "102a26d5-9b54-4252-924a-3357e5b9e1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "e48c3fb4-0201-4520-a2f8-ea857aa15c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 55
        },
        {
            "id": "426c14e9-017d-4a7a-bc7d-227a9042b66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "7d76448e-2552-4525-a3db-cdc129f80971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "a282abc5-f8f6-437a-b6a1-a0fb64fb40b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 87
        },
        {
            "id": "7ee13c13-1c8f-48de-b2c4-0be1f5e0e1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 92
        },
        {
            "id": "20278feb-f014-4893-bf78-d3b3402bb226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "207bc7f5-27f0-4d74-a923-16789602d974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "f4401b2f-2fd6-4f64-acd6-6b1f3ba470e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "19aaf814-c79d-4be7-86c0-f9d979996ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "b9b062f3-090c-4726-b6eb-f8b83533f38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 35
        },
        {
            "id": "0b1187aa-add6-4b3c-92e1-990f13036302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 40
        },
        {
            "id": "be3d0343-d4c8-4830-bfe4-9f176974a167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 41
        },
        {
            "id": "3686fbce-659e-4ccd-a59a-4582ed158f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 47
        },
        {
            "id": "66def3a2-358a-40b9-802c-c9d4eec673b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "3aaf04c0-58ea-4e74-b0d9-2c4c33cc1ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 50
        },
        {
            "id": "bda4c8ba-efe8-47b8-ad4e-b2c75269c14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "e6ec7148-e1bc-4e8a-82c8-621041f54b07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "40cdf9f7-ede8-4aef-bfd2-ada2645b79ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 55
        },
        {
            "id": "050b8ebb-7944-4691-8b71-48627fba1527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "f59d675c-de0a-4be8-8bbc-86398da93743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "3275f9a7-1425-4dc0-9d07-39fca41735a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "c0442795-d75d-45c6-bb63-124f4d324a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "d160a19c-80c4-4914-aed7-4423742bdde0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "64bf88ee-c97f-4b60-9f64-b472bf52d9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "8a026819-1a6d-497b-a869-ffb6ef80455c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "da7d746c-f657-4861-8ec4-b0c4f46f72e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 90
        },
        {
            "id": "ef76f2b3-6203-47d1-a0f9-4b54f8f92b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 92
        },
        {
            "id": "caaef50b-d686-4584-8f14-e4e5a55ab9cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 93
        },
        {
            "id": "6613a63b-c580-45e5-bbca-cb86de1e4167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "88a87845-0bb0-40c8-bb72-fca9a4d022db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "15f2637d-0bcc-470e-8dde-ab91e0dae142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "7dbd556a-ab0b-4878-9674-1c5056acb44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "a343aecd-9b1f-4072-8e5a-dd69dd040378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 115
        },
        {
            "id": "fa54daaf-5b5e-4d18-85df-912da10627e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "dac1fe7b-2692-4cf7-ada0-f94c6325d1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "5e452bca-4321-4147-b991-bad3df69acb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 120
        },
        {
            "id": "25da85f4-da52-42a1-8cd9-9a693af1f3ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "e8567bb7-e219-4c13-ad1a-16ee7435dbac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 122
        },
        {
            "id": "066de2c3-7b46-43be-a7e2-68688a603dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 894
        },
        {
            "id": "c6118aaa-704b-4746-8fb5-3b1240caab3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "f6b1935e-928a-4103-9980-59a9ec5b3551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "6cbe05fd-3642-4343-97fc-14724c306fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 52
        },
        {
            "id": "980aba7f-a429-4f3f-8f62-7417c5f16d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 55
        },
        {
            "id": "5f396cc3-e0ef-4fe4-9e8d-65566126454c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "1ce49a49-86ca-41ef-9132-0005346dfab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 84
        },
        {
            "id": "7ae1a689-9b45-4176-85f3-01bd2576c1d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 87
        },
        {
            "id": "0193b30f-f011-4240-b411-b24f47fb51b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 92
        },
        {
            "id": "6b78f9e6-3d33-481d-b21c-b611f8ef012b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "d35fea98-76ad-43d8-b6bc-927e52432a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "5ee879cf-e05d-4240-8955-f9e0ac7e97a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "2bf727cf-c2be-4c20-bcc5-29ff08951df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "0de30346-0056-417f-84f6-9713932df494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 40
        },
        {
            "id": "6d59fb72-4e0e-4809-a7ef-5bf9d823b246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 41
        },
        {
            "id": "a16f7bc1-8775-4b71-8e62-abb265982141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "b3ae8c11-f511-49d3-a173-1cf46941e237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 47
        },
        {
            "id": "df6852d9-ebea-4658-8e8d-9bce9269f05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 49
        },
        {
            "id": "10a1ab8c-4517-46fa-a09c-46466ee28272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 50
        },
        {
            "id": "fcdbb7ef-ec15-404d-bea9-512ea018c7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 51
        },
        {
            "id": "6355ff1f-62a1-41f8-8724-9687be549927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "7af9e33e-fd12-4da4-9444-6898a2068303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 55
        },
        {
            "id": "34010ec7-e1f6-47b0-aa6c-f0bf13c8a727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "8e109a42-3567-463b-9f76-7f5ca816c5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 58
        },
        {
            "id": "9db5a9a7-8d98-42c4-aa15-8c03f8834b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 59
        },
        {
            "id": "bf6cac21-50d0-4150-841f-73bdf115d988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 63
        },
        {
            "id": "00e2819f-8eb0-47fe-bcf3-8722504ba537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 74
        },
        {
            "id": "45480ca4-4811-4999-b35e-3b485adb3e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 84
        },
        {
            "id": "629f244a-4325-4f2a-90bc-c253d3ab1699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 87
        },
        {
            "id": "0bf9a974-78c5-4ce9-99da-32f159f5e6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 90
        },
        {
            "id": "bbcb8b06-d068-4c9b-a06c-33c54b0ddd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 92
        },
        {
            "id": "9e903a9b-6ad2-487f-88fc-d25e64f9d149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 100
        },
        {
            "id": "b5b338e1-4e16-44f8-ac47-399d68c5e1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 103
        },
        {
            "id": "c8dbeeaa-f0e9-4831-b0f8-75a32a56053a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 106
        },
        {
            "id": "c54136ae-1dfd-4df7-abea-eab88f1bd219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 113
        },
        {
            "id": "3ff9def7-3390-47d1-9765-6e51b904ee72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "92fa6f60-70b6-4e04-b130-2f4c19897749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "1eefdb59-87c2-4672-a9a1-02bba10d3e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 120
        },
        {
            "id": "d3b46b0b-821a-44ff-b802-22256c1858c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "3e333d5a-d6f0-4346-a1fb-a5939a66d802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 122
        },
        {
            "id": "5592c07a-d9b8-421b-b3dd-34bde3c35a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "54074ba7-3cb5-42ac-a6fd-a43742d52ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 894
        },
        {
            "id": "8eb49b4c-a995-494c-b8a5-60416316594d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "e5deb10b-77c4-407c-bff7-0f25e6a61bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 34
        },
        {
            "id": "3f19df85-02d7-44ae-a675-5a3446c0103d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 35
        },
        {
            "id": "601ea653-4729-46d8-a418-b9b10621c22c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 39
        },
        {
            "id": "6f2aab87-27fd-4cce-a858-0d32cfd97b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 40
        },
        {
            "id": "da589629-95be-4667-853a-fae89d444af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 41
        },
        {
            "id": "c4602558-1a17-4c8e-9b33-908c51b7505c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 47
        },
        {
            "id": "11ff2dd8-a807-4cfa-b178-c13a11c08890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 49
        },
        {
            "id": "ba8c7e32-52ce-4d09-9efe-4773565c207e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 50
        },
        {
            "id": "857a1d58-5e87-4be0-ae5a-1ddf6588649b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 51
        },
        {
            "id": "8fdf0869-8309-4dcb-9b64-d28c98ca7ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 52
        },
        {
            "id": "339ee052-6fc5-4ff4-9a13-b6755960bf7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 55
        },
        {
            "id": "933b56f8-ffe1-4640-a009-5ee42ee4830d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 57
        },
        {
            "id": "5d366024-51e0-4c5c-8769-d1302cd41e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 63
        },
        {
            "id": "9945af5f-9bf6-47bf-9ad1-8006baabe628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 74
        },
        {
            "id": "2baef6c9-aedd-41cd-9aa0-13faddea52c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 83
        },
        {
            "id": "fcaf5eaf-3d0a-4c37-b72a-a2b3ca0ef0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 84
        },
        {
            "id": "c552e8fe-eee0-46a3-8f9c-54f436ed0d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "b2c04971-79bc-4abf-9d41-da01cf4247f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 88
        },
        {
            "id": "ea3dcfc0-a729-46d6-a3dc-f40251b8f6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "1fc48e77-805e-4726-85d7-e31dec92cf28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 90
        },
        {
            "id": "6a2c0a4e-1f34-4567-bd00-cdb697ce3047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 92
        },
        {
            "id": "6e519c06-5e36-4807-a934-fbfc9b2f7266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 93
        },
        {
            "id": "2159514f-bd38-4754-b88f-1a399e1c01ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 100
        },
        {
            "id": "fcbc68af-5244-423f-8f30-fb3051ccc7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 103
        },
        {
            "id": "b3d8e60a-0f1e-47d0-9439-747b518c1e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 106
        },
        {
            "id": "578bb286-4044-4185-a17e-a2ad16cde0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 119
        },
        {
            "id": "8d2c3e4e-1432-4483-bc51-99147079531d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 47
        },
        {
            "id": "c1f8afda-6a4f-4874-9e9c-548c7de0a800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 51
        },
        {
            "id": "d438ded8-295b-4992-af5f-d4b266cc06a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 52
        },
        {
            "id": "17968101-e746-4c50-8f07-a9a77c71a882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 55
        },
        {
            "id": "fb573825-c7b1-4fc2-b21b-b1ad171fd76f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 74
        },
        {
            "id": "1c948cc0-8e69-4e77-835a-fb870b8c84be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 84
        },
        {
            "id": "dab77b02-3c64-464d-83dc-23c24ee57a98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 87
        },
        {
            "id": "0fcc77f8-6b60-4c3e-afa5-d6a78443af54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 92
        },
        {
            "id": "c45eec25-35e4-46b2-8cd8-0110217c667c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 100
        },
        {
            "id": "e8ef2dd8-ed3c-4e08-a35a-717a72f5e3be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 103
        },
        {
            "id": "a2bb7feb-7a76-4c6b-b664-c65064ae263c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 106
        },
        {
            "id": "9c6722cb-e9db-4e59-b56d-db276953153b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 119
        },
        {
            "id": "671a903d-5e13-4d91-9ae4-45e94f318439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "eaeccdd2-1ad7-456f-af9b-5a194633ba1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 34
        },
        {
            "id": "00989677-ed29-4c3a-a4bb-3893a9a552c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 35
        },
        {
            "id": "37ee6607-3d91-46cc-91c0-f1c6dea082a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "1c4c8f0f-d781-474d-a930-aecc7b1160c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 40
        },
        {
            "id": "ebec3254-0ec7-499a-85cf-eecd87b73957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "0eea1c1e-1a4c-4ef4-b4fe-6ca20d765c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "e9919d1c-76b1-4adc-83ce-68865ac8330a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 47
        },
        {
            "id": "65897cef-fb61-4479-b59b-707b64b5c156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 49
        },
        {
            "id": "2b6f3f2d-0134-4864-84e2-527e0ddd7cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "475246a9-d8df-4c0f-8fc8-3b73791cc216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "1afacac5-9f2d-4293-80a9-707b6babdbe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 52
        },
        {
            "id": "99494f2c-96b7-4816-a6f6-f0bed8e6f5e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "9123bffe-c2f2-4a1a-b45c-cec5b737a5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 55
        },
        {
            "id": "06feb7b6-0a53-4225-9426-e6b0c30968fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 57
        },
        {
            "id": "5a5b321b-5b1d-4424-8be6-0c673db89aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "0210f027-113b-4f0f-991a-151d3f5fcc4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "0c1abb74-26fa-4df6-94b5-e473c5681e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "a78e6f32-6883-4125-8474-6a29db11ad45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 92
        },
        {
            "id": "4f7e55db-662f-49ff-b10b-e898940a0a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "1609eb75-bbee-41e7-a9fe-66a224c87cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "ca5fe0f1-34cf-45b6-bbe8-e43fdd494f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "d60f0bbd-19b9-4ba1-bc6d-8acd4f7a9ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "159cc172-6b0b-481f-9ff5-3e7ccb66641b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "30aca459-33c0-4d8b-9a6d-b2ac2768b3c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "6d401522-6453-46f6-90a4-912d5d16e0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "ae35a9d5-4ea5-48a5-9e19-64d302b06e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "d187163d-3457-4465-98db-a866b1b61d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "c6afc32a-a5d1-4e6c-92d2-18b47fd67c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "dbae02d5-68ab-4727-bde8-5ada18c5377a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 173
        },
        {
            "id": "ad7de02e-f618-4a7f-9336-3a3b80b77e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 894
        },
        {
            "id": "37fccc7c-3356-40da-8d35-10a66a3bd14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8208
        },
        {
            "id": "39df012c-02c3-49f7-8115-430c77b27705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "54381c6c-ea32-48ff-97be-4efe63a6842d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "6926e594-b0f0-4247-ae74-ba4d8565271a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 35
        },
        {
            "id": "e1bbf04e-fb7a-4846-965c-10c6b6ee737a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "9a26e0b1-29d3-4c54-9644-938e0c03793e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 40
        },
        {
            "id": "7c537e52-8493-4db6-8d1d-d2c39b533fc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "1644e94e-d125-42ba-8b8f-a8e139a8729c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "cb6a2666-af57-4cc9-9e67-463f781c9763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 48
        },
        {
            "id": "032ab5bc-54fd-4d3e-ba2a-4e066cf206ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "b8150f1c-bedc-4e76-91a6-1493bcb3fe5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "b18a0fcf-1b40-40f7-9304-202eb7c5b628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "7023c8e1-4e8e-47e8-841b-f98316d267cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 52
        },
        {
            "id": "e1fe6ac5-f368-4180-b2b2-64bae32241e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 53
        },
        {
            "id": "a937a9da-d164-4570-8033-fd8b2e1b2f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 54
        },
        {
            "id": "47391479-f0d8-4248-838e-7255646e48e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "0f158b3d-cb70-4af3-ad55-2e599a6ca061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 56
        },
        {
            "id": "926f14ff-a9f9-4d45-82f7-db34d036b46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 57
        },
        {
            "id": "d5010755-3446-43bb-85cd-e979abb359ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "f39ae220-1cd3-48df-ba62-0093def765b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "d499745e-605b-4be0-91f6-ed4d10610024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "64d75e99-baf4-471d-925a-88782e6799b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "cffa8699-2aca-475f-b848-1ca6756c221e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "e270b799-9454-4cce-80cf-68305d6a3a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 100
        },
        {
            "id": "6e7fe713-f5e3-4a7c-a86f-de43d8419105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 102
        },
        {
            "id": "2594e04e-b88d-4f6a-86f1-bbc5ff803ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 103
        },
        {
            "id": "d0a040f4-0c26-41ee-83e2-a14d467bf0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "2580cb6f-caf6-4a1c-b430-eaf2269bcd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "a4cdb28f-81f0-4e14-885b-172dd72e11fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "324081ed-42aa-4816-977f-efba7a7ffa74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "f2467503-4e79-4aa4-9dd9-c3d3b95f7b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "c4e6624d-785a-487f-b795-c29a166f2a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 113
        },
        {
            "id": "47de179e-c24b-439d-b64e-d4d24b2ef952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "007ccdec-03ba-4adc-9c3a-c32303e7de11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "cd0f7ece-0660-47b5-ac8b-33f465870fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "f995a651-8f78-450e-b5c5-ca20a03ca47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 41
        },
        {
            "id": "ce030685-ec84-4f9f-a8c1-4fe9f8415fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 45
        },
        {
            "id": "23f43657-76b5-4354-876c-e87ffc92f183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 51
        },
        {
            "id": "9dd51b46-dcfd-4099-afc7-5ac9de10ddf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "d8e2f1ec-b9c6-4bb0-ae51-8d224774be8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 55
        },
        {
            "id": "9d87664d-b489-4623-86a3-a9c7f17f89d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "4b894cef-3d04-4336-bff7-dc04f19f1e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 92
        },
        {
            "id": "7168ac70-626f-4dfa-aeb6-46ef9542198b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "64f314d6-004a-42e4-a328-253e0f2e3518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "9b728511-d043-4384-96d9-7e51b9f3383f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 173
        },
        {
            "id": "76272d17-7af5-49b0-a37e-8147de863cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 8208
        },
        {
            "id": "b176e7b8-c5bf-4512-bc58-b8ee8aa3f0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "c1cf0028-28ff-412d-9247-739bc88d36f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "ff170e29-f044-473e-8534-38a00c495fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "33232565-32ca-47f7-9a59-b268c5295c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "33320c1e-0efc-49d2-87a2-aa99d6665e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "c69b79e1-6334-438d-81a2-d4a010e1da92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "52cd42d5-ae4b-4f70-af03-c2cffd83c14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "35a44560-6732-44d6-b2d8-d5f9061b5422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "c38a84b9-9834-4489-9c5c-521c6c9a8ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "332b5b20-222a-4327-8a7c-1626cf84a58e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 35
        },
        {
            "id": "3a7feae5-e338-4c75-a3b7-2b830b6b5981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 40
        },
        {
            "id": "d6133dc0-d9ee-46ce-a2b0-93c49d7c6741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "d5617c79-1d5a-42ab-abb6-5159a8b5dbd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "1df3a4d9-6289-47dc-9a31-f4651ee05c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 47
        },
        {
            "id": "70524a60-af72-4029-b28a-f238e90d7c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 49
        },
        {
            "id": "fb9f3988-f773-430a-bf83-fce69a8f61ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 50
        },
        {
            "id": "b0e4f132-28c5-4183-a6a1-e3ddbef9b66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 51
        },
        {
            "id": "d8460a1a-64a4-46ad-9354-19784f8a74d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "ba35374f-83be-43f4-99b0-0032f94c838d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 53
        },
        {
            "id": "05b386e1-f884-4fb3-986e-056326dd6cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "2ad51eb7-a60e-4442-ae49-6c22d1beffec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 57
        },
        {
            "id": "43d35c54-e112-4a3c-a347-5a0fd6c6c498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 58
        },
        {
            "id": "e5138abd-114e-4312-98d7-97f1964ed428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 59
        },
        {
            "id": "c9ddcae5-c963-4a9d-8447-9119e512c296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "334be4be-a1e9-4ef0-b46a-34bd940918a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 92
        },
        {
            "id": "4a27e43d-845d-41c6-a238-2883d950a1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "e1d4e64b-77c3-4128-ba91-19017992ccc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 100
        },
        {
            "id": "98b7ad47-06ca-49c3-8616-b1e751e25071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "ecb5edf3-04e5-48a9-9aef-b1fafedffc89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "b3ea82d4-371b-453b-8022-2ac08b48a6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 113
        },
        {
            "id": "e6d70b66-79da-44b0-8a65-faf3e7f370c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 115
        },
        {
            "id": "72a5aaec-0816-450a-8a6a-cfcc8c30740a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "c7468e3a-7ab5-4f05-baba-db9bf129a388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "efea3782-c048-406b-ac2a-6f827eaaa3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "9ff033f0-13d3-4769-9fb1-2478f1d1b441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "76a4b7a2-a320-425d-ac4b-0c26467c3344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "f244f296-c234-477b-ac9f-e173d2cd358c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 173
        },
        {
            "id": "9887af17-7635-4b96-bdec-7001a9dbaf9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 894
        },
        {
            "id": "0130d2de-ee5b-477a-9a47-eac6329d7437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8208
        },
        {
            "id": "f518210c-09a0-43b1-a14a-0507224e0c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 34
        },
        {
            "id": "21246d8a-12ea-440d-9341-d2272bb05b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 39
        },
        {
            "id": "4b6904a8-fec1-48e2-8a86-ebcd4191adff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 40
        },
        {
            "id": "c1788a02-df5e-4d3e-b497-98fea8101319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 41
        },
        {
            "id": "56a76cf7-139e-49f1-8a3c-18b0653505f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "e0ba1280-0109-4b61-ab97-552f47f791e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "c56ea365-0bb1-4ea6-88b0-0f795ff38e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "6e70b365-3f64-48f5-beea-1016065f975d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 49
        },
        {
            "id": "ff6e3571-9052-4905-b859-e85e98e38fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 51
        },
        {
            "id": "d33049cb-37fe-431d-8a94-bd39dfd2a3e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "4a03f1e3-923f-4389-8009-17823e2c4c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 55
        },
        {
            "id": "95cefcdc-3ba0-49c2-a828-d4e19ccaab75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 59
        },
        {
            "id": "328c08b2-853b-429c-aa19-5d41a2b6eabd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 92
        },
        {
            "id": "827b0068-98d3-48b2-9d3d-5490f8f54ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 93
        },
        {
            "id": "25703b7f-950f-41a1-902c-be7163f10514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "125504bd-27ac-4933-a547-012bf68b659a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "3ce594f0-6346-4e85-91df-6dc6f1fbcfb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 106
        },
        {
            "id": "d5e1852d-7b84-4eb5-bd16-5ad66b164fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "39dd9229-14d8-4644-829a-d302226e4f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "9e37a6e0-af35-4ae8-9355-522aa2ce381d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 894
        },
        {
            "id": "fd217784-190b-47f4-b765-669d93caa1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 47
        },
        {
            "id": "d9e56379-ffe0-4114-880e-ed910cd59396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 51
        },
        {
            "id": "992f563b-9a3a-49d2-ba8b-c26d718d8feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "3761e070-8401-4b12-9614-90b207d914bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "d6a76755-f4f9-44a7-8bc3-429e601216a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 92
        },
        {
            "id": "2ce57f0f-2a41-42a5-81fa-39d280b51511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 100
        },
        {
            "id": "d1d991bb-27d9-40b5-a426-199d88640acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "abf61113-ced1-47e6-96f3-d4ab57264333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 113
        },
        {
            "id": "f1ba97cf-080d-4048-9a5a-501d13589d88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "830948d0-6440-480a-8971-d217bf224107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "54873323-47bd-463b-beec-78b4907881c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "f620a745-307e-4b3d-960a-190fe202284a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 35
        },
        {
            "id": "ef72ebf6-aff3-492f-a336-6445b388df4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "0fc96ba7-8c95-429c-b4e6-b434102f3985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "ed229bae-45f9-4fc2-9026-5650de9f9b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "57f1a0f7-04bb-4ffd-ad30-b9884f8d9556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "586f0bf7-37ba-44e5-aefe-36b857177bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "98206fed-3c8b-4448-a97b-5759096938f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "df7e28dc-f75a-4364-bd59-e5e9526f5002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "d81f9b1e-3b0b-4a7f-95bb-20e25686bebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "49114c27-546e-41d8-a064-be483cb73f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "0342f17b-1f8a-404a-898c-8dcf9d255508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "b61115d5-e07e-4fe6-aa5d-6ad2f71a5bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "17a14a11-572c-4bfc-8128-8ee3b8542e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "d5cdc5e6-2ab5-4766-947e-dd9196e43b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "26b02313-853e-441b-9e1a-3c037ff39090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "1ee09bdd-7626-4269-abd0-c95a0365157a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "3adbb68c-6a42-44e6-8559-af7f1db45796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 64
        },
        {
            "id": "73440c9f-3428-4180-96a6-96d5e3ccdb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "f34c6099-3fc3-43aa-ae5b-3134e67cd83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 93
        },
        {
            "id": "1773a301-5677-4872-8b32-f13463a2c5bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "f6e1db1d-f450-4b64-80dd-a884be42b940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "393b74d6-ccf6-40e8-81be-3c20d74569a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "4500e34d-3df0-4baa-a706-f2ba4bfefd52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "f89b62f4-51f4-4d71-be93-fb8c1e6afe97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 104
        },
        {
            "id": "10c924ae-2efa-4300-9023-4c841ce63341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "323c990e-d4bb-47d7-9055-2f57ae111651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "3b920e9b-a420-4cbe-a49b-734ff5ac3c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 107
        },
        {
            "id": "07fc235d-40b7-44b4-b2d8-458a575753b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 108
        },
        {
            "id": "4bc7f704-9793-4a34-bebf-a14d73a16280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "de5e1a39-7a62-4121-9c49-0b246077ddfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "da04f0d8-4e4d-4cf4-9818-0c0dcb4da53e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "32550187-137c-4133-9b53-2b2dc0326c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 120
        },
        {
            "id": "568e3c76-ed33-49ad-8b85-3c4cfd9fcf27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 40
        },
        {
            "id": "01d369b2-1b8d-4565-bbab-9d7c13885890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 41
        },
        {
            "id": "79a9a86b-c96e-4685-b947-f0736537729c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 47
        },
        {
            "id": "17f6db01-3e4d-49c4-b91e-4c1180c3481d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 49
        },
        {
            "id": "9c82c696-2f68-4d61-9834-f7de48e216fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 51
        },
        {
            "id": "a5c5a1fc-29ae-4efa-b8f9-358db2c3e14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 52
        },
        {
            "id": "8ed1c59f-a3b8-44dc-b4a8-a6b1abde962b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "6e2252ac-f38e-41c5-b947-93be3e621177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 92
        },
        {
            "id": "0313f4f7-a4a5-4453-9b6d-b91a0b8a7e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 100
        },
        {
            "id": "94329796-481d-4440-9a2c-7c7e07948772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 103
        },
        {
            "id": "8c8a95a4-93a3-4367-a421-18fdcebcbf3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "eb1e3e29-de18-4def-8f4d-6a60d2a3f001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 119
        },
        {
            "id": "25265db0-a8c1-4da3-a36c-1655d80b6411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 34
        },
        {
            "id": "2ff2ca34-dbb9-4701-a17e-23c942206ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "c128362b-a590-4fbb-b829-bfbfa91a80ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 40
        },
        {
            "id": "b791f8db-c613-4355-84c7-ab3d485f8c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 41
        },
        {
            "id": "6910311d-0c27-403d-8cca-8a862b2b102d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 47
        },
        {
            "id": "476d0c65-5534-444b-a100-f28adc6e11ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 49
        },
        {
            "id": "3bfcb1cb-c157-4410-8996-60905c19a5cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 51
        },
        {
            "id": "23090ef9-919b-4a1b-8a06-e73321a0eb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 52
        },
        {
            "id": "952a0298-2886-4674-919d-b6c6b768619f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "c41d048c-9e02-437c-9f39-5684851a7a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 58
        },
        {
            "id": "b8a4a047-c587-4482-83c8-5ae07e4911d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 59
        },
        {
            "id": "17338212-f944-43d6-8360-69adea076a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "79bdcd8b-1777-47e3-b5a8-ddf83f0cf446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 92
        },
        {
            "id": "92094ba1-1d3d-4fe1-bf65-24356fe071cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 100
        },
        {
            "id": "c90d8811-60c8-4a75-8923-dd6d898c7c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 103
        },
        {
            "id": "a5e37c65-130a-45a2-aa16-04d5bcce5581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "6e4afac7-a8d5-4721-8157-0c4648486e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 113
        },
        {
            "id": "b51d428d-4680-4520-89d0-a294a68abd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 119
        },
        {
            "id": "1787dfba-6252-4e5f-93f3-6f39c7557372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 894
        },
        {
            "id": "aa8a0b67-adce-43bc-9fee-37a1a005945b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "4b03f242-534a-412c-b941-d1b2ceff5ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 34
        },
        {
            "id": "b799d35d-24f1-476c-a552-27f24a71a1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 35
        },
        {
            "id": "9dd89fc9-b519-447a-9cd4-b8f6d1706dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "bc6b1ee3-79ee-4ace-b284-c16477ca968c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 40
        },
        {
            "id": "25d3a770-d355-4356-89e2-cd83be001710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 41
        },
        {
            "id": "df440dbc-e4c2-4413-8c5c-6eada50edb06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 47
        },
        {
            "id": "ccdf230a-f176-4ecd-888b-30fc6260aeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "05eb9ddb-0f8b-4869-9292-0f48cb0f7446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 50
        },
        {
            "id": "48308448-aee0-4fd2-8d79-fc2f3c90abb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 51
        },
        {
            "id": "fa6bbe91-cd8c-4a06-905f-c33939a30cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "2e0f8721-b06f-4527-96c3-9ac066d1a7fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "60e7d5e6-8ed1-4fd2-9349-04ae2f0a75bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "73c60ef6-450d-4430-8dd9-9ce44a6afe88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "0ebd9b38-227d-478b-ac57-4422921c8a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "9cdaed36-d33c-4323-bf7a-20394fe259ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "75c216eb-8ca3-4f1e-9458-6e61213571b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 64
        },
        {
            "id": "62be6c1a-6577-4056-9f30-4227e8f7ef97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "f23e45e5-6225-4a64-8b5a-5bd3ab0cac1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "8a97f895-d27f-434f-8da5-d5367e3da8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "811c8774-af1f-44cf-ab9a-d2425a1b9a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "1dc2c1a4-d66a-4ccd-86e8-abda5e51d651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "300d920c-ed7c-46f0-8419-425813704c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "91ce4ef4-349a-437b-a76d-b7f00de4eef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "1a392e59-5dfa-40c8-9097-edd774d5eb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 33
        },
        {
            "id": "fdc31d0a-fa77-4bdd-bf10-8a764dee87a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 34
        },
        {
            "id": "a2aab3ab-e02f-4d44-a044-0c615fdbd0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 35
        },
        {
            "id": "3a0d00ee-a9b3-486c-bfda-8193fc3401eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 39
        },
        {
            "id": "88c0f6af-6547-49b0-9c94-2d1d8f90f208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 40
        },
        {
            "id": "ed57031e-f4e1-44d9-8885-86d3fc7fe104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 41
        },
        {
            "id": "cb7ddbcc-d4f4-458b-a4b6-89c13b0331e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "7ba81799-f16f-43de-bbb2-d5817149e6af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 47
        },
        {
            "id": "952b9e2d-1034-4fd3-8127-3b37545705d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 49
        },
        {
            "id": "8696437d-970e-49a9-b228-b49ba95a5380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 50
        },
        {
            "id": "dadcba50-6c7e-400f-8b18-90819d7d3578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 51
        },
        {
            "id": "78b992ae-800e-4aae-b7de-66ccac28aca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 52
        },
        {
            "id": "8c45a72b-2756-4d56-9391-f2da53e01e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "cdfa20ba-b570-4e37-aaca-4af03c1e0fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 55
        },
        {
            "id": "8803c1ec-76b5-48d3-a75f-e44c23a2b15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 56
        },
        {
            "id": "c83cd102-be9a-40fa-a4cf-c02af6d63510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 57
        },
        {
            "id": "efd9246f-2a08-4d44-8bd3-5f83337f1b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "23e2ddcd-6db8-40ef-a475-0ff4d50635db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "15946380-796f-4544-934d-9cbcfbbdd8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "a78eebfa-5499-45bf-a931-99be5352f65c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "7e1fdd57-c2d1-44f2-a8ff-de9be4a879d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 92
        },
        {
            "id": "06f2bbc2-d5ed-418b-8bd1-99d04a75a91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 93
        },
        {
            "id": "3650fc1b-67b0-4c42-aec4-dd4ff5efa237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 100
        },
        {
            "id": "f5a5914b-d126-4bc9-9f9d-bbfa9243e139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 103
        },
        {
            "id": "1d580e1d-4cdf-4071-a01c-c4c0d94f70f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "1299e154-01ad-4f76-bf29-138ddc2be9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "3d627957-3ad3-47cb-b50f-3c807be6624a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 113
        },
        {
            "id": "daf72220-a668-4580-b491-0d8fe14f879e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "5a40eeb8-4266-4154-a1ca-339e6273382b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "d5e66bf7-43da-41cf-8f81-44298e3d7b4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "74586e42-8f06-4f80-8d44-dc8a8cc8837d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 120
        },
        {
            "id": "a9074ee7-2745-41c8-b0cd-97265ec8d7c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "b564ad96-bae1-4b6c-b07b-5f9ca8dbadb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 122
        },
        {
            "id": "0896eac7-f022-4450-8b68-0434240f0c9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "4adc1bfb-cbc0-4708-82b1-bec5bf40b0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 894
        },
        {
            "id": "76ffbd5d-d9d6-4a19-9750-313e2448b440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8208
        },
        {
            "id": "a83c681b-07a9-40ba-a97a-1dcda35ff911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 33
        },
        {
            "id": "2d7f34e3-a310-4782-81f1-5fa825332510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 34
        },
        {
            "id": "4696cbda-cc10-4eb4-a27e-0d8ebbb1ee09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 35
        },
        {
            "id": "cd3f7f3e-fa97-40b4-8fed-f4a80388f02c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 109,
            "second": 39
        },
        {
            "id": "e9d6290c-72ae-49c3-814c-7b73dac441a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "11e6a688-4139-4dd7-92ce-5ddbcf1cfc70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "a2f51e30-ed3f-4911-bedd-d28d4e491853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "625c4668-3d15-4bac-9280-587dfacda269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "0beb1633-f090-4459-b724-d569dd38d351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "f5e7d80e-af3a-48c8-bd3b-cfabd359f30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 50
        },
        {
            "id": "d319dd9c-29b2-464e-8e81-cc90c47a2a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "94120f51-3d9e-4a61-98ba-012791c65499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "11c2666a-b5fc-477c-8ec2-9a95426fc250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "28d2b555-e04d-4fdd-904c-29f8f26f0e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "4738be06-e7fd-4353-aae1-7530ae433193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 55
        },
        {
            "id": "cad29b71-c808-4517-a974-50a48b8968ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "85c91573-e574-4a20-bafc-b707ef625b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "c41c623a-906c-4ea2-8b84-252f5967ed76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "aafbec64-e743-46c9-aa92-2d6a0c1404eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 64
        },
        {
            "id": "57a1391b-d18a-49fd-a8a2-2432d769e5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 92
        },
        {
            "id": "724fc1be-89f4-40c0-9d05-771c9e5d9c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 93
        },
        {
            "id": "fa661f59-eae8-44c5-a720-18ec35d7227e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 98
        },
        {
            "id": "c50bf326-ae1e-4da6-a67b-80b5661a2fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "4b21ce90-1ee0-4420-a852-ba6695b910ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "34f82168-5fae-47aa-87f9-31cd4571f6b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "c9f19362-e91f-4d2f-8399-a80589cac38d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "82c20e53-3863-4c08-bf64-2cdf91961335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 104
        },
        {
            "id": "ef700883-6f1f-4d89-b131-b7664eee2304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 105
        },
        {
            "id": "66a13d98-8ada-4a69-b8a4-4ed5de0e7156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "44357f71-fc0c-40ee-838d-df7cac58cfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 107
        },
        {
            "id": "fca50a56-3a3d-4275-9937-9a73c1d9347b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 108
        },
        {
            "id": "9063ba49-059c-4558-a458-b5f289302fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 109
        },
        {
            "id": "349698a7-b290-4d9a-9c35-dbc747310e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "0e0eaf10-c836-4641-99e4-8c6e9b6bf70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 112
        },
        {
            "id": "42c83ba2-3839-4988-9022-860fc979c6e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "b7e52341-e5b9-4af8-aab9-6b6e1b1f3681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "786ee875-1dae-41e3-a2b8-6d54b35a5877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "d14e192a-b1c5-4570-9f5c-7278f8dbf61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 119
        },
        {
            "id": "e96129fa-5f09-468f-83dc-5c8ca88f1ba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "02f4414f-4c12-44be-92ed-e2ab37c329fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "44e94d45-6899-41a5-b8a3-f0efaf3c4139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 35
        },
        {
            "id": "4a3125ac-6b89-429d-a8ea-32d404ddeda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "2c76c90f-778b-4d89-ba32-31ca1220f792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "2fe8da4e-3d2a-418e-96ac-a2a5116e82aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "40951250-6cba-41eb-a7c1-089517a88e46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "387ba0d5-2397-4353-a02a-bf9db5de11b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "aecf65cc-1b05-428d-81eb-5a37f57947fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "10e55908-c0b2-4efb-a75f-565d95b4ea29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "bef2e454-1abc-477f-8dc1-c1cb49fc60a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "5aa9d0d1-8705-4b56-826b-29981fb6fb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "107ac591-9615-4b8d-b5d6-fe09bfb8e183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "017bd086-05ca-4d6e-92f2-f343ccee4454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "43bab6ea-7455-4699-a627-c0b19e5ba623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "dbb529f1-a311-4cc0-85b8-ba49d5c9c28e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "d1659c78-3c15-46e0-acd9-f1b25904751a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "c89cb1a6-cf28-40e9-95d5-6e3d15fe3cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "7253683c-9aae-4f97-b7f1-2c36890f5fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "999569de-783e-4652-8816-0511dcaff500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "ee7ba39d-ee75-470b-97df-2b1e4efddc48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "b037b92a-fa3d-4419-bcbe-333c878b9d04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "568e8321-ce3d-4c5d-b816-fb7353f9c8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "8a3e0e70-cdba-4625-81d2-de74c7bb167c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 120
        },
        {
            "id": "3abd37f6-cd41-41ad-a7b1-6b97bffe764e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "d1324c67-6482-44ed-9807-2db6d24f6772",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 51
        },
        {
            "id": "05214448-e344-4450-a416-640ac4173043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 52
        },
        {
            "id": "b5afe751-baea-4740-b5c9-320d1ed34842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "3b8904d6-cdb1-4ef9-9eec-b5bbf6d055e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 92
        },
        {
            "id": "5c81f2f5-4830-43b2-a338-f311d3721171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "bbb69f29-be25-4c68-a1dd-665fa34277be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "7ab374db-4f8a-4f24-9736-21f7f0255fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "2409f4a6-683e-4fcc-917a-bef12cdcd024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 35
        },
        {
            "id": "c0cbf734-f24e-4544-b86f-e6bf9b205f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "014c877c-1cb5-4734-b2dd-f52871152a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 40
        },
        {
            "id": "1fe6235d-eead-4c40-9822-caa614a9d3cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "f22f548f-e97a-4a93-ba89-8a2881c35eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "3b044efb-d984-4ffa-a179-ac3b211d1102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 47
        },
        {
            "id": "60490728-5c0e-4d61-bff4-8ea8511ff0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "e31e74ff-815a-428a-8e8b-bc505d9e98d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "38aa982e-f788-4647-9a70-8aac5a7ef41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "51398fe3-11da-49c2-abd3-321eca57e70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 52
        },
        {
            "id": "7e6ef744-a912-43b8-a61a-043199e75c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 53
        },
        {
            "id": "e22ffa12-6334-4d03-b1d5-9cdd6ec13469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "8d992782-5ea9-4f3f-a618-ce61ab8f88d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 57
        },
        {
            "id": "26b32dcb-2403-493d-b045-2f1c27958a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "ce747738-e39e-4404-b17d-fc535eadef23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "0a4764eb-40ea-4c0e-a1cf-d3acdf39e375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "4df254df-e51f-4f4d-9225-27715269c597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "f1d9c813-74a0-4e54-a339-006178b826db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 100
        },
        {
            "id": "479706c9-008b-4ffa-bd7e-298ea1cb5a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "e6f73e2d-c82f-47dc-bad4-c309f0d2ec6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 103
        },
        {
            "id": "1c051fb1-6105-46ea-90a0-f0d29395a00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "06a13b29-a69f-44f4-8760-05a8d9321dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "cdc6fded-79f3-49f4-a609-a90412d00bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 113
        },
        {
            "id": "f1eec8cd-97f3-49b5-9f98-e936b9dcc3d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 115
        },
        {
            "id": "e8a0de50-7f4c-439b-a7fe-468ceea1b2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "2491f430-78ac-443f-b2df-e1943107cfb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "30e66893-581e-4cfa-88aa-e3cb441906c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "21252afd-8781-4ff9-9aa8-31592ff895a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 894
        },
        {
            "id": "4cbce7aa-f850-4282-aee3-a7dd76259d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 41
        },
        {
            "id": "418e481d-45d6-4977-9097-896f361ced50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 51
        },
        {
            "id": "1ae68930-7583-4baa-99c2-9a9703095eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 52
        },
        {
            "id": "f1465671-f4aa-4677-b32e-00a5c73dff0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "5978ba95-bdbd-4422-a6e5-6ed1fa6dae5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 92
        },
        {
            "id": "281d7c6e-e983-4742-874e-1fbc014dfb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 106
        },
        {
            "id": "24a14ffc-e3e0-4968-a5ea-ad66aac1b4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 113
        },
        {
            "id": "0b4974ac-a5dd-4531-b5eb-9b9ca5946b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "673b43f7-2cf1-4844-81f0-3055fdbb4a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 33
        },
        {
            "id": "0fb19baa-812d-4cfc-a5e8-9db8073690ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 35
        },
        {
            "id": "affe1828-4311-4724-b30c-2fc7b48cd645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 40
        },
        {
            "id": "43108ebe-fcd7-46e4-990a-13900db1d749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 41
        },
        {
            "id": "3e523d56-6789-4c02-bbe8-e46f1737e08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "13a10f24-d984-4bd8-b803-fc305f338766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "fb0381af-6788-4123-bd70-3cdb0b342590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "4542ec5a-bde0-4711-b630-211268b10a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 49
        },
        {
            "id": "c85b573b-af1f-4614-b8a3-bcc9619f8a78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "ca954baf-8870-4f1c-9f4e-93c179d12f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "0db66e8a-5714-48c7-910a-968788bb2ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "573d7bc5-5ad3-495c-9d7d-18738e2cf58f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "c8f01475-b7ac-42a6-bc27-40bd8c2d751d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "ac84229a-3ccc-451e-914f-573db56733fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "7e53cc92-335b-4d1c-8ecc-e6c513d5d1cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "b8b435ac-27c0-49ba-b08c-47cf9451c71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "a59d0ff1-d10c-4ce5-a638-d30bd20878ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "c6695d40-9e40-43d8-aa6d-314e825862bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 92
        },
        {
            "id": "0fb2cb59-eefc-47c0-9792-7ff6e74d8898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "162b9287-26ba-4d9e-ba35-6306ff3e4f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "86400fac-dc6a-4256-883a-fdd03f7a8ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "5f813c22-3de3-4b97-9323-45c2d819cf5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "45af1c72-36ab-4370-842a-9ceb2cce20c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "10f5d7f2-8d19-4d1b-a933-ecc665709dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "8c880f3e-73bd-40e0-8033-d9779109642a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 120
        },
        {
            "id": "28ab12b8-ddd6-487a-84e4-2358b296fd24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 121
        },
        {
            "id": "b170d4a0-93e5-452b-b845-c59b4081de4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 122
        },
        {
            "id": "ccc7c179-a96d-4423-afe6-d53a33329b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 894
        },
        {
            "id": "e8baa85f-ad36-4b92-9799-e59b0943442d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 33
        },
        {
            "id": "887255ef-532b-48d9-a905-f6145d8f743a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 34
        },
        {
            "id": "f795cb00-fbae-4cd7-966d-c195d52a8ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 35
        },
        {
            "id": "f94b64f2-ad4f-4270-bcb6-9eaea8a2f76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 39
        },
        {
            "id": "a830b4fa-f1b9-4d61-9187-d877ce90cf83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 40
        },
        {
            "id": "0ea3d0da-df02-489b-8902-03accfe0c429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 41
        },
        {
            "id": "aaf71f36-707d-4890-8916-abf928d6605a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 47
        },
        {
            "id": "ecfecc35-e3f0-4223-a7a7-f80ff585dfbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 49
        },
        {
            "id": "1d73ee6a-5b42-451d-a7ef-91059f5858ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 50
        },
        {
            "id": "c04a39e1-b019-4c8f-9099-990ceca69ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 51
        },
        {
            "id": "7907a375-1874-488f-ab88-691a752e4048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 52
        },
        {
            "id": "9e2f8ed8-b797-434a-9cab-eb3a6ffab95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 53
        },
        {
            "id": "62d910ab-457d-4655-bb52-4d338395910a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "4b310942-eab5-49f5-8fb3-93d9c73fc5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 56
        },
        {
            "id": "a8af6974-a45a-414d-92b7-06d3426390a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 57
        },
        {
            "id": "820e49cc-6dd8-4d1b-9fd9-8c026cf6f51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 58
        },
        {
            "id": "2cf93f58-979d-4cf2-8123-8a5f55e0092c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 59
        },
        {
            "id": "e5ca24b7-8db2-4c0d-b7a4-0fb37636b6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "0ba706b0-e634-49a6-8818-204673b239fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 92
        },
        {
            "id": "e99ec975-fae7-4dee-be72-c725b9f0a6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 93
        },
        {
            "id": "98daa36c-2956-497e-9746-691a380cb2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 100
        },
        {
            "id": "8438d0b2-79bb-4371-81d9-7999e7bc3963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 103
        },
        {
            "id": "5aa5f093-767a-41b7-9154-f1aff4bde485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "634819a1-e415-49d6-b56e-8ef8d902dbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 113
        },
        {
            "id": "1abdadfd-a5c3-44fe-be81-18ac34836679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 115
        },
        {
            "id": "0a641802-b48d-4d96-83f7-b6975ade78c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "8caa727a-dc82-449f-9677-1bcb8a59b27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "55843852-17ce-45fe-b8c3-69ea2c27eded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "c741cc5d-e909-4f74-af96-5c577516e45d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 121
        },
        {
            "id": "6e6c850e-f6c2-4010-ac06-a8b43755c7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 122
        },
        {
            "id": "54fd6a8f-8056-4de0-af8e-a539035f3430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 894
        },
        {
            "id": "dad75373-0a96-4341-920a-9030c6481a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 34
        },
        {
            "id": "fc92a5eb-5fb4-4edb-9e6b-ee4dafe5be95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 35
        },
        {
            "id": "e6ffcef6-16d5-4d09-90dd-0a22dc5aa34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 39
        },
        {
            "id": "40c5eb3c-024b-44a7-b028-912cb7f9faad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 40
        },
        {
            "id": "734dee0a-5279-4ee3-a9de-22cdd2fcb808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 41
        },
        {
            "id": "7ce8dd17-04c9-43ae-b221-6ce493f4c3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "e8385692-24c8-405a-bc22-b2ab85b0b455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "97ce6131-5a0c-4e31-89d6-ed5cdb2220a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 49
        },
        {
            "id": "22bded30-8dca-4e48-939c-1b35d6c0e286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 50
        },
        {
            "id": "80ac09d6-4a72-4f67-9372-983ad6660423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "6e61149d-9b2f-43fb-908c-4fe6c5fca191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 52
        },
        {
            "id": "2297f493-11b1-4ccd-8e44-ab16ff247517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "e4fa8210-3a2a-4cdc-83fa-258eb6e4b616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 57
        },
        {
            "id": "ed1c3996-9762-4064-8831-0f15ec9b305d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "701ce02b-14cf-468a-b045-bfaf4f7a1ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 92
        },
        {
            "id": "506dbc02-7476-4820-9d04-b3cd82a717e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 93
        },
        {
            "id": "a9d679ea-a8a1-4542-9c48-83215d52f759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "fd3a17f2-d33c-4886-b9b4-910af11fd439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 103
        },
        {
            "id": "97620198-49fe-4dcf-857a-11b8eda2ce27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "216092e9-cfda-4300-a93b-b88b31e3b602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "34586dc7-052f-41b5-a067-6bcb372cb236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "3250f5dc-64e3-44bd-85f2-65cdeb4c688a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "9cf31e27-1931-4da9-94dd-54824b25194f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "2efad100-091e-4c08-a9a2-a206ecdce5a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "84b7f689-7b8a-4a72-8f7f-aeaf77a91a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "dcf2b1b3-dadf-4cfc-8d73-cac76cde7a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "8c21d778-0777-4d50-90c1-58a1e628922a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "d8963b40-c340-4121-b01a-e084a2d07ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "6a78efc6-afd5-4c74-9b36-5ce83e7d11cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "6c7af4d8-8f41-4d5e-b62a-c75fddb3a4d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 41
        },
        {
            "id": "63bcde93-8638-407c-9ff2-dd90b6e4523b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "d40c64a6-56ba-4e93-b8b8-3bf52d0fbb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "0fae019d-0db6-414c-bf3d-e5509f2c6e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "de242559-8969-4002-b16d-f8c5eb84de9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 49
        },
        {
            "id": "3cd9bef9-a67d-43c3-becb-cc76c0159a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "99500f80-6ee2-47f3-a00e-4a9ed6ce3717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "a0e1b66a-57a5-4f90-b114-94ed1b19ebc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "d427f039-2899-46e1-bd37-3db67f655519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 57
        },
        {
            "id": "2f7e15de-7150-4de2-8ec0-49a5e89dc54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "d7665c9c-a0fa-4edf-988e-475514576436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "1d087305-4c2b-4ced-baad-c75aa0156c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "bda4f0df-6e5a-4854-b73c-84302f2c0a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 92
        },
        {
            "id": "337ba902-96d5-4335-babe-b416da1c8d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "7a14d9db-2e1a-4e5e-8e3f-507c1bed10d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "35431986-4e07-43b8-945a-417552ab3d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 106
        },
        {
            "id": "ff88ad33-367d-4068-88c3-af8729f2dd5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "b75b0ee1-2f2b-47e5-9560-8706e2fe1195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 120
        },
        {
            "id": "4cbb456d-a7e4-4543-8159-fa9fc804dbfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 121
        },
        {
            "id": "ac079d52-16d2-4095-8f92-662bd2bf299a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 894
        },
        {
            "id": "e8139a51-98c9-48df-8d5d-a3035ebbac34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "f8a7944e-54f0-493f-9abf-12dde7bcfa72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "2f8cf8c6-4535-4e1f-b616-517b183ae49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "13f5147f-41ca-4e42-9fbd-40f7640ef8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "421522e7-e994-42ed-925d-ee5d8caedc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "e023f60e-4283-47c3-b392-64f684976f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "e8006ba5-6288-455b-8c66-1e061e1e45a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "a65323a9-e88d-4f20-a253-2c8e629b55e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 35
        },
        {
            "id": "ebceef81-d819-4885-ba5c-4d99a10cf6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 40
        },
        {
            "id": "62fdb3f8-d64b-4bed-a590-5d670e479735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 41
        },
        {
            "id": "07f05cdf-b2b3-42f4-a73e-bfcfa0f17284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "96470167-227d-459c-a1eb-4413f5ef03d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 47
        },
        {
            "id": "c31330db-f151-409e-8178-f557753f40aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "a9fa8980-610b-4a8b-9318-9019ad98d25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 50
        },
        {
            "id": "6ed575cb-5dc8-4f58-be9b-66ff17decd8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 51
        },
        {
            "id": "58d862c6-8e00-49dc-93ce-224b01c8c46b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "335be878-94fd-4ec6-a156-ac3798a5f1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 53
        },
        {
            "id": "e7f65cdd-f49f-4f61-af3c-24e8c1daaae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 55
        },
        {
            "id": "190f44c3-6a33-43f1-95dd-0f8f755e7530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "885fd72b-2cee-48c4-8e02-1c71dc57783a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 58
        },
        {
            "id": "31a8a867-1280-4476-85cd-fb931c0e0065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 59
        },
        {
            "id": "2d6c4b05-4c8d-4a2b-86df-73cb614225d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 63
        },
        {
            "id": "a85394f5-c846-4162-8516-46f8e79c114e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 92
        },
        {
            "id": "7438817b-e786-4387-8795-6db71724d4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 93
        },
        {
            "id": "8cc528f3-7aa2-4327-a632-c15f45243487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "e7a0a8b3-078f-44dc-9f69-eaed48e06fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "ed5caead-1767-4c2d-9f56-74d9a3fc757c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "90704c29-c813-4c09-bbcd-042b21c59bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "f8072c66-d09e-461a-ad19-85b62cd3b61e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 115
        },
        {
            "id": "890af29e-e6c5-4a7a-bcf5-ba8ad5fa8058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 118
        },
        {
            "id": "c55b6cfe-2930-4a81-868b-6972edde6f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 119
        },
        {
            "id": "7c59736f-fb7b-4ea6-bdb4-ace25976c936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 120
        },
        {
            "id": "43fc238e-6237-4cb9-9a75-90b857ec769f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "f7240745-ccda-4add-81cc-637dcdbe2c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 122
        },
        {
            "id": "da685efc-cb8a-40b6-8bfc-bb7b7597e05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "fa7b3a3f-71f4-43c5-90c5-fc179b7cc699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 894
        },
        {
            "id": "54795abb-c9fe-4c54-ad1c-f2e615d5aa43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 8208
        },
        {
            "id": "eabd22c5-c2ad-4137-bd71-3a3e5f3e4df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 41
        },
        {
            "id": "6785fecb-8d32-410e-a4cf-46f82a1d3541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 51
        },
        {
            "id": "c6565815-4b47-4d2d-a095-681c5d36d156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "cf1340bf-9edf-4a6a-8a30-34cf6a4cba1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 55
        },
        {
            "id": "36dbf971-3859-460b-aa23-d772b967d0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 92
        },
        {
            "id": "be38058a-6a63-4048-99fa-156a64d8ca87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "7114c4e4-e976-4e63-8805-c4f8c919426e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "4bd79493-1932-4a24-b8d6-2f5c01f24c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 119
        },
        {
            "id": "7fb3db3e-ceb5-4a5b-abb5-999d4c59e45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 41
        },
        {
            "id": "436855ec-73e9-4cc9-ab91-5b550d8c1be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "880ef5c1-0be5-4fe5-be58-5b083517eb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 47
        },
        {
            "id": "4ad2cb96-0293-4132-a388-d9e2211870cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 49
        },
        {
            "id": "348c0fff-af95-4be2-a692-c4f25ba5eb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 51
        },
        {
            "id": "8fd1426c-8e8d-4c68-8e0a-9a7f4cbf2be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "9dba0d18-219f-4d2b-8179-b3c9cf81fca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 55
        },
        {
            "id": "bf7db894-08d9-488b-99e9-9876f2a642fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 57
        },
        {
            "id": "39e24889-bb57-4e4a-9b62-2d53637594d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 58
        },
        {
            "id": "1eacbc5d-714c-454d-a433-cdbd6f411b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 59
        },
        {
            "id": "cc0c8cc7-c71b-4b3f-a8be-5eda991d740a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 63
        },
        {
            "id": "c4b183de-cf32-4440-8c7f-491abcb63552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 92
        },
        {
            "id": "ed26b27d-4ff6-4c0f-86a0-907f99a58090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "aaf2f310-2b03-40b2-9974-b36ec73b8c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 103
        },
        {
            "id": "b4534202-01c0-4ac8-8f52-41a831b608b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "724501d2-8530-45e4-805d-ee5672e88c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 119
        },
        {
            "id": "d2c0ee5f-8d35-4bbd-ab84-5962690ccc23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 120
        },
        {
            "id": "c803b428-c3af-48b3-9cd5-bed7a81d0b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "ef6c0233-95f1-4db3-88a5-755cebd88c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 173
        },
        {
            "id": "27531f91-ad2d-436b-836c-fe3f0915b9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 894
        },
        {
            "id": "c6c81bca-e31b-4e14-8818-60d0a8b67711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8208
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}