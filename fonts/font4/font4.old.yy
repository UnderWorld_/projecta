{
    "id": "a47caaa9-7520-4dce-ac4c-54af4d907d78",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font4",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\font4\\BreatheFireIi-2z9W.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bulky Pixels",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "0dc95e66-99b4-42c1-9158-62632b4dfe48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7e4f5092-ec30-4cae-a5ca-e589a7d732b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 121,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "a9726cde-a8f6-4850-af92-923ddb778cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 109,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d00e6ffc-2efc-49d6-ad57-cab4d1d090d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 15,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5b651ced-7923-47d1-9e73-fc3581d6d14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 80,
                "y": 65
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "72bd40ee-7be3-4dac-9a9a-25cebffce531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 25,
                "w": 20,
                "x": 58,
                "y": 65
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fa35bebc-b1aa-4a08-976b-31fa3dcad7c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 42,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "51d6742e-b713-4fa3-b067-f226fa73bcb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 36,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9b064075-f2c6-429b-af55-c495cfaf151e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 26,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "30061cff-d416-4b24-b475-07853c63f70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 16,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "3ea82cb0-9b81-4f82-9d2b-b4ca631aedb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 15,
                "x": 127,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "028ec51e-6d72-4b3f-a7b5-75d59519162e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bdfa94d6-acf4-4075-87a8-cf5b4f605bca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 234,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4fe85e02-c815-454c-bd89-1bfb0f0ab85b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2cd89e7c-ad78-41ad-9c37-a5cc5fb9b452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 217,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "251ce15e-1b93-479d-aca6-4125f9f251ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 207,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "4319d449-1e65-480e-9507-cadeb0e8ee46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "35a88df9-6159-4f7a-964b-ab974c94205b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "fab5d2f4-da10-4610-b656-cf1945a70db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "06889fe5-150b-43a4-9347-1ba5d1428ce2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 161,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "09ed008a-b6de-4c87-98b1-ca999dee1acf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "00e160bf-0a2f-49c9-96d1-7fcc055b7b1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "048d2d98-2f37-40a5-89cd-bba14036a154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 144,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "c94d1214-98c1-43df-a0b5-22273c3bf580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 156,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "21ff453f-1e26-46e7-b620-14b70c9a4cf7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 168,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "6aa06695-f0a5-4899-80b2-7b44be204626",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 188,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d9aa822c-2d34-48c0-8d04-027671ab96bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 182,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c85abe76-2004-42b5-804d-55dedf1ec8dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 176,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8ee7a692-1e1e-43bb-a2ac-374d2b210285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 163,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a4436059-a6b3-47fa-86e5-4275beda4ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 150,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "112cb0f4-2554-48a0-9c74-c6be056eec12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 137,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f57684f2-500a-4833-af05-a346a60e8b88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 127,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "3857e668-e7e6-4dcb-a59e-91618cddcd02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 15,
                "x": 110,
                "y": 86
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8a7986d0-e7f6-4e1a-a995-27d9fe620e70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 96,
                "y": 86
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "38574130-5686-4720-884b-982193989bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 80,
                "y": 86
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9c3d2dc5-1507-4d17-8c0c-9a992b43a6b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "1eb43a88-4028-49ce-bd19-76f1209af0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "f94f2e3d-b090-403b-9f4f-266cdf94dec9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 42,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e60bfba0-3940-4201-ad47-66b97bc7c720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 30,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f40d8485-20b1-4d5d-aa7d-8fe58afe0be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "207497f8-11b6-406e-ad50-84365381f5d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "173a9003-37b6-44a3-8c52-49d4ad02e717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 239,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "0294b932-d221-4c7a-8468-aaaa2e4e0032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 227,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "f6f72545-b56a-49fa-ad52-c0319cdee913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 213,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "298c6940-6805-42f7-922b-6fcd983a1e2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 203,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d30c4916-a5a2-43f0-aab3-6559c017fb94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 182,
                "y": 65
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9db88515-bf12-4879-9e38-985c42aba660",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 135,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "f1720f2e-3878-448d-8c07-885e31c453a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "fe3d4f12-ea76-48ec-9ce0-1df11445f4d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 107,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "682d3a2b-1364-4dd4-9a23-0270083a09f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 52,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "2aa19d5f-a698-4396-9bec-547fe19b184d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d200449e-4775-44c6-9e16-62b2b2c7d500",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "89b927ff-de40-4cc5-8491-058b5e803a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "461d2fe8-e1c4-4fae-ae8f-4633e3c83118",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "abcf523a-c588-4349-94c0-c648003fff49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "a64bdd75-89e2-4c33-a367-c37a8496e538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "04e7282f-d1f6-4fc9-a1b1-3c0b9e9709c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "93943598-7058-4db8-a9e3-27f10cf7fc52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "5d0afe0f-619e-4ff7-836e-5d8da9bd75a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "770bec49-1420-4e72-af1d-37011174058f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 44,
                "y": 23
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9d8d8b51-0705-48e0-af45-547af658a33c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "89c0e78a-0544-4b1f-b393-9a707c1b0e65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "b9e59496-1deb-4a3e-8f53-03da5a55432b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "255130fb-11a7-4290-99c5-778a10917739",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4f4430c3-76bc-47a0-85e0-6dd62ed49e00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9d0e5c21-f153-4667-bcf1-ad7b23721499",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "bd31e4ed-a2b0-4f20-8e91-59fddad719b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d58d355a-2f3d-4a53-a57b-8b0e2b43ad37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b20706ed-0327-41fe-ab48-b4ac9cb21fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "832e1674-8ebd-46a4-ac77-b8a7e662b0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "eeb845a7-bdf6-4acb-90ca-65bd5d180a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "4c5c8fc1-e300-4bcf-a09a-df9c7feffe34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 68,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b031a8c4-1128-4e09-a6de-5039edff4f21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0cd3840d-111d-4db2-8c38-bae7177f6535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 82,
                "y": 23
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e39e4782-e600-4b9a-8848-5e2f070cc187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "11c7dad0-4791-4a92-8d53-5525a6f8e7b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 73,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6677a3de-a344-4552-8690-a0156b040d13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 65,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2c38c36d-1d63-411f-8e02-5c670f411f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 44,
                "y": 44
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e5cc4902-8802-4171-bc64-f024ef6f2ef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "c487d48a-c316-4f08-a4c1-8647f179b6ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f2672e8c-5a09-46e4-b454-1535dabdba87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "5790ee81-c12c-4bf9-8976-ff7fc177fb0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 231,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "69374039-9999-43fa-a9f4-89e794d8229d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 219,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "643d7fdd-312a-454b-8b7a-ddcaac0b8b4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 95,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "066a2c47-37dd-4eab-8d9d-d4a6abe5825a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 207,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "aa01a02a-b853-4f7c-ad13-3f9a7db8a722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 181,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c05d566d-4702-4982-89a1-355e42eac394",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 169,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "91815f09-4049-46cb-8b4d-6f3e6ebfdd30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 148,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4ea3a55b-8461-41c8-bb99-1feec4149baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "69424967-fe5e-4555-8a13-62c0f505f779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 120,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "40beac25-99ff-4fc5-8aef-2e39aa2fd486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 108,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5f4763e8-d7f2-4917-9ca3-05bccfdfc441",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 102,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7435bb72-2650-4b94-8983-8db0d841bafa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7078380f-7e0a-4b5e-a77c-69dae745ee5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 88,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f55f1320-ef64-4c74-98f6-95c01463cb7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 200,
                "y": 86
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2be88bf2-8bf2-4909-9b7f-24ed222789ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 221,
                "y": 86
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "7c0c3dc0-30e7-4473-be80-aa8efd3a790d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 33
        },
        {
            "id": "b42cc05b-dec9-43fa-9216-17e9b4f15b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 34
        },
        {
            "id": "1173d00e-511d-4869-b60c-854fd83af3d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 35
        },
        {
            "id": "ed58c06b-f510-4b8f-bf0c-0e6f1e118f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 39
        },
        {
            "id": "58e7f77c-26ca-4fd9-840e-b03894d644d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 40
        },
        {
            "id": "fe14d101-5aab-4158-880f-000041024777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 41
        },
        {
            "id": "46234142-6a9c-415a-a2bc-1c35c50a32b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 45
        },
        {
            "id": "49edc4ef-5101-4ea8-8041-15425a2a6054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 47
        },
        {
            "id": "ac012311-3453-4b98-807e-d589fadf0ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 48
        },
        {
            "id": "8a84e99a-2683-4e81-9c50-101f6aca0655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 49
        },
        {
            "id": "a77dfec5-fb3e-4968-904c-29bf1e940ec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 50
        },
        {
            "id": "40828dd0-03c1-427d-8c53-c87af8b1d068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 51
        },
        {
            "id": "59044d57-c229-4be3-b848-a876b49fcc32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "de86163e-0687-4024-b1fb-8ca46ca81896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 53
        },
        {
            "id": "fff0ce7f-986d-4212-9be0-834e9c7de446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 54
        },
        {
            "id": "5e3d79ec-b330-4d11-a61f-7bacfaef0b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 55
        },
        {
            "id": "4a80bb6c-2150-42e8-92d5-4642e6f86543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 56
        },
        {
            "id": "b095d1a2-42b0-47e0-90a5-a4f3a1779777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 57
        },
        {
            "id": "caed592a-1e8d-40ad-8745-11d9700ad446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 58
        },
        {
            "id": "f480b72d-cefb-4fc2-8d50-7f9cee582016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 59
        },
        {
            "id": "fbd48dd7-d9c7-4d68-b05c-714f9786d5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 63
        },
        {
            "id": "112b78c8-8dd4-45ee-965e-1cb91d42c2ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 64
        },
        {
            "id": "d2a7c4b2-dbcc-4275-9d67-602532424eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 65
        },
        {
            "id": "039af057-e6b6-4b85-bbfb-180ef895dd3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 66
        },
        {
            "id": "49c0ab5d-122c-44b0-9119-b7826c627376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 67
        },
        {
            "id": "7a7dcd60-91f1-44f8-b19a-5780b949b377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 68
        },
        {
            "id": "4964b859-84db-4aa4-aa8d-30c2185481b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 69
        },
        {
            "id": "ec700240-efbc-4256-b260-78844c604238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 70
        },
        {
            "id": "6d605830-1386-48df-aca2-7096a34f55e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 71
        },
        {
            "id": "2e6c2eb3-b5f9-4519-a111-9c9fc55f509c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 72
        },
        {
            "id": "75dd91ad-ee87-4425-8a5a-96ad038c7e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 73
        },
        {
            "id": "b5f91555-8633-49d8-9f53-4adfe32ac309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 74
        },
        {
            "id": "01f9690a-8ff8-4d30-b77e-9cede6b53756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 75
        },
        {
            "id": "065e3945-3b4a-44d3-82f6-f3c0d00a9d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 76
        },
        {
            "id": "69c868b4-0666-46cb-a97d-59a1026801b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 77
        },
        {
            "id": "31f3007f-346f-4811-8bb0-666c56957c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 78
        },
        {
            "id": "0c695b69-b314-4107-a0cf-0d7db6d139e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 79
        },
        {
            "id": "b486bb50-2621-4be2-a940-ba7bf02abd45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 80
        },
        {
            "id": "17f36955-d57e-4593-a3c0-ffa45a97cb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 81
        },
        {
            "id": "7f4648b5-2c90-4eda-9cdb-21b0096ec4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 82
        },
        {
            "id": "11a882f8-3ada-4329-b9f0-f8f4611889e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 83
        },
        {
            "id": "e5b6a6f4-e6bb-4517-8b5b-8266ce23bc41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "05294ebb-7349-43f1-90f3-a7d82eb665bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 85
        },
        {
            "id": "8f92a0b2-87b0-40cb-9c07-b0ae9d12a143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 86
        },
        {
            "id": "1e63ff1c-7631-4cea-becf-d293277e81f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 87
        },
        {
            "id": "f3af1b10-d278-4272-8595-21c7a1eebadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 88
        },
        {
            "id": "78f36d4b-34ff-4718-a418-2da63fc333a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 89
        },
        {
            "id": "86e9b963-15a1-49da-bdb2-0096e0f0b2bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 90
        },
        {
            "id": "d8770c17-0b2a-4635-bc3e-f62469808d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "69fc38e9-48ac-4fd7-952f-da07e14f2b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 93
        },
        {
            "id": "fc501542-f3cf-4cd3-b4f1-1e8298f2c973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 97
        },
        {
            "id": "01638698-40a1-45d0-ae28-54805c7e0130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 98
        },
        {
            "id": "eaad5558-ef30-46f8-bc5c-5e7602d839be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 99
        },
        {
            "id": "74eecff9-a03a-4457-8708-564c59e5add6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 100
        },
        {
            "id": "64223806-b379-45de-8fb1-1c8a2696619b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 101
        },
        {
            "id": "8d7c0d8e-532e-4f1e-9a5c-8460817bffff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 102
        },
        {
            "id": "a42b93e4-722f-40b9-aab9-c86759ade07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 103
        },
        {
            "id": "4d76ca0e-e24c-4bd6-99e2-d5e8543e7bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 104
        },
        {
            "id": "f1a138f5-b5b8-4ce9-adc7-cd33894060da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 105
        },
        {
            "id": "5f424217-9172-464a-a17a-4362172d7a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 106
        },
        {
            "id": "8ace3776-fe8f-4149-8ffe-b629fd57f958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 107
        },
        {
            "id": "1bf16566-bbad-401b-b3d7-fe1864362890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 108
        },
        {
            "id": "ede7656a-839d-4a02-8d39-7cfe75987dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 109
        },
        {
            "id": "ddd3edb0-f2fb-4bd8-81d8-8be70f9b1f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 110
        },
        {
            "id": "f9d746be-5de5-45e9-975f-034f1f225f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 111
        },
        {
            "id": "08f4b03e-5378-4961-ad8a-dcc66696c4cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 112
        },
        {
            "id": "b6238ae4-0477-48aa-ad79-8c483ad0e70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 113
        },
        {
            "id": "35461b49-0e3f-41db-a11b-d3ee5a63c72a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 114
        },
        {
            "id": "6bd1b9fb-471a-453f-9510-8b6c2836fbca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 115
        },
        {
            "id": "f6bb235d-44c1-45a2-afd5-1512f7481e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 116
        },
        {
            "id": "826055d5-2a94-4bbc-9e71-99b53cda4e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 117
        },
        {
            "id": "02b124bb-21e6-405c-9f7f-370687c7507c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 118
        },
        {
            "id": "381865dd-6f85-4e15-87f3-e77e1841c410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 119
        },
        {
            "id": "5ef645c2-1aed-48da-903d-ac929abbb82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 120
        },
        {
            "id": "3708ff1b-b959-4add-ae23-dcfb6e7096ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 121
        },
        {
            "id": "7f0e74f1-4e44-4a2d-bc93-17d617da18d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 122
        },
        {
            "id": "da264c07-1659-4272-ba31-198eb281180b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 173
        },
        {
            "id": "9d768e6a-8f26-4cea-a80a-cc5deabc9164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 894
        },
        {
            "id": "91bf7033-4501-40fd-9f78-f1f191cfbcb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8208
        },
        {
            "id": "8fe3d48e-376d-431b-a8b4-14a997986cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 34
        },
        {
            "id": "608ed119-f407-4cd4-a52e-b22be3b54fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 35
        },
        {
            "id": "9437f6ea-c89a-43c4-aa2b-2114a74f7581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 39
        },
        {
            "id": "28b28248-b23c-4eba-821e-3a95a539a980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 40
        },
        {
            "id": "56cc67c4-afb3-47be-9134-308ccb152d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 41
        },
        {
            "id": "a73e617e-0795-4785-a7cf-6f09cc194682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 34,
            "second": 44
        },
        {
            "id": "ea41826f-91ab-47cf-9661-b19a9cbded56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 34,
            "second": 45
        },
        {
            "id": "50e38853-77c9-4028-9f60-67a87829c1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 34,
            "second": 46
        },
        {
            "id": "e290f452-d483-4dca-af30-ac9797e8e54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 47
        },
        {
            "id": "8f1b9618-3578-4622-9fbb-d9f50458c0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 49
        },
        {
            "id": "0826ff61-c2d4-4dbb-9b7f-5e30496bc230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "4ac8b8ad-d53d-42b7-a0ec-dd27dc8519fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 51
        },
        {
            "id": "80704c4d-6cfb-4300-97a5-11fc10c1a6e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 55
        },
        {
            "id": "8962945e-3d1c-40cf-b38d-704916ad21de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "4873a107-a5fa-45bb-9e81-a4507f3f8dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 63
        },
        {
            "id": "ebc88252-65a6-447c-9814-9c5befafe8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 64
        },
        {
            "id": "f762d526-ea3c-4a09-bd23-fce2723a24bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 74
        },
        {
            "id": "e4a81c10-1499-444f-879c-cf16e4bb3aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 84
        },
        {
            "id": "34d44927-887b-48b5-8761-a3b67df45525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 88
        },
        {
            "id": "fd765ec8-5ad3-4ec0-aa62-a33d530a9f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 90
        },
        {
            "id": "637bfb0e-c95a-49b3-8787-fb4f0d7bdae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 93
        },
        {
            "id": "779b39bb-0fce-49f7-8684-0b9096af2937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "2f8a4281-b520-4cc5-9e2b-dc85af5ba637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "1649f9f5-7b51-4396-9dcf-9e68a57ed04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 105
        },
        {
            "id": "214f0780-fb76-4b8a-ade2-77afce219945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 106
        },
        {
            "id": "ff3abeb0-3da1-4e54-9947-9c1a68d26ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "8823f864-c79b-4895-9128-0bf12e5e27ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 34,
            "second": 173
        },
        {
            "id": "4c9ad068-da83-4a96-bf20-2d9487dddad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 34,
            "second": 8208
        },
        {
            "id": "0a79334e-83c5-4a66-b13b-92d83eafe98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 33
        },
        {
            "id": "f6957ac2-b95a-4c7c-afcf-f92358e9b1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 34
        },
        {
            "id": "3aa62c71-a3db-4296-a5c1-013c4654a60a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 35
        },
        {
            "id": "2b6f8653-a683-4168-9c7a-a11c89e7928b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 39
        },
        {
            "id": "651f7286-3961-4885-877a-da9ebbd0e226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 40
        },
        {
            "id": "a8ccd078-cd44-4ae8-84ee-50154b5ed3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 41
        },
        {
            "id": "44852e34-0da5-4491-b0ae-5346d8f9b05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 47
        },
        {
            "id": "3fab8575-9cb9-416c-8622-b325c81dfb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 48
        },
        {
            "id": "6a313a6e-12de-451c-9130-c4d9baa5d471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 49
        },
        {
            "id": "b93d97d0-c982-4f30-94b5-cf36a273031c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 50
        },
        {
            "id": "20d8e34c-5823-43c0-8742-4b9c8808ca8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 51
        },
        {
            "id": "a272794d-3ad3-4058-be3c-0508847fb0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 52
        },
        {
            "id": "74716373-62ee-40d9-8da2-4858abee13ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 53
        },
        {
            "id": "dd554ce8-cf21-405b-ac5e-b35c8c0a0089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 54
        },
        {
            "id": "36f9e5c9-7f7e-4431-8a28-c22e82a9dfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 55
        },
        {
            "id": "2ac27ff1-b1a6-4068-85b0-3fde0e698e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 56
        },
        {
            "id": "7df72079-e6aa-4eca-b7c6-fecd25d49672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 57
        },
        {
            "id": "1440cf27-6073-4e56-976f-e40e63dc1b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 58
        },
        {
            "id": "df7021a4-5179-4be1-8e76-6eca8285cf9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 59
        },
        {
            "id": "f8538c5b-19a8-479b-a968-0cf362591132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 63
        },
        {
            "id": "a16191bd-70ff-4a84-addc-5ca6610cff7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 64
        },
        {
            "id": "eb3a934d-ef8c-4dbb-8bcb-df77a7a7a693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 65
        },
        {
            "id": "bb2da7bc-b73e-44e9-8f57-368da5c78bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 66
        },
        {
            "id": "bbab632e-4c9b-4cc5-9d48-2a0156a2c260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 67
        },
        {
            "id": "05898ee9-1c03-4a46-94cc-79e85c488fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 68
        },
        {
            "id": "c396b54a-a93d-4452-a316-03ed40fb2453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 69
        },
        {
            "id": "55de533a-7811-4844-a8ad-becb645bbb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 70
        },
        {
            "id": "c6979e0c-c6ac-4088-b849-03b1bd43be71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 71
        },
        {
            "id": "66dcf323-1623-4918-843c-ed16603c9598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 72
        },
        {
            "id": "222d694f-ca2f-44dc-bdf9-30ff20f969e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 73
        },
        {
            "id": "702a7dda-e73a-48c2-ad37-c6a02abec9cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 74
        },
        {
            "id": "3181bb36-d8ba-4da4-827c-8343898f00b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 75
        },
        {
            "id": "382623ac-c38e-4745-8fee-249815b6a60d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 76
        },
        {
            "id": "f7825b55-c453-4c2e-8916-6371552614ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 77
        },
        {
            "id": "37e323d0-f33a-46e3-8e94-87584e0b80fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 78
        },
        {
            "id": "958f07fd-a4ef-4e6d-a0b2-c7579b3f260b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 79
        },
        {
            "id": "92dfc86d-4bcb-440e-8711-39394239e827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 80
        },
        {
            "id": "8c92d380-b377-4c23-9524-07d81630e123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 81
        },
        {
            "id": "88bff76c-82bc-4f47-82c7-b15367533107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 82
        },
        {
            "id": "4c460b80-4364-4088-97cf-93dac7d08a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 83
        },
        {
            "id": "d822d02e-926a-4e76-8937-36427186cda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 84
        },
        {
            "id": "bece0450-8f33-43e2-b36d-ae0357a452bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 85
        },
        {
            "id": "df898620-4a68-4241-b7f6-55aba61e1dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 86
        },
        {
            "id": "db2215f7-be26-450b-a7d6-cec4627f819c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 87
        },
        {
            "id": "879ce36b-a9ca-4301-b21e-2410738f4f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 88
        },
        {
            "id": "f63dfff7-4854-458f-8b5a-226817528fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 89
        },
        {
            "id": "281342a6-9ec9-4c48-81c0-d8f2a3b81304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 90
        },
        {
            "id": "f461b6ab-a681-4c33-ad9e-a34661acc297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 92
        },
        {
            "id": "6eaa3c77-1943-45b4-a4a5-f9cfa9a0f1e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 93
        },
        {
            "id": "9760a9a9-fbff-4738-b63c-a77606fe7ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 98
        },
        {
            "id": "d9f3fa65-f5a2-44d5-bf07-325f9c254311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 100
        },
        {
            "id": "3329a45e-50ef-430c-af36-d1f7c9d35771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 102
        },
        {
            "id": "1619df38-892b-45eb-8642-864872b26b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 103
        },
        {
            "id": "54219556-729e-425d-ba4c-57dbbf25db6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 104
        },
        {
            "id": "943e4138-d86d-4317-9d7f-ffb4c51ee0d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 105
        },
        {
            "id": "c042ca9d-c48e-4ac2-b131-8c80f7d3d616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 35,
            "second": 106
        },
        {
            "id": "cd74a8a9-ae72-49f7-81e0-8e55362bebdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 107
        },
        {
            "id": "44ce9a69-75ab-4f8e-aadf-2293a0ba72ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 108
        },
        {
            "id": "27db9cec-46b0-4ad2-8463-c5f836dbf6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 113
        },
        {
            "id": "04938563-8c72-4b07-a241-e64e0a8a0866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 115
        },
        {
            "id": "bf2f5fb8-0a81-40e5-9e4c-fa11f7911fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 116
        },
        {
            "id": "736b5202-1028-4e54-9350-b643f2c40cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 118
        },
        {
            "id": "d2df5c57-b408-4be8-991c-6ca963e53430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 119
        },
        {
            "id": "b67c1d9c-3ca9-4b69-aeae-d3a8700c32c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 120
        },
        {
            "id": "2666a82f-7c3d-4fe4-8632-29c413bba83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 121
        },
        {
            "id": "a92318d9-29fe-4a24-b7f8-5c1b785403d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 122
        },
        {
            "id": "be582ec2-207e-4ca6-a78f-25fae39ec418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 894
        },
        {
            "id": "6b352bcc-9e46-454d-9d82-25a7e9b064f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 34
        },
        {
            "id": "428c8d42-4089-4b0d-9fe8-da6be0c87871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 35
        },
        {
            "id": "4404c768-cf12-4254-80d7-cc8ade6b9c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 39
        },
        {
            "id": "3fbe7cba-9d01-4977-b8ef-3e469336b576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 40
        },
        {
            "id": "abdbdeee-9116-43ce-89dd-92e07948dcd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 41
        },
        {
            "id": "4595ac0a-0fea-484d-bd91-78d76e7e81a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 44
        },
        {
            "id": "b64d081b-6cf8-403c-8f99-ab604876eb98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 45
        },
        {
            "id": "041fdd11-7884-4d12-8328-6107188b154c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 46
        },
        {
            "id": "45402a78-b4fd-4b6f-9adb-8ee5ef1d94b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 47
        },
        {
            "id": "b7cf2d5b-8b5f-4c40-b994-356bfcad422a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 49
        },
        {
            "id": "271e2924-266d-478c-96f4-473e4b8157d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "1df52bf3-6ee8-4a4d-8318-3a408903f379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 51
        },
        {
            "id": "792b56bd-b3f7-4f56-a46e-100d8b260a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 55
        },
        {
            "id": "295c3aee-4bf9-468b-ab77-fe64fbf0a4ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "8f27e918-6f2a-4ddb-99f9-efb3a0955547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 63
        },
        {
            "id": "80dacef8-59ca-4faf-ba8a-57c0e2724ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 64
        },
        {
            "id": "d9352aca-e080-47f1-8525-5ecdb3888c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 74
        },
        {
            "id": "09ecf6bd-7bc7-40d7-b49a-1786782c0970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 84
        },
        {
            "id": "64a446c2-2bf5-404c-8d32-813437e14cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 88
        },
        {
            "id": "731ff908-0d32-4213-829a-68aa8a197226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 90
        },
        {
            "id": "7e562436-1f7d-4752-a401-09de3c52c4f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 93
        },
        {
            "id": "e76928d9-7bd6-45b6-9c1f-fe51751234a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "f9544abf-f0b2-4330-93e0-0801f810759b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "b26c3fa8-cc4c-4c66-bcbe-9f2c21c153e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 105
        },
        {
            "id": "80cdff86-5590-4569-b10e-86cd809fe6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 106
        },
        {
            "id": "2f56b112-97c2-4570-9748-de2c38e9a48b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "98bdc2b3-9d96-4112-9f36-c5632cf329d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 173
        },
        {
            "id": "669b571d-0d59-4504-a551-847d9a6f817f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 8208
        },
        {
            "id": "392b753a-26b8-494a-bbd5-cf142fae4efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 33
        },
        {
            "id": "6b9880dc-943f-4c3e-8be4-10cfff1a0313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 35
        },
        {
            "id": "d8bf02ea-16b9-459d-b773-57aa304d5c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 40
        },
        {
            "id": "b8c5fc82-9a61-4306-8595-a6554a26b89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 41
        },
        {
            "id": "2c4ab75c-4ebc-4f48-b5b9-43f5d8fea427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 45
        },
        {
            "id": "ea918a79-7d59-4f6a-b5b8-e07c06d90628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 47
        },
        {
            "id": "2118080a-04d8-4ad8-aa7a-ca3e5db295cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 49
        },
        {
            "id": "700368d3-4002-44d5-bb71-d8ef8fb765b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 50
        },
        {
            "id": "33f17545-4556-4c49-aded-b616ecae6403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 51
        },
        {
            "id": "26142ff6-801c-45e0-93f9-eda62b4f196d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 52
        },
        {
            "id": "fde4e06b-9f34-4e2b-9a22-ef2bb67df494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 53
        },
        {
            "id": "2ad01b8a-1a2c-4265-b7a1-78e9ac7b391d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 55
        },
        {
            "id": "51ccab94-6e18-49ca-91b6-fe7b584244e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 56
        },
        {
            "id": "37d5aaee-c672-49d7-a08b-5fc4bb9a2f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 57
        },
        {
            "id": "1532cd29-bbde-4e3b-8786-cdb4b26c9362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 58
        },
        {
            "id": "9434c06f-c25a-4c92-b6b5-d8a55c120209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 59
        },
        {
            "id": "eb765608-ac66-453f-815a-1ef1246e03c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 63
        },
        {
            "id": "94cb6c73-275d-43f5-a82f-41f11b6bf13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 64
        },
        {
            "id": "5bf7f701-c665-4072-b970-89ca2b5f87e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 74
        },
        {
            "id": "93e847c8-57f8-4942-b09b-4984da656bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 83
        },
        {
            "id": "0c943ecf-b888-46bb-a7b4-e00c9c8510ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 84
        },
        {
            "id": "9d666e0b-5c1d-4f83-b702-ba6385313190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 86
        },
        {
            "id": "24e47c3a-7758-4659-ac72-3d91856d6490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 87
        },
        {
            "id": "5935dc6b-0afa-42b2-a83f-4d1e69842c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 88
        },
        {
            "id": "969bbce0-e9bd-4ba2-be24-56fb3874869b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 89
        },
        {
            "id": "522d5ae9-3b05-429c-b843-754ab3489d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 90
        },
        {
            "id": "eaf35dec-a7e7-4484-a7fb-a3c61d432cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 92
        },
        {
            "id": "538c7514-9196-4810-8fae-603f5524b572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 93
        },
        {
            "id": "e98501db-5c5d-4812-8ba0-503f8a7fafda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 97
        },
        {
            "id": "64d8f04e-ceae-4607-b054-9dcb3d62cce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 99
        },
        {
            "id": "8c333df9-8475-4e78-a1b4-edcaf123a771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "a8f7eda9-66a1-4a68-9e91-b4476171b611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 101
        },
        {
            "id": "e9cb7177-c185-412a-a691-aa117abd4847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "a500e0db-0666-4023-97e6-d7db694fa88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 105
        },
        {
            "id": "94ec72ea-4e22-4186-b4f2-f99226145690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 106
        },
        {
            "id": "583f5af7-3e01-47ad-a01e-9d896b449eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 109
        },
        {
            "id": "99bb0c6b-6a90-411d-bb79-8bc64c6dfe1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 110
        },
        {
            "id": "42c16d4e-35f7-44d4-b8e8-8d1255ab5e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 111
        },
        {
            "id": "4ddbfe6e-ea2e-4901-944a-7ed4051914cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 112
        },
        {
            "id": "eb450c18-7331-4d9b-956b-6e88f52a54e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 113
        },
        {
            "id": "3264c2d0-74dc-425a-a06f-9a00bb2edd7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 114
        },
        {
            "id": "b49c1617-d039-4639-8809-e0048d2c25fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 115
        },
        {
            "id": "a0601176-3aa3-462c-a4f4-0fc4dc738f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 117
        },
        {
            "id": "a4a0dbbb-1690-4a67-9fb9-d914969a2a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 118
        },
        {
            "id": "4d7c42c0-3be5-45ae-9799-cc7a6aa9d7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 119
        },
        {
            "id": "78f6e7e5-9b36-45c1-962a-f5240fb502a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 120
        },
        {
            "id": "ae88ad49-f4c9-45d4-b95b-fb83068d90e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 121
        },
        {
            "id": "37405f0c-3465-41af-97b4-ec180a62d2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 122
        },
        {
            "id": "ab381577-8855-4b7e-af8a-ec2331ba1ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 173
        },
        {
            "id": "9add0fb7-23f2-41ec-8fa7-44c893e24e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 894
        },
        {
            "id": "98888663-cc0d-4266-8070-a4e66712dc2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 8208
        },
        {
            "id": "63ee5b4d-1e7b-47d0-b74e-0a95a29659c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 33
        },
        {
            "id": "d250adfe-8a17-4054-a853-9953eaa579b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 34
        },
        {
            "id": "1569d853-4367-4796-93a3-e5b42462ccad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 35
        },
        {
            "id": "eb2fb927-59a4-4572-b3e9-59730aeba1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 39
        },
        {
            "id": "852a5907-519c-469a-a336-6247c9b1b6d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 40
        },
        {
            "id": "d31d089b-fc77-4c06-b6b4-081ceaedfeb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 41
        },
        {
            "id": "b384bbec-8943-4f7d-98a0-676c32f754bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 44
        },
        {
            "id": "f0564af0-b3c0-4ac3-93de-3c0627a28130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 46
        },
        {
            "id": "54e3f41e-81ff-44e5-90ca-1ddc779d2137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 47
        },
        {
            "id": "4098dc02-7d2f-4148-b877-0e460e5f4768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 48
        },
        {
            "id": "15181259-d125-41ce-936b-9f54b33d5ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 49
        },
        {
            "id": "5c924798-5c43-445f-a48a-b42b17528af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 50
        },
        {
            "id": "e95be798-9aeb-4814-be54-21535b8aba8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 51
        },
        {
            "id": "87360839-6203-457a-837a-16757e8ef3b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 52
        },
        {
            "id": "b0959160-2d21-47fa-93e2-2d442350b4c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 53
        },
        {
            "id": "44e4f070-dfaa-4925-a87d-262993da5ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 54
        },
        {
            "id": "a422fa54-4045-42f8-8a26-89f067fb8310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 55
        },
        {
            "id": "1c3d186e-0ffc-45c7-9459-06b6f8c3e707",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 56
        },
        {
            "id": "003b8815-2479-48ab-9432-bf7eb5330184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 57
        },
        {
            "id": "db459465-afca-448a-b5b8-20bf2109df5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 58
        },
        {
            "id": "415f2595-7043-47e2-8c08-b0b28b455655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 59
        },
        {
            "id": "b5a0fe24-4d4f-4fb2-b6d3-843826172a4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 63
        },
        {
            "id": "ccc9d940-8e23-4d36-bc09-4b5fb8836d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 64
        },
        {
            "id": "91b76965-6cc6-488b-a903-3b730bd52533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 65
        },
        {
            "id": "ee00d73f-38fe-4a4d-967b-d7c14ee14ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 66
        },
        {
            "id": "c39736d0-9d88-4b99-af2c-eeefd4bb9c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 67
        },
        {
            "id": "01001308-d1dc-4cbf-8164-cc00cf34dd25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 68
        },
        {
            "id": "e73b4266-fa22-4059-a394-6a59e09074c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 69
        },
        {
            "id": "f4f82eec-9679-476d-9e13-963c3fc55c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 70
        },
        {
            "id": "d08898f1-cada-44a5-a6c5-922c71cde3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 71
        },
        {
            "id": "d3890b2c-1cdb-408a-95db-a3a99f9e3060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 72
        },
        {
            "id": "cf043ef6-abb8-4234-b8fa-0dc0c4249106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 73
        },
        {
            "id": "6bcd436e-26b1-4905-93b1-ad31bf748ac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 74
        },
        {
            "id": "612eb2e0-1502-4fd3-9513-eada9c67c6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 75
        },
        {
            "id": "b66756c4-e653-43c1-b0f8-f0fe2cda49e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 76
        },
        {
            "id": "7af79e07-766b-4603-8350-3315ddde2e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 77
        },
        {
            "id": "bf6cf09d-ba4d-41e4-92ed-7318aa94467e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 78
        },
        {
            "id": "6ed15bfa-4ada-4cc9-9a3b-b9aaa360a1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 79
        },
        {
            "id": "317a1501-9f76-486f-b451-05649a4cc050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 80
        },
        {
            "id": "ec027da4-bdcd-4c7c-9ab3-049f47f37210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 81
        },
        {
            "id": "4d42f77e-5a88-41ce-b62f-b307ffbd4558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 82
        },
        {
            "id": "f49a6c2b-020b-435c-8fa0-8eafd2aea051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 83
        },
        {
            "id": "b444b1ac-991c-4ead-99a0-2af6508cfef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 84
        },
        {
            "id": "f1affe16-acbb-49f7-971e-2f815b455dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 85
        },
        {
            "id": "bc8ff5dd-ce68-4027-a884-adad0f30412b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 86
        },
        {
            "id": "dc49b2cb-13bc-495a-b8f8-15955dd70979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 87
        },
        {
            "id": "301022c1-34af-4d8b-af93-d2c879752084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 88
        },
        {
            "id": "9df20035-4d52-4df4-ac4c-94f225328b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 89
        },
        {
            "id": "6474121c-16ab-4598-a32f-bab4d75f11e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 90
        },
        {
            "id": "9c7965bc-8f03-4401-8899-247677f8b4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 92
        },
        {
            "id": "80288acf-d3cd-424e-96ff-ade3ef6180c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 93
        },
        {
            "id": "d93abcff-dd9f-4d2a-8b78-cd5bfa374152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 98
        },
        {
            "id": "70fec3e4-5d8a-40d7-b56f-3af6c1dd1159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 100
        },
        {
            "id": "109d8665-3990-4941-b38b-d5dc70acdcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 102
        },
        {
            "id": "8886bce3-d922-471d-b307-bc5059ac86e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 103
        },
        {
            "id": "9925feb2-6384-414b-8df4-5d803be08046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 104
        },
        {
            "id": "90a34c7b-20da-4385-929b-3c1bec61f5fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 105
        },
        {
            "id": "4b296d97-cb47-4e20-a7dc-6d4dca8a4ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 41,
            "second": 106
        },
        {
            "id": "8172013e-ea3b-4ec4-8e77-39c7e3ad0cfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 107
        },
        {
            "id": "c8778af9-3a1a-4d7d-a002-f33192a6dde9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 108
        },
        {
            "id": "a862e941-03e0-4ae8-94f0-8e51194be5fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 113
        },
        {
            "id": "e5d482ef-07f5-4e23-89bc-93c00d028351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 115
        },
        {
            "id": "0d37eb4b-cb5b-465c-8542-1b696aba781c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 116
        },
        {
            "id": "0cdb208a-f206-4363-9bbc-08b262d25fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 118
        },
        {
            "id": "de3e68c9-93ac-4bf1-a3b8-b782ac4496da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 119
        },
        {
            "id": "1903bb16-141a-4363-976c-a54c491a739d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 120
        },
        {
            "id": "3fd11ee0-7acd-42c2-9432-c97937b80732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 121
        },
        {
            "id": "07d2945e-4a7c-4220-b75b-9126b791f286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 122
        },
        {
            "id": "c370e61e-d248-45d0-8593-15aef668fa6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 894
        },
        {
            "id": "383e376b-c572-491e-82ca-74027a1f504b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 33
        },
        {
            "id": "1cb3bc1d-7d75-4c40-8c9b-76d487c5afa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 34
        },
        {
            "id": "3665d0c6-9497-47d1-a026-ba1c01c05314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 35
        },
        {
            "id": "991cd92f-619e-4205-9cb1-6d1050430274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 39
        },
        {
            "id": "a0011fd9-f8e4-4190-95a6-dab6cef53e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 40
        },
        {
            "id": "f760d635-f067-432f-948c-9b51cb438f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 41
        },
        {
            "id": "a89f46f5-3d13-4f95-af5e-e0b41c183b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 44
        },
        {
            "id": "d1e73bca-9542-424f-8cd7-f89f646ea086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 45
        },
        {
            "id": "f0933da6-4502-4844-971f-c0afc54b244d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 46
        },
        {
            "id": "88f70237-8086-4575-94da-195b4967e447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 47
        },
        {
            "id": "8c41dd7f-8c2c-4cfe-a8e4-d4f358810b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 48
        },
        {
            "id": "e3e1c174-3dfd-49f0-97e5-25b4768ff267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 49
        },
        {
            "id": "7896bf2b-4d68-4979-9a76-e7aa85d01568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 50
        },
        {
            "id": "965a0bc3-70f1-495f-807e-53af28474db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 51
        },
        {
            "id": "20e268a3-bd35-4bc8-94f4-ade0b826bbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 52
        },
        {
            "id": "2f6020f3-b7d4-42b8-8601-416168b3c269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 53
        },
        {
            "id": "4388fe7e-6acb-4686-9f8a-69af2c10e535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 54
        },
        {
            "id": "3760fb1d-a745-45ed-9009-2d8beaa84ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 55
        },
        {
            "id": "f9014e53-e9c1-4629-b3d0-c6035bc32310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 56
        },
        {
            "id": "1677b2aa-d492-4036-8fe4-f1df2251016c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 57
        },
        {
            "id": "ce23c89c-b9fd-4268-b017-deaa7ff057a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 58
        },
        {
            "id": "a7ec4898-5588-4733-afd3-5b2f8df55529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 59
        },
        {
            "id": "ce762764-a514-40a9-b81f-120c265542eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 63
        },
        {
            "id": "2c3f9b6c-99c0-46f2-8c84-0932820e7a4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 64
        },
        {
            "id": "fc3d473f-e4e8-46fd-b916-d933a33aa550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 65
        },
        {
            "id": "8f7f7638-2ee5-41fb-ab64-ae2175112921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 66
        },
        {
            "id": "5e2abdf2-fff1-4571-9c45-2018f442c990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 67
        },
        {
            "id": "932c4924-2556-445b-a243-f81aba55a54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 68
        },
        {
            "id": "1b6c9f49-1052-43f7-906b-a090c7f15246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 69
        },
        {
            "id": "47e67122-e748-4960-98ca-ac8fb5dd8506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 70
        },
        {
            "id": "fcbfb5dc-69e4-4563-a88a-169a663decb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "b7827fe1-a6d2-4bcd-bb88-1ce43f21c951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 72
        },
        {
            "id": "9e6b5ff4-def1-4beb-af05-bc77b1be4afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 73
        },
        {
            "id": "ad1cbc7d-339e-4a6c-b4a5-9eb09c3d2354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 74
        },
        {
            "id": "f3d1814a-251a-4325-bed3-5144c043ca40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 75
        },
        {
            "id": "e9a701d2-63e4-40a8-9252-e5aeaf3014e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 76
        },
        {
            "id": "4014e981-07ce-4b8f-870d-258ee321a66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 77
        },
        {
            "id": "60f4bf01-9e78-44ee-b8a1-6fec38b7d64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 78
        },
        {
            "id": "4794caef-0b53-472d-aaa3-3d94e2e3451e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 79
        },
        {
            "id": "4a556bb8-84a3-44f2-9a90-b4ca528bd238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 80
        },
        {
            "id": "ac8f41d7-6444-4257-b18a-1d1468d0033a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "97a2c226-fe50-42d6-a15e-bfc8ae47c34e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 82
        },
        {
            "id": "1f1d17f5-da6e-46e5-8439-730bd3081a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 83
        },
        {
            "id": "94fd6b68-88f2-4c23-be01-df5b6e5ba75d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 84
        },
        {
            "id": "c2a37287-2e50-4842-afd7-6e605a71cb11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "5d74b018-b4c8-4ab7-8276-4c485dd02f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "e40d6ce5-1b2e-4e2f-b5fc-7b9961815ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "762d15a1-90ad-4e3b-b0e1-703e5775c349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 88
        },
        {
            "id": "8aa28438-5e5a-4383-be13-87bb96b3eaab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "87473f89-3a1d-4784-84e8-b7d0a38fa744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 90
        },
        {
            "id": "a720565e-a0eb-4814-9cef-a619285989c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 92
        },
        {
            "id": "f8868b77-fea9-4be1-97d6-f43ecb8f2dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 93
        },
        {
            "id": "9df548a8-cc06-4ae9-8d12-b401738de04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 97
        },
        {
            "id": "990cb3e4-6b8a-42a6-9028-838e7113df92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 98
        },
        {
            "id": "029a6115-5ba8-483d-91f0-65d43963f70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 99
        },
        {
            "id": "3dbe9446-1c78-4163-b647-16e0c3fd6c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 100
        },
        {
            "id": "c49ff2b2-fd2c-4124-9e16-fcfe82628928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 101
        },
        {
            "id": "d641b2fc-a380-4395-9814-b0fa512f4590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 102
        },
        {
            "id": "e4a2c12a-316f-4c6b-b507-e1e05a9d2a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 103
        },
        {
            "id": "0d7771ba-5161-4673-b6bd-ba3f330b6d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 104
        },
        {
            "id": "662e349d-cf09-460f-801e-9ea3fae7bd47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 105
        },
        {
            "id": "3881ab83-81ac-47be-b2d8-15e30546ecc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 106
        },
        {
            "id": "d4a03e35-f275-431b-b404-412b3677f3f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 107
        },
        {
            "id": "4ee8068c-c3ba-413a-9a41-95e40cb72c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 108
        },
        {
            "id": "7664819c-a536-4420-9e26-b566238ac602",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 109
        },
        {
            "id": "52b8b798-0e6c-40e7-8887-3353b439d2ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 110
        },
        {
            "id": "d3c46a74-95a2-4876-a2ce-5ffa89437655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 111
        },
        {
            "id": "319a0c59-3adc-44f0-b793-97c12879ef17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 112
        },
        {
            "id": "e4cfed87-8a32-48d1-a3bf-992d52745a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 113
        },
        {
            "id": "a031085d-b8ef-4f81-8707-6e689255b1f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 114
        },
        {
            "id": "6993aeaf-ace0-4fff-aeda-af57cfbcec01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 115
        },
        {
            "id": "f02a7175-a5d5-4ad6-a238-ad656812303e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 116
        },
        {
            "id": "e396ea20-68c4-4b43-a150-022887198236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 117
        },
        {
            "id": "a7e14882-bd0c-4782-93b5-7a111732cc72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "bac44ecf-4920-41cb-9b38-8ae9d446fafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "c0aac4e8-2d0f-4ecf-afe3-770e4209fb6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 120
        },
        {
            "id": "0fc9bf5b-d324-4f8b-9479-33bd3eaea613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "33b2e19d-7945-4ce5-9d7e-4330df7992de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 122
        },
        {
            "id": "746beb8c-d980-4347-955e-f887346cb2ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 173
        },
        {
            "id": "43804726-939a-4250-8f68-c3ffce0df9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 894
        },
        {
            "id": "f15a92ac-c538-40e1-98af-70512b2a39e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8208
        },
        {
            "id": "143c3c81-aaeb-407d-9f21-eae1c93de5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 33
        },
        {
            "id": "03b86573-19dc-4c9c-b2d5-c922bed1a460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 45,
            "second": 34
        },
        {
            "id": "4adf49f4-d1ff-416b-bff4-8f8192846bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 35
        },
        {
            "id": "dfe57ca5-a8ec-4ee9-b519-a5fce10657f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 39
        },
        {
            "id": "50e35188-7afd-409d-8453-afb1a668fe67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 41
        },
        {
            "id": "713b4bb8-0323-411d-9fef-4fc0de855b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 47
        },
        {
            "id": "572f2851-b044-4aff-bd6a-42eb2a5dd813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 49
        },
        {
            "id": "4734a34a-1975-4320-bfa2-988c5d34e205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "6ba19ed2-b2ea-4d8d-84d9-b431252401bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 52
        },
        {
            "id": "4ec64c15-5838-4254-a556-eb661b2a42d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 53
        },
        {
            "id": "50d93dde-0c18-421b-9b5f-bcdf0607bb76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 55
        },
        {
            "id": "a6ce7600-f296-48b7-b202-8b168f038435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 57
        },
        {
            "id": "1a8717c0-685e-4bda-8182-4c9e01f07d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "b8f110a6-1867-4436-9da8-954fe5c7a351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "90de2c98-f90d-4694-97a0-9a7fa34485ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "fb43424b-2bc2-475a-adcc-904ddfc06be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "4401caf0-aabe-469a-81cb-f6b15fc8ba8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 83
        },
        {
            "id": "145c5238-29eb-44cc-8939-b8d92188bbd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 84
        },
        {
            "id": "3780d730-01e6-433e-bd28-5e0ae5c7fd93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 88
        },
        {
            "id": "b3af1a3a-28e7-4b27-9a68-4e9a8c470729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "27eabb0e-d8e1-4e27-98dd-08b91b9437e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 90
        },
        {
            "id": "814e2aac-fd16-4deb-ad13-df23cebf4dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 92
        },
        {
            "id": "4da938e9-0842-4855-93ee-8df7ae0aed75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 93
        },
        {
            "id": "105b1474-6735-4efa-87a3-b17a3d99847d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 106
        },
        {
            "id": "7bc58ace-1bc0-4db8-bd65-d6b222791233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 115
        },
        {
            "id": "f60820f9-3179-479b-8a17-b8eae2b44851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "c85c059e-2b18-49e5-a429-0614e6fe1564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "21de8e6f-1009-48d3-a0cb-31268679a7f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 894
        },
        {
            "id": "cc91258d-7834-48fa-84d3-e0188759b2ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 34
        },
        {
            "id": "ebf87171-c346-4d25-8718-4e06cb92ff95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 35
        },
        {
            "id": "e3e069e4-6e72-40dc-88dd-f906395da208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 39
        },
        {
            "id": "f050f080-10ee-4337-a0f0-a36fa04ad078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 40
        },
        {
            "id": "44927c6e-8c3c-4690-ab78-3c8da7cfff1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 49
        },
        {
            "id": "aee361c3-2378-4012-a839-4ae8ab2066c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 52
        },
        {
            "id": "cad358aa-9cfe-43fd-b428-fa9890bd9ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 55
        },
        {
            "id": "ce3b289d-d445-4b2d-9a24-71f185cb0518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "cee04e2f-ec9d-4f73-a318-54e70a7b5c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 64
        },
        {
            "id": "2c88043f-31d8-4034-a8c5-2b0d41a3e4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 84
        },
        {
            "id": "3dbf05b4-2715-41c6-8716-04ab5a709828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "7bd3ac6c-5605-49cd-95f8-f7136132ffa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "85ab5841-0008-4925-97f5-c945c502e009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 92
        },
        {
            "id": "1799ecd6-1e00-4843-8a5e-56c5fdf829d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 103
        },
        {
            "id": "dcbbcb1f-c441-49d2-9fb9-8945ada41603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 106
        },
        {
            "id": "4b5215e6-e8f5-4890-be3c-5d56721d2635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "7b317abe-591c-4883-a781-79a31c8034f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 119
        },
        {
            "id": "1afb82dd-fddd-46ae-b71e-0a93fa9f7614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "3867840e-31a7-4a4b-90ff-4d12146ba51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 33
        },
        {
            "id": "fcacc1ea-5932-4233-9abd-641b388fc9d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 35
        },
        {
            "id": "528034f9-e09f-438f-91b2-bd5aa7f0cd85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 40
        },
        {
            "id": "d97bcf25-d49e-4b6e-bd80-100c80727a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 41
        },
        {
            "id": "bf1f4d74-a56b-4f22-a00f-14e722fc3576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 44
        },
        {
            "id": "d4254fff-784e-419a-aa78-944dffe03441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 45
        },
        {
            "id": "e116bde1-e478-4944-9353-ea280e5fa3f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 46
        },
        {
            "id": "49dcc7ff-4618-4dc6-9e90-85c885e3f2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 47
        },
        {
            "id": "a5caa7fa-5234-4021-955e-01b9c0c1d108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 49
        },
        {
            "id": "890865e0-6607-4cac-8937-9e5ae7533fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 50
        },
        {
            "id": "0d0bd018-d0a2-4225-b1a1-cadcfbfecd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 51
        },
        {
            "id": "8e0d4e04-9976-4783-a4f7-fced66bc4072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 52
        },
        {
            "id": "0f3829fc-858a-4dad-83a1-3dc8eaf52392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 53
        },
        {
            "id": "2c4874aa-5d8c-472e-add4-9bda26dcab63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 55
        },
        {
            "id": "70c0fbfe-974c-419b-bf44-6456967db6b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 56
        },
        {
            "id": "1ccae2f1-d35c-4cd7-ab08-404d0f8bc9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 57
        },
        {
            "id": "6b0cf61d-a711-43dc-8152-fb97dd9238dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 58
        },
        {
            "id": "3ad2ee45-1a51-4cde-8e51-6334903a3546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 59
        },
        {
            "id": "3a8ac542-547f-4b66-9f5f-fbd5b165b2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 63
        },
        {
            "id": "9023ff3e-73b6-479d-9c82-23441c4cd468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 64
        },
        {
            "id": "f959710c-db80-4e0c-bd3f-c7ee303ce008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "434a2d97-9000-4e93-9865-a53282e9ae3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 83
        },
        {
            "id": "66942db8-cefe-48fb-975f-bee6e0d590b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 84
        },
        {
            "id": "20ec9ef2-ee9e-4fee-92ed-0fbde9699ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 86
        },
        {
            "id": "8cfbed2e-cac0-4192-a94a-5944a4e6ef52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 87
        },
        {
            "id": "5ce088a9-a149-4d14-92d1-b88c7c1bb38a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 88
        },
        {
            "id": "c27db1ba-4d16-4c7f-aaf2-b7ec34e74ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 89
        },
        {
            "id": "d877db88-dfa5-4d4c-8fa1-2a778176cd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 90
        },
        {
            "id": "13e566a5-e4fa-4344-9ca6-8543486068d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 92
        },
        {
            "id": "d8d374cd-ad15-4c52-919c-51cfe37010a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 93
        },
        {
            "id": "259ca3e8-f5ae-4e80-a956-87d2a8100d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 97
        },
        {
            "id": "2b6ea7d1-4fbf-4ee0-b2f0-6001bfaa70de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 99
        },
        {
            "id": "849e28f0-7295-4b26-baaf-49fc715931c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "d69e3b61-def4-4a61-b7f7-97bd3cac0003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "1a2ff6ff-7bfd-4586-90af-4761dad791f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 105
        },
        {
            "id": "397dd22e-382b-4161-b8ed-43d9062e7fd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 106
        },
        {
            "id": "2eaac098-4439-41ac-b1f9-5d8c97ec5278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 111
        },
        {
            "id": "88807868-0de7-4a5d-b7c8-2f498f685b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 113
        },
        {
            "id": "a0ec3f49-ca3e-40df-b861-0de6ab3e2305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 114
        },
        {
            "id": "73aaf30d-d34e-4c3b-a156-9e0782616ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 115
        },
        {
            "id": "ffb7f20a-337f-4c81-988f-7fe584c47480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 117
        },
        {
            "id": "012b6735-2dfc-4f59-ab73-554510d5bf12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 118
        },
        {
            "id": "7e38b8af-5182-4b27-bfdc-b423e0b40d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 119
        },
        {
            "id": "c5545285-8a36-4c63-b0f1-467aa203ce38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 120
        },
        {
            "id": "31cd6b3a-0739-4792-aef2-82e9545db333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 121
        },
        {
            "id": "1bd3f2b0-59ab-4fb7-86b3-390f769689eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 122
        },
        {
            "id": "b7ded96b-6f66-4651-a2f9-0dc6c56a5290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 173
        },
        {
            "id": "f3369ffb-5831-4710-bc50-3307d90edac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 894
        },
        {
            "id": "c682e8d7-701a-4d45-8b4b-572450aad543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 8208
        },
        {
            "id": "9d33194f-fb1f-46fd-bd22-fdf9aa1a9d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 33
        },
        {
            "id": "4563b859-cf87-4e42-b68d-e6a24cc8c230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 34
        },
        {
            "id": "15896da9-f653-48f0-864b-6fe2f1b21153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 35
        },
        {
            "id": "73db4f08-3b01-444e-a987-133d9f77af7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 39
        },
        {
            "id": "67275136-c92a-4139-9dcd-9b4fa94da20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 40
        },
        {
            "id": "e6a39e6e-6218-4d64-93ab-7b68bc81b49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 41
        },
        {
            "id": "40835ca0-07e9-4ab6-80eb-a5047b13aa39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 47
        },
        {
            "id": "2c1392d5-b5e5-48a8-8c58-329917df4aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 58
        },
        {
            "id": "f4a9d747-71d7-436b-9915-56f931892e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 59
        },
        {
            "id": "21babac4-2b3d-4571-8c26-0d6880547d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 63
        },
        {
            "id": "9251f57e-bc96-4e3b-afa0-b2cdf057d898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 64
        },
        {
            "id": "8917ef28-ad8f-49e3-807d-c1fd2e97f859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 74
        },
        {
            "id": "6b5e67a4-59dc-44fc-9092-f81711734e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 83
        },
        {
            "id": "066373de-c084-43bf-b4fe-1e6f1bd1de0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "b2e0a882-1ab0-493a-937a-5842f5834157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 86
        },
        {
            "id": "67fbf70a-af4a-462c-b24f-a37ede427c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "6ff624bf-891a-43d8-b7f0-073805ee1b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 88
        },
        {
            "id": "8b4fcf8a-5dcb-4a08-ac1c-904d92e6b21b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 89
        },
        {
            "id": "54f3d88f-cdd4-4ec0-80b3-3f73a6587735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 90
        },
        {
            "id": "35933d07-a2cf-4506-a742-a6400ef3534c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 92
        },
        {
            "id": "e35d253e-ac27-40ee-bf94-7dffadcd5b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 93
        },
        {
            "id": "b445fcc2-7c6c-42fe-af00-8b4ae4bbe6d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 100
        },
        {
            "id": "f6a6e01a-f5c9-4b35-9485-c0c95bbf89c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 103
        },
        {
            "id": "374c566a-341a-4ee7-95bd-bf860de6e375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 105
        },
        {
            "id": "c3820417-8970-4215-9f26-0c5ff924f374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "dc2c5b23-62d0-4774-b196-111c642f17a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 113
        },
        {
            "id": "8f31679b-eb82-40e3-a4fe-ba6d5bce00d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 115
        },
        {
            "id": "b9f2a17d-035a-4cc4-9e08-e2b130ebc33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 118
        },
        {
            "id": "77867006-4da6-41c1-b847-e8d31e5c2c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 119
        },
        {
            "id": "e061cb28-abb2-4c23-9d5e-89d51cff157c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 120
        },
        {
            "id": "83e1792d-a82f-457f-bb80-741e1eb441c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 121
        },
        {
            "id": "d5eba732-b4ae-4320-a703-4e92e88d4f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 122
        },
        {
            "id": "1475453d-8594-43ae-8f3b-bef36b4fd30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 894
        },
        {
            "id": "49202a48-f4d6-4de3-bfa6-d60a9c021520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 33
        },
        {
            "id": "a631077a-1430-45ed-8fcb-65fa020aeaca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 34
        },
        {
            "id": "18dedde1-6af1-433e-8cdd-37ed858306b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 35
        },
        {
            "id": "3053110d-2497-47c3-86c8-d082b36173d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 39
        },
        {
            "id": "f6b6baec-0ef4-44ad-ab16-a07cac5e3afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 40
        },
        {
            "id": "ca0a3f60-c68d-4e90-a0e0-eb6a0bcb735f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 41
        },
        {
            "id": "0578ec81-1d1c-4221-b0fe-76234e03f4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "fb929f57-a6ca-47b3-8a07-5822763cb97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 58
        },
        {
            "id": "d5ed47d5-9877-4a1c-bf35-5e0bcd931394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 59
        },
        {
            "id": "513efdde-b48d-4533-b4b5-7700c085c2e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 63
        },
        {
            "id": "8d3a3a2a-475c-48e2-bb42-ee5170443a04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 64
        },
        {
            "id": "e1d84e00-e07a-4311-9968-60d029e2c445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "ef5fc551-773f-40f3-b7df-9e117915fe26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 83
        },
        {
            "id": "7a6b4325-b452-4cae-9fda-19bc8a7f34d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "fcd1e12c-5973-4d17-b9ae-495d49893ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 86
        },
        {
            "id": "75c748bb-5094-4f44-8917-dc808dbb5335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "40d7ccee-2e64-442f-83fd-ac10a5ca72e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 88
        },
        {
            "id": "4762242a-7458-4463-bafc-31b47a3c025e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 89
        },
        {
            "id": "151bbdde-ee26-4b24-a620-399a82229e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 90
        },
        {
            "id": "43bbdc55-f964-4b9c-a7c9-426cef57ca78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "0798515b-120a-4f10-b37a-708b046b717e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 93
        },
        {
            "id": "d384a96f-a9a2-4a59-90ce-dc47cf222218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "abe5d899-5acc-46fa-8d4c-3dec9bf98370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "e3a4d1e2-3150-4c37-8ac4-d88f14882abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 105
        },
        {
            "id": "51ad1a5e-d09c-487e-84f6-b8a94ccfc26a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "51219516-29c0-4b63-9907-593278ffa0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 113
        },
        {
            "id": "1e598708-22fd-49e6-b1d0-d4ec63513365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 115
        },
        {
            "id": "5d9e405e-d201-4b06-bc7e-5ed6681b5d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 118
        },
        {
            "id": "cbe89e60-772c-43ab-8955-0939bb357209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "9e47c57c-3ab2-41e1-a285-0bdd125c73b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 120
        },
        {
            "id": "732365ab-6b83-4cc7-9712-fff37d628bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 121
        },
        {
            "id": "b51c01b4-29b4-4aea-86bb-105b91176de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 122
        },
        {
            "id": "821f345c-7f5a-4c84-a649-3e786955e908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 894
        },
        {
            "id": "e1d11aac-1532-455a-b13e-1177512b3fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 33
        },
        {
            "id": "01abf654-c74c-462c-b8c8-bae32fced632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 34
        },
        {
            "id": "311ed743-eb51-4166-805a-6254ac02414c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 35
        },
        {
            "id": "81b3d0b6-c2e2-4690-ac11-7529c8f6834f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 39
        },
        {
            "id": "d7fb32e1-fc60-484d-9757-fb5e2fb4a001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 40
        },
        {
            "id": "77216cbd-0881-4b58-ac31-d198430fff84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 41
        },
        {
            "id": "d2226bc0-5f8f-4b8e-9ede-96b15c0260bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "789300fc-e253-4fed-aa0f-bd4ec8232261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 47
        },
        {
            "id": "ca7dca1e-2888-467f-91d5-7380456f6676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 58
        },
        {
            "id": "02ee422c-dd94-4a82-89cd-92c67859b694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 59
        },
        {
            "id": "830e933e-25e6-4860-8949-9c1930723d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 63
        },
        {
            "id": "0d9eea61-f6f6-435f-84a3-5f21cf3115aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 64
        },
        {
            "id": "7f726c14-c678-4fd3-b658-ed04010b8c62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 65
        },
        {
            "id": "8c56629d-cfbb-4bf1-b243-37720be62c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 66
        },
        {
            "id": "5264a49f-4ee2-4f36-9135-931ed874b226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 67
        },
        {
            "id": "1206b70c-3333-4513-a436-6fb9000e901e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 68
        },
        {
            "id": "bb2b2bc0-7af2-46c2-afc9-3ad55c5decd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 69
        },
        {
            "id": "f1da6725-750f-449f-902b-e26fe7401d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 70
        },
        {
            "id": "273b1863-0464-4eb8-96a9-6cc3443df089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 71
        },
        {
            "id": "0b3c0fab-46b8-49c3-8c5b-e1798c4f9938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 72
        },
        {
            "id": "2cb31188-9b23-40f2-b9a4-69160199fcc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 73
        },
        {
            "id": "6da433ee-0fe7-444c-a13f-c8bd26a0e570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 74
        },
        {
            "id": "d9469201-4d5a-4801-9c51-538acfde9460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 75
        },
        {
            "id": "4f08c38d-cfca-4d22-bcff-9729a1f9885c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 76
        },
        {
            "id": "6ecd5f2e-9f21-4fd0-89bd-83361100b4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 77
        },
        {
            "id": "33ff47b6-b932-416e-b582-5f3cb5c9b0d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 78
        },
        {
            "id": "07fe2ac7-fd4e-4959-9ee6-0ac939195926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 79
        },
        {
            "id": "bdd8a9d9-38c6-4811-b0b8-7052be986b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 80
        },
        {
            "id": "4799a7af-8190-488c-8fda-512146e5ccfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 81
        },
        {
            "id": "0c443709-f5ed-4f71-8306-3977d757886c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 82
        },
        {
            "id": "5f816ea3-ca50-40f9-b445-f0dd5b0a3f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 83
        },
        {
            "id": "05fe3dc0-debf-4fba-aea1-e6b7916e233e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 84
        },
        {
            "id": "83ed6364-c5da-4dac-bfad-2b8de42b1065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 85
        },
        {
            "id": "4cce3604-12ba-46c8-a712-16fcbb210f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "7416eb33-2092-4679-bea1-a1a31d3889a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "d072ecde-67dd-4510-8374-91c88bae4510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 88
        },
        {
            "id": "2deb2ec7-5579-40f4-af90-ce29f4e08bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 89
        },
        {
            "id": "f474742c-02a1-4ba1-9e62-31cc77218d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "8e104c75-2a79-41e9-8980-089dcf584ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 92
        },
        {
            "id": "479da380-c968-4c16-8583-517fd8daef25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 93
        },
        {
            "id": "c76c2192-017e-44e0-b428-7adabf61372e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 97
        },
        {
            "id": "b2cb66c7-7f37-4c7b-ac91-994fd2d35e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 98
        },
        {
            "id": "a04eb305-8496-465f-943d-bbd06d6f4ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 99
        },
        {
            "id": "15c8f480-011d-4bd2-8fd1-7abb5927d4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 100
        },
        {
            "id": "d1606525-d607-4784-a239-402ab32dbdc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 101
        },
        {
            "id": "bdec8310-aa73-4685-a162-fb3d8fc8ab5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 102
        },
        {
            "id": "5afb3f46-466b-4e55-861c-accac8a19ba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 103
        },
        {
            "id": "315e362a-893b-4d98-80de-fee2d3e7e7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 104
        },
        {
            "id": "6e10ac30-579a-4f1b-b0b8-d241e452cde4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 105
        },
        {
            "id": "bb87e4b2-8114-4b88-a271-420331c7f744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 106
        },
        {
            "id": "27af2034-d471-45d8-adc7-118601074e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 107
        },
        {
            "id": "d7f19fbc-610a-46ae-b5d3-1cf2d1a1e96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 108
        },
        {
            "id": "c965a5b9-a599-4e58-afe8-8c8ef5be4a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 109
        },
        {
            "id": "ef596f77-f802-4dbc-9490-dd3b8d2709c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 110
        },
        {
            "id": "6f556844-25d7-45b8-8332-f95511d2c5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 111
        },
        {
            "id": "10fa62a5-3c1a-4ac6-9968-17afd782447f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 112
        },
        {
            "id": "492be636-1741-44c8-90b0-7eed38defd39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 113
        },
        {
            "id": "f68cb1a2-783a-4dee-be9d-8c11dcb5a932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 114
        },
        {
            "id": "877c00e3-0cfc-4e05-9085-9fa30c86cbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 115
        },
        {
            "id": "052c86a3-3553-4c53-9c59-8adc1fad0a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 116
        },
        {
            "id": "3b3dd8c9-a4ea-4027-abad-cc8732a160e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 117
        },
        {
            "id": "b5eb3fef-f728-4b0f-96da-8878685ac8a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 118
        },
        {
            "id": "656717b6-5805-4d91-aa8a-607836a189be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "1cdce8ed-332c-485f-92ec-7d19a117f371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 120
        },
        {
            "id": "927d3f83-59fa-4453-8da4-bcce8f7842e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "492d657f-a30d-4cbd-99a6-b6027b9d9be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 122
        },
        {
            "id": "66013fd9-9060-4b22-8d2c-ccf0f2f54f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 173
        },
        {
            "id": "c41fa5b5-d797-4269-a46a-cdce5d8e2fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 894
        },
        {
            "id": "4c171632-d602-458a-ae15-bfdb2563e23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8208
        },
        {
            "id": "0c090681-3a5d-4ca6-8c71-6c7fa75e0c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 33
        },
        {
            "id": "0c909c63-4b57-422a-80bd-8df7f2bd464b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 34
        },
        {
            "id": "b9f67bb6-85da-400f-8c3d-f35c7c80bf4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 35
        },
        {
            "id": "c1e161ee-326f-4d09-975f-1df51af4d35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 39
        },
        {
            "id": "586ee86f-0e4d-446e-8310-959565773d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 40
        },
        {
            "id": "2ca3e1df-0b66-45b6-803c-e3abea1488b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 41
        },
        {
            "id": "23f35866-0722-4963-8d37-06097bd2ff97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 47
        },
        {
            "id": "01ff4864-b698-4a9f-bd24-a269a190305e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 58
        },
        {
            "id": "75adb77a-3255-4353-93a2-a3db85527b2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 59
        },
        {
            "id": "b5779539-029b-45b4-8701-a22ea3807f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 63
        },
        {
            "id": "0ac2b481-9383-4cd3-a8e6-d189d2ac5b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 64
        },
        {
            "id": "896c65ca-d65f-4a3e-9826-4539a39e30b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "a4e5dfd6-7846-4dc3-bed6-b1cd5a629d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 83
        },
        {
            "id": "ea7773e5-608f-4ddb-baba-389f1ed87f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 84
        },
        {
            "id": "5dc448a2-3bac-4dcc-972e-b94b7db176d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 86
        },
        {
            "id": "916e5870-bb80-45ca-9f2a-df6a61b2b423",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "b70c960a-03a8-4bd3-92d3-16b93944f400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 88
        },
        {
            "id": "3c2cb938-cd99-4858-bc54-b5baf93f8ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 89
        },
        {
            "id": "7980240c-41c9-4f04-94ab-99ac6aaf4043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 90
        },
        {
            "id": "e74c5842-d454-4b66-aa6b-3ba90946318d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 92
        },
        {
            "id": "83286b68-2e44-4c65-8d00-7fe5fcde5ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 93
        },
        {
            "id": "6bb0d4e7-3390-4c62-a76d-b85b5cf14845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 100
        },
        {
            "id": "df75b6f8-dd31-440c-8ed8-9b006b19a5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 103
        },
        {
            "id": "fe20bd07-52a9-4e30-85f8-0e440de17a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 105
        },
        {
            "id": "c5bc1d1e-5676-4b92-9e96-cf0b95c2e535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "7be74a57-bf5c-4eba-b4bc-8aab354745a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 113
        },
        {
            "id": "d0c0b8fd-080c-42ee-9750-caea3bcb4faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 115
        },
        {
            "id": "194dafc9-92a5-44b9-b9bf-711857e5d18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 118
        },
        {
            "id": "d3ae0bd7-710a-420b-8547-898389a385a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "8c999230-0fb2-4271-aec0-51dceeddcd04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 120
        },
        {
            "id": "7476e6c9-087b-4427-8ebf-b885834b9666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 121
        },
        {
            "id": "ec921c24-2302-41ff-9313-c1e64db9972f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 122
        },
        {
            "id": "0a7d1f08-0f5d-4f35-b85f-ba12286d479c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 894
        },
        {
            "id": "686b348b-5a10-4a47-96e0-dd73565b7d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 33
        },
        {
            "id": "f57b7c06-2b36-456e-ba4c-ee15e8270d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "a80e26b0-30a0-4cc6-9b78-0dc70994be72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 35
        },
        {
            "id": "048ee550-b65f-43d1-a428-f0732f5c477d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "e72f205e-6e3d-481f-8db6-0ee0aca99a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 40
        },
        {
            "id": "fe310cf9-27c0-42b0-bd00-6dba1149e485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 41
        },
        {
            "id": "fd8a32da-94de-4b96-a5f0-e9f3b2119066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 47
        },
        {
            "id": "074bba27-d8b8-4874-b64a-ff8b973d1e7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 58
        },
        {
            "id": "5ff255f9-73aa-4509-8bae-075f024e5e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 59
        },
        {
            "id": "596b17a8-204b-485e-8b83-608069823b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "c1479276-9c9f-4138-b163-3c2a079be8af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 64
        },
        {
            "id": "6f624d8a-1b8c-490a-91b2-df32cbaf35ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "a1b5e897-861c-4154-838b-0583af18d8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 66
        },
        {
            "id": "497362bb-4bf0-46d7-a7af-309f0d188cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 67
        },
        {
            "id": "05c9806e-8c9c-4b60-a40f-3ec72df99104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 68
        },
        {
            "id": "37c990ee-cbb3-430c-a41a-577cf308dd36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 69
        },
        {
            "id": "7ac8fcb9-8ea3-4d33-bd9c-b5452c171fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 70
        },
        {
            "id": "8354dbf5-4e2e-463e-ac50-5faf6e2dcc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 71
        },
        {
            "id": "2618f92f-f0c3-41ef-add6-3c3a0c13b979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 72
        },
        {
            "id": "25124140-5984-4d17-8a1b-36d01d1c2f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 73
        },
        {
            "id": "48087953-5b27-4392-9781-51543a764c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "e2725af7-817e-40cb-b6fc-7db35e592e76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 75
        },
        {
            "id": "d6ad93d8-bb19-41ef-a396-6626e81316ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 76
        },
        {
            "id": "f981cffa-5dc7-42c4-a08a-a5acd8d934b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 77
        },
        {
            "id": "6c804260-fe11-4fc4-8e42-9b69d3a921c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 78
        },
        {
            "id": "5770ec6f-960b-421f-9c0d-7aaad081fede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 79
        },
        {
            "id": "d5a75471-48ac-4b5c-be01-77022fa3c4ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 80
        },
        {
            "id": "dc0007c5-2b7b-4918-8479-2e4f12ab505f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 81
        },
        {
            "id": "12a22878-ec26-4b4b-ac40-556602af32b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 82
        },
        {
            "id": "5eaffc5e-d37f-4d54-bf95-dacb2e7f63de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 83
        },
        {
            "id": "1c734f4f-aee7-42d1-a92d-2590a8764150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "f3c84bdf-500d-4f2b-94d0-1936ebd12885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 85
        },
        {
            "id": "c9a04b5e-999e-4107-8f27-f2b4146dcf47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 86
        },
        {
            "id": "6dbfd242-0a89-4f07-8c2f-063d8529bbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "183d8a31-6581-4b7b-83a6-4f46fcc6cb67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 88
        },
        {
            "id": "d9214b1c-2bf0-4c56-9f7f-0f71dab1d0ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 89
        },
        {
            "id": "d3945e4a-2bea-4ea3-802a-8e494d6e25c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 90
        },
        {
            "id": "5040b22b-5c7b-4f03-9f5d-8962d49aa970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 92
        },
        {
            "id": "406edf4c-0b80-443c-a921-1b53e942c32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 93
        },
        {
            "id": "e3e4d3b3-f02a-433f-9f5b-f41c6b000888",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 98
        },
        {
            "id": "056b5120-41e8-4036-a71a-5fa1470fdb8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 100
        },
        {
            "id": "24aa3c3d-fe6c-432e-8016-bef3dcee75ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 102
        },
        {
            "id": "0624ddfb-ade5-4161-ac06-56838a6c2c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 103
        },
        {
            "id": "a934a96d-c417-47b5-bdec-41a0c2652522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 104
        },
        {
            "id": "42b56bbc-046e-4f49-94c1-35be6221bd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 105
        },
        {
            "id": "7e8be7b6-282d-4159-a186-11b549cb15b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "32650c03-ce70-4633-85eb-c0d67d0e35ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 107
        },
        {
            "id": "8f0d20f0-f0b9-49ff-ae84-ca660e9ba12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 108
        },
        {
            "id": "d676ea20-ebe9-49db-bdec-6a0609e12d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 113
        },
        {
            "id": "38391119-6129-45c0-9e21-35cac8fe9bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 115
        },
        {
            "id": "1f06c42d-dff4-47f2-af51-66bdb570f03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 116
        },
        {
            "id": "a042025d-7e13-47f5-9cad-94861902a488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 118
        },
        {
            "id": "fef820e5-b977-4b42-8c98-793cfea42741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "96bf9218-c1d5-4668-95b4-00d3502c586a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 120
        },
        {
            "id": "cd118394-7a9a-4997-9bdb-4b9f13779ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 121
        },
        {
            "id": "f8084cb2-7854-435d-a843-41674c9a214c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 122
        },
        {
            "id": "724534d1-a085-4265-9bb7-89abc4470dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 894
        },
        {
            "id": "458f22f0-03c8-46a6-8d61-77334d959299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 33
        },
        {
            "id": "c36e19e3-6c2b-4ed3-8c29-9bb3069e2a03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "2048b18d-d751-4f50-8baa-225759478d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 35
        },
        {
            "id": "12348df4-cd75-49aa-b861-7b50995eeb3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "a0c018d5-98ff-419e-8adb-94f1e6f74d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 40
        },
        {
            "id": "2f0130e2-f7c4-4a3b-bcb7-118021731000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 41
        },
        {
            "id": "2f1be600-40b6-4062-825b-006f85ee8e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 47
        },
        {
            "id": "a2520e77-fb8a-4059-8cf3-1adc00568903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "e4c656a0-f102-4e76-975e-7bf4ed80f256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "ec2bce36-87b6-4365-82f3-58567f111775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 63
        },
        {
            "id": "23e38a43-5238-4d7e-9767-4cd305f130d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 64
        },
        {
            "id": "a4c1229e-331f-4569-92ce-4573f7a2de8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 65
        },
        {
            "id": "a4ab6cb7-b670-48b4-941f-df2ce468df18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 66
        },
        {
            "id": "41654228-e393-42b0-9f06-8e502469a397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 67
        },
        {
            "id": "cead454b-c21a-4684-bdb2-bb9bc9e82bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 68
        },
        {
            "id": "4f47de73-d6a1-451d-b2f0-2e075a7f9012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 69
        },
        {
            "id": "5381919d-b7ee-4135-a82b-27f079a6f3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 70
        },
        {
            "id": "b1c62a84-10e9-4b44-9441-92c9a341e93f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 71
        },
        {
            "id": "4888c1dc-4efc-4e55-b377-5cd68087e98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 72
        },
        {
            "id": "14d28050-c208-4ffd-bd5f-aad0ee676c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 73
        },
        {
            "id": "2f80163b-f286-4532-8c8e-73f50190adea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "277a7885-477d-41b8-9006-0d1ca3369370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 75
        },
        {
            "id": "e2196a3f-1a04-43d0-ab2b-d30d132d8391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 76
        },
        {
            "id": "895129dd-1705-4e98-97e4-90a9e9135b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 77
        },
        {
            "id": "a1f741d8-6df2-4991-a6b9-8df9f2036373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 78
        },
        {
            "id": "6402b996-18f6-4d6d-b6b7-ca134996d00e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 79
        },
        {
            "id": "c9db0100-e6b2-460b-8766-fba3530dd541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 80
        },
        {
            "id": "84a48d95-6066-4d4d-b689-0e07ba6cf1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 81
        },
        {
            "id": "2d092133-2de2-4f52-adbb-b5b965f5b278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 82
        },
        {
            "id": "2027c167-1c43-49e4-b282-4824575dad75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 83
        },
        {
            "id": "f5f3dbdf-436d-4066-b8c2-29062092811c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "709de9f5-cf6a-4730-bd16-aba6802a46f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 85
        },
        {
            "id": "5abb303e-65b1-4a34-8c6f-4345cbdd9308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 86
        },
        {
            "id": "da7bb152-d672-40a8-bc31-13142fc1393d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "58d5137c-e1e0-496a-98d3-6bc9395dad47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 88
        },
        {
            "id": "8d7a606f-ea43-416a-a96b-59b9ceb5f919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 89
        },
        {
            "id": "d50c0699-25dd-4dc5-bc39-06df2450ad3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 90
        },
        {
            "id": "e65742e1-0646-4b0d-a000-20c9d51a5664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 92
        },
        {
            "id": "6a73e68e-cbe9-4545-b3f1-edfce4716840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 93
        },
        {
            "id": "702ecb88-7d89-43f1-9902-dd007bf655fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 97
        },
        {
            "id": "549e9b2d-214e-45e6-a31f-f91e59860144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 98
        },
        {
            "id": "f33a7974-7ec3-4cdb-805d-97f73735ffe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 99
        },
        {
            "id": "9e11985f-5a10-49d0-bb9d-5b3c1b0c9075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 100
        },
        {
            "id": "2e36fc32-d875-4855-b532-769a55e6ab81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 101
        },
        {
            "id": "a3c0c24a-3c04-486f-9ca5-12edc3301050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 102
        },
        {
            "id": "ee3f8af1-786b-4fde-b82b-2052a44c6e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 103
        },
        {
            "id": "78b38b3c-a853-4a2e-984d-ed486940e93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 104
        },
        {
            "id": "1665501c-902b-4df3-b9a6-fa68d32df0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 105
        },
        {
            "id": "fb9d1db3-5fe7-460a-8a6d-81d09cad9813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "7828d99c-f018-4ed8-878c-88f37e88d980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 107
        },
        {
            "id": "ef0594ab-ecda-4d07-a4d1-11b9785eb9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 108
        },
        {
            "id": "2074108d-e7b3-4f9d-97f2-550ff0d6d96c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 109
        },
        {
            "id": "ddea26fa-3397-4ce1-86c4-6101528e8580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 110
        },
        {
            "id": "6f1b22b6-7686-4cb3-b66f-241ed87c4883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 111
        },
        {
            "id": "1d341048-b93c-4651-8229-ce83b56bd726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 112
        },
        {
            "id": "e4dc441b-6c6f-4d69-97e0-0a21bbad759d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 113
        },
        {
            "id": "e1bdd5fa-34ef-455c-8ee1-47ee197d6eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 114
        },
        {
            "id": "43631887-b7ce-40fa-949a-c980d5535f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 115
        },
        {
            "id": "167c4e48-2cdc-427c-81d6-7b0edbab3b62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 116
        },
        {
            "id": "f37ff6a6-6559-4c18-b734-b9edb3f98398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 117
        },
        {
            "id": "95a4abc5-4737-4634-bf13-61299bb6ee4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 118
        },
        {
            "id": "6e3a5654-ed11-412b-a7ed-bc8176c4aaf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "b6d436a6-beb8-4c7a-9e94-fd89109cc016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 120
        },
        {
            "id": "2b3939b5-1eeb-4894-9bfb-541c531d9dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 121
        },
        {
            "id": "7eb2a8e1-72db-4369-93c2-1a078c07870b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 122
        },
        {
            "id": "48d1efdf-8eab-4e9c-b647-3acc46a1b09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 894
        },
        {
            "id": "c0c90d86-4892-400f-87c7-b1d0781f6493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 33
        },
        {
            "id": "cbe9f41e-dc10-4e7c-b2cf-20647528a514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 34
        },
        {
            "id": "7276b92c-2cad-4493-96b3-5fb6c2fb01f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 35
        },
        {
            "id": "84b99782-e5f4-420d-94f3-1e2fbad18e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 39
        },
        {
            "id": "b817030b-8d47-47f7-be5a-badc7f0ec283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 40
        },
        {
            "id": "d6addf3c-dcb6-4749-891f-091d2f68bcd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 41
        },
        {
            "id": "8892ab58-db9d-4aa1-9fd3-82d01bbeabd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 47
        },
        {
            "id": "1b1ea818-5d24-4613-bf91-7ba16a121f9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "7ab7bb32-bddf-4571-ae6f-d2e3fae92f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "6800880f-8963-4f3a-bf7e-4c7dd9457848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "ca33aeea-9eb0-4190-b7a4-c61438ca7a18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 64
        },
        {
            "id": "c39e721b-63d7-43f3-afdd-017ff311a69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 65
        },
        {
            "id": "75e9cad6-48c0-4c7d-8921-9742a0ef910c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 66
        },
        {
            "id": "ff399213-ece7-4051-b8bf-ed4bd23f4232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 67
        },
        {
            "id": "0bda0475-9fb1-4bc3-9149-20ac3fd1a6c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 68
        },
        {
            "id": "a0a9c460-a15f-46b9-83a1-83239786e7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 69
        },
        {
            "id": "b476945d-8411-48b8-bc5f-7986b1cbaffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 70
        },
        {
            "id": "a29ac3af-d309-4c1c-8e54-495df8d7c34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 71
        },
        {
            "id": "bd8ba5e2-494c-45fd-a432-282b25a5df8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 72
        },
        {
            "id": "94be03c7-5a9d-4614-bde1-fc7e7b45db31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 73
        },
        {
            "id": "3960bfc8-a43e-4787-b063-69131393fb75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "2cfa47d0-4dd1-457c-9b71-6a31095623ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 75
        },
        {
            "id": "a588e1ec-da23-4e52-b850-69c25416fc8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 76
        },
        {
            "id": "c7d4d2e6-a7df-4bdc-8e3c-eca35374c57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 77
        },
        {
            "id": "f28b37fa-1b9b-42e2-9536-7e063a979219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 78
        },
        {
            "id": "3097308a-ef7b-4668-a0fb-0b16959b93df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 79
        },
        {
            "id": "f8b3de20-b68c-42e7-9508-23c0eccaad90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 80
        },
        {
            "id": "92115169-b00c-43a2-9c97-6a0f12e7fe2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 81
        },
        {
            "id": "c10b9b79-6b84-4c29-9428-a470907706b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 82
        },
        {
            "id": "36498e4e-6f96-405e-abec-f1aefa9a69b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 83
        },
        {
            "id": "513ee4bb-cd1c-40f7-9cec-f95d01e31ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 84
        },
        {
            "id": "6e05ea66-1596-45d1-8cfd-e93c3d1f148c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 85
        },
        {
            "id": "e619047b-7451-47ca-aae5-ba79b72076b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 86
        },
        {
            "id": "7242bbbd-b890-4531-9724-b1a7bd7b8dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "b748e10b-a605-4c60-9161-967e218af91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "b7da1146-792e-4b55-9c00-7f361d3be90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "35df5823-94d1-41e5-939c-a853ef6668d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 90
        },
        {
            "id": "1fd7ca5f-6aa5-427a-a730-73b74977f081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 92
        },
        {
            "id": "89f11bf4-1f5f-4fcf-ac1c-511dad1049ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 93
        },
        {
            "id": "b0771ab6-eec0-4f4f-b99b-4e1bccb95adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 97
        },
        {
            "id": "906be487-5015-4b71-b0f4-f6daf9361aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 98
        },
        {
            "id": "b9f317ae-365e-477e-95d1-47824caa592a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 99
        },
        {
            "id": "74eb648c-25a4-4b4d-9f62-8f23b254ffff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 100
        },
        {
            "id": "13aef250-d03e-4aaa-8f9a-fe3f7621b780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 101
        },
        {
            "id": "41125ddc-e64c-4727-a8eb-0b91e0c7892a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 102
        },
        {
            "id": "ebaa3696-b615-4aad-acde-ce108a1d1eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 103
        },
        {
            "id": "748a7a55-a69f-427e-b384-14711eab294f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 104
        },
        {
            "id": "8e0566f5-7749-4146-bced-f820e2ca0e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 105
        },
        {
            "id": "6edc16ba-d5e6-475a-8147-406e7f8e3e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "e64b6a0f-9806-4165-9c1d-7faa288cd136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 107
        },
        {
            "id": "ee691563-02b4-4f1f-8436-313c6c2a2bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 108
        },
        {
            "id": "48f11f46-2df3-45f2-b8c0-b822a3e1a227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 109
        },
        {
            "id": "e96e07c7-886b-413f-aede-869dfc1580ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 110
        },
        {
            "id": "2681434c-b035-412d-a50a-acf284e5df8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 111
        },
        {
            "id": "e52c9e94-c8ca-492d-94a1-12793592f857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 112
        },
        {
            "id": "2bfb6fdd-9857-4cc0-9197-951a71c7bc05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 113
        },
        {
            "id": "c4b07c11-7e1e-4c70-a118-901f1552362b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 114
        },
        {
            "id": "4443f7f5-4165-4176-99b2-7ce149dd8cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 115
        },
        {
            "id": "7e49526f-383c-460d-b02a-8cee7f710922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 116
        },
        {
            "id": "f0fc662e-1cad-4902-a560-0a506fc6f9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 117
        },
        {
            "id": "4caf8575-5f52-4ec4-9dca-7a179bda47b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 118
        },
        {
            "id": "ed3795c5-1094-44a0-ba1f-b6a046daf4cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "e810588a-8815-4533-b772-992b8efe4420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "55bd811b-5eb3-4de7-9961-275f327e67b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 121
        },
        {
            "id": "ececc9f2-7197-406c-afcd-db4df0750e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 122
        },
        {
            "id": "421296b2-9f5b-4361-828e-8ddc6ba04487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 894
        },
        {
            "id": "6622fa36-b36b-4169-a452-94d1e285f062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 33
        },
        {
            "id": "25c8168f-f8b2-47f4-aaca-2135736ff2ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 34
        },
        {
            "id": "9bdafe5b-a303-41a3-941d-5c58f5c820f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 35
        },
        {
            "id": "b0d5c251-6261-4d55-83a8-96443d3b8060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 39
        },
        {
            "id": "42c191bd-c306-48f9-bf30-54fd5781b95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 40
        },
        {
            "id": "9dc35af5-805d-4da7-aefe-2a358efbf23d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 41
        },
        {
            "id": "5bbea231-1a94-45f5-af66-1ccc9cbc9f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "d9b89436-8210-4e4c-96ad-461421a8b964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "f0705f8b-c5c9-405c-b36a-c73092b9228a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 59
        },
        {
            "id": "a80bda18-9673-4b01-8680-5c29ef248bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 63
        },
        {
            "id": "db17d936-18af-41fe-a90f-6eafae45d365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 64
        },
        {
            "id": "b7b52d31-c8a9-4fe4-afd3-635c986bcdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 74
        },
        {
            "id": "ce189faa-579a-4e45-be7f-2d9555c3d01d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 83
        },
        {
            "id": "98f1d40f-d824-4a72-a625-361b7295b005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 84
        },
        {
            "id": "6bd5dc87-37bc-4d47-b25f-89148cd41a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 86
        },
        {
            "id": "279a2e77-9f11-4466-b0f8-1f49e013c0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 87
        },
        {
            "id": "fd5dbafe-4d58-491d-924d-61810dbc79fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 88
        },
        {
            "id": "fcf0ff32-4318-487e-b713-285acde54bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 89
        },
        {
            "id": "af536cba-5901-4aac-979b-81fcf54e1dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 90
        },
        {
            "id": "d12966bf-00d2-47b0-9073-e97a9bd975ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 92
        },
        {
            "id": "025349f6-2607-4539-96e9-426f56a301c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 93
        },
        {
            "id": "808f6278-4e95-4bd8-ae81-bdecec9c6c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "ddeadb37-0e89-4a2e-bcc1-a3f8a6a71d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "f7059f31-4645-4921-9eea-d16220f34cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 105
        },
        {
            "id": "d752f383-6d17-4b51-b0bc-558689ae7ee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 106
        },
        {
            "id": "4400d4cf-4bd9-4705-8b1c-878548d059bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 113
        },
        {
            "id": "1bdc45dc-e649-4071-be2e-8d7bd47d5bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 115
        },
        {
            "id": "47f320e7-8351-49d3-a678-208ef78ad1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 118
        },
        {
            "id": "6831214a-b161-4a15-b895-5219b8602890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 119
        },
        {
            "id": "3240cefe-fc9c-488a-a5a3-de5ba84ec171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 120
        },
        {
            "id": "286ba796-dbd8-43ee-9411-af3610ad8338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 121
        },
        {
            "id": "30d7a1d7-0517-4668-84b1-ccb9a2caaf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 122
        },
        {
            "id": "e8607650-6cd6-48a2-bd87-5df6cb8796f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 894
        },
        {
            "id": "3e884ad7-7fc7-4773-aa20-5b6990e5d652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 33
        },
        {
            "id": "e49092bd-6bce-4f45-930b-52d694862bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 34
        },
        {
            "id": "1c4d46e2-54e3-46d5-b5c7-45cf54fa43cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 35
        },
        {
            "id": "d9812e4c-cf8e-4cab-8cfc-77b05a5e2e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 39
        },
        {
            "id": "784032eb-1e83-4213-866e-c4cc3efb2432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 40
        },
        {
            "id": "d0c58c6e-a1da-480b-8807-b6a76cfd5862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 41
        },
        {
            "id": "e7b5d38d-219c-479a-bd7c-6b8faa041ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 47
        },
        {
            "id": "c95b5731-72ad-49f7-b30d-b394598a2da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 58
        },
        {
            "id": "e6e8c480-0995-45de-91f0-b2605a786a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 59
        },
        {
            "id": "c71c8ea8-a62d-4c7e-a95b-3fc8d2b64684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 63
        },
        {
            "id": "f45b05a8-e9a5-4483-8088-07de827f0518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 64
        },
        {
            "id": "c060afb3-c9d0-48b0-9aa2-4317d3e83794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "f0f1df71-3ea5-458f-b63f-72defab7fa77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 83
        },
        {
            "id": "0b747307-7902-484e-ac22-69ab3209a0f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "ffc6a71b-b1a0-4870-918a-3bb273dc8c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 86
        },
        {
            "id": "617eeaa9-3b4f-4c48-8f5b-6ade33ea7119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "6db389c2-b374-4765-9a31-8db098309ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 88
        },
        {
            "id": "25e0eadd-cb16-443d-8020-31601767ea87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 89
        },
        {
            "id": "fb319e19-2378-4c0e-9801-eb35e624a6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 90
        },
        {
            "id": "58f8801e-6129-47b0-a0ea-311c0372ea25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 92
        },
        {
            "id": "57f1bab4-2d07-44fe-b1bd-351b0149bd79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 93
        },
        {
            "id": "1fedf19d-85b0-414d-8bf9-6aba4cdf73d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 100
        },
        {
            "id": "23299698-814b-4046-a2c9-e7113abb7e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 103
        },
        {
            "id": "7ff64d98-1034-44e7-91ce-1f20e9d9732a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 105
        },
        {
            "id": "9a68b6e0-6737-4c71-b09a-fcba98a3eb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "8f665394-d0f1-4b51-985f-556b87a1e073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 113
        },
        {
            "id": "47d3c2ac-f2fe-453b-a038-938a44d81faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 115
        },
        {
            "id": "0f1f0722-907e-4e49-80ad-8aa668968edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 118
        },
        {
            "id": "534fd437-7294-4bea-841a-c7aca983d636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "8e070b90-a955-44be-afda-6c7a8660e05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 120
        },
        {
            "id": "c30d6fee-a315-4101-b2e7-300ae8ddb31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 121
        },
        {
            "id": "cba6d8c9-c3bf-4fab-98a6-df219780a3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 122
        },
        {
            "id": "31a3db4a-9407-4074-bc5a-0fa4194c5ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 894
        },
        {
            "id": "dd4ab185-8090-41df-8527-a64fcaf62d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 33
        },
        {
            "id": "23a3b64f-0247-4b53-8947-1ae3e0421b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 34
        },
        {
            "id": "3f8fe6d2-c960-413a-9cf3-e6ce06a83fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 35
        },
        {
            "id": "914fbfa9-cc83-4b9c-9166-34ffa910df0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 39
        },
        {
            "id": "d0806292-5ccd-419e-8f27-3d47e7e7197a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 40
        },
        {
            "id": "56526a4d-00b8-4b9d-921d-d3266d225dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 41
        },
        {
            "id": "c3634da5-a010-4e51-80d4-68e6a8cb4dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 47
        },
        {
            "id": "f796dae1-ca5b-4766-ba0a-c4ce6d36a448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 58
        },
        {
            "id": "1399a21b-1f25-434a-8601-7bdd3f8587a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 59
        },
        {
            "id": "1c3ca979-1a19-4ba0-9d04-bbb6e473f4b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 63
        },
        {
            "id": "2f278afb-9ffc-4574-8557-3aa42088da8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 64
        },
        {
            "id": "c0409400-ec68-4229-b2a5-14beb931ae60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "17f97066-edaf-44c2-a8c1-2d57ab5302b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 83
        },
        {
            "id": "52095d93-ab37-4f7d-b813-120c906264ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 84
        },
        {
            "id": "9a0401c0-c330-4cff-9098-ce9237065338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 86
        },
        {
            "id": "cf4eac74-9295-4356-99bb-256f5f51db76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "5e3b45b0-36a2-48b4-bed7-1e1c57837b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 88
        },
        {
            "id": "3c0a2595-3f6e-48b6-9763-741b47a6734d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 89
        },
        {
            "id": "00e4e909-3cdc-4a2b-8d1f-6bc72333d599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 90
        },
        {
            "id": "59539dcd-9479-4617-83b6-55e96b9b7457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 92
        },
        {
            "id": "c6697d7a-31d5-40d1-9c46-f6e4eb3d77e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 93
        },
        {
            "id": "04b0edee-5262-4106-aa54-c86be7bf1c82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 100
        },
        {
            "id": "2997d523-d683-4c6d-b287-a53d57ca4b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 103
        },
        {
            "id": "c3fa5086-81cc-469f-9ee1-eaf516b474e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 105
        },
        {
            "id": "ffc18b3b-2067-42ce-8acb-3fe67ac4308b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "fc184682-41ef-4727-92cb-5f518a66de56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 113
        },
        {
            "id": "1c4b5e4d-ee61-463f-b71e-89119103a4b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 115
        },
        {
            "id": "f0d99733-3ea3-40e8-951c-d6cddb27ea59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 118
        },
        {
            "id": "f8b21990-4d27-4a4e-becf-da9baa482e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 119
        },
        {
            "id": "732a62e7-11b9-4ff0-918f-83123086481d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 120
        },
        {
            "id": "82360c8b-64e9-454f-bf3e-afba902627ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 121
        },
        {
            "id": "6e4cc959-9dc0-420c-8f43-ee4c9f40c131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 122
        },
        {
            "id": "b1b835b0-e78d-4701-921c-80e6f8c7091f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 894
        },
        {
            "id": "88eee93b-7d43-480a-81ef-1c7a60e04ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 33
        },
        {
            "id": "90e29c6a-152c-4606-99eb-dc4c74b99cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 35
        },
        {
            "id": "0a79e0ab-b95c-4f25-b25c-afc56a0e587f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 40
        },
        {
            "id": "e290a7b7-1048-4e52-bdf6-166110ef3ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 41
        },
        {
            "id": "0aceb187-dfb0-49f4-a9c0-75e5a1456438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "b5bcd267-9428-4c93-b0b7-0811c5f3e2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 47
        },
        {
            "id": "626ba4ba-4303-437e-80d3-7e3dc70d1cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 49
        },
        {
            "id": "1c6d3769-55a7-4c1f-a7dd-b37e46dfa951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 50
        },
        {
            "id": "37813df4-cf95-4ef8-b6d6-0923db0ce2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 51
        },
        {
            "id": "149495df-d054-461a-b499-82ad4867b45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "ab0359ac-2eab-44bd-8768-289703a1db22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 53
        },
        {
            "id": "b1d44501-32e2-4c25-9d20-52b29753f443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 55
        },
        {
            "id": "f9113b05-065f-4713-82d6-b99c37c3a044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 57
        },
        {
            "id": "f65585f4-99ec-445f-9e97-f62640f82b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 58
        },
        {
            "id": "bfb699f4-b34c-4434-a9c0-24d073c1c7fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 59
        },
        {
            "id": "a69765f6-2911-411b-9b18-965c13b1c20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 63
        },
        {
            "id": "c1374885-7eab-4a51-9192-0ae03f65146d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "e7afd021-1388-4395-bf1c-b04733f6b998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 83
        },
        {
            "id": "1d95b1b0-8244-4c10-a744-d00d0191cb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "d78b2927-c433-45d0-a690-49ad40800549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 86
        },
        {
            "id": "5d691f98-1e2c-4654-a4e6-601b16dcb494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "d2b060c1-b80e-466f-bc08-e1692d0f9fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 88
        },
        {
            "id": "23426e7c-a6be-49d7-bff3-160cae653058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 89
        },
        {
            "id": "f027c690-ff10-48bb-a1a5-ae68075cf35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 90
        },
        {
            "id": "4ded07e2-faf6-4a14-b69a-f611ef2c314f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 92
        },
        {
            "id": "bb875759-6835-46ee-a246-b7bfc7242fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 93
        },
        {
            "id": "5c033301-f23b-4141-b3fb-401e1194ea8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 103
        },
        {
            "id": "739ce7e0-c49e-476f-9516-0720c1e2d19e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 105
        },
        {
            "id": "386d9155-4ebb-4c57-b86b-4b7cf16c89f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 106
        },
        {
            "id": "2c7b6f94-f8c5-45db-b227-707e32b6cf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 115
        },
        {
            "id": "f0f6314d-28b9-481b-ae93-0d0f0aad5e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 118
        },
        {
            "id": "191c8cc1-f060-408a-82f1-3a9e924dd1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 119
        },
        {
            "id": "fcd52131-20d0-4a0b-af75-52fea27c74e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 120
        },
        {
            "id": "7f583fd7-4eaf-417f-9b0f-f4621ad1259e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 121
        },
        {
            "id": "dedc9f6d-3dfe-4b44-8694-9bde276f59a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 122
        },
        {
            "id": "6f5bb99a-8db6-4f6a-88fd-401d47432cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 173
        },
        {
            "id": "55c47588-c115-47e3-bb44-7c8d85b5cccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 894
        },
        {
            "id": "f93f41c6-ad0c-4177-ae7b-11c7e89b196a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8208
        },
        {
            "id": "6a8f667b-57d8-4c36-8e9f-1a081e8b281e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 33
        },
        {
            "id": "fc5a2ad2-43cb-4f24-9ac6-ad47017576f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 34
        },
        {
            "id": "8da44353-aa07-4fa8-8675-01945bc12241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 35
        },
        {
            "id": "99ed488c-037c-4fc4-9eb6-5e88173b9d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 39
        },
        {
            "id": "fc4328eb-bab7-46b1-8253-6c536e4288a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 40
        },
        {
            "id": "2563aa54-1466-4770-aa98-7e1f366c9fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 41
        },
        {
            "id": "df549481-9a85-4cf9-be5c-b8c2bcd8159d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "4dda9af1-63e0-4e1c-b10b-9df90a210860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 47
        },
        {
            "id": "201f5b7c-b219-4768-b88a-e8568dbb76b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 49
        },
        {
            "id": "6cc9e1e9-05e0-4395-8274-54c198865458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 50
        },
        {
            "id": "87678028-49e5-4159-a901-dbd7d37ed82a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 51
        },
        {
            "id": "ee8b48ca-0dbd-43ed-be77-ddbafcc2531b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "37514df8-ebf0-4c1d-8df3-12c5bd978216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 53
        },
        {
            "id": "052a1544-dad6-4b02-ab27-9eb285d95899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 55
        },
        {
            "id": "3e3e3771-1d52-49d0-b927-cd9445435f7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 56
        },
        {
            "id": "348b7ed5-9b1b-423c-8269-0545f4bad21d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 57
        },
        {
            "id": "2a3e7270-1899-4832-b5bb-3ebca64e498d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 58
        },
        {
            "id": "a3c70a19-502b-414c-937c-9c9732a6601a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 59
        },
        {
            "id": "0c30dd53-23d3-4f2a-b17e-289df9541082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "98e9e157-2f62-4b6e-949a-c8ac499b61d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 64
        },
        {
            "id": "a974bf02-9c8a-4801-ba92-dc29eee59766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 74
        },
        {
            "id": "737949e2-ef12-4ac0-b8ac-974077c4aafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 83
        },
        {
            "id": "ff9efa32-06ec-4a88-88a5-831ba52e7d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "8954ed8d-f4c7-4a26-b2b4-1684f79912ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 86
        },
        {
            "id": "b1d9ed2d-ea2b-4603-912b-e822f2e48a10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 87
        },
        {
            "id": "db8e9224-3862-47e8-b151-5f85b0f85529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 88
        },
        {
            "id": "a653b693-ff4b-4138-ae71-e50726678550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "ab0b59f0-2b18-4527-9cae-2e195d39ad79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 90
        },
        {
            "id": "67ed56c2-c47e-4aa1-aa57-87a153a4b410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "05b28946-86b5-4e02-aee5-a8c07fdd1662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 93
        },
        {
            "id": "24a204fc-9804-477c-8172-ef50fea984c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 103
        },
        {
            "id": "abd0f0d6-7a70-4075-a4f9-82a46c051248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 105
        },
        {
            "id": "72aa2446-710e-4e5f-89ad-6a833180bf4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 106
        },
        {
            "id": "ee89c504-6e93-471a-8e2d-7f402da6c53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 113
        },
        {
            "id": "64adaf41-24bd-4195-98b5-eb8db79e1a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 115
        },
        {
            "id": "9aaa701d-0b5e-44b1-bc96-e5f68cc7ce53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 118
        },
        {
            "id": "b0f4271e-76ee-47ec-9987-c2dfbb330ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 119
        },
        {
            "id": "fbb15721-002c-4d28-8466-3701c811d228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 120
        },
        {
            "id": "35c72d24-4b30-4fce-bf75-313fe0ddfe28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 121
        },
        {
            "id": "f51f58f1-8b5c-4560-a9d4-a22484d5b06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 122
        },
        {
            "id": "6f6607c2-a345-4aaf-8862-833ebc6273e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 173
        },
        {
            "id": "77426e61-1e33-4ae6-a158-9dd1c75fa3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 894
        },
        {
            "id": "473ee217-afc5-4f6f-a3e4-89abd9609f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8208
        },
        {
            "id": "d42b0dcc-0c66-4852-a9f0-2d15d51bb57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 33
        },
        {
            "id": "ebbac46d-aadd-4a3e-9bdd-48bb5cc50523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 35
        },
        {
            "id": "28619fad-9cc8-49f1-b0c1-56b16b5ac70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 40
        },
        {
            "id": "34f91eec-1eae-402c-804c-f8081b643b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 41
        },
        {
            "id": "4d2dbc26-a450-4176-87c7-d1188154b134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 44
        },
        {
            "id": "1db3face-f7cd-454d-b0f8-0c69d1c17462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 45
        },
        {
            "id": "3f416a46-08ed-452f-80da-f79bb50a0a13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 46
        },
        {
            "id": "553c8217-f266-4305-b3ee-39ffa2f26d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 47
        },
        {
            "id": "03875a77-137d-4ba4-989b-907e290256f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 49
        },
        {
            "id": "79ace04c-ff9f-47af-ad6e-af5f4b054100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 50
        },
        {
            "id": "bd0dde75-af73-4a42-ab90-af7483bc0a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 51
        },
        {
            "id": "34341b95-828b-4e6a-941a-3196e49aee31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "b5d7996a-50f5-435f-8bec-8a78feed78c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 53
        },
        {
            "id": "6725d2b7-87cf-4515-a5fc-af8e31edd457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 55
        },
        {
            "id": "9d277c3c-f58e-4c9a-9c18-c091dda0c8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 56
        },
        {
            "id": "45dc033c-0e33-4564-a2ff-8f8bae41154c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 57
        },
        {
            "id": "998cbcee-6eeb-4fdb-8214-352eed7b34e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 58
        },
        {
            "id": "60278a2f-56d3-4d01-a80c-a9b694fed6d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 59
        },
        {
            "id": "29776be2-4990-4f5a-92ee-8d5e4a850ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 63
        },
        {
            "id": "c1e4cae3-c86e-4fa8-b975-c6f8ff08e20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 64
        },
        {
            "id": "58687d8f-a9bc-4eed-9a8b-9fe230a5dca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 74
        },
        {
            "id": "ba02ba09-95f6-481a-a1b2-36c919e39415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 83
        },
        {
            "id": "00bcf059-9731-463a-b1d1-31ce87910ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 84
        },
        {
            "id": "35941214-48b4-4115-832d-957c4b30d5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 86
        },
        {
            "id": "f59dd2c8-c192-4b71-b686-c7fccd708a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 87
        },
        {
            "id": "143caf7e-faf1-40fb-a862-b806c23c4178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 88
        },
        {
            "id": "30a60d19-7aab-4f0f-bd68-3497e8d725b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 89
        },
        {
            "id": "7e83f7bd-fd36-4db7-89c5-30191bcc1e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 90
        },
        {
            "id": "c20cfc56-0da7-4294-83ed-4604416fd3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 92
        },
        {
            "id": "d639b768-eb8b-44e2-9fd0-b7d0f3f0c3fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 93
        },
        {
            "id": "6cdea538-5c14-4b5b-93b0-b5f2d0fa20ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 97
        },
        {
            "id": "4879ca36-3676-4daa-a26f-7ec332113178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 99
        },
        {
            "id": "2c257666-5c1c-4589-b83b-d13fcf3b3637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "dd2aa446-eb52-4df5-b9a0-ddd4a72e3e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 101
        },
        {
            "id": "b028e72d-06c9-4058-aada-4a3f897bea02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 103
        },
        {
            "id": "d2b27b6c-53f1-4eaf-aef6-de5003771fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 105
        },
        {
            "id": "6d268ee0-f0bb-4ebb-8451-740d49d1fe9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 106
        },
        {
            "id": "f895c7dd-72d7-4801-b654-e93e23fb263f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 109
        },
        {
            "id": "ff324ee6-9386-4f1a-82b4-598155a25648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 110
        },
        {
            "id": "4a7c6f81-4dd0-45b5-a3eb-020b88c7445d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 111
        },
        {
            "id": "cfba18a0-c197-44e3-b345-1ff2ed7d9163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 112
        },
        {
            "id": "0ffa1ef6-2b4e-4d98-958b-27676cc616b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 113
        },
        {
            "id": "a0b72512-6015-42d9-b076-58f667ec7ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 114
        },
        {
            "id": "5d4a5bc2-93e6-4de5-87f9-d80b0a45ed7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 115
        },
        {
            "id": "119e171e-8705-495f-a5e7-ab0100482189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 117
        },
        {
            "id": "342d2909-8c8a-460e-8dac-1c54f0d504a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 118
        },
        {
            "id": "9378edb0-0b0e-462f-be1c-1e39d0f81c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 119
        },
        {
            "id": "23649903-add1-46dd-a3b3-36eabd0f4a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 120
        },
        {
            "id": "59884ec6-8c0a-4044-9307-5aaefa1613cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 121
        },
        {
            "id": "0a9b1575-dac0-4ace-8100-b77131658cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 122
        },
        {
            "id": "2e3f273a-039a-43b4-990f-2d5add985eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 173
        },
        {
            "id": "a1fa6bb1-3e82-48c1-acf0-827c66339c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 894
        },
        {
            "id": "1d2001b6-b02f-439a-bfc8-28bce4309105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 8208
        },
        {
            "id": "cf762b12-3a5c-4011-b8d0-836bf4cd4ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 33
        },
        {
            "id": "c3165d51-931c-4f6b-a4cd-bd14f1ad4cdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 35
        },
        {
            "id": "b33bb63a-1e97-4f29-ada7-8bd75f920c5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 40
        },
        {
            "id": "2005ee51-2063-4ea2-8301-ce308c5b5ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 41
        },
        {
            "id": "df344389-3e27-47ac-ba48-f3ea6fab3287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 45
        },
        {
            "id": "91bbd50e-2a30-417c-a9df-8ef26a6d5d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "630d1e99-0b31-498f-a2a1-bd861c69c314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "d8b4c43c-0de0-442d-91f1-b4d4b6a56d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 50
        },
        {
            "id": "938fae20-74d9-4d39-bb7d-87cb1fbd95db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "33c42ff3-b45d-4656-97da-e630f67a2cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 52
        },
        {
            "id": "2ceedecf-d7f6-4134-8248-202f03954c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 53
        },
        {
            "id": "1b195b3e-05bb-442c-b383-0c251908c995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 55
        },
        {
            "id": "8c67a358-6796-4f39-bf19-79d154fd19d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 56
        },
        {
            "id": "b8acda55-210b-4d55-be8e-3f3761288eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 57
        },
        {
            "id": "e9132404-e03f-4df5-b907-b8f4fbcf3ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 58
        },
        {
            "id": "82e087e1-c0fa-442d-94f6-2b22331692a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 59
        },
        {
            "id": "4c184037-4a08-4269-85d8-5e660e90695f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 63
        },
        {
            "id": "9736bcb5-57c6-4049-98ec-90fdb97bf6ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 64
        },
        {
            "id": "e2c33966-8c2c-4274-8118-a9bdf0cd2ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "01e897ce-1a79-4459-af38-65d55b5d7d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 83
        },
        {
            "id": "322fcd72-4743-4f4b-8ab5-338e3e3663bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "41dace5b-6e54-4913-b3c7-33c981c99775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 86
        },
        {
            "id": "074547c9-a86a-46ce-98bb-aacb78792e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "59ce3208-db75-4e72-967a-eeb7cb801314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 88
        },
        {
            "id": "2ae08826-9157-400d-8b28-bb1be5016f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 89
        },
        {
            "id": "92396ef9-eeac-45d8-92ad-d8b3861e93c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 90
        },
        {
            "id": "58ee4cce-f26e-4da6-a181-ebe268f20d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "3dba1780-7d67-4f9c-a574-7bee02411d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 93
        },
        {
            "id": "c49f5606-7cb6-44d8-85ff-4ae7747ca57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 97
        },
        {
            "id": "e078182e-c9e6-4108-9365-03c57b3576df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 99
        },
        {
            "id": "60ff1a65-bb0a-40a0-a27f-33cffbf608e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 100
        },
        {
            "id": "1c8c26df-4748-48e8-894c-47d11ac4a940",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 103
        },
        {
            "id": "8401bff2-b3d0-407c-bbde-5d47e9f87377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 105
        },
        {
            "id": "c4964e5b-52c2-46a4-a31a-80eff2563949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "448edc35-1524-426e-9ca2-5a016a1855e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 111
        },
        {
            "id": "fdf44990-78c6-44d6-ba69-7df5f1f5e78b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 113
        },
        {
            "id": "faa701eb-bca2-4590-ad49-d23801a45ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 114
        },
        {
            "id": "82969b3e-eec6-4ca6-9c7b-b040afeadf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 115
        },
        {
            "id": "d25573c6-7c9e-4a3d-98f9-028ab5a155db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 117
        },
        {
            "id": "fd986de5-fb93-4cd2-be66-d88c8d4aedd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 118
        },
        {
            "id": "a307480b-01d9-4f7c-b9dc-5f942941c929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "0af12315-0c42-44a6-ab27-9036d938605e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 120
        },
        {
            "id": "28f4b420-4f0e-4538-9503-177700b20440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 121
        },
        {
            "id": "0ef5a5b5-fac4-45de-a38a-de440f7af47f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 122
        },
        {
            "id": "79223b44-a9db-43bc-83a1-d60f6c5006d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 173
        },
        {
            "id": "d53325be-6cee-4e7e-99b6-a98aa39eec98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 894
        },
        {
            "id": "c602cc7c-f158-4126-8e0c-91ff6783aab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8208
        },
        {
            "id": "73ec4174-2bef-4334-a29a-35227105abf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 33
        },
        {
            "id": "ec774983-1954-4ea9-a64a-5ae0f1d8115f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "79130375-29d7-472a-b5f2-baa5d6140b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 35
        },
        {
            "id": "656c76b4-5322-4598-a875-b645a7f3de64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "82474285-b636-45df-87a2-900eb3625883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 40
        },
        {
            "id": "e480fe5c-3b8d-4073-94ce-ca5963539d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 41
        },
        {
            "id": "6a99db83-a2d3-4b65-8ad7-6c52bf8adc5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 47
        },
        {
            "id": "3a4d7c14-a48e-4228-a228-86342e26f6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 49
        },
        {
            "id": "bb38939f-3560-4e9f-9484-6c13fb14f4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 50
        },
        {
            "id": "dcf8ea6e-ddaf-49d2-b6f1-264c6aaa059b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "586af28d-9b3c-4a59-83c0-f3fa523bdb19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "18c8934d-d642-4bf2-8746-345380544a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 53
        },
        {
            "id": "e27edd44-9ad8-4dc8-bf4e-d5b2358004c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "ed5f3b97-040b-4726-b977-cc801037110f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 56
        },
        {
            "id": "c1e042b6-538f-4e42-a778-6956c5fb60ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 57
        },
        {
            "id": "d524df31-a0ae-42c8-b4de-72954ed40464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 58
        },
        {
            "id": "7291781d-04ce-4cf0-9e4b-5505777bfa61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 59
        },
        {
            "id": "1c218dfb-a0e3-47f8-a953-a9c80fa08a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "74a449c3-4c52-4f99-9c20-388c73bdac37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 64
        },
        {
            "id": "404de7f6-a817-4628-80d8-7757de6ec4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "fe7e9122-3230-4926-ad82-af01e00a5794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "fa49b1e4-17e8-4917-89ff-fa54fbf2ddba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "0b578f8e-31af-4adf-9226-0cee3ea4e462",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "68e75afe-dde3-42c9-9c3e-4caacf779da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "4d351bd7-e573-4258-86e4-b37c4e80485e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 88
        },
        {
            "id": "03113ae0-9a06-465e-8040-c09482697936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "27175d8c-bae3-4809-9922-04876f9432c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 90
        },
        {
            "id": "53bb67ee-6396-4ca6-adfb-5d581bf7ba07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "50172679-dc24-44f4-8253-02e3b94e6a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 93
        },
        {
            "id": "452e6cd3-d3e1-4aac-a212-c87e213ad279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 100
        },
        {
            "id": "1c9b1d5a-96fa-44f6-8122-aa10e4e41415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 103
        },
        {
            "id": "5d3d9d2e-5d51-4b74-8537-fe525c00255b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 105
        },
        {
            "id": "20dd2d16-363f-4657-83ac-fc7ec26f0534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 106
        },
        {
            "id": "34410bfc-02f6-4165-8149-f00fb61ba43f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 113
        },
        {
            "id": "6e962b9e-9783-450a-9ac4-969c640c599b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 115
        },
        {
            "id": "c71acf72-ac7b-4869-93cd-1fcf8fb7ba95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "f4597a76-b7f6-4591-9689-9e7540e430d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "70c4a2aa-4678-4e19-a654-3ae3fb9fbcda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 120
        },
        {
            "id": "11fdafca-b1cc-496a-9226-807538559823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "55676d61-c89f-4652-8429-acb276162cbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 122
        },
        {
            "id": "ad27f8d2-76b3-49dc-b28e-d1b320ed708c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 894
        },
        {
            "id": "80100260-1889-4631-9e17-6e5adba13984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 33
        },
        {
            "id": "2b0be968-49a1-4938-beda-78f5a3485b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 34
        },
        {
            "id": "f2afeef8-8f99-44a0-9765-69711fd7cced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 35
        },
        {
            "id": "3a3531a2-bbac-4f6a-9d40-2ba543699527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 39
        },
        {
            "id": "3262a9fd-3a76-4c03-b509-109bce58b558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 40
        },
        {
            "id": "9cfe9c0d-c69a-4b60-a8c3-a1faf519e496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 41
        },
        {
            "id": "4ca0f77e-5fa3-4467-9422-57be52544997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 47
        },
        {
            "id": "0e143ab2-ece8-41b6-98e2-5b412b9976f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 48
        },
        {
            "id": "8d68b2a3-b1bd-4475-a095-700ff8213a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 49
        },
        {
            "id": "d212dda0-b89e-4d0c-89bd-c6c8906955ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "60e80366-f07f-445f-85f1-b598fcc9a94d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 51
        },
        {
            "id": "8fa56f22-7bff-4415-bd58-c0ce301d2e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 52
        },
        {
            "id": "e1d7124d-b546-4446-857d-2f9c9d31a895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 53
        },
        {
            "id": "e903ec90-579f-4020-b610-cb756e9d4454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 54
        },
        {
            "id": "45836707-852b-4c28-b719-3b03f99d516b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 55
        },
        {
            "id": "e360078e-ba4f-4eee-b7db-a77829deca1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 56
        },
        {
            "id": "aba0ee82-2417-4dbd-9e91-49fbd6575d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "ecc52be7-b245-4286-8328-37ec1a57a332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 58
        },
        {
            "id": "dd8dd6d0-198c-445c-983d-ff64c50d6fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 59
        },
        {
            "id": "b4c86bf2-e594-4fc5-831e-05cca1f22d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "f2159a45-03ad-4999-b033-b743bed5cbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64
        },
        {
            "id": "11d61144-8a51-40f2-b546-dc2ae28014a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "a71c7575-6b05-4a22-92f3-dc62023a47d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 66
        },
        {
            "id": "1a807c26-d35f-400c-9620-8e1e27ea881e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 67
        },
        {
            "id": "15e90a13-e2e5-423e-bee9-15cd8d4de659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 68
        },
        {
            "id": "11bd8fd5-a63c-4d44-92f0-d646e8fcc9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 69
        },
        {
            "id": "78258df7-3485-4cc3-8dbf-c54e2e75ca29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 70
        },
        {
            "id": "85a34256-2141-4f55-9043-fbd226b71039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 71
        },
        {
            "id": "2255c9f1-0203-452c-9e72-f57414cd8503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 72
        },
        {
            "id": "49fcfd64-0ef5-4a45-a174-13dc41e68642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 73
        },
        {
            "id": "820ca7f7-f52a-495d-b5aa-9be2c5de69dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "e4392762-d00e-4d53-918e-6325c5f98efc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 75
        },
        {
            "id": "c986fc04-d371-402d-b54d-dea12e54b427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 76
        },
        {
            "id": "bcaff177-31bf-41a5-a6f2-aac6e0db813c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 77
        },
        {
            "id": "44b10da0-c45e-44cf-b7f0-6c763e1bef1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 78
        },
        {
            "id": "a12423a1-5dfe-458c-be24-6356c92573b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 79
        },
        {
            "id": "cf5bd12a-6362-43f7-b542-5ce8ea1a9f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 80
        },
        {
            "id": "04d5076d-8406-4faa-8e87-cb21c3b1437d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 81
        },
        {
            "id": "f7c92b34-3e2d-471d-8a64-33bd9943340b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 82
        },
        {
            "id": "29a09a3e-2f99-4f2d-b6eb-096b314fac10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 83
        },
        {
            "id": "873b8b0a-d10c-48c2-b1f9-e1d161bad91e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "d250fe1c-3b55-4a35-baa2-d8d3b2a6f6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "a0e77a15-d189-43cf-bae0-626bea9f6309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "dad50c7b-35b4-4e53-a77f-249272427e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "ada74693-684b-49c0-a69b-bc4487611ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "792c8814-7465-4677-a950-dc8a690e0396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "69e80f2a-0af1-4997-8307-8530b3077717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "11667e2f-7be9-46e2-bff3-36cc31d06c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "78a17600-90f9-4d4d-af81-38b3f16a6c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 93
        },
        {
            "id": "fa2b6858-4fb0-411f-807c-c9fec24f80df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 98
        },
        {
            "id": "d794e841-3468-45e2-96e7-d09ddb6f6439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 100
        },
        {
            "id": "c0541710-bc0f-4b32-b9f7-ddc49120b6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 101
        },
        {
            "id": "353033af-557e-4aa5-aafc-c8b96c5b2ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 102
        },
        {
            "id": "da624a53-9559-4ad9-8fe5-545ad60af74e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "c7f265f5-84b3-4c57-a502-1b4f89e129a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 104
        },
        {
            "id": "3db68560-6620-45db-a45d-93cc4bbb450b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 105
        },
        {
            "id": "94f8e9e2-0bf2-4f60-b692-5e9e7a4879db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "d08251ef-4664-4ba7-b7f9-3f354838f085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 107
        },
        {
            "id": "4519460f-6eb0-4119-9f78-02cce8d06067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 108
        },
        {
            "id": "66bb7ed4-7723-47be-b8eb-8aa1bd841f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 109
        },
        {
            "id": "d03374d4-7271-4967-b012-0cd2fdb1670c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 110
        },
        {
            "id": "5c0f494b-2ff4-4b46-9957-8211e546a6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 112
        },
        {
            "id": "9f1c0745-3d67-45f3-9b8b-0709e76da5b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 113
        },
        {
            "id": "babf522e-106f-48fb-b6a1-3784b3a07a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 115
        },
        {
            "id": "1492369e-28af-41b2-9675-009dbf24725d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 116
        },
        {
            "id": "24fb5251-e094-46ec-92ca-66aa07e5a208",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "c522d586-d205-4068-911b-58363bd7b2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "73da17e6-3675-440b-973a-402dcaf133a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 120
        },
        {
            "id": "b68b757e-6649-4ccf-86ad-fce53b21a90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 121
        },
        {
            "id": "5bc0c484-0a2e-45cc-882a-422b3f6e6ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 122
        },
        {
            "id": "6f4bbace-ae6c-42ea-b266-d545a7fee42e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 894
        },
        {
            "id": "713ac04a-c666-4ea1-93d3-b45cef5e17c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 33
        },
        {
            "id": "e258bc2f-2c78-48e5-94ae-554c1eca1cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 35
        },
        {
            "id": "954fca8c-bd17-4d93-9483-5ad723320e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 40
        },
        {
            "id": "4a05702f-e0aa-476d-b607-fed426c9e405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 41
        },
        {
            "id": "73eebcfb-01c4-4e11-9d83-c294a9e41088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "5b18071b-0eb5-4c77-931b-fbfc7dbf276b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 47
        },
        {
            "id": "d39a6eb7-bf80-4957-84ef-adfd128b57cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 48
        },
        {
            "id": "583c6cf2-d6e8-4f93-b485-0a4c1af98296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "3fd82901-70c0-411a-9ad1-96cce618e8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 50
        },
        {
            "id": "8ccc9ee5-dcf8-4da4-93e1-2195c81e76c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "8eec1d31-8e81-4b05-8c3c-fc430520434b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 52
        },
        {
            "id": "5c7ec549-34f0-4c63-b9a8-6c88756005bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 53
        },
        {
            "id": "7260555b-6c83-4938-8e23-29eb767606b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 54
        },
        {
            "id": "a786e9d3-f660-4f99-ad92-3be064f12c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "2fe7d5a2-36d6-4adf-99ab-3a0b48c4b0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 56
        },
        {
            "id": "7c5fa8e5-558c-4d9a-bead-e013c2be0b9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "aa490788-22f8-4c5d-8b58-7670c3d9b07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "4a0d4f21-27ca-4173-a794-34229968c189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "e0abe0bd-2f0c-40c3-8b93-7d0535867d29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "aeaa1a0f-09bc-49ef-aa8f-d6c7c2e99e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 64
        },
        {
            "id": "eb053639-bd09-4fc9-9fd5-b75fc2c8129a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 65
        },
        {
            "id": "6ed2899b-a65e-48ab-a389-b7252dafd4de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 66
        },
        {
            "id": "286a3de6-bde9-47a4-9cb5-03f8915a3753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "2b4de7b9-d91c-4156-80ec-26c4004af650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 68
        },
        {
            "id": "19ded0cd-635c-4d52-873a-53efcf400ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 69
        },
        {
            "id": "e6af5cb5-1708-4524-be1d-7a52323f3eb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 70
        },
        {
            "id": "eb5a2675-f48a-4b37-b344-40af000bbafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "3ed48867-f328-4109-b0f7-1e7109d99ae6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 72
        },
        {
            "id": "b057060d-818e-48cb-b275-c78aec7bafcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 73
        },
        {
            "id": "7cd801c3-80a8-459a-86f5-e0058a65b1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "854b1ab0-4da0-4021-b355-08b3b6c7c0bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 75
        },
        {
            "id": "20f25d55-bcb4-405e-b637-2b2747361424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 76
        },
        {
            "id": "b5ced14b-8d38-42e0-8e62-4e0136c070e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 77
        },
        {
            "id": "58e75bf3-a85f-441e-82a6-399eda94081d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 78
        },
        {
            "id": "57f08f9a-17be-4794-8c4b-aab131ab5adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "f0decdb5-c648-447c-8a2c-cc78c858633a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 80
        },
        {
            "id": "47aac7b1-622b-45c2-9af9-10c0ee114f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "f2551785-92e8-4aac-9d60-af8e9c5ff204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 82
        },
        {
            "id": "528f8498-a63d-498a-a625-1d9dcf892293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 83
        },
        {
            "id": "68bb8590-c2d3-4bd7-9c3b-5facdf2cec04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "6118ec54-2c10-4155-b48d-2382ea3278e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 85
        },
        {
            "id": "6c50033e-6c99-4a4c-98fb-db4242ad8906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "b17a1daf-016f-4049-a2db-1e2abe4ada95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "850e377c-eeeb-4a94-89dc-448eced3e3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 88
        },
        {
            "id": "6447c527-56f4-443f-bb14-c0397b734c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 89
        },
        {
            "id": "b15947b6-eeaf-4aa6-b368-5be1864ad531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 90
        },
        {
            "id": "90c66163-7127-41a0-ab46-de4e62958d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 92
        },
        {
            "id": "fa9cfed6-afe1-4ce5-8377-9db4b98c474c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 93
        },
        {
            "id": "df974a38-6d80-4a53-8cdf-9e9718b2d1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 97
        },
        {
            "id": "c4871fef-6ef9-481c-bc58-cf61f64e891c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 98
        },
        {
            "id": "13058974-2ef2-4e32-a402-034761e2a134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 99
        },
        {
            "id": "ec655f11-e624-4c33-88ce-afb1b559e5d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "97ddf2b8-d477-42bb-8973-e494d3a2bc84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 101
        },
        {
            "id": "a8d51699-429b-456c-a451-f9b45ef5848e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 102
        },
        {
            "id": "6074d344-f381-4e8f-884c-2b489e7f4198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "9a97b466-6135-487f-a282-9d28627478e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 104
        },
        {
            "id": "3ebfe55c-fc2c-40c6-80f2-573fdb4e1907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 105
        },
        {
            "id": "1b51064e-fb3d-4243-bb3a-3ad228bc64df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "68012a5f-436b-4eab-b439-43c9deba29ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 107
        },
        {
            "id": "013b26ab-5616-40c6-a1af-541b02fcf6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 108
        },
        {
            "id": "497f9bc7-3b87-4f00-afd5-45086e716562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 109
        },
        {
            "id": "8be2f0e9-e76a-43b8-9378-9c62e522bbcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 110
        },
        {
            "id": "fd87b157-20f4-4243-a906-18582a16affe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 111
        },
        {
            "id": "50ec7d12-f5d0-4778-897a-e726d27fe29a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 112
        },
        {
            "id": "cae43d0b-dcbb-422b-b14d-6b0cec61dc84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "14fbe781-8cff-4eea-93a0-dd4576150864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 114
        },
        {
            "id": "2ffd14d6-2092-4e1a-a404-58cb89395b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 115
        },
        {
            "id": "9357947a-1c76-44e8-8b31-c22e2149c1cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 116
        },
        {
            "id": "fce29ef9-fedf-418c-b6a5-5f1f406595ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 117
        },
        {
            "id": "84760aa7-85ee-4dc1-8c51-4136c989182e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "d2f59d8d-4745-4a85-ae05-b47943d974c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "bb492297-85bf-44bd-8e45-8de44c2db1f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "9e758f9d-803a-4532-a434-7ff8c0444076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "64f1e5d7-40ac-451b-acaf-85a6646ed5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 122
        },
        {
            "id": "e4a13a32-9ec9-438f-9e27-c8a38238050e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "7147e595-2f03-4aac-969e-e2a7e2d0762e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 894
        },
        {
            "id": "7515a1f9-22d1-4372-8c67-91564b19cbbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "1d6729da-46d7-4933-91d0-c73f32865d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 33
        },
        {
            "id": "7a37353c-ae54-455a-947c-c0e00c3d3c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 34
        },
        {
            "id": "60647f53-7084-46ec-97cd-5907de191ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 35
        },
        {
            "id": "d61ce129-e356-46b5-b091-fef95d8613c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "9ecdcf81-328a-49c7-ad30-62d8754160d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 40
        },
        {
            "id": "c24aafc6-8a4f-47fe-a20d-c6fb1b96a34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "07d81a36-0290-49a3-89ce-cf4ddd2c0c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 47
        },
        {
            "id": "d3ae01a5-f3d1-469e-85ae-43c692c22bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 48
        },
        {
            "id": "0158aea8-e61e-4821-b3b2-30c9e1ba8314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 49
        },
        {
            "id": "b8dfc871-deb4-4c32-a66c-06ebd5d642cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 50
        },
        {
            "id": "7671d886-bc4a-4f01-9213-f2814edb8e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "2e72bea1-14e9-431a-a078-b6262e44e9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 52
        },
        {
            "id": "2ce06cf7-d34d-487f-9f12-d23cb3624442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 53
        },
        {
            "id": "1f5cab82-f078-4173-a260-3f4a38f61fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 54
        },
        {
            "id": "bc5f7671-69e1-4ed6-b483-9f7225a299d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "28b0d341-79a5-4bd8-8859-2e5689495850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 56
        },
        {
            "id": "e78c3b85-523d-45a6-a671-7f165814296c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 57
        },
        {
            "id": "d56826b6-b754-460c-8c03-7c992daffa98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 58
        },
        {
            "id": "50875f90-93c1-423f-b5f9-0796ad8881b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 59
        },
        {
            "id": "6956b0f4-35ec-426a-8b0a-ddc81865d699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "5bc66410-5d2f-4d2e-8f8d-ffedaf27c435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 64
        },
        {
            "id": "c0cf7f7a-973f-4b16-8cbc-4ed03204692c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "c57f5bf3-c709-406a-ae54-5a8ccd77f6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 66
        },
        {
            "id": "286d35a8-ad8d-4323-83d3-bac01f5fbd2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 67
        },
        {
            "id": "353d784c-87dd-41ef-9b83-aa64abccb065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 68
        },
        {
            "id": "fb68c674-75c5-45ee-a4aa-2367c4d1ab9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 69
        },
        {
            "id": "a2759e5f-2ebe-406f-9124-cfbfd4a87c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 70
        },
        {
            "id": "c7d81622-3e50-42cc-b352-ccdd5d5d8c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 71
        },
        {
            "id": "9116192b-9654-4c2e-b5fe-e1a6b75fb18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 72
        },
        {
            "id": "c026c462-2769-4c3d-82ec-a4c06ca42434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 73
        },
        {
            "id": "cf61c249-12e4-4723-81e4-1f5d1e21642d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "b45b55b7-2f11-4d33-ac3c-cf80b9c9d1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 75
        },
        {
            "id": "faeae2e5-4236-40ed-9631-bf8a639acd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 76
        },
        {
            "id": "b3c2a94b-640c-490b-adfe-f2ff9d850d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 77
        },
        {
            "id": "3b114754-6bbe-4101-a039-c0e32df18057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 78
        },
        {
            "id": "77fbfbdb-66b5-450a-b41d-33da7d695608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 79
        },
        {
            "id": "c697a9f1-cc91-4c3e-9737-c21f6e2f26cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 80
        },
        {
            "id": "363960a2-62e2-4b2f-ba79-44e146b5cbbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 81
        },
        {
            "id": "e6f27e46-48cd-455b-aed6-7d8b87894ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 82
        },
        {
            "id": "0803b2d7-00ca-4ba8-bf02-dcfc345da6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 83
        },
        {
            "id": "b77ce0da-343d-427b-80a5-9654a997ff39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "54370c2c-0c45-462c-ba8f-290f6f0448f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 85
        },
        {
            "id": "b0aca001-08e4-4b30-a144-7c270099ac1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "6d2029fa-8489-4645-97cf-ac6f4c3ffe7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "0491d9b8-22f6-4c3f-bd3c-4bbdd2a99465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "2a1441d0-1db6-47c6-a0c3-7acbb0b28367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "2558af7f-19a8-4d98-a836-47ac0679ba70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "1f049d28-cf05-4bea-93bd-3b4430b2000f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 92
        },
        {
            "id": "f2b9d628-084a-4ab2-a1f4-ab53ae8adca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 93
        },
        {
            "id": "5ff49fa8-f5a6-49e2-b2be-c0066d664057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 98
        },
        {
            "id": "6dee02c5-df73-4f97-a997-a37b1c111887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 100
        },
        {
            "id": "a2110c5b-ba21-4cdc-81ee-c6be898cb2e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 102
        },
        {
            "id": "d1a24c50-d1aa-4fad-ad68-d168394bbcdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 103
        },
        {
            "id": "9bf137a8-06ef-4618-b20d-82b436aa84b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 104
        },
        {
            "id": "4acea735-f9bb-4ea8-83c0-7f20b13b63e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 105
        },
        {
            "id": "52f382b1-5da7-4348-b6ff-224e4a3ab246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "18eedd43-a082-4593-9526-ed38a202fac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 107
        },
        {
            "id": "280b0c07-ad49-4736-93dc-4540b51b6af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 108
        },
        {
            "id": "f97703c3-0abd-45b8-9a46-21e263ce0928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 113
        },
        {
            "id": "eec61519-14ea-41df-9e41-2c46d1a12000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 115
        },
        {
            "id": "85d96483-05ba-4b5f-8040-e32c20b5a10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 116
        },
        {
            "id": "9a69f475-06f5-422f-8061-971969359228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 118
        },
        {
            "id": "3893c328-941e-4b43-9ca1-573f0488698c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "7868449f-3f47-48a8-a8ae-c37d4110399d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 120
        },
        {
            "id": "73ed2666-3691-4eef-add2-e22ea4c6f977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 121
        },
        {
            "id": "329edd34-caef-487d-9382-4d724b2cd0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 122
        },
        {
            "id": "e6f8c925-4e19-427f-8980-9776562c184c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 894
        },
        {
            "id": "e7b9365e-9425-4b7c-8d73-3ae91ee302cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 33
        },
        {
            "id": "e6155c19-f5e6-4f6c-84f4-1c4666b0ed23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 34
        },
        {
            "id": "451db92f-65cb-4cd8-bfc1-0e6a4d85270c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 35
        },
        {
            "id": "1bf12426-acd7-452d-8b5f-3753fab2a97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 39
        },
        {
            "id": "6cbf22f0-c085-4c47-8e6e-219706e946c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 40
        },
        {
            "id": "2209b74d-84a1-4e0e-9432-fb01d2cf19b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 41
        },
        {
            "id": "56d9504c-867f-4fa1-a464-bcd05cd5add6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "6f45151c-1774-4a1b-ba46-2f89aaed535b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 47
        },
        {
            "id": "b574f1c3-5f27-447f-a318-13c2661b15ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 49
        },
        {
            "id": "e176cef5-c749-46ec-8809-874cc9c276f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 50
        },
        {
            "id": "ec22bfc8-be84-4284-adb6-eb6fb10f9cca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 51
        },
        {
            "id": "b2d04960-8440-48e0-ae2b-477f2c25d18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "8fc74873-b98d-4c85-8450-c1db4251e456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 53
        },
        {
            "id": "9d852c91-f091-4e87-a1bc-d23b2bb2cbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "e64671a3-c9d8-4298-8409-c50218f36cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 56
        },
        {
            "id": "8840eabe-9176-4726-a8ce-90e452f0fa21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 57
        },
        {
            "id": "3b4efd11-1625-4072-ae16-1ef4b3062632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "208632ea-53c7-4772-a969-70d73eeeac95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "d0d81c69-f08b-42c5-b70e-e480b5540f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 63
        },
        {
            "id": "3b225752-b4c0-40e9-8e56-ecd135e39d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64
        },
        {
            "id": "c21735e5-5fe8-4279-baee-3707ea43e679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "53ccba07-cd95-4b1c-8e32-d5c2b3bf339b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 83
        },
        {
            "id": "e62967d0-53f7-4462-8f96-dc8c72c1afc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "5fd874df-8ce3-4695-872e-01543d97a2fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 86
        },
        {
            "id": "2193de3b-a3a7-43f6-9a1d-7e0ad0bd932a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "47dff29d-651a-4b7b-a00d-11162b9c92a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 88
        },
        {
            "id": "e902f1c4-f6f5-4f1d-a794-250164096c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 89
        },
        {
            "id": "de4990c2-2814-4482-b115-1e52a3d3ad86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 90
        },
        {
            "id": "03f3fcb8-68f3-48ae-99a7-9cd5f9ff02a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 92
        },
        {
            "id": "c77b5c70-379c-4072-9090-9a530817b7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 93
        },
        {
            "id": "e5a36d12-fe41-4ae4-b0ba-d8940138b27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 97
        },
        {
            "id": "e05c2123-f910-49c4-a918-6e18483ec3c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 99
        },
        {
            "id": "4c9f14b6-ac41-4043-a076-bd5379cc3aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "d3fdc060-d247-4088-8dee-59cf893bae72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 103
        },
        {
            "id": "83cf9f3f-1a7f-42e0-b841-4b4799529c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 105
        },
        {
            "id": "d0b9963b-c1ee-4f98-b262-161680b2814a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "937025a7-2674-4219-bc35-3b91eaec8d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 111
        },
        {
            "id": "c53ebb20-5970-4140-877f-329159d836f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 113
        },
        {
            "id": "1f3a1025-85fc-4376-9031-9d80c9ec3024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 114
        },
        {
            "id": "0ecd2e8a-41d2-41cc-a9ff-a733b8a2144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 115
        },
        {
            "id": "f2eca6cb-9771-419d-86b4-d2b32efff94f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 117
        },
        {
            "id": "5af3b2b7-53d5-4a88-b037-bb56ef06de36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "21338bcd-3c55-481f-ac68-925552434f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "08e8fd65-d711-41a5-9693-ae1e42fa7948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 120
        },
        {
            "id": "05c7d5c2-e96e-4283-a392-20b516a632b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "c7a8e226-0067-4374-9cca-92fdd1b11c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 122
        },
        {
            "id": "30b7dee0-0390-413f-ab1e-59bb3a3491a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 173
        },
        {
            "id": "3851f8dd-0e74-4881-a46a-6f4a3d18bf8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 894
        },
        {
            "id": "9e5e24a1-cd04-47c3-bb8f-defeff36e696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 8208
        },
        {
            "id": "25de6e03-9623-4a4e-8ada-bc465542fafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 33
        },
        {
            "id": "e1e1565d-acaf-42fd-ad6f-b490674f9ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 35
        },
        {
            "id": "74607244-40fb-41a7-8ffa-8a845bb48ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 40
        },
        {
            "id": "f1ce1654-45c2-4db6-87c1-8ce8a16b2582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 41
        },
        {
            "id": "ef714098-62a2-495c-beb1-a6d15c733ed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "1f99eacf-dec2-4c22-a4bb-fcc7548a1165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 45
        },
        {
            "id": "6e03bb3b-3c44-46e5-9f37-a60fb54f5a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "3ac9c92f-9b5f-4416-9667-5176ded22694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 47
        },
        {
            "id": "fb5da216-b0d5-4665-b40d-5e9cc0e5b7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 49
        },
        {
            "id": "85eb9e58-f903-4749-9970-7e385ba4c5e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 50
        },
        {
            "id": "370cf46d-7b0e-42cd-a525-f8f0d2131ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "95aefe82-a2e4-454e-93af-3969f82bc3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "2078d1c0-0fea-41bb-8cc1-085b8cd04087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 53
        },
        {
            "id": "6a840195-897c-4cba-b3f2-21f26cc0fb9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 55
        },
        {
            "id": "14bc69e8-4384-43eb-88fd-fe658ccafcca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 56
        },
        {
            "id": "b9f5f521-924c-4c38-aff1-c7657439036b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 57
        },
        {
            "id": "fa167702-3f18-43cf-b621-81a162f99033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "a71eff4f-8728-47a2-9604-ce1d3aa23d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "b87d8f69-1d6d-4aa9-be29-c7d3d4c345a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 63
        },
        {
            "id": "af5ada11-1f80-459c-97b4-75fc22fde9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 64
        },
        {
            "id": "6b956b70-54f0-4fdf-9b20-60b15c743b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "d5f9035f-ad2c-4170-a2e6-e79660cfb2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "b9891c51-5af4-40fd-bc56-4baba30c7618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "bc8fb8b2-4c51-4952-85c8-b5cf42b63b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 86
        },
        {
            "id": "8672ec3f-4f6e-4b40-ad84-1ac61e67227d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "967285d8-b05a-4372-bc8e-a5a581d6b724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 88
        },
        {
            "id": "9549f6aa-33e5-4b1f-a7bf-3233da531b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 89
        },
        {
            "id": "51859b1f-f774-47cd-8a17-e802a9368028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 90
        },
        {
            "id": "fb980228-bea7-4024-ac3c-25ca3e03dc54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 92
        },
        {
            "id": "f4c97cf7-0523-41c9-9cca-542245060cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 93
        },
        {
            "id": "675527ab-11b1-4ee6-9364-d121b9896ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "98d9e377-f8cc-4c76-99ec-b0868d7447c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 99
        },
        {
            "id": "394be4d5-d43d-40a3-aaba-14414ef75ebf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "77e56f28-cb45-4458-805d-7fdd2c806b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "cda0ebeb-1b03-4ca7-bc69-e64015e4893d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "0b6d9ad4-dc6e-4610-b223-822b525a2dbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 106
        },
        {
            "id": "f550304f-a21d-498e-9127-114dbbb3fdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 109
        },
        {
            "id": "f4f7f86e-08b3-40a6-ab38-c7bfc03b9a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 110
        },
        {
            "id": "4ec71ea2-bdf6-4e67-bf20-47cd3f23c3a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "2412438d-ae6b-42f1-ae7f-b9276634857a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 112
        },
        {
            "id": "5393816f-c42f-4bd1-96cd-fee81b0b1644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 113
        },
        {
            "id": "e8f02e0e-f824-42ce-ae7b-d426a25e601b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "e2355e7b-669f-46db-8f8f-f544652c33d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "dd748b84-e542-4aea-ab88-bb9bbe322204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "12425470-1d86-45f1-959f-ffb32a2f7cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 118
        },
        {
            "id": "08982863-15ce-4715-bd9f-2b690f82e01a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "d05bd351-177b-42ce-8648-6c813545de11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 120
        },
        {
            "id": "d1d0b1f1-658b-4523-a92e-4685d1dbd4bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "e8d49ede-0411-47a6-b0fe-0eceb0926775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "eca88358-61a0-4b9f-ba5b-9ea51b295f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 173
        },
        {
            "id": "8e9b6153-6022-43b1-a80c-aa53a6fd190c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 894
        },
        {
            "id": "521458e8-113e-4ada-954c-25a470d00433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8208
        },
        {
            "id": "b7d91c5a-0942-43ec-aef7-907c0f2ab20e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "b06b77ff-619a-4a5b-a665-178e52dbdef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 34
        },
        {
            "id": "58018803-b18c-4c35-940a-76ebcb12a2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 35
        },
        {
            "id": "056e0c5c-ba8c-4c87-8c9d-22dd0a659f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 39
        },
        {
            "id": "bff6ee9b-2a06-4f65-94cb-77fdf88c22be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "0abb768f-87fd-44ce-9a38-abaad8cf19a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 41
        },
        {
            "id": "e7d4a434-6394-4a0c-9c10-839c59aaaafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "e62d149e-2f3b-4e59-a550-c7cd9ea31d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 48
        },
        {
            "id": "7a5dda23-c2e1-4eaa-a933-2e5d2b868745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "13cdc782-7703-4f0a-bf04-fc1a00051894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "01ff3216-6643-4993-8233-77b587bd23b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "f19bf160-0cbd-4437-bdcc-b96bedde30bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "042bc1f0-41e7-4c9f-967c-dbc5637e9479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "2700e9c9-1a6b-4133-b46c-77a1682c9f84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 54
        },
        {
            "id": "a4d03169-eced-44b0-807c-e28fd3faecb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "330cfddb-56b0-4762-89ef-824522479f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "34a04b22-ea61-481a-a494-616aecdd97a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "de905318-5f5c-417a-a00f-50dad956410a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "2b1e6588-ba0d-4a2a-8d4b-b6a067a73aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "3042ed77-82e6-4288-aefb-69ad974bb73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "ba13dfb5-097d-400a-b00c-d84837a7804d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 64
        },
        {
            "id": "bf91a115-f263-4775-a3ed-dd4a922d3ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "1eda5431-afd5-44fc-9c08-d5989b381485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 66
        },
        {
            "id": "e436ae79-aaf3-4f0e-8697-452470978b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 67
        },
        {
            "id": "b4e916ca-9a2e-4642-ac7d-8712eef2000d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 68
        },
        {
            "id": "b0d04d58-0b45-4e6c-8b2d-cc617c3da345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 69
        },
        {
            "id": "792e8de4-c632-4e75-bbed-df82c3fe599f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 70
        },
        {
            "id": "ed6ebf27-2ee5-4867-854e-149117bd31a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 71
        },
        {
            "id": "9bf34df9-cab1-4c29-a5b0-fc9daf7b2bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 72
        },
        {
            "id": "2351d013-96b4-46e6-940a-ba0109122c2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 73
        },
        {
            "id": "682599cd-5426-4681-843e-54b577fa9202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "b8e105bf-05be-4260-8460-77842636ffcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 75
        },
        {
            "id": "d907dc36-0106-445f-a5a0-ba0cdb4f0d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 76
        },
        {
            "id": "95dda3bb-34a5-4704-a526-386eb95a79f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 77
        },
        {
            "id": "8768f438-bfc4-42ac-b1a9-0a452ef14fb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 78
        },
        {
            "id": "eca16f33-1b7a-43f7-a543-885c33a69201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 79
        },
        {
            "id": "ae5eb428-9270-4937-8d9a-9c7cefc254f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 80
        },
        {
            "id": "9daa098b-91cb-4b62-b3f7-edc8f284c54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 81
        },
        {
            "id": "d06c2001-035d-4e4a-a7db-2ecb4ca544a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 82
        },
        {
            "id": "f6d7bb40-c2f8-4d70-a2c2-56db6795a54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "d6231f89-6969-4a53-aed8-7103d869e941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "81efbc0c-7ccb-4b58-971b-300d5389087d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 85
        },
        {
            "id": "ffd22c4d-49cd-4d67-bb24-bc8f9db1c0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "1c53eca3-d4bf-4a8a-85bf-96f1752591b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "2f9b3398-62d8-461b-a27d-7ec896ed4309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "00a19b1b-04b0-44f9-8acf-c87e980c09e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "0fdc4954-d500-4405-b006-e0c2a1743fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "b19db671-18a4-4806-82fa-b451a14d485f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "37c8971d-d1c7-495f-a99c-dda9482db983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "0f175637-612d-4741-b5e9-29c45a62138b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 97
        },
        {
            "id": "70790404-7df1-4726-b5a8-cda3eae34e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 98
        },
        {
            "id": "e36b2382-abd1-4468-b757-8dd186717487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 99
        },
        {
            "id": "b87cfa61-6ae7-4459-a4fb-b98bfc68a812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "05f39fec-85c0-4fb0-acfa-60284543a20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 101
        },
        {
            "id": "ff065f56-69d2-4ac5-b58a-8560d37f522d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 102
        },
        {
            "id": "fff51971-0c89-40a4-a302-f9c64887f58a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "d3f112b6-b770-4341-a185-89dbd2d6bc1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 104
        },
        {
            "id": "85d6af43-19a7-4af4-9c06-9780c384e372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 105
        },
        {
            "id": "7c3e1cca-e458-42fd-abc1-18edc830f3d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "bf51ae9b-af26-4d20-ac6d-d6fe07bb43de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 107
        },
        {
            "id": "0f044630-439c-4c88-aa49-6ab14bb91e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 108
        },
        {
            "id": "846f9b7a-e0c1-4bd1-86e9-d5ff2b49bdf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 109
        },
        {
            "id": "760e4bc3-5ef3-4e14-a347-7148e4eeb170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 110
        },
        {
            "id": "c915a01c-18d7-49f3-8867-07a50fe99857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 111
        },
        {
            "id": "8f2f77f5-21af-4c00-8cdf-061bd3f56a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 112
        },
        {
            "id": "73bcbf9c-fe9d-4116-91fe-5068e6583256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "625d71ea-5b2d-49ea-a714-93279524bfa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 114
        },
        {
            "id": "918ac336-6fed-4fdc-8a34-c3139b052a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "3cfb663b-340c-4e43-a4d9-13a3468d39ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 116
        },
        {
            "id": "b5093b99-1449-4b69-82ee-7d938a04c42e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 117
        },
        {
            "id": "7cfa8d25-80a4-4f9e-b6ed-62d9d580cc58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "0fc8b6fe-4e47-4dac-99ec-f95997cae185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "c02727ab-e60d-4050-a117-211f4d45feed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "61715bfd-ea40-465e-a8c9-5078e551df4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "ae09958d-f220-4c6d-b92e-96aabc55c2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "a75ed95d-7796-45b4-b109-492a1049d39f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 894
        },
        {
            "id": "280c32a9-96a6-40ef-a46f-080bbc2e9c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 33
        },
        {
            "id": "6559648d-c58b-42ec-b238-9152635b0d1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 34
        },
        {
            "id": "0fd8f084-0ed6-4cdf-a7b1-5d27b9b8af2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 35
        },
        {
            "id": "d90a257b-16d2-4870-a9d8-a05209d998fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 39
        },
        {
            "id": "9b4c25ab-f14e-4fba-a23d-97a57cc3f63c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 40
        },
        {
            "id": "1fb5741c-3cfb-45f6-bbbb-97db80041611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 41
        },
        {
            "id": "517268a4-418e-40bb-846a-64ffb250f948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "08978386-a89b-4aa6-a5e3-6409e3fdad9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 49
        },
        {
            "id": "4576264d-1e67-45d4-822f-fcca82a9fb4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 50
        },
        {
            "id": "c535ca5f-df04-4e4c-bc3e-2e6319d0b9a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "087b22da-6d9f-48cd-b740-8e9529d7a86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "2637825f-024f-4a5b-9d81-68789eccc047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 53
        },
        {
            "id": "3371dd52-86cd-4108-9455-cd21d0ced2cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "ed800ed1-61ca-4795-8848-e77a91f809e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 56
        },
        {
            "id": "4ad09273-29ca-4e5f-a998-82a9a54a059c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 57
        },
        {
            "id": "7b3e8a1d-a4ef-4e53-9ecd-41d17444e142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 58
        },
        {
            "id": "db5c5f4d-c588-4ea7-9d6e-d487be9a996d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 59
        },
        {
            "id": "83746eb9-0399-4107-b9f4-04db011242bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 63
        },
        {
            "id": "9a78c187-df13-4146-8f64-d9bd592dd4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 64
        },
        {
            "id": "4d1da3ea-2ecc-495c-b42d-84fcc6cefb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "80a2312c-0ebd-484d-b908-df830a6062fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 83
        },
        {
            "id": "7d726677-95b4-493e-8579-2cc248fff59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "5b886feb-71b2-4a71-b609-61621a6f5c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 86
        },
        {
            "id": "ba92500f-6fab-4152-8c06-20ed8ee424ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "38f13397-85cb-4563-aa07-5ea037b95697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 88
        },
        {
            "id": "8dc3c2b7-2419-42cf-a3fd-00b31990e9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 89
        },
        {
            "id": "9bda4b80-ecf3-4647-ab2c-f4cc9e72f983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 90
        },
        {
            "id": "46b388f1-73db-4547-b436-4de172f1e88c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "98c930f2-0f7a-49fa-8a70-4867782447da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 93
        },
        {
            "id": "f2a1c7e8-1a35-427f-bddd-d5e30ceda65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "621cf5ea-99e6-45d6-9e16-5ca1ab1d2aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "adc1bb90-d682-47a3-8de5-0b4eedf94af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 105
        },
        {
            "id": "19320d16-d305-44d7-a016-a25ebecf6dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "830ebe9c-b850-45ba-a8e5-42e078ccfc1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 113
        },
        {
            "id": "078cf0c5-1980-472d-a2ad-4daa554c5a1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 115
        },
        {
            "id": "eb9f7401-7808-4066-a2f3-04d8ac980754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 118
        },
        {
            "id": "44fd0ad0-7ff2-45b8-8396-838de5587b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "410fe4a0-9959-49ef-913d-443957b4c19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 120
        },
        {
            "id": "52dc9300-bdec-4645-a4a8-4d7615353542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 121
        },
        {
            "id": "612cf94c-7f86-4dc4-9df3-35d9375abb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 122
        },
        {
            "id": "004ce2e4-9020-40cb-9c11-d38086282c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 894
        },
        {
            "id": "0fc0e94d-5318-4f04-a8a3-756920c7b28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 33
        },
        {
            "id": "4a24e01e-0ae7-42ea-bed4-81f4e0fe92c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 34
        },
        {
            "id": "52974fcf-4786-4ac1-a4a0-19b30bc06e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 35
        },
        {
            "id": "86c12d63-1182-46a5-add8-0eb7df24f15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 39
        },
        {
            "id": "61ced2f8-d77f-44b4-b622-d446c5c33ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 40
        },
        {
            "id": "b6469008-171a-432c-b9e6-8a618e80a9d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 41
        },
        {
            "id": "0bd35b49-6d0d-405a-8201-b348a9d25169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 47
        },
        {
            "id": "714a9637-3290-4c43-855e-ef4a9c3ae42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 49
        },
        {
            "id": "737814f5-9516-46fa-8fc1-8f9b9ea9b625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 50
        },
        {
            "id": "62c69e43-a23f-491f-bf2d-a9314b553d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "9deb4f7f-2eb6-4e6a-a52a-a92f403dc81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "5cd13463-7e6e-4065-a1a9-9d8dd53da1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 53
        },
        {
            "id": "b55f1488-d3ba-4dba-b2da-5d677007bfd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 55
        },
        {
            "id": "f993bcfa-3298-4f2a-b7f3-f7781068bc6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 56
        },
        {
            "id": "c5bb64cf-f2fc-41f8-86ce-95e4cf335554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 57
        },
        {
            "id": "730e408a-c194-4140-ade4-655cef9496c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 58
        },
        {
            "id": "65ec6d3b-8e8d-45a0-a677-c8b65659119c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 59
        },
        {
            "id": "734b6e9d-9938-485b-91a6-c700bcce5b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 63
        },
        {
            "id": "8010f05b-e0f8-419a-be58-076fdfd7a988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 64
        },
        {
            "id": "6851c8cf-1c0b-48bf-ae4f-c62fc67ad5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "24a8b935-96af-413d-bcef-e2ba0bb2b7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 83
        },
        {
            "id": "4825c3ce-d65c-41f7-bf65-6e5d6fc4f737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 84
        },
        {
            "id": "881cbaf3-d962-40ca-8e59-b66eb4978cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 86
        },
        {
            "id": "6ea2d0bc-71f7-4af8-8e5c-30de4eec46cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "fe3166e0-24da-49b5-ada9-412f9b839e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 88
        },
        {
            "id": "6c134479-ccca-43d8-a3c8-50342ce05227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 89
        },
        {
            "id": "3ce1a009-9504-4b46-8983-9ea8c4ec5809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 90
        },
        {
            "id": "bfacd37f-4363-49df-bfd6-546ce8369505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 92
        },
        {
            "id": "f9f75fd8-6657-4fef-832f-77a1b830b49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 93
        },
        {
            "id": "3128943d-872a-4399-9f41-334e439ccf55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 100
        },
        {
            "id": "e642a727-3611-40bd-a2dd-7e32b0ce3502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 103
        },
        {
            "id": "88460879-c809-475f-977c-f888d6ed9d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 105
        },
        {
            "id": "49779794-9b8e-44e5-8542-59851c38157c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "731811b6-4731-47dd-aaee-0761867716c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 113
        },
        {
            "id": "3286f8c8-791c-4ac9-bd9c-1e6447f5c1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 115
        },
        {
            "id": "36ce0ba0-dabe-43d8-9a56-98eb2723ce83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 118
        },
        {
            "id": "0b19aab0-a2b2-4ce2-b057-8831a28e27ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "9d7c0a47-2df3-4a7f-a5ed-9fc7b4edfed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 120
        },
        {
            "id": "6eb6329a-1cbf-4fb2-adaa-11fff14386a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 121
        },
        {
            "id": "a92d1ace-2d7a-413e-8ccb-aa8f5215970f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 122
        },
        {
            "id": "6bd03bef-003d-418f-be6f-301919956120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 894
        },
        {
            "id": "f917f5db-2108-4524-97e3-e1373068d128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 33
        },
        {
            "id": "11a77c49-cf14-4a18-872b-6c84ea6ea9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 35
        },
        {
            "id": "6cf2ce78-8859-48bb-b910-3fe91a6e00be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 40
        },
        {
            "id": "1ca22cb7-be11-4d84-9544-9b2d9494aaab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 41
        },
        {
            "id": "e061c2a4-5f62-4665-8b17-5d415b36c617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 44
        },
        {
            "id": "c5ae2b07-1813-4734-8b1c-3e7d9e0758e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 45
        },
        {
            "id": "f60bb19d-20bc-4b37-9b68-1fd0dae4b960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 46
        },
        {
            "id": "1e38a868-8ab6-4ce9-93eb-e56278355d5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 47
        },
        {
            "id": "6565e427-9346-48db-a0c3-2c44b5713949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 49
        },
        {
            "id": "d976d4d3-7659-4840-8e47-c309263a651d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 50
        },
        {
            "id": "33127820-d763-47f4-9841-91d920e45ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 51
        },
        {
            "id": "bc304ada-b360-4645-82d8-f66f6a71e021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "2f8594a4-3f02-468e-be5d-f9145be30e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 53
        },
        {
            "id": "9502aab9-b5cc-44d7-b262-15ad31c9a46c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 55
        },
        {
            "id": "513e3669-5f06-4aa4-b288-7bdf7057dd8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 56
        },
        {
            "id": "a97f85a3-0b4e-4476-86f5-cd1efd9efd3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 57
        },
        {
            "id": "ef156ff9-3540-42fa-93b7-14d740bdcc5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "b800f597-5d51-4ba0-80e9-a4734173d162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "0387f33a-e613-4d1b-84ad-bbbdb008b478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 63
        },
        {
            "id": "07b73bcc-0c79-435c-b27a-603f18be7b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 64
        },
        {
            "id": "f835688d-481d-46a4-8b9a-62a866d99db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "4f22ce66-b51d-4919-956d-1f5d6b892546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 83
        },
        {
            "id": "5b420744-0112-42c4-8d3a-639c9a63aadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "6fcfb5cb-66c2-49d6-b095-9f8a9a628184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 86
        },
        {
            "id": "680713b5-1244-4da1-ad1b-6745bd48a7ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 87
        },
        {
            "id": "f872dac4-3b77-4f88-8e6e-91b0e0f38fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "34b9afe1-4a56-471a-8efc-1bc74fabed8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 89
        },
        {
            "id": "0189db82-8074-4d23-9a60-8c2dea2da3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 90
        },
        {
            "id": "29006da2-f1bf-4863-910a-23c3788bd81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 92
        },
        {
            "id": "043ef20e-838c-4c61-8e97-515398141b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 93
        },
        {
            "id": "7abaf5a6-ee7b-460e-8d8b-10c1872e1ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 97
        },
        {
            "id": "2a86a26f-eae9-40d2-9cc0-512b5395165c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 99
        },
        {
            "id": "aab6653d-a62a-4b72-b117-8ae4b06c9a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 100
        },
        {
            "id": "5a9f75ff-5158-430c-8cd3-1c1d85868f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 101
        },
        {
            "id": "bbebabb5-f1fb-46d8-a797-1c708b902f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 103
        },
        {
            "id": "f83493e4-c98d-4932-8c8e-0e323cb5d63f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 105
        },
        {
            "id": "08775fc7-90de-4287-acaa-2ae34e960f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 106
        },
        {
            "id": "b017ebbd-4408-42ac-8dbe-bbb7388fc7af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 109
        },
        {
            "id": "501b3790-0d8e-42db-9c6c-5fffe64b0af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 110
        },
        {
            "id": "8a0941b9-17e5-44e3-963f-37b0af78b226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 111
        },
        {
            "id": "f49bfdbc-fe2d-4ede-959e-455c39db4d94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 112
        },
        {
            "id": "ac2e20a0-f41b-48dc-970a-9d8cdafcb1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 113
        },
        {
            "id": "77364a66-d0eb-4c42-90f5-c867ad9fcfae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 114
        },
        {
            "id": "4c36dd8d-59b4-4338-94e6-4e70ccb0d2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 115
        },
        {
            "id": "cc9f76c7-1f62-4391-8677-7556942c72c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 117
        },
        {
            "id": "9151aa41-b6b1-41d5-b4f2-d65f0fd1e70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 118
        },
        {
            "id": "9de2ae64-f3c9-4659-8e7a-c71cc1bdb5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 119
        },
        {
            "id": "6d88d753-673f-4fed-b994-64ecf9e3b979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 120
        },
        {
            "id": "673d2779-0371-4ed3-ab34-e58bd4e1c4bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 121
        },
        {
            "id": "b24f501b-e127-4e90-8ed0-7179385d4a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 122
        },
        {
            "id": "8d2173bd-8682-40ee-ba9e-8db0e0a6894f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 173
        },
        {
            "id": "9fa9bd0a-2ebc-4482-ad24-61b1019baf12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 894
        },
        {
            "id": "6fb61f09-6f8a-49a0-b90d-029a3f97ff0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8208
        },
        {
            "id": "af5f5510-d8bc-4a18-a5f8-d2b6ffa50046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 33
        },
        {
            "id": "24324fd4-f470-45dc-b848-a4591cde71b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 34
        },
        {
            "id": "9e6aa12a-e604-4326-8a0c-34212257d149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 35
        },
        {
            "id": "2017f3ef-6ca0-467a-ba3a-eab7ebe933a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "96d13a0c-25a3-4914-b7cf-a09210e6617f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "c6e618f3-5b95-44de-8e45-c8d9d43a291a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 41
        },
        {
            "id": "f248d0e2-07a6-45a7-a05f-2124dadd833c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 47
        },
        {
            "id": "24890683-595b-4f9b-b385-6c8966d68ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "5d52532b-2791-4ae1-80ad-18e518cd2393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "e4b107e0-04e4-4a16-bafc-8666d0c1eb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 50
        },
        {
            "id": "3e443f49-31d6-468e-b69c-702a8ce9d59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "8f79cfd2-cc32-4580-8cf4-b734b3908ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "b50b38a9-64d7-4936-8d5c-fc57463763eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "951b53e9-28ee-4077-acdf-bb8942ca3704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "66668741-3465-4b1c-a216-ab61b126b059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "5d56ad86-1088-4302-85d2-f9395dd673c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "81662ea9-f39d-48b8-a5bb-4e0fcda4ca7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "b0092455-9771-4bd3-9593-aae016c9ee7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 58
        },
        {
            "id": "a0cfeb68-9786-4722-828c-77ca4bd6c2d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 59
        },
        {
            "id": "5f69e420-9a0d-493a-a36a-cc87727255af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "2650b371-6034-42c7-b543-20b82bdf1f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64
        },
        {
            "id": "16791bb3-4d34-48aa-b627-5ec18acd1bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 65
        },
        {
            "id": "f434f529-d296-4b42-a4f8-c41aa4c1554f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 66
        },
        {
            "id": "1d95d28d-8354-41f8-8683-5683b20528ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "15cf3697-7ef1-4a24-b986-714fc0592862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 68
        },
        {
            "id": "a803dba8-b520-4bd0-9271-5ae6bbc243cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 69
        },
        {
            "id": "e7493c01-7ed3-4697-a418-52c59e988421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 70
        },
        {
            "id": "d997dac8-439a-484f-9902-9d5b910a117e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "9e409271-d340-4968-9bfd-5ed7fedf71dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 72
        },
        {
            "id": "bc89abf8-2743-4834-b9a5-46db78c358ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 73
        },
        {
            "id": "bde09083-7708-498c-98e3-6378b47df45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "3640526e-c60b-431f-9496-5789b855f3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 75
        },
        {
            "id": "9de33f8e-84cc-48de-bbc3-d3b1840ce64e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 76
        },
        {
            "id": "2c8616e7-6bc1-412f-99f9-bf41e7b22b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 77
        },
        {
            "id": "285d7e69-ccaf-4ba9-ab28-4304bad4eaab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 78
        },
        {
            "id": "2655a634-c92d-4922-be49-f0a8089e857e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "a3d4acbf-a592-451a-bb5a-2bac543ad622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 80
        },
        {
            "id": "488512af-8db5-41de-8ee6-444dce986ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "080f72e2-9c91-41c9-84e5-2cd0b2b2237d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 82
        },
        {
            "id": "aaaff498-63c8-4dcb-84ef-da6a71cf978d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 83
        },
        {
            "id": "447d11c7-b2dd-4da2-a1d9-e2a7037305db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "c3fbebea-93a2-421f-9b97-5e2e16d0d1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "b0965b97-f90f-4c90-88d7-745ebf656bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "6db18f26-7d23-4cb1-9d76-c8780c606ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "4db268cc-09ee-440f-a809-1545ce6f3681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 88
        },
        {
            "id": "cbd0e051-7c0a-42e1-b583-947b5da99425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "fdaca45f-1d51-40fb-941f-3b844fc36d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 90
        },
        {
            "id": "7b366b04-a318-4dbf-8440-0464097ff6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "2daa167b-e0a2-4414-b2a7-9e244f420fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 93
        },
        {
            "id": "2dec8c2c-d959-4266-9ef4-c5909e96a5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 98
        },
        {
            "id": "4dca5720-96d8-4ac9-a589-02db39bc678b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "596132f0-73d3-4b56-aa5f-2fcd4e09c28b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "43916e11-7c16-4601-b974-c9e32af88f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 102
        },
        {
            "id": "9aec3978-27a0-4473-8db4-8503330a390b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "25a8efcc-c881-4ed6-aaf5-0b72b9bf5584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 104
        },
        {
            "id": "87cd42a6-cd2f-42f8-8cf9-8e81f92905a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 105
        },
        {
            "id": "063ca6a2-f6fc-4032-9915-992f56ebd6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "0da6a054-a0c1-46fe-9bca-5e696b6d00c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 107
        },
        {
            "id": "ab938ed6-0449-4ff2-8402-d3249768ba2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 108
        },
        {
            "id": "44f550d7-159f-4495-943f-a520f6a0a631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 109
        },
        {
            "id": "7c6fb3b6-0859-41f5-bbca-b421cdca561d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "711f04a7-6c62-41ff-a951-8e9b71604585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 112
        },
        {
            "id": "bc666e84-b9f7-44f0-9554-bcd48475ac00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "3a56c072-b6c8-4dc5-b1f7-29a18fc77f0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 115
        },
        {
            "id": "771145ff-d36e-4c88-a388-ec6926e396d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "35c3a5a0-a5d9-40ce-89e3-0719c1d8e312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "7b71ecb4-2adf-47c1-b937-3ac2f006773e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "a0062c22-d9d9-48f8-82a2-b23b403d780d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 120
        },
        {
            "id": "c6d311cf-2e81-470b-85ed-63080d9dbcea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "17d69d8c-8183-4788-8801-55c9d8bcef51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 122
        },
        {
            "id": "78208359-b59a-45d9-b8f8-b298b155f04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 894
        },
        {
            "id": "8a50aa70-720e-466a-bd89-f1a7e305894d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 33
        },
        {
            "id": "634ec997-8055-402f-8836-8f646a01cf2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "f1c672d5-4a09-4cdb-804e-e897e9da84e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 35
        },
        {
            "id": "03bca0ad-4a00-439d-9a7b-cc4b590f6bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "c1efda92-a517-4218-a498-8bf357343c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 40
        },
        {
            "id": "d18e48fc-a787-4e70-8318-f71422e66efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 41
        },
        {
            "id": "d2477ee2-6401-4927-9e9f-aab66f602ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "69a4e29a-5cb7-4763-afa1-f32538d14c31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 47
        },
        {
            "id": "e03d7874-a065-4fd7-92d4-68ac49828343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 48
        },
        {
            "id": "58267e81-aff4-4570-bf95-e9bccaa61e7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 49
        },
        {
            "id": "72393fa3-6342-4021-8218-c625de2e356a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 50
        },
        {
            "id": "d35f7c38-0f01-4df5-813b-23707b2c7bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 51
        },
        {
            "id": "eb1de0b1-07ef-4b34-b2ec-fd484ff493b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 52
        },
        {
            "id": "0efca2c8-8967-489e-b605-aad42a737621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 53
        },
        {
            "id": "1c9748c4-5860-488c-a521-4c0425997b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 54
        },
        {
            "id": "4eb65968-189f-4009-91ba-950467e19af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 55
        },
        {
            "id": "72b42a15-6a51-4c46-a586-e910fa39b1cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 56
        },
        {
            "id": "1f1e6247-3ce5-4107-8cf7-22f44659440b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 57
        },
        {
            "id": "a49701d0-1098-43f3-a955-7cc66127b683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 58
        },
        {
            "id": "a549d136-2d7d-4ec3-99e3-ebc8b80af79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 59
        },
        {
            "id": "75150fc4-0d8b-4afe-a77b-2ecc813704e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "cb1cadaa-3b1e-496b-b2f1-088623d5c046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64
        },
        {
            "id": "22ac24de-f8af-4745-b1c4-815c86db7a1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 65
        },
        {
            "id": "1a63672a-c44d-4ac3-84ca-aed65741da26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 66
        },
        {
            "id": "f052be47-b9fc-4caa-86a3-08137280103d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "8c5d88a1-c9c3-4a26-b676-62326ea88ed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 68
        },
        {
            "id": "af73a265-2b48-4097-9b2e-e97662c687ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 69
        },
        {
            "id": "a065df3b-c092-4a2b-8459-aed32c768d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 70
        },
        {
            "id": "e712a62b-fabd-4d0d-b9c9-d9f11b542c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "70231329-b7c6-420c-880e-54d3df9da0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 72
        },
        {
            "id": "f05044db-4426-4cb9-94b2-1611f5fa8d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 73
        },
        {
            "id": "4bc9f64f-caca-4ee6-ac32-c3872c57c3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "095c6a89-3673-4b0e-bd0f-0e0a1a71ef0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 75
        },
        {
            "id": "e4ecad3b-37bc-4c2f-a4ba-fe7fb55130e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 76
        },
        {
            "id": "c9d7fdeb-95d8-4f32-9b85-59fb57910920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 77
        },
        {
            "id": "a90763ff-b875-4013-9643-df7e4fa70a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 78
        },
        {
            "id": "6ef3167b-082d-49aa-884b-4fda3a1b71f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "bc8ee26f-52c7-4583-bb5a-dd3487c3a8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 80
        },
        {
            "id": "ab7d9f5a-350e-494b-bf4d-9004e02465a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "66373fc3-83c4-464c-ac51-3bb8054d9ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 82
        },
        {
            "id": "48b69b24-4354-4253-8e79-aedc610ca0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "0036d6c5-3252-40a9-8668-b6edfdb468f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "d278c5dc-be97-41c8-92d8-cbb56af0a92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "26ce4490-3fc7-4848-a7df-69aecc67fb51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "92784c40-92ad-4711-b7b3-4319d605ad1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "34d8750f-5dff-4917-95e7-dec79efea1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 88
        },
        {
            "id": "95e1e2d1-844a-4031-9ee2-bd52f07490ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "33e63116-c2f6-4db2-bd73-2384674e29e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 90
        },
        {
            "id": "172e9ec2-9fbc-46bf-ae92-862ae1f74c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 92
        },
        {
            "id": "659ec884-ecbe-4241-8d5c-ba3a32df2f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 93
        },
        {
            "id": "9e12ed65-ad3d-4f57-81e5-9723f268e5cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 98
        },
        {
            "id": "6feed4dc-e409-49c5-95c0-9a905362f7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 100
        },
        {
            "id": "3cf94c4c-f5b7-4ba7-8f1c-88a503b189d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 102
        },
        {
            "id": "01bad224-ed0c-4eda-bf5b-83bdc598a0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 103
        },
        {
            "id": "f63ab724-0335-4671-9e1b-9c679bcdab93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 104
        },
        {
            "id": "c3b58368-8079-4902-b664-9ae26f1392fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 105
        },
        {
            "id": "56a4d573-d0aa-4947-97b7-5bab32199f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 106
        },
        {
            "id": "21983780-5fa8-4ecd-8e10-023e633a0d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 107
        },
        {
            "id": "04d930b8-d83b-4990-976d-a1999d582dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 108
        },
        {
            "id": "4a3fec6a-500b-4eee-9b03-a261cdf727ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 113
        },
        {
            "id": "2be22b46-d6e6-4bd1-86ad-40dc0c6d8a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 115
        },
        {
            "id": "4cbb39a2-36b5-4ac0-975c-b4ac47bd5525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 116
        },
        {
            "id": "e5fcdbb9-5596-44af-a239-0337408c4bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "34bfdf6c-66ac-4b82-b48b-fb2df1189e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "ee277362-5d35-4a19-b8ac-a1ebc8774c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 120
        },
        {
            "id": "4b47a70b-dfd5-4505-acbb-adf3b15c16e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "b6985ad1-4c77-4ccf-9d97-da22882838d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 122
        },
        {
            "id": "95710c55-b67a-4eff-8f12-853b8bf4d2cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "04121c75-3216-42ff-a52c-7bb03bf5122b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 894
        },
        {
            "id": "15d8886b-abea-430d-9aba-bda2d9f7d548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8208
        },
        {
            "id": "b89571f9-6235-4713-8a7b-d91070851fdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 33
        },
        {
            "id": "1aa1bfb0-0bcc-458e-bff1-6129b82829a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "255b634e-11e0-4123-938d-5d1aa51fab5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 35
        },
        {
            "id": "ed0c9b3c-78e8-4630-a187-c168a4e23c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "d7961c42-2f23-4262-bb9c-5f2816f58840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "f6cff60a-0aff-4430-a965-174cad810209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "eb6c157d-0305-4dfe-9d03-bd1a0afdac77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "3288ff90-f118-41d5-ba11-30fc29acf4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 48
        },
        {
            "id": "c5133319-8a0a-4644-8f69-012a1dbd3012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "90c49886-5d47-41a9-af59-1270b7de244a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "27a62a2c-2327-44e8-976e-cb310a92fe9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "ccaa2b9b-bf2c-459d-8085-bd80b95aa985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "66a58bd0-c211-40eb-a691-10c1ac672013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 53
        },
        {
            "id": "e7ca1d19-a0e9-4870-a472-168f20c30ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 54
        },
        {
            "id": "95ef91c6-18cc-413c-ab21-dfcee706f56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "109322c8-b155-4325-b244-e58bd2dddbcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 56
        },
        {
            "id": "4eb36466-5c77-4d80-a078-6775b1dea12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "df8be937-bc54-4d32-b2bd-51830fe88f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 58
        },
        {
            "id": "8b3e9e27-69c9-4b59-b268-83e688630a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 59
        },
        {
            "id": "1152dc38-0335-4141-97e9-b6a0d6d2914c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "0f92c563-bdd7-494b-85ea-582b55ee358b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 64
        },
        {
            "id": "43189de0-8001-474b-afc7-08e40ce9dc0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 65
        },
        {
            "id": "15842a6d-cdc9-44db-adf2-38b00e568c49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 66
        },
        {
            "id": "7d4a2931-5886-43d8-a9dd-65160cba593f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 67
        },
        {
            "id": "b1e15d05-f822-4217-aebc-57836fc45763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 68
        },
        {
            "id": "38f79d38-ab74-44fc-8c32-4dfd74ad0586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 69
        },
        {
            "id": "a22cdae9-f05d-41b7-8302-29062356cb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 70
        },
        {
            "id": "49b7f85d-c2a2-4491-a656-1a8ba3ad1a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 71
        },
        {
            "id": "92197904-cb2f-48fd-8c1b-ed8dee86c0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 72
        },
        {
            "id": "74b4244e-f38e-4ad8-9fbd-d08ca8a8ae22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 73
        },
        {
            "id": "472b10be-e822-4c50-ad81-2771763dc8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "9550cbba-c735-49f3-a5d4-398adb5f6739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 75
        },
        {
            "id": "243c956c-0f2a-41e3-bd08-7f790e192ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 76
        },
        {
            "id": "6898ac18-571d-4399-9e15-1bfa901b8e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 77
        },
        {
            "id": "2a9ceda3-3c31-4342-8015-35c6f809d6a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 78
        },
        {
            "id": "842ce776-d722-4283-8ee9-b06789270019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 79
        },
        {
            "id": "e9b499a0-fbd3-4a3e-a7f5-99af6f034700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 80
        },
        {
            "id": "508cb38f-db26-481e-9d7c-7a3ed7ae6518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 81
        },
        {
            "id": "2ee24f2a-6843-493e-9cdd-c101c15a35bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 82
        },
        {
            "id": "5e47b707-edcb-48f0-a374-7227ec290c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "14348206-cc20-48c5-924b-7621169bae19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "73df8417-e910-40a6-879d-5ff9d439198e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 85
        },
        {
            "id": "26b1ccaa-4edd-4b48-a4ae-b9d3d551d4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 86
        },
        {
            "id": "3f69b880-a8d8-4c95-8347-398ff82d18e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "5fc3d4ba-f0c7-4d39-9771-ce9e554ba64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "07ce0ff0-4989-4340-8b22-741c51e0620f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "8a7b0b81-0820-4554-b636-12d04e524472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "206d13e1-c288-4d19-994b-c3978a02ffe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "b6f3f181-faa0-4965-ad37-5c403066673a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "72debfdd-df00-4cc8-8e8f-948087a35fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 98
        },
        {
            "id": "585be54b-760a-40c3-a2f1-d6a601cccc59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "19df461f-d13e-40e5-9f02-73a90718fd55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 102
        },
        {
            "id": "a1363493-9b8a-4572-a933-33a48b8fbcbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "16a6b675-3d4c-4771-8728-57ad20bde0a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 104
        },
        {
            "id": "443d283c-5e3f-42f7-860a-5fe9a676f560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 105
        },
        {
            "id": "89b2e49d-a773-4667-9323-8619a2608666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "119a52ba-7010-4088-a5a9-13ecad72681c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 107
        },
        {
            "id": "05c4acf4-0f3f-4cdc-b9d0-16e3e1e32c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 108
        },
        {
            "id": "3405c84a-2b93-4a68-9e96-7a12d4430431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 113
        },
        {
            "id": "7ebd9ef3-846f-4a5e-a0b9-c82e9b0e56b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 115
        },
        {
            "id": "f3a02ef4-07bc-4a60-be7f-aedd8e4d6663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 116
        },
        {
            "id": "0cbee437-c90b-4cf6-85e7-ab92fb575a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 118
        },
        {
            "id": "fd8e6903-1a9c-4c3d-85d5-fc2684bc9556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "4b902f46-c62d-4ebf-8b14-0c1fe482b607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 120
        },
        {
            "id": "32a5d66d-bd5d-4ddc-8005-3c4e046472e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 121
        },
        {
            "id": "b818f73c-a99e-40e9-8cbc-4f38b84eb4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 122
        },
        {
            "id": "dd143aa9-539d-4c97-876b-5509be468185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 894
        },
        {
            "id": "940c6131-190d-4d62-95b3-e8720f136de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 33
        },
        {
            "id": "69d33a08-9253-452a-a583-4b679738e1f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 34
        },
        {
            "id": "af2aafb2-d55b-455b-8e15-1fc40774530d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 35
        },
        {
            "id": "10b194e8-595f-4e39-a5ed-5c9f1b562091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 39
        },
        {
            "id": "b38dcfc3-b17b-4e3b-9c32-af97c767331a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 40
        },
        {
            "id": "b0a0ba01-1665-494c-9042-4d0fc2d73a93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 41
        },
        {
            "id": "ebb60a47-0ce7-4409-8474-cf98c2b2fc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "08776212-eae8-4036-94da-bcc0d68c374a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 49
        },
        {
            "id": "a19ac425-dd38-4305-8568-8f64b6bdc1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 50
        },
        {
            "id": "7b900cef-e2ba-43d6-aeac-a3e3a6aafa03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "d4f30594-9a14-464e-a7a1-0bff61af45f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "a2f538e3-d9dc-480f-b692-5c091bc56851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 53
        },
        {
            "id": "48f789b3-12fa-490d-8855-39e9ebbfec24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "8e1a41b6-2c7b-4c8f-9df4-21a6c892f9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 56
        },
        {
            "id": "2cc97660-7720-4d24-b2af-1abd8c4fe0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 57
        },
        {
            "id": "b1e2ab16-f274-44d4-a039-e5c36e8424f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 58
        },
        {
            "id": "0b9a62e5-4113-4127-a3b5-7d0b7b76880d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 59
        },
        {
            "id": "7fc998fd-608c-4906-83cc-eaae1716b807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 63
        },
        {
            "id": "f0fa073d-6a42-4a2b-b352-574ff3a40e36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 64
        },
        {
            "id": "82214323-bbdd-47fb-86a0-760990636e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 74
        },
        {
            "id": "84f0ef10-04c1-4f1a-a8d0-e415ab42a399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "eb8ae008-3639-4bba-b29f-6291b5ed2767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "44ef1bc1-93ef-4be1-9fb5-01f072a8fb6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 86
        },
        {
            "id": "ab266e4f-ee67-4e92-93a3-45cba15d1e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "346f91d2-4210-4b46-919b-21ca18ffe555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 88
        },
        {
            "id": "1f6a25e5-8ec5-4e7b-b47e-60f4fb7c5040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 89
        },
        {
            "id": "25cfd6ec-5ae8-4845-b584-962a963b1b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 90
        },
        {
            "id": "ca8d7ae5-2357-43db-b97b-1cf07ff02efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "5ffeed82-7d2f-4609-947f-e88215cd3679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 93
        },
        {
            "id": "1711184f-bb02-4bc1-8273-f252dfa84359",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "0bb61871-2f8b-4932-b1e6-1de80f0e9ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "db687525-227c-47c6-abb6-09b29d925218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 105
        },
        {
            "id": "959a8f10-1688-4888-b3cf-91cd5e7365ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "f36a0874-3d18-4305-abd9-d0f3816f35eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 113
        },
        {
            "id": "f7c23a40-8757-4c55-a1a9-450947e53a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 115
        },
        {
            "id": "319e9b16-d51a-4055-9e2c-b500b6d52da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 118
        },
        {
            "id": "7d0845c1-3c2a-4344-9595-1ce0a9184d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "e61c6dc8-1a06-40ba-9398-585ccc126ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 120
        },
        {
            "id": "e2ebd556-8a40-4c57-97c5-d56731595b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 121
        },
        {
            "id": "098ce8e7-72fc-455b-81d3-4e8d281ec7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 122
        },
        {
            "id": "a574bdad-7eb4-4c13-a19f-ecb799a51e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 894
        },
        {
            "id": "45dfaccd-e2d2-4d16-9fb8-b501087ec070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 33
        },
        {
            "id": "9de7bbfe-85ad-4fef-a67c-2282a6fb6426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 34
        },
        {
            "id": "bd947d5a-f8a5-4664-ac2e-0fb338cac67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 35
        },
        {
            "id": "5f083b75-044b-45a0-a843-e510017103d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 39
        },
        {
            "id": "56e1fb79-874c-4f14-b397-dfd85c97a35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 40
        },
        {
            "id": "fbf73001-9c1c-4c6d-85a8-27c48c44dac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 41
        },
        {
            "id": "3c17499d-aea8-4dd5-b9a1-24706d88ebd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 47
        },
        {
            "id": "a1a7e087-c033-4f8b-a2cc-28e96623cb34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 49
        },
        {
            "id": "94d9f4d0-2fd7-4715-8be4-191c7cf25383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 50
        },
        {
            "id": "3dbbaec3-412d-4abd-8e52-cc161b9a430b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "27b33b1e-4a76-4e05-9e16-c68a88671a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 52
        },
        {
            "id": "a36b699d-7517-4f3e-b456-9ba44c83746d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 53
        },
        {
            "id": "5f40f47e-6e0f-4dd8-b60e-9a64b0e04534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "5dbe0852-1ad3-45ec-aa09-2cb81a008725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 56
        },
        {
            "id": "519c9f0b-70c5-4816-abb1-8b04f30c291f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 57
        },
        {
            "id": "e54eeeb9-b587-4e6f-baf1-471c70d9e58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 58
        },
        {
            "id": "5ee41556-63f8-43ea-a64c-a7fafd158669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 59
        },
        {
            "id": "eba50402-90a8-4753-a3bb-568b27d42e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 63
        },
        {
            "id": "50774821-9ca2-4122-aa4b-990ea402a31d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 64
        },
        {
            "id": "88a64e7d-6932-4fb8-88b8-95d35f763510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "897998fd-9b55-48eb-b7da-a1b4e8a1c31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 83
        },
        {
            "id": "5814a313-61ae-4e74-a56f-9d69df54f291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "c307ac00-1ca7-4273-a7ac-30a81e32fb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "99298e52-adb0-4ea6-be6a-b896b5524dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "9f886756-46e9-4b80-80df-8c4041924220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "dbc78294-396b-46b9-9d5f-a373c35ec95a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "57b973ae-b335-446b-8d37-7f764e423b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "2000d5ee-2f0f-4493-a281-a371358b8556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 92
        },
        {
            "id": "3c1da8e7-5af9-4acb-a961-df0879bf12d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 93
        },
        {
            "id": "32c68a14-4c05-4598-8554-0c015136aaf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 100
        },
        {
            "id": "5f98f30c-1474-4364-a26e-038161543c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 103
        },
        {
            "id": "a773eb7e-745d-436b-b8a1-27544cd9f5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 105
        },
        {
            "id": "d1e16eaf-8546-41fc-a5be-0c676219ee29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "95cac2c3-c6d9-4db8-b180-6438d71cd252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 113
        },
        {
            "id": "78efed6b-8625-465f-ab2f-c6ced8b1b8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 115
        },
        {
            "id": "14f7fcf4-c1a8-47a3-a607-aa82176108e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 118
        },
        {
            "id": "7d250b74-58e2-4c72-90f2-66bcb7fbecd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "22fb4521-8552-45fc-8872-ad9390a91585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 120
        },
        {
            "id": "d2880f56-b57e-4829-9790-96b8fea2ba3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 121
        },
        {
            "id": "46434113-d75d-4726-a459-8475833b2356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 122
        },
        {
            "id": "a1374e39-9b1a-4d32-8f0d-f156c76f741c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 894
        },
        {
            "id": "74f3c9d6-d89d-4fef-aa39-c3f9acacf870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 33
        },
        {
            "id": "f81c184f-f1d0-42b8-97bf-84ea6aae140b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 35
        },
        {
            "id": "1d3da190-4314-416f-833c-df9ff7c4cd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 40
        },
        {
            "id": "04b30d1f-63f2-4e5f-b7b4-49e119cfbc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 41
        },
        {
            "id": "18ffbad4-71cb-4071-8833-5ec99e94f35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "a0f7c011-8e2b-4bec-a3f5-2d81bf224903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "a2d01527-740f-4175-8686-04bb0920128f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "ec7be274-6c0f-4607-b7b4-3ed7100f6cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 49
        },
        {
            "id": "0b25b272-42b2-4f7f-9e27-a8c81ccf3059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 50
        },
        {
            "id": "a8c132d9-ff02-40cd-9ff9-28504cb9a60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "246b2adc-b0b2-4218-946d-7601bb2e51dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "90d964d4-0bc2-4545-af9d-daccc6c1c97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 53
        },
        {
            "id": "9518019a-a90b-4e24-aa92-0500574273f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 55
        },
        {
            "id": "7b2831ca-ccad-4d83-aa84-f358ad77c15c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 56
        },
        {
            "id": "9a7578aa-9b32-4b2a-91f5-70147e98c277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 57
        },
        {
            "id": "546609b1-4ef7-467d-9e46-f0178107872c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "02991425-6ff4-450b-ab9c-852d87d0d18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "ea7f70e5-7247-42e0-9a08-1d86c887654b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 63
        },
        {
            "id": "0102baef-86b5-4995-9e60-7fefbeed4e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 64
        },
        {
            "id": "a66793ae-84e7-4a85-ad92-45f466f58ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "8f023770-74d3-4c0b-9e97-64ca047d083f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 83
        },
        {
            "id": "a6194a57-2da8-418a-aae3-cb7339efe980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "c84f7ded-7f73-4f4e-bd90-93bbff853b46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "be521f9d-bca6-479c-8c46-f8ebd074ea3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "84e2446d-d549-46cf-8ea9-58203aeddc0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "f8d2bfe7-4e2c-4ee1-ac69-9d254f636e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 89
        },
        {
            "id": "22aa5f80-a0f2-460d-81fb-13988d6f23d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 90
        },
        {
            "id": "9bea62f7-9b5a-40e2-aee7-5a8e984bf153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 92
        },
        {
            "id": "ba4e6297-5f2b-4326-a887-09d7a097ce35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 93
        },
        {
            "id": "f992fb09-14a6-4697-8d08-95778e9bff6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "5648db90-d0e3-4d17-a97e-d17436cb8afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "8b9a8643-022b-494d-b9a0-abf8072e88e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "0ea0cc3c-3941-4cf9-a4fd-9b60defb015c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "3aed0229-a649-4d92-94bc-6783adf81c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "2d1ec2d1-8974-462c-a8e0-6d7acfb94a69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 105
        },
        {
            "id": "f493f4d8-bd7b-47a6-bbf0-70956dc2d0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 106
        },
        {
            "id": "4bf6cb67-b05f-44ec-ac45-b80e933f78b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 109
        },
        {
            "id": "1a9e5d97-563c-4d97-bc0f-85db072546bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 110
        },
        {
            "id": "b98a6d27-1e8a-40d3-acc6-8c14f9026c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "58e8329e-1bb2-48af-9bbd-3731f83b39fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 112
        },
        {
            "id": "67a400cc-e595-4529-9132-c4956c45a20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "be762c3f-b1a6-4b4c-8775-a5fe0c6763c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "6960531d-4828-46c2-b204-9f29931e87cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 115
        },
        {
            "id": "53825a74-b589-4979-b351-694462d9bb3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 117
        },
        {
            "id": "00b8d414-86a8-41a4-8529-3f0a8da9e4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 118
        },
        {
            "id": "048f130f-5a59-498e-8284-c69e48b0883c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "46aa7a31-69c0-449e-9aba-4210ea9d16a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 120
        },
        {
            "id": "509be79c-b663-4eba-9ad4-19dcef3f1259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 121
        },
        {
            "id": "23951131-a4f2-47bb-8076-aab744136702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 122
        },
        {
            "id": "67dcd69c-41bf-4147-9568-cb5b08213a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 894
        },
        {
            "id": "d42c6804-a7a3-488c-8c53-2151c65584e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "f38fdafa-f9a5-4790-a73f-b67cef99bcb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 34
        },
        {
            "id": "a781840c-f464-4b67-8457-2cb8272d4686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 35
        },
        {
            "id": "4d8d9493-57e2-41f5-9baa-df4022e76d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 39
        },
        {
            "id": "f23198a3-0687-4b46-b7ad-82cfc34c7cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 40
        },
        {
            "id": "b590ecde-c986-4a3d-9aff-db4c5158028e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "cc266dcb-9e2d-4c82-836a-80f0d3cded33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 45
        },
        {
            "id": "ffe14ccc-9e38-4977-993d-c381b8bf6e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 47
        },
        {
            "id": "90097865-421b-41e6-aaa9-47e3486a4181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 48
        },
        {
            "id": "67e16915-3ada-4ecd-83fb-1e0b8947ac86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 49
        },
        {
            "id": "efa6ea21-b0b3-448c-a36c-b318e441a311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "d9d3307e-7e1a-423b-82ad-f46018f73cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "41acd517-a20d-4c20-bc89-2a9b25cf5bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 52
        },
        {
            "id": "f808c358-0f9f-48ba-a51d-a8909855a377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "9e6b7772-1dae-4c19-a202-4cbd96b798b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 54
        },
        {
            "id": "2f33f34f-9387-42b0-9f8a-33ddb7025de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "a06c7724-34a6-4372-a00c-ac0dbedab134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 56
        },
        {
            "id": "fd86c431-5f25-4fa8-b67d-70daeb350e0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 57
        },
        {
            "id": "c1f10334-d19d-4736-8a69-694a48b49414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "5f976c64-9845-4605-919c-2c370d98bcb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "f0c17542-f714-4c66-b41b-d07a67ee090d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "1b370916-b08c-4d61-a48c-74c55a8757e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "8ea98237-3b11-44d3-b418-9a40b637ef20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "0166dcd0-81ec-4645-840f-feee7a9ccd0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 66
        },
        {
            "id": "53bd71f6-eab2-46cb-b076-e4a754be0f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 67
        },
        {
            "id": "29285cfa-1de0-4e24-b230-2f70056f9919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 68
        },
        {
            "id": "1ca9e270-4cb5-425d-b98a-48998454207c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 69
        },
        {
            "id": "a9099839-1bbd-463e-a316-f781e3c83679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 70
        },
        {
            "id": "a636c1ac-57e1-4be4-a950-27d15afbeab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 71
        },
        {
            "id": "7630faaf-4ed0-4b43-a3c7-c735a9a4c79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 72
        },
        {
            "id": "5a641f11-de2a-4781-8c4e-8c170d88825e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 73
        },
        {
            "id": "2d97e0b5-411d-4006-908d-8ad05ec15a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 74
        },
        {
            "id": "1cf665ee-117e-44dc-aab2-a684945db69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 75
        },
        {
            "id": "51523cc8-d9a8-422d-95b3-7ae85aade7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 76
        },
        {
            "id": "2a1b97a1-0676-4e03-9e29-01d5b965ce5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 77
        },
        {
            "id": "df2bb41e-f11e-4bf7-ba78-fda5801079ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 78
        },
        {
            "id": "2f179c6c-ba11-4726-b68e-2c1d6c19cf1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 79
        },
        {
            "id": "2d3d23ab-02a7-4db3-9392-ae5317ea0bfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 80
        },
        {
            "id": "8591f113-72bb-402b-87b6-fd69d637765c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 81
        },
        {
            "id": "45a68a63-ec58-4017-9905-8b7bee991c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 82
        },
        {
            "id": "ebe4a585-57e6-4ad4-bed5-cb131ccdd336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 83
        },
        {
            "id": "88cbc453-a233-4bcd-a1bb-fbec6d6c44c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "155508d4-2659-408a-9faf-076f94678386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 85
        },
        {
            "id": "877af026-ecc1-4cdd-9cec-0aeaed397fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "88a2cc7c-a9aa-46b9-bd92-16fa96e60446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "9b8ab0b4-3fee-4e19-96ff-724ee7fa54d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "b9169e52-d8d0-4ada-b273-ab6c3f500e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "99c5d7bb-dfd6-4989-9eb0-0cac2530111b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "14b508d8-aa23-46bf-8e96-ffd62dae0479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 92
        },
        {
            "id": "4a6a2ae3-a27c-445b-bb28-3e36fb8b3324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "db3c162f-4cc7-4d4c-9162-2cd90526debb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 97
        },
        {
            "id": "5e2706f0-436e-4c50-8394-3272db1ab3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 98
        },
        {
            "id": "0c569d30-6863-40c7-bb48-17fd55d2159d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 99
        },
        {
            "id": "029409ef-7393-42b7-86d8-c1415db21327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 100
        },
        {
            "id": "2029d390-686f-4b40-a30b-27248f677536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 101
        },
        {
            "id": "1ccc2732-5153-4a4d-b600-f54b254f4db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 102
        },
        {
            "id": "f6e20405-9318-493a-bf9c-6f1febeb3d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 103
        },
        {
            "id": "87713291-77dc-41bf-b5f0-3b5d147c0d53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 104
        },
        {
            "id": "d92507af-813f-44ff-aade-8a3cf42d6992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "581ce112-59f4-473e-8cb4-c365f7606389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "32b4a1e0-1df5-4489-9d6f-97a91a9df9b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 107
        },
        {
            "id": "164ecbaa-e5e6-4f5a-9a0c-7145e2bfc64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 108
        },
        {
            "id": "e9e6a55a-b1e2-4dd6-be30-d6d81052d3a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 109
        },
        {
            "id": "15aed7ba-5a52-4c1b-b806-61c3bd46f88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 110
        },
        {
            "id": "cb1aff3f-e7a7-4ec0-80c1-23da3504f1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 111
        },
        {
            "id": "48d5682c-a55f-42b1-8cbe-746352e92f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 112
        },
        {
            "id": "c4e0442f-5d45-44ea-902e-b963bb502e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 113
        },
        {
            "id": "7f02e392-4550-4616-a043-da68a162b68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 114
        },
        {
            "id": "3d4a9f02-5464-4324-af97-e3ac194c635b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 115
        },
        {
            "id": "97e1cd7b-4b4b-4510-be07-05c872d19484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 116
        },
        {
            "id": "df5cc18f-25f2-4b64-9fa3-8ae7c8301181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 117
        },
        {
            "id": "d0d79703-ed05-4bda-944a-14c73120753f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "3aeecc6a-eab1-4eb0-9d2d-6284296f987a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "97ffc5e1-2415-47b1-8750-ed6bbbb631c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "177b65bd-9662-4b6f-8beb-4b9e86f7758d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 121
        },
        {
            "id": "b6c47e2b-de29-4120-ac4c-e08f3da2b5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 122
        },
        {
            "id": "50b6f6bd-30c3-4a5c-9661-c3d1959aebb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 173
        },
        {
            "id": "cf8a9acb-d0e7-4840-ac46-315fc11a345b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 894
        },
        {
            "id": "f1de3472-cb66-402a-9b3c-af6931093848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8208
        },
        {
            "id": "fa7a221d-9514-4a65-ac80-405a150f7115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 33
        },
        {
            "id": "6e8e15a0-6e76-4564-bcf1-853634881b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 34
        },
        {
            "id": "01f4f663-33bc-4bc2-841e-af835a996499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 35
        },
        {
            "id": "b9528bff-d381-4046-ae0e-e30a7fcf452d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 39
        },
        {
            "id": "87331c57-40dd-4778-9169-5d489a26f0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "d060310d-7802-42bd-bfbd-572e684df250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 41
        },
        {
            "id": "8b1b35c3-bb19-4e8e-be00-9c7a328e75c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "3193e005-df31-4f0b-a8d0-ec96e5c2c19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 48
        },
        {
            "id": "33a9af47-f7fb-49ee-a586-b17bec1dd560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 49
        },
        {
            "id": "3925e2e9-0654-47a1-a8be-82092792aea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 50
        },
        {
            "id": "d57907c6-0073-4a13-8e3c-b6fd2125235e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "da22550a-e083-4471-a298-d50d4eea9b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "089da5d0-d5bb-47ea-b90f-62a6016f0112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "f492648b-3575-42ec-b757-cbf8d99f7a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 54
        },
        {
            "id": "0cecc6ca-512a-4f49-83f2-25b8489eec5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "5206e203-9c5f-4753-a30a-967ff8454ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "88960e14-31b8-4be4-87ef-c5b984d1a164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "19015b17-f49b-4531-9644-5813cc96e586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "b556a436-bf5e-4f58-900f-c44072c6b802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "28e2634b-aa1a-4d02-8765-80fa21be5ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "2eb1e40f-381b-4d29-ae7b-838b29e6a134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 64
        },
        {
            "id": "4f9a72e2-414c-4a94-8604-43ef5c2784ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 65
        },
        {
            "id": "ab85b4d2-58bf-4eb9-9f59-bbb10950ba58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 66
        },
        {
            "id": "f5917469-1b6f-448f-9e4a-6e074460febf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "327f1b45-29bb-4aa1-8454-a65b7e28fa54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 68
        },
        {
            "id": "e7a92bb8-6187-429f-b921-8d6ce8216693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 69
        },
        {
            "id": "5a33bf68-b4e0-4701-aa09-e95d111a2c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 70
        },
        {
            "id": "8f63ad6b-93cf-446e-9692-957effff56ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "56887d4d-575d-4c13-9432-330106672bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 72
        },
        {
            "id": "a81ca21a-48de-463a-b87a-c147862b5c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 73
        },
        {
            "id": "585ec5ed-66ec-49f7-8b0c-d5ecacf55033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "70f8417e-65a1-49fc-87cf-a74b59cbb0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 75
        },
        {
            "id": "b3c26390-9c70-4131-b327-018d9258d99d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 76
        },
        {
            "id": "95daeb5c-d368-4bc1-a697-92fa89c652f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 77
        },
        {
            "id": "9d28df72-b274-49d0-975a-13835ec69ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 78
        },
        {
            "id": "918754b2-d813-4bad-9ddf-968d90013adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "b5159849-1658-425e-8662-bb7702bc3bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 80
        },
        {
            "id": "6c5cabe2-62ed-4fbd-81ad-71dd7b410027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "ba947c2a-0794-49a5-a9ea-1f50f2cb185d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 82
        },
        {
            "id": "4c1ea540-3376-47aa-bd3c-28ff2c4648c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "45eee432-90ef-4181-80dd-014e28fa06ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "0cf4fc8c-e811-4a1f-a152-07eb9ef020fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 85
        },
        {
            "id": "dedb7ebd-3f96-47d7-b064-1ccb6dda8c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "b5fac7db-6be9-487b-894e-e33113945152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "5736088a-8b0a-405a-b6a3-e326309f5f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 88
        },
        {
            "id": "5634794f-dcc5-447c-ad2c-57376f10bbf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "82083042-325a-40e8-a821-6aeb06740e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 90
        },
        {
            "id": "e2c36525-ee05-45e9-9844-68977788bdf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 92
        },
        {
            "id": "ab00821c-e33f-496a-9551-c85138060775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "4a89846b-9c50-415f-a7c1-96a6947b9267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 97
        },
        {
            "id": "480b868d-d464-4d00-baba-5739343147dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 98
        },
        {
            "id": "a9ac53d1-d6d4-42d1-ba17-79ed8e291c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "ddb899cb-5b86-4d24-a810-034cd6f9c0a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "af71302c-85b1-4f97-941d-bafbc2764ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "e330121d-6261-4567-92d9-de3d5d9bd1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 102
        },
        {
            "id": "39665cf9-4388-4041-a206-b7a839b003fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "b08829bb-3af4-4046-b530-88f48255cf3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 104
        },
        {
            "id": "22d9fb9e-f2e0-4bb1-a825-90b903d6a8a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 105
        },
        {
            "id": "db64a08b-3c80-44d5-85aa-2cc6fb1ea796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "828e425b-0693-4759-ade2-8aa3c6e18167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 107
        },
        {
            "id": "9d46d155-3f5e-4dab-98b9-fdce99b8a27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 108
        },
        {
            "id": "b614ca98-8011-412c-a62d-e8c224908f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 109
        },
        {
            "id": "63725c3f-2ed2-47dc-8eaf-fdf76a15c608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 110
        },
        {
            "id": "c706f704-fe98-48b3-9b36-b17d1cb4e2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "38335c53-1c92-45b9-a79c-c62521c685ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 112
        },
        {
            "id": "92429c61-c53d-4d08-adb3-deba02f3b5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "15c290e1-6baa-4516-a702-44f8a9ab1c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 114
        },
        {
            "id": "aeec9973-a625-4b47-a3ec-585000d102ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 115
        },
        {
            "id": "26da213e-a782-4e58-91a2-2f6a205331ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "1020f39f-e83e-4be9-8b28-777e7d9ed3ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "ea5dd545-88a0-4deb-be24-2c226982b7f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "58e8f29d-1d41-455d-9c3b-bff17e69267f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "49b4cfaf-003d-46e9-815f-0cbdc50def78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 120
        },
        {
            "id": "fbb29c61-bbce-4c6b-9034-a61a0fd2eb48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "cf35905f-e278-413d-bb24-0d5419939969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 122
        },
        {
            "id": "6e7fbf07-2ee0-457f-808f-eede66163ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 894
        },
        {
            "id": "b5e7c45c-0f15-4aa5-945b-cdd1050f214a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 33
        },
        {
            "id": "775faa3e-e8dd-4f0e-887d-3b0efc1a125b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 34
        },
        {
            "id": "8998e428-ecf1-4943-ae58-d575ed91d960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 35
        },
        {
            "id": "79e00e63-5822-4078-ae78-b24931bf53b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "5dec7a7f-c897-4961-94bc-95a07f7e9053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 40
        },
        {
            "id": "c33e2b79-264b-44ca-adb7-a0f8b097840e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 41
        },
        {
            "id": "01a66e0f-7455-4092-b4f4-b795ba7a32f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 47
        },
        {
            "id": "24c5a8fb-6cb3-4b2d-bb2c-675a5c299fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 48
        },
        {
            "id": "0d83438f-c309-46c4-a95c-696ebf7af17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "4885108a-6450-4fac-a233-836abb426b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 50
        },
        {
            "id": "ff29518b-7b32-4216-92f4-036fc18e679a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 51
        },
        {
            "id": "534692e7-7258-4fcb-bdcd-c1a530b67df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 52
        },
        {
            "id": "032c9d5c-9077-4ff9-af25-18b0bb0de112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 53
        },
        {
            "id": "8ecd7556-ead0-4cfd-9705-09082557e23b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 54
        },
        {
            "id": "611e5979-66e7-4fb0-b20e-f71ddc79a98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 55
        },
        {
            "id": "952ef773-de2a-4183-b8fa-59d6342e24a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 56
        },
        {
            "id": "aff74033-00a8-4605-9295-5bd46dbc7393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 57
        },
        {
            "id": "f3c404b4-dd2f-4240-9d70-291d957d2d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 58
        },
        {
            "id": "77b31b8a-7800-4011-bdf5-4a37a7c66ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 59
        },
        {
            "id": "2af9c51c-c243-4631-91b1-705be32c318c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "6503d71b-f81f-43df-9f08-f241a5c82d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "5053c11f-f61f-4dba-be52-e9e40f105b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "fe712dad-c88e-4497-bdee-015c23209824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 66
        },
        {
            "id": "c8e7c6e2-65ab-4b71-989d-f1c7e37648b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 67
        },
        {
            "id": "0260640d-2410-41bf-b287-acc00ada770f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 68
        },
        {
            "id": "76f9c348-8523-402a-a871-92799a4aa527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 69
        },
        {
            "id": "65934056-efd8-4122-87b1-3667371bd950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 70
        },
        {
            "id": "cbebdc6e-935b-4ca0-9678-2aef16606932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 71
        },
        {
            "id": "ad7eb66f-a57b-4632-81af-e38c15a629f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 72
        },
        {
            "id": "cc3645d4-da5e-4c63-8b04-c34e6cd2232a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 73
        },
        {
            "id": "34bd0c29-ac7a-435c-a46b-4aab4aa81492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "993cf255-06b2-4b7e-b22b-cd1e49ddfe30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 75
        },
        {
            "id": "1b3cfafc-2553-4a23-b85f-2b2d646f3fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 76
        },
        {
            "id": "2f802ecf-01a1-47c7-993c-836923a41091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 77
        },
        {
            "id": "d00e0f64-181d-4707-94f0-0124b527145d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 78
        },
        {
            "id": "af69757d-cf98-46f5-8482-cfadacc4d22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 79
        },
        {
            "id": "4bb7b4bb-b909-4c88-b41b-317ea3580614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 80
        },
        {
            "id": "72916dcc-3954-4966-b60c-0710be7f9bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 81
        },
        {
            "id": "888cd8b8-449a-409f-96f4-cbf2b3f6f6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 82
        },
        {
            "id": "2afe973d-7897-428f-9b3b-381e3f47bca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 83
        },
        {
            "id": "4f1c8232-a0ea-4bb2-ad34-b1c2a7dc9bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "155c61a1-2484-4819-bb08-8fe3090e6439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 85
        },
        {
            "id": "3220963a-0e92-4e47-949f-2ece3ca6ebbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "bc462da8-1120-4a81-b452-3196991daf67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "e87e8afc-5e6c-44f3-b8ee-cf445149b5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "237e3d9d-9dd9-4540-98d1-1d1eaec5f127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "eca50fb8-48f2-47dc-834c-94741e6dcfa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 90
        },
        {
            "id": "90a3445a-4983-4d22-8a1a-435b977a0ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 92
        },
        {
            "id": "6ccf534b-5ed5-4b34-9cc9-16dd5692b327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 93
        },
        {
            "id": "72fe62e9-a1d3-40ba-b91a-df579f2b3bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 97
        },
        {
            "id": "7e46e5c6-35c2-4ec4-a1ed-1cbddd2afa32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 98
        },
        {
            "id": "fec907e6-2771-4e67-a417-051340f7bab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 99
        },
        {
            "id": "97d8586f-58f4-4b65-bbcd-d91d11d3387e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 100
        },
        {
            "id": "6a2f4976-87bc-4645-b164-83875de9bb40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 101
        },
        {
            "id": "9482270b-3cfb-45bb-88dc-803ef9263ec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 102
        },
        {
            "id": "04d5ad3e-b970-4f2d-ae1e-f0c2d2b45c69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 103
        },
        {
            "id": "2ca7d5c1-972b-4005-bbd1-b98bf903c9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 104
        },
        {
            "id": "27ec838b-48a5-40e9-9517-ea7ad039981c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 105
        },
        {
            "id": "f628b649-0db4-4763-a047-fea79f7bcf93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "6ae7f747-5c99-41ef-9496-fccea6ac34d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 107
        },
        {
            "id": "ce7b7726-1a23-4561-8d69-2564ac63fce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 108
        },
        {
            "id": "91e63855-0485-4a73-ac67-b6061a549c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 109
        },
        {
            "id": "225a578d-2bbe-4085-999d-bd7682166ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 110
        },
        {
            "id": "1563318e-32ea-474f-95c4-ce14fbc02009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 111
        },
        {
            "id": "835a2d5b-64e1-4721-9ae8-b0f247bb4d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 112
        },
        {
            "id": "336b2b9e-3922-443e-84d7-f29bf238843c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 113
        },
        {
            "id": "b7f1282b-e6c8-4fd7-813a-1ba6f5bf76ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 114
        },
        {
            "id": "b0c6e09c-4391-4f4f-9bce-dd11683c2975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 115
        },
        {
            "id": "1bc1cf0b-35c2-4077-a536-76ca1caf6020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "71d9ab69-f92a-4edd-a4f8-1d08e710bfa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 117
        },
        {
            "id": "6e4afce4-e5ec-42bc-8433-9f8aeea194b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "c6e274c5-2841-4827-a6e2-e063a34c20a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "0859ea25-ec67-43a5-a1fa-c72b177f1206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "f328948c-3203-4cc6-b156-a208207119ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "c0440c9a-35e8-4c31-9c58-a248b0c97f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 122
        },
        {
            "id": "f0e710a0-0464-44f2-bdcf-8987842cb5d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 894
        },
        {
            "id": "cb9c1ced-903c-4ba1-9fda-bca924034db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 33
        },
        {
            "id": "4257bea1-0b0b-4508-a1f9-cbb75a0659ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 35
        },
        {
            "id": "a34d844e-8651-4931-8679-591b3fe924af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 40
        },
        {
            "id": "c2684fde-2815-435d-8158-c1f66b488430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 41
        },
        {
            "id": "a9ce876b-13ce-4ba5-ac6b-2f2da0eb734c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "6de71cd5-45a8-40fa-a927-806ede3beaf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "fece432d-692b-4984-905b-f0cd0ca8cbd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "80af9048-2caf-4d5b-b82e-56d7f408176a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 47
        },
        {
            "id": "0a8f0433-eca1-4e4f-85df-4c1e2be81c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 49
        },
        {
            "id": "4e99e33a-a901-43f2-94d2-79987d450514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 50
        },
        {
            "id": "1bb06146-0c74-44f9-a35d-51568b5b8e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "f692c73d-57f9-4cf6-9cb2-ac9f95d9894a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "98971a1b-ad70-4d03-b9bf-18bfe2a01c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 53
        },
        {
            "id": "54d8fbe7-cf1c-47e5-a322-96ad0c116724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 55
        },
        {
            "id": "29ce8462-8c55-4dfd-8ac3-cc812d2235e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 56
        },
        {
            "id": "8e57ff73-7279-44ae-9605-ddc2fd8921e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 57
        },
        {
            "id": "0dd19391-b714-459f-816b-8de7f3adf5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "bd6e8dd5-abd7-40cd-8da3-799d9e815615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "9089fd94-af1d-42b7-80ef-0cbcd62c74aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 63
        },
        {
            "id": "6681d1cd-960b-456f-9279-f5db3a6d04df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 64
        },
        {
            "id": "24da9758-eb91-4b93-bb7b-2569d547a8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "8d009fdd-7261-465a-926c-8acf930b5eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 83
        },
        {
            "id": "545f38a5-67d7-4fc7-a330-fe445f4af3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "2ad6a882-653f-40d6-a626-2c8e116517ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 86
        },
        {
            "id": "b521ffcc-d8ca-4963-9f03-1dd7cbbd7190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 87
        },
        {
            "id": "d74b8c0b-39d8-4334-8397-037f61b6d775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 88
        },
        {
            "id": "a9ca3452-d478-4b55-862d-6ecf9690cda4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 89
        },
        {
            "id": "010c8909-8292-4c97-85ec-bf5dd4a90c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 90
        },
        {
            "id": "d73b6067-56ac-4c08-9596-ace0a3bce876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 92
        },
        {
            "id": "ae80f8bf-3568-4109-89a0-5eb306bb088f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 93
        },
        {
            "id": "be483de0-bfa7-4ae2-bf7e-e473c8e09cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "ace5e4a8-6b46-47be-9822-345151d44fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "70ac166f-4d57-4aaa-ac46-52ca4a09e95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "d6297a9b-38fe-4427-b7a8-d4246d69287b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "ff5aa042-05f0-42ea-b5f1-2a3961509a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "fe7de6fb-b136-42b7-851c-474f99f9f03c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "9b751eb1-6f6a-40fe-abcd-bf319546af43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 106
        },
        {
            "id": "8361789d-1109-40a8-be46-366a546d1284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "a5f1c688-2f55-4046-a066-ae6a9ab2ec61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "c32ae427-93da-4e50-82f3-d6f0c3c10b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "b56a9589-1971-4cfb-bf29-3c6bf1a02429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "c0741f4f-129b-4c58-bce4-97a1746578af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "35ed7eb4-f803-436f-a5d4-81b9baa83921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "00e9e9ea-dbf7-4f49-8746-9b81d70b90b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "500ca4aa-9df0-45a3-800f-9cd238ee5fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "b2229fce-6d5c-4cbd-a690-53a517b447b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "71bb064f-7595-4540-952d-20594c7f9f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "795e7fd2-99cf-42e5-85ad-aceece3e41fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "4f2501ae-de1c-4c4a-b4f0-6752f79d3a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "ee0f5340-f2dc-4270-97df-8886981ea119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "f3777bd0-f462-41c5-ab9e-7d60a24f993c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "f9f1b675-35a0-4408-b76b-c738421003b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "4e51ee99-dbc7-448c-a285-02afc6a6f7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "ddde63e8-039c-488a-b349-53fc8aec4cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "4e7e3e6f-3e69-4e88-b904-cf6b9d512267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 34
        },
        {
            "id": "7c0c8629-7009-4353-8bbc-545e944bbe92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 35
        },
        {
            "id": "0bc93aa7-ab0f-49e3-8656-125d887f6893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 39
        },
        {
            "id": "80a14930-bb8a-41b4-89e8-da1fc8c476eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "6d33ad90-8202-418d-b14a-9f7028df4d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "5d2d8fee-5472-4369-99fa-6205b8795834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "f52d697e-0a58-4c5c-ab73-a61a228508ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "90e08c90-b422-4507-a637-347527daf051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 48
        },
        {
            "id": "e9766d03-bd64-4f87-85e1-303918e97e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 49
        },
        {
            "id": "c80f70c4-9a48-4b7e-a687-888d4221806b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "01af10a9-d971-4e3c-b826-d98e6719f4bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "f1533cdf-bd08-4d06-b1e4-f9e376404da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "d4373a2e-979c-4454-a002-21cef9efc727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "747fdbfa-fda7-4fa6-9466-7df19a6eee0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 54
        },
        {
            "id": "f558c8d0-0322-4836-ac91-39ce29d09786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 55
        },
        {
            "id": "ea8b9a1a-31f2-4e62-9805-043a7aa9ee17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "b95ee66c-0e42-4e8c-b125-40feb10965a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 57
        },
        {
            "id": "edcce5d8-81e6-4c38-a2a3-8e0acc966882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "392a8716-e485-4069-8c70-b5c944b983a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "d96989f3-92cd-4369-9b2f-946e9261e87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "acc3ae33-1d37-472a-8529-389525a96080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 64
        },
        {
            "id": "fd810e6f-daa9-43a3-b456-e4e50de7fa7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "6e589f6d-0c9f-4af5-af1a-8e4652663428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 66
        },
        {
            "id": "e019756e-c0a3-40bf-bd3e-264a1bcc0216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 67
        },
        {
            "id": "8ee05db8-feea-4989-a84d-cba72ae26955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 68
        },
        {
            "id": "6a444ddd-9007-47f5-8d05-31ca6cebe6cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 69
        },
        {
            "id": "bd83831b-2bd6-43ec-9b23-dbe5142939d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 70
        },
        {
            "id": "a8bca833-2007-488b-9db5-729c3f2eade4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 71
        },
        {
            "id": "40cec63f-a038-4a39-bdea-838463c51f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 72
        },
        {
            "id": "533a3313-1569-407b-87f4-6dadcb531b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 73
        },
        {
            "id": "a7d0136c-62a9-4512-ba6b-f92b04ec48c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "f3ed9fdd-38b1-4d93-9c0d-339a4a09e446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 75
        },
        {
            "id": "19da2495-431b-4c8d-9149-17ba4424f36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 76
        },
        {
            "id": "aa31a01b-73f1-4a10-ad64-7f7f8e10e111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 77
        },
        {
            "id": "5e7601bc-2059-4815-aa43-c932960f4dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 78
        },
        {
            "id": "532bf7c9-6ff1-4856-9394-c4a5367f1e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 79
        },
        {
            "id": "8cb1beac-1a3e-4e69-a575-8255e31dbcd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 80
        },
        {
            "id": "8d47a8bf-258f-46e5-b2d5-861d99e5799c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 81
        },
        {
            "id": "204ef65e-2a8f-4f2f-bf6e-02c7e1f2a8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 82
        },
        {
            "id": "55e30ad0-f1b6-452e-a54c-68bda71d4801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "93a6269e-2458-4494-afb3-cf3644504ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "832c636c-e8a3-49a2-a99b-160d6d159ec0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 85
        },
        {
            "id": "5f22894c-b524-4947-a163-73ac3ffe9ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "e974e230-e317-4bd1-b77d-f3057fbd9e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "a707ec56-8d52-4c20-a0b0-2d56f1964da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "89ea0f05-65f7-4288-be57-33daf1a355d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "b65efb1a-f951-417b-83d5-b5a99dd98938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "33d3430c-2c80-414e-a22c-86e7d8d5b8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 92
        },
        {
            "id": "120f21f5-8fa4-4c1e-a7e2-338530cde14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "2e4aab1d-69fa-4421-9ab5-84143fd68697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 97
        },
        {
            "id": "12a768d9-6560-43f9-a97a-39f71b557f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 98
        },
        {
            "id": "e54c68ed-8e91-4391-95dd-e142195185e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 99
        },
        {
            "id": "200cb3c9-f8f1-4a41-a93f-470be10529b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "5750df78-2787-406d-9848-c121b0c83874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 101
        },
        {
            "id": "72a18bd9-eff0-4cae-b584-f617a6a411b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 102
        },
        {
            "id": "382f4b06-4a13-48ca-a3fa-6382e1de591b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "ab63c831-57f2-463f-965a-0f31745bdf48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 104
        },
        {
            "id": "ca7d27e0-047f-4aff-bac2-d65b66552d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "43197bc3-1706-4bc6-a536-f13862cbbb2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "1126ae38-e60a-44ac-a291-e01d183be46f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 107
        },
        {
            "id": "8d7de382-e300-4ddb-b6da-416acd50887b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 108
        },
        {
            "id": "05a785aa-d904-47d6-84eb-8369d266f559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 109
        },
        {
            "id": "1871cd04-7f76-4f06-98b3-e5609a8e5106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 110
        },
        {
            "id": "77e181ae-54ce-4d7b-bfad-eababe887e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 111
        },
        {
            "id": "724e5b7c-62bf-49ec-b8d5-9719ae1d102b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 112
        },
        {
            "id": "ea3c8be5-a75e-4989-985c-8af61855197d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "8824f496-c121-4134-b2ad-c062cb6739c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 114
        },
        {
            "id": "52190c48-16a1-4ebf-af21-9fd96cd61b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "c6995282-d935-4717-9313-3a78ce41c473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 116
        },
        {
            "id": "11dad7a7-bee3-49f7-8c09-f97c9b04ec75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 117
        },
        {
            "id": "785f5613-6f23-42be-855d-9dbd560085c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "b3c43f85-3fac-411b-bfdd-9b354ae0beae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "34ed3a75-2a20-4086-b0b5-b7df45dd1308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "6c5884c4-c50d-457c-8813-fdd0571e2fc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "94d19732-fd13-4429-a8f6-5f37b915b06e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "d38bb336-9b43-4089-bb67-530361854cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 173
        },
        {
            "id": "173783ef-06fa-4bd6-8885-8e79fecc93a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 894
        },
        {
            "id": "9639be00-8438-455a-8fb3-e5f1e6458e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8208
        },
        {
            "id": "eeb59535-7adf-4784-a522-c8bf17459790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 33
        },
        {
            "id": "cc6e7744-c0a9-446b-b00c-4c7aab9cd680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 34
        },
        {
            "id": "e067f944-1705-48e8-895f-f9960dbc4a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 35
        },
        {
            "id": "76337978-5a8b-46f9-a734-7552a02003c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 39
        },
        {
            "id": "4a34e6e1-e26e-42df-ba1c-af4101d6d85e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 40
        },
        {
            "id": "9374d615-3d6b-49c5-a1c0-9a08aecbf00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 41
        },
        {
            "id": "9d1937ec-d93d-4f40-b857-2893a3dc93fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "52058687-4f4c-4bf2-b090-1f47e2fe259c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "3441c6ac-579e-4582-9c71-58736bce8568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "5873dcc0-5b93-4c7a-beac-2a7901223733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 48
        },
        {
            "id": "65c9bb93-2a32-4ca3-b6a4-4b0d596f6162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "aa832570-fee6-40b1-8053-62e297fefc0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "3e0a81be-bd1c-478a-a846-e383c3afb449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "74075fc0-4931-4ff9-a2e8-42df091eeae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "06585b02-c713-4394-a04c-5200519e332e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 53
        },
        {
            "id": "ddb99743-a82b-4878-8b0b-4f9afdc3045d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 54
        },
        {
            "id": "b99026c5-4a1e-4dc5-86ee-370a7444fa32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 55
        },
        {
            "id": "4da289e2-cdb7-4d35-b54b-1a03c923668c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "a2c26c8d-783a-44b3-8fc2-54afd2bd4040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 57
        },
        {
            "id": "01289592-c176-464a-baf6-b594eb666e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "d8c7aa14-fedd-4aeb-b0d8-58b269162d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "eddeb588-2333-4445-a521-0f23ca07c202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "a4fec693-b49d-4af7-b8d7-428879761abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64
        },
        {
            "id": "9cffdcb8-f6de-499c-a921-f1332d50739f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "47e51e03-6e0b-4fa7-9385-875b55ebf5c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 66
        },
        {
            "id": "2361a095-2b5a-495a-81ec-d16c5dbde5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "237f7f7e-7f31-439e-8a47-746313ad8a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 68
        },
        {
            "id": "7018ba40-a8d5-4522-b764-375632554463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 69
        },
        {
            "id": "a30dc0f6-5aa0-4780-8c91-61fa80acb494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 70
        },
        {
            "id": "f3077784-4eef-4dd1-8bf0-426f552a0cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "885027e7-ca05-4326-919f-48fd3f11afc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 72
        },
        {
            "id": "ff156f9a-6754-404e-a3d4-5d5d903e1331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 73
        },
        {
            "id": "453766e2-8118-49bc-bb46-2b220891b518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "b4048559-51e8-481b-802d-0913302072e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 75
        },
        {
            "id": "60230285-4831-4cb8-af01-c6a5de288e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 76
        },
        {
            "id": "a85beb51-4b33-425b-a72b-9c6d72e3deca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 77
        },
        {
            "id": "a1d33d2f-53e6-4869-8b0e-c5e30f66f7e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 78
        },
        {
            "id": "057f74f4-9492-4265-9595-264a421bc91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "425da0f4-8665-44d5-b454-6fa7636d5902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 80
        },
        {
            "id": "3f99e5a4-e7b1-4b9b-af3a-d27333ac5537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "ada32863-d32f-4257-93b0-13a7509c1490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 82
        },
        {
            "id": "2ff54b62-e491-4639-82c5-ba49c1644545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 83
        },
        {
            "id": "0e023e16-b994-4086-a3bf-7b9c85d48afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 84
        },
        {
            "id": "a2c618ac-cdbc-4e3e-8ff3-acb7a2838373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 85
        },
        {
            "id": "0d90272b-675f-43e0-93d0-c9af996c5f36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 86
        },
        {
            "id": "50d668e9-1c64-43ae-b806-7ae8e257c6e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 87
        },
        {
            "id": "3849df8e-e395-4ab8-8b0e-d7f554030823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 88
        },
        {
            "id": "07f11c9b-c17c-4838-9a69-51b53f3b1eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 89
        },
        {
            "id": "8880bb4f-2aad-416b-9ec4-70910019d220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "eb3a8eb3-40a3-414f-8ad6-e6956a9d6356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 92
        },
        {
            "id": "f72aa19f-8685-4715-a4e3-b2c9c7ce2276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 93
        },
        {
            "id": "a95f026c-1e59-4583-bbf6-af7b01657b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "84c0a268-c21f-44bf-90eb-26b0cda6fe9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 98
        },
        {
            "id": "392b09e6-518d-45af-9d5e-916607b6e9d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "5a3002a8-5af7-4eeb-8185-5a186cfe1ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "8008b824-2dfb-4a3e-b36e-3b02b2b5c7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "d2d35699-ce9c-43e9-9c7a-038c2c948c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 102
        },
        {
            "id": "e8eed02f-cecf-4fcc-aef6-b68dd17dd4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "057a8169-8350-491b-b778-1c6829f06a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 104
        },
        {
            "id": "cc4927bd-bc40-40af-8ac3-79a8c43d7b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "007f054f-8ae7-4e9c-8ca9-771a56bcf3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 106
        },
        {
            "id": "732f436a-54a0-4ebd-955b-79edb93328fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 107
        },
        {
            "id": "a748928f-6fa0-44c8-ac29-724b4d748e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 108
        },
        {
            "id": "336c8b6b-0bf0-4177-b7b4-1ec00322a0d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "f782dfce-9b3a-45aa-951b-1e6775b58c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "be25d392-e559-480e-9dff-46eb579341c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "90efb5e6-4582-49b3-b115-e20a2295ad4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "95adca67-5bcb-44cc-91ed-0d4beee652d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "93bfc052-2d82-438d-abb3-8f3c12d82e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "bce49fb7-091b-4fe6-b483-bc38b3477a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "eb3fe11b-6c2a-4cd5-8aad-4834fae91ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 116
        },
        {
            "id": "fb7675ee-e712-4d1c-a632-d58d6b4f0027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "19d9009b-e19c-451e-9d43-b53feeab0b26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 118
        },
        {
            "id": "fa378c5c-24a3-4b88-a832-2478845297f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "b0856482-c5cd-47e2-9443-6f1a20224753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 120
        },
        {
            "id": "65841d51-01e0-4943-873d-73c1911523e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "f5468a1a-0e03-4339-8aa5-92d60228683c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 122
        },
        {
            "id": "02139fae-140e-461c-9ae3-91e38d315be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "9960365f-2e50-4b1d-b55f-a4e1de137210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 33
        },
        {
            "id": "41951ac6-4f27-4e72-aa3d-afe5ce73d54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 34
        },
        {
            "id": "71e2f9f6-aebc-41ff-bf9a-3d0e2099e657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 35
        },
        {
            "id": "aa9a19d2-0892-4438-91c3-e3c77375d40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 39
        },
        {
            "id": "8ee331e4-d733-440d-920c-161ee5b62e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 40
        },
        {
            "id": "c1072363-adc8-49a8-9ccc-44538afdfb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 41
        },
        {
            "id": "16eabf68-ffb4-4247-b9e9-df7c8595e270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "df4fdea4-ed95-4663-a88a-e5a77e5d8b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 49
        },
        {
            "id": "7f97dd3d-6951-4799-8a9c-ed5f78f2a2e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "7bb41dde-4cc8-4aa8-a44f-14d53171900c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "fd9fdc76-f60d-423d-b3b1-256b2d4612d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "1615147e-0958-4c50-a448-55887f0a736f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 53
        },
        {
            "id": "2d6e551a-7968-4475-844b-1f3ce3c6a220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 55
        },
        {
            "id": "ed4430cf-b8da-40bc-848f-abb0cedca1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "2781b44e-1b54-46fb-9e64-798659ab4402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 57
        },
        {
            "id": "4269d929-4177-4970-8284-e8e1be230c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "f54df1d9-bbab-4660-a20f-6074e0122140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "3e1cf838-67dc-44f0-9f66-3a4cf70f0adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 63
        },
        {
            "id": "5951adb1-1ee5-49ba-a975-5caeb3d36a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 64
        },
        {
            "id": "a8d14a69-e9ef-4dab-b38a-4ec8059647d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "d664c7db-4f61-4982-83f1-1eb46caacc22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 83
        },
        {
            "id": "1bd37b54-636b-4784-9917-799032130c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "d407a5f1-61ae-4ebd-bd62-cc2dc77593b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 86
        },
        {
            "id": "ddf50c82-8d03-4d2f-811b-c51cbfcfc194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 87
        },
        {
            "id": "7207e8d4-39c6-4fe3-abd4-3c9c6751e933",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 88
        },
        {
            "id": "9d5eb28a-602a-4dbe-aa08-ceef48bcf98b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 89
        },
        {
            "id": "64e586a6-bc28-482b-a662-c0aa6a83eb39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 90
        },
        {
            "id": "641223ca-529f-4a1c-afd6-edcd9a3bd9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 92
        },
        {
            "id": "16b3335d-bca4-4cc7-b93f-8520a5d8e171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 93
        },
        {
            "id": "12eb5eb3-1c5c-40c0-9e1c-badda43286dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "2f4f821b-9b39-41ba-89fe-2bf9a6f9160f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "feeb30bf-6c97-4a6d-9164-76e84cab9bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "38200e5d-030a-4028-b4e8-41afb45e359f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "c09aa880-38ae-454c-9734-509e557b65c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "1fcd95dc-e597-413b-bd7b-77cc88ab837b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "86141881-b7fc-4869-bdf2-c5d8a1891c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 118
        },
        {
            "id": "be98ca4f-76a8-4205-92b8-254cd4112582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "081c6677-29bb-4f32-946d-7a6cb415b49d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "1a0df450-cb9b-476d-a9d2-56029f38a159",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "e48536e9-4cdf-4ec2-b14f-9c0e6cf81a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 122
        },
        {
            "id": "74262f7a-a05e-424e-8f7d-d6cbae8ae8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "c640705d-7762-4643-8d5a-6861e1701117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 33
        },
        {
            "id": "23d78a71-7d7d-4b84-8ec7-c5b0194dbc01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 34
        },
        {
            "id": "919c5332-31f1-4123-9868-40f7d0a975ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 35
        },
        {
            "id": "f0706e12-b636-430a-aa41-553beef47a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 39
        },
        {
            "id": "48136403-51ba-43bc-a9f3-92fdfbbea71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 40
        },
        {
            "id": "43c679cf-a67f-4032-90af-5411e8079c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 41
        },
        {
            "id": "5fc6fa99-93b7-4367-ab1d-531a2214141f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "f0a7dba9-018c-41a9-ac01-b5a1d60a217e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 47
        },
        {
            "id": "65739d1c-657c-4d55-85f2-d7039b7adc5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 48
        },
        {
            "id": "632eef74-e017-42b5-8fe5-7c93e0e79ad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "53c4bf2c-8b00-4bf6-8017-5a1253a720bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 50
        },
        {
            "id": "739e29a1-39a7-4599-b7f6-48ad1d36d714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "634da8e7-d470-49b0-80d5-de1c79c3c99d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "a39900eb-3ef5-4c73-b5b6-6958d9d798d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "41ae378b-f4c9-4577-a91a-6c607a48aa01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 54
        },
        {
            "id": "b4f51f61-b2ec-4bcf-ba2a-c98b7858e3e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 55
        },
        {
            "id": "573c1df7-a79f-4b9d-966a-cc9460910b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "ea3e75ec-dcf9-4517-889a-2ad0330a04fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "60251b08-ea97-4dfe-980d-e2c42cd9ac53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "cc8288ad-ea43-4b2b-9b7b-1226679db919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "a9675a4f-6cf3-40a8-b086-7546ba327293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "e3f29338-b49e-44f8-be06-f1b18af7dbe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 64
        },
        {
            "id": "06294ac8-5962-49a3-9522-ea5bf82371f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 65
        },
        {
            "id": "60733afb-4c6b-4034-aec8-b21590988a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 66
        },
        {
            "id": "fe5c7618-fbc4-4f86-ad57-4e0e28c6e731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "b9270cd0-35fd-4683-a075-8f5e46608349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 68
        },
        {
            "id": "41bb6f0f-ce60-4b66-8710-9cac78befee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 69
        },
        {
            "id": "c7616a5c-9769-4785-a298-b5c22cf3d88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 70
        },
        {
            "id": "e22ad56d-fb16-442b-9da7-57521ddaed1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "1ab31b59-b74b-4efb-b0d7-a6b340583d9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 72
        },
        {
            "id": "fc1f25fa-a391-484d-a8f8-a653ee839f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 73
        },
        {
            "id": "c26bf184-3011-43c4-a776-bc5b6d59a132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "3faae4d3-3e95-46d3-b984-57aa50d893c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 75
        },
        {
            "id": "35d4f948-fc84-4651-8c61-dfb8290c3417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 76
        },
        {
            "id": "bbe77fce-c026-4369-b213-b117eb09f633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 77
        },
        {
            "id": "e69699c2-36f4-4fdc-a302-921b09c35bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 78
        },
        {
            "id": "26761f97-bde5-4e00-ae7f-c5cf844f9ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "880f93b1-c222-467e-9eb0-5d07868033fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 80
        },
        {
            "id": "e95abdea-0544-4314-9246-e34a2d1d1f26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "5e9bbc66-b327-4f9b-890e-f7d44b2b7c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 82
        },
        {
            "id": "127276a1-e8a7-40e0-9c80-225d36c881ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 83
        },
        {
            "id": "108cdbc8-5c79-47be-9102-065db76a7a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "e5ff14fc-951b-4b93-8ead-fc4d1869539a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 85
        },
        {
            "id": "f5e682be-e250-4386-a3c3-7587d118c7c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 86
        },
        {
            "id": "00d2771d-791d-42ea-989c-2b13c465dbed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "9254ae47-0c15-47fb-80aa-cef093e3707a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 88
        },
        {
            "id": "3f297157-ec04-41ca-ae0e-c5e018fb9b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 89
        },
        {
            "id": "c90aff20-ec33-49ae-aa7d-58551415c946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 90
        },
        {
            "id": "3cce0167-94d7-4f7c-a2f8-283ae71277b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 92
        },
        {
            "id": "41825063-3f7a-4997-aeba-35454c735ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 93
        },
        {
            "id": "29b7ef1a-8d4f-4b2d-989b-9c8d7ec5cf99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 97
        },
        {
            "id": "10641efb-e696-4f54-ba80-ebfc47eb8373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 98
        },
        {
            "id": "0d564d6f-7259-4a25-a46d-c1a8aa8b69fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "c842a7d9-2b0e-47fb-8245-2744d69bbd06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "3604ac2a-2eb9-4e12-a8cb-35c5a5bbbaab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 101
        },
        {
            "id": "9c49cace-62bb-4dea-a0d4-b4342d71b376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 102
        },
        {
            "id": "5e8709cc-c140-44a1-aa15-0714fb4a1d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "8b8658e3-34a6-4394-9673-70460a9281e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 104
        },
        {
            "id": "6bed45cb-7e0d-4835-9a91-9abea9352066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 105
        },
        {
            "id": "be7cee41-894f-4345-9fb7-eab7f6676d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "2f48b2e0-2a1e-4e99-adc9-ef84cadc93fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 107
        },
        {
            "id": "5fff624e-f51d-4ed3-ace0-c14a19167c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 108
        },
        {
            "id": "98704556-869e-449a-afe7-0afc69f47cc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 109
        },
        {
            "id": "da234e4f-1553-4962-9272-ac7b48c9cb6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 110
        },
        {
            "id": "0260a9dc-5ece-4023-b83c-e536fbbbf861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "f7ce136c-f5b0-4e58-aaaf-23cf28ef086a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 112
        },
        {
            "id": "3a765eb1-73f8-4831-8f15-8085960e8cd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "772d38b4-dfcf-4f37-b2db-8f0e3f103d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 114
        },
        {
            "id": "d24ef489-2d36-4df1-8ef0-4384b8442333",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 115
        },
        {
            "id": "b8b860e7-7bab-4c67-b0db-d95110a09acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 116
        },
        {
            "id": "b8bb96d0-749c-4e40-b2b8-7e8a9ee3a57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "44d8345b-7624-4fd8-8ec2-b13696030cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "f3751a15-c8cd-4c9c-adb7-a676ef1b7543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "0dca6647-ee3b-4342-a2fa-24b0df21faff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 120
        },
        {
            "id": "8e0df022-b29c-494d-8bbf-f0cd9f54b967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "211c9f17-1f48-4a5d-8642-fd11998776da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 122
        },
        {
            "id": "7b8a7f42-b95f-4ab6-9c78-8e13dc86353e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "723eb40c-23a1-45ee-b938-818b2546733f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 894
        },
        {
            "id": "20825930-6f67-4dd5-aeab-e64446fb6629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8208
        },
        {
            "id": "068a2ea3-b03c-4028-84dd-34f7c20c641f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 33
        },
        {
            "id": "6229fdaf-8c65-4283-b0aa-a6a79b1d0233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 34
        },
        {
            "id": "b8981f20-2f57-40f1-bbd4-dd14b1d56ad8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 35
        },
        {
            "id": "920e1f02-d6aa-437d-9a63-425d6d7bc229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 39
        },
        {
            "id": "4ca8fd37-7f20-42c8-9530-091fc4add315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 40
        },
        {
            "id": "f4d08d6f-bf3b-41c7-b05e-03618a87b736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 41
        },
        {
            "id": "6310bac7-bf15-4a35-885b-a596ddf90eec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "210a023a-a605-4404-92c1-cf94b62518fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 49
        },
        {
            "id": "59b33764-30d8-4060-b078-dabb29e09e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 50
        },
        {
            "id": "8f35ca3f-a53d-439f-80d9-abab736c7977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "932c0b53-0cc8-47c8-94b9-33278c053103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 52
        },
        {
            "id": "8420f606-4e56-421f-bdaf-d4ed7a8c7dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 53
        },
        {
            "id": "b1caf1a0-64ff-4c64-a2fb-5bc661660505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 55
        },
        {
            "id": "9e58cffc-d0a6-49f1-84ca-78d52826b232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 56
        },
        {
            "id": "aca6fcee-c5c7-4632-9e40-9cf0d4ff568b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "0d8ef222-cb2e-4c56-aa77-880f072c6d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "8a76ea97-2316-4d58-8f12-d04357ed2570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "dfe3c9cc-3c9d-4eeb-b5bd-4142730a52be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 63
        },
        {
            "id": "66205a21-32d7-4211-9303-de2114b95900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64
        },
        {
            "id": "b60fdf33-ee93-4736-840a-adf48a34fc49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "16e23d03-2e12-4645-a42b-16eb32cd1b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 83
        },
        {
            "id": "559c07cb-9e90-4ea1-8581-26f6e35f6b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 84
        },
        {
            "id": "d8c8db35-7f57-4547-8657-5be12327e61f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 86
        },
        {
            "id": "160e8c94-5ab3-4f91-9e84-e75701f30f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 87
        },
        {
            "id": "21befceb-2d05-4515-aabe-ab06ef8f3b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "45a6ad85-b865-4873-8b17-9be298b9e1f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 89
        },
        {
            "id": "a50e445e-6af1-4445-9c7a-2416faabba2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 90
        },
        {
            "id": "a82691f9-5ff2-4061-b187-a619f4df04cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 92
        },
        {
            "id": "36dcc03b-3433-4523-b95a-4587a46f0080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 93
        },
        {
            "id": "0d14ae7f-f6d5-443b-86b8-14e795b026bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "a7781901-945f-412a-a53f-4580487ab30e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "c5061d07-a6b9-44b1-8520-5b0e51a8abb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "9f3e7433-923a-441c-9671-54ed9af84695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "bf575f19-7ef1-4cb6-ad18-4f1fd09de5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "2651e468-402a-4230-9f6d-554c6ab66a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "3cc3f024-f99d-4a46-add8-59d60d187a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "cc01f958-5ea3-4be7-91bb-b6dd4643b66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "8f1944ea-2fef-4ca0-b26b-62fb9cb7ec07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "d7ce0a57-95ce-4dea-b22f-750f47e3a597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "01a5e99e-d310-4a7e-8aff-9772e3e96079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "a892e099-1605-40cd-b92b-a988ffb566cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "7fa5ac6e-f952-4cd2-8a9f-57e117f49d17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 33
        },
        {
            "id": "2f4e776e-c299-4abb-a51e-dc18ec0d96ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 35
        },
        {
            "id": "03335d94-377a-40e8-a139-c50c242bfa94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 40
        },
        {
            "id": "197f9b40-eef9-44e2-b079-eb322928d430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 41
        },
        {
            "id": "e90f25a3-83bc-460b-ac5e-9c45f7994714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "dcbdf0ed-ec0b-4b1e-a3aa-a4d8a395c17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 47
        },
        {
            "id": "288d4e72-ddc8-43cf-b261-3a1e4c251b0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 48
        },
        {
            "id": "fb399205-5f05-4ba3-a3b2-f00f909964ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 49
        },
        {
            "id": "2bb5ff8f-c4f2-4d65-8fb1-d510033b0eb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 50
        },
        {
            "id": "c4391e20-ea12-4863-87ba-7a5d5764a3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 51
        },
        {
            "id": "0c58e1da-b1a3-4525-b8e7-93764fdeedc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "d62a35d4-43a5-40f6-96ad-048e999f3713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 53
        },
        {
            "id": "259d65c5-c3d3-4a37-a779-bb76a7454618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 54
        },
        {
            "id": "db56a993-fe74-4d52-a896-bce31e14df5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 55
        },
        {
            "id": "8df32db8-2719-4a5b-92ca-35a05cf2337b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 56
        },
        {
            "id": "00e13a57-3480-4997-8fa1-27a8bf7c6962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "b7036917-4c03-44c6-9cbc-71a8e52aed3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 58
        },
        {
            "id": "e745270a-e225-4f99-9dec-0f72f254bdba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 59
        },
        {
            "id": "9edab6dc-ba2a-4602-bf9f-d9dab5ddf2d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 63
        },
        {
            "id": "c62d00e1-373c-4e97-96e6-aa3546551464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 64
        },
        {
            "id": "25d048f0-4bd8-425f-a31a-51d389e80242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 65
        },
        {
            "id": "926aabb0-3891-43da-9339-e5687693a8c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 66
        },
        {
            "id": "c6fe32ae-dbe3-497d-a52b-06a8e0670863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "ce3bb9fc-bd8b-4543-8465-5a799c171896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 68
        },
        {
            "id": "268f5cd7-b10e-42f9-9b6b-ebb7bf34e507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 69
        },
        {
            "id": "1a41fdb3-c3c1-4e23-abe8-7335fe9d2ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 70
        },
        {
            "id": "aaedbbf4-074f-45b1-b036-01efb24772e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 71
        },
        {
            "id": "ccfa2216-b218-41c4-85d9-ce1675cf5614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 72
        },
        {
            "id": "b82a166b-c0d4-4c9e-a286-d402c374a0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 73
        },
        {
            "id": "dd06f4cf-0fad-4543-9f5f-5b39dcc604bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 74
        },
        {
            "id": "f992c6b5-f9e3-486e-80d3-7e3054c57144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 75
        },
        {
            "id": "f9075e30-52f8-482d-89e7-64f5939f3959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 76
        },
        {
            "id": "039defe8-f3ca-4861-85ea-2ac290c19b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 77
        },
        {
            "id": "3b0e0574-e1c8-492b-a0b4-62a2bb550576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 78
        },
        {
            "id": "170c5e4d-c846-43a7-b679-55c5725657ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 79
        },
        {
            "id": "d4c31c83-5ed1-4e85-be1b-657559c04df6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 80
        },
        {
            "id": "c0293c11-4cd1-4ac3-a110-938c4bb79bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 81
        },
        {
            "id": "c28f924f-8703-4d86-97ce-0bae5cf08786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 82
        },
        {
            "id": "8994930a-50ae-4aaf-811b-2541d26aecef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 83
        },
        {
            "id": "870a6649-a335-475a-a8c2-5f29cd7c74fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 84
        },
        {
            "id": "2e4a17cb-8ebc-4376-be28-46c15e6b606b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 85
        },
        {
            "id": "733183bc-1724-437f-8931-674f07bee68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 86
        },
        {
            "id": "bccc8ff1-a588-4bf1-9177-968759fe3310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 87
        },
        {
            "id": "4b302a82-e68b-4f5f-92f0-897104c039a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 88
        },
        {
            "id": "3c0e7707-c268-45f0-b937-af00b23d4869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 89
        },
        {
            "id": "18b0b736-dc5e-4d92-b62b-57f33f4fa900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 90
        },
        {
            "id": "b7aa6b00-65b7-40bf-b4d6-f7e29f5b415d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 92
        },
        {
            "id": "0c946ad8-28b7-416e-b84f-e2d4019ce179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 93
        },
        {
            "id": "ea85db42-d991-425e-90ea-6fb7aaa81d1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 97
        },
        {
            "id": "b31c4773-fd97-4aa1-9b9b-23edcd631113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 98
        },
        {
            "id": "b5ee8014-7d38-4f57-8df7-2d54923e4027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 99
        },
        {
            "id": "bb269132-487d-4a72-b94b-ee2a896380c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 100
        },
        {
            "id": "0aa0c765-226c-4dc4-98ae-bb1378c6a4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 101
        },
        {
            "id": "e65c0a3f-4354-468e-861f-66f2429c1442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 102
        },
        {
            "id": "02cb611e-b6a8-49e6-9bce-e73bb490bb80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 103
        },
        {
            "id": "267a73b4-c56d-4643-9df3-f2543a07fdf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 104
        },
        {
            "id": "6c0e051e-231f-4b77-a007-249bb2c3d6e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 105
        },
        {
            "id": "69adf7b9-4699-4726-8020-5089f1bfc892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 106
        },
        {
            "id": "ef046a48-a2e5-46b7-9533-b1b3dbae1df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 107
        },
        {
            "id": "566ac090-4543-45ca-8c97-dc81e2f7ffa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 108
        },
        {
            "id": "189ab9d7-bea3-4e44-885e-be677dfbd28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 109
        },
        {
            "id": "258d5278-cd0f-41cd-a32e-aad8d9cd748f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 110
        },
        {
            "id": "ff7835f5-4f87-45d6-8db7-980a65661999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 111
        },
        {
            "id": "881a808c-ecc3-44e2-9234-493dbfe6d3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 112
        },
        {
            "id": "ef6e599b-7d52-4a60-934e-0af572c487d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 113
        },
        {
            "id": "5c61b4bc-e832-4b82-a1ee-875cbc616a08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 114
        },
        {
            "id": "0b34fccc-27fa-4ce2-b9d9-4351ddcb31fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 115
        },
        {
            "id": "58f45b01-33e9-4be6-8146-e23c3697a881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 116
        },
        {
            "id": "a866728b-7a18-482a-aab3-d0352147a947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 117
        },
        {
            "id": "f4bd9fad-0a1b-443a-b549-31c518d22541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "2eb29316-37b1-40c3-84d6-102d5e8a37d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "8f6e6e03-f660-4ab7-8a84-d52f12dc917b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 120
        },
        {
            "id": "bf5a536d-6a2e-4fc5-b9ef-864c32200e70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "b4361ba2-2112-496b-8ef2-c05189564694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 122
        },
        {
            "id": "026ef8a4-8281-4cf6-8e68-2e5cfa7a2336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "5c0c819a-88b9-43fc-9321-db1f65ccb6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 894
        },
        {
            "id": "162c847e-6100-4b79-9668-cbbfc965c493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "84510536-8f10-4192-9880-97de7f12b86f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 33
        },
        {
            "id": "757e1199-5129-4311-a8c1-482332582726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 34
        },
        {
            "id": "d5a20fbc-0f84-4065-a50a-1b0b0bf2981d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 35
        },
        {
            "id": "04726f89-b2e4-458e-b6be-ce01376ef071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 39
        },
        {
            "id": "5f0438a2-2e2c-4abd-8ffd-910647ce0a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 40
        },
        {
            "id": "0a6487f3-33dc-4ea1-9f5d-ac9550ac24a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 41
        },
        {
            "id": "b9977b60-c66c-4100-a1ac-19e5e5f24e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 47
        },
        {
            "id": "040ebcc2-3903-4695-b3eb-10eb03b9f898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 48
        },
        {
            "id": "e3c6533c-4504-4ee2-983b-4887e47f2116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 49
        },
        {
            "id": "a4d1159b-0c46-4e61-870e-63dcc0f592da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 50
        },
        {
            "id": "04783a5e-8773-4e88-bcf9-4b0cc724a3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 51
        },
        {
            "id": "698f46c3-5432-4266-9a26-7094060fbfa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 52
        },
        {
            "id": "cecadf0f-2aa2-4de0-b246-8e80fc6f5b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 53
        },
        {
            "id": "0686430a-c254-4cca-bd6f-a912f24c43aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 54
        },
        {
            "id": "113b1b89-8cd8-480b-be69-60d65597465f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 55
        },
        {
            "id": "79026e96-09c4-426d-9e92-953398c14e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 56
        },
        {
            "id": "cf19259e-a3d0-4caf-bdc6-40d4072b4016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 57
        },
        {
            "id": "2d1788d9-939d-4bcb-b071-7cd02943b0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 58
        },
        {
            "id": "48d02758-5b80-4e0c-b1f4-61213199c9b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 59
        },
        {
            "id": "5d86ca28-deb0-4339-a493-31c33212232b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 63
        },
        {
            "id": "ecc70c2e-5df3-4dba-8967-d72b9f7c74a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 64
        },
        {
            "id": "8ed355f9-5c9f-42fc-91b4-f9ff1a798bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 65
        },
        {
            "id": "3389a8b3-5ba5-4957-9fed-068d77c19382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 66
        },
        {
            "id": "b998eab1-88f3-48c7-8176-583ff8f4add0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 67
        },
        {
            "id": "107154fd-0b5b-45d1-8b48-d3dfb3994764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 68
        },
        {
            "id": "0a621cd4-317c-4daa-bf1d-67a0f4fff4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 69
        },
        {
            "id": "d34cc5d0-94dc-4d03-bc37-f2b5c911f89e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 70
        },
        {
            "id": "3126ec27-2bfe-45fb-8bab-a463ddcf22e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 71
        },
        {
            "id": "5b3cb84b-c66b-451f-b0bf-995094c6e05a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 72
        },
        {
            "id": "d2aba3ef-0a41-4c2a-981c-2220b246b95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 73
        },
        {
            "id": "f9fe4b11-6827-466f-8ccf-68407b31328f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 74
        },
        {
            "id": "4a62c8f2-e981-4605-b43d-160d785679c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 75
        },
        {
            "id": "d404a412-0437-4196-b171-07a10e683929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 76
        },
        {
            "id": "d67a7a57-ee0f-450c-9baf-6f04ef9e449b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 77
        },
        {
            "id": "dcdbdff8-5acb-4950-a306-d8aabdb08dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 78
        },
        {
            "id": "dc609b26-824f-456b-950d-64f57f0297ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 79
        },
        {
            "id": "4917d99c-0514-4b39-b2a3-93a28ff077cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 80
        },
        {
            "id": "f7fa517e-1bcb-41ff-ac6c-785ac4f88bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 81
        },
        {
            "id": "ac14f6e9-7779-4ba3-b136-5eb163970614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 82
        },
        {
            "id": "556915f8-3f8a-4388-8a2b-55205babf656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 83
        },
        {
            "id": "c7ffbb73-3310-4cba-b436-4691ba171278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 84
        },
        {
            "id": "de0cb743-ca60-4fcd-b877-e9a6e92820e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 85
        },
        {
            "id": "025633e3-9534-42e6-91dc-bff96e6982d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 86
        },
        {
            "id": "8e39a6fb-5737-4bce-8a6c-d7a670ebceab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "603a04c9-2655-4444-9682-7d985d81c76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 88
        },
        {
            "id": "bfbd9bbe-324d-41e8-88d8-2fbf66d78acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "192a5735-0f80-46a4-8fbc-f30a4018554b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 90
        },
        {
            "id": "f67747b1-16fb-4889-bc38-69227a0ec91e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 92
        },
        {
            "id": "3926dc1b-0ec0-4ab4-837a-23eceefd6150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 93
        },
        {
            "id": "706d7cd1-3540-47f6-8876-459cb8d205de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 98
        },
        {
            "id": "cc2f312c-5cca-43cf-a7a1-86781358e531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 100
        },
        {
            "id": "84ea962b-bf6a-44c6-8b40-7c8e0713eb8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 101
        },
        {
            "id": "9ac25557-f489-41fe-94d7-cff80bb78d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 102
        },
        {
            "id": "30902192-0c19-4955-8707-114f90756ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 103
        },
        {
            "id": "651e966e-aef4-40d8-a363-504e59152e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 104
        },
        {
            "id": "626c9f3d-cfb0-4573-946c-ba279625566b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 105
        },
        {
            "id": "6769a079-a137-4daa-8f56-5825e030863b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 106
        },
        {
            "id": "94d1502e-8dea-4922-9b5e-b3fe4fe3ebd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 107
        },
        {
            "id": "17c77d6f-878b-42e8-a13b-51722298704c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 108
        },
        {
            "id": "a4a94a16-e981-42ea-931d-8eaf7fe2e04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 109
        },
        {
            "id": "abd18405-fd66-4da2-85bd-edd4e4e7fa34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 110
        },
        {
            "id": "6e48d82e-b973-4b59-a3ac-b3e58486ba54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 112
        },
        {
            "id": "c01861d4-5c8e-4d36-9c33-9ab10b69b58e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 113
        },
        {
            "id": "725666fd-bd07-40e0-b89d-5ab6c9a2b5bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 115
        },
        {
            "id": "3e6d2da3-5901-48bc-8fde-9ce3d4248f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 116
        },
        {
            "id": "6f4a1407-be59-489b-a406-12beef204263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 118
        },
        {
            "id": "4a5d6631-b3c8-4027-ad4c-00d9717978cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 119
        },
        {
            "id": "06714190-ce90-4944-a5e0-38b3d1f00d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 120
        },
        {
            "id": "dd6029dd-1c7f-49b4-92f8-36ea6db84ef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 121
        },
        {
            "id": "2afd5cc8-3e31-4386-8664-e918e8dfdbc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 122
        },
        {
            "id": "f6b87067-6734-440e-8051-08cfbd44c49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 894
        },
        {
            "id": "cff775c2-6aa5-4aff-b4eb-09ce71a24451",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 33
        },
        {
            "id": "416fb035-c2b0-439d-9717-0309a161ca35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 34
        },
        {
            "id": "5f8320d4-dd26-4436-b379-6716b8cfeda0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 35
        },
        {
            "id": "675dc8b6-9f43-47a8-9ed0-82183cc63c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 39
        },
        {
            "id": "25380e5a-2091-46f5-bf4a-899ec4a5ae85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 40
        },
        {
            "id": "b4c08bb3-2ed5-4c27-b911-4d1f87204afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 41
        },
        {
            "id": "3f653ead-70e4-4979-ad1a-ca2b8972d94f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 47
        },
        {
            "id": "5411ad7f-8c08-4e95-aea7-1729fac526f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 49
        },
        {
            "id": "a60f9023-41d3-41ef-bb8c-886383e27212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 50
        },
        {
            "id": "87a949dc-bf69-4bff-89cb-0721c49c4df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 51
        },
        {
            "id": "617c40ff-8335-4f7b-b89b-a69f1a156ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 52
        },
        {
            "id": "4dfbfa35-e106-43e5-b6d8-617c1537d09e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 53
        },
        {
            "id": "b539620e-dd24-4361-9a57-9b18b224c8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 55
        },
        {
            "id": "52e9f856-6fba-45b5-a44e-158206078ba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 56
        },
        {
            "id": "6d73f036-21da-40e9-9244-8b77b5344ba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 57
        },
        {
            "id": "8a792403-ceed-4a31-9b11-933e6ba14b04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 58
        },
        {
            "id": "80b69038-0179-4be6-ad8a-c2985accb790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 59
        },
        {
            "id": "2825d6e8-47cb-4491-ae44-d909fa8f1b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 63
        },
        {
            "id": "19e4d9ae-490d-4c67-8b02-20e169864b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 64
        },
        {
            "id": "dea3cb10-47ac-4601-a890-5ff714966347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 74
        },
        {
            "id": "9bcabe99-e166-4ec2-a430-b9485d92329b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 83
        },
        {
            "id": "76418613-9aec-4eff-bb59-673c05b45d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 84
        },
        {
            "id": "b65576f0-0758-4b0f-a611-16985391ba83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 86
        },
        {
            "id": "df0197a7-374a-42d9-a165-a551a38b80fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 87
        },
        {
            "id": "6dd31615-e494-4743-862f-b01e04758d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 88
        },
        {
            "id": "e2352a42-2b5f-41b0-bcbe-3933d5598df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 89
        },
        {
            "id": "f90dd01d-f4c6-4be5-bb3f-33b07a537559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 90
        },
        {
            "id": "76c7a9c0-3872-4dec-8d9b-7d375177ca06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 92
        },
        {
            "id": "efc14112-36c8-4239-a600-7d1e7f03bb0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 93
        },
        {
            "id": "c5ab1287-0ca3-4fb6-aa55-130d1bbe737c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 100
        },
        {
            "id": "4ea1ef92-8c75-47c2-b2d7-4a949d465e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 103
        },
        {
            "id": "7c2dc4a1-6b8d-48b9-a623-72130dd2d358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 105
        },
        {
            "id": "4e96c9d8-b04f-46dd-9fbf-21af96283ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 106
        },
        {
            "id": "2680cc80-6540-4b03-be64-a916ae8d06be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 113
        },
        {
            "id": "f37e436e-16be-4244-a4dc-abd60ec65472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 115
        },
        {
            "id": "1b43f834-3eb1-445c-999b-d861911460a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 118
        },
        {
            "id": "133d0e41-9aef-4003-85b5-7f29afd2a854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 119
        },
        {
            "id": "6a37155d-6d01-4149-8629-d315d5938eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 120
        },
        {
            "id": "f67f6baf-2aa2-40f0-8d18-bc696eeb4cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 121
        },
        {
            "id": "6f3b4e62-5567-4d2c-841b-be6814ae3f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 122
        },
        {
            "id": "d75d93e4-fe51-4e2b-9b5d-eac3cccc19e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 894
        },
        {
            "id": "59992906-66b5-40d5-80fb-98cc4a58cd87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "fe242ace-0974-462e-996e-94f198916632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 34
        },
        {
            "id": "a7e3f9fa-05d1-459f-874c-112abda84399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 35
        },
        {
            "id": "07767c72-8787-48f2-80dd-e71931546a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "2fa00344-0377-44d3-9b12-ad8b6359b9d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 40
        },
        {
            "id": "ec733625-ffc8-48df-8c9e-99f595a4801f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "e26bbb59-eeb7-4001-8cdf-435056c732ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "9cf0df78-14cf-45dc-9c61-011e00115649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 47
        },
        {
            "id": "d78d2391-e4a5-4ab9-907e-f47ef3475a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 48
        },
        {
            "id": "4c17b83e-4de3-47f1-922c-5be14acf5b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 49
        },
        {
            "id": "536bc13c-25b4-4c27-a3b4-c2b9e9d717bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "9bcd7998-45a4-462a-9f67-fc22de4ba6a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "353b35ac-0f31-43de-b191-7c3df52cabe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 52
        },
        {
            "id": "a7d0465b-61ad-4535-85db-52726d7257bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "dcaf54c7-063c-4fe1-9745-9435230c3630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 54
        },
        {
            "id": "6f44e1f3-1e03-4c47-9c8c-cbac3bd29132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 55
        },
        {
            "id": "c906a7d6-a013-42fa-a789-7bf030bc256d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 56
        },
        {
            "id": "7a6d955c-57df-4dd3-a302-dee9faeca7b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 57
        },
        {
            "id": "38e7b474-62f4-449d-b396-84f3520faa28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "06780a22-4483-472b-b961-6f42bea566f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "a6cf4317-8b10-4990-bc74-cd8bcf0aa070",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "24092f36-abce-4f90-b5ce-9e0986532319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 64
        },
        {
            "id": "41d027b2-25cb-45ce-aa4a-f97fbf3d04b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 92
        },
        {
            "id": "6488783a-90cc-4f44-b63a-39c24604e2f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "da2e85dc-5529-4a0d-84d6-d9f023d8b7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 97
        },
        {
            "id": "4ba9bbe8-6e4d-4c9f-949b-2b416cb607cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 98
        },
        {
            "id": "c501c1fd-ac37-498d-ac8c-94c9a22d573c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 99
        },
        {
            "id": "2369c010-e064-4fa3-b5e1-779a1ca4ba14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 100
        },
        {
            "id": "3fac69e2-2c13-4146-8ed7-843786bdd27d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 101
        },
        {
            "id": "b322e06c-4f30-47ff-ba2d-9df20e431c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 102
        },
        {
            "id": "766693fe-ffc0-4246-93e9-a3bb236d872c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "2b7264f2-f2b4-4659-bf6f-937d08eb52f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 104
        },
        {
            "id": "31cff887-ffd2-4a22-8022-9ff4e515b887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "fca2b125-013a-4927-9bd8-8479efa80b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "a9aac966-1393-4d00-9921-a7968625d57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 107
        },
        {
            "id": "3b8c104e-716a-4b52-befe-4f04a94dddfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 108
        },
        {
            "id": "3eb6f981-6534-4af6-ae78-be979c2647f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 109
        },
        {
            "id": "eb45ee4b-b937-41c9-821d-45a6e8df5309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 110
        },
        {
            "id": "d6bcc255-ab0b-4d83-8700-5f1f5e7576fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 111
        },
        {
            "id": "28a44f22-5fcf-47b1-a81c-162c48e5bb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 112
        },
        {
            "id": "7e0326e2-36d1-4e65-9c0a-77eca8c8db93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 113
        },
        {
            "id": "2a3b8635-d2ce-4bbd-9b43-e4e61ddf9594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 114
        },
        {
            "id": "ed3fe5ef-becf-488a-bfde-de4bf42ccf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "8e489863-0089-43be-8834-46ac2ce16918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "10816e8d-96d4-46df-a846-5ba4b350ef31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 117
        },
        {
            "id": "daca03c2-732c-438e-91d4-6da7630080a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "7bb525d1-4c6f-4e46-8518-bf8b3cb1ae00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "847cdd03-7795-4ed6-a408-e186faf3bc8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "2067446b-5577-4b71-bd77-1c99dcd55a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "723e01c0-85fe-4a58-bd2b-a581739f1a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "876dbda4-4d6d-4c90-9f38-122671dc7607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 173
        },
        {
            "id": "c7d1cf43-574c-409f-88c9-0f6dec5068e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 894
        },
        {
            "id": "25cf1c76-2de0-4ac4-829d-355a760d9ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8208
        },
        {
            "id": "b41fb478-f32e-4f46-a7c0-4955904ecfaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "0a86c3dd-f7ba-4672-9164-9d26b0d33c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "4d1cd0b9-37a7-4410-9d34-7003768ea5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 35
        },
        {
            "id": "46a7273e-f35e-4d90-98f6-72c880d75d08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "22072dad-308b-4201-a48c-528dbdd445cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 40
        },
        {
            "id": "54217c26-8cc1-4027-b0f8-88ae63ec0883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "fa536021-a93f-4d7f-b53e-9bd348c1e947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "7f80b63f-9647-4dec-afcf-9964c93fb5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 48
        },
        {
            "id": "0be317d3-7d2a-4091-8895-bab1a9285da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "cc4c6e08-a848-413e-968a-845909891ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "30effb2d-e79e-43f3-96e6-dc35cb526724",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "6c5a5a1b-e620-4e86-bc49-5ee537aceb94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 52
        },
        {
            "id": "299643f1-2f41-4e90-b4c3-8db868d9bf9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 53
        },
        {
            "id": "e9d7fcb5-7730-4fb3-b26d-5ddde74147b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 54
        },
        {
            "id": "5be46674-54d7-4851-8f67-da06efcb017a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "dfa66739-7426-4a27-93f0-bb789745b3db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 56
        },
        {
            "id": "ad5660d3-9c65-4fbf-8c61-662f8097f280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 57
        },
        {
            "id": "9b4fb593-d4f3-423f-b030-540c74891de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 58
        },
        {
            "id": "94eac47b-acf9-4936-978a-1381079d9cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 59
        },
        {
            "id": "272ec2c2-49b3-4013-8751-995a45b0f1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "a8ecc9b3-e77a-4750-bad4-1698e192cd53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "07709333-8b34-49b6-ae8e-058c26f21ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "e5f0f8c4-ab1c-4944-8872-b9a826acc53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "73a063b0-c8ed-4a37-aaa7-99692d21dd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "f4fd65e4-cb31-4b69-a76f-59acc7f0f3ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 100
        },
        {
            "id": "32eacefb-0d0e-4ea2-8378-e639b93c2ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 101
        },
        {
            "id": "dd699094-86f2-4011-afe2-15fc0f08011f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 102
        },
        {
            "id": "6164fa72-c5d0-4bea-8d32-b0ec918fc0b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 103
        },
        {
            "id": "1288dc76-f896-4ccb-b67c-10fdaa9e6d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "e3e91761-5d98-4959-87cd-f17287d739d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "7b592748-3ef2-47ae-a004-0a6acccb12b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "72ea9760-b4c4-4f4f-a0b1-2e340b33b4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "b40dfef3-6c45-4774-9c52-a86ede342332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "f9d4f4c4-6337-434f-b475-a613dbba67d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 109
        },
        {
            "id": "11c88ddd-36d1-435f-94e3-11d6da173bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 110
        },
        {
            "id": "59fd37f4-e7f6-4770-ae38-f925651c2418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 112
        },
        {
            "id": "4f725620-5d2d-4a1b-9bdc-024aa990d2cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 113
        },
        {
            "id": "25cb5b42-d3bc-4e72-85dd-55208309e598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 115
        },
        {
            "id": "97b1f5ce-41a4-45e4-bad5-60ea75049fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "d02f07a1-652c-4be8-b065-2aeabbda25ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "8b4d594a-e3eb-4743-afb4-a2494be0af75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "f4d25c06-69e1-40cb-b035-e2ade17262f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "14ede59e-fe9a-4e3b-a2fa-20d1402f4d0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "fd54f7b2-7924-4d66-a73f-fca351b2ac90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 122
        },
        {
            "id": "6ae9ebf5-7bde-4c21-acaf-cc9caf35a25d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 894
        },
        {
            "id": "20cba983-c1c2-49c2-b85e-0837c08f94ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 33
        },
        {
            "id": "360aa537-ef07-469f-9f6c-dcfefe962572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 35
        },
        {
            "id": "eec1adc8-cd45-4e5b-8ec6-f377131e7fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 40
        },
        {
            "id": "ffe4549b-ed4f-41c5-80be-8fea81fdfa52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 41
        },
        {
            "id": "13690b10-f2b8-4481-9a88-642315cf10f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 45
        },
        {
            "id": "e463e299-0835-46bd-9bd1-cbc8ccad30d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 47
        },
        {
            "id": "33aaca31-3c6b-4824-a001-e95fb4839970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 49
        },
        {
            "id": "42af6c3d-168c-412c-a100-7709bc31634a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 50
        },
        {
            "id": "2a3883b5-81d0-41e0-a2d7-097fdaf0d41f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 51
        },
        {
            "id": "980ee672-04b3-4b2b-b68c-b08e5c4e75ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "1685611a-59f7-4eaa-b257-f0769392ad9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 53
        },
        {
            "id": "78df9e94-f654-405e-9676-324ed812dc8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 55
        },
        {
            "id": "c5588002-94c8-42a6-bdc3-f6b89d273227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 57
        },
        {
            "id": "905685d1-4f6f-4239-9d09-f2919c405a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 58
        },
        {
            "id": "f9333a50-b780-45d7-a93e-d9a93b517094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 59
        },
        {
            "id": "1f6e8352-8cdd-4a94-ab87-b5c674312c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "2b5b336d-f5f0-4160-b951-f19348e8905d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 92
        },
        {
            "id": "0e8bfd62-cd5b-4b37-89c1-7ff63d5f59b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 93
        },
        {
            "id": "b623fbfe-81cf-4487-b0ed-573b99e62a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 103
        },
        {
            "id": "268a3f58-e3ea-424b-b0d6-beae5185ecd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 105
        },
        {
            "id": "af193e57-cb77-490d-8889-2fccc9675564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "3ece7972-5af4-4130-aab9-1e6a479095ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 115
        },
        {
            "id": "94513167-53d8-4cca-8f55-5d0147d905ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 118
        },
        {
            "id": "75d17450-8c72-449c-bdb6-1aaa11d2b693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "8d8216a1-d100-4755-b9b2-2de14c566d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 120
        },
        {
            "id": "fef88827-b4df-4c45-b5fc-1d3821107847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 121
        },
        {
            "id": "57603780-afef-41da-b4be-e87c1cc38499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 122
        },
        {
            "id": "4264cfa5-d583-42ac-b43c-9bec7d0f9e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 173
        },
        {
            "id": "a7b4ea86-1e21-4a2a-a26b-8231bfa0a149",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 894
        },
        {
            "id": "5bcc4adc-3226-4479-b63a-902bb1293e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 8208
        },
        {
            "id": "6903c9cd-4746-42b0-adb2-2d97163a91c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 33
        },
        {
            "id": "f23636b9-e664-4356-8d73-985793f7a7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 34
        },
        {
            "id": "c4d17855-44ab-43a9-9409-b32085657e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 35
        },
        {
            "id": "3ccee9de-899d-443a-9c09-55a82470bbf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 39
        },
        {
            "id": "7170b9c9-30c5-4ace-bef9-712c80014a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 40
        },
        {
            "id": "661b005d-1484-4dd2-8cdf-334902c87293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 41
        },
        {
            "id": "d9927478-9acb-4b16-bf78-2902a40dfb33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "825c52c5-cc81-4401-9e93-755b5d045202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 49
        },
        {
            "id": "cd582300-13c0-4a8d-b2b7-b62f60f91338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 50
        },
        {
            "id": "ae97292d-7236-4f1b-8fd2-a94f2b51f951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "76d5447a-46c3-4fbc-8488-bdcf1adddc92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "2b26f2cc-1d36-4d94-8250-3132dc9453d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 53
        },
        {
            "id": "cd90f04d-78d9-4525-b77d-1ddb83b40b66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "7155f8c1-e717-4ecb-958a-cc2e88f3ede5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 56
        },
        {
            "id": "81867667-e7e8-4586-a2cd-f287f751d69e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 57
        },
        {
            "id": "1e37ed42-6c09-42d8-98fa-f49d33d455a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 58
        },
        {
            "id": "7b6b4eff-a2d1-4a64-a687-0abe726df1a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 59
        },
        {
            "id": "84272786-374e-4c65-92c3-dccfcf2d4504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 63
        },
        {
            "id": "ac111e11-ee2b-4ca1-93e9-ac15400bb9b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 64
        },
        {
            "id": "4be8f1e3-7d92-4686-aad4-b42cfa42bde5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "9c4001da-22eb-4139-85c0-e513bcce7773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 93
        },
        {
            "id": "18554a61-14a5-4844-bdc5-661a9904810e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "9ed42c31-7630-4faa-8323-804baeb68415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "3a8abed8-62d0-4f71-ae51-6968d120e404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 105
        },
        {
            "id": "80acf474-116b-4ad4-9c0b-93c2dc6315e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "e504972a-6fb4-4abd-9669-728c73a77a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 113
        },
        {
            "id": "85e51250-b060-4d05-a569-bb4d04105698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 115
        },
        {
            "id": "29b0f9e8-244f-4842-bb7c-812746fa84a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 118
        },
        {
            "id": "e862c1c1-7d68-45db-861d-b82c0ab57f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "084b7503-4215-45bc-a8b5-80a21fa5d59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 120
        },
        {
            "id": "346fb512-d386-4898-899f-4ef35bfe4c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 121
        },
        {
            "id": "d0a711e8-d30e-435c-8ebc-fbdef15b3ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 122
        },
        {
            "id": "552db43b-9c6b-4148-b6a2-436e9611404d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 894
        },
        {
            "id": "4637db41-076d-4c16-96eb-3ad40af8060d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 33
        },
        {
            "id": "bd5a0222-fbac-49ed-accd-a71bf6ee96d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "c75f3ff8-0a28-4164-a662-c03aafbd110c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 35
        },
        {
            "id": "69e59670-8290-4136-bba9-8d1cedefa08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "3962cd07-31f8-476b-8121-80cb8997d00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 40
        },
        {
            "id": "c0d869a8-23b7-42a3-a030-4aac9d629c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "54a81602-e95c-4c0c-b407-05cf947dfaaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "91bae659-1b13-4434-8767-0d73ab36a1a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 47
        },
        {
            "id": "81b36438-9ea7-4341-8d1a-f44f166a4a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 48
        },
        {
            "id": "416bc93f-580b-421f-8750-c2d3192522e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 49
        },
        {
            "id": "9479274d-d00b-48e7-81c3-02baf1ff1c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 50
        },
        {
            "id": "7274f921-a766-41fc-ad78-0b9e15b5a74b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 51
        },
        {
            "id": "b3c040a4-7c55-4b2f-b11c-ec6fa8233feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "534a302a-3472-4ebc-be1a-b1f061692825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 53
        },
        {
            "id": "ed171ee4-6a18-4a7c-95bb-7ebb92ab4101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 54
        },
        {
            "id": "ba510498-e82b-42af-ab72-d41b1c744f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "104b126f-11b1-4e28-b33b-7767fc5e19cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 56
        },
        {
            "id": "502a072f-5e7f-4653-8c28-1fedac5d8385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 57
        },
        {
            "id": "aa793e02-67b7-4708-a897-a4e9d77979ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 58
        },
        {
            "id": "4c24891d-d1c4-4eb1-bbd0-fe1f5ca11858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 59
        },
        {
            "id": "cca0e145-d095-4262-aa40-b6b2e23cf437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "5fbb297e-2ea7-443f-b6b2-d8a84eb46b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 64
        },
        {
            "id": "527161e3-2140-4349-bf07-a8a99e19c427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 92
        },
        {
            "id": "1163acf9-f9d8-4bf6-a30f-d03d3bf90348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "7f1f0e02-eecc-42ea-8b45-7a4e894a20d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 97
        },
        {
            "id": "fbc5ae5b-4b04-49cb-b45b-8a6fd7ebc9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 98
        },
        {
            "id": "b19664e4-6787-4956-ac46-b3642281d5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 99
        },
        {
            "id": "8349d27b-a106-411b-8f43-ff7d9bce1583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 100
        },
        {
            "id": "df42cef9-7999-4d4c-962c-775b438b82b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 101
        },
        {
            "id": "7cf358db-3c27-4fbd-b602-7c1941662638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 102
        },
        {
            "id": "efc99914-1307-4cbb-bcc9-fe83c144cccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "6e36507a-6fcb-4030-9f50-5dcdaf06f417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 104
        },
        {
            "id": "69b7bfd3-a7a8-408d-86e5-0bcba8ad153a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 105
        },
        {
            "id": "e692a294-f2c4-4ff5-b782-1b38295b5e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "9f45e6a6-9509-4c44-b37b-9d9424db5bba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 107
        },
        {
            "id": "b9d53a54-2e76-4ab2-86a6-1487818818d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 108
        },
        {
            "id": "5e18dfe2-90c1-493c-b228-04ae2b79c126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 109
        },
        {
            "id": "4514ae03-33c1-4a54-af93-b83486e9770a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 110
        },
        {
            "id": "aa64e9f5-a31c-4277-b079-72d40fa3d665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 111
        },
        {
            "id": "40edfd28-ee47-4678-9f36-e8472a5bfbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 112
        },
        {
            "id": "10b669c2-4afa-4c40-b6e0-ae784ea19d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 113
        },
        {
            "id": "a171a29d-7757-4dba-a685-7ca6b2523cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 114
        },
        {
            "id": "ca5ae1b6-b7e0-4576-9744-d5abd5d89996",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 115
        },
        {
            "id": "2fb42bd1-a91d-40fc-8acc-aede13a7ec87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 116
        },
        {
            "id": "7d24e72e-93b0-4dbe-86c2-e19680fe9db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 117
        },
        {
            "id": "227e9160-fa91-46a0-afd9-356a1ef4eb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "04b320a5-49f1-4967-8f79-3cceef6690a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "d996eeea-c154-4231-8d95-6f9111904556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "144fb5a7-2625-4b8d-a210-bd996b1e46da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "c5cb8295-e8f4-4180-981e-b4786a884432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "5357dcfb-6872-477c-9fe8-547f0cbdaffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 173
        },
        {
            "id": "e0f848c9-6ad9-43b7-a30d-5cb9aad5098e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 894
        },
        {
            "id": "c292f5e9-04c7-47ef-92c7-cb85b5ebdbd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8208
        },
        {
            "id": "b577b054-2928-4bb4-8bae-9ac77dab196d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 33
        },
        {
            "id": "584be2a5-242d-49af-9406-b3edbbd94ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 34
        },
        {
            "id": "4d195d19-f08b-478b-a6d2-491f2ab42abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 35
        },
        {
            "id": "ec24e0ea-676f-4e42-b3d7-38fb151563cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 39
        },
        {
            "id": "df896b71-957b-41d7-bf60-c8ed5081ccd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 40
        },
        {
            "id": "edc0600c-15be-4783-9b33-da524d286d87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 41
        },
        {
            "id": "b54ca3c4-7e13-4cd3-a482-5438da92614c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 44
        },
        {
            "id": "df8dae6c-0b93-4955-bfad-1bb8ac38a86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 46
        },
        {
            "id": "99dbcb99-53ee-4754-84ac-955d0fa0a565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "09729b2a-5da8-4020-845b-c7c928f70b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 48
        },
        {
            "id": "5f41367a-e6bd-4168-8097-fa2ce01f21e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 49
        },
        {
            "id": "533df7b7-0fa1-4e3c-aa71-a25cbea5c6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 50
        },
        {
            "id": "2bf8a4ad-caa8-40d2-983b-e72576b8c7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 51
        },
        {
            "id": "eea45efd-5cc6-49e9-a41d-a20b3736e73e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "a9335a89-74d4-45e7-9c06-55c1daa0a658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 53
        },
        {
            "id": "7d3beb19-419a-45e6-91be-11693e83f76a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 54
        },
        {
            "id": "b5f5f2d0-478f-4902-ae0f-be5aae5ac1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 55
        },
        {
            "id": "4c4db13c-9f8b-4cf8-8782-538eb96f4b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 56
        },
        {
            "id": "487dab5b-70a5-4c42-b2ba-a36bffc8e09f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 57
        },
        {
            "id": "46becfa0-151f-441b-916a-c4e660b7a02d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 58
        },
        {
            "id": "1d76ce0a-4ce0-4731-9d44-6fe047b47347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 59
        },
        {
            "id": "3ee68c4e-090e-4df0-b8d9-7fb4d2dc1274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 63
        },
        {
            "id": "63747514-a542-43e2-9464-ef7d75c3f16d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 64
        },
        {
            "id": "7b2fbe49-2a36-443b-a1b8-63b229c81e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 92
        },
        {
            "id": "1edb8bed-d278-423e-92a3-bdef1601908b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 93
        },
        {
            "id": "24072c99-f99d-4894-a183-032bb1927acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "82c13ea4-0946-4803-8bb8-095d3930dc88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 98
        },
        {
            "id": "8b32bfb4-9d2f-443b-8d98-279506bc8147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 99
        },
        {
            "id": "e54249de-b47a-4da5-bdf2-a07c8efd2dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "db6c9a36-da82-4f70-bde6-c956c6a4bd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 101
        },
        {
            "id": "8e3858dc-5753-4b99-88ac-d8ac5e7bc406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "b97eeadc-8218-4fa9-8613-5c349aedf662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "dbd4479a-ad40-4007-877c-986816e09902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 104
        },
        {
            "id": "fbfa1daf-0fdf-4dff-9356-8d5b12f69806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 105
        },
        {
            "id": "bd0c2cea-a217-4c60-80eb-80330f0f113b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 106
        },
        {
            "id": "8b5b150e-d8e1-4a14-876a-08866d50dadb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 107
        },
        {
            "id": "326e3a26-f13f-4dc3-aaed-6487e318e9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 108
        },
        {
            "id": "f6cb67b1-f59c-4ad5-8ed2-07021d34e75c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 109
        },
        {
            "id": "6374e355-fb71-4166-ab99-e9a07d498b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 110
        },
        {
            "id": "f137dc1c-7275-4fe9-aa9b-15cf178475f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 111
        },
        {
            "id": "d30dfae9-3d2e-41ff-bcab-d62d202e0095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 112
        },
        {
            "id": "3d65d7c9-cf6b-4196-ab00-c20dcf3095dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "608e3e1e-22b8-4220-b9f3-5170f80fde7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 114
        },
        {
            "id": "a5a4b8d8-6de1-4ace-b7e6-a08db11544cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 115
        },
        {
            "id": "ddf528c5-fd91-4cbb-a2f0-b560da4f44be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 116
        },
        {
            "id": "6803e958-0839-4b56-9cf7-951cbd79e8f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 117
        },
        {
            "id": "9931a609-5513-4f75-bb00-0c6853fee266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 118
        },
        {
            "id": "8acfc417-11cb-45b7-aa35-8c17603e6b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "bbc50349-a75a-4ef8-8716-a6b83fdfc5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 120
        },
        {
            "id": "369b6c32-5a4e-41d1-bf3d-7c65116e3ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 121
        },
        {
            "id": "97ca4cf6-295b-4c53-bf85-0bb65e8461b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 122
        },
        {
            "id": "a42d75df-617f-4686-97f9-f33d60ae9c6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 894
        },
        {
            "id": "93a1002a-229d-4719-926d-c070cc6b3690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 33
        },
        {
            "id": "7553b885-dbb0-491e-8d68-f8acfaac242f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 34
        },
        {
            "id": "6456902f-dd83-4526-a0f2-d4683b25b986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 35
        },
        {
            "id": "94711437-0703-4fd4-b659-781e1206a449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 39
        },
        {
            "id": "8c90e0d2-3947-4a43-862a-d1e4f46c3734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 40
        },
        {
            "id": "27e162a7-a3cb-48fb-adff-5eb93d7b73b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 41
        },
        {
            "id": "40fe7e4f-8e4d-42a1-9ebe-59b03d921648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 44
        },
        {
            "id": "e84c901e-8e63-4a36-811a-da429d1b2a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 47
        },
        {
            "id": "094ed119-44cd-4aae-9aea-084b43e91dd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 49
        },
        {
            "id": "099804d1-07f9-4d94-96bb-f5f11c086d48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 50
        },
        {
            "id": "c03aeaf8-a18f-476c-8c41-5ca4e97ed80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 51
        },
        {
            "id": "01ed6942-7802-471a-99ac-24af69c1b936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "8496d950-ab12-41c9-b936-b542188ee0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 53
        },
        {
            "id": "5a01040b-aa92-4f44-afb2-d418bf9837e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "83eb5e98-f9f8-4e59-bfb9-96ca093a1ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 56
        },
        {
            "id": "0b8477dd-41e9-4f78-af89-1f04c2e1e650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 57
        },
        {
            "id": "56a2a371-2a9f-412f-8934-598635be71f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 58
        },
        {
            "id": "ed347067-577d-4d12-934a-b934af7d88b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 59
        },
        {
            "id": "7feb38eb-7ee6-492f-9a97-52b6a8e9a6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 63
        },
        {
            "id": "0f9e5e34-7517-45d9-a327-8ddf556861d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 64
        },
        {
            "id": "8b3c7d8a-d492-4cf9-b7ff-8926d7e7cf75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 92
        },
        {
            "id": "e7bfa78c-4399-4b05-b5e2-6fcfc428739a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 93
        },
        {
            "id": "54bc5f5a-d273-4d8a-997c-eced2440af94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 100
        },
        {
            "id": "53dacbee-1ef0-4e40-be29-c48b1fb1f376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 103
        },
        {
            "id": "a7d7b24a-7afa-4c79-8487-ce5858ccdb2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 105
        },
        {
            "id": "63227c27-bfd4-40c8-bc36-7795ee50d107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "b486ee42-3809-4499-8487-0dc0b10d747b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 113
        },
        {
            "id": "7aa1cf71-2e61-4360-9e76-d14db629e550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 115
        },
        {
            "id": "35db8b1a-c273-4b9c-bbd3-9acdd70616a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 118
        },
        {
            "id": "d6eec734-ee1b-4f42-b6ac-b924f5f89351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "91a3763a-db3f-4973-9043-483b390bbe3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 120
        },
        {
            "id": "15a17cd2-8096-4a16-b82b-13762cd2b3e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 121
        },
        {
            "id": "c4ccbfa7-bc77-4eb4-9895-ac2c2ae0156b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 122
        },
        {
            "id": "99060b4e-fa50-4436-8856-79ed80b9e03a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 894
        },
        {
            "id": "32ef231e-e32c-4d45-8bd7-b14ad445256e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "2606aeb9-02cd-4550-b531-66fe346eeeaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "2a7b81da-cd39-4eff-934e-655be43fa579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 35
        },
        {
            "id": "ae1cc8e6-8120-439b-a38e-834db94955b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "51be6320-c825-4a9c-9471-6b1fe09453c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "26324f99-7bc8-4267-b4ba-5c7b14b09d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "48bce38c-5bd5-4ba4-ba96-0db45a9bb2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "89a65892-9879-48d5-b561-56b64122927a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "0d5c1fda-51e9-443b-9861-d79e5715b255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "6c878252-dfd1-4b1f-abc5-dd80629fe15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "f75a30ae-5252-4715-8a61-d60117ce54d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "2a4b78a1-c14f-41ef-bd03-44d2899c4721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "3d767b51-63de-4abf-b3b9-ad27974c0db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "a37f5313-97cb-4d91-b449-c122076e1854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "76b54a6f-e93b-412f-bedc-30435725f95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "e8a96ab4-a1f0-4ec2-8d2f-de546f3419b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "66d91f22-52de-4d8d-b804-b7a5cfafbddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "99fd27f1-817c-49e4-987b-1f0ad4f1e713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 58
        },
        {
            "id": "3cbc2ca4-9139-4743-8c9b-90357ab501fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 59
        },
        {
            "id": "3a8028de-27b0-4aa2-b0fc-bc42fc395caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "bdcd2e7d-9ab9-43d7-b936-e1a3e43e27d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 64
        },
        {
            "id": "5c5b09a9-2064-4ae1-8ec3-b6038a02387d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "40795acc-80b1-43f7-946a-bbc7142380a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 93
        },
        {
            "id": "20b8f8c5-d02c-430e-bacc-f0d67b5648d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "0c22e125-44be-47da-bd90-cdbb7e24ae0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "1bca9c03-1e08-44ee-8650-4e87cba7c5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 101
        },
        {
            "id": "803defce-0197-4dc3-9831-c68cb333b15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "9541c9e3-db45-42cd-ac32-f229e0a18c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "8143d825-1c4b-4cf8-8bc5-da244e81963b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 104
        },
        {
            "id": "ecd88072-2e59-4ce9-be52-f8ddb2d26946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "39347370-5486-45da-a003-eae6480f1218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "9ee5963e-58eb-49e1-8405-abbbdbfd36c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 107
        },
        {
            "id": "1ad0f631-cf60-46f7-88c4-0505916cdbb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 108
        },
        {
            "id": "0046eb83-7ca7-456f-9dff-372889421e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 109
        },
        {
            "id": "1a11c937-d1b4-4a0a-8429-9de998469edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 110
        },
        {
            "id": "ae30d75a-b7c0-4d7c-ba2c-b761d97da5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 112
        },
        {
            "id": "db6295e7-cc6e-41b3-9f84-b42cc0d1eff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "4a2eaa97-0e76-423b-bb1a-e463564baf83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 115
        },
        {
            "id": "bbe9681b-df83-41eb-90a7-537953343ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "dcfe6381-a346-402a-9fe4-657e42442b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 118
        },
        {
            "id": "da36762b-9f7d-4506-a424-5be0dd3e4c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "de0485c0-1d76-4c2f-a798-dac74a8a24c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 120
        },
        {
            "id": "086315ba-2788-41d5-9927-2ff8d8251463",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 121
        },
        {
            "id": "321062a3-22eb-4b23-9097-063fa576294c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 122
        },
        {
            "id": "43449546-8d7d-47ad-b4e3-0db6254b1625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 894
        },
        {
            "id": "be709699-a380-4c89-aca0-24131df0eec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 33
        },
        {
            "id": "ab201e1e-9f2c-40ba-be70-318abc4c5be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 34
        },
        {
            "id": "03091c71-1aef-4526-b2d1-a1edd57407e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 35
        },
        {
            "id": "9e258498-1f24-4703-951b-7f3a727d32d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 39
        },
        {
            "id": "d6a6d6dc-4d3b-4c33-9b6c-c6197b9fb4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 40
        },
        {
            "id": "e6ffed04-4a70-42ad-88fb-9b7e8217ea36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 41
        },
        {
            "id": "5d9b1bd4-d780-44a6-9d48-93735287a2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 47
        },
        {
            "id": "a9cb3230-f6a7-4cad-956e-3be0b4cd29be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 48
        },
        {
            "id": "06f13a71-a5ae-4d97-a97b-4db0aa37182e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 49
        },
        {
            "id": "d2bde22a-7e99-4dc4-8615-6e92482dc5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 50
        },
        {
            "id": "c798741a-ab91-4963-8adc-498efaa3690b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 51
        },
        {
            "id": "1b70a801-9864-41c9-adb3-f1248bfc6aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 52
        },
        {
            "id": "f82c8a50-7636-427f-8d6c-35d1727a39b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 53
        },
        {
            "id": "108ef563-8901-4026-b137-fdb346a9007b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 54
        },
        {
            "id": "5777a0a9-450c-4afd-9edd-0e085259e3c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "377f74b4-681b-4d6a-8812-64a1f7660f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 56
        },
        {
            "id": "ea613e47-bd95-4eed-95be-3a3854453701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 57
        },
        {
            "id": "03fc997e-197f-476e-83e3-c636b4dc8dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 58
        },
        {
            "id": "5229b2fe-97c8-4d58-9f60-d0933fdb948d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 59
        },
        {
            "id": "5d11ea37-e11c-4312-8dd5-0c7db2528716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 63
        },
        {
            "id": "cc8c0b6d-a9e6-4688-b2f1-04be8b411eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 64
        },
        {
            "id": "92a52ebb-d29f-4f64-ac2f-9d382f69c6c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 92
        },
        {
            "id": "064e06b9-3db2-45d8-b007-eb9735b74907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 93
        },
        {
            "id": "314cef6c-5fb8-4f2e-9baf-53b2097b0b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 97
        },
        {
            "id": "590f23b4-d1f5-42a2-b0dd-96319fc2d3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 98
        },
        {
            "id": "7f206c4d-112e-4ee4-8955-60f123582ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 99
        },
        {
            "id": "8a382778-546d-4728-a573-a2c2da856dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 100
        },
        {
            "id": "59a60038-bb42-4664-8b8f-e9b94d8bf20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 101
        },
        {
            "id": "db0b15b4-4c75-4e17-816d-1687fb47031f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 102
        },
        {
            "id": "b32705e4-4082-4480-8c43-f022d6162017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 103
        },
        {
            "id": "66f63050-d1ff-41ac-aa92-a90e27069b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 104
        },
        {
            "id": "c53099bb-b83d-4838-a517-7454254af077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 105
        },
        {
            "id": "e282bd55-ff14-4e79-bf56-a1352b37bd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "f9b0bb0b-5e2f-49fe-82f6-bb7191b91df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 107
        },
        {
            "id": "5b5070f8-d1dd-48d8-bd57-a66e13899838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 108
        },
        {
            "id": "733ce3b1-e6ae-4328-a961-1390e435a17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 109
        },
        {
            "id": "acd0efe2-672d-4ee3-a5f9-62c85e9aef20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 110
        },
        {
            "id": "6eb14d9d-4a6a-47c1-a0d5-42c0ddd9bb3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 111
        },
        {
            "id": "2b49f9c2-f5c9-4276-b566-232cfb1d7e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 112
        },
        {
            "id": "6044b520-420d-42d1-9ea1-192ab3c759d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 113
        },
        {
            "id": "e628a813-5600-4134-9e94-a46ddc5eff0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 114
        },
        {
            "id": "52345942-04c7-4f42-af53-00ea01de74cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 115
        },
        {
            "id": "366aeff7-c162-4821-af60-5b2ad5e597b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 116
        },
        {
            "id": "38200a2f-2592-47c0-863f-91a52af53f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 117
        },
        {
            "id": "fa9638bc-4c99-4739-9364-a7f2f0e38e55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 118
        },
        {
            "id": "78b92815-40b0-455e-b48f-9868e3e02a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 119
        },
        {
            "id": "6d5b686b-c04d-4ed5-bf9d-c23d7769491e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 120
        },
        {
            "id": "442b877d-085f-4955-b0ca-80e71e69f0a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 121
        },
        {
            "id": "597db0a5-e50e-465e-84e4-b141c38ae0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 122
        },
        {
            "id": "fb6a9580-3963-4186-8f23-a8e9a5989ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 894
        },
        {
            "id": "47364a06-e67b-4087-bdbf-a731a73b0efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 33
        },
        {
            "id": "c301ae4c-b0f4-4e01-b0ec-1a3cd83ece5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 34
        },
        {
            "id": "edd07e49-ab34-4fb2-ba18-daaf9da6457b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 35
        },
        {
            "id": "6be57e56-044e-4246-a676-34ab36d00547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "cae186a9-578d-42b3-9ec2-3f9f2f634bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 40
        },
        {
            "id": "17baf2ce-7132-498a-8bf1-fc7a61ff0487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 41
        },
        {
            "id": "ee8e2b94-897f-44e7-9b72-29e45308ab3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 44
        },
        {
            "id": "dfbd60b1-6d60-45dc-beca-6db517c28d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 47
        },
        {
            "id": "6deea185-5459-4616-9533-7f47ad77583a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 48
        },
        {
            "id": "1e9ea93a-9882-45c7-83a4-2b3737c1e404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 49
        },
        {
            "id": "1c118ce1-b750-4fb9-b768-3d61eae2a8f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 50
        },
        {
            "id": "d425b884-d287-4b6b-afee-737741173067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 51
        },
        {
            "id": "2fb41587-23a1-4414-9a09-32ae9822ff81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 52
        },
        {
            "id": "93fb76e8-062a-41a9-9877-144f92870b97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 53
        },
        {
            "id": "e2c83dcd-e54f-4f46-ac52-4a3065f22832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 54
        },
        {
            "id": "cc855690-ca82-4bb3-9e01-9ee3f2ba89c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "dc5a7567-1f38-48aa-ba3e-026b0bca3b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 56
        },
        {
            "id": "a2e300b7-1df6-473a-b950-8035e56a36a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 57
        },
        {
            "id": "ce79ef2e-fe5d-4dcb-8816-20e38c08331e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 58
        },
        {
            "id": "039cf936-f58b-461b-9edf-f1bf18dfdba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 59
        },
        {
            "id": "42c43150-cd6f-443b-af78-9f3a58d181af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "ca6d68f4-796b-4fa0-81cf-14a9e62dac57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 64
        },
        {
            "id": "87b1dc0d-52ae-4d27-9181-3d1e884c479d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 92
        },
        {
            "id": "08b6508f-69ff-4957-9f3e-be965666561b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 93
        },
        {
            "id": "f3065b5d-e0ba-453a-8fd2-2e7c9bcf5828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 97
        },
        {
            "id": "454aa743-80a5-4614-9ea5-6718991b5c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 98
        },
        {
            "id": "f6e1d314-21ac-4a58-ba17-5b01eefcfa60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 99
        },
        {
            "id": "1525d10c-b9f6-45e0-9ee2-c9639f44bdca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 100
        },
        {
            "id": "e6d4de31-27a4-4af6-b1cd-5da5dbccda94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 101
        },
        {
            "id": "664ae6a0-4923-4aff-8ee2-de621a475784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 102
        },
        {
            "id": "7e041d22-a34a-4317-b944-56a1d3fad6a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 103
        },
        {
            "id": "ea4bced2-1382-460e-b2f3-30f952e4cbc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 104
        },
        {
            "id": "3ae31aae-84a8-4b73-be8e-d5948da28905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 105
        },
        {
            "id": "49073f1f-10d5-46ae-b780-1274342c33bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "221ac6aa-bc33-42a6-8e9d-105b56415959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 107
        },
        {
            "id": "2214485b-b1a2-4734-a7d4-6b56355f0319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 108
        },
        {
            "id": "52bcd51d-f67b-4ce1-9511-942f1d6a5bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 109
        },
        {
            "id": "8cd95dc6-66c3-4ef2-b60e-9f0d28f880c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 110
        },
        {
            "id": "86345b44-4ccd-47d1-806c-75dbbb376698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 111
        },
        {
            "id": "d77dcec2-89e4-4b78-aa48-be6784ece386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 112
        },
        {
            "id": "bb5f4e19-0eed-47eb-bd2d-271476b9d8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 113
        },
        {
            "id": "e4fe13e2-05f7-4652-a798-2f20afc3fba4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 114
        },
        {
            "id": "d00af2df-a2f6-4cd3-af6f-0550d0e5cfb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 115
        },
        {
            "id": "7d1c29e5-b264-43ed-a467-5eec8474cdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 116
        },
        {
            "id": "7b9307fd-412a-4b0f-abc8-8c593bc803b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 117
        },
        {
            "id": "49b208a0-7fa6-4c4c-88b6-f18be85aeaaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 118
        },
        {
            "id": "b13af1ac-9b7b-4986-8b46-794e64396ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 119
        },
        {
            "id": "652ab1c7-23c1-425b-99f9-212065b37198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 120
        },
        {
            "id": "5f740419-f7fd-472b-93d2-f4b3c4aa0d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 121
        },
        {
            "id": "5ee3c524-5e55-4237-aa9c-a1ffb2c7812a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 122
        },
        {
            "id": "7500f8bc-7965-4dfe-8eda-43add8879145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 894
        },
        {
            "id": "ff0de207-48eb-42d6-8ecc-e77602c1140e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "36e65e00-2a85-4c52-b7c8-59d4898c6caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 34
        },
        {
            "id": "8f343b9f-7c4a-4893-80cb-c8531615f18b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 35
        },
        {
            "id": "2740fd1f-ad77-46d2-91ba-cbe0b0614b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "9b436268-bd5e-46c3-a770-94e568d27cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 40
        },
        {
            "id": "cb0c4776-1040-40b6-897a-b8bf2f44e024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 41
        },
        {
            "id": "c7b79b0f-b968-4423-bc0c-eb2853d4cce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 47
        },
        {
            "id": "ee1677c2-33af-4798-8fe5-b4e298453f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 48
        },
        {
            "id": "256ee59d-9ef1-4ae2-a74d-5235a7642dc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "aad36c9a-1c07-41ea-8d3c-83a3068c2b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 50
        },
        {
            "id": "abc1a835-a1fb-4ff9-a5c9-8b1179e012ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 51
        },
        {
            "id": "34894a9c-c9fd-467e-ac68-adb5af15a263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "d4c61b30-2c64-40df-8f62-f4b35a315990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "8c9a758f-03d7-4eae-bf47-635a7fc8d684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 54
        },
        {
            "id": "4d13e40b-fdff-4986-8781-52a2d62d553c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "68c51499-76a9-47a4-82bb-c7944b1b5ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "a7563b34-63c4-4c35-b186-14ccd4b5b692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "f87ec6aa-46c8-4730-be38-fdac5a01616f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 58
        },
        {
            "id": "ea97ad9e-ff41-4a8d-a773-419caf3eb86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 59
        },
        {
            "id": "22fe6be6-29b3-4a69-8b66-5b5d7d424990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "77429e14-4bc6-4db2-86d1-22b637a8c4d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 64
        },
        {
            "id": "fce1f231-a535-4223-802b-287fd12471df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "9947de89-fc41-4d02-864b-8305442f6918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "71ba2311-abaa-46eb-988f-0d337ebea7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 98
        },
        {
            "id": "d3753472-60f3-4ea8-9f9b-7da07bbea25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "6f037238-2a34-4285-ab11-344897d38b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "2d9beb06-6c69-4640-b43d-ada4129a0613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 102
        },
        {
            "id": "989c946c-a78e-4ad7-942c-5c8ae3788342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "3e2840be-28ef-494e-bd5e-aa72291475e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 104
        },
        {
            "id": "f4c84fb4-fa49-43cf-bb00-12f136c08783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "bbfa82eb-7a0a-4953-b02f-668650f77c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "a8174181-a232-404b-a10d-92e6d480a060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 107
        },
        {
            "id": "60ac4ac6-922a-4e17-abd3-bf1aca3527d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 108
        },
        {
            "id": "7443c726-05f3-49bb-84aa-0ae903c4fbbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 109
        },
        {
            "id": "9d5c1c07-8ce9-4ee9-8f81-59394c3f4fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 110
        },
        {
            "id": "b623b0b1-157c-416d-8e58-6e96da1d1417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 112
        },
        {
            "id": "40ecd51f-c72f-4c7b-b4de-310933b5d569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "e7130057-aa9d-4328-ad12-ebdd4c534907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 115
        },
        {
            "id": "c18296d4-4584-4074-ac27-22e195898760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "6dc12e6c-39ff-4715-96e9-5e50b123d60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "1f20e02d-2d7c-47b5-9e4b-958acc4d0b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "65fb6bb9-75c6-49b6-884f-cdcd2b03f7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 120
        },
        {
            "id": "59e1e2cc-a469-4160-8223-b2b2fb37352c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 121
        },
        {
            "id": "bef3f6d6-87bb-4471-922d-3fda0a3be27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 122
        },
        {
            "id": "4513bf27-e904-4ece-aae8-c834d754b56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 894
        },
        {
            "id": "8c700d76-7058-4ba3-8af9-adccab4a6243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 33
        },
        {
            "id": "b46833ea-58c9-4672-b65b-f2e3a241ca5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 34
        },
        {
            "id": "75e3dfd0-3513-423a-8bbe-50347e3c2e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 35
        },
        {
            "id": "e4473e45-e07c-4319-9281-40addc13f2cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 39
        },
        {
            "id": "dffda36b-e17f-4b77-accf-323c1ca3a560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 40
        },
        {
            "id": "2886dcdb-aab0-4ee3-8e87-c7bc97223915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 41
        },
        {
            "id": "973a04d0-1532-4dd9-8b27-b2ec92c4e130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "be760c7c-e40c-4031-8448-30555c45215e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 47
        },
        {
            "id": "a6f7bbf0-7fb7-4b94-af73-f65c468b01e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 48
        },
        {
            "id": "0b467aa4-08f9-4a26-a74d-aa97e0e50b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 49
        },
        {
            "id": "3741bea7-3ffd-4d14-80a3-d4506299e2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 50
        },
        {
            "id": "ab1703d2-8e4e-4580-8438-f01347453fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 51
        },
        {
            "id": "cb5e0168-7f56-47eb-b6f7-a8158624b7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 52
        },
        {
            "id": "84ef8810-454b-4246-8ec2-12e0e2016924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "2f4fa9d0-6102-4960-8542-fac9080175f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 54
        },
        {
            "id": "5225f74b-52f7-449f-a52d-a39d8e28d672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 55
        },
        {
            "id": "31453686-6d35-4a92-b9e8-4c37dd7baf97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 56
        },
        {
            "id": "c05e01fc-bcf6-447b-940f-7b545aca0fa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 57
        },
        {
            "id": "6dca6619-8dcd-4175-989b-169c69ce9373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "ac8ae66d-da01-4ff4-9232-1d3ecf1fc20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "62a40bca-5fac-4076-b2d3-5fa45772f896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "89794bd7-3818-444a-8d76-9acda88c08a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "e0426049-8568-46e3-9765-cb8860074636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 92
        },
        {
            "id": "ff534252-3a2a-41fa-aa45-ba66f3d95873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 93
        },
        {
            "id": "c2cd8f8f-395b-450e-ba4f-6dec7c50f725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 97
        },
        {
            "id": "7931e6f5-2749-48a6-abba-a4dd1dc2f6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 98
        },
        {
            "id": "be8d7b4f-3545-42f4-baf6-124792c3abae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 99
        },
        {
            "id": "193981ba-2e88-4121-842d-1c4ceb62b647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 100
        },
        {
            "id": "ab3168d1-333d-49ae-b028-5ef27c5b9342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 101
        },
        {
            "id": "7877bda0-4669-4cc0-950e-07783fc0f18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 102
        },
        {
            "id": "3bb4d54f-da1e-45d5-a52b-00bc3d2cf328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 103
        },
        {
            "id": "3fae9e5b-6b56-4b9f-8520-98a9c8014ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 104
        },
        {
            "id": "9696b886-3ba1-4247-9377-85ecb275f9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "3e3478a9-101d-4e37-89cc-df09713bff8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "75fc5c2a-ad28-4f30-9fb2-873615a36694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 107
        },
        {
            "id": "063f5caf-1e67-47bd-a223-404e74368b48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 108
        },
        {
            "id": "bca8db57-d738-470b-a57e-ca16719591b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 109
        },
        {
            "id": "947e6e9d-1f21-4253-ab36-ab768f761b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 110
        },
        {
            "id": "e4a8e64c-8160-481e-b0b2-8499b80673e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 111
        },
        {
            "id": "4a61cb39-4b5b-4ee4-92b6-b77567cc2d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 112
        },
        {
            "id": "19d02b3c-6d7f-4ccc-a524-3922bdc6ff43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 113
        },
        {
            "id": "6a6ef9d7-a110-4158-a66e-75914704026e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 114
        },
        {
            "id": "f295cf21-8c08-458c-ad6b-d0870c07c670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "2dc3b2c4-f3cf-411a-be2b-d6bd13a60769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 116
        },
        {
            "id": "94499412-9c4d-4843-9030-be0fc5789122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 117
        },
        {
            "id": "63b1ad44-9339-4c7e-bcb8-78d33423308a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "93a17b39-2638-4712-ae89-52475d20d7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "f3a8e0d7-35ab-4fb7-b52e-e7f5f214525c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 120
        },
        {
            "id": "b42186a1-9202-48ec-b1a2-4cde21a3c247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "6714b7f2-7be3-4d2c-88be-abb8be53047f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 122
        },
        {
            "id": "5d1ee747-9e83-4605-b428-71dfcec619fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "8c6b02f2-2046-4818-9872-629cc90931f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 894
        },
        {
            "id": "34a2eec6-96a8-40e6-beee-da1070bbf982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8208
        },
        {
            "id": "e3c27115-07db-49c4-a7cd-dbb8d8d5a765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 33
        },
        {
            "id": "6165376e-dbb6-4090-9494-9d861a1ed50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 34
        },
        {
            "id": "52837953-3944-4b91-9329-bc20e6c2e0fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 35
        },
        {
            "id": "f1465a41-5e99-41ef-9f05-0f7658dbdc79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 39
        },
        {
            "id": "6e4235c0-9976-4589-9b9a-969735ec28fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "02a8ec2b-adc2-41ae-8838-cad923353e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "65be8697-38be-482d-aac7-7a64ed61a362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "0aa2c07e-6f39-4b01-a2ba-c99108db8c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "9f0c7900-b001-4367-b61c-18c2b30f2df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "580772e4-53af-41ef-a435-4e47e82b3761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 50
        },
        {
            "id": "9dff487c-a6d0-43bd-ac39-ce0720e63822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "e9922229-7670-4a6d-8bb9-dc9fcaf25e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "0ab92aa2-325c-4c94-ad89-6354776e0187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "ce82c183-a233-41ef-9a15-c253414cb535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "4b8dbeb8-d112-4179-b5b4-a80c156f756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 55
        },
        {
            "id": "1b2b7926-4beb-4e81-97f3-51ee671154e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "fa7103ca-fa44-4588-bff0-1660c928928f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "0238ea39-84b3-4da9-bee6-57303e4f5d91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 58
        },
        {
            "id": "7035a3d2-c2ce-4e1c-8b24-c8088bbf8310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 59
        },
        {
            "id": "a1b1a82a-1f34-450d-8ce3-d8ecb87334c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "a27a3f68-b8b9-4d8e-b046-bb3a53386de4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 64
        },
        {
            "id": "8c1dc786-32dc-48d6-90f5-14426493f59a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 92
        },
        {
            "id": "58565337-e4d4-4b54-9efd-4589246bb513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 93
        },
        {
            "id": "03e351e0-5157-46eb-ae18-974d9b2a03d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 98
        },
        {
            "id": "29d74f49-1c68-457e-94e7-c4bdb9075dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "07fc4dca-5981-4c4c-8536-67d9066563f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "0e8c80a9-9e28-4273-a007-6ac740a39c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "1014d278-8089-4a8d-9d35-4bd81bf48605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "e591726a-b8d2-4ed5-923d-b656ed7f249a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 104
        },
        {
            "id": "f5b274f2-c1e3-48da-abdd-8546a0a41dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 105
        },
        {
            "id": "e3250efc-0809-4fcf-8ae6-7b208bd7cc19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "397cfe8b-1ea2-4b43-8102-e1cd3dca3d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 107
        },
        {
            "id": "dad87f0a-0068-415f-9ac1-715d0e876b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 108
        },
        {
            "id": "644ebe43-c373-4776-a7c5-dd0863ddfaa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 109
        },
        {
            "id": "bde79213-ae38-43bc-9b35-413675e3d3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "3a3df057-885a-4c97-9454-2c2e9ff50d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 112
        },
        {
            "id": "b14a6e50-badd-4662-8bce-7013ba775aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "05ec3e40-b72f-49e3-9bea-e43d4080ec3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "04f72ade-fdce-4d6f-8874-dd0ae529cee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "326f422e-4285-4c72-920b-dbbbd499bc62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 118
        },
        {
            "id": "1a28db84-73f1-406f-845f-3dc07ce2b642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 119
        },
        {
            "id": "8df492f9-cc1b-4b69-891b-97de5e8c9c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "6cf08c20-452f-4f83-a848-ef22bc38b0d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 121
        },
        {
            "id": "fd2c7835-c237-4b09-963e-afebf2bea170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 122
        },
        {
            "id": "06c7c9d9-019a-4ff3-b574-a32eb6f61688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 894
        },
        {
            "id": "20543ebb-60d6-4cec-b04b-89f7214a959b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 33
        },
        {
            "id": "bb456f0a-249c-4743-8eef-72eb2a5a5dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "352b5125-3bc0-4cb1-91a9-8c88e271299b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 35
        },
        {
            "id": "7d81a6ea-4a4e-4cf0-bc4d-2c12322c72ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "69ec1bac-823b-45cf-8b97-6226abe38b3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "cba0a636-0834-4df8-bbe9-0ca82be10554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "1aa08ee2-859a-4548-b909-f5efec4af141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "b801c068-af90-4ac8-95a5-50bb8a87e74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 48
        },
        {
            "id": "5520d68e-6f7b-4ff1-bf3b-8ecd77e71dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "3bdcb85a-07eb-49d0-ab76-d1ca2078e226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "26f653c3-30f8-4d56-816f-78cb4c6f2b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "e2540f4b-53a0-4b36-9ff2-c156fad3493f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "6c3a1e74-63c0-40c7-88db-52fdfd4721ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "c3b16001-ae5a-4b58-add9-19235f4b272d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 54
        },
        {
            "id": "031b5ff8-5767-491f-a02e-2a0f8d3b10cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "44435070-ac19-4fcf-86b8-77eb0cd03ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 56
        },
        {
            "id": "4522434a-93e9-4cbe-9868-afda0cdac14e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "57efdc91-ce8f-47ce-bd7e-548d4d5ab98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 58
        },
        {
            "id": "838321e7-dfff-4e90-81d6-f2272527a70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 59
        },
        {
            "id": "2558f2fd-15d7-4aeb-99aa-4d81725f205b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "6ab2c893-7a58-4aed-9b46-fe8915a382d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 64
        },
        {
            "id": "49b33e02-2e51-40dc-931a-291ee3b540f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "a9d101e5-3d72-4237-9e79-e735a4ff35ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "1ae37301-a61f-4e5b-bad1-a3a5632d47d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 98
        },
        {
            "id": "f638b975-b3de-42d3-acae-406ade0df64c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "cda5e6eb-5603-461e-9bb3-d23e6e00219e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 101
        },
        {
            "id": "d3e17de8-aeb3-4385-9b9a-04791823299d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 102
        },
        {
            "id": "dd23fd51-7275-4342-95de-bfc4caf42bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "fc353524-5c7a-4c87-8f21-89310f1eec51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 104
        },
        {
            "id": "4398cc80-d3eb-4456-a1e9-d21e4e7c6dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 105
        },
        {
            "id": "a968aa32-ec70-4e44-8809-67461abccf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "e11cbb7d-7935-4667-ab06-fc4b6d41fe4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 107
        },
        {
            "id": "460ff2c0-32f5-472c-99dc-acdc8b128c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 108
        },
        {
            "id": "07297d74-866d-4b21-b860-979a011c97c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 109
        },
        {
            "id": "d44077ba-32de-4d3c-9191-bc9ef2427ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 110
        },
        {
            "id": "5af10300-3839-4357-99e1-9c87ea5482a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 112
        },
        {
            "id": "725d4c06-6c38-4a03-86d6-1a816ce3d9e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "fcb22d9b-57ac-47cf-8135-d8932fe11fb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "43eeee41-d3c6-4e15-a261-1aa44c2de38b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 116
        },
        {
            "id": "4413bdb9-ab58-4e54-8d7f-78c7f641b9ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 118
        },
        {
            "id": "6466234c-c936-4f8f-bcb8-34a51ecfc4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "61fa63be-328b-426d-8278-589cb237cc09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 120
        },
        {
            "id": "dcb2de5a-59b3-48c6-ad30-5cd07aa964ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 121
        },
        {
            "id": "2e03ed3f-a683-4974-b10a-d0e3c5d83799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 122
        },
        {
            "id": "5c93bd6f-d067-4a9c-9220-dbe536506053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 894
        },
        {
            "id": "eb71f2f9-dbc2-4c1e-9876-8c29cf5f42c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 33
        },
        {
            "id": "8788ea6f-8c0d-4a15-9854-539cd05cae8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "e5fdf46a-0306-4dc3-966b-9b871aa672a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 35
        },
        {
            "id": "4241d59c-4f4c-4466-8bbb-4d50a9b9002c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "37f92aa2-c5c6-478a-90ae-31a41e16da57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 40
        },
        {
            "id": "2fc67310-a49f-4a5f-a8d9-48700e281684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "46bbe601-9108-411a-bbbd-cf0bf2df1cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 47
        },
        {
            "id": "5f85fb30-74d0-4bad-8d91-28822b5450ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 49
        },
        {
            "id": "fe9f5c97-39d8-479e-9027-9ed9c3886c96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 50
        },
        {
            "id": "ef1b9c72-9624-47f3-b41c-05c50d4a7e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 51
        },
        {
            "id": "d8853a96-af5a-4fb9-95d6-3de5ad7e438a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 52
        },
        {
            "id": "a0636461-23cf-4212-bea3-bde5ff4c5913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 53
        },
        {
            "id": "37049bb7-dedb-49e5-8dee-4d7215386fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "f198b134-2357-4a46-9cd6-cad4b1389def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 57
        },
        {
            "id": "13ac786b-e831-43d9-8e06-3ce1def4e21a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 58
        },
        {
            "id": "ee5c4539-062a-496e-b88f-016bdacfc140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 59
        },
        {
            "id": "c2083021-9c1c-427e-a051-1c0e66d9981f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 63
        },
        {
            "id": "c86f393f-9856-414d-97c6-0ae3aba017ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 92
        },
        {
            "id": "efebe6c0-eca3-4746-ad65-70b302cfcbf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 93
        },
        {
            "id": "2f562321-4097-4f08-a6d6-0e5ce66ece5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 103
        },
        {
            "id": "600b15e0-1884-4dbd-9f0e-c9ebf8aecc4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 105
        },
        {
            "id": "22e0df19-f188-4072-b05a-1857beb5fd7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "4c54a40f-00ab-4acd-8239-559a6e7ae79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 115
        },
        {
            "id": "dd5a28c9-9be1-447e-96bc-dc8d2132fb86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "c77cd10a-771c-4721-b113-536352d80023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "d85eb30f-c65c-4983-b0c9-acec0309b9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "83c4976c-56f7-420e-a54c-f5e8e31c9bbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "b0b97160-a626-433b-9bfd-eddcc5b536bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 122
        },
        {
            "id": "5c39596d-0bff-4656-b109-36d277834908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 894
        },
        {
            "id": "5aef33da-711c-49a5-b588-4585712c8ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 33
        },
        {
            "id": "a2ba3df0-fb9c-4ac9-92a8-76dc040e6228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "45ec5419-8397-419d-870b-080478460d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 35
        },
        {
            "id": "cff73644-81db-42f5-880f-96ed16b0344a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "6ac3df3d-3c3a-4ebc-b979-1aa3bb75decd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 40
        },
        {
            "id": "ad959983-1fe1-4d01-b078-3bdd1f26f992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "5fca6f6e-88fb-455f-b87c-ac56cf16b2f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "85665934-388d-4112-849e-0facf6aa726d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 47
        },
        {
            "id": "43448af6-339f-4ffe-a8df-c514142716d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 48
        },
        {
            "id": "d5df19e4-0736-4c40-8792-005289e34abd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "16f2428b-3806-4db6-a10b-7ee6a8cc9733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "dc85f6d8-7ff1-4f4b-af45-d64f4547e0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "e03686d2-fe7e-4555-bba4-cabf8acb4665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 52
        },
        {
            "id": "ea99f49f-10eb-48c0-8777-ec05ba578893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 53
        },
        {
            "id": "bbe04ae1-6799-4299-ab60-31426e022cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 54
        },
        {
            "id": "656d1e1b-c90f-42fb-a053-4d80abf90e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "7d5fb47d-a031-4a3a-80c8-482bd0ed54bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 56
        },
        {
            "id": "f87f85c0-f91b-4467-ba99-088d7d9ef83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 57
        },
        {
            "id": "24c1190f-6894-4889-9820-672bd44af27f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 58
        },
        {
            "id": "492e8b27-68ef-4fd6-8050-103bd4578e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "0ee0b537-0212-4918-9604-3cb1f0397843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "e47e0b6f-cec0-472b-a627-826f381bc82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 64
        },
        {
            "id": "750f21b7-4cc3-4183-90dd-d6bbc367931c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "c1d7e95c-237a-4efd-9f52-08c14d526450",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "3736406b-7414-4613-8119-34ef6d5feedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 98
        },
        {
            "id": "7d229fbe-a411-49e1-b23b-167c5bc3abf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 100
        },
        {
            "id": "ea3b7518-2b72-47be-b020-1712b53da6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 101
        },
        {
            "id": "bc96d9e7-4322-4ea5-befc-e2f93e3ccada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "6c9c8f2a-c29e-4236-a132-47c272ece0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 103
        },
        {
            "id": "5e0e7694-babd-4103-b502-7b7bdcc6de9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 104
        },
        {
            "id": "78c3ed56-edf2-4fe6-9927-db9da4833964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 105
        },
        {
            "id": "73a2ec6a-9437-4825-8b4f-c8062086adac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "6c6856b0-6f97-4efc-bee5-7088c744dd43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 107
        },
        {
            "id": "1bc98095-433a-4a28-b068-bb0962409cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 108
        },
        {
            "id": "25206f5f-4498-47f2-9584-cf1e38f7e7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 109
        },
        {
            "id": "eda49a1e-1aab-43e3-b4cc-5722ed0a7bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 110
        },
        {
            "id": "5eb308d3-fdc1-46c8-bac8-1d3aff8efbf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "133c1829-7778-4990-a449-94708e7b8eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 113
        },
        {
            "id": "13b1ddf8-a0e9-4c1f-972d-9e39ebe84e4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 115
        },
        {
            "id": "a28aa2ae-2939-4bfb-8103-ba3b4cea0a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 116
        },
        {
            "id": "09a31e8b-11f2-494f-884b-a17ca3d276ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "c2a956f6-1c9d-411b-b3b3-dacbbf36b31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "99fb04b9-e352-4158-a590-2b48b34060c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "09c7ba6c-6182-46a9-8d81-8ad68480a777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "3f4aa72c-6984-4a0a-a249-095e06ab20cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 122
        },
        {
            "id": "fc7f3e3b-2fe5-4f59-9f16-e59e54a73a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 894
        },
        {
            "id": "2c1dc404-d506-46dd-838e-ea0794909985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 33
        },
        {
            "id": "1044af28-9b87-4615-b4f2-147d07e16f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 34
        },
        {
            "id": "3f606b40-043e-4ded-9289-e7992bdc259b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 35
        },
        {
            "id": "e7cd80b8-49e5-4815-af28-28ab7f4b2dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 39
        },
        {
            "id": "6032678e-1372-494b-98b5-bc18ac82e148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 40
        },
        {
            "id": "1a088ff1-9127-4ea9-8e10-e72cce800acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 41
        },
        {
            "id": "55e2fc59-8e0f-49a2-b544-7854a7f1a955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 44
        },
        {
            "id": "7106d6fc-0eb0-476c-8364-b955ba634bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 47
        },
        {
            "id": "5f15cd4e-a16b-4f39-98eb-530856f5f916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 49
        },
        {
            "id": "780460fc-a7b6-4164-b666-1770ff7a276f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 50
        },
        {
            "id": "834d5e48-ccf9-4ab9-954b-6968e2a1d278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 51
        },
        {
            "id": "9c60aa5e-73e5-44ce-b7d4-f8a8af66981f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 52
        },
        {
            "id": "e942fd4b-bb14-4f84-905b-bd60ec372687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 53
        },
        {
            "id": "914c9bea-1af7-4ca0-bdb2-c47a4fa1a000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "bff64655-f54c-4569-bbcc-b359c074b513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 56
        },
        {
            "id": "afedf6d2-0be3-4821-abe5-ba3bb06dfc06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 57
        },
        {
            "id": "256c7567-5266-4824-850f-aaa9433f2f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 58
        },
        {
            "id": "52d267c9-b579-43fe-a12b-543cc9f1c70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 59
        },
        {
            "id": "41970830-9d7e-4963-bc7f-19fe5f1cbdb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 63
        },
        {
            "id": "702c379e-25f0-499a-be28-f7bd0c30c07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 64
        },
        {
            "id": "26ee1161-1be5-4945-b342-be180181cd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 92
        },
        {
            "id": "423a58c6-3ab8-4d49-9f24-661e976357b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 93
        },
        {
            "id": "6bd97638-4b3b-44bf-9a12-0094d84f0ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 100
        },
        {
            "id": "3bd39484-3adf-4db6-8b10-7a19dd4e95d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 103
        },
        {
            "id": "59ce8e2f-3030-4035-bb7b-3012d0912456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 105
        },
        {
            "id": "bb7ce97b-fadf-4093-806d-5e4e7fecca16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 106
        },
        {
            "id": "7a234ee2-1735-4153-afff-d170b0c4dff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 113
        },
        {
            "id": "7cc1f361-7906-487e-9845-7a168c965584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 115
        },
        {
            "id": "6fb66acf-104e-4c43-982f-7afc0f12bfba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 118
        },
        {
            "id": "ad16c2dd-5375-4497-a36f-e5ab32cd5460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "18ccc7d0-2bb7-40b8-90a3-244ecfe2b1f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 120
        },
        {
            "id": "aaf85470-090c-4716-bff9-6b04c9cadec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 121
        },
        {
            "id": "de563f70-efbc-4ccd-ab0c-02870dacd26d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 122
        },
        {
            "id": "153eee10-7aa3-47ea-88eb-4a076f76948a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 894
        },
        {
            "id": "eafaea75-4f4e-4664-9866-6fafe4baf827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 33
        },
        {
            "id": "496e8acf-225e-41ec-8460-02acc106b476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 35
        },
        {
            "id": "690b1929-6674-475b-bdc2-73ee3f36c94e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 40
        },
        {
            "id": "1b01071f-b5df-4ed6-97b4-dbdedd145f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 41
        },
        {
            "id": "eed24c22-87a9-4b59-abb0-44f5a935e3e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "f4a54c3a-cb1e-45a3-9da4-c390bc69e271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "75063c51-d08a-4d22-b4a5-d644928ddbf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "ec58d617-5091-4701-b260-ebff981b7f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "b3dfe758-8475-4e15-b2e0-b46a82e4fe16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 48
        },
        {
            "id": "47bf2ed3-35f1-482d-8eac-32c47fc87ebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 49
        },
        {
            "id": "604168e2-c0e2-4bba-88e1-b3b09164252f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "dcefc261-5bbe-4e43-b6dd-10dc0c193a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "4b23b346-9a3b-43cb-8196-2a830cc5833d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "cc6a6e80-5a53-44a7-966d-6fa9244d3cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "3e5e7fd3-02ed-4ac2-9f21-6c74a32c4b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 54
        },
        {
            "id": "190e9aa2-e7e0-427b-8948-37bc45c4824c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "3ec991a5-931f-448f-b914-945c55b30271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 56
        },
        {
            "id": "6f93fd31-79ba-488a-8a29-d0915e913b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "a74ac1ae-fe40-4207-9b34-e1f409624439",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "f399d431-2e59-4b7a-b0ce-4cc0b04fbf7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "996124c1-6814-434e-a13c-33c9f1d6fd22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "47dcb74a-525c-4fd8-ab4a-1b37d2057e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 64
        },
        {
            "id": "d9814734-f93f-4486-9684-09eea5d7e0cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 92
        },
        {
            "id": "d39e9066-7202-48e3-b77e-97ab8176e576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "b0acb630-7e82-45b1-ae6e-a2dad33ac706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "84b8708e-947a-4975-92dd-7a94ef80a90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 98
        },
        {
            "id": "eb724268-dc29-446c-910b-b24c433dd57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "f22b7ec4-a4c1-4fc6-9517-df8f805db7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "2864a646-f1d9-4063-b597-e3d6fd2a46a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "bb5844b5-9203-4cb3-ada3-bd23721e6536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 102
        },
        {
            "id": "dd00e779-2054-4907-ab9b-7739f5d269a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "def98d49-be08-403c-9d85-5123b2a0da49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 104
        },
        {
            "id": "ae09fa66-49b5-477e-ae16-7efcd1704c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 105
        },
        {
            "id": "0d41ddad-11d1-4df3-89b3-59dc9d77de74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "3d1cdfba-4282-45bc-a678-f8b7fbdf5e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 107
        },
        {
            "id": "c155a5fb-28fb-484c-9ca3-23c1f208f865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 108
        },
        {
            "id": "43d6a223-f047-46ed-8a87-ba1d4bfae9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 109
        },
        {
            "id": "e34a4128-3f73-497e-b535-e73c7503cc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 110
        },
        {
            "id": "0708f3b7-2ab9-4742-a358-15cc0c7e2ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "3e84c92d-a4d1-4f7c-b3b9-fbf0396a8f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 112
        },
        {
            "id": "084abd26-cdbb-4a8c-9a66-514b5584cbee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "582195e8-c7dd-475f-9829-2d51bca89c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 114
        },
        {
            "id": "4fd17bd1-9f7f-471d-8f22-38855d344b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "935042f9-9128-43cb-ba28-d338baf9413d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "98810c51-3e94-421e-a2b7-5b5b41abd68a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 117
        },
        {
            "id": "f0e6571d-660d-4aae-a6c8-ffad455d63c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "6ad09a1c-363b-4a05-b4b2-8e038b7e79e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "dd68fb31-d8c6-4054-a78e-a53b0346034c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 120
        },
        {
            "id": "f683faa7-26de-4728-935b-75d5e75aafbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 121
        },
        {
            "id": "a58d90f0-e9d6-4b20-8508-fe61b0fcb3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 122
        },
        {
            "id": "1c0facec-9224-4092-ab15-5b64d6940a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 173
        },
        {
            "id": "d8bf5571-44b2-4ed9-b529-e297336f88e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 894
        },
        {
            "id": "222152ab-5d8f-4a29-ab9a-0c32678ecbbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8208
        },
        {
            "id": "33fb2043-ae0f-459d-93c4-af830b2e8ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 33
        },
        {
            "id": "d2cc37a1-ceb5-429a-8778-dfd4adaaeac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 34
        },
        {
            "id": "c0aeb085-1de9-4c3d-9d1c-cdf838826eba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 35
        },
        {
            "id": "208c81ad-007d-47fb-a513-d384f3a2ad6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 39
        },
        {
            "id": "ac16fec5-91b3-432e-856a-2c15b906a964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 40
        },
        {
            "id": "b7bc64f6-9d3f-474d-a294-43a7acd80905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 41
        },
        {
            "id": "27e459b4-4140-4b79-bf9e-b503253fb4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 47
        },
        {
            "id": "806ea61a-0384-4a98-83bd-860af316ca2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 48
        },
        {
            "id": "c2e134be-6549-48aa-84f2-d3a90f0e3d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 49
        },
        {
            "id": "9a52a32f-054d-40f6-9557-c0e1a5f8cd3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 50
        },
        {
            "id": "b6f1ef8b-fb62-4580-83a5-9598ee72a6f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 51
        },
        {
            "id": "253ce3c5-e988-43f0-a50f-8ae400d5fd53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 52
        },
        {
            "id": "e0592a22-1fbc-4725-b983-52128cd149f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 53
        },
        {
            "id": "863a2cc6-3edc-4aaf-99f7-72a76e9754bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 54
        },
        {
            "id": "554f2703-e265-4780-9a51-89db320504a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "931b3cd5-4604-40d5-a155-9ef675df8e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 56
        },
        {
            "id": "eae250b6-f620-4e78-90a9-2363dd9154b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 57
        },
        {
            "id": "177e0bcf-c6ee-482d-ae82-f95d6ffe6293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 58
        },
        {
            "id": "c5c03e9e-6bdf-4d93-9d3b-ade38ff79690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 59
        },
        {
            "id": "2b15e5b6-e730-4463-8f40-a4ff6e3b996e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "4e5cb24f-0e53-4335-a342-aa1bda9f75ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 64
        },
        {
            "id": "27acd4f4-7f5b-4bb6-8c94-e6a9d0aff043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 92
        },
        {
            "id": "2ead2c27-d06d-45c9-b4fe-d3d757207e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 93
        },
        {
            "id": "cb110d6a-ef46-4cba-a1ad-67e95197fcb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 97
        },
        {
            "id": "7f535e7a-7019-435b-a92f-787e6fc8a80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 98
        },
        {
            "id": "adee268d-0758-40e5-ac6e-7ce4a35fe846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 99
        },
        {
            "id": "df66751f-91bf-48e0-9e82-6a0af10f71c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 100
        },
        {
            "id": "aabaeefb-53f1-43ba-a9da-ae9c40771e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 101
        },
        {
            "id": "5fc8fcd4-a6a2-43c8-9d89-def243d740f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 102
        },
        {
            "id": "fcb5360e-ae28-49f1-a218-a9bef02d7a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 103
        },
        {
            "id": "e1a75945-1d14-4b9a-8f78-771aa9979e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 104
        },
        {
            "id": "ac85d89f-a450-423a-9160-2f4b6efa970f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 105
        },
        {
            "id": "07420cfe-c503-4f41-8193-6cd6b125cdaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "0e8de107-7801-46cc-86f7-43f96f17679f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 107
        },
        {
            "id": "4e1cbbde-bee4-4dc4-a491-557f9ebe6301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 108
        },
        {
            "id": "b46cb672-4f00-474c-9345-395fedf828cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 109
        },
        {
            "id": "172c1227-a377-4136-965f-e42571c95d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 110
        },
        {
            "id": "4752b6fb-fe1f-4bca-8116-d13aa9600938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 111
        },
        {
            "id": "86c75203-08d0-421f-91ce-9924363d361a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 112
        },
        {
            "id": "2adc6fbf-9c46-4278-a5f1-fb23bfb99dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 113
        },
        {
            "id": "86415c57-0f11-4d9b-8005-a730446624c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 114
        },
        {
            "id": "e9f2de9e-6f7a-445a-b3cf-004f651c4561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 115
        },
        {
            "id": "ca0edde8-d211-4d66-9bf5-59f538fde791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 116
        },
        {
            "id": "9eef3633-0b0d-4743-9f76-1c2c561165f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 117
        },
        {
            "id": "cd028101-9dbf-4ed9-a518-db6fc8de3ed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "42b7f286-6c89-49e2-8801-976c586a6d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "a8b27103-5a50-4b0c-a890-e03779d66f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "0667de44-25e1-4382-a664-8b0fa4a9d726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 121
        },
        {
            "id": "e3705828-9d96-4a49-a7e6-259fe8ecfaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 122
        },
        {
            "id": "322ca628-1e4a-46fa-9a4d-dc57f9d6e119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 894
        },
        {
            "id": "df1ad734-98df-4df9-be8d-7f6ba5c21a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 33
        },
        {
            "id": "34eaebb9-a11a-42ef-b567-1b3b24c436d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 34
        },
        {
            "id": "533b5e05-a50f-4d8f-934d-59164dafd56b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 35
        },
        {
            "id": "0734e5d9-fd4d-4c96-80cb-5be80bf4ef12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 39
        },
        {
            "id": "ca3ad49a-b38b-4d70-a233-0b0350e64059",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 40
        },
        {
            "id": "98bcf7a4-697a-4747-b70f-415126f8ff72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 41
        },
        {
            "id": "565ca941-7eb5-401d-b400-85335f0d9f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "057e4385-155a-47d6-be3e-905736aa3652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "8c0e9d7d-5646-440b-b7a0-58ed065292fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 48
        },
        {
            "id": "21eebaf6-7b20-4f33-b3a6-0fef64c34157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 49
        },
        {
            "id": "0bc0baf7-401a-48d5-8024-062631de7064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 50
        },
        {
            "id": "ba1391f2-3fb0-4326-b866-700b607ea97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "b0186e13-3ecc-4118-b308-ee4d29257901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 52
        },
        {
            "id": "5b407975-397f-4fe2-ae8b-47a1dc84be87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 53
        },
        {
            "id": "36a2421a-039e-4e99-9d07-0a2c77c25c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 54
        },
        {
            "id": "d41e4d07-d801-4146-aa2a-80441f9327e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "233d7ec1-efaf-4647-9a24-86cd84cf3189",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 56
        },
        {
            "id": "ecb0dba5-a2a9-4328-81fd-4c1c87221360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 57
        },
        {
            "id": "82dd1e1e-d337-4151-a7cf-950a85c83e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 58
        },
        {
            "id": "b9077bbf-e4e9-46d3-8278-745da2dadc73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 59
        },
        {
            "id": "9edb8804-b9fd-4fcf-91e4-2d2edca9a188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "bf39ab50-81d6-471b-bc7c-13c8179d9572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 64
        },
        {
            "id": "155aa90f-0e71-417e-8b43-e252dd2d3cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 92
        },
        {
            "id": "1fa705d5-dfc2-41b3-ad82-62bbe5d04e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 93
        },
        {
            "id": "c3c46667-a880-46bc-be88-0686ba6a0f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 98
        },
        {
            "id": "b1cdde46-af68-4d2e-a661-03b9df195bd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "f093250f-f50b-405e-b24c-1c17216fd9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 101
        },
        {
            "id": "fca55ff1-3d6e-4092-9ff6-b9ba2ca98953",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 102
        },
        {
            "id": "5fdadb55-7bba-4aca-b7ad-8c97415bd475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 103
        },
        {
            "id": "9e0a7275-4b97-45b7-ab97-5796585d6ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 104
        },
        {
            "id": "0ff75213-ada7-4e7e-9a98-45b6ff3e08aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 105
        },
        {
            "id": "9f780336-323e-40d0-a1c4-b746d8e3105a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "b5291959-a856-4748-a339-7922f12a93b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 107
        },
        {
            "id": "d78e4c0e-17c3-49ce-90ab-7e99c6647f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 108
        },
        {
            "id": "8ea71715-31e5-4182-9965-1eac173664dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 109
        },
        {
            "id": "a7adf1b9-5aed-4fdf-b9c5-e7bc93b09e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 110
        },
        {
            "id": "8a533785-3874-4673-8669-2a6ce9eb71d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 112
        },
        {
            "id": "b26f0b4e-5632-4d8d-b2d9-76312f11b58c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 113
        },
        {
            "id": "ffa7cbfb-fd68-4b39-bd31-7a70a0cc8ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 115
        },
        {
            "id": "8eddf49a-19d4-4d91-86be-c449acc17049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 116
        },
        {
            "id": "ca21a1a3-d271-417d-8dcc-017a49352a4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 118
        },
        {
            "id": "86e94c6c-629d-4006-9023-9bc5f16b912b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "bfba2b9b-befa-4fdc-bd67-479bf032cfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 120
        },
        {
            "id": "e52aa0f9-5875-4a6b-96db-43f8cc4fffab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 121
        },
        {
            "id": "3244d0ca-debe-4328-a916-8a047a458d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 122
        },
        {
            "id": "e83a1a22-637d-47d0-840c-98ce76c847cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "b688755c-0902-460a-902d-f8cbf17e3534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 894
        },
        {
            "id": "693f0f55-7b35-4243-96a0-bcd2064d1838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "bc872862-c492-4e67-8cb4-ec86f799c4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 33
        },
        {
            "id": "5dcc2643-8ea1-4b66-bef1-8315304d17c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "540a5d79-7fc5-453b-85a3-d9247b073240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 35
        },
        {
            "id": "4630cc14-99e2-4335-a662-3ad867b2c28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "b2d0718c-b0d1-4dea-9887-01d5d982466a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 40
        },
        {
            "id": "54194882-cb1e-4f87-bf97-3feecc92460a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "e7377e1b-48bf-443a-818c-af874c6cd209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 47
        },
        {
            "id": "7c43ecf0-de25-4028-98cf-086a1cdbc62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 49
        },
        {
            "id": "dd2f5a92-0986-4906-9166-fc814cf296d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 50
        },
        {
            "id": "4a2ea9c9-34e0-4fd5-88a4-b02352d955a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "879e5e34-a282-4947-ac3e-7364ede16644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "564f990a-7edf-4164-8453-6b94af7be68f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 53
        },
        {
            "id": "e7bcfb46-b092-4ed7-bc65-4553d6606927",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "c7e33f30-b1dd-4f8e-b8c1-10466e578f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 57
        },
        {
            "id": "c10d7f8c-1e0c-4a5f-8ae6-bf5539108aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 58
        },
        {
            "id": "3e684dbe-7452-4094-b5ce-ec2ab7cd6031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 59
        },
        {
            "id": "43df12d1-6f63-4756-9331-712b358488f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 63
        },
        {
            "id": "d39974d4-84e5-4d8d-84eb-bad75dd9a3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "0c2cd36e-bb68-4518-8bed-52e4d98bed94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 93
        },
        {
            "id": "90dbb2ee-3605-468d-ab46-d33e88d5527c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 103
        },
        {
            "id": "76660d8f-1df4-46a1-850e-7e3dce1287d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 105
        },
        {
            "id": "99dc96eb-d0ce-4a0a-88cb-47a55fc90d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "47d49421-8552-48a9-a06f-e155452ab6da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 115
        },
        {
            "id": "a346c43a-8e96-4ca2-aa50-0b19f3c900c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 118
        },
        {
            "id": "a9d5c8ef-b142-467b-a610-f5a82b6c8bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "6f111d0b-1431-4b0a-ba9f-6052911343c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 120
        },
        {
            "id": "2cbbe53c-444e-4828-ace5-8687677f4a61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 121
        },
        {
            "id": "0764f17e-353f-44eb-9743-14045cef37bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 122
        },
        {
            "id": "bd8261f1-7e43-4890-913f-b4cfcac869ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 894
        },
        {
            "id": "f09981c4-4e31-4e78-beb2-85892e3074b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 33
        },
        {
            "id": "f1fcd85f-e3af-4a8b-836a-17452a34d14e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 34
        },
        {
            "id": "856de9ec-442c-4447-ac59-d819bb22022b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 35
        },
        {
            "id": "5c225835-2039-4540-ace4-5c9a6be1cd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 39
        },
        {
            "id": "14e030a8-be85-45cd-ada3-c472fb688c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 40
        },
        {
            "id": "a6b88469-94e5-4ca1-bf49-32f4744d32e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 41
        },
        {
            "id": "e7fb02ca-2082-48cc-ae5d-11d1e51de873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "a54f24de-9931-4527-b479-ab615aac3c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "3314cea4-93ca-44e2-a262-14afbc5a62b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "d9633084-55e4-4d62-a52f-56bf33561f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 48
        },
        {
            "id": "6213bf1f-84b4-4eea-8a92-fc157ff0fd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 49
        },
        {
            "id": "6decc111-9c62-4dbe-857f-c7c782e33196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 50
        },
        {
            "id": "aba92f28-23ba-40af-95e6-a367cbfbf9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "3afed9f4-3d12-47b5-a8fd-93c3a30a5a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "1b81147d-db52-4481-affd-73088948de37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 53
        },
        {
            "id": "7fd2ce11-17b6-40c3-8382-6fc987b35b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 54
        },
        {
            "id": "8d22915d-afa6-45cb-a31e-2e2870a8394c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "0e7ca416-6a0c-44cd-b05d-cdbc5ae2ba11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 56
        },
        {
            "id": "2f34abdc-c3f9-4a78-9f4a-38d64b5e6bef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 57
        },
        {
            "id": "bbf049b2-027a-438b-9e3d-d802acff48da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "0aedfdb9-8c1d-49f5-b927-fd612c49a34c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "bd1df0e2-e524-4fb8-b08c-d89b02613bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "59f53fce-6034-4972-a941-98ef687db2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 64
        },
        {
            "id": "3eb4bea9-20f6-41c2-b0ad-f80d41388623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 92
        },
        {
            "id": "c119fef2-8a09-492e-85b6-fadf5dc56e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "d77d6168-6c2b-457c-baac-a07802ac148c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "75b19805-cb28-424a-9ddd-ac9431c743eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 98
        },
        {
            "id": "785cc732-b184-405f-a023-1353cf06c8d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 99
        },
        {
            "id": "6f632fc9-347e-47e7-abd4-533e25701f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 100
        },
        {
            "id": "f78933bf-aad7-4dd9-bd22-f42da00584b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 101
        },
        {
            "id": "fc5ab471-e6bf-4860-818f-d613fd26c967",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 102
        },
        {
            "id": "68252097-dd7d-4395-81f6-1a80bb99c5a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "bf14f5af-51b3-4240-aeba-0f8be9ffcc77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 104
        },
        {
            "id": "b658f394-87d6-4108-a84f-7a95e1d6a204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 105
        },
        {
            "id": "a043bec4-17f3-4d65-9c7d-702e92c50615",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 106
        },
        {
            "id": "34b7d76e-297f-4fff-8f74-f308e32c3418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 107
        },
        {
            "id": "eb868c31-244b-4263-88ae-d8f30947e324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 108
        },
        {
            "id": "48eee431-37b2-489b-b45f-eec9664def89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 109
        },
        {
            "id": "fa58177d-01ef-4ed2-a7de-4f0abd5cbb0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 110
        },
        {
            "id": "e0ffde3a-d9f3-47aa-b631-ed7f32918901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 111
        },
        {
            "id": "1a34047c-7f24-4455-a6cf-cd2997194889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 112
        },
        {
            "id": "2d17b349-b801-4a72-9a68-59cc2b3aad44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 113
        },
        {
            "id": "cc917b9f-01f8-4e79-91a2-83b9d97d4dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 114
        },
        {
            "id": "cb8c1f32-bae7-4267-9402-b29e2521864d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 115
        },
        {
            "id": "824a9f82-cbdd-4916-9f93-c07ed272422c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 116
        },
        {
            "id": "2993fa43-3adc-491d-a51e-d544fd91c30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 117
        },
        {
            "id": "232b7b42-8581-446a-ab1e-eb88cbdcb606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 118
        },
        {
            "id": "07526aee-c2af-4db1-958e-ad139b071955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "b618275e-d60c-4e79-b68e-f99eb3f8835d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 120
        },
        {
            "id": "58d6687b-e726-49e7-8d9d-b84b9ab1ee34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 121
        },
        {
            "id": "24d0c464-f4a9-47ed-b498-08fcb2b331fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 122
        },
        {
            "id": "d97fe4b7-ae9b-4599-9355-1d5901c3a833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 894
        },
        {
            "id": "8601b38e-196b-4ed6-85b9-04f21e20678d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 33
        },
        {
            "id": "9e89a741-de94-4ed9-a921-6f8210a6cc89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 34
        },
        {
            "id": "2b6ba734-c747-44c8-a122-f29a26ac7bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 35
        },
        {
            "id": "0433d394-7b3d-41c9-95e6-1115e0891ddd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 39
        },
        {
            "id": "05ea2bab-c429-4e19-b537-ba9ce070c865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 40
        },
        {
            "id": "fbd5c09c-418b-4795-985c-a75a9f33bdbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "4a92409f-46ac-47e2-84f3-0d4cd394aae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 47
        },
        {
            "id": "f84aa50d-f7aa-4c03-bc22-20e4cefaa412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 49
        },
        {
            "id": "e1a53995-9759-425b-b52b-f660b98c3cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 50
        },
        {
            "id": "3fc8f7ce-7293-403e-8d74-b2098831ab50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "bc0329a3-bfee-4c1f-b793-9ad8f9965d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "fcacf855-f722-4742-9049-7ae867eaa13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 53
        },
        {
            "id": "220dd59e-ee5d-47ad-9d61-7fa6f6536f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "d402eca1-66ef-42ca-bbe0-b7f3d98c021e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 56
        },
        {
            "id": "dc6a671f-7407-4beb-ac65-8b28d8f2fe10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 57
        },
        {
            "id": "28b38b5d-52cd-41cc-8ae7-c09c7ee33d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 58
        },
        {
            "id": "e5c9e2e5-b873-4b8a-a75c-cbae5d790be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 59
        },
        {
            "id": "1ecf546c-5bf0-4fb0-91ea-ef99fe0cde9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 63
        },
        {
            "id": "97dbc6b4-b64d-4cbc-b2f4-21e0968965b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 64
        },
        {
            "id": "48e48ca5-33ee-4046-aab9-ecf4fe4d75b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "dac1659e-9f84-4382-a799-c3cc7d9834cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 93
        },
        {
            "id": "d2881b0b-9d58-4cf7-9bc2-ff50c1f6ac0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "c975430a-6434-468a-9b1a-2b5d9abcd9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 103
        },
        {
            "id": "88a8b247-d86b-4e22-bd47-c9773311fa94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 105
        },
        {
            "id": "408b26c0-6479-4218-9ecb-1a91f7881bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "017f3cad-0d41-4242-9f70-955c85519cb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 113
        },
        {
            "id": "592ab8f9-52b2-4d03-9a2d-51fd7722892e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 115
        },
        {
            "id": "914056df-cc23-4ff9-aa38-2f092b9bbb00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 118
        },
        {
            "id": "996a0dae-f553-428f-b79b-cf6e850a9d2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "56ace6c8-0c7d-4b06-82ce-dcec24467e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 120
        },
        {
            "id": "a1eeb0cb-a86a-4dfd-87ad-66661a1a8a82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 121
        },
        {
            "id": "18314788-7886-41cc-806b-2ac6002cb86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 122
        },
        {
            "id": "977a3ec3-0f6c-45e4-ac6f-af2e77f08fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 894
        },
        {
            "id": "8a4f41f6-9789-488f-a848-71360dff9308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 33
        },
        {
            "id": "529ae666-0345-45f1-b8d8-6378ac634e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 34
        },
        {
            "id": "6bb6e845-bb67-449a-adea-641944c288e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 35
        },
        {
            "id": "7127b75e-bc1d-45dc-b476-812321a9269d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 39
        },
        {
            "id": "e7f132e0-3783-4fd2-aa2e-938bcf50bf22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 40
        },
        {
            "id": "78ed7885-d651-4f88-9106-3f25c36b788c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 41
        },
        {
            "id": "251b91b8-6459-476f-a173-ed1fcf0792a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "c5f827bf-d932-45bd-856f-892307f23f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 47
        },
        {
            "id": "095f6caa-84f8-4af8-aa9d-d2ff83abac6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 48
        },
        {
            "id": "d45aa3bb-fb20-4219-8619-9444b3a4c290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "39a6a308-2f48-43a5-b914-f3079dabe82c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 50
        },
        {
            "id": "96ef79e1-0c4a-47ae-baaf-0e10cba4da7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 51
        },
        {
            "id": "21bd955b-4c63-406b-b6f5-264328099722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "b7488d1f-b961-4521-8286-b9def39730a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 53
        },
        {
            "id": "5bddcbb5-0673-4ce2-b8cb-9353337b3e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 54
        },
        {
            "id": "16477039-6e9d-427d-b7f7-31deee056398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 55
        },
        {
            "id": "f76bb7ca-a3b5-4bbd-861d-f20b8c4bd048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 56
        },
        {
            "id": "17ae323e-f59d-411d-a9da-76c041e26176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "4d334edd-980f-4ae2-8853-ceb8f311bf6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 58
        },
        {
            "id": "015a066d-ecc0-4168-99a9-34f7809a5e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 59
        },
        {
            "id": "805b3990-4ea4-44e9-aa43-420017307634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 63
        },
        {
            "id": "ee1d4cf1-20dc-4fa0-b5cb-62b55ca6f0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 64
        },
        {
            "id": "8fa06c18-91a8-4869-b691-1f483b4ab9c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 92
        },
        {
            "id": "968cb429-c6f5-46c4-9648-ffcf7e9a023f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 93
        },
        {
            "id": "43bdaadb-bce4-4f07-838e-7e70d3dffcfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 97
        },
        {
            "id": "62598fe0-f567-437d-936e-7d9870b5a90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 98
        },
        {
            "id": "e2cd901b-c845-42ef-ad7e-db61cce92ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "3180034e-7a39-483a-a78f-fcf0b9f87ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "cca48d59-a05c-4c1e-90d8-4fd906ee4f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "9781f333-d7fa-47be-8faa-990ae6f8f982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 102
        },
        {
            "id": "7515c545-71c2-4f26-af20-56f5858dc0b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "2a7e86f4-0900-4251-9c56-6444106fc069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 104
        },
        {
            "id": "a5de7fca-6318-43f7-98c1-7858f9bf8e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 105
        },
        {
            "id": "1514e0ca-30f9-4227-a32f-358c94bb1401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "1234f6ee-740f-4ee7-be96-bff4dffb76cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 107
        },
        {
            "id": "527ed618-f860-44ee-8492-45b9d9d6a246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 108
        },
        {
            "id": "68f50f4f-9a57-48dd-b4db-14dc9a5b5705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 109
        },
        {
            "id": "4156b38c-273a-4013-a702-2411f0e24441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 110
        },
        {
            "id": "2ee9a713-9c86-4cfe-a40a-18e2ef4620e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "30e1f986-6023-4469-a694-593269cc363a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 112
        },
        {
            "id": "fbe083be-21a4-4400-afa5-3f9bea2880ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "1f85aa58-d40c-4f1a-9329-5bf153626754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 114
        },
        {
            "id": "d1226bb3-18fe-4030-8b41-75fe026fc0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 115
        },
        {
            "id": "b0a9c705-1c75-4cbe-a9a1-3e133d8432ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 116
        },
        {
            "id": "887dfbbb-d8df-4022-bda8-c34daf074ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "2d6e6e34-1dc6-4648-8d68-b0eda698a5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 118
        },
        {
            "id": "458e7a64-ab48-4b59-b0c7-c504f4f3ce0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 119
        },
        {
            "id": "b9f6ed58-726d-4e46-b5df-24f0e6e06e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 120
        },
        {
            "id": "412b3787-da1c-4cd9-9e9f-5d5d15463af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "c5a0554c-a309-4312-b672-1115e8cdde59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 122
        },
        {
            "id": "37a2c221-b708-45cc-8a0d-3da0be24a9fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "e806d4dc-b783-43f8-bf23-8946cf0aa201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 894
        },
        {
            "id": "f86ff44b-ed36-4293-b4fa-b824cc67d502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 8208
        },
        {
            "id": "06187083-be4b-4a05-ad0f-57579d32785b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 33
        },
        {
            "id": "bf6fbc91-cf08-468d-b503-c4429269f275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 34
        },
        {
            "id": "9b5b6f53-c5e1-4639-bdf5-2f98c03dd1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 35
        },
        {
            "id": "761b005e-247b-4a32-812f-130bfb958f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 39
        },
        {
            "id": "152481e3-6e16-466d-87a7-b7eddec5158f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 40
        },
        {
            "id": "c3ef2697-4b70-4c6d-893a-fc62bb23c3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 41
        },
        {
            "id": "98167b11-c726-4c9b-8dca-e2e97c58813d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "cd97ae8e-5457-4adc-983e-29c790fc5c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 47
        },
        {
            "id": "8914f764-f090-4be6-b73a-8cb6aab287b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 49
        },
        {
            "id": "e5284675-3216-4e24-bfba-09909c62aa32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 50
        },
        {
            "id": "0e207275-308b-4bfc-9f02-64801b990a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 51
        },
        {
            "id": "013c0c91-c631-4f9d-a2b5-2f6d0becb799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "54ff99d9-b3a0-4f41-8702-c337a6ad48ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 53
        },
        {
            "id": "c1799014-3145-4b25-b344-8388a8cbcf7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 55
        },
        {
            "id": "44f0dcb2-34b0-478d-bc4a-64e4c084331c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 57
        },
        {
            "id": "f9bf2566-ce13-44c0-b1eb-639cc78a0244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 58
        },
        {
            "id": "fe9ac55d-9b7c-4dd8-a948-1feb1d94d87a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 59
        },
        {
            "id": "d06413d9-edb0-4fac-a9fc-ecb1a6eb89fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "53339db6-f392-4da6-94c7-ce1347116d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 92
        },
        {
            "id": "9c15e3fc-f3fa-41a1-83c7-51a439aafe6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 93
        },
        {
            "id": "6eeebf15-7641-499e-9965-f7fc2fae4624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 103
        },
        {
            "id": "9c8b1d8d-47b1-4e0e-b1cf-717ea83b585d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 105
        },
        {
            "id": "1edeab05-5a5c-4c5b-b6a7-b1b576270b31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "a573df95-2d27-4c29-b4e5-101ef80a5b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "474cc86c-8cd1-4f20-947d-8d967858a5a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 115
        },
        {
            "id": "f4289cd0-6ed1-40a5-87a3-7604e98c5a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 118
        },
        {
            "id": "331c6232-9f8b-4681-b9c4-b5e8b1b6460d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 119
        },
        {
            "id": "8f407a76-4a14-40f9-8e66-3202cb3f819a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 120
        },
        {
            "id": "61de5a1b-29c0-4bf2-8904-0c39792c0d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 121
        },
        {
            "id": "5f390332-4081-42dd-82f3-10c4bb65014a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 122
        },
        {
            "id": "c2136527-8e57-48ea-9aed-731f38a79fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 894
        },
        {
            "id": "0874ad7a-9aef-400f-9d7a-009ef797c5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 33
        },
        {
            "id": "906c72d3-ab41-4140-8057-8b8a8f967143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 34
        },
        {
            "id": "ee712201-66cb-49da-856d-0f61fb3ade47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 35
        },
        {
            "id": "f3697b78-0c47-411d-9d6d-ce94a0cccaed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 39
        },
        {
            "id": "227d6d25-459d-4616-8f89-cfab6947ed97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 40
        },
        {
            "id": "417ac2df-3c6d-4366-974e-b76410d0a871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 41
        },
        {
            "id": "b8b6ea62-1ab1-4a5d-9fb9-358ddc17bac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "45ae426a-3f21-469c-afe6-6e93b6d26e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 47
        },
        {
            "id": "6ac63bef-26e4-4706-8f57-a770d65d7850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 48
        },
        {
            "id": "31f3fa54-025d-40c2-a35e-939040471795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 49
        },
        {
            "id": "e225c09b-728c-4dcc-8a54-2075e7c5a7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 50
        },
        {
            "id": "45efc237-7243-45b8-8347-9291f3dfdbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 51
        },
        {
            "id": "ba771722-5d78-431e-9ee1-45b5fe7209fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "bf4e88b2-b476-4de3-81bf-99f0a7911400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 53
        },
        {
            "id": "57e4e4f3-0c5d-4224-b1a4-955271b39337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 54
        },
        {
            "id": "4bf97a76-f108-4539-9273-87158746dda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 55
        },
        {
            "id": "ff48f5a2-75d8-482c-b6da-2f3b98257c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 56
        },
        {
            "id": "d1b626f4-3707-4cb7-9096-4df278c19214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 57
        },
        {
            "id": "b386fecf-2b4f-4c69-a137-b66b389f4309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 58
        },
        {
            "id": "84d2b5cb-e2c0-45ef-909f-47f164553307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 59
        },
        {
            "id": "ef722e95-a844-4eeb-95aa-2cb58a8158ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 63
        },
        {
            "id": "16d0e99c-5fc6-4ec6-8828-0930f55b4c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 64
        },
        {
            "id": "3a02c53c-59cd-4414-a0de-8c698c470cfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 92
        },
        {
            "id": "f54accf6-e235-4121-bde0-3eaa68ec3598",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "ccf1111b-a554-4663-ab3d-dff618a49904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 97
        },
        {
            "id": "fdcd7129-8962-4306-8919-bc29b9caa310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 98
        },
        {
            "id": "8b0db81c-50de-4770-9151-ed8611ed6c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 99
        },
        {
            "id": "9f25d1fa-60b3-47c0-ad22-d3b20c5bd700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 100
        },
        {
            "id": "9b162517-8ae0-457f-977c-28d1fe7a2f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 101
        },
        {
            "id": "cb31d1ba-a9f1-4ca8-b072-93d674ad743e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 102
        },
        {
            "id": "5aa3185f-88c5-4e47-b3db-dafa71910432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 103
        },
        {
            "id": "519f2b5c-7dd3-4647-bc51-bb1220216672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 104
        },
        {
            "id": "d303b5bf-d93b-447d-9056-43c5bcf0c14e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 105
        },
        {
            "id": "bc894d7c-484c-42e5-9649-240a5e2a4a31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "8958b9c8-d592-407e-a359-5c8561bf6550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 107
        },
        {
            "id": "3d9f9978-2ffc-409b-8392-b8d6180a3e30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 108
        },
        {
            "id": "54fb0ce1-134c-4d5a-b47d-8883c7c7556e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 109
        },
        {
            "id": "1b31c8f1-051b-4a3e-8bba-8e58539162d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 110
        },
        {
            "id": "6ea11538-a211-4236-ae99-6b1fbf38615c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 111
        },
        {
            "id": "0d7173c3-04e8-4a85-b2a4-3ac7a3311eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 112
        },
        {
            "id": "d3011b62-7fdb-461a-84b3-1e428b2053e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 113
        },
        {
            "id": "d8a40669-1f07-41c3-b344-94f348d4c989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 114
        },
        {
            "id": "c87618d3-d20b-490c-ab20-daaf7c70b351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 115
        },
        {
            "id": "dad25ed3-637f-46b7-8140-9482d3c3dda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 116
        },
        {
            "id": "2ad0fc89-7e72-4a34-ae3b-2ec0e48559d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 117
        },
        {
            "id": "3917dc23-b780-43b6-9aba-cde39d7b1127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 118
        },
        {
            "id": "2aa7287c-0c41-48aa-8218-28acd8000fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 119
        },
        {
            "id": "30678f0b-609f-4484-9e13-394cba01f1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 120
        },
        {
            "id": "c7da12ac-be50-4fbf-b382-cd72a1f776ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "e478cf21-b063-4ccc-b98c-8f788892859b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 122
        },
        {
            "id": "6c3f4272-6cb2-48d7-911e-0bfe45c0bfbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 173
        },
        {
            "id": "04725f1c-1555-473a-a8bd-612767d214cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 894
        },
        {
            "id": "4a056a06-60e1-475f-9cfc-65702b72608e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8208
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}