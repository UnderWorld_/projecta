{
    "id": "b89c224f-cdac-472b-9e04-d5c1cc17c675",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font4CS",
    "AntiAlias": 1,
    "TTFName": "fonts\\font4CS\\BreatheFireIi-2z9W.ttf",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bulky Pixels",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ccb4b18d-499a-4650-9d85-38216e1819ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "39c79359-0514-4d96-b371-6c2b43282dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 62,
                "y": 62
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5780ebec-5905-4e49-9873-d92405fa7a8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 51,
                "y": 62
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ad176525-ad15-41cf-9bdc-51e7cbdbe175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 18,
                "offset": 0,
                "shift": 19,
                "w": 14,
                "x": 35,
                "y": 62
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a269109e-6927-4e9e-8349-67f7a505e20b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 23,
                "y": 62
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ae471748-59d2-49ba-b206-a22fa4613bd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 18,
                "offset": 0,
                "shift": 23,
                "w": 19,
                "x": 2,
                "y": 62
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8015b5cf-6bfe-43a9-90e6-907bc8e5ea8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 18,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 234,
                "y": 42
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "12de4a3a-2603-4324-96ec-b65df841edc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 228,
                "y": 42
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "2b993439-44f5-43c5-9b99-b3acc10a7836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 219,
                "y": 42
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "24423bed-f3df-4b41-8738-d80d7ea6dba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 210,
                "y": 42
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6ee9dae1-9fc4-4709-9a95-28e28751f526",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 18,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 68,
                "y": 62
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "01979cb4-562a-4adb-8a5b-3c7d287f9b74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 197,
                "y": 42
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ce275c00-9090-4023-8bfb-29b80ff35694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 180,
                "y": 42
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "8922a5d4-4d3a-42f2-bfb3-b44ea97aeff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 170,
                "y": 42
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b95a9eb6-40a1-43de-bd7d-ddef44e9c6ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 164,
                "y": 42
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "57e91ead-64f2-4511-bfd3-2da5090c41f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 155,
                "y": 42
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "799a8e21-1424-40a1-8ed3-ff6d57247335",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 142,
                "y": 42
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "18013d3f-6cd8-4402-b909-ca0f6e9efcdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 134,
                "y": 42
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8532f1ab-a9e4-4413-b389-0601f4240ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 123,
                "y": 42
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "89a29652-be55-487d-91f3-82bd97e361b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 112,
                "y": 42
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0cc0367d-64ee-426a-a124-acfe327537b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 101,
                "y": 42
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "80b7048f-df04-43ef-bf51-c9a315cc39fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 186,
                "y": 42
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "ccebadf7-2008-40ca-bc5c-119e33c446a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 84,
                "y": 62
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "9b6f49a5-2b0f-4897-8a92-f1fe36ae6906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 95,
                "y": 62
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8b2ac057-c1c7-4e80-8564-e8caf5913ffc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 106,
                "y": 62
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5d4af65e-c27b-4358-b258-3fa5512fa0e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 103,
                "y": 82
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b7c51384-cd42-4c73-93e1-6a23f4f0704f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 97,
                "y": 82
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5e69db4f-1a6c-4d9f-aa9f-75d22ab5e04c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 91,
                "y": 82
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "58e01989-6303-41da-8c29-55da7e5159ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 79,
                "y": 82
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "5eba5b8a-1219-477f-92c9-a58af4292407",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 67,
                "y": 82
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "4b5a47ac-1b48-4a9e-a31c-26b9002a6faa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 55,
                "y": 82
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3557482e-291f-4e12-8c9a-ff1066f223af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 46,
                "y": 82
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "68ea28f6-7835-4336-ad4a-101750ad5944",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 18,
                "offset": 0,
                "shift": 19,
                "w": 14,
                "x": 30,
                "y": 82
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "3d47b152-ab4c-4aa6-aa87-d7b8fa3cf2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 17,
                "y": 82
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c877c66c-ce3b-4ae3-9cf0-528b7d710704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 18,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 2,
                "y": 82
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b47f4bf5-e288-459f-8897-e33e372c5981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 239,
                "y": 62
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "bd5eb1c6-844d-4eef-b7d9-1ba1abd203bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 226,
                "y": 62
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6a449b6d-e304-4077-ae63-187df857e6c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 215,
                "y": 62
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "7a6e237c-c5db-41c3-9604-3ebac788d264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 204,
                "y": 62
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "06c6ba5b-dc60-425c-a832-aa563bfe4a62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 191,
                "y": 62
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f7ca6db5-53d6-4c10-ac45-7af30c07bc9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 178,
                "y": 62
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ab1d74c6-3117-4b8f-a92c-ab8ae3665b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 172,
                "y": 62
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6abb0b2c-a744-40e4-ad06-8a2083c07f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 161,
                "y": 62
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "34a03b73-e2ed-4efd-9bed-353e7f6ba2c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 148,
                "y": 62
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3a28b051-613d-45a6-bb12-769d034bcf07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 139,
                "y": 62
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d3d1bb9f-8f04-4f26-ae84-cae96b7a6474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 119,
                "y": 62
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "567229d8-9f00-4665-8a43-bf997d17d263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 88,
                "y": 42
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "72cb31ca-03d7-427c-bb54-145644e6dabd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 75,
                "y": 42
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8c22c236-3902-4315-8bed-359c607386dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 62,
                "y": 42
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "627d52ec-ad22-4586-be66-bf7a7399d91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 18,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 36,
                "y": 22
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "4ddb9627-37c9-49e2-88fb-9876fbe8603c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 15,
                "y": 22
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2600fe5e-2b14-48fa-9433-cd01bbc5cc3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 2,
                "y": 22
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "0c405319-bc3f-4cab-85fc-26f9cddac721",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7117a75e-5c98-43df-b1e2-7006689d392d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 18,
                "offset": 0,
                "shift": 17,
                "w": 13,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0e200c62-0fbb-4c65-9afd-e9c7ef9af02b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "7d99e023-6970-4c18-becf-de288efd9d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "74e2cb9a-a32f-4b76-9104-24430db07f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 176,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "57acc209-a334-417b-8a5b-b1356dc362fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "544a7868-18ae-4646-bb44-0f11ef8bb84f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "78b5e58b-902d-4066-969c-899b98969187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 28,
                "y": 22
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "ae0e3bd5-ac72-4384-94a7-fd43f2600b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 18,
                "offset": 0,
                "shift": 12,
                "w": 7,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f16184b5-c67c-4ec5-ada0-c83a916fc76d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6e772ae3-dd05-46d9-b39f-0dd4d6e5e019",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "82893abe-d105-4db1-b5c4-563d8491e7c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a3687693-2b3f-4932-a23b-ff97fbda0cee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 78,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "851f61f0-363d-47e0-a7b3-993bb7011fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "dfcc424c-bb1b-4a62-933f-f43aa61020f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 52,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b57009b0-2833-40be-8299-aa8a9a578dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "b0a2197e-c478-49a6-9d89-5c09ccbbaa46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 28,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "b3c653c4-6d5a-4947-95a9-2cd1527b8a50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "683efd3d-8ec7-433c-9ab4-1c5d4e79381e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 18,
                "offset": 0,
                "shift": 11,
                "w": 7,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "cbab352a-c503-4c18-a66d-51f5bb7198da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 51,
                "y": 22
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "05a26efe-e935-4d48-91d0-21e01862cb07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 168,
                "y": 22
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "afcf00a4-164f-47f3-87a9-4dbe7c9cf2bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 18,
                "offset": 0,
                "shift": 8,
                "w": 4,
                "x": 64,
                "y": 22
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "f2f4f2ff-89d3-4acb-b471-27cee47487a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 43,
                "y": 42
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "e874f257-e712-474b-b6d5-402ea660804c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 30,
                "y": 42
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "654ec394-abd3-4943-b6e9-79e5b57241ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 18,
                "offset": 0,
                "shift": 10,
                "w": 6,
                "x": 22,
                "y": 42
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "71fe946a-b999-483d-ad42-5efba89b1264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 2,
                "y": 42
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "4b51859d-707f-409b-a481-c9c2562081b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 242,
                "y": 22
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "67616d3d-8065-40bd-8999-023dc9a373a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 229,
                "y": 22
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "61ff3f9b-8c0b-4758-91ee-9bb62aae4d30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 216,
                "y": 22
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e4d28c25-b514-407e-a14a-9eab481682a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 203,
                "y": 22
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "07664123-770d-41a7-bea2-4eb3064e8939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 192,
                "y": 22
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "52c679b4-033e-43ed-9ba7-4c7d49380675",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 51,
                "y": 42
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "249b64d4-71da-4725-ac8d-b4fda30741ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 18,
                "offset": 0,
                "shift": 13,
                "w": 9,
                "x": 181,
                "y": 22
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "3c113f7f-0ac7-4c69-b452-a15b43adac54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 157,
                "y": 22
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "74bd7edb-03b2-45c2-bdee-93a43493fbc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 146,
                "y": 22
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8ddb7315-0299-463e-bb92-09f040568f3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 126,
                "y": 22
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "24b1a79c-635d-444b-a959-bdbf86ffa3cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 113,
                "y": 22
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4b85645e-acbf-435c-9471-8da1b1485760",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 18,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 100,
                "y": 22
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7dfb4358-c256-45f5-822a-e3b862fe52cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 18,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 89,
                "y": 22
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e3ebe228-43a1-4e24-bf67-73cb81e66591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 18,
                "offset": 0,
                "shift": 3,
                "w": 4,
                "x": 83,
                "y": 22
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "30c20845-11e1-4541-90ab-0ce99c5de20d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": -1,
                "shift": 3,
                "w": 5,
                "x": 76,
                "y": 22
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "22da1375-bea7-4e0a-b648-462a50d0ebb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 18,
                "offset": -1,
                "shift": 3,
                "w": 4,
                "x": 70,
                "y": 22
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a6a13561-1b3a-49ed-8d45-bcc379c166b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 18,
                "offset": 0,
                "shift": 22,
                "w": 18,
                "x": 114,
                "y": 82
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5bdfb57d-464c-4cab-82d9-fc3c4516666f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 18,
                "offset": 3,
                "shift": 17,
                "w": 11,
                "x": 134,
                "y": 82
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "3ff970a5-f269-4957-b06e-3b039eab8e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 35
        },
        {
            "id": "d063e865-15e1-4eac-8c08-dc47f62ea1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 40
        },
        {
            "id": "6ab3f5f6-19b5-4e80-9a79-69c3f8b76521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 41
        },
        {
            "id": "4d3708d9-aef8-4829-867b-9a0a0d861e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 45
        },
        {
            "id": "54055496-d9a4-4725-bb67-87cfb839a3c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 47
        },
        {
            "id": "095b442a-0f4d-4457-a974-7aad2c99afd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 49
        },
        {
            "id": "6b80cc6f-2286-4942-9290-56f55b3cb830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 50
        },
        {
            "id": "683970d7-70e2-44b1-8066-23ecaec60a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 51
        },
        {
            "id": "401e95c6-4cfe-4e63-814b-a4d7ed47e8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "4901ae0a-2c92-4829-bc33-8f16cf0dac1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 53
        },
        {
            "id": "724a9bac-a105-4b67-840a-7abc6600b096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 55
        },
        {
            "id": "034bec88-8d3e-419a-aaf9-92af2ef499fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 56
        },
        {
            "id": "c1b328db-86ad-4481-9729-52038e4f9e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 57
        },
        {
            "id": "c59fbd8a-3c08-4102-b312-18f9ba9626a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 58
        },
        {
            "id": "c866a717-6f36-4993-83d1-cdda23048b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 59
        },
        {
            "id": "ea381701-7b33-48e9-a8cf-a00be1edf049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 63
        },
        {
            "id": "9c3700c6-016a-4071-afcd-9912a05e0950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 74
        },
        {
            "id": "011b7d3f-26b8-4e11-b4d6-4de644471248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 83
        },
        {
            "id": "92150918-1693-453f-b06b-fc63a687b0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "fe313aa8-e8c5-482c-bc33-54a0f1820a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 86
        },
        {
            "id": "4d5687c8-0ad0-4dfe-8992-aed80709c678",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 87
        },
        {
            "id": "77e2bb27-30d3-461e-9d28-fb095a3ed5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 88
        },
        {
            "id": "4c156892-c159-4731-a3ae-43d80da438f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 89
        },
        {
            "id": "fa840717-14e1-480d-87a5-c69220ffff98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 90
        },
        {
            "id": "0c3b104b-6bae-4ce5-bc73-3758a1083d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "7bd3ae1d-cdf1-4dec-adf3-f9a3ed3cdaf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 93
        },
        {
            "id": "606a1cf2-1dfb-4150-aa58-a2ca3a2e4eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 100
        },
        {
            "id": "5b299763-0b9d-45c7-9083-2a29c178aacb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 103
        },
        {
            "id": "f19ff0b2-7629-4fa4-a689-c296aa537f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 106
        },
        {
            "id": "fbc93f39-51eb-474d-93c5-46ac761bbecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 113
        },
        {
            "id": "12ab5af6-5ac8-4c0c-83ae-73643401a134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 115
        },
        {
            "id": "a7cc41f1-b3d2-466b-8510-03bb1ca8c36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 118
        },
        {
            "id": "fcf2c094-fcd6-4f72-90de-5fec516b423c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 119
        },
        {
            "id": "704eb669-88db-4d50-98cb-715cbb49a972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 120
        },
        {
            "id": "5137a7ff-ffee-4b78-9d41-57c6dab2268a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 121
        },
        {
            "id": "c8f6ec50-a241-4d10-a88b-3748437f709a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 122
        },
        {
            "id": "36cd16fb-9dc0-4952-a709-2cbc9e07f46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 173
        },
        {
            "id": "1f9b0fad-e330-4815-9f75-43c46756f045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 894
        },
        {
            "id": "48518e20-4a2d-4a13-a8cd-bd1627c312c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8208
        },
        {
            "id": "a209e1bf-164a-4cd8-b555-743d0f43a36e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 40
        },
        {
            "id": "49e3149a-1aea-4a39-9792-9afd091d66b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 44
        },
        {
            "id": "8e406488-b819-4b37-b4df-d944020b8cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 45
        },
        {
            "id": "40610e85-63f2-4ca9-86c3-6231b0372d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 34,
            "second": 46
        },
        {
            "id": "2da3a62c-878f-49d7-bde5-2fe23fe4195e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 47
        },
        {
            "id": "5ed3f4e1-2318-4f9a-8092-b8a328c29f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "a053d44f-d891-4050-9c25-a7a95a2bb800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 51
        },
        {
            "id": "f6e0b35d-6780-4d66-a7c7-ed480c802f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 55
        },
        {
            "id": "e2a85aef-173a-484d-bd77-b23078a60793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "cbea236d-ec5a-4f37-bafe-8f8c06cc3e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 74
        },
        {
            "id": "a246351e-33cd-4725-a8ed-5e27b58eed9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 84
        },
        {
            "id": "68edbe96-02c3-4f20-a0d1-65fb97a51975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 90
        },
        {
            "id": "a9c38651-f181-4c5a-b56a-f769a8cc4722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "6f2eed2b-7d13-4911-8b6e-9ebdc546bb3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "624c39e0-682b-4278-beef-85f565598a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 106
        },
        {
            "id": "5a86f2f5-e38a-40e8-94e9-7bf2b2c00a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "a7cf0244-c985-4a48-a5b7-d70aa210b414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 173
        },
        {
            "id": "895143dd-8a3c-4c61-90bb-aba29d00a5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 34,
            "second": 8208
        },
        {
            "id": "2d7da83d-0179-4dae-8400-0b8ea7bc1101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 35
        },
        {
            "id": "3d33e839-6ce9-4697-90ca-4e250b1d4b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 40
        },
        {
            "id": "67446ca6-7178-4cbc-a5e0-93e03f36dfe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 41
        },
        {
            "id": "5f3f1f7b-9fab-4f80-91b4-f2200e6a3f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 47
        },
        {
            "id": "280e2f92-5fb4-4955-b054-17387124d399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 49
        },
        {
            "id": "3788b68c-dbd1-49c1-af54-280eefecfb93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 50
        },
        {
            "id": "60fc3880-b92e-47cc-a0b5-c1d70860da4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 51
        },
        {
            "id": "6e043c71-6988-41bf-965f-0d800cc185df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 52
        },
        {
            "id": "cd8d3636-fc28-4a40-ad50-55a8f786630a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 53
        },
        {
            "id": "2c1946eb-15cf-48ee-a581-91601f2a4fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 55
        },
        {
            "id": "e7dcc869-c14b-4b96-8eaa-c1fdac3ddd35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 56
        },
        {
            "id": "3fbc704b-4bd1-4300-ab84-471e0c274710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 57
        },
        {
            "id": "bc3cc876-6845-4009-82ea-c8bbe98ea840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 58
        },
        {
            "id": "f85de378-20dc-4588-8e0e-b2980c969568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 59
        },
        {
            "id": "8f636cb3-4dbb-4055-b8f6-a08463f83dc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 63
        },
        {
            "id": "8f75e8a8-ba5b-4e91-a80e-86003b30e445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 74
        },
        {
            "id": "8dd932c7-960e-4185-97ef-77b685b38260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 83
        },
        {
            "id": "d892231c-efa8-492a-ac78-8307338ccdf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 84
        },
        {
            "id": "8a77c923-f3f9-4723-8c61-21a1d5862e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 86
        },
        {
            "id": "208e599a-d020-40b2-822b-47c646d1df77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 87
        },
        {
            "id": "66ee60ee-1dee-416c-a561-b671708c8aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 88
        },
        {
            "id": "f5fd4119-0875-48ed-a2e3-816fef141ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 89
        },
        {
            "id": "8223b6ef-f10a-49ee-9af5-d129d93353e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 90
        },
        {
            "id": "523f6cf0-e023-4a09-a735-55c57c278d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 92
        },
        {
            "id": "6cefe73a-f67d-4478-977d-4cf67846a047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 93
        },
        {
            "id": "c2177282-cb3f-44bb-b4af-ee91010f45d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 100
        },
        {
            "id": "1fe0048e-1728-4534-9ccc-00ec23e335f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 103
        },
        {
            "id": "1680cf8e-776e-4b36-b2eb-64413f690cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 35,
            "second": 106
        },
        {
            "id": "e6365aff-f8e0-4101-ae44-fed3ab3618f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 113
        },
        {
            "id": "3d12a74d-0233-483f-bfde-0c0e3a2e1616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 118
        },
        {
            "id": "6b0a925a-c061-4463-b31d-6b948fcddcb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 119
        },
        {
            "id": "ca837487-df08-4395-90bb-df17c3a3508c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 120
        },
        {
            "id": "159d753f-b8c2-48dc-a7cf-361c5a8f3001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 121
        },
        {
            "id": "4aa3acfc-986d-4082-a49e-5a610e31b4a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 122
        },
        {
            "id": "63286809-8f56-41be-9452-867725d8bc3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 894
        },
        {
            "id": "7532e905-c251-4f42-8ea9-1bdc8c2ed6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 40
        },
        {
            "id": "1fd2bca7-e8c3-4f1c-a7b2-95e7bcb591d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 44
        },
        {
            "id": "4d2f439b-a1e4-4513-8c6c-148a86cd66fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 45
        },
        {
            "id": "6c8fa29b-73bb-4e9b-b10b-1d4949e33a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 46
        },
        {
            "id": "b8e22079-53d4-4614-b676-0cc6fff3216a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 47
        },
        {
            "id": "3c187ea9-47ae-4650-95e8-8c1a23708fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "9df12a9d-de0e-45fc-83e3-9c5f46d3a6ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 51
        },
        {
            "id": "dd2985e7-f657-4545-aca6-e6b1d3a20535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 55
        },
        {
            "id": "437d79ef-7ecb-44f9-a108-eb4a8c00731c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "ae2116ca-73c9-48ad-8cba-ad85c0a34163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 74
        },
        {
            "id": "e0dbd497-7388-4c9f-8f44-59d2b69dce55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 84
        },
        {
            "id": "afb574a9-6122-43f4-a861-1f95c889bbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 90
        },
        {
            "id": "1438fbdd-ca53-41e6-9b91-36609c8c2808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "5bd9c176-49ce-418f-84ac-8f7e83741dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "dddd52b2-d04d-4e22-99ba-78f5c2a6f767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 106
        },
        {
            "id": "615cfce7-b47b-4347-9c89-845ccbe18584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "9c4c3c5b-844e-4a0a-8623-0ccf7e9490e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 173
        },
        {
            "id": "3c944160-7822-417d-b16a-5d419e2323d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 39,
            "second": 8208
        },
        {
            "id": "485dd5e9-6c36-4626-a3e3-eb9280b5b91c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 35
        },
        {
            "id": "018543cb-1459-482b-b688-92c1cf42103b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 40
        },
        {
            "id": "2981bb1f-d475-4009-b702-4f9d1029eb4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 41
        },
        {
            "id": "662842c5-970e-4062-8c83-54d275f9826b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 45
        },
        {
            "id": "35a1363f-8c89-4e83-8190-f6db479a71c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 47
        },
        {
            "id": "21e2f7d6-f315-4b8d-ae2f-8b039b86ee75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 49
        },
        {
            "id": "cb384fdf-9823-4863-aa44-93964e5bb04e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 50
        },
        {
            "id": "7deb1785-891e-4545-b542-36fdef04fb40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 51
        },
        {
            "id": "69292556-581c-49b2-a57f-da072a58d4a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 52
        },
        {
            "id": "48d82fe9-8e10-4939-ae55-2605b7e23bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 55
        },
        {
            "id": "1ff81633-c15e-4ef6-995d-16f5bbe61021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 57
        },
        {
            "id": "ee7e2323-cd27-4f40-b719-d9470c88ebe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 58
        },
        {
            "id": "ccf8aa30-0c18-4374-bd5f-cf5130b6423a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 59
        },
        {
            "id": "3c2ea9b7-df5b-4b41-9742-cd2cf3ba9ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 63
        },
        {
            "id": "7dba616a-1482-436c-b794-5c994c6a0c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 74
        },
        {
            "id": "962a60ba-483f-4ce2-8968-7554fd833802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 83
        },
        {
            "id": "99b0879e-58e6-4ba2-b830-2f9b5b614d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 84
        },
        {
            "id": "86de0903-9db4-470e-a5ef-79ec5a7be60d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 87
        },
        {
            "id": "10c668be-bf16-49f6-8b73-8bba92f8204b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 88
        },
        {
            "id": "081d89f2-938a-4df0-8312-2742f622daac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 89
        },
        {
            "id": "4a9be257-3b3c-42dc-982c-53f4aaca8956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 90
        },
        {
            "id": "cbc67bc5-9d3f-4bbf-9dcf-29ba735f701f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 92
        },
        {
            "id": "1c3c6144-9f4b-440a-bd03-5bc6f2e9d6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 93
        },
        {
            "id": "47de9e8f-6426-4706-a74f-1d67c172de53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "a1864a38-7f2a-4e9a-a1c9-7361a86f9cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "6a175c42-d07c-42c3-8ea6-af71690165f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 106
        },
        {
            "id": "c5368a37-cf62-4a3e-b213-9b3583da7b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 113
        },
        {
            "id": "55a42be8-5867-4e8b-bdaa-dee4871d4942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 115
        },
        {
            "id": "141ce344-99e6-44d1-a216-2421ddcb8bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 118
        },
        {
            "id": "29465133-327c-45fe-91db-3ea47cf46f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 119
        },
        {
            "id": "d4afba9b-2a6e-4cf1-8f04-16eaefc313a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 120
        },
        {
            "id": "f6e31663-c452-4ef1-9f1f-e8352102c396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 121
        },
        {
            "id": "32e7a830-fd98-47f5-8786-6aee7738cf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 122
        },
        {
            "id": "f5a5bf03-75a4-4608-bd91-ff5b4479a137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 173
        },
        {
            "id": "ec2edd38-e78d-4e33-96bb-3e66b4c45ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 894
        },
        {
            "id": "ebe97c03-a1bf-4211-b55a-5d14fac0f7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 8208
        },
        {
            "id": "70faa4f4-663c-4b83-9ad8-d99ff38dc2a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 33
        },
        {
            "id": "deeb4b4e-ab8b-49af-b11c-203274ef1b1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 34
        },
        {
            "id": "4fdb5111-7f0b-4675-a3c3-8928860b8166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 35
        },
        {
            "id": "8a9ba56a-7195-4d82-8596-f1105792ad78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 39
        },
        {
            "id": "dba048cc-8474-44df-986c-1c8e43aaf2ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 40
        },
        {
            "id": "c3750aa6-73f2-45ed-9d41-68b832c1a613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 41
        },
        {
            "id": "7feb8fbb-0aea-4ea4-ae15-7d94ff70a442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 44
        },
        {
            "id": "42892f19-9e4a-4d80-8edb-e26c7b1fbfc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 46
        },
        {
            "id": "19d4dd52-d0c2-459b-8de4-7043dbc5b584",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 47
        },
        {
            "id": "fff928eb-87d7-456a-b549-2e02aec28d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 49
        },
        {
            "id": "f24045a4-bdab-4d3b-96cb-09c1918491f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 50
        },
        {
            "id": "723311f4-9ba4-4a4f-866b-f1c8bfab96c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 51
        },
        {
            "id": "30b20014-ac77-459f-9358-842ef68f3a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 52
        },
        {
            "id": "dcea6d16-16c0-4ad9-886b-8f0d2e7d799f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 53
        },
        {
            "id": "456f0aa4-cb8c-4c97-8cf2-0085982a27e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 55
        },
        {
            "id": "c44b5a93-d723-4b77-be58-42c39c004551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 56
        },
        {
            "id": "c3fe40c1-a54b-436f-8584-5d294d2a7648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 57
        },
        {
            "id": "d90ab1c9-8694-4fce-b512-3a9abf0b3b20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 58
        },
        {
            "id": "a094067b-b745-4ae4-80af-71962436317a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 59
        },
        {
            "id": "192e5bc9-a2b6-4533-9c21-b5819f2d4207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 63
        },
        {
            "id": "d59ba513-00bf-48a8-8d5e-94a3f8a75a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 64
        },
        {
            "id": "63bca152-63d7-4b91-92b9-7866ac3be4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 74
        },
        {
            "id": "7cdee60c-48b8-40c6-a1da-8b84000fc3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 83
        },
        {
            "id": "b5a6f4fe-fd59-4093-9eb3-a18c12234568",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 84
        },
        {
            "id": "fc24d72b-ad7d-4ba6-a47c-a1e4d87af1cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 86
        },
        {
            "id": "0058d73d-1887-4421-b258-9a65a53edbca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 87
        },
        {
            "id": "41d1aaca-b7e2-4cde-958b-94274531ab44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 88
        },
        {
            "id": "1d168054-ae17-4472-93e1-1c4dbbbca42c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 89
        },
        {
            "id": "2c6bfd8a-79d2-40ee-aecd-bab65e3f66b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 90
        },
        {
            "id": "32b6b4e1-3b6e-4ac2-972c-3c8030bab2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 92
        },
        {
            "id": "513dbbe4-4c3f-4f65-9980-38399859e68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 93
        },
        {
            "id": "b5bd6bec-1baa-4bd6-a481-831982c7f7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 100
        },
        {
            "id": "1c56205b-601d-4b1d-8e40-6d864b33f981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 103
        },
        {
            "id": "96effc7f-0a05-4ec0-8afd-eb97c264ac72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 41,
            "second": 106
        },
        {
            "id": "f0f5ad50-fda7-43cb-a58b-be857c364f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 113
        },
        {
            "id": "3afb2aaf-0ac5-48de-b2bb-e86ac3393e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 119
        },
        {
            "id": "d0b7e80c-b622-4dbe-961d-9723349bc843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 120
        },
        {
            "id": "9d192969-edd8-4154-839c-72969b2766c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 121
        },
        {
            "id": "69274363-64cf-4880-a4bf-2e012771f3b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 894
        },
        {
            "id": "5245be6a-ff5b-43b0-8492-a28961823d06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 34
        },
        {
            "id": "fb227cdc-d57c-4d75-b227-d023e5c859ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 35
        },
        {
            "id": "c339f53f-72a0-4578-919e-43b16d12669e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 39
        },
        {
            "id": "2ddd24bc-cb3b-4273-ae66-84c4fb1db166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 40
        },
        {
            "id": "6ba7bd44-5994-4ecd-8140-3a750821cf65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 44
        },
        {
            "id": "b90072e1-2409-4b64-84a0-96013eaa6d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 49
        },
        {
            "id": "1cae3f96-7b21-43e7-910f-5fd7769d366d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 52
        },
        {
            "id": "b47fdf71-bafc-4838-bfd7-3a32ab4ca393",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 55
        },
        {
            "id": "0789726c-1360-44fd-bef7-6b4c137b718a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 57
        },
        {
            "id": "eb52959f-1e77-4906-a4aa-101c223344bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 59
        },
        {
            "id": "aff333e4-b54c-4f12-8333-49124f857d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 64
        },
        {
            "id": "fd66a736-4c33-4f63-8903-2a031bacb03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 84
        },
        {
            "id": "4fee4a73-d31b-49c7-b12d-fe08203fd1f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "91525251-9f62-4498-9229-12494165bfcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "50d6b5ad-92cf-4f72-936a-2a8ecb491aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 92
        },
        {
            "id": "6b4a8c32-8673-4018-b738-5fee39bcc2c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 103
        },
        {
            "id": "2822c8f6-e59d-465f-836e-4833c01719c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 106
        },
        {
            "id": "b67f2a42-0893-43ac-b575-72367aa6058d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 113
        },
        {
            "id": "5da7a751-5e9d-415d-b4e8-285d14751279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "1339ee43-e946-4611-8738-40bea3a92060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "3a87e6c9-fee1-4adb-b2ea-749287831c32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "8ed3f86f-19ff-403d-af91-f6c9e9dc8b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 894
        },
        {
            "id": "3bf0e16f-6e99-433a-92c4-d78afc6fa4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 33
        },
        {
            "id": "1448b97c-ac3b-4cc1-a685-cf5406860ec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 45,
            "second": 34
        },
        {
            "id": "4f35cec2-03e6-471b-9acf-75e6d28e2fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 39
        },
        {
            "id": "19eb7748-1f45-4aa8-942c-4fc94411773a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 41
        },
        {
            "id": "4b993ffe-f631-4ccf-9a7d-8694fd7f47b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 49
        },
        {
            "id": "3b4b8ad4-f55f-47a0-adc1-c53bddee891d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "410c561f-fbdc-48b3-b4fb-32c0ee3c1635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 52
        },
        {
            "id": "cea1bc68-d866-4ea1-a165-0b6c303ee7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 53
        },
        {
            "id": "1ca935cb-7162-40b0-8e98-b41a50e2f65c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 55
        },
        {
            "id": "6ef36484-2044-4d6f-a564-8f28fa6e5155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 57
        },
        {
            "id": "7efb1366-8e01-44f7-8193-78a54eca2028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "d275051b-2201-44b4-b0d1-945409225c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "72ac8020-a38a-4cb8-a63c-4e59fd43a8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "d48bfad1-3456-447a-8fde-54ae609a1681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "293a6266-3dfa-44ec-93fc-65b57e77b746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 83
        },
        {
            "id": "042101bf-eb8c-4248-8579-e9177b249a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 84
        },
        {
            "id": "bbdda99e-9391-4e8a-b8be-abec81d7d020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "aa6418c3-51cb-436d-94f1-fa7b792ebd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 92
        },
        {
            "id": "4c048bb8-4df6-473f-9530-293fa355d1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 93
        },
        {
            "id": "8b1d5dde-b448-46db-bc93-c2e6d6f6a257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 106
        },
        {
            "id": "01754bd6-2205-4a4f-83f7-b57d5249fe5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 115
        },
        {
            "id": "afca80e2-2584-4705-bd05-7519e9b4ea60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "948b5aa6-cba9-4d51-bd2f-dd0eb4643b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "2501ddd8-6113-4b5b-b296-9395e5baf133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 894
        },
        {
            "id": "c8785033-9244-4498-9da4-6ccf3cf78f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 34
        },
        {
            "id": "b9eda106-a632-4cb2-9003-67c805587ed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 35
        },
        {
            "id": "c66c7c03-25a9-4ab2-aaa7-bba5168fd47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 39
        },
        {
            "id": "c1292fec-d6f8-4f42-a01d-645dbe4e2b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 40
        },
        {
            "id": "a35b9a39-1c1a-46c5-9f6b-123734606ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 49
        },
        {
            "id": "fbdeab3c-36eb-405f-99df-4347fb36d4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 52
        },
        {
            "id": "b5cc43fb-5b20-4764-9d79-15f8b8519484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 55
        },
        {
            "id": "0ad3b8b6-ff53-4172-a398-bf6c1e96fcb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "24b97fc2-7c84-467f-85c3-1e5274c29504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 64
        },
        {
            "id": "a7ddf140-be67-4bf2-b53a-b2fcc66c0753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 84
        },
        {
            "id": "d2f0f634-c004-4567-a50b-5874bea4e5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "c400c11c-996d-4f1a-bb25-ca1681cd9bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "95b0dd4f-1d8c-479b-b468-bb32254a3cd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 92
        },
        {
            "id": "e0466b54-0444-49cc-8983-dc581e58304d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 103
        },
        {
            "id": "0d597cd9-c866-478e-91f3-166cc03396a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 106
        },
        {
            "id": "2d17db7c-6843-4dfd-a8f9-bcb2c9ac3ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "9e28c561-a9e1-4c4a-a391-55aa3d997769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 119
        },
        {
            "id": "6a2663a2-d194-4913-954e-30cd0a05c4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "7689616e-8fc2-4d36-8c57-e057929c8b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 40
        },
        {
            "id": "76e14327-b872-46de-a97e-8f00efb6f057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 41
        },
        {
            "id": "413bb76e-73d0-4815-a49d-0d0365d34b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 44
        },
        {
            "id": "21c480d6-cfa7-4921-8422-672a51e988a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 45
        },
        {
            "id": "afdf8e19-0019-44d3-a4b1-a945f376a848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 46
        },
        {
            "id": "8f2a18b2-7c05-4cdf-92f3-115de15b7105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 47
        },
        {
            "id": "d146edee-fb28-484d-8caf-dd7b43a390e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 49
        },
        {
            "id": "76913d0b-538b-4542-9fcd-97a7bb34acfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 50
        },
        {
            "id": "9a76ba0a-56da-4033-9f8f-8e7058ef95bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 51
        },
        {
            "id": "3a07b9db-34e8-42f6-ad99-879dc35bd523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 52
        },
        {
            "id": "8eab3be8-7bb6-4435-93b6-4e4a3d00b11c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 55
        },
        {
            "id": "dd6eec76-0fdb-4ec9-82eb-c24a52209a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 57
        },
        {
            "id": "eec36e7f-fe82-4437-b4e3-20f10123fdcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 58
        },
        {
            "id": "506b4d4f-8e34-4aa9-adb8-16a7c13bcc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 59
        },
        {
            "id": "8ec31e20-e49e-445a-a72a-cc62c20d34f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 63
        },
        {
            "id": "1fcab0c0-6696-42ea-8ad1-b2789056c9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "a27430f1-0368-43dc-8a57-f6d62cdadb3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 84
        },
        {
            "id": "af30d960-6c09-40c1-9602-00bd2600dc82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 87
        },
        {
            "id": "7ab7df89-fd49-4c07-994e-933829c87297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 90
        },
        {
            "id": "76b4c629-f8d4-4b39-aa24-8deb90612091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 92
        },
        {
            "id": "b74597c5-5a54-421a-be50-c9fdea0d6160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "680acf2e-3522-47f9-8c73-52a1d60891f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "d12bf1b6-f6f6-4086-8f6f-93893af01d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 106
        },
        {
            "id": "f1b434db-dc35-4973-a9df-dccf83400be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 113
        },
        {
            "id": "0b69f37e-7f60-4708-acbc-31248b58e8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 115
        },
        {
            "id": "466e51e5-1717-4d98-97a4-7cda628bca49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 118
        },
        {
            "id": "5f9fa3ec-5f39-44df-ab08-7366e3235228",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 119
        },
        {
            "id": "13f080f0-b5b5-47ea-b0df-b29dd6ecb840",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 120
        },
        {
            "id": "2308103e-1f2f-4d8c-a4b0-47a6b0fba559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 121
        },
        {
            "id": "6e845f7e-bfc0-46ac-aef3-971659e4e45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 122
        },
        {
            "id": "604b2a9b-8347-4c4a-8ca9-b1367068bfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 173
        },
        {
            "id": "f0a1a31b-e3f0-4346-98b0-9b1fbcaa5aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 894
        },
        {
            "id": "32b11b9e-4eef-4c34-ae4b-a0fd89c35446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 8208
        },
        {
            "id": "b2177585-b27e-4c87-8d79-faecc92ceffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 35
        },
        {
            "id": "6d187e83-8dd5-411b-82e7-cb9fdce113db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 40
        },
        {
            "id": "17fd0292-2c62-4c6b-a5dc-8975306c6749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 41
        },
        {
            "id": "6b478387-ebb3-4724-9a27-92c5c2e89b2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 47
        },
        {
            "id": "c6c48add-e6f6-406e-aaa2-aeabd698f310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 58
        },
        {
            "id": "28e7e1b6-f6c2-43ff-a165-1169311385e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 59
        },
        {
            "id": "727980d9-3142-4820-a6ba-69937f856114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 63
        },
        {
            "id": "aecb19bc-9902-44ea-8983-a394419dc694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 74
        },
        {
            "id": "ea0f06e6-0865-4f4a-9c02-479dd279f480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 83
        },
        {
            "id": "7f924301-33f7-46b3-8187-d1ea6b2f364d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "e07ebfb1-64a5-479e-a864-42cc69b40d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "4366827c-5f47-4dc1-8b1b-5b5f42d50554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 88
        },
        {
            "id": "53c5aa72-e404-4c23-b968-c27e5f855a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 89
        },
        {
            "id": "29a87864-c14e-46ae-b1ed-ff0ce79b05a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 90
        },
        {
            "id": "a699287c-9f1e-4b10-a822-cc45a3d77146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 92
        },
        {
            "id": "60d31ff8-ab5a-47a1-aa54-6fd8ea2c5560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 93
        },
        {
            "id": "2e4a9384-839b-43bd-9e84-1d411f30b5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 100
        },
        {
            "id": "2a220734-40e5-4121-86df-9d3c0166330f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 103
        },
        {
            "id": "d58de0aa-b3f4-4a1e-bfb5-ffe49b372759",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "1b3445a8-becd-4a9e-9700-23c23b27699f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 113
        },
        {
            "id": "65a79a38-9e9d-4f27-a89c-cc8ff16a6f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 118
        },
        {
            "id": "7b74cc86-a4e3-46a7-93f3-e2f605b09b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 119
        },
        {
            "id": "5e4a2c52-ef51-4b10-b25b-758751f79fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 120
        },
        {
            "id": "bf94d2f4-3f07-4ac6-9c74-971281ba12a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 121
        },
        {
            "id": "97b7365e-c36b-42f1-991e-a9c021311de7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 122
        },
        {
            "id": "761ee0e0-94ef-413d-b8b2-2686426040db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 894
        },
        {
            "id": "406f2e36-a8ad-40e7-90d1-b4fa28eed452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 35
        },
        {
            "id": "14bc1765-9d4d-4272-9079-93ff661cb896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 40
        },
        {
            "id": "daf2af2c-e9a7-4b93-bc8c-b43f6fc7f7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 41
        },
        {
            "id": "2190d004-640f-4443-bf16-df0ef22dfde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "3d7b74cb-834f-4a28-94be-c1aea7344168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 58
        },
        {
            "id": "f9062bba-7782-4d2e-b9ab-8165155ef7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 59
        },
        {
            "id": "ad591b96-fc4f-4cca-880c-4b8ab26814f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 63
        },
        {
            "id": "b332b2a3-9f5b-4bc2-a96f-089438a78334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "e9677b92-665c-4a37-9e0c-ad62076b63fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 83
        },
        {
            "id": "9c4abeed-5074-4292-bca6-49b68e6bebbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "01073130-ef7f-433f-bd2c-c5d64e432fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "1b25d4a8-2687-42d3-9126-38baf348a860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 88
        },
        {
            "id": "74a95271-790c-4a59-81fc-8e6a781671ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 89
        },
        {
            "id": "c2513498-9264-4411-825b-2ce176094a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 90
        },
        {
            "id": "631b9708-d56c-4dbc-87ff-f8fbdddde089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "6350ff06-8538-43b7-8fb6-7dd110032b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 93
        },
        {
            "id": "6fedabc6-469d-45ce-bfbd-6e5d538c8906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "a55ec20d-49b4-4498-89e8-0fa339b26e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "22694486-d497-4a07-b8bc-720b2d199df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "3e1ca53b-6212-4d8f-990c-8de68a5f277b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 113
        },
        {
            "id": "1eb16c25-bd49-4186-a0fa-de4262f90e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 118
        },
        {
            "id": "15858682-290c-4a23-8e91-a8073ad69698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "320c31c7-ee8e-474b-b49d-48da5955b014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 120
        },
        {
            "id": "e85df480-ac10-4aa0-94bf-94aec7e3a580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 121
        },
        {
            "id": "0e2dda9b-831e-4886-928f-80fa55bb8b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 122
        },
        {
            "id": "029db3bf-18f7-407f-a55d-44f5997461bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 894
        },
        {
            "id": "007c5f47-83aa-4c08-8d79-5b9274f7f859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 35
        },
        {
            "id": "8e0a219b-7a20-4b28-aa29-abbef7fc84bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 40
        },
        {
            "id": "dbd72501-19ca-49b0-ac8f-4e9f65ede820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 41
        },
        {
            "id": "5a9c7cc8-16cd-4b09-8f2f-93789b3f6cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "5bdada0e-4159-4088-8c9e-4b9c52dfd46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 47
        },
        {
            "id": "ebe70ed2-0660-44a9-a962-4d5d722c2727",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 58
        },
        {
            "id": "cb414d9c-10bb-49fa-b96d-2d5da2d76862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 59
        },
        {
            "id": "ca24a7e5-eb3a-4083-8fa6-c2b99d2fe487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 63
        },
        {
            "id": "c93cb3c8-d6a1-4fb2-80e6-a95e0181f279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 74
        },
        {
            "id": "58616092-f476-4bc2-aa11-8002ba44c6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 83
        },
        {
            "id": "d59de8e4-14ac-43ec-a027-868ae18d00a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 84
        },
        {
            "id": "db0c5870-b894-4f7c-9226-0b9da7792764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "fd418f40-6583-4d34-a03b-0272eef82cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "37601648-76bd-4e07-a3cd-bd7fccd4c05b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 88
        },
        {
            "id": "c77c4289-5c75-49b2-9958-7aabb0459e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 89
        },
        {
            "id": "08c57454-de88-45b8-bd73-fb4c32b2b623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "7ecc84bd-aa2d-4123-9c16-49dacdc7bfe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 92
        },
        {
            "id": "8e32ec78-a65d-420c-baf9-f8e0dd44ff45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 93
        },
        {
            "id": "f20020bd-528e-40e2-a778-e2bd093a91f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 100
        },
        {
            "id": "bc3a136c-f37f-4213-93bc-10b48b8a2003",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 103
        },
        {
            "id": "6142ba7e-fa9e-4b41-89d7-723afc9ed9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 106
        },
        {
            "id": "a3b658f6-feb0-44f9-85a8-5d4c19155a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 113
        },
        {
            "id": "0ac8652e-7043-4ff9-ae8e-9058586a7ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 115
        },
        {
            "id": "ff7ea725-040f-4cb8-914d-8c8e68d8f0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 118
        },
        {
            "id": "b9429b2a-ad90-4e77-b87b-3bda2366124f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "1d7b8fa3-2589-4fbd-b59d-8fa35d3e7fc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 120
        },
        {
            "id": "701d458e-cde2-4a31-829f-b3015183e645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "239dda28-6d19-4796-acbc-58abc4a65dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 122
        },
        {
            "id": "ddec3817-a913-4998-891d-531c515dacc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 173
        },
        {
            "id": "cc835a6a-4fc3-4b28-b72f-0e7875611616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 894
        },
        {
            "id": "ba352684-3327-4f5a-9a69-1754a8cf97c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8208
        },
        {
            "id": "16bb953a-33e8-4f13-b600-e8c76f790def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 35
        },
        {
            "id": "fb2eab56-1d0d-490e-9062-efac87689c4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 40
        },
        {
            "id": "4254ef80-0e02-4554-974f-3ce02ccbaf11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 41
        },
        {
            "id": "105b72f8-ec7e-49c6-b7fe-da7264fab247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 47
        },
        {
            "id": "23228ac4-6734-4630-ac82-d7ed291913af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 58
        },
        {
            "id": "9dfbce1f-2b75-4893-a4b3-7904e697349b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 59
        },
        {
            "id": "83dc1fce-9d38-44a0-b6e2-a22c6b6f6e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 63
        },
        {
            "id": "6a95e737-ac52-4f20-a6e3-e15cf10961bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "05526fbb-b9a2-49d2-b16c-7a157630d0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 83
        },
        {
            "id": "1741ff9d-8038-418d-8b05-8b1af15f392f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 84
        },
        {
            "id": "ce2bd39c-0e75-45f8-98ed-ee9543323258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "b01e3107-faea-41ce-ab2e-749d50b8b041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 88
        },
        {
            "id": "95b349e9-e9ac-4dde-aec0-551751d02f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 89
        },
        {
            "id": "ab58d7b1-809a-49b6-b5a1-5b2bae075012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 90
        },
        {
            "id": "a6e74ae6-ec78-4ff1-aa88-ba9c15029a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 92
        },
        {
            "id": "b18b7729-d56d-4acf-9610-ff4ec9aec0c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 93
        },
        {
            "id": "2e926bf6-22d5-4218-ad5e-b357f91ba821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 100
        },
        {
            "id": "6a668201-fb7e-46fd-ad30-70cfee0fab0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 103
        },
        {
            "id": "29f69fa2-c116-4ba2-9770-224c8e0f9db0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "d44276ef-bfcf-4874-9918-a06a85aee7b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 113
        },
        {
            "id": "45867047-5e78-4218-b995-6f73cf615f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 118
        },
        {
            "id": "9a90d991-5cb0-4a28-b47a-680dfe61122c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "a732bf90-fac0-4799-8cd4-6fe0762f8bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 120
        },
        {
            "id": "0b8438a2-e250-4417-b28a-cf714771d1d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 121
        },
        {
            "id": "5207d5cc-753d-42ea-ade7-04ef41c81928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 122
        },
        {
            "id": "9406dd8d-9dfe-4daa-b582-8872e87e7206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 894
        },
        {
            "id": "3e4bdcde-2b84-4190-8d83-dbab835499da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 33
        },
        {
            "id": "92938361-e68a-4e2f-9c89-a2416291bae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "1c03f390-a765-40f0-84bc-7614ec7e5ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 35
        },
        {
            "id": "a66c0687-b89e-4539-8583-cd46dfaac9c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "07c502f2-a891-4987-a7cc-e7607f1c8dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 40
        },
        {
            "id": "4263ddeb-b977-49b0-89fb-1a42012897b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 41
        },
        {
            "id": "d548b6b6-0b88-4a33-93fa-55f4ef370693",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 47
        },
        {
            "id": "34559078-cc1f-4cea-af7a-9b437b0eda6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 58
        },
        {
            "id": "36d86a8e-7d7b-4407-ab53-69b2643648da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 59
        },
        {
            "id": "cab94237-5c47-456e-a786-9ce221dca6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "f50abe6f-3621-4ebd-92d0-03077ca3fa89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 64
        },
        {
            "id": "9252d9a1-6d49-47e0-93b2-27a68a0fd053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "b241ab78-ebbc-479a-a8e2-981a3232513a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 83
        },
        {
            "id": "75115cdb-e037-4a13-9b6f-ffffdafe09a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "fde56a1d-9113-4af9-bea9-7e42f3c39c65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 86
        },
        {
            "id": "795bc995-2c85-48c8-98b1-c187fbdedc72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "3f6c185d-6d95-4ded-a89c-0aac91077468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 88
        },
        {
            "id": "5d385465-9359-4976-85a1-ebdeb9d8de2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 89
        },
        {
            "id": "ce641b67-b694-47d5-a234-17c705555eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 90
        },
        {
            "id": "68d8d810-da74-4aff-8565-eff6046b541f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 92
        },
        {
            "id": "ac815a18-8997-4895-8cbd-f51655ba14c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 93
        },
        {
            "id": "5dd4e7c1-77c3-4239-a15f-c895ec36696d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 100
        },
        {
            "id": "2500d50a-7d59-448e-bf0b-c3442d790484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 103
        },
        {
            "id": "ed840e44-6973-4d82-b323-90ee930fa317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 105
        },
        {
            "id": "2c1a4fb0-8150-4ba8-b2ad-6582a3b74a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "cfe45e27-1156-4fad-97aa-f660a15c16c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "01c7499c-4f11-44d3-ab14-0c5fbdb70481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 894
        },
        {
            "id": "8593c7b6-f0cc-47bd-800e-97754ed1eb06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "5c1b4ddb-a8f3-4a59-a2ce-8103723ed20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 35
        },
        {
            "id": "583226f0-5a71-41e9-abb5-860f7986f1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "5c9f92e7-f591-4ec1-babb-070a6c5505dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 40
        },
        {
            "id": "69ec6568-22be-468a-b372-64217bc5c1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 41
        },
        {
            "id": "c632b7e2-a213-4a17-8440-a9333d99af1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 47
        },
        {
            "id": "dae360fb-e1ad-4498-8322-a58628f12667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "715597a5-2a61-40c7-a106-6441f285cc28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "8d670fb3-69cb-4d57-8010-2eddf37b3879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 63
        },
        {
            "id": "5a326684-3925-4598-8e87-db75a228cee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "71c398b5-f039-4251-94b1-28fbfa756775",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 83
        },
        {
            "id": "2a78e484-7aef-4f29-b1b1-4a328a848a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "1c26d9a3-37cb-4d87-8e79-15468c42ceaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 86
        },
        {
            "id": "b2f5222e-645b-424f-a216-04670d0cd065",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "7c4cfae4-4d38-4be3-9eec-46a9cd7d9ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 88
        },
        {
            "id": "560c8c98-fcf8-4cb6-81f5-08ca2adba2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 89
        },
        {
            "id": "63b780bb-0019-4fc6-a989-14add31db872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 90
        },
        {
            "id": "ff2b0ce1-0c8a-496e-bdec-8e6b7c35d6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 92
        },
        {
            "id": "79e1e281-8635-4c31-a45b-eee4cbadd936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 93
        },
        {
            "id": "75c2239c-89c4-45c7-998d-d88f5e5f7d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 100
        },
        {
            "id": "5c90cbaf-c5f6-45c1-ae0b-74a261d8daf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 103
        },
        {
            "id": "01076c29-282c-4ed6-bf26-3a4f1dce506c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "318caf43-6a9b-498e-8307-c2de422633e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 113
        },
        {
            "id": "6eebea14-1e65-481c-bc57-e5b7cacb996c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 115
        },
        {
            "id": "5e29f6ab-fdd6-4e93-9880-74b11bce3267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 118
        },
        {
            "id": "8dfafea1-3b8f-4fbf-98b1-314fe5a2fb78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "e4e6b1ac-9dac-4a94-8370-7ff6d750b2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 120
        },
        {
            "id": "eb8f3b53-6f3d-4a16-9885-bd1194a20f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 121
        },
        {
            "id": "c54a3739-8323-47c8-8b0c-a28fec67cac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 122
        },
        {
            "id": "9569ef11-d2c6-4925-8173-8f96ce5bac91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 894
        },
        {
            "id": "3f7e0005-2b64-4fc0-8ffb-77ab95731725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 33
        },
        {
            "id": "a61b6415-2d36-4b84-903d-370414009417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 34
        },
        {
            "id": "47633fda-9513-4339-90bc-ff419f22c86f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 35
        },
        {
            "id": "7496b210-ccc1-4855-964c-100e016b3521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 39
        },
        {
            "id": "15a53c3e-2670-4175-bfa0-14f8f0ac6c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 40
        },
        {
            "id": "41167362-f859-4029-b6a3-775d9753fdac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 41
        },
        {
            "id": "2408afa8-7d85-4f7e-9a8e-8faaae581318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 47
        },
        {
            "id": "4219d189-b65e-4497-989f-b6270394a5d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "11aee839-7df7-48e6-bb65-a7c26051fa8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "a1e0e1c0-0aec-4e5b-8418-d73bc402f3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "8c8614aa-4073-4487-8bf8-7865b81f70fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 64
        },
        {
            "id": "874715b2-6574-4a1b-9491-e549d3bb2316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "fb9e3442-7621-43a2-9f46-386691458f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 83
        },
        {
            "id": "38396e48-802b-4980-954c-5dcab4f72839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 84
        },
        {
            "id": "89992ba0-5cd7-4dde-8e5f-bc4e5b0cc74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 86
        },
        {
            "id": "71bbf176-6ba9-40f1-96c7-714c5c69d70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "978c8893-956d-44ac-9a19-2f85fcf8904c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "c752db54-6f30-4ab1-aa23-b0dbc548d2ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "fe831898-5f24-4fba-82be-cc6bf2eaa911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 90
        },
        {
            "id": "daf90152-533e-4d32-b4c2-fb541fdc9483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 92
        },
        {
            "id": "226e35d1-53c4-45b0-be0f-b7af525aa681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 93
        },
        {
            "id": "68bce9d1-fdde-4175-8064-c0f62dba7c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 100
        },
        {
            "id": "93cca13b-61b3-4775-ab80-8c626ce8a71e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 103
        },
        {
            "id": "f57ffdd0-2eb6-4d1b-b4ce-c01795fa08ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 105
        },
        {
            "id": "38864d49-609b-4c82-83d6-8202f6c22a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "45d38078-5de5-4c90-9193-9d20cc9fc8b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 113
        },
        {
            "id": "bb0421e3-31e1-426a-8144-d9c975ca07e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 115
        },
        {
            "id": "72288fec-cfac-4342-861c-32ec07fed7d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 118
        },
        {
            "id": "5f326344-7412-4fc9-894d-6bbe5c01ba42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "79428837-e113-4c54-8d76-2669201acef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "d0f33feb-f3e4-457d-a7f9-77ec9c4ce856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 121
        },
        {
            "id": "4056ff51-e6db-49c8-afb8-ca667c939e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 122
        },
        {
            "id": "40a0d31d-bd30-4ef5-a9ea-e47059c0af11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 894
        },
        {
            "id": "8001c33a-65a5-422a-b28c-95364e5ffc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 35
        },
        {
            "id": "9a9b6962-559c-4aab-9f29-3dfd31bbb909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 40
        },
        {
            "id": "1dabb266-e76f-4770-8313-3723c468934b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 41
        },
        {
            "id": "8f346f7d-f95f-4cba-bb9c-59cd3ddfed6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "64b73fb7-cdb8-421f-b459-c0397f963745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "6815ac26-cce6-4a15-85a8-9684f9378c7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 59
        },
        {
            "id": "0c67f7fb-6399-41e9-96e2-4def923669ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 63
        },
        {
            "id": "11636b92-c977-43de-878d-f4e262c2aedf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 74
        },
        {
            "id": "1c67af71-9c29-4aa9-86e8-4da8e3cb55a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 83
        },
        {
            "id": "89f2525f-9bff-47b2-b3f3-f38989ab2dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 84
        },
        {
            "id": "18c3cad1-6a15-4a9e-8d73-ce4b9551dc34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 87
        },
        {
            "id": "ecd7818f-1cf1-4e24-8be0-8d3be0b66181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 88
        },
        {
            "id": "c6083db8-73d5-4ebd-bab7-e4f2695fc8c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 89
        },
        {
            "id": "ef578628-7a2d-408b-ae14-0303a7fd1eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 90
        },
        {
            "id": "d9cb552b-83a4-4d4e-b9a3-7b3cf2609a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 92
        },
        {
            "id": "d07149ff-3de5-4ba1-b4be-e67cde0aa6d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 93
        },
        {
            "id": "cd7fb8d7-bb7b-4840-bd62-c60fe641ea15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "3549c1a6-b5d2-4c94-a3d7-7012ff8f259e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "0a1b7f62-3465-4d12-aba8-543ec2fe392c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 106
        },
        {
            "id": "3a7877d9-6415-4acb-8603-758e15980290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 113
        },
        {
            "id": "bf53833e-5857-4280-a5b7-d6b15d41324f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 118
        },
        {
            "id": "cf5effbe-9d54-4571-9e79-0a76c93623d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 119
        },
        {
            "id": "9f696941-6859-4398-8fce-8e8195a9a165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 120
        },
        {
            "id": "1d0fe31d-abaf-40bc-be57-9bb16acbb48f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 121
        },
        {
            "id": "b94d11f6-e9d0-4bdc-8098-6cf3d5ba0c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 122
        },
        {
            "id": "5156a507-4642-4eca-87ff-07c2cdffe06d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 894
        },
        {
            "id": "78887974-5c8e-41a5-b6dd-ef7d93c38608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 35
        },
        {
            "id": "922bb52a-ad56-44f9-bde5-51dd26434e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 40
        },
        {
            "id": "ab4e562c-0fe2-46e3-b882-c1abe479be03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 41
        },
        {
            "id": "72ce04b7-1435-441c-a5db-ad5a9bce3083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 47
        },
        {
            "id": "074033ab-6b8e-474b-b3ab-e0a009fde54c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 58
        },
        {
            "id": "10c5e844-d27a-4e0c-a030-58568000eb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 59
        },
        {
            "id": "814bf2ae-5d16-4532-a37f-ef7f1b7efc06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 63
        },
        {
            "id": "92259451-eeb7-4a54-82ce-ff57bd18aeb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "27eb7877-6ca2-4e29-91b7-714b82581344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 83
        },
        {
            "id": "5c0d7539-e702-4863-bbe5-9fe1c90b16d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "65edf19f-035d-4ebb-88cb-b8601b65c435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "78aac9e7-8b14-4b0a-83c5-fa91ee02cb67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 88
        },
        {
            "id": "d90d84f8-7615-4127-9899-9bcf61c523c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 89
        },
        {
            "id": "25ca18ee-7f75-4afe-8880-303af7adcb4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 90
        },
        {
            "id": "fa419357-b445-4e65-8736-59d961045a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 92
        },
        {
            "id": "400d36e9-15f1-4f9b-8a80-cb31a66802b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 93
        },
        {
            "id": "e384cab6-507d-4367-a38d-4be1a9b76994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 100
        },
        {
            "id": "fefb9e31-db8c-4e85-921d-554c2b89d776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 103
        },
        {
            "id": "fea8a8ad-b0fd-46e3-973a-25793fb7d83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "89e5a763-8c07-4ae7-9aec-6fe5e147f6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 113
        },
        {
            "id": "f6f4f033-8f42-45b6-902a-47d00b3ee746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 118
        },
        {
            "id": "206809a9-90a7-432c-acf4-8df5cdd9a667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "25a6ae1e-9368-4071-9d05-e8fe9f973fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 120
        },
        {
            "id": "f69fd980-49fc-4f55-be28-7005dcbca6b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 121
        },
        {
            "id": "a5aff9e4-cd2c-47cf-845a-889b9398a498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 122
        },
        {
            "id": "183d83e8-dce8-4415-8076-77585b85cafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 894
        },
        {
            "id": "1d7889e0-b348-4e36-8c61-7f80e207d082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 35
        },
        {
            "id": "24efb5a1-d256-4091-9898-5ed7dfb6418b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 40
        },
        {
            "id": "0eb76c35-08d7-47c0-86c0-aa9c1c119ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 41
        },
        {
            "id": "8c44d567-3333-4b18-9fc9-c7525cfc8d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 47
        },
        {
            "id": "4ae51c55-ce6e-490b-b359-8c0b8f789133",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 58
        },
        {
            "id": "6dca16c5-5eba-46b7-a8d1-8ce3ace88637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 59
        },
        {
            "id": "3a29855b-98e1-4815-8390-351ab4adac22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 63
        },
        {
            "id": "9f719dca-9d4b-4d08-8dec-244e3544f766",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "5f029b15-4f67-48f1-a4a3-0fb77ea45b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 83
        },
        {
            "id": "09a2b1fd-f704-4217-a0f1-bf5c50e22c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 84
        },
        {
            "id": "48e80296-b553-4f27-8136-974a2b3b454c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "2c14575a-5c8c-4a39-8222-7994b688138a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 88
        },
        {
            "id": "a96eb8e8-b1ca-44e7-9e9f-ceec48983e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 89
        },
        {
            "id": "c61d415d-3bba-44d3-9473-8378034118d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 90
        },
        {
            "id": "377b1753-0348-43ca-b316-c1dfda17d281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 92
        },
        {
            "id": "c329af60-57e9-41e0-b55e-81c1ff513620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 93
        },
        {
            "id": "6bcfb684-91c8-4d64-9083-914db64d8f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 100
        },
        {
            "id": "887bc54c-250b-4c77-a05e-ea761c7907cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 103
        },
        {
            "id": "169900f2-7aad-45bb-92b4-9eac21c556c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "405c047f-52f9-4ea9-aba7-3a29db7e70fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 113
        },
        {
            "id": "702ee42f-343b-4cc0-9c55-851589b14ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 118
        },
        {
            "id": "13c42c1d-868d-4ed9-a444-e33246dd32d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 119
        },
        {
            "id": "d849bc09-483c-436a-8baf-d86e60688f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 120
        },
        {
            "id": "5f7219ba-5001-4ce5-890e-f218bfef7929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 121
        },
        {
            "id": "659f5fa0-ebd9-4b22-bcbb-b8f12bc52f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 122
        },
        {
            "id": "05198ad0-3bfb-4535-b4db-f18f11c8f1d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 894
        },
        {
            "id": "5264dcf2-527d-48d8-8b2a-7ef713440549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 41
        },
        {
            "id": "7f24b3a8-2ee0-4c7d-b676-d79867d66f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "c92f7be5-6607-4a8e-b76c-0078be11990b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 49
        },
        {
            "id": "c18714a9-a1ff-4664-b0db-cff4895a3655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 51
        },
        {
            "id": "8ebdb447-b2b6-48ea-86d1-902739e372ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "e5202f3e-c9c9-46a6-9964-07c89a4dd351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 55
        },
        {
            "id": "0ebfe5b8-3ec8-4538-b085-77362378f54b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 57
        },
        {
            "id": "ac050e13-4ecd-4edf-8e88-d1f61b16016c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 58
        },
        {
            "id": "547783ce-4e69-49ae-8cd5-4073719b2bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 59
        },
        {
            "id": "7a6a6f34-598a-42c3-b7c1-44e2600c0dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 63
        },
        {
            "id": "73dfe166-aac7-4566-97db-17469e937187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "e6e6b50d-cca7-4619-bf1b-dff69901f8f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 83
        },
        {
            "id": "19a8d514-0699-43d5-9590-92d96b657a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "37c7bbe3-f76c-4aed-9493-365d61d22786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "4b64d6d1-1ba5-436a-aff7-e230542bc5de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 88
        },
        {
            "id": "d7c60d91-4654-4b00-9dc1-b79b7d62c4c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 89
        },
        {
            "id": "6247bf3c-8a0a-4318-b521-2ef9a4a73fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 90
        },
        {
            "id": "3cdfc599-a72a-4e33-91c9-2a07c54ce186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 92
        },
        {
            "id": "5e882ae1-2a1d-4818-98e9-8bc1167b2717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 93
        },
        {
            "id": "43696ac4-ff5c-48dd-9966-869da117967a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 106
        },
        {
            "id": "844be505-183c-4a48-a210-6ce09d33f5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 119
        },
        {
            "id": "d4a6ab89-cd1d-4aea-bf61-a9792db84a3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 120
        },
        {
            "id": "4b8e1739-7842-47ed-ae29-f96167f7262c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 173
        },
        {
            "id": "3f71e068-5079-45f5-9e04-241d8b86b706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 894
        },
        {
            "id": "5b534205-68e7-4c1e-a062-04371006bdb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8208
        },
        {
            "id": "039f29c4-2db4-42cb-9ebf-d5c45ef1b924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 41
        },
        {
            "id": "2cfec579-a033-493e-ab65-d4e1aea03fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "1fef0eba-0adc-40e5-8033-29913abab846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 49
        },
        {
            "id": "51d1ec96-4ce5-454b-be55-7fb5dfb3d22c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 51
        },
        {
            "id": "1e50a4c2-8e16-409c-97e5-296f20c92b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "49c43439-b039-40a2-b001-9d9b8a1afb0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 55
        },
        {
            "id": "d8edb99d-e5fb-47a1-a811-8a6e14544033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 57
        },
        {
            "id": "8b489a9a-0015-4eb6-a520-95cc3f8e261a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 58
        },
        {
            "id": "b37f76be-bdbd-467d-a06e-d22af08bbc1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 59
        },
        {
            "id": "a5b7467d-02ee-4db0-a5af-1c75cb6e3d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "c35e089a-c8c7-4dd3-b6b7-115425c47457",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 74
        },
        {
            "id": "f0704cc8-de08-4b0b-aa9c-caff0ffee7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 83
        },
        {
            "id": "2d43db37-8fa2-4bd3-a8e6-ab64e29a00b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "a4cc93b7-3c87-4f41-a31b-48559e2e5ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 87
        },
        {
            "id": "11d6f793-66c4-461e-8e06-9b3430e0f597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 88
        },
        {
            "id": "e3a5543f-f124-4e71-94b5-f3fe96b6b17d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "6b434ac1-7e94-4154-8f4d-0ce6c1ff2b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 90
        },
        {
            "id": "7f0fa143-dd75-49b2-8d62-18cedf220751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "3a318a53-6b5a-460b-a657-27c5fdbfe8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 93
        },
        {
            "id": "a2c8d6ea-612e-4422-91bc-ca9ea0351401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 106
        },
        {
            "id": "6b150949-6afc-4f24-a6e8-d1b6d565240d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 113
        },
        {
            "id": "51ab3b7f-77e6-4738-9f60-d71541067ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 119
        },
        {
            "id": "dfedb67f-8818-4e90-a425-272447793cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 120
        },
        {
            "id": "af020495-aac4-458e-94f0-9268aac9c67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 173
        },
        {
            "id": "d187ed2e-bf71-4d63-b8ee-dcc95e7b9399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 894
        },
        {
            "id": "d8742725-b594-41aa-a3a7-3b3c95f46a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8208
        },
        {
            "id": "907a7a7a-81ee-4b98-9222-b31960261965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 40
        },
        {
            "id": "6d17316f-096f-4275-862e-94a5b54bb5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 41
        },
        {
            "id": "28f110cb-2e63-4359-8ad4-4b14dde8604f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 44
        },
        {
            "id": "b589660c-af89-458b-a4e5-ae50fef0c5ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 45
        },
        {
            "id": "f98bd220-94d1-42bf-96c1-3752761933b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 46
        },
        {
            "id": "2d7d48bd-0063-4915-be91-c8c38312712e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 47
        },
        {
            "id": "80712832-6da2-4abb-a780-5547047d24a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 49
        },
        {
            "id": "2a20255b-c29b-4672-ac29-e7d246bded82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 50
        },
        {
            "id": "0f4a7103-d2f0-47b0-82d4-dc5827ffcdd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 51
        },
        {
            "id": "32ef4546-59b0-4038-885f-369f4b6b88d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "020ff48f-ebe1-4dd6-a8a1-3c9fee9836f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 55
        },
        {
            "id": "44a51664-7ce5-48bf-bc4f-4c88646bcfa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 57
        },
        {
            "id": "0d2d021b-ac8a-4e66-a30d-47a097ba8878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 58
        },
        {
            "id": "983ecfcc-61e4-47d5-922e-bea9d80e8efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 59
        },
        {
            "id": "a5fbc056-42f5-4c87-8864-e3f3acfeb5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 63
        },
        {
            "id": "d02dfb65-adef-4ba6-805e-5a84b0d08dd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 74
        },
        {
            "id": "b9fa82fd-5754-4848-85ce-7282e7086288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 84
        },
        {
            "id": "64fa8e41-fd69-419e-b361-3964d03f18c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 87
        },
        {
            "id": "dc14db3b-7b69-436d-a75e-2d9eb1f9fb5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 90
        },
        {
            "id": "8ee1c42f-869e-4d6b-8f6d-3251df99e0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 92
        },
        {
            "id": "f063b5d8-78ac-4429-a0e0-3aafb632e4e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "db326978-8a0a-4b6b-9f55-21a910525cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 103
        },
        {
            "id": "9c87de7b-26b2-4f45-bf04-95f7ffe20e1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 106
        },
        {
            "id": "34250dd9-0c7b-48ad-ba87-3c4986f370ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 113
        },
        {
            "id": "38f838d2-e0a7-4c72-a641-40a210d7e011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 115
        },
        {
            "id": "b2d05d40-5881-42a0-be34-467ad2205bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 118
        },
        {
            "id": "ac52d43a-7909-4a74-8cfd-e3a6caff51d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 119
        },
        {
            "id": "8148368e-7cb4-4bf6-86d7-19d747c8b6c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 120
        },
        {
            "id": "6beea215-6775-4245-abe3-56a38e2903a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 121
        },
        {
            "id": "7b6080ca-91b7-467a-be30-e75270c53e72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 122
        },
        {
            "id": "e86a4694-a8b3-4e98-90fb-38aece4227f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 173
        },
        {
            "id": "1fc4c5ab-1e28-4a3c-858c-d113f324d8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 894
        },
        {
            "id": "ae715694-c3e5-4f15-9953-a724af8655ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 8208
        },
        {
            "id": "4b453961-1586-4205-a010-1607782afeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 35
        },
        {
            "id": "47897d48-7082-4409-a679-b105ae2138c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 40
        },
        {
            "id": "7f1135e0-45c6-4502-aa77-77e1988f97c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 41
        },
        {
            "id": "420719fd-41f2-4013-8cc6-aa61799834ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 45
        },
        {
            "id": "6962aaeb-d29f-4cfe-8b54-14a5147c981b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "b9d97684-6e8d-4978-8508-f1eec03d3a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "b0f8155d-b3af-4c79-98a2-0c51c49410ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 50
        },
        {
            "id": "199d9c12-3d13-4002-a56e-72f48b7a8175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "8bfe1e63-d37f-46a3-997e-d50b86caf3a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 52
        },
        {
            "id": "9d3d0771-dff0-414e-9f69-5b859e68155e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 55
        },
        {
            "id": "37306582-df5f-49d5-84d8-4cc98f5adac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 57
        },
        {
            "id": "5471fec0-7749-4d3d-9f5a-da0ea0f88aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 58
        },
        {
            "id": "18e5278f-15dc-43d4-a2df-bec2b0632d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 59
        },
        {
            "id": "9a104f4d-36da-4aa0-a240-267b2386fa80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 63
        },
        {
            "id": "d52c0afc-1cdd-4b20-a6b8-1485d2cc9026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "4cbeeeb1-5c7a-4c3d-aa81-26ea769e8acc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 83
        },
        {
            "id": "653181a7-174e-45e4-a24a-25747a398867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "e425a54a-5064-4d36-b903-e7b59a8fafba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "c43a3b8c-b6db-45fc-a4b3-b8da507644b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 88
        },
        {
            "id": "727da6b2-4d5f-485d-8fa7-1123d98d7abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 89
        },
        {
            "id": "029977cb-7104-4cee-b49c-c6c8dda78901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 90
        },
        {
            "id": "f62899d5-9711-4f1b-8826-ad8e296b82ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "9575ef69-6775-4dde-8ad6-0569d4590e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 93
        },
        {
            "id": "ce0cfdd4-097c-4077-b01e-2dc688b8162d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 100
        },
        {
            "id": "e7fef730-c45c-41ba-be49-2c511dcab581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 103
        },
        {
            "id": "ed96bbf7-765b-410f-9df7-7876f18896e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "2c7e71f9-5734-4c67-8dd8-7113275cc345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 113
        },
        {
            "id": "121e07d2-1d1d-4d9e-a25a-27bf3f8ebe3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 118
        },
        {
            "id": "53281f4e-5708-4f6f-a3dd-4e769414f52d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "76e5bd12-fce8-4951-b7a7-82e3b1b5731e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 120
        },
        {
            "id": "1f6e87b2-b880-48f8-b0f8-d9245ea088b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 121
        },
        {
            "id": "8940159b-c940-45fa-934a-937a443a2ab0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 122
        },
        {
            "id": "cd92a8c1-3e84-4aee-bff4-a419f48822a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 173
        },
        {
            "id": "7a24e205-a7df-4a21-aaed-9ac9197054e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 894
        },
        {
            "id": "140eebb5-8902-413c-b3f9-4f09793fe114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8208
        },
        {
            "id": "684f0edc-4065-4f6d-ae03-a81428d31a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 35
        },
        {
            "id": "343b7d50-a977-4417-a80a-9bbbd33882b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 40
        },
        {
            "id": "1121446f-e55f-4996-97b5-df8157a0d7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 41
        },
        {
            "id": "d5eba70c-9d64-428a-822c-b49535437152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 47
        },
        {
            "id": "7641d63b-5866-4dcf-ae9f-84d4464a8695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 49
        },
        {
            "id": "1a12da86-f015-4539-b877-0601b026bbab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 50
        },
        {
            "id": "56f7bb14-cda5-4468-9bc1-f7b98e6f911f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "ac000c5a-0b21-40b2-84fc-4a96f2d269e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "12a0ccce-13a3-48be-a32f-9da607f4398f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "82bacaf0-d2c3-453d-9ba5-91bca09ad6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 57
        },
        {
            "id": "22c2548d-a27e-4326-8917-f8b24529b834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 58
        },
        {
            "id": "0e6cab81-fcaa-4b65-aed3-03bff4605c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 59
        },
        {
            "id": "f8150b53-e179-4f80-b88a-297290b48763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "b40ff930-57b8-44dc-bf02-d59e19fa758d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "8a303ef8-63a2-4ef7-957e-0283b3cb4fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "f71763a1-0b17-4f1c-8a02-003382669212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "27bc98f8-90f7-457e-a364-2df0359be0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "c4662897-3b97-4fc7-979c-7dccd35507ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 88
        },
        {
            "id": "4a95efd4-0a1d-4c38-a654-c6ca356fe6b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "d8421eca-d49e-4710-86cc-3598af0cc20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 90
        },
        {
            "id": "b0885248-b57b-4ab7-b5d2-656eea3c275d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "f6f016bd-0f38-42df-9362-89e97e8a6806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 93
        },
        {
            "id": "02c58406-feeb-4e7d-9cf6-cbf3591bea99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 100
        },
        {
            "id": "534afea0-599a-4541-a41b-d87ef4c01725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 103
        },
        {
            "id": "00013892-fe38-487f-92e5-f58c2420c46b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 106
        },
        {
            "id": "cfac3962-379b-4e4b-863f-0a1d60cb96b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 113
        },
        {
            "id": "a34ca571-1560-451a-89fe-e5edf5a5f91b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "ed4e1b5a-7376-49e3-8f37-351c1dbdbf55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "b70f0e79-415e-4488-b333-81cd9ed75900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 120
        },
        {
            "id": "ab79f1b4-ecd5-456d-88f8-0774ae6fc5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "9231ec9f-e97d-4b0f-9148-6bd2ac2a139d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 122
        },
        {
            "id": "f2133275-0e45-4365-89eb-12c2d8f51b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 894
        },
        {
            "id": "55ba6158-8a6f-42d0-ab6a-8a5493535aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 33
        },
        {
            "id": "1e9fc0d3-eae9-403c-b23a-151701fdab92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 34
        },
        {
            "id": "9e945994-4c5a-4d5a-8455-5834fdc9ebbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 35
        },
        {
            "id": "13910560-61b0-4ae9-9c0f-28b75b7b99d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 39
        },
        {
            "id": "bf7d7d81-1fd9-42a2-bfe2-3f41ba81586a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 40
        },
        {
            "id": "71a77528-ec3e-492e-bd19-8a4e3f05f0e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 41
        },
        {
            "id": "c552b009-d7bf-40fc-bddd-536fde26979f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 47
        },
        {
            "id": "c676003f-0c61-49b9-9c1c-a2b9ec346a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 49
        },
        {
            "id": "5ead4d0e-bcfa-4dcf-a3f3-c4dbb6d8e73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "5acd8570-7f8d-4893-868f-4c3814dca608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 51
        },
        {
            "id": "f32d5d6a-7445-4100-b11e-b426d683bb83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 52
        },
        {
            "id": "0f98c3a6-028b-4cac-9384-0f87ab2dfe53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 53
        },
        {
            "id": "de1807e4-60ff-4191-a2bf-3586851b2d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 55
        },
        {
            "id": "6ed06515-98ce-4cc2-8fdd-0287a76671d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 56
        },
        {
            "id": "23bc2f28-0b42-40a6-b987-6969f56fd42a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "7a866796-768e-423a-ba4a-af94c0fcf877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 58
        },
        {
            "id": "3ce2eb8b-5993-4584-aea8-b16a12d5bb72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 59
        },
        {
            "id": "25d58184-f746-4e9d-a97c-49a28e30a2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "500dfa54-5d79-49fa-8a10-53faf8042e77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64
        },
        {
            "id": "d926e614-a9f0-4167-983e-05446376d784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "f7c7554a-91eb-4e07-a05f-b0cf3c0497ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 83
        },
        {
            "id": "ba323344-76f7-4617-a71c-c8ee50591abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "446823a0-d71b-4c9a-9123-c3889a48315c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "dc6937e0-40ff-4fbb-a360-e272248af683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "d66381ca-910f-48f7-ac79-ea6d9930fa35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "3915a391-f25d-4419-9259-4188acbb9b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "30d7c3fd-3f61-4194-abd9-71ccaeec0fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "cbd58aa3-be4b-48c1-871e-8330645d485c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "e9f89e68-a7e6-4ed8-a6b6-f30460b64871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 93
        },
        {
            "id": "8c1738dc-071f-447d-abf0-2957c69fe9f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 100
        },
        {
            "id": "ca5ed059-a08c-4683-8e62-df53dc31db9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "cf2748d1-cb23-41dc-bdbf-396d7d04923e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "ebeda6f9-604c-44b6-9b7c-7f87485341e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 113
        },
        {
            "id": "3b72b3e4-e2d0-4a38-95ba-45330dfdf41e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 115
        },
        {
            "id": "a3cd6dc1-9898-4cd2-90d5-617cfcdc7bb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "1dd45e2f-6968-47a8-ad1a-ae417f6d619c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "d6b904c5-0e7f-42f7-8687-68fecd413d62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 120
        },
        {
            "id": "dc4b8c9a-4bd1-4bda-b49b-dacf7811ebac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 121
        },
        {
            "id": "a8bcd861-0bd9-4137-9777-6cc585213f30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 122
        },
        {
            "id": "9a22002f-5304-4f53-83b1-0951c3f912f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 894
        },
        {
            "id": "6f9f9a0f-9433-411f-a924-9004437828dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 35
        },
        {
            "id": "6e99d123-bd2f-4de6-bf0b-8b182c4d8a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 40
        },
        {
            "id": "e1cf06fc-1043-4b07-b7da-7560e4edf66b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 41
        },
        {
            "id": "5db13e19-062e-448c-bad6-5b0b766021eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "72845a60-a8fd-4888-9dac-649c5bf98857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 47
        },
        {
            "id": "a4718974-eaa6-4d65-8ddd-b22acf245b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "63de09e3-36d3-4fdd-9cba-2af9a27c4115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 50
        },
        {
            "id": "83864417-d639-4e91-af7c-64cbaa74ff8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "323c1588-d062-433e-a6f2-e8a2ced8ffc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 52
        },
        {
            "id": "6575d267-159d-4024-b64d-dec9b3156a83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 53
        },
        {
            "id": "6e54ecdd-496e-48ad-a9d3-78ecc680b083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "0e881455-fe81-4818-98f7-635793e2dfcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 56
        },
        {
            "id": "9d68bad9-ab60-458d-bbee-c465d0f05623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "8352657c-badf-416a-9484-4bcde9d1c356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "7f71c5e4-64f9-4378-bb61-d82376b0b44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "2fee3c11-20c7-412d-8362-7e07175c10b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "694d1a1c-2c09-42a8-a4c7-9b1601af0573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 64
        },
        {
            "id": "0fd4381e-4951-458c-8c51-6e6c6fa6934f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "f53d7558-db5e-4bb3-aefd-f27c20ed6d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 83
        },
        {
            "id": "2e116031-2496-420a-8d8c-f06618659061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "306ad3b7-b9ad-46b2-93b2-25c436e57729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "1929e60e-884a-4e53-bfe2-867df6224f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "11d5f846-e984-4934-b01c-00325aa49295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 88
        },
        {
            "id": "6d030672-ea54-49bd-a78a-586e3230fe4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 89
        },
        {
            "id": "55e8f7ec-bbfb-4499-b27a-e0141d1853dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 90
        },
        {
            "id": "3d358767-ac80-4384-99cc-a1e110879fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 92
        },
        {
            "id": "b9897ab5-9cf5-41a5-874a-00cdac37a0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 93
        },
        {
            "id": "f95f8fa8-6ed3-48fa-86fb-960e510587fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 97
        },
        {
            "id": "81d5c9d2-81c4-4afe-ac58-1a49e2c6d9fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 99
        },
        {
            "id": "a8919a34-2c43-438a-9ec8-df7311ab2446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "7dcf6b62-4d2d-43b9-946e-268ff91c0299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "a3d43369-9586-4ee2-ab11-f0dbf6d2e0d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "ce5f0b6d-42e4-478c-84d3-cf8e8013fe55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 111
        },
        {
            "id": "d9f3c67c-3d9d-46aa-8dd5-553ffe4ecc30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "a88feeeb-f483-4f0b-b1ed-6156cb014b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 114
        },
        {
            "id": "72800f83-78ba-46f9-bb4f-dc0c18e98b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 115
        },
        {
            "id": "3078fbb7-8e40-4533-8268-abe3b8e18982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 117
        },
        {
            "id": "cb4af23c-6476-4968-b4ba-28f3b197d9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "1a2a9a64-d080-4902-aa9d-b0c899211375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "67555a94-5d33-4174-811f-19aee7b6af19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "e8815381-b806-4153-8149-9711d3d56031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "527a383d-a71b-47a4-bfb2-12484a2732d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 122
        },
        {
            "id": "c45dc38e-da90-45f0-a577-32e147a9ccf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "d3d5decf-f37f-4fa4-8f80-b1bcd8312dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 894
        },
        {
            "id": "4b48735e-397b-43a7-823f-e2ac3b524908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "b3207976-12fe-4005-bcdd-0896973fa899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 34
        },
        {
            "id": "b1c3da53-8f24-4a07-9b42-03d7c9acd799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 35
        },
        {
            "id": "c7f43cfd-c4ac-49b5-91db-fc66b5c47c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "ea08de04-0806-438f-8c59-455c178a39ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 40
        },
        {
            "id": "21158c95-a01c-410a-8819-196332544c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "232bdcbd-b633-4539-9cc9-76fa7294b1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 47
        },
        {
            "id": "cc86714f-acba-4d1e-8003-38a55cf09739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 49
        },
        {
            "id": "71dbb9f4-e3a4-45ed-ad90-b2a81208a3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 50
        },
        {
            "id": "103c728f-5230-4542-a356-38756b8fb3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "98e1df44-50e7-47cd-8743-4b8e57805dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 52
        },
        {
            "id": "f89bf384-5cfc-4cb8-8d59-cd64e23af1e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 53
        },
        {
            "id": "ae35a385-ff1b-4a8d-b33b-b2cfd7550ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "233cbe95-f407-4183-aadb-a28a3218b1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 56
        },
        {
            "id": "d2ced774-80e3-46f6-bd83-bfb1d1556a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 57
        },
        {
            "id": "235062e3-09b7-422a-ba8f-fdf877a42697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 58
        },
        {
            "id": "535d9847-7b8b-4c5e-b33a-c8fc9c2be6ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 59
        },
        {
            "id": "35f54bcc-7134-4a2d-8a57-b3173c4b6dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "ef847783-7df4-4690-8a84-5c2998605aa6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 64
        },
        {
            "id": "1e2ae0f6-f935-434a-a80e-731fbe55f90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "bb47a02a-05e0-440b-8027-31cc021a28ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 83
        },
        {
            "id": "350dfcb0-e322-4fac-b05b-52fa1367a685",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "4ee6ad47-3713-4fe9-a441-dfe6e4cc8da2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "81424058-ac7f-4479-bbb2-7902c3832fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "f7ea2fd5-c575-4465-b6ae-bd94bedc6a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "af49ac51-f8fe-43ec-b67c-73b5a669f5a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "6d648556-d36e-48d7-9249-027db3fd61af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "a4844cbd-d91b-4eff-b194-2d272e44e90c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 92
        },
        {
            "id": "cb22ded7-9554-401b-8024-39996dc238fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 93
        },
        {
            "id": "2a3cec16-086f-4086-8b94-b05ae9892e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 100
        },
        {
            "id": "2a50d615-4fba-49a5-88bf-cbc647f8035b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 103
        },
        {
            "id": "f467e890-f735-44eb-aef1-46d9d6ff66dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "d8d1a0d8-9785-4d05-ad08-9bbd78231c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 113
        },
        {
            "id": "d5a052e1-0736-4254-a0cf-c63ca947709e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 115
        },
        {
            "id": "65e3fe9c-29ca-4370-b610-ed1edac78264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "480c3f3e-05e8-490a-aaef-1543f51e2467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 120
        },
        {
            "id": "aa583296-2af3-4954-b7d6-3bf68535d5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 121
        },
        {
            "id": "2b1c60d7-c933-4e01-8fe2-3148a7acb395",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 894
        },
        {
            "id": "c2010a34-f225-4b30-8771-b2ad83602b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 35
        },
        {
            "id": "33e1ab8f-12ea-4b10-81e8-605ee21aedc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 40
        },
        {
            "id": "a8adc68c-b52d-495b-8293-4b7908119b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 41
        },
        {
            "id": "c61bb532-0152-437f-a7f8-b7183fabb355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "eb0b0fea-fce1-4857-ae4b-6a1f34d2caf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 47
        },
        {
            "id": "e3dfe228-84ae-422f-ab7e-9546806aeb4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 49
        },
        {
            "id": "96ecc7e6-47e3-42b9-861f-64801df57f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 50
        },
        {
            "id": "8b999ed0-9599-4a67-ad18-9d68a9e38470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 51
        },
        {
            "id": "ceb3515d-307d-4b02-8286-24688f008ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "d30202fe-2221-4f25-b260-cfb8f95f5776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "ed0180f2-aaa8-45f6-a0da-edc3b783cf6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 57
        },
        {
            "id": "cf4430b8-ec0b-4de8-8f6b-fe24e7324ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "41dbbe5f-a2c4-48bf-af51-945139351186",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "2bf889ac-a7bb-44b5-9c08-32a12bc1c3ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 63
        },
        {
            "id": "d5d2cd2b-c8f1-4cff-84b5-7d12671975a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "a514baf2-c437-48fb-b6f7-66a2e0ff515d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "54e05323-0c82-40e2-9a10-12d8b35bf920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "f4467449-1e0c-42e4-a9d4-967e4b8e7bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 90
        },
        {
            "id": "b8f8421c-26f0-4a7c-a4d6-e1565268237e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 92
        },
        {
            "id": "b98daefd-031a-4158-ab18-3af7a248618f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 93
        },
        {
            "id": "7a5d4d18-984a-42fb-8c43-65a6c9734596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "14045080-d379-4ef4-bfd4-f66fed871f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 103
        },
        {
            "id": "0fdbbb92-3bef-4d75-bbaf-ef322ae8bd0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "42ea1884-f132-4d4f-bdfa-658892511d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 113
        },
        {
            "id": "d4b398ec-ce9e-4f15-a451-e4d7da5b301a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 115
        },
        {
            "id": "04251ec0-d9a0-44e3-9db1-0d777e6b82d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "414ad796-0002-401b-b102-6de0dc88c2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "1508dec4-c75b-48c1-b5fe-c8399d0e0bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 120
        },
        {
            "id": "53208119-3e13-40ab-bf64-a74d1bfa9b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "7ae7a462-e244-40cc-89ce-69ded24e71c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 122
        },
        {
            "id": "2b0798a1-8197-4173-bf24-d96601ae5352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 173
        },
        {
            "id": "3fbc5f94-28c7-4239-b9e2-42286d2760aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 894
        },
        {
            "id": "50b614eb-55b6-49f3-bfb7-b530c851b410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 8208
        },
        {
            "id": "c4e89e1b-439a-4169-8a37-13ae4f3a26ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 40
        },
        {
            "id": "76a94940-0fce-412b-a63b-21627f3145bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 41
        },
        {
            "id": "395c71c2-554a-4d1b-9be0-79aeeae13786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "00144002-4141-44ea-8a13-e904c26b5e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "feab5f99-5375-4428-b9a0-b4156e83ea20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "bb2aff8f-62a8-4233-a407-f19b7cc17c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 47
        },
        {
            "id": "b32f9f7f-fb79-42eb-921b-f3335854405e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 49
        },
        {
            "id": "1dcf92e6-1b20-4451-8b1c-d80d0ab750b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "c142fafc-7da1-4f50-bf83-89948705b7dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "91e5a873-dfd9-422a-ada5-fe809d1abd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "20040423-9014-4e6f-8522-2d2dc3b27f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 55
        },
        {
            "id": "b5fc987f-ccf1-4f31-98eb-b6996f9f60fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 56
        },
        {
            "id": "10bff15f-bbb4-422a-8aa4-d3a04dfcd5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 57
        },
        {
            "id": "75b3c59e-81e9-4456-a8d7-0bf429ccb976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "0fd31e86-dbf0-44f7-8a19-e1da0004b531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "75601347-fd10-4770-a236-1da69cc192db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 63
        },
        {
            "id": "7effd495-6587-49e1-9dfc-41e9185288c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "0a80487f-ca4e-4d3a-a97a-d8eb803c0974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "1c7ef9a9-bde9-40e4-bbec-105090216bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "1b76f589-7bf2-48a8-815d-1d318f4fe71a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 90
        },
        {
            "id": "22fe1c87-333f-4bb9-b9c8-e436ef02215d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 92
        },
        {
            "id": "a7c72483-0af8-4dc3-b724-dc24cf337cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "70624e2e-90c2-44a3-9612-b8b63e9730d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "d2f5e71e-c546-4e90-bc7c-2df592e44ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 106
        },
        {
            "id": "da1a5028-6c06-4f3f-9779-201017413c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 113
        },
        {
            "id": "88354c21-f742-4f3b-9069-9a9d2fd9fa96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "4adb9579-a55f-4e1c-882c-e0ba87109190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 118
        },
        {
            "id": "6a819166-693c-42ca-b63b-ea5097bf29e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "fba2d7b1-11b9-4461-9616-0450a4a1b4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 120
        },
        {
            "id": "5c90d271-a7f7-45bb-b5a5-911c3eb547b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "5ba42591-962d-4007-ac6b-86ca9b73b3bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "c3ecb11e-dd23-495f-ad5f-f57a07dab891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "db61ad8f-a1f8-4be8-a68e-3af842c4bc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 894
        },
        {
            "id": "74a3e59d-9914-426f-b6cc-caab28597e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 8208
        },
        {
            "id": "ae2edd1f-c521-444c-94de-e04b2c064735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "786444a6-70e7-4493-a9d3-6e30722d80bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 34
        },
        {
            "id": "384aefaa-dd50-4079-aa5b-e63aed0a1721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 35
        },
        {
            "id": "bd388ed2-d5b7-4f62-b84c-193dd4dbc259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 39
        },
        {
            "id": "e86c072e-f9ab-44f6-8e89-f7443bcb1d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "575e6ba9-c2a7-429d-9f12-d2662d3e3a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 41
        },
        {
            "id": "f2549c44-657b-4e8b-9587-a7d2060c027f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "4116be1f-dc6e-4337-8261-589a5c295991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 48
        },
        {
            "id": "52461b47-6c59-4d33-88bf-53e74ae5ef99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "18c3c093-dab7-4195-8cde-7d5d99dad0a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "2b045e1d-d263-4902-810d-cb48e774f157",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "c6004488-c154-4186-9e29-9ad34439e83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "d409dfa1-4356-443e-a8ef-c101cc790e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "a99a70ec-f447-48cf-86c3-075f0dd02ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 54
        },
        {
            "id": "0ba2ebcd-b1d0-4139-b76c-921d0346e4c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "a98aeea6-c0fd-4c6e-b6ec-a4a2f16c89fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "14d1e255-66fa-44c3-9a36-111cf8d7dbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "3b81fd67-3f58-4288-a4ad-73456335d5a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "e9f8ea86-3959-4b38-90f5-88a684be7fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "35093e74-ce87-4559-9c4c-fdbfda0dd80f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "75e6df9c-92ad-4be2-912e-9b3d307b3b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 64
        },
        {
            "id": "c104babf-7018-4c71-aecc-32cb169e3854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "4fd375c4-fc7c-4dbd-a90f-e858e0523754",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 66
        },
        {
            "id": "7d42a47e-d75c-481c-bd32-ea122aff41c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 67
        },
        {
            "id": "9cfbb972-b23a-49bc-b1b6-8ba1db9543a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 68
        },
        {
            "id": "b99d9b91-5375-4477-9300-9503fa88ae5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 69
        },
        {
            "id": "d1fa4843-9159-4b57-bfea-f55f3991371f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 70
        },
        {
            "id": "326763e9-20c5-4092-bdd2-ab2f0a8548c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 71
        },
        {
            "id": "f03ffaa7-627d-4bec-8879-7649ee12f651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 72
        },
        {
            "id": "b68680a8-a356-4a80-b3a2-113aa4bc5e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 73
        },
        {
            "id": "db0074e4-991e-425d-ba4f-9a99b1c75481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "21c0ecc7-fbc3-40d9-b6c8-fcc2989b62c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 75
        },
        {
            "id": "792138bf-a3ca-49ab-8de8-1254dd9ec6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 76
        },
        {
            "id": "581ca3b3-aaab-40aa-b87b-faa4c45a5c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 77
        },
        {
            "id": "c97cfc44-31e8-4bb7-a54b-2bd76956157a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 78
        },
        {
            "id": "34287f6b-cf32-44fa-8ecf-6906d1350ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 79
        },
        {
            "id": "f6a9f968-36cf-4d5c-96ed-11de5d997c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 80
        },
        {
            "id": "df5f1b64-96e3-472f-87af-93158b8cff54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 81
        },
        {
            "id": "d9410924-73f4-4828-ac04-005997cbee39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 82
        },
        {
            "id": "22aad770-c0f0-468c-bb63-cd0a2f3492ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "27fef13a-7cbf-4ecf-b7c8-f3cf18392582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "6bb20bf9-3b1b-4831-82d7-8e8272ea13ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 85
        },
        {
            "id": "8ea127aa-eee6-482f-b043-9605c3dedeb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "873c15bd-c278-4121-aa87-c1c00b77a038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "b886b8a0-9309-4742-aa3e-81a2bee644c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "53c3f3fb-daff-41f4-a84f-211101cf73d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "fdd57eb6-9e8c-47dd-9beb-12934de5c5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "863869fa-3604-4f53-8344-b4910f54c4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "8d6ff976-ef87-4ad5-a2fe-3ee7bf145a49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "a3b7c2db-cca3-4a4b-b77d-bdbbb30cafe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 97
        },
        {
            "id": "80409a77-ac7a-4873-a848-ab9d50bf2b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 98
        },
        {
            "id": "f3eac564-88d6-4628-9ba6-c3d574e5e04d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 99
        },
        {
            "id": "aa4109aa-2d5d-4d8f-837a-5f2d7a2ca939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "dabfcdf0-681b-4b1d-80e1-37d3695df44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 101
        },
        {
            "id": "cf21cd72-8ff2-4292-920a-1dccf32f4484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 102
        },
        {
            "id": "e88e6cf1-7b7e-4b90-9d92-6eb2dad31945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "2012c108-4262-4a56-9ffe-c3aafc94f10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 104
        },
        {
            "id": "78d42bbe-e10f-4650-abb0-bea2de311172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 105
        },
        {
            "id": "e1f3cfa2-8b05-4a17-ad88-93bce126315b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "d055aea3-bc02-4b90-8a7e-915f061d4fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 107
        },
        {
            "id": "0d919fc3-3f8c-486c-9310-aa1aff0fa180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 108
        },
        {
            "id": "334f9cc4-7b23-421f-a849-ff714b763a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 109
        },
        {
            "id": "f2f26269-94a2-40a2-985c-fa5c56690f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 110
        },
        {
            "id": "8d86c6a2-4110-43ae-a193-7728a695cce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 111
        },
        {
            "id": "f5de028b-e8cf-42a9-abfd-fda7925af1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 112
        },
        {
            "id": "f69a9e73-d8ea-482b-bf73-e30e5b8ac81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "af9d3552-2554-47f3-be94-f77cddc7d0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 114
        },
        {
            "id": "7fc4572e-b4a2-4bd4-b153-fca0d6d8bd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "c1d9a3ff-c255-480b-b29f-8ddd439e4b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 116
        },
        {
            "id": "81279f6e-2d54-4e6d-a2f7-b05e9a7e8466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 117
        },
        {
            "id": "3f6114af-83e9-488e-bc0d-3c71190c8d4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "c8dc62bd-7ccf-45a5-ba80-88c55d16f9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "10a0c023-5801-46fe-9421-dd97d461b561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "0eff36fa-d9c6-4ca3-b861-921ab384b83d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "837e86d0-4b2c-436b-a392-1ca679466846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "1fd2512c-b930-4869-bc46-b07ba235d903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 894
        },
        {
            "id": "d54535e7-10a2-45c9-ac9d-bd1c5c4ec249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 35
        },
        {
            "id": "0b3466cb-0609-4a95-9fca-526953a18ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 40
        },
        {
            "id": "9c40fd42-6ce0-4387-8e02-1c0bd1b65ecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 41
        },
        {
            "id": "c211d39c-8cb8-44b2-b82e-5b114550a5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "6dfb1087-493e-40bd-8f75-7b20a2ac9618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 49
        },
        {
            "id": "84086ade-588a-4b29-9c29-1c0389ce94f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 50
        },
        {
            "id": "e1b8067e-cc74-4db3-9af9-307e3e1c3690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "c930e935-3409-4606-809a-10cc25a9eacd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "cf2a5fde-a0f3-49e4-a1aa-d19cb4fc7718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "cdd91477-6912-4b53-a101-279821fa04d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 57
        },
        {
            "id": "066f7ebc-8fac-471d-97a5-dc81f95611f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 58
        },
        {
            "id": "8c776a54-c842-417a-b894-e64fdfafd910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 59
        },
        {
            "id": "42f196b7-3d88-4cf0-b90e-a280544e27d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 63
        },
        {
            "id": "6ac65a9a-8444-46c3-9220-7090eebd3474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "afb9ee27-56e6-49bb-ab6e-517fbefcc51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 83
        },
        {
            "id": "92b73888-d088-4af0-956c-283c15bef5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "6a57b1b4-6045-4c12-90ca-68f0a89ee8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "cc3a28dc-650a-45b5-a616-0d307901ff5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 88
        },
        {
            "id": "419c9028-b3a6-47c4-8a77-f6e55d92c5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 89
        },
        {
            "id": "970a61fa-bbd2-48ac-8f93-b154c0aaa0f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 90
        },
        {
            "id": "ce4c4e34-2b3d-4f61-9841-3e66186d693c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "ca9dca90-17b8-4c58-b3a8-e4f28412929a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 93
        },
        {
            "id": "1527fd95-28a0-492c-b943-1e048cf82de2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "78e95258-d490-45d6-b3ae-a565161e8700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "913c202b-ac58-4ea4-9771-8ef35377a3ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "ec51cc5b-1537-4452-815f-ed7e0e0a638e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 113
        },
        {
            "id": "437bbf12-65a4-4bba-a192-9622100ad852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 118
        },
        {
            "id": "fc068592-c785-4e33-a734-8341e9fa69d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "b5f7ade8-148c-4345-a919-e78727779b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 120
        },
        {
            "id": "70243dc2-08c9-459b-8fd8-d83c75e13518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 121
        },
        {
            "id": "05b7223d-42cb-4028-a7ba-0dd158c79c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 122
        },
        {
            "id": "846c4126-f34a-4feb-9ed0-81bc2ad2b36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 894
        },
        {
            "id": "82200632-f7d6-4549-9d70-b32495fc4e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 35
        },
        {
            "id": "ff0a5256-db6c-4a13-9d66-90f7fcf5b6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 40
        },
        {
            "id": "554a46fb-522d-4395-8b17-0baf046bbef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 41
        },
        {
            "id": "b3b885c8-7bc5-414f-aae8-73de84824437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 47
        },
        {
            "id": "64250480-39d0-458c-9715-03430cc9313e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 49
        },
        {
            "id": "2bd82fba-bcb3-4d48-9dad-ce32655ee45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 50
        },
        {
            "id": "6aa851eb-4c12-43d1-8f0e-000fc88aa9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "67110b32-6c3f-4dbe-8835-1887856b7cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "4df127c6-ec53-4ff8-8f1a-7c13f8fc60c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 55
        },
        {
            "id": "eccaeb08-a5f0-499b-9a4f-4c393a7da670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 57
        },
        {
            "id": "ca554b82-2da1-41de-a5cb-6eb6795da8c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 58
        },
        {
            "id": "77868ebe-6cf8-4903-85c8-9cd7c92307b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 59
        },
        {
            "id": "88d3f10a-093f-405a-9975-4422ebdee566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 63
        },
        {
            "id": "cdb6e0e9-6764-42c4-96da-31bf6e309459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "b21432b0-7e1f-4a57-a354-fbc1db5e4670",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 83
        },
        {
            "id": "c13f13c5-631e-4bdc-ba31-36a2a067b41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 84
        },
        {
            "id": "2a0d1713-4c8e-4b47-9ca0-93691d00e8c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "a74c8659-c806-41dc-9d82-2548f9ffa04f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 88
        },
        {
            "id": "cb2c1a4e-b655-4ba2-8e55-2815677d1a99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 89
        },
        {
            "id": "07e391da-c815-4c8a-8d1d-d286434f09cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 90
        },
        {
            "id": "6f36ac46-ed98-4c2f-b3c6-f25885ec9a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 92
        },
        {
            "id": "06c9f2ef-3e0f-4cec-b628-5914a2d0ed41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 93
        },
        {
            "id": "ee227bd5-b6ce-4e71-bc3f-4acc7c7be618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 100
        },
        {
            "id": "a4e49a6f-a4a9-4195-aa1b-76d7a635e195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 103
        },
        {
            "id": "42a21ec3-997e-456d-98a5-a7f1252c0313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "fcba3a3a-2fe2-4a45-a97a-e024620aa92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 113
        },
        {
            "id": "48bca2b9-633d-40ca-92ac-3be0f7a26b70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 118
        },
        {
            "id": "6af9c4cd-7937-493b-987e-5d42f0a899a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "e51ad969-7e2b-4f0a-908f-aefb511a4a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 120
        },
        {
            "id": "b3f6515b-eeec-443a-9fdb-197812919874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 121
        },
        {
            "id": "8d2d631d-f64d-40aa-ad7f-008da30a7703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 122
        },
        {
            "id": "ab4c1e05-aa58-4df7-b532-fedc8b112886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 894
        },
        {
            "id": "042a3f5b-ce5b-498c-aec9-31f8daa22b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 35
        },
        {
            "id": "0a77f455-cf49-457e-9139-c45f5d7c52e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 40
        },
        {
            "id": "09f6874a-0073-4223-b7dc-ac58c70a7938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 41
        },
        {
            "id": "5fc285ab-7ebd-4944-b593-0b0636dd1792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "e8591081-55cb-4196-b796-e4ed367f81c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 45
        },
        {
            "id": "72b2323b-ecc6-48e8-89d1-422eab6e6eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "778a4f33-34df-4a5a-ba60-b4426a3ad222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 47
        },
        {
            "id": "8172e166-7898-422f-8103-2ed3ec1cb2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 49
        },
        {
            "id": "185441dc-7da7-4958-bf59-66fe41ecbd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 50
        },
        {
            "id": "8032b144-5f7a-4d89-a4c9-647a2a5cc94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 51
        },
        {
            "id": "2cd9c577-4e0d-4237-a6d7-56a5ae441864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "5f02023b-ef0e-4d39-8d12-d5623864ab5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 55
        },
        {
            "id": "71540813-1ee9-4868-a7b5-26f03eee98f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 56
        },
        {
            "id": "4b2a6230-ac19-41db-af29-4eaef27a56de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 57
        },
        {
            "id": "67e0e61a-27bb-4bca-98f4-fa8c19812027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "ce91d0a9-8d31-47b2-a911-bba722ea55b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "511b2db3-b04b-4468-95cf-42404dbec0b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 63
        },
        {
            "id": "58cf1240-246d-402a-a0b5-4d28c65f485b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "ff60a232-f8e9-453f-99b7-b8a39f1a1a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 83
        },
        {
            "id": "4ec556eb-f816-4ef0-9803-e909a1f8ffd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "fcb0a2e4-48cc-4e61-9b66-1e9e4bcb526f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 87
        },
        {
            "id": "f80cdccf-db86-40e3-b323-89972f4fc14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "99b0d684-687f-4554-a1b6-683d60dcf660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 89
        },
        {
            "id": "30049510-2851-47bd-8d5e-f08018d72ad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 90
        },
        {
            "id": "e1d341e5-0735-4976-b78a-69b722feb45a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 92
        },
        {
            "id": "adda2a3c-9799-4802-b7b4-5d8efaae3b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 93
        },
        {
            "id": "73750ce7-2dbc-4c2f-bb68-c58fd4cfdeb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 100
        },
        {
            "id": "c5cb8740-2e06-40ab-93c4-6fe1f120bfa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 103
        },
        {
            "id": "6a787cbe-3150-4642-a0e0-a960468b4cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 106
        },
        {
            "id": "3c0dc95f-8d5c-4e56-9df8-7580afbc4719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 113
        },
        {
            "id": "c2135b17-b9ad-4b92-9728-a7efc2c1b313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 115
        },
        {
            "id": "efaae50e-7d4a-4f59-a410-d557d9609267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 118
        },
        {
            "id": "3a1616e0-b4fa-4f2d-a120-c69b969b723c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 119
        },
        {
            "id": "82d95828-e694-4af3-a72e-5ac43592cfcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 120
        },
        {
            "id": "9be71ed0-cd5a-45a9-b22a-255e9a91c417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 121
        },
        {
            "id": "91a8d2fa-43eb-49d5-9983-10530b59dcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 122
        },
        {
            "id": "151d0ae0-fc98-4dc0-b5a3-1089f8f16ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 173
        },
        {
            "id": "3c1a61cd-0db6-4e33-a6d2-773e85963a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 894
        },
        {
            "id": "351801d6-c462-458e-9a60-27ad43fd37f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8208
        },
        {
            "id": "c184870c-963c-4bbf-ab59-f3d82f959682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 33
        },
        {
            "id": "0186da11-4f03-4253-aa9f-aa6f5d4ab47d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 34
        },
        {
            "id": "dedd44c5-5928-4ecf-a0f0-dd0b5d766bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 35
        },
        {
            "id": "99326dea-6a48-46da-825c-a39a1980c965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "d6feda1a-4e40-43e4-b3b0-dd44a5ff83ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "d6b83d4e-7c01-4d4d-b949-2d1241e81346",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 41
        },
        {
            "id": "45c3aa63-2535-4818-bfb7-e6d4be328a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 47
        },
        {
            "id": "29acee0b-82ad-47b3-883a-db49628b950c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "e02620c3-4f56-475e-822a-3935e895a226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 50
        },
        {
            "id": "8c5b9dfb-3bba-4482-9c64-7a965e1286d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "6c89ba09-04a9-41c3-b02f-787fb90dea3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "3060a59c-fbb1-4c6b-b993-623b97c39035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "8a248edc-65b8-4b7b-bf74-061514b0a424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "778513ff-f7c7-4966-923f-8e2eb6459780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "778f93e2-50af-4ed2-ac6e-ed63f5509aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "377f5809-273a-4b12-b905-8aec6d05f75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 58
        },
        {
            "id": "0dae4ca4-948b-4c44-9342-ec5581f00d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 59
        },
        {
            "id": "6f8e8495-bc1d-4ebc-a4d7-b89eec7de4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "510519c1-5289-4074-b4c1-83b8165c046b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64
        },
        {
            "id": "50c20f1f-14d1-42d6-8a2b-07a9af654dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "efead2bc-4c75-4ba7-923b-e15ee14f3591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 83
        },
        {
            "id": "88f70843-04e2-4986-ab2e-753fb0452721",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "fa8eeed6-bd11-4182-9c92-db03749b5e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "41addba0-e915-4048-889b-e928aa2c84be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "7526b791-60fe-44d1-b9f4-2d27116ce2b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 88
        },
        {
            "id": "af744979-f979-40bf-96ab-92cc041a30f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "d6525a53-5b70-4893-b97f-22333c70fe6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 90
        },
        {
            "id": "2ecb5c4d-06eb-4207-a486-45f1f4831c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "c9dd3292-7d79-4b91-b9c9-dca6fc3899fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 93
        },
        {
            "id": "021eac8f-4c3d-4b9b-a869-d1d4c67bd000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "316243f4-2561-4120-a903-ecc1ca6da97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "31cb51cf-4eeb-4c6c-b673-ac85df303b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "0a4cc93d-3e8c-4cc2-a7a5-89b4ea401952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "4951621b-b914-4b5d-adf9-5dbc4e66eff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 115
        },
        {
            "id": "4fbcb551-c1dc-40f5-a3af-10dd06e92cd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "67862fb3-6458-49a5-968e-503a386b950b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "5e093124-f602-4e2a-b7e4-fd3531ac8008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 120
        },
        {
            "id": "5946218d-e22a-41b6-a030-43cd6b38043e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "2ecb1e5a-9268-4041-bb7e-9e9995b89f9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 122
        },
        {
            "id": "ae417d79-bb0c-4d1e-b25d-f65c8de672d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 894
        },
        {
            "id": "3e9a3a36-d324-43e5-93fd-24e2a6b1516c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 34
        },
        {
            "id": "52f96aea-e4ad-4d19-86f9-263f609ea6ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 35
        },
        {
            "id": "175b0c93-d8ff-40c3-b9e5-cd63c172973a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "3ff132e7-06b0-44e5-aab3-f12395cc610f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 40
        },
        {
            "id": "9ef3d6a3-4adb-4fd9-ab25-18dc2c12da38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 41
        },
        {
            "id": "4599accc-311b-43d4-8362-9101d9a85fde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "6b465554-6e39-44bc-9129-a5b17787eba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 47
        },
        {
            "id": "6141004a-4a74-419f-91ad-efbd2eba21b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 49
        },
        {
            "id": "eabccfe0-38fe-4561-ad07-de272a1452e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 50
        },
        {
            "id": "598c0b8c-8dc3-4d9c-b22a-d70a5c90042c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 51
        },
        {
            "id": "70a1e5ff-6f6a-4f76-8f4d-fb0b7971388f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 52
        },
        {
            "id": "1a5830ad-1ede-40ec-bc19-aedb2ffe565a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 53
        },
        {
            "id": "465fad92-b28e-4932-ab5f-42384fef68f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 55
        },
        {
            "id": "aebd4dd9-cf56-49c0-955e-4c93f386f94c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 56
        },
        {
            "id": "27e968ee-0b1a-48ef-8926-b376d6e0f5d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 57
        },
        {
            "id": "4ce7f39f-5b39-4d66-9dd5-0d99ccc0f034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 58
        },
        {
            "id": "59842ef1-835f-4883-846c-6fba02a74aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 59
        },
        {
            "id": "4e83897a-0275-46c4-906c-3f1c72009c85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "75745933-c269-4d99-a34a-f52556d48f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "db165f04-7653-4a9d-b6c5-77d5bf7921e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "2ab5abff-14ed-46d7-a2b9-e1550fd4351a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "2a9c057d-93b8-444c-a8e6-10af1ce6b9ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "2d88a5f8-69a2-4a18-bce7-5bb6a7d94d69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "79cb61ad-3aad-4f2e-bf89-3a77e874803d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 88
        },
        {
            "id": "ca9af2c8-f81f-4403-940d-0121f39ff573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "d6e0f908-6f6b-4650-95bf-cae33b198369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 90
        },
        {
            "id": "5ffc2605-ba6a-4890-a2a2-d64f0e0be80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 92
        },
        {
            "id": "c90ef26b-f80a-4967-9e81-c0061a44e24e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 93
        },
        {
            "id": "d82829e0-9791-4f7b-b370-1cf808caa67a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 100
        },
        {
            "id": "6222bcfd-c437-46b9-9029-d229b53e2296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 103
        },
        {
            "id": "5a17f946-e1d7-4d4c-9dad-8789475e70cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 106
        },
        {
            "id": "de00cfe6-9f32-4cdb-a1dc-309ce6753a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 113
        },
        {
            "id": "cd80bec6-c83b-437c-936b-0da01c7b81dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 115
        },
        {
            "id": "0c9704f0-3f37-4795-a29f-379e807bd5fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "1519f69a-4718-4c6a-b9dd-d9d88941a215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 120
        },
        {
            "id": "f5d3e523-b7ae-40e8-9b0d-5b9291e86a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "eecece24-a844-497d-b4ec-32d3ef5e3487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "7e5f27a3-494b-4c4c-998a-4cb5768babf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 894
        },
        {
            "id": "33aa1556-6a12-46b3-80e7-e1297cdb00e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8208
        },
        {
            "id": "ad42aabf-3af4-49b9-b258-1c0c16d31d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 33
        },
        {
            "id": "74b7024c-7717-4f72-891f-2fc692dd0d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "02eea995-3a63-405e-a1c8-cc65b0825962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 35
        },
        {
            "id": "a042b7ab-4559-4501-aad4-b7a2eea114fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "f617284b-e015-4162-9b61-5604180087b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "6486497d-9f99-4230-9060-cca4f9f1d6b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "4a65044b-3500-44ab-8fc0-df04ada0484c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "10695131-fe73-4c3c-8444-4e97d51019e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "412af1de-9577-42e5-a60a-97a1fe3462c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "804d97e7-fecf-437a-bce8-bfb0d35f1756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "7d8c58ec-3aaf-4a16-bb2e-799356d8a86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "38a26fbf-aa41-49f4-81e1-a0e25a87533d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 53
        },
        {
            "id": "b686f7b5-191d-437e-a967-b6853fce34fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "8b85ef3f-8651-47b3-8a5e-5a5206740afb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 56
        },
        {
            "id": "89467667-6159-4fcf-963a-25248814b551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "8ab7566b-185e-443e-befe-d61fe204a894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "fc58f946-6814-4950-b7c4-370e2b94cb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 64
        },
        {
            "id": "7c3aea65-a2e7-46c1-8b59-1afe8a2c7345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "31640ddb-2887-465b-a2ee-b077692c2898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "d5452d99-ce7a-480a-9448-feaa13bc50e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "9ecf4ab5-6d36-40aa-847c-52abc8e8661a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 86
        },
        {
            "id": "7b9d9a78-1890-45f7-ba6a-54db251f6767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "5e5faf6b-07df-4ba9-9d40-e94fecc38077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "1df806d0-5d24-4778-9343-4ebef6c4626f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "567003a6-fc9d-4a9a-861e-282588646291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "2e92e2ff-4869-4011-ad4f-d72e77a40337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "2b04a96c-5fb8-4f33-a6d7-8797b8271f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "d937dffc-0541-49ed-95d0-bda0cd454d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "179f16a9-ba67-416b-a9b0-9a8e9cc808fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "f8577cdc-5213-4a83-8ed9-c5c5293a0a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 105
        },
        {
            "id": "70690cc6-45b6-4c43-af16-1ab9c4681f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "7b42163a-3ed4-4311-a0cc-1fd8d6238578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "1e067615-fb78-4bec-823f-727df86568f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 35
        },
        {
            "id": "768be319-b5d6-41ab-81ee-98eea0e413e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 40
        },
        {
            "id": "4f1eb547-9398-42e1-bd1a-92622d6aef9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 41
        },
        {
            "id": "7607c262-0def-4c8c-8494-608712b958f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "76d6d206-03f4-4be9-915e-55d8ecc8c371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 49
        },
        {
            "id": "14fa74c0-567b-47ea-be7d-7d098fbf7b7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 50
        },
        {
            "id": "7ede9439-5711-479f-bf87-54c02b338c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "1d21d950-c49e-4048-a684-f40848d9900a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "c646b872-6513-4a4a-9751-fcd1a18289a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "686ecab5-7de6-4d0d-974e-e60ca8fe2b0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 57
        },
        {
            "id": "58e85bca-9ac0-4764-b449-6fa3d5291c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 58
        },
        {
            "id": "3bb9d12e-0aa8-481e-95a1-548ddeda77cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 59
        },
        {
            "id": "d1023977-be20-4c36-a211-42f519a28ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 63
        },
        {
            "id": "9c685720-8757-4646-8969-630f5b227060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 74
        },
        {
            "id": "8fa0c2ab-5044-4513-a39f-a7beb3ed40d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "72d63945-1b57-49ba-9eb6-2a055961c229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "03e0aabd-bcd4-4b0a-9082-044eb9f0bcfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "cfec1cde-d392-4e67-98dc-f3f79a35773d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 88
        },
        {
            "id": "20de2b53-be60-47ab-a3e2-12d297b4d783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 89
        },
        {
            "id": "fe4384d0-6886-46e7-9903-6e781d48b5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 90
        },
        {
            "id": "d3ee4c58-844a-4fde-83c6-a33f5feee8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "4a43c935-d415-4756-a2c5-b867182e78bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 93
        },
        {
            "id": "73d7a0cd-e076-4ba9-9cb1-e6d32a0ec3dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "de4697d2-09b4-451c-ba74-186773d0e7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "71f3543d-a3ab-46db-a624-aeb368dc3f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "b6daa68c-dfd1-4b35-9050-a08ddeb73fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 113
        },
        {
            "id": "18d17cf7-f7be-48e2-8ddb-5cbf514369fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 118
        },
        {
            "id": "7c2dbc32-1712-4be2-9fbe-6004954ec146",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "607eff97-b946-471d-a2c6-250c50d02b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 120
        },
        {
            "id": "d8198ec3-6f7d-446f-87fa-3c53918f4dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 121
        },
        {
            "id": "8bd9c106-5784-438e-a0de-7760e6581496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 122
        },
        {
            "id": "c7f2a6ca-5dbd-4576-ab9c-5d2e7db41fcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 894
        },
        {
            "id": "5468efd1-0275-44cc-a9dc-bb10ea02c7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 35
        },
        {
            "id": "2efb1725-70b0-4ec0-bd02-5cd3e430d397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 40
        },
        {
            "id": "1b885e23-7114-4ec2-94bc-fcf5ec2c7131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 41
        },
        {
            "id": "a7159223-3ffe-434d-a5d3-bf78a0ad722b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 47
        },
        {
            "id": "3aafc24d-da9f-4a38-9e35-8b687c66e829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 49
        },
        {
            "id": "6ed63286-c87e-4396-acca-012e250f4bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 50
        },
        {
            "id": "bf029cb9-441a-4d36-9340-966eaedc505e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "48e453f3-7f29-4197-bd9a-746a2688e7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 52
        },
        {
            "id": "f1185cd3-b5b1-491a-936c-47a6683abecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "b304fa7a-88bd-453e-9693-290fc1d01b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 57
        },
        {
            "id": "7a8374cc-6e0d-4a88-a819-64fda44aa85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 58
        },
        {
            "id": "15087268-609e-4ea9-81bf-ec5563c75be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 59
        },
        {
            "id": "83c025d8-68e2-497c-af5d-5858ce53a95c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 63
        },
        {
            "id": "d538701b-1c49-412d-ae34-2925bfc53a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "d7e9cb06-119c-4073-912c-c45693da3d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 83
        },
        {
            "id": "de46fea4-3458-4fac-a736-2a03c9a302c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "8b92927e-6478-478e-ac05-da1c45812f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "2a61ffc3-53e1-4d8b-a4de-533b2119d7f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "71b719b7-737c-4b14-8025-29e3018942a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "c0cf39b2-14cd-430d-b59c-34ae793f913e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "7b15c235-4b22-4a0a-a807-fa9691101e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 92
        },
        {
            "id": "0bdafc9f-e42a-4b98-8688-034f635c8a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 93
        },
        {
            "id": "c3591b4e-5ea7-45aa-95a2-8d90769bf03a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 100
        },
        {
            "id": "d2a5fa50-78b6-4560-9084-1371e6bee015",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 103
        },
        {
            "id": "bcc20b53-d71f-4300-b9f1-719459a3888e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "8a6cf256-ead4-486a-b558-adca1aaa9c42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 113
        },
        {
            "id": "a15a26bd-71a3-4e4e-a58d-7af43b153c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 118
        },
        {
            "id": "2b03d6fa-72fb-441f-a91e-cd1df20e1319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "24009611-594e-4eb9-b36f-9a49c3beee09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 120
        },
        {
            "id": "4227df2c-85c6-469b-8001-5e03d68a88f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 121
        },
        {
            "id": "7926eff1-2fd1-48fa-9e6b-c401a7219853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 122
        },
        {
            "id": "dd2a7f21-45d5-4f01-8363-a9f6c765f238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 894
        },
        {
            "id": "1bd68595-0224-4530-bdb1-b415bb89b0cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 40
        },
        {
            "id": "d7a2f220-9d00-44c3-ad81-776bdd02b527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 41
        },
        {
            "id": "63358d40-9159-42e2-89a4-43159f719b51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "c87e3561-9a4d-4309-9e2b-da0888a389b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "d7280885-9d4b-42c9-9d39-1488ea818164",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "5ab6a103-7db7-4866-a386-f39c307b77fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 49
        },
        {
            "id": "797ff11d-643f-46b1-9d5d-bc77e37aa7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 50
        },
        {
            "id": "e6a77630-e5a2-471b-99c2-090cb5ea4757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "279ddbcc-eef9-4054-a920-bb860a1526d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "4eead6f1-2bd4-4635-9e28-ccb1dd2bf565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 55
        },
        {
            "id": "2930a122-f1fd-4d8f-a86f-64a05279b00f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 57
        },
        {
            "id": "6ccededa-fa5d-46c3-9796-7a85a5949ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "47e419ec-6eea-4605-a3b7-87929e35567c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "94ef56ed-4ecb-4675-900c-5a6c614f707e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 63
        },
        {
            "id": "9d7c25a9-2e28-4ab9-9e6f-fb70199f7983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "d2fa8c5e-1cee-46d4-9b71-ffa8c4dbd12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "46724287-98e8-4e62-b0d5-21907ab20d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "ea633d95-3182-4dc9-8b41-12aab4fa1098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 90
        },
        {
            "id": "180c2592-b0e1-4666-a99a-47018f720981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 92
        },
        {
            "id": "584193bb-0d1c-4664-b978-211612d37628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "81dc04a7-f633-41d7-9280-c67b540fccfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "a4716cfd-8c8b-4d3a-8b7f-6eb0b3413a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 106
        },
        {
            "id": "41213f53-c4cd-423e-b467-51d83e86b730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "00fea3d8-b69d-4200-bdef-d67e0ca7f542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 115
        },
        {
            "id": "38b6c8fa-4c29-4bff-8933-78bf74de3efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 118
        },
        {
            "id": "c0caeac3-d338-4d9a-a1cc-c90937f8a49d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "93254cad-02f8-4229-99ef-cc13829b923a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 120
        },
        {
            "id": "885cdd3b-78f8-451b-9234-0895a7dcf0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 121
        },
        {
            "id": "dd161d64-f7bb-4b31-8385-b71ab19320ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 122
        },
        {
            "id": "0c89007b-42f8-4eae-8800-f6a178eaa319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 894
        },
        {
            "id": "6e908223-5ffd-4737-b628-fe299d0437aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "d5282944-0f24-4b27-8248-ecab7848e7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 34
        },
        {
            "id": "ffae27db-94c7-4620-8d3d-fa8ae740ecf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 35
        },
        {
            "id": "d3278623-de4a-476e-8748-e4f548d3df53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 39
        },
        {
            "id": "daeb1ae4-3255-47a9-bb19-ba3cc4ee22cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 40
        },
        {
            "id": "c11a374e-8ee1-4a5a-bd5c-9360f65d0011",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "1d0b7366-60db-426c-a498-77e17c7eaef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 45
        },
        {
            "id": "022ee682-684f-404c-8022-dee252387098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 47
        },
        {
            "id": "923ac5ed-d185-4a76-acbd-b9fe44f429a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 48
        },
        {
            "id": "10100810-7c98-44df-97c4-b3efd2c9eb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 49
        },
        {
            "id": "f0fcf9b0-a22a-4bc0-9cdf-1b0fc7a0b4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "bb1cf620-9b8d-4208-afee-a0d2401c28d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "8bbd6706-25c5-40fc-8cbc-0f70cc912f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 52
        },
        {
            "id": "f928200e-fe85-4312-a9f2-a4c2c0cbe02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "33fc53d2-79ad-42cf-852c-74f95fcc49cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 54
        },
        {
            "id": "21584093-7e8a-4d51-93f6-05891596f4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "03b32847-97dd-41d4-9959-cc1488fa5fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 56
        },
        {
            "id": "26e31e2a-81dc-41a5-b8e2-9ed0c2eea0b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 57
        },
        {
            "id": "89f48132-26fb-431c-994c-1f2609a15612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "721860dd-89d8-4fec-9b23-49cde1308b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "65ae2a4e-490b-4dff-bde0-a90ff77e1581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "7a6f788b-6717-4961-8f78-467d32c11cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "b28834f7-bb81-4fd6-87bd-18a37b1d87af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "7e8b619f-0b9a-4448-bb21-738066116349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 66
        },
        {
            "id": "09650d30-b12e-4bab-96d3-6300a58af9e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 67
        },
        {
            "id": "76c781a8-63b8-47dd-a8c7-1de7e3ad0895",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 68
        },
        {
            "id": "c9b77994-f8ea-4cd4-ae80-911f01d2adb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 69
        },
        {
            "id": "702eca8c-6543-439a-aaed-46d775c075d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 70
        },
        {
            "id": "68b961c7-dfb6-483b-bdf6-bf60eae99c44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 71
        },
        {
            "id": "aa2b68cb-09b0-4285-827f-ffedd078c152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 72
        },
        {
            "id": "57f80307-ce96-474a-96a3-43f4bd7241ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 73
        },
        {
            "id": "51b484a0-a989-4ccf-a053-449f21a1bb7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 74
        },
        {
            "id": "650ee2ce-0b17-4a91-919b-cc0b48844fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 75
        },
        {
            "id": "c3e62416-9c25-49a7-8f26-bf7ca2d8c188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 76
        },
        {
            "id": "2b64b649-42f8-481e-9a1d-a05856fb995b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 77
        },
        {
            "id": "0f9907de-2c4d-4c0f-a30e-7b370edcbaa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 78
        },
        {
            "id": "36cad4e7-e8e0-42c7-8b96-2508500c6253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 79
        },
        {
            "id": "43fa2d86-82cd-4df4-bad5-73ac6c5ab045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 80
        },
        {
            "id": "0a01b66a-1db7-416a-a0ab-43e3d8d2b2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 81
        },
        {
            "id": "d004f5ad-c91f-4ed9-9188-deb89ec81810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 82
        },
        {
            "id": "777fa51d-cd14-49f2-b207-4800b39b60e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 83
        },
        {
            "id": "e78b1a4a-1ccf-4adc-a1b3-e0b882da7986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "5895a69a-8f3a-4bb6-aca1-43e7f3812f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 85
        },
        {
            "id": "c7ecfaa5-81c4-4243-8bfb-26bb8a74afef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "dff00f44-f54a-4ccc-ae2a-5d3e52b74daf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "69ec1a89-77ea-403a-959f-b3acf90a2073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "c8e7fb68-07c9-4c42-bdb2-024da1c0f46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "f951fb94-bd42-4b1d-ba0b-6f01a8efb555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "d774c328-d802-4abf-bc5e-cc3fb7357675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 92
        },
        {
            "id": "7f004f9e-b457-468f-abcd-23fb5d3ed4ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "ce4c680e-60bc-4749-8444-667dd86563aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 97
        },
        {
            "id": "d97a3ce5-9f4e-4e3a-a17c-56449bc99475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 98
        },
        {
            "id": "d6299b05-9694-4324-bb1f-eabfac9eed77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 99
        },
        {
            "id": "4e9a177d-1128-4185-95c0-079600a38a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 100
        },
        {
            "id": "57302c52-bc11-4afe-b765-2637a464fd07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 101
        },
        {
            "id": "7bf8cadc-d7dc-48aa-94ba-69aa4bcccd9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 102
        },
        {
            "id": "45385496-2c9a-488a-8005-21ed885630c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 103
        },
        {
            "id": "4af919a7-5218-4c24-a58e-de7b0bbc76ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 104
        },
        {
            "id": "071fc56c-825b-43e0-82a1-e20d6b1f0859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "d144358f-bd1a-4e84-b1e8-3cd3fe078db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "b675685e-5e75-4c36-8c05-f15237c8e8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 107
        },
        {
            "id": "aab0a578-63c2-4d67-a85b-00b591b7c1c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 108
        },
        {
            "id": "b663eae0-88ac-4d3b-a9e3-061fb1e2e5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 109
        },
        {
            "id": "6804f820-f89d-4dd3-9027-7ae5504d9a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 110
        },
        {
            "id": "b571b3f8-a180-406d-b5c8-f448037933ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 111
        },
        {
            "id": "3c2f3455-4a3a-4987-a3e6-f9143d7659cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 112
        },
        {
            "id": "08840d00-b74f-44ed-ac0b-bc7698b13aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 113
        },
        {
            "id": "2c1c6712-5990-4128-8a41-9e4df3880fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 114
        },
        {
            "id": "6faa38e9-dc0f-4614-aaa2-29f82e84f5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 115
        },
        {
            "id": "9fe07047-9e99-4737-9218-07ecff029d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 116
        },
        {
            "id": "0952d206-d716-4642-add3-edc0be0b8be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 117
        },
        {
            "id": "4ac39519-ee9b-4384-a664-d983ac15de36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "2a0d0fcf-a1d7-4cf6-a7d5-16373bcc0c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "2879a046-8e1b-4c62-b4d0-2f6adab0f638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "3af923d0-64a6-4366-8e38-55167b2342a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 121
        },
        {
            "id": "d49ff324-bf3f-45c9-8261-53a2ecea1ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 122
        },
        {
            "id": "e6cb4617-d5e4-413d-9c62-ddb67c066c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 173
        },
        {
            "id": "58512835-e35e-4410-b22e-d8ef389d48d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 894
        },
        {
            "id": "78642627-bdb2-45d5-abad-d6f9cfe60709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8208
        },
        {
            "id": "672224bd-3273-4dad-a28f-47a552cbf59c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 33
        },
        {
            "id": "699447df-a584-4c0c-9df9-fc2d3945fdb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 34
        },
        {
            "id": "ec4238e6-9cb6-4d51-b183-6bcdd9c53314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 35
        },
        {
            "id": "406e15f6-6172-448c-9216-9956ce76502c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 39
        },
        {
            "id": "621daeb0-70b9-4928-a353-8617d2f67ce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "c7f16a1f-7767-4ac8-bb15-c973ef9e26e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 41
        },
        {
            "id": "a53eee86-b94a-4058-b1b7-b772d3fc3879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "3b86379a-451d-42ea-988f-0f4d5ed4ddbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 49
        },
        {
            "id": "761a2c02-a042-4e57-ae78-77e5eb4e9642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 50
        },
        {
            "id": "4871cde7-6fb3-4979-928a-1ffb5d9c6cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "1f3ba6b4-eab9-400a-ab80-cd9982a93bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "65360176-28ea-438b-b2e3-5ea6925cc797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "86d778c6-091d-4353-8229-5738f01f99ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "db86c5af-f943-481f-a000-325dc8160c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "2d751c16-0641-4c28-b27b-21834e6fcc62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "e885faff-812d-4390-8d11-71ab4abe7ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "e0763e5c-2a93-4f34-8b19-dde5e47d7eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "7fa4c1a7-5893-4084-b0c4-b0b31f04030b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "01c5bc80-d942-4656-a8ea-85735608178c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 64
        },
        {
            "id": "e80d4e86-511a-474d-b166-4667d6b3a961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "891bd54d-a5b4-43a4-ba10-f736590e83bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "2dbb32ee-d63e-449a-94e3-89499336a05c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "af1e8c83-0b4a-41b6-95e1-79d367c9ccdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "9bda5e1b-1550-474a-ab89-ca7f4697c5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "04fdebb5-a3cd-424c-b048-6f37111e060b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 88
        },
        {
            "id": "fa74afd8-9de9-4bfb-be25-0a594bbab620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "b5b57f30-8d4f-4282-b79a-8c94d8e282b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 90
        },
        {
            "id": "b098c801-22fa-464a-a7bf-5299a4a92a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 92
        },
        {
            "id": "088a8b7a-52e7-49cc-9f30-285c699e80b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "f516dc5a-a8ed-48a5-8000-015b2ea4415e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "50cb8fb5-561c-45f7-8fe0-81788c9deec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "487a2314-96ca-494f-9f57-bf1870b963ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 105
        },
        {
            "id": "a6e79425-1baf-4c1c-86aa-e62d89f3d2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "d0909814-37b8-4bc8-a892-84f8457ea9b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "c1508fe9-7219-4e90-b625-516b67007e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 115
        },
        {
            "id": "5fa81b2e-c4e7-4bb8-b076-f74c540535b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "9d2b9ff3-c20c-438f-9998-e287d7744974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "c03c8b1f-0d54-4522-aaf5-d1cbf1b95571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 120
        },
        {
            "id": "bcd5c2cc-5ef9-426f-be87-5e29c587870c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "3b7e58d4-46b6-4d45-b852-17a466e65b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 122
        },
        {
            "id": "782481cc-8bad-4d8f-b1f7-22e6e50d50ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 894
        },
        {
            "id": "f8cbc6b1-ba30-435e-b683-0df511aed7c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 33
        },
        {
            "id": "cc48a21f-453a-46c8-9791-d3b88ae6682a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 34
        },
        {
            "id": "dd0015e9-6112-4b7a-b5c1-f3a1508fa9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 35
        },
        {
            "id": "2d6a5fd9-8579-4495-a389-1c4a47dc6412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "bc746f16-58c8-451a-b921-24abc8f5fece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 40
        },
        {
            "id": "0cfa7475-2c06-4ee8-ac09-d4e735782e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 41
        },
        {
            "id": "e9db9a98-6527-40d1-8924-dd60a38aba4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 47
        },
        {
            "id": "27c4bfdd-5f27-4178-bd58-d5c071829ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 48
        },
        {
            "id": "af7d4f18-6d2b-4f6e-9b8e-1c794a1202b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "9697fc73-50b3-4940-903f-b59a4679b6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 50
        },
        {
            "id": "13a126d8-dd28-4df4-be4f-350b17e196a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 51
        },
        {
            "id": "3567e8d6-5349-4310-8911-781fd512d0b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 52
        },
        {
            "id": "ae33e7b4-2bc6-42cb-b943-698f3db51bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 53
        },
        {
            "id": "ea06ecd4-7a4f-48f2-b313-ec83ece74957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 54
        },
        {
            "id": "b4e11b2e-d414-4d17-84c1-e54479fd7540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 55
        },
        {
            "id": "b13637c0-7eae-4280-850a-d298daad7d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 56
        },
        {
            "id": "e4e9ffb1-fc1d-4fed-b56c-cbdf7f53eb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 57
        },
        {
            "id": "ca783da6-2f6f-402b-b21b-203496b0ea05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 58
        },
        {
            "id": "90432899-567b-4f01-8cb7-317751d6c5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 59
        },
        {
            "id": "92ee9616-448c-477a-9f74-f4d589951d3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "1bd1e98b-4700-4cca-bc82-45933a8f854f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "c4c144da-c0cb-407c-b19d-6548b82840c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "55ac8c00-432d-48f0-a0cf-5045ffb9cda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 66
        },
        {
            "id": "4eeb8cc7-6126-43a9-a9a8-648af52b095b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 67
        },
        {
            "id": "3e287e7e-9797-46be-962b-ce3cce6e1cf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 68
        },
        {
            "id": "f1d2b846-5e62-4306-a107-c796f9a301b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 69
        },
        {
            "id": "697f4ab3-2091-401d-b136-e26a8c99e122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 70
        },
        {
            "id": "2a1228dd-5f39-4398-b335-01448c06114f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 71
        },
        {
            "id": "780d0844-b697-47d9-a3cf-42de85574caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 72
        },
        {
            "id": "ca62520e-77c7-4f15-a200-5577343253f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 73
        },
        {
            "id": "1207fcbb-191d-48c3-89cf-2bcc5759e153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "404766f7-7262-483f-8238-6f7b158b1478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 75
        },
        {
            "id": "7f26780a-b687-4c45-a9fe-4a099ebbc390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 76
        },
        {
            "id": "85ab55e4-0ee1-4c90-911f-56b92506e390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 77
        },
        {
            "id": "b86b5686-4a78-46e7-90ce-81fd093e0044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 78
        },
        {
            "id": "439bc866-b154-4fff-bc2c-18e23a76ca25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 79
        },
        {
            "id": "155dc16f-eb9c-424b-a97e-5cc66e7cd491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 80
        },
        {
            "id": "9da126b3-0ad5-421f-8ecd-ad7be87908be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 81
        },
        {
            "id": "48c4d0c3-dfed-4828-97ca-bab4917c3b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 82
        },
        {
            "id": "c19c19d8-c317-4f79-90cd-b337b0ccf3c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 83
        },
        {
            "id": "017e9835-c154-4cf6-ad43-6392c0954e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "954c2981-efde-44fb-b34e-d7c946322768",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 85
        },
        {
            "id": "3c8a4a9a-dcbe-4add-ac86-5741168da53f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "f848754d-3d4c-4344-b6a6-b744ef7a547a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "ee4fae40-5b7d-441f-a04d-7140adc7d373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "5ac791b0-a85c-42fb-a958-fa3e36912b38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "40eb7d61-a4b7-41e3-931c-9bdca7bbeb9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 90
        },
        {
            "id": "1b8e70c8-497b-47d4-a47c-7b0d2fbfaf37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 92
        },
        {
            "id": "d6557fa7-f483-46c6-93fa-5404deed162e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 93
        },
        {
            "id": "b6f6b87d-c523-4731-b067-c5e93eed68d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 97
        },
        {
            "id": "8933ebf7-01d8-4e6e-84a1-ccd7afe2154e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 98
        },
        {
            "id": "adcc68bb-cea1-4596-b388-eef9f557a9dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 99
        },
        {
            "id": "0d84577c-9181-4f78-927a-a704a7289669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 100
        },
        {
            "id": "953bb122-89d0-44be-87fb-a0407649fbc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 101
        },
        {
            "id": "ec7d089d-7493-443d-ab3e-c48bc41cd142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 102
        },
        {
            "id": "74a29945-8865-4a46-9713-b48333cc8697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 103
        },
        {
            "id": "c7fddac3-7aeb-412c-8253-c3c289b844c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 104
        },
        {
            "id": "c8da8413-d476-4d10-8cef-02501ae683c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 105
        },
        {
            "id": "15626f7b-ff1a-4223-a0b3-e15c0c3c3673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "9ff4e26f-13b5-48fb-83f8-51a22e64eed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 107
        },
        {
            "id": "98aa9bbd-bf0a-40c2-adb9-a7b6aee7618e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 108
        },
        {
            "id": "bae1f82d-ce21-46c6-b405-b774db0d37b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 109
        },
        {
            "id": "78fbd45a-b12c-49d1-91e7-f06d71ccc56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 110
        },
        {
            "id": "661e31e9-f32b-4407-84dd-5e11bd9747d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 111
        },
        {
            "id": "ae53101a-b6d2-4a97-86ef-8df138256892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 112
        },
        {
            "id": "ff712900-3caf-48da-acfb-d835642dff53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 113
        },
        {
            "id": "7e0bdf66-029a-4781-92b6-6f5de9feb47e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 114
        },
        {
            "id": "ea8f197f-55b9-410e-891d-70d09530b7fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 115
        },
        {
            "id": "711e76fc-a366-4dd0-80cd-b09dde8153e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "95492f65-35b9-47c2-921e-abd3a643c018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 117
        },
        {
            "id": "d32d2975-ec98-48d3-83eb-0fd1b6b1de17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "73baaf18-bb16-4af5-a365-21c8517d5d99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "c62bec46-e056-48bd-8676-628bd7ab191d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "463ff457-5108-444a-9cda-a65e380111d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "49c7c4ac-1cf5-487b-8a2c-61fcc8950daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 122
        },
        {
            "id": "e400a8d5-24ce-42ed-9944-cfc2372b6851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 894
        },
        {
            "id": "0176ef02-cea5-400d-bc55-db8ef1eb7d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 40
        },
        {
            "id": "bc0131e1-d88a-4f90-ab46-987e319d8f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 41
        },
        {
            "id": "a2908c92-83e0-452b-9f35-d918481ce8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "b780b358-e066-4435-8def-5889ecff0148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "3f849962-e049-4337-a2bb-ebe84d54de06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "c6ba2b86-9c5e-4cd1-b2e1-fbf5be8cded0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 47
        },
        {
            "id": "7f1c9222-702d-4225-966a-43e1e59fbb52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 49
        },
        {
            "id": "7c44216f-dde0-4396-a4a9-941c3b87c824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 50
        },
        {
            "id": "22d81da3-2652-4e1f-96e4-0e4dc5db389f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "c2f109d8-974d-4c09-a848-f2c9fd3ad78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "0317c64a-b23d-4fc3-bd0d-ccb432180223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 55
        },
        {
            "id": "68e7228a-407b-43ad-8da9-a8effa06776a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 56
        },
        {
            "id": "270c4d88-4ecf-4b63-896a-9fb629959c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 57
        },
        {
            "id": "cd53325f-efa9-486f-ba5f-4f8302bc81b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "2d5b93eb-e849-4215-9661-7c091e3ab168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "a949b1c5-884d-40f3-9eb7-be4dbc2a8143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 63
        },
        {
            "id": "d187bbda-88a8-46a9-99ef-ccc9d1340a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "e5da2894-582c-4924-9b98-23884c51a227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "6d37436e-9f33-421f-9a91-6a2601232cd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 87
        },
        {
            "id": "d0dfec75-b6b8-4d42-826b-17da6599949d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 90
        },
        {
            "id": "8370da0f-744a-4e1f-b13b-32cf5143e3db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 92
        },
        {
            "id": "dcae473b-b6ca-478c-bff6-23dd4a6af123",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "0f37b8c5-f0c2-4b15-9b9e-79eacf07458a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "82bc8477-a7e1-412b-bf2d-f05c8ec442ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 106
        },
        {
            "id": "5070c8ed-ab75-492b-84b4-17367c8ae320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "509ea887-a3f3-4f8a-8527-0cf6ea488d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "e804a753-a330-4880-aeed-cc9e05b34456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "f61d99fb-9494-46a5-b069-27e82a7220ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "956048bd-3ef1-4b35-b8f8-d1f151d61162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "02f2abef-de0d-429a-8de6-8d6eb50fb3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "56790ac5-a9f5-4fd1-a02b-70e1a99d7c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "88d8e35f-3111-494e-abcb-69c84564f67d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "ae5e963a-11ba-4c82-9dc6-e931467cfa85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "4ff4d39e-b124-4197-9bd4-d631abe047e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8208
        },
        {
            "id": "a4e1c597-0302-4fa4-b0d2-7643e6d611b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "df7879e6-1ec0-4ad8-a8b1-d2cde8bbda55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 34
        },
        {
            "id": "73fe15c1-5c9d-418c-8513-9a9911a6903e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 35
        },
        {
            "id": "de4c0c1a-230c-4b2d-a3b1-b5d23130bd40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 39
        },
        {
            "id": "097201d8-def8-428b-b379-b2e65be94796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "883e400d-d6e1-480b-829f-14a63931bb7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "ec81c68f-b687-43fb-96ad-3ddfaa7ebc68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "cc233d54-56bd-4cd8-ac2d-a28553f339f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "dc4bb0ec-863e-435c-b459-bf438e9e9497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 48
        },
        {
            "id": "c00ebb3d-268f-4523-b270-bbc5d36d20c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 49
        },
        {
            "id": "c464f957-c9f5-4b78-8bb8-b011c3bb735c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "c4d6f743-1fe7-46a2-a621-e2b99b42a80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "8a7de786-0819-4102-a6ea-7f1dfa7470ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "b2a19b46-abc3-4cc4-a96c-75f110859030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "bbe548dc-b2c4-4ccc-a245-392c9d59a55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 54
        },
        {
            "id": "864b39d7-5a06-4368-abf0-beb6edc6be27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 55
        },
        {
            "id": "3d1f2422-b758-4a30-acdd-511f3ab662d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "cf2f6999-819f-42d0-bbd9-44cf45f51de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 57
        },
        {
            "id": "20a531ac-2393-48d3-bfc9-1f463d9aa971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "c57edd83-ce32-4b4c-b550-894a41a5d2b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "26089816-a211-4d6b-bf1b-b6214b95433b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "2a94e241-54fe-4614-815c-c89f2d7fa88a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 64
        },
        {
            "id": "ac6ef187-91c1-4579-82cc-4d38a526632d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "08b26cb5-8860-4ba8-9502-198f19e887e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 66
        },
        {
            "id": "b747b1e1-26c7-4344-a686-0e177293e5dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 67
        },
        {
            "id": "15c7e1e5-3079-48c2-89f0-9efb182b3125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 68
        },
        {
            "id": "98647ae1-fdca-4fcc-bc29-52f9c4a3800a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 69
        },
        {
            "id": "67844be7-70ff-41dd-aef6-902b21a3eab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 70
        },
        {
            "id": "8aa6c01e-43a2-4182-894b-fc6688a64ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 71
        },
        {
            "id": "db431c6d-2f78-4102-aa1f-532cb0ac7df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 72
        },
        {
            "id": "e0f4f9aa-be72-41f8-b5bc-623a0fad7745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 73
        },
        {
            "id": "2feef6ae-0196-4f84-a611-432ae92a1185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "1126f2b4-b5f8-45a4-afb2-76ca4a870b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 75
        },
        {
            "id": "b3cc4ccc-5bf8-44be-804f-54b956bd49ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 76
        },
        {
            "id": "e73036c8-98f8-4e80-b7bf-fab28e693a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 77
        },
        {
            "id": "ea7eb757-58d8-4fdb-a847-0365b2757634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 78
        },
        {
            "id": "17af4757-8d9d-48de-9baa-9acb3e6a3eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 79
        },
        {
            "id": "3ac99d22-73fc-4404-864d-c01f82b2425c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 80
        },
        {
            "id": "0430ffbb-5935-479e-89b6-749a4e6db99f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 81
        },
        {
            "id": "640c257c-6348-4f75-ae77-2b75672d9a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 82
        },
        {
            "id": "ded92c3d-8e75-48f0-8ed7-991d036a006d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "54f98c2d-a8e0-4223-8fb1-6a9d97fce66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "e9dc6da4-4e60-4089-8ce7-bda35bbfe4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 85
        },
        {
            "id": "3e31656c-5b42-46d2-b910-323bee67b5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "8a3bd830-1597-4632-b505-5384c9359cbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "13e46899-8d7d-45be-a78b-c036ee680d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "1a497d27-79ed-4dd5-a3ce-fb34110de7b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "7ca9d98f-e06b-4b9f-bb3a-e49a0d9afc11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "56d807d9-ce92-42a1-8149-d67e5f29c56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 92
        },
        {
            "id": "d4d31d78-13dc-4d15-8ad6-e8c87e36d55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "f28d3337-4d76-4a39-a026-c5c9f6303e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 97
        },
        {
            "id": "ebc469ec-8698-46f2-89df-39d9efa551bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 98
        },
        {
            "id": "92b8a010-4c25-40b8-90b1-b044446365c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 99
        },
        {
            "id": "c595374d-4a33-4e99-875a-18bd3906282f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "ffbecfa8-3000-49fd-ac44-1223b60eab63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 101
        },
        {
            "id": "9b117a27-43d8-4562-bb6e-67ef2f5df105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 102
        },
        {
            "id": "797d6b2d-9075-4ad2-8cd6-33f0b8973c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "92162595-466c-4eda-8fe1-80f2aeda409b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 104
        },
        {
            "id": "93e9f0e7-c6a1-4272-a69e-4fa6ad66d8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "80ac66d3-9328-4e68-8b96-770ba31c4404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "96f0a67a-dccf-472a-a184-eb8155f2936e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 107
        },
        {
            "id": "f6a20e99-0bb6-4344-9d39-87e827e876a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 108
        },
        {
            "id": "a7b70913-2a37-43bf-b296-82e5afe7132e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 109
        },
        {
            "id": "3b4d08dd-9fcd-4999-877d-a447e57b47db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 110
        },
        {
            "id": "687016db-3b60-4f75-948f-bc3fb2c0e3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 111
        },
        {
            "id": "ed5e3a13-55f6-4e3b-896a-10f9dd1956f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 112
        },
        {
            "id": "d2e0117c-dfc4-4b51-8ca8-2614c59ee93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "4681d1d9-2725-4097-b15d-8c91f0bc8e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 114
        },
        {
            "id": "6d959a9b-6fba-4154-8bb5-bbc0b3f679c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "89bcbab0-c3c5-4b8a-9dae-b6b06c958020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 116
        },
        {
            "id": "c54d1f85-a2c9-4582-b0a5-a403451c6dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 117
        },
        {
            "id": "6f15a4d3-da5b-4f94-8096-7f3a3744cded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "411017a7-f9ba-4bbd-829c-1554a45056de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "5e54ca62-bb2d-40ef-9519-67c7a4811eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "05d81c0b-001c-4a3f-9daa-209239041311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "4ec2e6bd-e761-4db2-8fb8-d7d11b1f4c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "499dbf8d-c711-4387-baeb-1b163ee910e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 173
        },
        {
            "id": "dcf167b8-c378-4b99-989e-481d61c5924b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 894
        },
        {
            "id": "a8b0112e-7343-499a-9fa5-d4074595ac5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8208
        },
        {
            "id": "41fae664-f175-4004-a2f4-e6aaa3632637",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 35
        },
        {
            "id": "365840c9-9911-4c00-819f-69f4dbaeaaf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 40
        },
        {
            "id": "1a483c72-ab20-4c47-bf59-480b8da69ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 41
        },
        {
            "id": "e98fcedc-b4aa-4805-a9f8-b1f43d91477e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "b9ace352-6125-41f6-b750-cf0b5934fa3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "379e584b-9c39-443b-b561-17ef6f50f38f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "6a391814-d271-4698-8efd-6b56bda04256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "a9b61e42-ae19-448c-9ffa-6de7ecd7cdd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "81fd0fae-a897-42c1-bd51-a8168e95d797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "0f5746a6-949f-438a-8c2c-b4e7ec61b10a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "1b9bcb57-019c-4427-a204-b308308a622a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 53
        },
        {
            "id": "be952e8d-d79c-4c47-9581-6f4dbf9f9129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 55
        },
        {
            "id": "4b8afd08-6d93-4c68-9a0b-ef1ce1db9eb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "fa6ae91d-0d47-40dc-a50f-5dbe214ade69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 57
        },
        {
            "id": "1114880e-3692-4c0e-b362-842ca02caa4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "ed4782a5-afc7-4f9d-a831-e8787ae1a6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "976de801-11d5-4a08-a698-fc907ef8be61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "5c29b0b1-7dd4-48dd-be92-48cc0b99a3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "8c05f625-b817-4899-ac21-393d2d8c7857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 83
        },
        {
            "id": "266ae3e9-bb19-458a-9bb1-7e97ba83551d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 84
        },
        {
            "id": "778387e5-6785-4bbd-b532-417146498c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 86
        },
        {
            "id": "356d15f3-18d6-4b58-ba68-743c0aab2d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 87
        },
        {
            "id": "208a8bc3-8c3d-4606-8cf3-c15142a48328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 88
        },
        {
            "id": "6a281ddb-d007-48c4-8e55-291f68704dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 89
        },
        {
            "id": "b2d285c1-c9ac-4f94-ad56-cf646e93c2e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "0dba9cdf-6c43-4ddc-83bb-110151852f09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 92
        },
        {
            "id": "26b7f727-33db-4a81-9864-7bef7f12a120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 93
        },
        {
            "id": "874bdf73-dc7f-445b-a8d0-e34078d456b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "c63bfc5d-8b1b-4080-9547-03798f054500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "b680430c-7543-408f-9034-0cdd8248f3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 106
        },
        {
            "id": "3e031bba-3b19-4039-b6d6-1de63fab8b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "0f63099a-db72-431f-983e-8482707e1437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "d6efd276-6b2f-47a7-9a3f-137576f918a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 118
        },
        {
            "id": "26b49170-1f54-4e12-b2c2-c8c52003a5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "b4a45541-ef15-4d36-97a6-5063bf69663a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 120
        },
        {
            "id": "9b1c7690-3770-4036-bd05-11876f69ba06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "cb790bb6-1ede-454e-b9c3-c2c3c12c9007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 122
        },
        {
            "id": "7adbdbee-7a30-4885-a510-75bcaaf3e37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "1c6d8584-0289-4e4a-80a0-281423370241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 35
        },
        {
            "id": "7e21af6c-d51b-4ad4-9886-070a54126761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 40
        },
        {
            "id": "3f830f9a-92de-4d46-a7a0-e638f3a715bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 41
        },
        {
            "id": "d854764e-7c1e-45a7-b225-d77e0e1a4e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "2c4550ec-b373-4668-a41a-daf428e44d10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 49
        },
        {
            "id": "cfaff78e-b8c3-4147-9ff1-217d3cac307d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "ba43e51c-e448-43a4-af64-b3e6502e4db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "697484bb-7ef0-47fb-a198-66daf5137f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "7f533a8c-00bf-46c9-b394-414c34151964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 55
        },
        {
            "id": "9b1fa20a-6421-4966-a2c8-972bcc22b8b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 57
        },
        {
            "id": "8ca42309-dbcf-4ed5-a0f6-27aa6c53cd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "840567d3-d078-4cab-a036-f125af265f8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "9a5c45f7-1c61-47a1-b8bc-035b4ea72d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 63
        },
        {
            "id": "6c9f40fd-c740-4041-851a-de0507513c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "0a4abb54-3b5b-4868-bdcd-8af0d1030645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 83
        },
        {
            "id": "2cb1baac-bc42-4558-ab5d-c481c23960ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "c6ed10c1-ef0e-4a5f-96cb-e6ab3e45b91e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 87
        },
        {
            "id": "5747cf83-8186-48da-bdbd-9693dafd612f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 88
        },
        {
            "id": "a0a0df8b-17b0-4527-9ffe-9b98de73f0c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 89
        },
        {
            "id": "38f49dab-94ef-4b33-965a-2a392c3952f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 90
        },
        {
            "id": "98ffa6a6-bbaf-4790-a20d-b4fc7db65348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 92
        },
        {
            "id": "06b174ce-9493-4001-ba43-f968fd67f43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 93
        },
        {
            "id": "fa33b684-b5d1-43f3-ac01-9a79e308c564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "354d714c-229d-42c4-9eb1-8824b23ea442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "a141cc20-8362-4d6e-9ebb-7f97b00e8977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "dc3d5c8e-939a-4f3b-b1f8-120df8d71876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "a958a70f-8346-4552-b406-78e6206906ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 118
        },
        {
            "id": "5ac17f58-3878-4fbf-81fd-df4dcf2d35c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "539249f3-8650-4a05-9710-3948bdb1c81d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "b7681634-22ed-42ca-b59c-2e46e8b5aad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "9f76e4d4-ea8d-4797-ad65-c33447e9392b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 122
        },
        {
            "id": "bc264c1c-2e06-47de-9adf-687eaecf6bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "3f820054-799e-48e1-882d-5427d1a6d058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 33
        },
        {
            "id": "986d081e-6db3-4040-81e6-9c145c104481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 34
        },
        {
            "id": "c58dba74-255f-4849-adc6-0a270646a421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 35
        },
        {
            "id": "c9e6dc8a-a3ae-4d6a-aee2-3bb3b7c8e710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 39
        },
        {
            "id": "9659b325-b063-468a-832d-a635cc2e6e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 40
        },
        {
            "id": "cf083f95-04d9-4b4d-8ffd-98858703e24f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 41
        },
        {
            "id": "7084b410-92f7-43f0-afd8-70d1a14d1f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 47
        },
        {
            "id": "70c502ab-b4b6-4d1f-a26d-de8e550dcc2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "5c7fe895-ed1f-4512-b823-611325bbb145",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 50
        },
        {
            "id": "98cd9363-04f3-4468-af41-161fff757263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "4c7c035a-bc3f-4fad-b907-156f6d6dd3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "fc2bbf58-a33c-4480-bd54-635f8d47fff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "3563ea53-4574-445b-8d36-f11605d8c1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 55
        },
        {
            "id": "25feee62-c183-4e88-8e7b-75b637af2339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "30ee3dbf-2b05-4092-a8a1-a13efd9b13aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "b15f73a9-8bb8-4865-9304-b736e65e9fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "a5da6263-2016-4e16-80b9-1733f9ee59d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "cce80a24-ed9d-4094-945b-4e4b4b32492c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "ef34b410-fcd5-47a2-93d4-7c5930dfe5e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 64
        },
        {
            "id": "61801008-bbcf-4f89-b5ba-369dd74e29dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "7630d636-8a03-479a-84c8-82d6e9fe66c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 83
        },
        {
            "id": "2595be58-85e3-42cb-b9c7-b251cd5b30f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "53b73f1f-5d29-43ae-8ba2-2fc5caf00044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 86
        },
        {
            "id": "54c15233-3e10-477b-baf3-deff301546f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "48634159-8281-45b3-a862-437cfe14cba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 88
        },
        {
            "id": "924a21e8-b205-4a0b-a401-9a81340f9c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 89
        },
        {
            "id": "8eee7338-f50b-4d4e-8a30-009b9f8ab37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 90
        },
        {
            "id": "858f0299-2d0a-4938-a09d-f1c2e8129791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 92
        },
        {
            "id": "1e395a1f-3aa2-49bd-9621-461e39866bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 93
        },
        {
            "id": "5aa1aad1-092c-43c7-960c-1831c84e19b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 97
        },
        {
            "id": "d2e51210-2412-4d64-b79a-b36bdce351dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "25ec9c8d-8fc1-4401-9f47-1d1252ebe27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "4f598483-d138-4d5d-9d83-4eae04e46a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "ade233ca-df7f-428f-84f6-252acc6fdaad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 105
        },
        {
            "id": "34e44e11-3c70-4d3d-8c8d-e004314e90c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "953e3384-960c-4b0d-85e3-4820de445338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "6ce19191-0495-4939-a6be-2ce98d3615ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "e9edb34a-ec3d-40dd-8971-7c8bae59f1e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 114
        },
        {
            "id": "ca1d2af3-e47d-4422-a68e-5d8aed24c0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 115
        },
        {
            "id": "753f652f-826c-40f1-a836-7d7cb8d6793b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "a956ea77-fda0-403f-81a1-20a26eab8084",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "4e6459a9-1725-4b70-a73d-369190dac97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "19e97c87-a129-4206-b5e0-ba9825b18c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 120
        },
        {
            "id": "e4487c6c-1550-47d9-b95f-5fc2a15f80ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "0524e367-3e97-440d-a460-849c96194d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 122
        },
        {
            "id": "c90ea833-d793-467c-868c-25f2dd9e0f2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 894
        },
        {
            "id": "e4fb2f5d-45d5-458f-99fe-2498b27fee7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 35
        },
        {
            "id": "7429d490-d4fb-496c-9ac5-beeb049d0576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 40
        },
        {
            "id": "28d9e6a2-bfd7-473b-abe4-cef86e20f983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 41
        },
        {
            "id": "8f2dfba4-9fba-4788-bc7b-cbd3dc68306f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "bc08906a-1ebf-4baf-858d-ee611a1fce09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 49
        },
        {
            "id": "9f8a51b5-bb89-47cf-a2a2-6cfe1867e8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 50
        },
        {
            "id": "beb7a68e-1c44-4919-afa7-cb85a6619cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "b8571e9f-217e-4645-abfa-1214db8b5ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 52
        },
        {
            "id": "5ef971bb-0629-4cc7-9be4-07d7c38e2a6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 55
        },
        {
            "id": "cf80b5e8-de10-4870-83d9-8e043e6e9c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "d0481277-bb53-4fc7-b519-e7cd869c614f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "ff41648c-ec60-472f-a87c-b6e418e14b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "6c971038-23ea-44ea-ac68-3feb545e1ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 63
        },
        {
            "id": "d19bc9e3-1170-486c-ae5e-2f31a0737447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "202992e6-73d9-42ff-bfb8-70b4cea7f963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 83
        },
        {
            "id": "9d4aba74-a154-4b6d-9e0d-26ee9ead7e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 84
        },
        {
            "id": "2ae0040d-2219-4556-823a-3908f6e68281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 87
        },
        {
            "id": "86c90ad4-6c1a-48a4-944b-e1758d7fe917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "03f835f2-aba2-416a-a82a-3600bea29e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 89
        },
        {
            "id": "77336e25-04e8-4c3d-8543-3d075dc7e62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 90
        },
        {
            "id": "46649312-1e8d-42d0-960a-049d5d6b0441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 92
        },
        {
            "id": "2a82d7b0-7132-4812-9bfb-bdda2c368a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 93
        },
        {
            "id": "e72436cd-530c-4bce-b55f-d7aed8755209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "c4f52072-c528-4d3c-869d-46d6f78c8b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "e78201c7-01d0-40b7-90f1-ed5acb606c87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "6af8e4fd-6290-48b0-88de-14b053c0f8ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "922f1c9a-07c3-47e9-bc4f-3d3536074fda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "929ad45b-f266-4f86-a734-a14ff42ce2a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "2a5fc746-d995-4f64-96e4-401dcc8006e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "cac1adf4-43f5-41d3-aa49-db91520e3478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "10049ed9-04e7-46b7-a843-15c8cbadf41c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "8854f001-13d2-4058-83e6-2106bb982426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "d8df764f-72e6-4b75-941e-bc36431f6f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 33
        },
        {
            "id": "14016a6c-28aa-4949-86c4-0eed69d49c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 35
        },
        {
            "id": "32f46c3a-4478-49d4-8976-5dcf173a6a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 40
        },
        {
            "id": "7eaf3969-da67-4528-bb2a-bb8659e6b026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 41
        },
        {
            "id": "c01375a2-130e-4f34-9b5e-3c03d61c899c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "f274c123-2a7e-4119-ab35-82a6bc6867ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 47
        },
        {
            "id": "f3373152-d58f-4382-80b6-609c5ac32c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 49
        },
        {
            "id": "f20f76f9-ec0d-4824-af9f-0993fba2faf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 50
        },
        {
            "id": "ea42b5f4-6e24-43cd-ab69-78c74d2d3ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 51
        },
        {
            "id": "ef5ae16e-2c34-4826-971e-9f134fad44ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "8fcb1cd0-717a-4570-ad54-a66c222cb01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 53
        },
        {
            "id": "c92a9c7e-823a-4f02-b44d-1f617e803507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 55
        },
        {
            "id": "32e3872d-8293-47dc-b67e-b71ed86b2a05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 56
        },
        {
            "id": "d3ecd5fe-2262-4668-ae3e-f5f8dda48a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "49de5738-0a13-4fcf-bf40-53d539d4821d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 58
        },
        {
            "id": "9068dcf1-8a58-428d-a467-ce64656b4dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 59
        },
        {
            "id": "505b157a-8ae2-432a-aafb-1d1218c3fc3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 63
        },
        {
            "id": "e99912b3-4652-46f8-b129-818ed438208f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 64
        },
        {
            "id": "114f4811-fa8c-4d98-b1d7-1093d57b855c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 74
        },
        {
            "id": "fa187b37-d5df-4543-8919-350318034105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 83
        },
        {
            "id": "769b1f6a-9b78-4ef3-9faf-0872cd8717bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 84
        },
        {
            "id": "50e75783-035b-4936-8687-d965eebfdaaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 86
        },
        {
            "id": "6e4b7e72-702d-4734-b98d-f3d8c12788a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 87
        },
        {
            "id": "e8240db0-9edc-4b71-af5d-bc3821588b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 88
        },
        {
            "id": "b1be08fd-4437-4c85-a3e8-e2def8f6c0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 89
        },
        {
            "id": "202fd99c-93d1-4d64-8a84-07c25dfb3543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 90
        },
        {
            "id": "5649eb5d-16bf-44d3-81c2-cb380988ec21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 92
        },
        {
            "id": "f3da7942-c043-4bbe-81f5-bdbed8bd16f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 93
        },
        {
            "id": "52f0e96b-35bb-44cd-a50d-10403f4825bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 97
        },
        {
            "id": "43364fef-5310-488a-92f9-a3f31882feb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 99
        },
        {
            "id": "8a26f99b-c2b6-4a93-b403-4492af543f04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 100
        },
        {
            "id": "4c167dda-0b8a-474b-81ad-9d83d744d1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 103
        },
        {
            "id": "db6f712b-9521-42f6-8464-f6b73eb34f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 105
        },
        {
            "id": "6fd82724-49ec-4cfe-b5b3-cdbfd6fb76ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 106
        },
        {
            "id": "f4b4598d-98e5-41d9-a3ea-55810e802d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 111
        },
        {
            "id": "5f615c7f-1d82-4e5f-b892-abff66d303e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 113
        },
        {
            "id": "30b6c6cd-73e0-49e6-b61e-d2434707da95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 114
        },
        {
            "id": "53c5644d-7e9d-4255-99da-417c038f47ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 115
        },
        {
            "id": "c901d47b-e070-4d47-bd29-975f619c9a0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 117
        },
        {
            "id": "eac33e67-9fb7-471a-825c-d47ba4b20d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "4b40d472-4ac8-45d2-a0e3-297a35b89a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "ac2b0f72-8278-4bfb-a165-8a908aa4526d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 120
        },
        {
            "id": "7dd61e04-a6d3-4a6c-9404-cf354fd523c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "e078a07f-6a19-4486-9e08-5df4598e579b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 122
        },
        {
            "id": "e08b38d5-cdf0-4c5c-b948-f1aee0a86b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "bc1478eb-e41a-46b0-a9ec-6f81eec8b90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 894
        },
        {
            "id": "c7a74b87-a4fa-464c-8a04-34175760cc4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "d89d60e8-93a9-4f17-95d0-115e9f623cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 33
        },
        {
            "id": "d669515b-ff85-4b68-94ef-f6981614040b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 34
        },
        {
            "id": "966a0da1-546c-4932-8845-599811d126b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 35
        },
        {
            "id": "5995b03c-7fb1-4785-abb4-4c8c4b90842e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 39
        },
        {
            "id": "d9cfcce4-6e25-417a-a01b-c99b987510a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 40
        },
        {
            "id": "822e1c9e-adb7-4edb-a64a-5acb45a1d3d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 41
        },
        {
            "id": "0ea6cd1d-07be-49e8-a7fb-66a62544bd27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 47
        },
        {
            "id": "53cdb808-562f-48dc-9860-3473f7c9479e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 49
        },
        {
            "id": "09320650-caf8-4ecc-9fdc-ce2b123d7e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 50
        },
        {
            "id": "bb519800-7dc9-47d5-b191-a10a0b483951",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 51
        },
        {
            "id": "847c770b-753d-4341-8656-78a69959823b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 52
        },
        {
            "id": "509bb250-4e56-499c-b7ea-fbff4c27d2f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 53
        },
        {
            "id": "19a294ef-0811-44f7-ad18-fe5afe05c22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 55
        },
        {
            "id": "06a8c281-e198-4870-bb21-6f0cd0adf812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 56
        },
        {
            "id": "e9d3fa69-c166-4c01-bb81-ceb9a2db7a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 57
        },
        {
            "id": "e6436f9b-9de9-453f-8e59-075c95a9a4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 58
        },
        {
            "id": "d461ec0e-1bd1-4c0a-8e37-47baaf3542b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 59
        },
        {
            "id": "767965da-beb8-43ed-bb8f-a5dfbf25df52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 63
        },
        {
            "id": "4e18ec74-4061-47cb-ab3f-407552f14bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 64
        },
        {
            "id": "98ead4ea-a5ae-47b2-88a7-e037373bd695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 74
        },
        {
            "id": "862595bf-a06f-4399-90d4-ba755c71c5ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 83
        },
        {
            "id": "ab557245-ae82-4c86-8cbd-883937b0b9e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 84
        },
        {
            "id": "54f6dda0-6709-484f-890c-18be04a5fb21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 86
        },
        {
            "id": "d72a335f-ac21-43cb-94f6-33d4ac3cd19a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "b9265e90-657c-46fc-94cd-8d28503d371a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 88
        },
        {
            "id": "0dd20557-285b-4035-8cb6-54fa9a3eedd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "feb72dd2-6821-46a4-bfb4-c08a8e598c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 90
        },
        {
            "id": "af720e56-d6d8-47cd-bb1a-e92d9af50bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 92
        },
        {
            "id": "459e1bf7-6aac-4058-b35a-27a8aa3efd12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 93
        },
        {
            "id": "44710ee0-e8d0-4fa5-9108-4a0b3f668cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 100
        },
        {
            "id": "1032f184-a54c-4714-abe6-2b4dd6af6b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 103
        },
        {
            "id": "acbe6b7a-931c-4a2b-ae31-bcde23f117c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 105
        },
        {
            "id": "7a5d8717-8a38-4c5f-ad4b-ca30e80227f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 106
        },
        {
            "id": "1d618873-d109-4f7d-94de-06fa57688f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 113
        },
        {
            "id": "860bc02d-ac5c-4774-bf12-2bb7b7470aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 115
        },
        {
            "id": "129d3bca-3a68-4b05-8ab1-aeb636f55c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 119
        },
        {
            "id": "8b67dd86-de96-4993-8ca1-0fb766ae363d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 120
        },
        {
            "id": "e5fadeff-8200-414c-800a-c2d6f52fed9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 121
        },
        {
            "id": "6f63721f-d0aa-46b7-9944-d9591bb9ebbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 894
        },
        {
            "id": "bcd3173c-f1fa-48ae-8000-cfe33e6216e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 35
        },
        {
            "id": "a0011774-ec29-41bb-833d-21b1bf378029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 40
        },
        {
            "id": "db5b4fc8-92da-4fce-a782-96fc520afdfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 41
        },
        {
            "id": "90e2761d-ba60-4962-8480-d40477d2c6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 47
        },
        {
            "id": "6ad60113-82fd-4a50-9c49-c2f114b1ec4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 49
        },
        {
            "id": "b399c2ff-1441-4526-b227-70eb73fe5992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 50
        },
        {
            "id": "44cdbb80-1c47-4b48-8d9f-d900c7011fb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 51
        },
        {
            "id": "c6f8ce86-2bec-4ecb-ac0a-18c93dec9710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 52
        },
        {
            "id": "2c038db6-dfb6-4ef5-8b93-2b9edd401374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 55
        },
        {
            "id": "b18f155c-12b0-49b1-9a8d-0af5f4c0b7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 57
        },
        {
            "id": "5e0fa09a-232b-461a-9b38-1d9751e90999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 58
        },
        {
            "id": "30638335-af51-42d7-b7ef-f40cc7b810d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 59
        },
        {
            "id": "7f6a7f55-675d-4750-bcd1-7aae92b197b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 63
        },
        {
            "id": "8babf9f5-56d7-4d94-bbf9-c0a08b8c2901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 74
        },
        {
            "id": "da658560-d05e-442d-8d64-aec87d2c854e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 83
        },
        {
            "id": "a721f44b-8875-431e-89d0-a6f82f7a8e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 84
        },
        {
            "id": "350a6709-6859-4f8e-8d5a-509523adb9f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 87
        },
        {
            "id": "22f80338-74f1-4735-9e1d-13c248fc9654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 88
        },
        {
            "id": "ed90a495-d5d7-4e50-b9ad-9f1f9ae5b277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 89
        },
        {
            "id": "52524bdb-7268-4936-be89-1abcc8fbfe90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 90
        },
        {
            "id": "2312d85c-262b-4213-86c6-89ed530d7811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 92
        },
        {
            "id": "4cd49128-4340-4e87-a9ef-cbd1e3eceaf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 93
        },
        {
            "id": "3f049a74-7bb9-4525-9852-d7ef27471ba8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 100
        },
        {
            "id": "debaf466-cec3-41f2-8067-f8b194085459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 103
        },
        {
            "id": "69bfe5f8-6246-40a7-b2b8-3d4ee7ef2587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 106
        },
        {
            "id": "2b86b7bf-f082-489e-966a-f62936821aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 113
        },
        {
            "id": "426959e6-e8b4-416d-9c9f-d5f233323748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 118
        },
        {
            "id": "d7150193-c9d0-440f-a573-87db01234407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 119
        },
        {
            "id": "545cf9b8-48fb-4def-bf93-912fff1a4313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 120
        },
        {
            "id": "6080dff7-cb03-41de-8305-5063d7c59197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 121
        },
        {
            "id": "501dac90-a528-4a74-a51e-e9d8625947cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 122
        },
        {
            "id": "17ae728e-db30-496b-a681-9b5bc2e8746a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 894
        },
        {
            "id": "5cf0ea4b-f87d-4519-aee6-56f5e865f904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "4785cef5-2dd5-480a-a8ea-3ba25009dbd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 34
        },
        {
            "id": "fe95529c-ad8d-433a-a560-5469d43387b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 35
        },
        {
            "id": "60992828-10bc-44ba-9263-c71bf3dfb1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "cb646e85-e3cb-437f-a574-c25c948fd8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 40
        },
        {
            "id": "98409e76-70f7-44ab-bd2f-a825f4fdf204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "4515fcca-b531-4783-8625-98c477fe6968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "3e2c1327-592e-41c8-986d-453020999f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 47
        },
        {
            "id": "2d10874c-b2c2-4790-af26-08c47633356d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 48
        },
        {
            "id": "9f5b4497-559c-43fa-b4c1-fc85bb96765e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 49
        },
        {
            "id": "effb5f35-0620-4022-9507-f0a8841404e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "5fa8caa7-3db6-4d84-8dd6-c5de3a9a918b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "5e0152b2-998c-4bbe-9bb5-f06bded3081f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 52
        },
        {
            "id": "967a78a0-3688-4b60-a7b0-24c889a08b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "c416beda-f770-4f53-8dd2-924070275a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 54
        },
        {
            "id": "03c66c31-4671-4e54-b083-813809714fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 55
        },
        {
            "id": "c8c081aa-77a4-4734-9d49-7fdbbc68516c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 56
        },
        {
            "id": "ec856979-9a45-4f4f-a9d0-44442524e679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 57
        },
        {
            "id": "62e7c49b-c7b6-4df9-88e4-1d4c76469b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "d7e5cb18-d249-4134-9ed3-3aa9180a7420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "0875a9fd-c57a-4f62-8c55-767ab2af0aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "5733aba9-04c7-4751-a2ef-e9853b58d660",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 64
        },
        {
            "id": "a8267e0c-50aa-4b05-b98c-39d75d3c348e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 92
        },
        {
            "id": "6dd7e201-b2aa-4cc0-a80f-43d0ae0500a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "2f76eca4-d6e1-4672-9f34-9f3f686ea0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 97
        },
        {
            "id": "0953b674-3a38-455c-9220-95ccf10af7c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 98
        },
        {
            "id": "dfca47da-adc4-4006-926f-47b0aea2db8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 99
        },
        {
            "id": "8f7f9a69-3a93-48a3-90bf-727868a82246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 100
        },
        {
            "id": "196c9cf2-ef9c-4076-b9c7-800db7ead8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 101
        },
        {
            "id": "10ceb851-91a2-4763-93d0-db8b8c377553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 102
        },
        {
            "id": "5c125aad-5f5e-4301-a4b2-3ba608af9644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "da5a2b27-0c77-45c7-b585-ae77d6e2179f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 104
        },
        {
            "id": "129ae6c8-e2c8-4640-b0da-0aaef3b85d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "9e874fee-fd59-47ca-a0be-c092941f5f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "978dfcd9-83ee-49b2-a4c0-f51aa8659f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 107
        },
        {
            "id": "8bf120ec-8d73-4d3d-9d8c-7220463dc1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 108
        },
        {
            "id": "10c92df7-cb3b-4b04-aca9-c5ec77fdef96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 109
        },
        {
            "id": "dadf165c-05af-4cac-b012-e24ac66b9947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 110
        },
        {
            "id": "4394b53a-e091-4d2c-9151-14ad4fc0dcd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 111
        },
        {
            "id": "325dc099-a0ea-48ec-93c2-e9eb9a644150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 112
        },
        {
            "id": "df6a95bf-2986-472b-9a44-cbc49fa9b086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 113
        },
        {
            "id": "c51898a9-ce28-4287-b5ed-c25a47b578ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 114
        },
        {
            "id": "cb3b0394-6947-43c6-a34e-56067fc068ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "b98a95b3-8bd9-44ca-b7af-eb4dfa2f4cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "4f820a53-ae74-4f4d-92ac-b4eea2f397f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 117
        },
        {
            "id": "82e1db7d-50a1-40c4-9403-b1557bb3366d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "367a7123-ae2e-4155-b208-11afe767255c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "1c529795-1022-4524-b773-22e26e8e408d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "1e59c4fa-c632-48a4-8a07-cd0e1df599b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "7711602e-0f2e-471d-ab4b-8aaf7b6787ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "c1af665a-b605-43eb-9493-66afb6ee78e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 173
        },
        {
            "id": "085b1418-8cd4-4788-be4e-6c2696b9951b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 894
        },
        {
            "id": "f471b5cd-6d3d-43fa-bb64-b17f4841ec24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8208
        },
        {
            "id": "28642391-2ec9-4c22-bfc9-9252565228bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "e2b7b161-f51a-4288-9b31-4df23a6ce092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "71c90934-f182-42d9-9559-48f8519c9272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 35
        },
        {
            "id": "b0c3a3e6-f504-4d54-bc36-a6ee05d6678c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "4c0fb91f-fda2-41d0-a7e5-13d0be7a13e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 40
        },
        {
            "id": "eb7cae51-372e-4eb5-bc90-c899ccc3b1a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "0e93b8c7-8f23-49fd-be09-09fd9de25bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "d480adb9-86e1-4aad-a5eb-16e2df61c8c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 48
        },
        {
            "id": "6ec61978-8df6-4360-8eed-a4dac99c856f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "6f5dc9ea-770f-450a-92e3-23b76d2f7cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "b590edc5-6357-4829-94a1-5e1e77fb1a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "70420f78-2e20-4d4b-919e-5f2d436cd983",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 52
        },
        {
            "id": "8ad855ce-1154-4f49-b208-9d7902e9f828",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 53
        },
        {
            "id": "0838dcb4-9c86-4569-9d2e-ba8d2783ea82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 54
        },
        {
            "id": "ff7a4eaf-4626-42c5-ac85-7bcb445776d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "d6ff75b6-dc23-4a9d-bcb6-0a5cad5f900a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 56
        },
        {
            "id": "3a76ee5c-a0d5-43f0-af76-9ab9ef512c1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 57
        },
        {
            "id": "af9ff26c-0220-4907-ba32-e8ddf77411f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "0477a2e6-df42-46e0-91c3-f431e21396c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "af4675ad-eae0-44dd-976e-65b9e9872b55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "617ea6be-0c9d-4ef6-9214-2632546c4679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "22fc487d-7fd1-4997-b35c-a27fdc791e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "5d4f1d27-06b5-4d07-8502-287b44f30653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 100
        },
        {
            "id": "28814751-ee96-4933-815e-768183f95476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 102
        },
        {
            "id": "76d81f8d-f017-4b6b-92b2-0a963290c57b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 103
        },
        {
            "id": "beca98d7-ed69-4779-ab4f-2642349ba103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "466c561a-bb47-4da2-ad7b-cba2bac3109a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "5d0ec026-923c-4ae0-810b-5cf8d3dc7d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "2e906806-fe46-4d28-b719-6f8c2ec26517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "751fa161-c8a6-43dc-b316-d3b1849ad185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "340b37ee-863d-4d58-8723-ce3e215d789e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 113
        },
        {
            "id": "a1534521-4415-4b82-baad-3e55ff753b8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 115
        },
        {
            "id": "a98b064a-8d60-439a-bd5d-40f0ba03e6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "3ebe554c-20d7-4236-a8c8-8c6da4ca6e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "de42a4e7-38e6-4e18-9280-9b59ec725171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "ad47995f-4e99-48fa-bac9-b461629276e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 41
        },
        {
            "id": "56f32b87-f3fa-49a0-9cc2-5f7f3a05dc6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 45
        },
        {
            "id": "8f264298-ac53-4cf2-88c8-2914e76c59ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 49
        },
        {
            "id": "a235dc47-cafe-4eaa-ad4f-82a117d009d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 51
        },
        {
            "id": "99e013a3-557c-4762-aad3-0c7928c88d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "6cfefcfe-475d-47ce-8e4e-9c2ee7548e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 55
        },
        {
            "id": "3ff4ebda-8370-42bb-8d63-667ff71a9537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 57
        },
        {
            "id": "e5bf6ed7-11eb-4a93-902e-50c3ee3c85d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 58
        },
        {
            "id": "dd462165-7f9f-4b90-a0d5-e09188dc7796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 59
        },
        {
            "id": "8cb25a63-26bf-4cec-971a-e4005848eb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "f8677576-43f6-4883-ab66-ec03aa64ae8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 92
        },
        {
            "id": "8d45a291-626b-43cb-a511-e633031648eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 93
        },
        {
            "id": "9dbfe19b-a90b-42ca-a12d-89404673dc1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "3a00daaf-dd95-4ad2-be3e-4b938128830d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "7561ccba-e13f-4448-b454-ed3011a8bda1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 120
        },
        {
            "id": "a6f9c653-f14b-42f8-9761-d1def121597a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 173
        },
        {
            "id": "eb90f77f-84d4-4dc8-bd8b-b9e643b3bfcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 894
        },
        {
            "id": "9e904c14-4093-41b5-8229-e706108a08ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 8208
        },
        {
            "id": "765dd8ba-136d-4a6f-81f9-3e97863703e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 35
        },
        {
            "id": "3a65de2c-fc18-4a48-be2a-a494aa2af552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 40
        },
        {
            "id": "4a331a9e-38c9-4209-ae4d-238bc77c36f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 41
        },
        {
            "id": "4138ec04-fcef-439a-a7ad-bf6dfbfb3dc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "aef3309c-14ab-47e7-9b9d-799a9db6178d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 49
        },
        {
            "id": "43fee1f4-0d6e-4c48-97e1-9d5b9e32667e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 50
        },
        {
            "id": "89edf0ec-0a8e-43fe-b06e-7422e7a33969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "1fa634a9-953e-4b13-98af-41dae0a65848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "b769174b-4278-4e39-8f50-649715d616a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "62f3d42f-bcf8-4d3e-9fd6-69e021ea2fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 57
        },
        {
            "id": "a1508039-3cb1-4f00-b0f9-7a269da413f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 58
        },
        {
            "id": "e7b34b9c-8202-4ff1-9106-a00d7da1826a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 59
        },
        {
            "id": "027bfad0-7046-42b0-9cbb-be6b7a7209f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 63
        },
        {
            "id": "106e847c-7537-414a-8219-80e49e84a976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "6abbfd4d-5040-4c41-89b8-a67abaf2b92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 93
        },
        {
            "id": "90631921-1da2-436f-ae2b-124db0d464e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "4022ede3-0cfb-40f6-bf4c-cdb678b843ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "b83e4aed-e88f-4de2-95de-db5f464c043f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "39586d50-2af7-4027-864e-3dd9875948cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 113
        },
        {
            "id": "e89c8acc-c4bf-4014-aea9-bfb57cc95a51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 118
        },
        {
            "id": "ca8acb05-f1d8-49a4-8cbe-5f3d35e38229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "d58bb5e8-b639-4990-b9fd-3f3565790ef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 120
        },
        {
            "id": "4d1d4f65-4655-480b-8759-6c38e49c2b0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 121
        },
        {
            "id": "45a40dbc-0c56-447f-9f59-16bfa6bb3fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 122
        },
        {
            "id": "520074b2-2e21-42f5-a4cb-245d619611e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 894
        },
        {
            "id": "0b9f2b8e-a672-43d2-a32c-0d2318fb93c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 33
        },
        {
            "id": "ef9752db-c56b-435b-a64f-fe204e412735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 35
        },
        {
            "id": "fb60587e-e50e-436f-a91d-5a8d73cd917d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 40
        },
        {
            "id": "7d836591-5979-4990-be73-eb62d6888b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "72ddc330-fa50-426a-81a8-ecb0df8c9347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "cf32c888-4881-4e06-97d9-d6779b3bc019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 47
        },
        {
            "id": "2ca8ed00-16ee-4d23-ad4e-9fa7d5ae9f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 48
        },
        {
            "id": "e68bbdf5-a765-4d9d-ad90-fc0f3d503734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 49
        },
        {
            "id": "ba539087-8c22-46b1-9b00-54371f4f294d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 50
        },
        {
            "id": "b688175c-e044-444a-be2a-45232bbaa00a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 51
        },
        {
            "id": "359ac205-e2f2-4779-89b8-e04fd0b5ed4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "08a773e0-b1ee-4b5f-a84e-f847699bdcd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 53
        },
        {
            "id": "497673f4-d33c-403b-98dd-312152d162a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 54
        },
        {
            "id": "82849db0-94ee-4429-9525-2ba05cb73cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "6eb0896c-771c-4a52-a6b1-1eaf33df510c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 56
        },
        {
            "id": "c18f0ad6-9295-476f-93de-3dc26aea58b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 57
        },
        {
            "id": "c0a7c3be-1f67-4c8d-8ab1-183bcbd4fd4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 58
        },
        {
            "id": "9fe9eb76-0f11-4e61-8f53-e387db5910ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 59
        },
        {
            "id": "32eb7e43-be9f-4fc5-8112-7d70f1f5e6cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "24ab2366-ed2d-46ee-9648-59868529e16f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 64
        },
        {
            "id": "376216e3-3bcc-4a6c-9f38-041d210caff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 92
        },
        {
            "id": "b13decd5-2d27-4bdd-b52c-dca7f968e15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "fda4c2e8-4a2b-4dc9-b30c-75ff4ce0d0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 97
        },
        {
            "id": "9c19f05b-2c11-4df7-9f47-a56cb726dec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 98
        },
        {
            "id": "929c6465-1e8a-4b41-8c39-9afe260949cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 99
        },
        {
            "id": "be5da89e-0d29-49eb-8205-d72e2079195f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 100
        },
        {
            "id": "5f408e9c-734b-4406-9864-ddbe86fa3f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 101
        },
        {
            "id": "e50b0a8a-cbb0-484e-a6e4-a7b97c97e428",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 102
        },
        {
            "id": "01f37112-ad53-4b38-bcf2-f386202f74b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "1fcd24bb-f569-47fd-9171-860c0751b567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 104
        },
        {
            "id": "cda629df-ff97-43c6-adb3-0c1d7ebf29d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 105
        },
        {
            "id": "3fea19ae-1765-4b44-ac28-3ad5daabdcdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "aee2c2a3-d833-4749-b844-c8f3fd22dd6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 107
        },
        {
            "id": "27d5a5c4-a9a7-4b20-9128-10ec5b168e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 108
        },
        {
            "id": "aee60a26-83bb-4204-a6f3-c4d149578cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 109
        },
        {
            "id": "b14efaee-0470-46fe-8943-a91e3a429a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 110
        },
        {
            "id": "807fc014-8eec-4fd7-b31b-339cfb91390b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 111
        },
        {
            "id": "d5b531ba-dcbc-4e24-a7a5-e9426ebd7664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 112
        },
        {
            "id": "38434825-8e38-4090-903a-53cc0d2484cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 113
        },
        {
            "id": "8e6f21ee-de50-4b4b-ae08-16af255580ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 114
        },
        {
            "id": "157f279b-7eda-47ad-bd9f-a22217aa826d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 115
        },
        {
            "id": "accaf4cb-8154-4936-aa7a-54126937ac9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 116
        },
        {
            "id": "daa825ef-a208-44ba-a567-07b9aa547219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 117
        },
        {
            "id": "9b650fde-db62-4d3e-a16f-d474d2637f97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "539ca770-bfde-4335-a9d7-90759103a437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "e6c9e2b3-eab8-4e6c-a7ce-e255ef5b9b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "2256c03d-4c78-4cfe-8940-81f5dac75cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "020c5030-dc88-4226-8165-b36d23c531a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "7841b394-7bb3-440d-b74f-73b6560d80da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 173
        },
        {
            "id": "33252aee-8d98-441f-828d-d25cc54cdaf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 894
        },
        {
            "id": "f2566467-ab25-4a1b-934e-969af0258515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8208
        },
        {
            "id": "b068b0cd-65f9-4326-9f12-4a6bce107d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 34
        },
        {
            "id": "4089388f-04da-45df-95af-d70ed61981b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 35
        },
        {
            "id": "6e4faddd-749b-4906-9acc-4e5597bfbd8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 39
        },
        {
            "id": "91983eeb-eb9f-48a6-829e-c9b584f20f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 40
        },
        {
            "id": "66d0ca59-9cbe-4d5b-bb9b-55b7fc52ea8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 41
        },
        {
            "id": "4b83d40e-e5ea-4f7b-87b6-b179e7115ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 44
        },
        {
            "id": "755e7fb0-efbe-4fa8-bc47-3ee575fd88f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "ea0a30a1-1607-4e48-92b5-2510e4efd889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "6d264334-f7cc-4c6b-935c-f8945b25adee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 49
        },
        {
            "id": "f5e1e542-297f-47cb-9990-157e4bb40124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 50
        },
        {
            "id": "12e0c665-3316-4cdb-be05-b46c7d4e29d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 51
        },
        {
            "id": "4ce168ae-016a-4627-a6e5-15667e6489be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "3630764d-2f3f-40cf-99da-90f7bbbfeef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 53
        },
        {
            "id": "ab516b86-14d8-4ce0-b646-4a7751f75eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 55
        },
        {
            "id": "12c371cb-a035-4ceb-ac0b-84fd5921f876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 56
        },
        {
            "id": "532697d6-b67a-44eb-9012-39a451722ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 57
        },
        {
            "id": "1d38a246-4625-4833-814d-66b26bc4b3dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 58
        },
        {
            "id": "d735e297-4854-4cee-9f99-c63c50029038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 59
        },
        {
            "id": "5184b3d8-f8e8-44db-ba2b-a1e830ded98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 63
        },
        {
            "id": "30f45efa-e19a-4b1d-b729-cde41e3f9bb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 92
        },
        {
            "id": "b4472522-567f-441c-acf3-2cecfa83ff0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 93
        },
        {
            "id": "b4c6ce10-465b-47ea-9313-2bb471386820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "a659a5b3-b3e6-49fb-9ceb-54d89c2f1163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "1fb717eb-a100-4b9c-bad9-e6731252205a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 106
        },
        {
            "id": "73cad4f9-e041-48ac-af69-7606aa73333f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "0506bd2f-eeac-4233-a7ce-c9aba4597f7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 115
        },
        {
            "id": "ee98a785-81ba-481c-be1c-7a79142c0988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 118
        },
        {
            "id": "98dbebd9-5ec0-468b-8a5e-058ff57b7478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "687e672c-af1a-423e-84fb-3d24789f85b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 120
        },
        {
            "id": "670391d9-d32f-47de-8c6a-661e1d3ea534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 121
        },
        {
            "id": "fc19e234-d7e2-41df-821d-2200d5c7ff4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 122
        },
        {
            "id": "fe587d72-226f-4110-9bc8-f35db7ce1b80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 894
        },
        {
            "id": "7aebf7d8-d06c-4eba-a638-1d9b887300ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 35
        },
        {
            "id": "8694c0b8-e9fc-4193-90f5-77f2a694a4f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 40
        },
        {
            "id": "05cdde05-5e4a-4b59-851c-4610644decff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 41
        },
        {
            "id": "3c7eee9d-40a5-4c5e-b6ff-144bfa0b0725",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 47
        },
        {
            "id": "aae339fe-ca68-4a50-9efe-8b3003990d86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 49
        },
        {
            "id": "fb66e6ec-66ad-4a6c-84ab-35dd743abf64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 50
        },
        {
            "id": "2161915f-c529-4817-b544-fda6530d3709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 51
        },
        {
            "id": "d3427501-604b-4687-94fd-a4f0c39ddc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "3b968026-cb5a-4245-ba6e-612b9582032e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "25d78d54-307d-4e14-ae89-6a3d540f2f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 57
        },
        {
            "id": "ffe3dec7-365d-4eec-9e03-188518ecf654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 58
        },
        {
            "id": "639c0579-88be-4f67-a32c-4bd38ec38e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 59
        },
        {
            "id": "a3598b0f-ef3c-49a3-bb7d-604aef85f440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 63
        },
        {
            "id": "d3d9e3ae-4cea-4ed8-9fc8-e791145b9cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 92
        },
        {
            "id": "b25f7577-9811-4d78-9d0f-0169ced351ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 93
        },
        {
            "id": "de59a4cd-90f1-420d-ac7e-5b8688f64771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 100
        },
        {
            "id": "4f4b20d6-02be-41a6-8d1b-b7da57b0f6b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 103
        },
        {
            "id": "e3c7b176-5b9b-442b-b585-63bf22966d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "ac1f3253-7f5d-471b-9f3a-f35661edb5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 113
        },
        {
            "id": "d7798e5d-4717-48cb-9364-fc7628db24b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 118
        },
        {
            "id": "28ca66f4-96f1-40b3-a9b1-cc72258ecac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "29a20782-ddc8-415a-8dfa-b0c378f143dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 120
        },
        {
            "id": "44739252-2d94-40f6-b521-5f6c896deb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 122
        },
        {
            "id": "34b5a9a5-c97b-41cb-a303-af998ce39561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 894
        },
        {
            "id": "07f37916-640c-4899-af81-17bd1da485fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "d562f79f-2ebc-4721-9172-6d38081a4f0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "52d7aa13-148c-48ad-91d3-4a1f03b124b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 35
        },
        {
            "id": "c889d650-32be-489b-b7ce-426f28b28f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "52aded90-7f98-40e5-94dc-8f77c89b1169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "8e85562d-8c5e-4158-be05-9a2a8be9ef4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "73fa9466-484d-47ed-94b6-0266ad7f053d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "b3759307-eb03-40c0-af5b-bbb77a2adb7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "e848535e-457f-47bb-b4e9-01fa64422c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "bc98d424-4fb4-4d90-9858-d2ec085cc644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "4221ee88-64a3-4f82-aeef-132f61541853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "4dae078d-d913-4d04-81cb-ef80207bc78a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "38b8bc51-0a1e-47bd-b919-92eaba07ab31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "48e6310d-6558-42c1-8fdf-9e411ae7b042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "4c2f14be-6331-428a-a924-c53ac222b2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "f84866d9-fe05-4c7e-abe6-ee82144da024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "93449b81-3613-4c93-9781-7b447bc8f23c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "ff7a4f46-3ea9-4e8a-ae55-dd85805fb1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "da05cffb-e00c-41ec-b7b8-71da21991f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 64
        },
        {
            "id": "2a38f6e0-8b2f-4513-9871-46e34d9c242d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "6107c3ae-5652-4ecd-8b36-98cfa24a08fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 93
        },
        {
            "id": "dab2c6d8-6364-48a8-afac-1e5801efe33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "925ebec4-3d69-426a-8cae-28b694b22f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "fdbba785-c1ae-49b2-ab11-50aeb4929a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "9fb98bb0-aaf3-4c63-9f48-77a47e350757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "27705617-5033-4173-abe0-412fdb7c004b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 104
        },
        {
            "id": "f7e430a6-f216-4400-b1b3-156d6cdb1fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "813a1a4f-3698-45d7-b2f5-9a9dae04fb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "97b2cb39-a0e3-45ed-8ffb-e6d7ce88e2bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 107
        },
        {
            "id": "45faa022-c3bc-4a75-8870-7e459dc0918f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 108
        },
        {
            "id": "649ab1a4-e1cd-4ca2-8e2a-bf6d6e2a7f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "6c5688b8-1c55-4f6f-9fbb-9f36b23e61d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 115
        },
        {
            "id": "e3e190e7-aa61-4e66-ab94-f955377b760b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "1d9d3977-a881-4710-a63f-416d8ef60c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "fa0b67bd-5a50-4d61-8fd8-ae91380852e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 120
        },
        {
            "id": "b159e6f4-2be7-4a66-89b7-d67157244948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 34
        },
        {
            "id": "b0117ca1-da1b-4999-9722-f911ab095a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 35
        },
        {
            "id": "1713f9df-22db-4422-b6a8-17069216d09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 39
        },
        {
            "id": "782915e4-e001-4d21-bb8e-d821ea632b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 40
        },
        {
            "id": "9e9d281f-80ca-4add-a041-d9be66800131",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 41
        },
        {
            "id": "ef5ed196-e00d-482e-87b9-2636a17182ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 47
        },
        {
            "id": "1e59ab56-d1db-4aa4-be9c-067bda5f9172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 49
        },
        {
            "id": "7b3b75c2-cd26-4e99-a450-b4b29eefde4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 50
        },
        {
            "id": "8d4795c8-5e3f-4ce8-b3d1-f014a912c211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 51
        },
        {
            "id": "e216ceaf-314d-4da3-b06b-a10be29a3bb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 52
        },
        {
            "id": "f7bf5efd-80bf-48fa-a1af-1799522c9f78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 53
        },
        {
            "id": "a7723d38-9f02-49a8-b103-30decec49ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "4fac1bc7-8fcf-4b6c-84fe-5ab50e926b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 56
        },
        {
            "id": "3b511363-73b4-4a82-8031-8aa7d2b8e0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 57
        },
        {
            "id": "8703d2ac-eee6-4d8f-b854-c20617744980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 58
        },
        {
            "id": "8b6c6fb8-676d-4bc8-9e87-c596d29dbaff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 59
        },
        {
            "id": "550ada45-cfea-4749-8bf2-6602f83d5d44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 63
        },
        {
            "id": "a1881a42-ee90-4c51-a5d3-7b76e94239c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 92
        },
        {
            "id": "7f27d952-9ffd-4684-8b99-1e53effb38d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 93
        },
        {
            "id": "d6e01f45-7251-4ab3-aceb-f001877633ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 100
        },
        {
            "id": "1497a166-98e8-400a-8f37-b1188ac10116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 103
        },
        {
            "id": "96aee400-ce32-4bd4-ac6f-38afffca8315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "7c8a5167-014e-474a-b975-d6cc594e0627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 113
        },
        {
            "id": "fb2f4055-b4d5-4582-8c1e-cebb1ddb12c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 115
        },
        {
            "id": "ec920f50-7a30-40ba-8420-b1a3af1f3fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 118
        },
        {
            "id": "77e0b9f8-edc9-4c19-8f0f-e3a8620ccca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 119
        },
        {
            "id": "cbd96fda-0d53-431a-bdc9-0022089aa0ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 120
        },
        {
            "id": "a534de85-e7dc-492c-b572-3d021cbbf142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 121
        },
        {
            "id": "19454312-ccc8-4c0d-99af-599f82ed97b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 122
        },
        {
            "id": "92d18b7e-d858-441c-9b7a-5dac5986e0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 894
        },
        {
            "id": "553e4136-7a7d-439a-8960-90c370391a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 34
        },
        {
            "id": "a11383b6-e109-46e3-8027-336946d6f151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 35
        },
        {
            "id": "bf59f214-c0cc-49db-9992-3c4ff6caa8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "92061688-f31d-4659-88b3-9a8d259d2da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 40
        },
        {
            "id": "7cf35ab7-bdde-4680-b070-3f84f8d904ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 41
        },
        {
            "id": "13b8cfc4-833d-479b-b26d-79e507af46c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 47
        },
        {
            "id": "ad001206-75d1-43cd-8da3-70697474b92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 49
        },
        {
            "id": "995d39fc-b67c-4405-a7f7-5e32c94401b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 50
        },
        {
            "id": "5ffefbe5-daf6-4d6b-a007-a8b074062a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 51
        },
        {
            "id": "1b420e83-499e-47db-ae81-abf1c92ce4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 52
        },
        {
            "id": "f6de6d41-2a02-4fbc-8e3c-fb8a3c87b682",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 53
        },
        {
            "id": "fd9bb9ea-01b3-4eac-b9bd-05815ce397b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "29d1c4b0-489c-4466-9899-2be2dc9e1303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 56
        },
        {
            "id": "293d4c2a-c7c2-4f94-988d-6239e7636b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 57
        },
        {
            "id": "436a897a-52d7-4167-bca3-06afedb3bb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 58
        },
        {
            "id": "b6578252-a3b9-4b0a-b235-4aa80b5e7198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 59
        },
        {
            "id": "4cfc79b6-f77a-4069-a622-2076cfc48634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "41a3dd19-545e-4bdf-8d7b-28a049a039c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 92
        },
        {
            "id": "872e8633-d000-4660-a4f3-84811a03c907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 93
        },
        {
            "id": "b5ec8843-efec-465c-b644-4fb64ce7b1cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 100
        },
        {
            "id": "cf5b5093-7aca-4d65-8ed2-617a91b07f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 103
        },
        {
            "id": "b77cb3e9-e005-44bc-948f-6658b1fb09a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "ca88c34a-8fdc-4d48-9882-7827d99e5376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 113
        },
        {
            "id": "17d854d2-c1c8-4539-bb83-52086d51dc9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 115
        },
        {
            "id": "de1f2965-16ff-45e7-a538-515b310f6a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 118
        },
        {
            "id": "d0e8c189-9f1b-4c6c-8130-608853f93945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 119
        },
        {
            "id": "ab6090dc-49d8-4fd8-b46f-29e6b65c919d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 120
        },
        {
            "id": "79c2eb79-01cc-440b-b9ea-e4d98c6c4baf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 121
        },
        {
            "id": "5937eb63-3304-4175-a851-ef6fc1ae0bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 122
        },
        {
            "id": "a8909f66-dbd9-4154-8797-4839cf382486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 894
        },
        {
            "id": "9558b6c9-7333-4225-9e7a-785d23fc20a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "635627e7-24b8-40c1-a701-0296ddb7129e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 34
        },
        {
            "id": "e90dea59-92c8-4c85-801f-eb9defdf1098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 35
        },
        {
            "id": "746f0a7b-c54d-424a-aa21-0d95a727f335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "ae033627-8821-4be6-a1d4-d3b684534511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 40
        },
        {
            "id": "8ad4a69e-e679-4bb8-b6a2-5b8a6b0ca151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 41
        },
        {
            "id": "03ccfa3a-87d2-44e5-88f4-1856517273bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 47
        },
        {
            "id": "7b0b4e75-c114-4c0f-8a46-9fa800a52c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 48
        },
        {
            "id": "04f6771c-36b3-4510-adfd-c798e42b8fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "83442379-b8d8-44f2-a688-12a5af805a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 50
        },
        {
            "id": "4b537959-64e8-4837-93f5-52832bbb6611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 51
        },
        {
            "id": "511699ec-e771-4cb1-b85d-9cf46124d2dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "f4a67bc1-9649-46c5-aae6-4e360a1b6ae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "78e835c1-3f5f-458c-b161-af4f326e6874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 54
        },
        {
            "id": "a83c3b05-4f52-4494-a062-a727b1dd44c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "a7eec431-8127-4788-a230-96b531ffc55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "2835b0ba-b993-42b6-9f1c-48c188159f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "621c1176-c138-40c7-a478-efeeb0eb773e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 58
        },
        {
            "id": "e46e9bbd-2689-4453-958f-74b4650b06e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 59
        },
        {
            "id": "a788c0ba-41d5-4a22-87e4-e3cbcdfb48e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "c330ac06-fed6-4f23-b1e5-48ba0e9b4f96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 64
        },
        {
            "id": "cb6afa0d-7835-4c37-96ab-8876b4527e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "49d7cb83-07db-42d3-a8a7-111ec361c467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "cc32e01b-6877-4939-b9f7-73a7215e01c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 98
        },
        {
            "id": "b8cd80d6-a968-47a6-be1c-4192f2dc9958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "cc5fccea-e14a-421e-9602-73c890935377",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 102
        },
        {
            "id": "46d68e69-67c9-4317-ad8a-0c389c60afe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "9f2929b5-4442-45f6-8fcc-91b1f052d0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 104
        },
        {
            "id": "35007b0a-3027-40b0-9ac5-cdd7b5153214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "85378c9d-6b43-4678-8bab-b12b8e201e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "e3bd6015-037d-4f70-9f87-17a65ff097ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 107
        },
        {
            "id": "650b5a20-4202-489c-9fcc-cc82267519aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 108
        },
        {
            "id": "5fe38366-b631-4798-99ef-4e943eadbd83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "297e0529-4183-40df-9cdd-a13f0ed9182c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 115
        },
        {
            "id": "451317a4-2c41-4e18-9ecc-fe0fc33e89d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "33d9e91a-e7ae-42ee-b58a-22634f0a7be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "825af6c8-43cd-4d62-9b40-682d2cc00e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "9223fee2-cdcf-4035-a7a8-5f6f156e707a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 120
        },
        {
            "id": "7b82418c-21f2-405e-9b0b-b1c2974cde72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 121
        },
        {
            "id": "c7027a19-563a-49fd-ab37-8572f3d27452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 894
        },
        {
            "id": "05e33a92-3165-427d-bcec-dcd401b54ccb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 33
        },
        {
            "id": "5f91902f-61a2-418a-91d5-8d0eb5a7789b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 34
        },
        {
            "id": "495fb1cf-0653-406a-b52a-5bd34872cbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 35
        },
        {
            "id": "291259a4-d785-4a95-a66c-7b4b9fc43371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 39
        },
        {
            "id": "3a7295e6-3424-4803-a1a7-3a899f671fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 40
        },
        {
            "id": "2854fc5a-3cb9-41c3-a28e-2a4dd65de955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 41
        },
        {
            "id": "522f577a-b024-4179-b95b-d76856eca1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "cf1ff2c6-44eb-4479-869c-8ebc7b05c496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 47
        },
        {
            "id": "1c13fde7-0cf1-41c1-8ae6-8aa3629ad1ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 48
        },
        {
            "id": "2b7fc527-2c65-4efb-8a74-6f0d8beaa441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 49
        },
        {
            "id": "7595df54-d72f-40c2-b990-202b6c14e2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 50
        },
        {
            "id": "2f1fec05-0b3f-4308-9067-6c4b7cc1dca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 51
        },
        {
            "id": "f5eca60e-cbf4-4791-be9a-f7f485c67302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 52
        },
        {
            "id": "653f0564-66cd-4804-9f53-cd05bb165fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "0cbbc010-16f3-44a8-88a8-16b5db0f3df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 54
        },
        {
            "id": "44783281-6635-4d93-af7f-177fd9714fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 55
        },
        {
            "id": "2c617e86-a43a-49f4-8351-b2ed2697367f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 56
        },
        {
            "id": "e9fb8865-d430-422a-b58c-7cbfec466ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 57
        },
        {
            "id": "2b0e35f1-b317-4b6c-ac06-db909d65d915",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "ff28dcec-25b4-482f-b4e2-a003e941a8f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "f3f90ad5-1441-4549-8230-9d3a4a399714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "9319b0c9-9611-4f92-bc57-212994754116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "51bea15e-73c0-4778-b1f9-d3a5f2c0607f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 92
        },
        {
            "id": "59c3a02a-3ca5-4d78-b039-4d1416582437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 93
        },
        {
            "id": "d22902e0-0154-4d0c-ab61-fb7aa0204703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 97
        },
        {
            "id": "6a592ba7-69c2-47ad-8358-a8914ad3ba8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 98
        },
        {
            "id": "3054f1ba-45ab-4d5d-ab21-43e16473b8d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 99
        },
        {
            "id": "88d517a9-d50a-467d-87a0-301bbad31da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 100
        },
        {
            "id": "4a982548-ba86-4e46-bd69-d10e6e2f805a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 101
        },
        {
            "id": "c86c8a4c-6b54-4cd2-a51c-94f6930c9d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 102
        },
        {
            "id": "241cd7b9-8bf2-48ae-9144-6beaf44d26f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 103
        },
        {
            "id": "6aa28e46-fc6d-42ec-88ef-5ec2c5df40ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 104
        },
        {
            "id": "8355b80c-f077-4d43-b493-f778976cdfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "a4a80d85-a95f-4f3b-9c4e-4f41624633fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "2bcd6b7f-fdfc-4255-b967-bb75b9ed0f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 107
        },
        {
            "id": "0242460b-34b2-4b08-9097-830c6a7d79cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 108
        },
        {
            "id": "f238f995-7625-42f7-9940-b5200c7eec28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 109
        },
        {
            "id": "b60f25eb-dee6-4980-92ec-ab5726efde22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 110
        },
        {
            "id": "9b9b15d9-ccb9-4735-aa17-89b02a2b14fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 111
        },
        {
            "id": "579d658b-5233-4643-ad63-4d24520569b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 112
        },
        {
            "id": "c4a66a63-d707-4024-8ddd-c89ba46583cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 113
        },
        {
            "id": "dbdc96e7-cce8-4423-82cb-41c0c8da6675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 114
        },
        {
            "id": "d61ec73a-b3b4-4c46-99f4-ff8abea33504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "cf4d4d37-aca8-4b2b-bb74-366cd1cf156d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 116
        },
        {
            "id": "ee759233-be8b-4d4c-8999-52834bad9ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 117
        },
        {
            "id": "3873ff44-72e4-4872-a06a-87870e0b806a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "693cdfea-206d-472f-92b8-64e476e34db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "76aa495f-c816-4bc1-ba43-0a0e3c95df56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 120
        },
        {
            "id": "037be97e-ed07-4b38-be27-22615a951219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "147588dd-d09d-4a18-ad1d-1daaa379a810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 122
        },
        {
            "id": "b870d4a2-7363-4cdc-ab4a-036577dfec52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "21d32f87-587f-4d48-85c5-4f449b3e7dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 894
        },
        {
            "id": "9b549afa-128d-42d3-8eb3-c0506af3e036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8208
        },
        {
            "id": "231e658d-22dd-4b15-8ead-e63fa7f470c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 33
        },
        {
            "id": "68ec8788-e7c5-49e0-a0a7-cfb68bbc38ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 34
        },
        {
            "id": "05d6b256-eb43-45fc-a5d2-17b9c15c12ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 35
        },
        {
            "id": "97b7e7ce-3e8f-41d8-82f3-ee5678928757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 39
        },
        {
            "id": "a685396b-c2bf-48f4-9981-8fa430eadd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "53502219-3029-4616-b497-b5f353de8851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "44b5c27b-0ce4-4fee-aabf-2cd8d37813eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "9d3e4e25-807a-4800-bac6-d62482704862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "763bec8f-080a-4da8-9136-7b60d6df8c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "c9a5c9b7-e66b-473e-b2b9-8baf116e5732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 50
        },
        {
            "id": "bcc9f5a4-094b-42a3-bcd6-03a92cf42d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "101cc276-c979-4647-bc24-19bfa560c182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "c8f6d6ae-d11a-41a7-9bfb-707104dcf0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "8d32a464-4d8b-4d68-83a5-6c923499b731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "8fd04c7d-a535-4cb6-b79f-bd52feb6cb0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 109,
            "second": 55
        },
        {
            "id": "bc28bdb9-607a-4976-a88a-a613d93bcf22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "12bf0757-2553-4a58-9b2e-f44d1bedddb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "0180a641-d2c4-400c-8e88-ab1119bbcf26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 58
        },
        {
            "id": "2e272101-df09-4e0d-a1d4-6f0b8648b55d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 59
        },
        {
            "id": "f1704fb9-49c1-483a-aa8b-b8ad9a741fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "fff43f8c-4065-457f-a21e-445b3e660a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 64
        },
        {
            "id": "ba7fd401-3405-451f-816a-84decee0052d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 92
        },
        {
            "id": "aa40821a-3c1f-4db3-8f9e-aac984168b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 93
        },
        {
            "id": "b2fc8718-2df5-4b26-ae83-baac3cc57d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 98
        },
        {
            "id": "b32a9610-93c9-4bf5-9009-a8baea94c0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "83a66b0b-8c6f-42a9-885e-c1380a473227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "beb810ea-d43c-4f8b-a133-80695a5ae17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "02ef999f-e3d0-498f-bad0-b081b56375a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "2a3ae126-7539-4039-a02c-507658ab8833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 104
        },
        {
            "id": "4fb51642-7515-4660-aadc-88e8571f7ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 105
        },
        {
            "id": "5c099c64-abf9-46db-9f0c-730ef5519e8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "3d744a6a-29ff-46fc-98ab-576e79ab9e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 107
        },
        {
            "id": "b0d6ed9c-3c1d-4c64-8131-ca841bd9b1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 108
        },
        {
            "id": "b9919818-dde6-4495-834b-c5c440be4341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 109
        },
        {
            "id": "37a790b7-3815-420d-95b2-a67873701dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "a003ca65-3b1e-4230-aa7c-917b8fa81caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 112
        },
        {
            "id": "da940952-2f2f-4082-98f3-207a674e8536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "bbb6257d-1563-4ea7-8172-3aaddf966a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "e4aa3ecf-02e0-4cfa-b6f4-8e57e7e201de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "b43eeef2-6a73-42f0-8fbc-d868392561c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 119
        },
        {
            "id": "5eadacbd-f4bd-4a9e-9629-03a68fcb9fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "c4a6cb23-d060-4b0e-8fb6-ad61d644189f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 894
        },
        {
            "id": "ac36dfbc-9e25-4673-914a-d574e0226964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 33
        },
        {
            "id": "e8a852ff-a272-4651-abef-bef88b5be19f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "62d95f91-b8d2-44eb-9740-2e495fb36424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 35
        },
        {
            "id": "241b646a-2996-4ae0-9747-24a1c6383280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "24d22a23-7676-474f-846c-35b200b6152a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "04a7d1eb-2fe9-4f01-822e-d8557c3158a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "5bffd87e-a7b3-4fb5-85f8-146e424a70c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "7132328a-ea5c-4a79-a9ad-5d4b5aa539b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 48
        },
        {
            "id": "759b98e2-9d33-44df-ba40-66bd1d557e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "8eceff75-2377-4d8c-9b62-7a4d2b6561ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "bc835c49-1d32-4089-b5ee-a9e079e054f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "408b04fa-6007-4c7f-a66e-659c80a57dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "540249e3-aba1-49c7-985d-4e847735b0d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "3c0884f7-cfd6-46eb-be35-65c5447d356b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 54
        },
        {
            "id": "6ef175a9-895d-4859-8d48-4a3b47d9cdd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "3a75f6da-a000-4f34-b1a7-2b2ace282a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 56
        },
        {
            "id": "8d385227-3e33-4c8a-9662-868ca65d1f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "c4a9581e-fd65-42a0-ac89-dcb9b1c03224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 58
        },
        {
            "id": "779110fe-689f-4964-ade4-3f79ff474f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 59
        },
        {
            "id": "43b6e978-f8cf-4a8e-b29e-6fb58a6d33ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "8404e3b2-21b4-4a37-95b2-71ae1c448d6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 64
        },
        {
            "id": "be083f47-c8dc-4ba4-b50a-c9062b9ad69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "e0d7506c-1a4c-4f7b-b05c-1afc9d64b37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "b1b7ebf2-a9f5-4bb2-b068-873e457b058a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 98
        },
        {
            "id": "86659e2e-57b2-4502-8600-9a1f5116e6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "b02ab3d4-daa3-41a6-a7da-2a0abdfc8bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 101
        },
        {
            "id": "6e2d902a-9358-4237-b3b2-a7982f6263aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 102
        },
        {
            "id": "3f7a6ee6-c9c1-492b-85ca-32429390aed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "968207d3-af42-4021-b1f1-22c35a428d57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 104
        },
        {
            "id": "314872ff-4dfc-4aab-b219-9aebe0964a24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 105
        },
        {
            "id": "af28000a-c29a-4bab-92b6-1561991fdf65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "8b0f3836-68ed-47be-978d-324b70dbe09d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 107
        },
        {
            "id": "a676802d-321c-47e6-b337-c49919312b2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 108
        },
        {
            "id": "a2b8ab57-719a-4ef0-8b72-43dc157853a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 109
        },
        {
            "id": "c4342fef-0d39-4061-86c1-55b4b3038124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 110
        },
        {
            "id": "a4fdac0c-01ff-47d5-8ac0-c20ed9042da9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 112
        },
        {
            "id": "7a7c290f-1359-4b66-89e0-0f3271595e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "a9ce8d67-ec2f-49fc-8b42-3e2c5e8ac5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "619e7908-8389-4dc3-b47a-07871c5c0195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 116
        },
        {
            "id": "f98c6c35-8a46-4c3e-8070-ab6e6388efa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "ab5ac068-596c-4783-93bc-4c03b6ab92db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 120
        },
        {
            "id": "f0a11474-baa4-4ef3-8028-8626e5d268f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 894
        },
        {
            "id": "6756bda9-eb7f-44e8-b574-546547d30ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "0f563cff-fab3-44ad-9ef6-049bfc06273b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "9807bb46-dea2-41d6-a3a4-c0964d31d485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "03766c19-6881-4f52-b5f8-75d19e6567b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 49
        },
        {
            "id": "ab5ee9f6-d320-416c-be59-a955de4c273b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 51
        },
        {
            "id": "63913bda-3a32-431b-906a-d8b5c94bada4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 52
        },
        {
            "id": "a5ce2a68-2c09-49dd-b13d-35cc382a1e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "d7f2dd20-5ebc-4d65-9f36-737768b661c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 57
        },
        {
            "id": "18ded94b-f0d4-43ee-8407-989c4beee2b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 58
        },
        {
            "id": "52925ec7-429e-41e7-9bc8-ddbe48578a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 59
        },
        {
            "id": "53517f75-06a8-4ae4-b06f-980ebbfef928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 63
        },
        {
            "id": "f03d2cdd-46c9-4fa2-968e-cc38551f319b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 92
        },
        {
            "id": "c6219f06-c833-4d2d-aeaa-8acbf649ab32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 93
        },
        {
            "id": "89666b3a-9569-4ecd-bd90-27d11c51f734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "095273ce-42cf-4e44-8f30-31c444d692e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "d1ea9971-1a5c-4ab0-93e5-8fd11abe70eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "c0a4bbe8-6605-43fb-b8bf-5c9e407f8a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 894
        },
        {
            "id": "d7ba3895-d68f-4f1a-a422-40bc9472362c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 33
        },
        {
            "id": "06b155af-7271-49c0-a8d5-14acfbb797ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "11953014-2363-4ae3-af1a-297b8c04774d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 35
        },
        {
            "id": "4ac67189-931b-4e99-8031-6383f52ba141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "2a58d44b-4159-45e1-b60b-3099c789ef55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 40
        },
        {
            "id": "5d7bfd4a-fe1c-4f83-837e-1d1dc5262d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "651b6b15-6e86-4936-a31f-167c7d121b72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "273d74ed-ba98-4448-8601-14fcd03ed64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 47
        },
        {
            "id": "2f306601-5f49-4623-9d10-e31835b34872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 48
        },
        {
            "id": "ee9548d8-f5e3-4c86-bb7f-a4fb25f0b7f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "08dac4a1-b69f-4115-975d-83778e64b3d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "c909f3a4-5584-481e-a432-3cc15fd6678d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "c83896f6-eeb5-4118-b3bc-0e99a5963997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 52
        },
        {
            "id": "68e30a4c-6abf-4492-a4e5-434070391623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 53
        },
        {
            "id": "cf0fe7d8-293a-47be-bf05-2712dff7f930",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 54
        },
        {
            "id": "00cc860b-11a1-4f66-8a8d-9e748ac59a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "a9e483cf-a0e9-43f9-8cd1-55deb760011f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 56
        },
        {
            "id": "d20668f8-d1cd-4d5a-8d6b-cea039f8f7ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 57
        },
        {
            "id": "c4c7b226-19a7-485f-841d-28011b5fef56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 58
        },
        {
            "id": "525fd3e2-6b6c-44c1-b141-00f1b78d1361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "cffd3d75-d5f0-4cba-b919-57e9b66ac401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "9e9d4b7c-d4ec-4e1d-94f4-7eecaa10f20d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 64
        },
        {
            "id": "96e4579f-8e78-473e-b78d-b29ed7098a1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "3db64143-c5bd-4c12-b229-933685e08867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "85d340df-385b-47c2-a1f9-e628be7c59f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 98
        },
        {
            "id": "061968b9-79eb-42bc-b2b3-185c749707be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 100
        },
        {
            "id": "20156b4a-e238-4456-ac7a-321451fa073c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 101
        },
        {
            "id": "5c51ba5e-fec6-42c3-a1ef-be68f36545b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "0a38b420-54fc-46e7-a91b-e5cb5d72310e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 103
        },
        {
            "id": "7529184e-b2ad-4f3e-8038-c30def93d863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 104
        },
        {
            "id": "c8e03f48-b561-4311-b40b-de019dc7da05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 105
        },
        {
            "id": "253be555-0108-44d0-b750-a5784fd99659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "5efe2567-6fba-425f-a9cf-289d0b2dbc10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 107
        },
        {
            "id": "a0f574f5-4e05-4ac2-89ae-617c1ce2621b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 108
        },
        {
            "id": "7ac4aeaa-b599-4bb0-89f0-fced90876f03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 109
        },
        {
            "id": "9b02a04a-bd1f-47f6-86c7-0bf98b37446f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 110
        },
        {
            "id": "56e9ad25-5f55-448e-8cb2-8d2ddad24a44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "a67543d9-f13e-4a37-b26f-96c2ceff5625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 113
        },
        {
            "id": "8b243d94-82fa-40c0-8062-489f41d6095f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 115
        },
        {
            "id": "d532c303-9a2a-406a-b990-502768b43700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 116
        },
        {
            "id": "83049960-e770-4342-8c86-af78af4ceaa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "7cc5f93d-12e1-4f0e-a851-0ed04b870ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "7a3eabdd-682f-41e4-8242-4200e676af72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "4fca4714-22c0-446c-97bc-ba0bf57dbd3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 894
        },
        {
            "id": "92ec1d7a-b6c4-4210-a217-ff25d46e10eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 34
        },
        {
            "id": "5fa5808d-b5e9-49fe-ab44-44bb0bf98292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 39
        },
        {
            "id": "306845d7-d543-4209-9f72-74b5adc3b11e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 40
        },
        {
            "id": "08a5fe55-2872-4f69-8447-9ef36cf35795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 41
        },
        {
            "id": "789c5c06-f4d6-4a7d-8b87-bd20cb268055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 47
        },
        {
            "id": "e8de2162-5ed1-4f2c-ad08-5a659ac91883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 49
        },
        {
            "id": "44458d01-caaa-4ee8-a057-ffbe7b1e9db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 50
        },
        {
            "id": "3cf952f6-fd5b-42cd-a1a6-87b47babfec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 51
        },
        {
            "id": "ea1c9f88-3689-4227-9e73-b6d0edcec700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 52
        },
        {
            "id": "16fc79ac-cd09-41ed-90a4-c650fad8375e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "4bb5e85c-3edf-4c69-bc5e-b73fc59e39de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 57
        },
        {
            "id": "0a9c8859-8f49-4fe0-8502-714b5632e199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 58
        },
        {
            "id": "03db699f-1525-43c3-bd72-86ad02eb45f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 59
        },
        {
            "id": "0b445796-4338-4c68-9466-109baf312497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 63
        },
        {
            "id": "b3614d4e-7f7b-4357-8a8d-4d2db22bdfc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 92
        },
        {
            "id": "29350f7f-5f80-472e-9fbb-4d5ef3e6b120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 93
        },
        {
            "id": "e2d50eed-95d2-4f4e-9b68-28fe4c90978c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 100
        },
        {
            "id": "49655f5c-0a9a-4fc2-a1e5-9c56019a12e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 106
        },
        {
            "id": "7dffc7a4-8f0b-4a03-9097-6e7dd235f561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 113
        },
        {
            "id": "71472a60-93d5-436c-86e4-e611125b6e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 118
        },
        {
            "id": "0e034b57-2a67-4874-9afb-71ff973ffc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "37cc9e11-163a-4e56-9de4-56a0e6c303fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 120
        },
        {
            "id": "79ae2d6a-20e7-46f4-8f4f-ba1c555bfd4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 122
        },
        {
            "id": "068575c9-d729-4828-9b7b-0334ffbe66a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 894
        },
        {
            "id": "ad8ff932-3c08-41a8-b135-5307299a5117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 33
        },
        {
            "id": "484e4430-656f-4db0-bdad-488537dcda59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 35
        },
        {
            "id": "ec13a6df-3cef-49d9-8587-61564ed54236",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 40
        },
        {
            "id": "7f5137ce-47f8-426b-9180-0e79c41fa7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 41
        },
        {
            "id": "db208a3f-6aef-47a0-b160-99ad576b7aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "16fa90b3-f726-4b8f-bb54-11c55fde91f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "4b80bca4-6175-4f8f-9a42-0a0b867f6e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "98952fd0-e0f0-4f39-92e0-0a75a479ca25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "8832209c-3ac6-4258-8fbf-d2fe6eb9e2c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 48
        },
        {
            "id": "18973e9a-9d60-467d-bc36-ce8e5d0732f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 49
        },
        {
            "id": "2fcc5f9c-e09f-41e2-ac5e-c2ab0660336c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "1515f0ea-9ca6-41b7-8252-1de8b5461054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "915330fb-79aa-49c1-bd37-b28b5bd87a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "79a3c336-7d2b-4766-bbee-89e779ce5334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "ea7bfdc2-a8d7-4475-931b-861c2744ce35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 54
        },
        {
            "id": "5a787006-8230-4222-bdd5-c97e769a0cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "8c795a5c-7d47-45ff-93c5-6d000f31f935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 56
        },
        {
            "id": "946931c5-f0ea-4054-b529-89a4d508196f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "f6788eeb-a9df-4a4e-b5a6-832174be93ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "c5c88af8-e86f-42e9-999a-cae76e402a8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "d6f495a8-3bef-40a1-86d9-425a5306e0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "a55fa1bc-aabb-4c8c-9f0e-eb17a7311515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 64
        },
        {
            "id": "a0f59fea-f315-4dd9-98cc-352ec46377bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 92
        },
        {
            "id": "030c40ed-0fed-439f-b8bf-dd02cd105fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "ba1c3c52-5114-4347-a7f1-2bd0fd4404d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "f6bfdce3-792f-47cb-99a5-90cb41dff235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 98
        },
        {
            "id": "51fe824a-2ac2-42f8-b1bb-06dd42adc402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "992baf73-02dc-46fb-ab61-b59ee0655eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "eb0036d7-a7aa-4c4f-b81a-80fb67cdde90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "fd1fb26e-1047-4133-9675-05b5c66ce77d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 102
        },
        {
            "id": "f01ae94a-fcaf-4a13-a7f0-9755a4140124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "d708b2df-bccf-4239-b6ef-a72b0f0249d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 104
        },
        {
            "id": "8e0958e2-0699-4a2a-b7d2-bf5402585400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 105
        },
        {
            "id": "b2264e17-b937-4bb4-aa52-674b9be563f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "157d186a-beb3-4c70-9c30-35b38601ac10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 107
        },
        {
            "id": "bcf1a14f-a6eb-4584-9570-b91051a48cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 108
        },
        {
            "id": "81acfe46-5226-489b-810e-2101cd6bd351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 109
        },
        {
            "id": "fbbd0767-f56a-4264-b06c-424b79274e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 110
        },
        {
            "id": "3c5ccbed-a705-4108-aa68-5a1cbb231237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "5c0fb18f-c4ec-4699-ad43-3db1b7e7655f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 112
        },
        {
            "id": "fb188ccb-63e8-4577-9dc6-537b60053831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "99a599eb-6479-44f3-9973-516db3e9fe1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 114
        },
        {
            "id": "f9ea6b1d-e3f4-4f5b-b0c9-05b1617496aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "ad8a467c-5ddb-4a13-8e05-5cd87a4b3129",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "81559a21-b3d8-467f-ad41-65a759eef099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 117
        },
        {
            "id": "78666407-cf1b-4726-8d9b-0b61ec31cea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "d9ae6a6b-6e60-4bb6-9146-388ba84cefab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "0bcae2ec-fa4c-4f7a-bfe1-5e3a5354546f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 120
        },
        {
            "id": "56fe428e-8afd-40ad-98c7-bd2ad8c3e55c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 121
        },
        {
            "id": "9cd35ca7-0ecf-408d-9473-656ba0203b87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 122
        },
        {
            "id": "bee10fbd-a998-4bb5-a507-9138b0b4ccf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 173
        },
        {
            "id": "c4e2badd-a34f-4f2d-a0dd-54f466ae8127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 894
        },
        {
            "id": "32a317ab-ce98-418c-bfce-a80d8b1af051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8208
        },
        {
            "id": "e1b30795-aafd-4e2c-96c8-c61eeb7112f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 33
        },
        {
            "id": "beccdea5-8d27-400a-9056-8879a5e0625a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 34
        },
        {
            "id": "453d6647-c944-48a4-87b0-2f5fd75fc1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 35
        },
        {
            "id": "fc7b0764-0c5d-475d-a2f2-f114d2007632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 39
        },
        {
            "id": "ba412f20-d995-453e-8df1-d940ec01347d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 40
        },
        {
            "id": "025f2279-40af-4883-a8ae-cfed1ff7b152",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 41
        },
        {
            "id": "012e71ec-2a5f-4ee7-9220-1ae80ab3b808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 47
        },
        {
            "id": "42edd3d2-22f7-42ec-bec9-5f68550f29b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 48
        },
        {
            "id": "e7db3f6e-a5fb-4cfc-91f7-2b7725b9a45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 49
        },
        {
            "id": "efd10087-7808-42db-949e-a18288f73ed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 50
        },
        {
            "id": "efaa3726-2b90-4a8b-a01b-280e5176f088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 51
        },
        {
            "id": "1539ae64-1770-4a5e-bb04-294fe565350c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 52
        },
        {
            "id": "cc682873-43f0-4fbd-9db0-7275198d61ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 53
        },
        {
            "id": "7eb6f66c-d644-46a2-8c48-af9f6da39278",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 54
        },
        {
            "id": "b922f9f8-a4a3-49d8-a66a-091ae4eac217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "3dd26da2-54d2-4def-bd66-62d9bb0140b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 56
        },
        {
            "id": "4d155708-173f-469d-9150-6e58588ce1b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 57
        },
        {
            "id": "d4b599a4-7e49-4812-9295-8d13690bacdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 58
        },
        {
            "id": "6669bbd3-6db8-4c3f-9a3f-ab5ae94b222a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 59
        },
        {
            "id": "79ba076e-cbf1-4a52-ba87-21adb46d9984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "6803e3fe-a340-4504-aeb9-ada27ea5a156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 64
        },
        {
            "id": "bf5315d4-0666-4e50-b73c-3baca52de301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 92
        },
        {
            "id": "1e4a39c4-2ab1-4e84-9d6e-20e7bf0251a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 93
        },
        {
            "id": "f775a8d2-8557-4753-8100-843a7c07d9e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 97
        },
        {
            "id": "a5f6de47-1087-4a6c-8753-0be6a822c855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 98
        },
        {
            "id": "67b354c2-6699-4eaa-ab05-3351395efae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 99
        },
        {
            "id": "512e503c-e201-4fa6-8d8f-f4f6792bdcf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 100
        },
        {
            "id": "07339363-6c25-4979-a919-330e5c6634be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 101
        },
        {
            "id": "514b0b77-e321-472b-8f50-fc3e351748a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 102
        },
        {
            "id": "f9fce4c0-45ed-41df-a980-b3652abca98c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 103
        },
        {
            "id": "22b99f83-929a-480e-b718-c9eb93d1f476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 104
        },
        {
            "id": "de63ba7e-865e-4456-b8d8-9e75dd3616e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 105
        },
        {
            "id": "30a7b670-b9dc-4ca5-b3c7-bb286239fcf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "f0a1b5a9-27d5-43be-aee9-68391aa28ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 107
        },
        {
            "id": "485275e2-f5d7-4b6e-aa80-9debd9837b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 108
        },
        {
            "id": "64ad55fe-ed91-4828-865f-92a4d27ce6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 109
        },
        {
            "id": "33885fcb-c2c8-4764-a6e6-9fb8813a7181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 110
        },
        {
            "id": "ce854f9d-ab47-46d8-b8fc-dbfd1bc962de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 111
        },
        {
            "id": "00f446e5-91cc-4ae4-b8f5-65ca24a094f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 112
        },
        {
            "id": "fdc6425a-b814-488e-a65e-f9755087141c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 113
        },
        {
            "id": "6b8cc7a0-0b69-4685-a1cf-62484480aa70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 114
        },
        {
            "id": "5f8890ab-9212-4f47-bc6d-7dd25d8ec797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 115
        },
        {
            "id": "f3066bac-027f-4c61-91eb-214912e39da7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 116
        },
        {
            "id": "8231afbe-7798-45ec-ae85-c1ce9ffe79f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 117
        },
        {
            "id": "45ab12a5-5b5d-4927-b1a9-85b75e9344d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "0c10bd4c-5056-4a88-84d6-c3b8bcee8621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "dd3cb187-d088-4d0d-9f00-acfb4f32e415",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "25ce285d-2b07-4745-86c1-ae893703b7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 121
        },
        {
            "id": "07a2b55c-e185-4c08-81a6-c1fdbdfb8655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 122
        },
        {
            "id": "ab8ab829-d317-45b4-937f-0b8e9b261f7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 894
        },
        {
            "id": "0ea86688-dba1-4c39-a016-8f6d6c438079",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 33
        },
        {
            "id": "5ac9302f-5776-4c94-9d62-b826fe74f1e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 34
        },
        {
            "id": "262fdfd3-ffcf-4c7a-8b1e-930f72e8f576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 35
        },
        {
            "id": "ddcac260-5b24-49d9-a410-77f0742b8c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 39
        },
        {
            "id": "8f26addb-4d67-4dc5-ab54-46e6dcb546f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 40
        },
        {
            "id": "86959200-5ff2-42c2-88c3-6a7b14cc6e79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 41
        },
        {
            "id": "99d9021b-7aad-4aeb-8bf3-f0eb2d5b2cf3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "2095d8f5-4b6d-4448-94a6-32b97ebfb41a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "b78ebbb5-fe2b-494e-9fdf-4c8e50a3249e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 49
        },
        {
            "id": "15df183e-73c2-4c6d-bcbb-8edede487a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 50
        },
        {
            "id": "fc021eb4-501c-4ca5-a9ac-f4b30fb1fa28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "33ec03c6-35ed-40a4-a648-e7222eb78397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 52
        },
        {
            "id": "49764728-8e5e-4261-80de-b117639e2bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 53
        },
        {
            "id": "827c6f84-b4ec-45d1-9ab2-d990e3f56ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "6b74804e-3285-4599-bbdc-5906dc092520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 56
        },
        {
            "id": "f24cbd6c-4831-438a-9799-2f9c2ff20974",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 57
        },
        {
            "id": "041d6c38-df92-4d41-b4e3-deac9adbd5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 58
        },
        {
            "id": "cc80b748-7f1e-470c-aae3-1d141b0ac441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 59
        },
        {
            "id": "ad282be3-0db6-42d7-9094-58a0c8245745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "0fef84ed-b25b-4970-a7cc-fea5b79638bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 64
        },
        {
            "id": "3dee9540-2492-4f32-8a88-091e63350367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 92
        },
        {
            "id": "9f659674-3f0e-4b7d-af68-ad72a2a35436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 93
        },
        {
            "id": "587ac0b0-2c29-4ae7-8ef6-9f76f3927932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "49aaab47-0ecf-42bb-b5cd-22ac5df16f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 103
        },
        {
            "id": "208231ff-75ab-4b07-97a1-3cae3e07e6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 105
        },
        {
            "id": "efda3b4e-f0d4-4e6b-8a85-8eb174bd2050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "10a6ea17-04cc-46db-b8be-0c1a39d84830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 113
        },
        {
            "id": "45ce62ae-0692-49ae-9094-27254b059007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 115
        },
        {
            "id": "7d21aba6-367f-4c3c-a7b4-38d71794d323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "2dcc981e-840e-4d1c-920b-ae42359efc11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 120
        },
        {
            "id": "2814ef1b-9253-4840-bc8e-8fbb6c3d786b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "1efa36b1-d2b5-4bfa-8ca6-2d95fd22f205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 894
        },
        {
            "id": "b1931102-19a9-4906-b378-eeda22f831e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "86349c83-a1a2-4aa2-b2d4-5d3eb84f7e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "f68a5259-d05b-4e54-82a1-d0cd2da111f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "4971ab3c-7771-4233-b40a-c8233dd08400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "02c0fa21-377b-4d28-afb1-c0c9a1198a41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 49
        },
        {
            "id": "3f1ab90f-97fa-4b24-995e-d98b6a9ba1d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "beef5a0d-ab28-4fc8-9617-bfffad08cdba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "b146584d-e95d-4a14-b874-527efe01bd7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "89f215bf-f285-470d-9bdc-06499479f148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 57
        },
        {
            "id": "6436a010-672c-4800-95d9-2c73ca2eeb48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 58
        },
        {
            "id": "d8d5f4ec-646f-42ad-9cae-c8bd302312e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 59
        },
        {
            "id": "b1fe5ded-0aec-4129-8440-831d49754c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 63
        },
        {
            "id": "2aca9984-4227-4a88-97e3-31de9d788d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "0b49a792-60bd-4cf3-a875-5c3bc9651c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 93
        },
        {
            "id": "b5ab4363-6979-4a45-90c3-5868ca94e85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "2b6dbc63-0c83-4dd6-9d89-f9a864200e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "95c2534b-f641-4bba-8e02-8e4fd744371d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 120
        },
        {
            "id": "6aa0393c-935d-44b3-a760-fe816409cbca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 894
        },
        {
            "id": "e934c31d-5a37-42c6-8e55-95900efeeb14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 33
        },
        {
            "id": "0f121200-a740-4e13-a56f-58fde2f32bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 34
        },
        {
            "id": "db5a7d99-57d2-406a-917b-5a69ab2621c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 35
        },
        {
            "id": "5904b5e6-bdf8-45fb-a6ac-7c8b2bb47fc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 39
        },
        {
            "id": "179d9b23-48aa-466a-95ee-06d81170401b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 40
        },
        {
            "id": "05f77bc6-c81b-454a-8735-b5e38ca28324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 41
        },
        {
            "id": "a086da72-e5f2-4d78-8f93-074fafa6cef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "b4c58c7b-0350-4817-91fd-d187acd4501d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "ae551015-ed7e-42cb-890c-be9d6b20a86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "1217945f-39f1-4013-88ab-1d34649060c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 49
        },
        {
            "id": "f91b718f-fc76-46aa-a53a-a40aad8506db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 50
        },
        {
            "id": "1590390e-2821-49cd-8275-4ddf7a47a3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "90082227-d401-40c3-afb2-1b01900f217e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "954551e1-447d-414c-8ebb-43c658b01a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 53
        },
        {
            "id": "f1e8374e-2763-40cd-8389-39ef50fa154f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "0bfc0256-f284-4d91-aee9-030cc54ab516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 56
        },
        {
            "id": "68bfbc95-0bde-4dbb-8dab-91b46dc3763b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 57
        },
        {
            "id": "9a27acf7-957a-4a74-bb35-f75b4230d7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "2f0a9863-362f-4199-9976-41d157ace644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "ed2e32e4-4112-42f9-8176-8155bab9b4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "a734abc9-a583-4370-b73f-03fbec9186bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 64
        },
        {
            "id": "ea22fd77-5b26-41d8-8370-b3747c014b83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 92
        },
        {
            "id": "6305c70b-0fa5-4697-9bc3-cf02a54b08a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "c13d6cce-63cb-4076-a410-bb7feaddb526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "fe167fd8-8704-42b1-9848-ba54127eb57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 105
        },
        {
            "id": "6ecbd7a5-ba0d-41b4-890f-bb8f9457458a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 106
        },
        {
            "id": "51c838d5-48a4-4f82-8607-e4a5a836aa0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 115
        },
        {
            "id": "43f7a1c7-7d83-499f-a40c-0d5c8e94c799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 118
        },
        {
            "id": "8c0f1b37-76fd-4b7f-b899-85a8852bd2dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "7b21eaf4-2fd8-41e5-8873-d73aba73ad92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 120
        },
        {
            "id": "43ccf0bb-ee42-4a50-97a3-2deea0b972ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 121
        },
        {
            "id": "822d70af-e655-4517-b1db-d2f85fa655ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 122
        },
        {
            "id": "e7b495fb-f2b0-462d-8bc7-de260d219c27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 894
        },
        {
            "id": "19aabdab-cf66-4f38-a354-fcb11e86d9f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 34
        },
        {
            "id": "ec242914-ab98-4d40-b713-617abc77ed11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 39
        },
        {
            "id": "640b9273-b11e-4366-9844-9a8e28b6b39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 40
        },
        {
            "id": "d0596319-7815-4db9-b21f-ebb8a2d4b36b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "f9e67462-a8ed-43b3-a8d6-85ffa40291aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 47
        },
        {
            "id": "fcb5d91e-8d18-43f2-9da4-d6f467988b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 49
        },
        {
            "id": "ede2de70-ecee-4815-b715-fb1d7045c04e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 50
        },
        {
            "id": "e3f4292f-8490-4681-b298-ca4e091d4016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "33d0f3b9-3380-462d-871e-b4d5625eb779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "fc98a9de-391e-44fa-99ca-ab45e8febfb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "9299b328-a0e9-40a9-b4ff-e1e95c132ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 57
        },
        {
            "id": "f4720e3d-2692-4db5-8d28-953d9ff45ddb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 58
        },
        {
            "id": "8435b1f6-be4e-4b53-abcd-3113e28f1a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 59
        },
        {
            "id": "345ecdbc-7892-4d68-afee-3d0e6a6264af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 63
        },
        {
            "id": "efe8f37e-58a8-4a46-9cf0-1db570a39cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "a4ed4478-d159-450a-a25a-5d0e370e1c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 93
        },
        {
            "id": "ceb58e0e-c661-4c52-a514-9c9f6157389b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "d1724120-c5e9-418b-994c-f60116a916c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 103
        },
        {
            "id": "4e061496-9879-4da7-beb1-e4d85e74b345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "e5d7e0dd-5789-4c34-b483-a041610105e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 113
        },
        {
            "id": "7a06222c-77b4-4ed6-86c6-2880531e4260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 118
        },
        {
            "id": "b0daea88-39ae-4607-bec9-acfc7118beb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "c3463432-843f-4566-b273-1f477ac8d1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 120
        },
        {
            "id": "78fa8f37-5fd1-4a83-aa41-01c5ef8ff1f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 121
        },
        {
            "id": "2bc8886b-8e97-4e4d-ace3-e56841ef1cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 122
        },
        {
            "id": "41c661b1-38ab-4898-8f64-6e20e17fb551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 894
        },
        {
            "id": "b748f23a-a45d-4835-9b73-b0043af0bb7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 33
        },
        {
            "id": "ca009bfa-298b-4ae2-a383-6b8859289c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 35
        },
        {
            "id": "f63e5d54-1bfd-40b0-b4a3-d32b86dc40aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 40
        },
        {
            "id": "60f1cfaa-c325-464c-9f1b-e54c9a26a8bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 41
        },
        {
            "id": "422a4bc4-60e7-4a7f-899c-f3952ef31414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "f407cfc0-a4fc-4bd1-8964-c3e5aabbae21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 47
        },
        {
            "id": "6ca5b80f-b59f-4348-ad03-ec58e171124d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 48
        },
        {
            "id": "85f63bab-793d-4569-a00f-14ccf4c619a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "bd66ca26-4c1d-4601-9d0e-35728140f40e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 50
        },
        {
            "id": "03d94043-a572-41df-a484-25797754ac70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 51
        },
        {
            "id": "4b90b300-8c58-4513-b8a8-c8c528752630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "b2bf691c-cb7b-4f99-aa11-147c95ecf9cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 53
        },
        {
            "id": "ad8cb078-9eab-481d-88fb-e5c82ecd3ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 54
        },
        {
            "id": "34ec4442-d202-4284-ba73-2f04f83885af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 55
        },
        {
            "id": "bbfcf59f-00d8-4964-95f5-cbb8bd32a736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 56
        },
        {
            "id": "388ae55f-f037-49b0-8957-40f27d2647c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "a8473db5-0fc4-46b5-8854-890144bd0cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 58
        },
        {
            "id": "5773ee6c-2199-4c54-946b-4bc24cf17731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 59
        },
        {
            "id": "671c14f1-978d-41c0-ba93-e2ca2657a304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 63
        },
        {
            "id": "61861177-e45b-482f-b569-e60e0d7c29ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 64
        },
        {
            "id": "c57246dd-8ae3-4368-bbac-94d7a0251eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 92
        },
        {
            "id": "99a131ed-ac89-4210-8a79-fe36d117e98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 93
        },
        {
            "id": "93081bb2-d0c6-4298-b591-46933bbcfacf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 97
        },
        {
            "id": "6e3017e8-e83e-4e01-844d-95b1b02529ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 98
        },
        {
            "id": "3ad6c22e-8b66-4fd2-b88b-9cffb88e84c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "c3db24c1-6622-4280-a371-36b2e135b88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "cfb34fd0-af22-4af1-8582-92198aecca9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "911ca9c7-47f5-4eb9-b07a-ee56a19cbd0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 102
        },
        {
            "id": "5a2e383b-e9b3-4f51-889e-9078d6f7027d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "a42a2c38-4bfd-4dfb-8c25-bd57261feb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 104
        },
        {
            "id": "0f675a41-ea32-40b8-9570-57d9701157cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 105
        },
        {
            "id": "dd1bd596-2c51-4fbb-b9c1-f127c680d9fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "3c39f897-9e20-48fc-b726-2e2535afb928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 107
        },
        {
            "id": "1f52797c-1d2b-479b-b024-3c4cc92155ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 108
        },
        {
            "id": "c036ab20-6227-4d48-8619-92de27380622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 109
        },
        {
            "id": "fbd95bd1-afc0-432b-a4d6-664b397e6af0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 110
        },
        {
            "id": "9a0c93b8-0a74-4e1b-92be-b77f99bfa14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "e5a2d2e0-a416-4c0a-b669-6ecdeecfd327",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 112
        },
        {
            "id": "fb3a33f2-8b88-4731-ad20-cb38ffcb2912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "e1c37b39-3107-4277-ba7d-233d8029ffba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 114
        },
        {
            "id": "86bb91ea-557d-4924-bc9d-36aa1dfe6b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 115
        },
        {
            "id": "c27677b4-d050-411d-a48d-8130f8ecb334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 116
        },
        {
            "id": "943edbdc-a754-4885-a765-0c5fa58f812a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "6783a766-6072-45a3-9387-c77407d8d83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 118
        },
        {
            "id": "b7421a56-f1f2-4659-a25b-0a845a56a017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 119
        },
        {
            "id": "446bdfe1-0673-4ed1-96b2-9f801a42ce77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 120
        },
        {
            "id": "d37c8bec-d7c4-4d6a-809d-f927d58e1a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "d26545af-8d82-4db6-83bc-0364d48047c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 122
        },
        {
            "id": "b4345f77-b549-4aa6-853c-cf3fa33b5066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "0703031c-275d-48ba-ae57-5f170e409f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 894
        },
        {
            "id": "006a0c55-f21a-443a-a395-6125eedda9b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 8208
        },
        {
            "id": "963a756f-b5ed-421f-bbd9-32ee9a53a8cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 34
        },
        {
            "id": "d271bb52-6fad-4056-8937-4a7da8cc28f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 39
        },
        {
            "id": "4ae04463-148b-4f89-ad48-ad0358754870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 41
        },
        {
            "id": "14c8bbb2-4892-48b4-b27c-40f6ab4f396e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 49
        },
        {
            "id": "beb7f2f9-5e68-4db4-b88a-658d292ccd7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 51
        },
        {
            "id": "deb412d9-f8e6-40db-943e-ca8dd7af3ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "5d3aaa7d-6904-4be7-9bd3-4ce09c480941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 55
        },
        {
            "id": "19e4cb66-0c8a-48c8-8aa0-9bf644455a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 57
        },
        {
            "id": "4ece3313-734d-40c0-b14b-12aef8fdad30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 58
        },
        {
            "id": "7b1d5c7f-6620-41d6-a46e-cf659277697f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 59
        },
        {
            "id": "dd69a3dd-998a-40cc-96cc-b95411f282d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "10cca0ab-e2b1-43b7-9b72-6783b4522c7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 92
        },
        {
            "id": "7bb089be-2d88-4fc3-b187-c2c91ccb05d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 93
        },
        {
            "id": "38118557-5b1c-4bda-804d-cd182a26ed9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "3f9dfa51-1e1d-4e7f-a17e-71265873a3d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "ce278a43-24a1-4c84-8455-efd1f8f0bdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 119
        },
        {
            "id": "40c067ac-1b28-4ab5-a66f-04be8f7bf0d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 120
        },
        {
            "id": "54fc2205-6e9f-4bb6-b901-38b914004f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 894
        },
        {
            "id": "9f79046c-7e37-4883-9c21-25fa9795423e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 33
        },
        {
            "id": "68349b7b-70b1-4749-95c3-4beee2f0977c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 34
        },
        {
            "id": "d26f0715-3b93-4a5f-bb8e-5e153d07327b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 35
        },
        {
            "id": "c9ebd8b0-791a-426f-81c6-9b4dfdac485c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 39
        },
        {
            "id": "884f2f60-1d1c-4eb9-ad8e-5ea047e1ecfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 40
        },
        {
            "id": "eecdcde5-bd1b-4c63-a624-2bc2804a99c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 41
        },
        {
            "id": "d8764722-433a-4524-afb9-fe18f597d650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "9daa886e-8bef-4e80-aafd-34fe416301b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 47
        },
        {
            "id": "812741dc-c468-4a39-8fa1-cbb2a9cbca29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 49
        },
        {
            "id": "af6aaa1b-d7a7-4fca-9513-730e0b927694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 50
        },
        {
            "id": "87e45cae-49bc-4636-8e68-0d09031693c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 51
        },
        {
            "id": "2fa169b6-f1fd-4fbe-8ee7-cd0c2df3ef3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "44d5f7cb-3f8b-4fa1-ab64-dafae3fac90e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 53
        },
        {
            "id": "6d6eb4e2-961f-4484-bed3-5b66e5169d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 55
        },
        {
            "id": "088a7b4a-89e3-4a0a-b5e2-8ee763666399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 56
        },
        {
            "id": "bcb3d3d0-c8d4-4f15-8386-9d28000ae5e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 57
        },
        {
            "id": "ea7960af-c16c-4081-9125-922371bfc64a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 58
        },
        {
            "id": "bad95f98-ca79-424c-b547-84c11e63f22d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 59
        },
        {
            "id": "25a904ec-ed5f-45e0-94d7-ce0be8a3b3d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 63
        },
        {
            "id": "f2774735-b189-48a8-8e3d-26c7ea00d703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 64
        },
        {
            "id": "36164760-3d8a-4168-9268-49822a84d9f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 92
        },
        {
            "id": "7717c501-679d-4db7-9789-cece861bf9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "8a95131d-5566-4e9f-b617-43663fbccfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 103
        },
        {
            "id": "158873aa-8c10-4ef2-aab8-46447a3ae6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 105
        },
        {
            "id": "d3aafc65-69e7-4378-aa01-501ba3097783",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "5be35457-361b-4adf-89da-461c1d9d6a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 115
        },
        {
            "id": "9da3ed1b-e9fd-476d-9c56-1c744763be2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 118
        },
        {
            "id": "ee80e9c1-b223-4cf1-b461-6d099ce364d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 119
        },
        {
            "id": "62866da0-1ea3-431b-8e64-f41632800365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 120
        },
        {
            "id": "04303276-7921-46b0-a6d9-26e43b373dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "6cfd6591-178d-4833-a71b-201a63dc1b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 122
        },
        {
            "id": "509d558e-36e1-492f-9d63-93485d9d21ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 173
        },
        {
            "id": "0180571c-01d6-4fed-bfa0-8f9ab42999c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 894
        },
        {
            "id": "923794ad-ec1f-4d8e-8b8a-91c55e894121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8208
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 13,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}