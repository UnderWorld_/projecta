{
    "id": "b89c224f-cdac-472b-9e04-d5c1cc17c675",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font4CS",
    "AntiAlias": 1,
    "TTFName": "fonts\\font4CS\\BreatheFireIi-2z9W.ttf",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Bulky Pixels",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "74935127-bcc7-4842-b48d-c0807a178aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e5ce9322-5f49-40bc-b542-9893690919be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 121,
                "y": 65
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "793e3819-c364-4257-a072-e4749b2ad69c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 109,
                "y": 65
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "272cced6-9ed6-43f4-ba83-b8dd16378ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 15,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "92156028-66cb-4a4c-9240-210afd93329c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 80,
                "y": 65
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c1173f65-ef8a-4770-ac0b-6ec7abf47f46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 25,
                "w": 20,
                "x": 58,
                "y": 65
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "398875f9-7b36-4477-ad59-c1409e5a7348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 42,
                "y": 65
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dba58038-e5bf-44e4-be98-082b8eb57501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 36,
                "y": 65
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "62c13404-3593-4024-935e-84d0482ba187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 26,
                "y": 65
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b0047dab-b0b7-40b6-8dbe-b4c8e468819d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 16,
                "y": 65
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2f3de5aa-7f6a-4e6a-82d1-899cf7c4ea26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 15,
                "x": 127,
                "y": 65
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "efa836d7-8073-448e-8905-a9778211c30b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8c99649d-5bf6-4f36-ab82-6acbdec8a29e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 234,
                "y": 44
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "a31e442d-ea3b-4186-8992-d72608ee1b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 9,
                "x": 223,
                "y": 44
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "33ba1e9c-b19a-4f5d-8b0b-e2d2df69f56d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 217,
                "y": 44
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "669f8ba4-0570-42ea-ab3f-f6d7bb435b46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 207,
                "y": 44
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "69c7ab9c-8db0-46ec-ae56-b5a3bc8052fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 193,
                "y": 44
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "21122830-e974-4dfe-9a2f-6d2c2a067f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 185,
                "y": 44
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "83dddfc8-b1ee-47d3-a07b-f589ae51652f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2acfe63a-f890-47ad-8178-52fb839db622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 161,
                "y": 44
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "121f9fd3-35a5-4a3f-8324-e9479c9e144e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 149,
                "y": 44
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "22f8bffb-9ef6-4a8d-babb-1940f0af5849",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 240,
                "y": 44
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9bf84c07-8d78-4ab6-90c2-f6a1a16b0e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 144,
                "y": 65
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "199a8636-dba7-42f3-92bd-bac57725a258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 156,
                "y": 65
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "b1113812-0a30-4cce-87be-dd8422f9f515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 168,
                "y": 65
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7723d829-b728-4914-af99-3d2a8511c41a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 188,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "53a2f82d-dc6b-4c8f-abba-312869a8c825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 182,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ffeafa07-863e-4a27-b5df-c7b9d2015141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 176,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "0a2760e2-18e7-4ed8-81fa-a351c1ae3bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 163,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6e11b9de-1277-49cd-8bd5-f090218313ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 11,
                "x": 150,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "b439a8cd-2fdf-438e-9675-88cfec3f4068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 11,
                "x": 137,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8af386ab-defd-4fa2-a74f-71e08941cef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 127,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "700a6e5d-6ad7-4087-b001-7a877b3d225a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 20,
                "w": 15,
                "x": 110,
                "y": 86
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d5b537af-3088-489c-a77c-da393e131703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 96,
                "y": 86
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8c296d02-d8e2-4d80-a658-198df9d804a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 80,
                "y": 86
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "73beeb82-30bb-414f-bd63-ba5ce0291d55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 68,
                "y": 86
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "30c4fa1e-2c25-4309-8e75-560b07aac02a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7d4a52cf-1235-4d7e-9907-11376a564942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 42,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "4ddd45c3-3f45-42d0-b781-a0a70aba8189",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 30,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "0e293d4c-f962-443c-ad77-8137b51b2761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "587d6580-ea13-46e3-ac31-3798cff40fb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "0f932230-fbb1-4975-b68f-975269f3c134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 239,
                "y": 65
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "c64bcf51-4e7d-4f05-8c84-8fecf8cad161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 227,
                "y": 65
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "9c58fd58-7bf7-4493-8a22-5673679f7e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 213,
                "y": 65
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c82a7afc-b8fe-4057-ab63-41d29c35aa89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 203,
                "y": 65
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "3f1f9796-ce0b-4ef1-823b-612739d4d82a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 182,
                "y": 65
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "d54604b1-9fc4-4293-91b1-b42823ec5cde",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 135,
                "y": 44
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b79c5c28-8da2-44e4-b889-0b686ff7bdca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "498e3500-ffd1-4bbc-8d59-133ebff6eb5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 107,
                "y": 44
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "60e10054-8173-4371-84ab-dc102ee88ef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 52,
                "y": 23
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "8a35a8db-4b43-450a-8c58-c615a0760d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 23
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "f2fd07df-a8f2-4720-9fa2-4dc4a69a13ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "42351a03-fd97-4ff8-90f3-2f4da05019f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "81495aaf-fb36-4db1-9a55-33555179a20c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 0,
                "shift": 18,
                "w": 14,
                "x": 237,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "33334f84-25d7-4198-8002-b7c4eb88d318",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "d43d92fb-105f-495c-9c7d-31c8a7ea51ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d4b930c0-8a17-4f30-8cb9-06718ee8b619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "bb2a1245-dd48-4827-aa5c-7b78dd579e50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 174,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "da166f78-bbff-41c4-ad0f-8f26d2310b05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ffaee8e6-ac1d-4226-a42e-cc5b22d6f818",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 44,
                "y": 23
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f8682952-b122-48c7-badc-5e5487cc3b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 8,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2b8ff01a-69c1-498b-8cf3-80a6aeb4be9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f91116b7-f7a3-41f3-8eb7-374fab3e818a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7778bdbd-71aa-42f9-8145-c401904dda6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7107a65c-c331-4396-83a0-aec213e37c77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b640e2f9-69e0-4a8b-9429-4eda7273319a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 70,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "8f7b3265-da17-428a-a795-d565ce05432d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "199641d5-868e-4660-a7e3-d4093a66f7aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "09abfdda-1672-4b1e-830d-7c10e2593e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "65061766-40ac-4fda-a44e-87a28694a9a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ac6eb350-66ea-45ff-8981-ae940f2717df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 8,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "03d98672-7ca6-4994-9252-5613abb6eb16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 68,
                "y": 23
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d23a9d41-d072-4486-9689-8ff9bc744aa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 193,
                "y": 23
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a6cce07e-467c-4a8d-ab56-b7affa5bd16d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 4,
                "x": 82,
                "y": 23
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "64d1b28a-ff34-45ec-9e5e-2f340b2ec5a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 87,
                "y": 44
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9783ec2c-d196-494b-8f84-ed2e6bb4546c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 73,
                "y": 44
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "be962c2f-6e8e-4033-adb5-7a8ac5bbebca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 6,
                "x": 65,
                "y": 44
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "99b5aa1a-08af-4f76-9c55-4bed09f77b09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 44,
                "y": 44
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "01759f85-b0d6-4bef-8cc9-cbaad6d478d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "17ac073d-0a2f-406d-ae4f-f3d3070817d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "ba4151c2-9bb2-46ec-975c-032738b7e7ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "0bb8bb13-c4f8-4645-b5b0-a890296b0237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 17,
                "w": 12,
                "x": 231,
                "y": 23
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "195ceebe-76b8-4391-a721-7b62b7122b7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 219,
                "y": 23
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "f70aec1f-769e-41c0-a91a-1b9fe716cc79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 95,
                "y": 44
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c46f15ea-0bf6-461d-9c90-167627411374",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 10,
                "x": 207,
                "y": 23
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a996e893-002d-4a32-86ba-69bbad7d9cc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 181,
                "y": 23
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "cf1da993-0386-4086-97cb-9b29cabee971",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 169,
                "y": 23
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3fb30130-d2e5-498e-bbf4-f9ed1dc137eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 148,
                "y": 23
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "695d080c-fd0e-4e8e-a088-670b7451c6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 134,
                "y": 23
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6c05aa3f-2e03-4fb7-9b43-c720cb5af03b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 12,
                "x": 120,
                "y": 23
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "47a2be0d-bb65-4bf4-8fa5-6ef06673dd0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 10,
                "x": 108,
                "y": 23
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "60cf1a07-55e9-4a26-8a14-bd9042b616e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 102,
                "y": 23
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "55b32ba4-d73a-41a5-8012-c860caf88342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 95,
                "y": 23
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "0e6a2995-b66a-47c4-986a-75d46398413f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 88,
                "y": 23
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "adc58dce-3e33-4b8c-88b0-1d5b12297c1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 24,
                "w": 19,
                "x": 200,
                "y": 86
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ea9db6c3-6e2a-42d5-a968-f05c20a7e906",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 221,
                "y": 86
            }
        }
    ],
    "hinting": 0,
    "includeTTF": true,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "5b79d78b-5332-4084-8fda-d2a29226c825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 33
        },
        {
            "id": "54bc2d56-546b-4724-8f26-e6873487225c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 34
        },
        {
            "id": "2e417d70-01e8-4882-8441-1d6ce22ed82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 35
        },
        {
            "id": "00dc8abb-616f-4ee7-a7d7-91f1a3fc837c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 39
        },
        {
            "id": "c905c0ee-0b54-4ff4-b348-f5e16c10466f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 40
        },
        {
            "id": "a94df42a-47bf-450d-9175-714d22837f56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 41
        },
        {
            "id": "c116f6e6-041b-46e3-9121-5a16fde89680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 45
        },
        {
            "id": "3f8dedb9-3a1c-4116-a57f-2ae8b4764ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 47
        },
        {
            "id": "afca1261-77ec-4c8d-8759-5f2492ab18d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 48
        },
        {
            "id": "49970492-573b-408e-a655-e64c8404774c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 49
        },
        {
            "id": "e8f27614-3553-4084-87d7-0809638df12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 50
        },
        {
            "id": "38ea923a-51bd-4344-b3ab-a28f72b3260c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 51
        },
        {
            "id": "91d25beb-7ad9-4af9-9315-2253e52a9f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 52
        },
        {
            "id": "4a07b30a-9829-4aaf-898a-4fa89a77a95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 53
        },
        {
            "id": "870f038d-5104-4bb0-b8d1-e06b1da33572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 54
        },
        {
            "id": "19bc0563-4dd5-4067-adbc-0380c4e3ca19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 55
        },
        {
            "id": "87e23053-5426-46f2-8880-9a39a846e7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 56
        },
        {
            "id": "1019100a-6964-4810-b5bc-5e170cc5ec13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 57
        },
        {
            "id": "ca3bde7e-a65c-48e8-adba-c663da7d0c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 58
        },
        {
            "id": "0574421c-8212-4258-9474-673d6f0586c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 59
        },
        {
            "id": "a09a67b6-ae28-4d71-bed4-b37abf4fd21a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 63
        },
        {
            "id": "25745bf1-20ab-4466-a607-00abf3ab8920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 64
        },
        {
            "id": "1c57f5bb-7b47-49b0-9d3a-2259b83a8743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 65
        },
        {
            "id": "d4979eaf-4997-42cf-af09-bc125d2d3dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 66
        },
        {
            "id": "e3c65b8b-4dc6-4a99-aba1-fbf916f3ced0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 67
        },
        {
            "id": "e801ba92-3770-4b75-b82d-7e8abf09cc43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 68
        },
        {
            "id": "1672bf24-412c-4a42-a3d5-5b3c42dda97e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 69
        },
        {
            "id": "0af589fd-0953-4c26-8292-6ceded314bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 70
        },
        {
            "id": "a5a8cbf3-c3a6-4b5c-be7c-efa53042658e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 71
        },
        {
            "id": "c7520eb6-5570-42aa-9440-801969bc5430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 72
        },
        {
            "id": "092b0555-880a-465e-89fc-bd6496cab7c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 73
        },
        {
            "id": "c3501d46-3ba9-411c-b5d2-6ee2102d1fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 74
        },
        {
            "id": "7c6b5b17-657f-4fd4-b4df-b482ab08bf9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 75
        },
        {
            "id": "2162e589-a558-43ae-8757-4d95bf000b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 76
        },
        {
            "id": "bb252e19-c58b-4551-9dbb-55695bf5fec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 77
        },
        {
            "id": "1b4695fe-3714-4f8c-97bf-6aa79d46a0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 78
        },
        {
            "id": "600777cd-eea6-429e-a027-6b68cd66932a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 79
        },
        {
            "id": "5dcbb2d4-c2c9-43a3-b115-d81da0f467f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 80
        },
        {
            "id": "dcc78751-5006-4d97-8d8d-0ecac2d8f245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 81
        },
        {
            "id": "8a881ce6-11a0-4a37-942e-e46540262cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 82
        },
        {
            "id": "27d2a3e0-ec95-4288-b00c-29ed87eee01b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 83
        },
        {
            "id": "b839d12b-ab85-430e-8b8f-1cd36c31cad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "02b070b1-b856-495a-b1cf-a5702a7d8fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 85
        },
        {
            "id": "ca72c8d6-0c03-4334-af1b-61dce7154301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 86
        },
        {
            "id": "b9a58899-e171-482a-8108-04fe1441140a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 87
        },
        {
            "id": "cb9eab11-7400-471d-a5da-222ef3214897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 88
        },
        {
            "id": "9002218b-17d4-46bf-a350-785da7673e2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 89
        },
        {
            "id": "3a7f840a-d8a2-4a68-be6c-3d06ce792331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 90
        },
        {
            "id": "88639654-c970-4242-9102-bdf8c30cf692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "3cabac87-6ade-41e0-9f59-4793632e58cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 93
        },
        {
            "id": "7da0e48b-d88b-41da-a22a-732f5a5e1249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 97
        },
        {
            "id": "8aa46c45-82ff-4682-9f60-e1a82e84a2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 98
        },
        {
            "id": "0c9a7403-8f9f-4356-ba79-56ee3614f9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 99
        },
        {
            "id": "c5ba04d7-b6be-4ff2-9b72-c19e4759e803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 100
        },
        {
            "id": "3a6fdf63-9ea3-4fdb-89d2-b72688027899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 101
        },
        {
            "id": "eeeedaf0-d942-443b-a2f8-088171bece8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 102
        },
        {
            "id": "91dd2a65-2895-4163-a292-b084846aea21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 103
        },
        {
            "id": "ac5d5d69-71f8-444f-a8a5-8870d70fbf21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 104
        },
        {
            "id": "9e47768b-b63f-4d9b-aabb-0a5cc039ec5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 105
        },
        {
            "id": "55c9e34e-6394-440d-876c-649fba8bffcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 106
        },
        {
            "id": "a57bafe7-8543-4b25-9af2-af8632986339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 107
        },
        {
            "id": "ef94a1f8-7177-4d68-a07e-425da0c0eb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 108
        },
        {
            "id": "2e506f39-e281-4b98-b49a-773a4b0a9a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 109
        },
        {
            "id": "3baa971d-3096-403b-9b74-7f041d8167d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 110
        },
        {
            "id": "5e46af0e-5006-413d-84d2-8970fd517503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 111
        },
        {
            "id": "71546a48-fa24-49c7-b473-21a3f24baa8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 112
        },
        {
            "id": "208a334c-e592-478c-bfe8-fed5b5d142b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 113
        },
        {
            "id": "c5652246-f996-4b4f-8295-cae0b09a171c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 114
        },
        {
            "id": "68fe0b38-e4d3-46e1-a269-becf85a190fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 115
        },
        {
            "id": "e6351555-35cf-4108-813e-ad3ae607bd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 116
        },
        {
            "id": "7c4cb78c-9104-4967-81ce-984aa1149826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 117
        },
        {
            "id": "dbb00174-d697-4e8c-8d2f-9c9a12dbc9bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 118
        },
        {
            "id": "088b5042-9a8a-43b5-8a95-49330a3abcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 119
        },
        {
            "id": "6bb65f05-f86c-4603-bba3-960bb5c44868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 120
        },
        {
            "id": "09e238d1-ec0b-4ac8-a36b-9bb0a28c23eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 121
        },
        {
            "id": "70004169-ddef-4bf9-9e36-4f965b68456b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 122
        },
        {
            "id": "18543ca4-7887-4f6c-a9eb-06b5d0d1a591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 173
        },
        {
            "id": "b709584d-0a07-4b09-8046-190b64b023e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 894
        },
        {
            "id": "11595c82-7ea9-416a-9094-6ad3ac556458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8208
        },
        {
            "id": "b513fe55-2826-4ef5-8fc6-e39ca66a775c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 34
        },
        {
            "id": "4a543a45-5f09-4695-b610-e859dc17c66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 35
        },
        {
            "id": "b77474d8-b50d-471a-ae2b-9d8ddeac2748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 39
        },
        {
            "id": "a6a3d3eb-b9e0-4c22-9520-3ea37383dfbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 40
        },
        {
            "id": "88df0093-4a2d-472b-b890-34d48c7668d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 41
        },
        {
            "id": "d77413dc-2b94-4dab-8b1f-f5116d93453b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 34,
            "second": 44
        },
        {
            "id": "458e8d31-6f8f-4ede-a12b-6e46289e68a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 34,
            "second": 45
        },
        {
            "id": "dd7d7a12-1ce8-42c8-85fa-5e4ba976d14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 34,
            "second": 46
        },
        {
            "id": "a4f828c2-64f9-4294-9fe5-adb8b0274aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 47
        },
        {
            "id": "fd8050fd-9cae-414f-9540-1aff71faa518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 49
        },
        {
            "id": "571c4255-0d35-4d59-8875-1e8978217a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 50
        },
        {
            "id": "c9af04a5-31c6-43d1-9c78-02e105e8678a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 51
        },
        {
            "id": "05e1b7f3-1491-4696-963a-8ef577f21246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 55
        },
        {
            "id": "92cea980-2675-4385-9abb-eedd32b7ede7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 56
        },
        {
            "id": "72059c20-653f-4078-8cca-828eeaa2bfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 63
        },
        {
            "id": "fd110f9f-db83-4ed8-a8e4-9f555c638628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 64
        },
        {
            "id": "dbbb016e-86b2-48c8-9abb-c44836ae4378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 74
        },
        {
            "id": "8dfb1e36-1027-4df5-9a34-b5f3c441e02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 84
        },
        {
            "id": "fb24bc0b-ff9f-4f88-bba8-bb2c75dde83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 88
        },
        {
            "id": "2f53d173-d7a8-49be-81e2-cefcc94f2f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 90
        },
        {
            "id": "12c60af5-5f4d-43cf-a4ad-f203eb177900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 93
        },
        {
            "id": "7c7bdb11-d637-4cd6-8b8c-f811cc03a67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 100
        },
        {
            "id": "d7f4eb13-34b4-4acd-8e0d-040e1d75e472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 103
        },
        {
            "id": "8ad44f76-d54a-4a0c-9581-d4d66ca5582c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 105
        },
        {
            "id": "0071a1ab-b4d3-467a-9501-11d438e28945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 106
        },
        {
            "id": "a7d910de-7a3d-456c-8213-5ecd8973ca9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 113
        },
        {
            "id": "8765bb80-4a78-4aff-bf2e-783bbb44a7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 34,
            "second": 173
        },
        {
            "id": "91b8e5ff-372b-4e25-81db-329e89f12937",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 34,
            "second": 8208
        },
        {
            "id": "8998897b-e6d8-4d36-8adf-b4161792e841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 33
        },
        {
            "id": "30517c8f-4ff4-4493-803a-81f4529e4603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 34
        },
        {
            "id": "bbdf94fa-f7ec-4512-8841-1d36aa029486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 35
        },
        {
            "id": "cf6c16a1-1ec5-4034-a9c3-9ac7ef4be14d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 39
        },
        {
            "id": "a9f6582e-6dd1-4c48-a277-581b136a72be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 40
        },
        {
            "id": "189ddc9c-b6b9-473e-9c6b-327883598496",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 41
        },
        {
            "id": "772048ec-5d82-45a3-968f-141804263ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 47
        },
        {
            "id": "c0b11165-48de-4f9b-9188-71c5a856b46d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 48
        },
        {
            "id": "94c91779-1bdd-4c99-8a03-531683cd71e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 49
        },
        {
            "id": "b49e836f-2376-4665-9fea-a7759ef7a16e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 50
        },
        {
            "id": "8f0e80d6-ff01-477d-a56d-a8a4725fd122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 51
        },
        {
            "id": "763c9b18-ed47-46d2-b38d-6c110d9e0281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 52
        },
        {
            "id": "9a7bb6ee-1d7a-4a3d-b095-56defd34c121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 53
        },
        {
            "id": "3017e06d-bb24-425b-b477-e7a71d503cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 54
        },
        {
            "id": "8230edb4-9351-4bc6-98be-f71a3b2b869c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 55
        },
        {
            "id": "98c95ac9-9ce7-48cc-80a6-32243df60525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 56
        },
        {
            "id": "b312dd80-60b4-4c75-880b-f23f66c62928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 57
        },
        {
            "id": "726fc0c9-ec24-4afd-adb7-a3249201304c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 58
        },
        {
            "id": "7781f4ae-5d3f-41ab-8f9d-2c698923069a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 59
        },
        {
            "id": "4d55d8f0-c56b-4419-927b-ea6632972639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 63
        },
        {
            "id": "175b0d23-004a-49fd-b87b-19fe9ebe166c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 64
        },
        {
            "id": "4edcbd16-4ffe-4317-b1dc-7ed61d2d4153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 65
        },
        {
            "id": "a569e423-6890-497f-9e86-e0fe84c66646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 66
        },
        {
            "id": "8a824e8f-6bf4-44d7-90b7-7e7aba612c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 67
        },
        {
            "id": "302dddc1-9f0f-41be-8056-e93198e183a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 68
        },
        {
            "id": "f458b1b6-6ba7-4f45-8fe4-04c58b0e3e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 69
        },
        {
            "id": "53cbceb4-9ac1-4314-a6fd-eb0178ca7632",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 70
        },
        {
            "id": "7fe7d58b-67e8-41c9-a144-d5cf841ffd31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 71
        },
        {
            "id": "bacc9735-9c38-4056-8824-37c3a4ad08aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 72
        },
        {
            "id": "cd25bb14-0a77-4bf4-bc63-ffb320902438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 73
        },
        {
            "id": "8f969d34-12c2-4c72-81ca-b2e70581b93d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 74
        },
        {
            "id": "edd0aefa-ecb6-4156-8526-ab79f9a3c0b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 75
        },
        {
            "id": "a29e4338-4208-4bdf-b383-fd8b5c30dfe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 76
        },
        {
            "id": "42ce31b5-bb8f-45d4-8c34-7279dd4ee540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 77
        },
        {
            "id": "5ae828ef-1dfe-4813-aa30-c7f2ecb1694f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 78
        },
        {
            "id": "91bb4369-72f1-4355-972a-08c6348bf350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 79
        },
        {
            "id": "f3138495-d247-4cfb-8271-bf2a2e1c1cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 80
        },
        {
            "id": "fb00dbe8-3ef8-4166-8063-e8c23effe097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 81
        },
        {
            "id": "2bcb7e88-8020-455d-aeb4-4bcffb32b1e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 82
        },
        {
            "id": "4846d906-6fe3-4d81-9884-89f36e2cbd1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 83
        },
        {
            "id": "272bc225-2e73-4454-a071-8b5e8b74e517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 84
        },
        {
            "id": "e0cf27a1-b893-441a-a5cd-8973a9cf9ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 85
        },
        {
            "id": "6e2165e0-8bf3-44a5-aa49-8dd102f4f566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 86
        },
        {
            "id": "066785f5-19ed-467c-995a-684e52ab1527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 87
        },
        {
            "id": "67145fb5-791f-442d-9eda-91512170173f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 88
        },
        {
            "id": "31992d06-e80a-48c6-a469-104e13639e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 89
        },
        {
            "id": "3102f925-41aa-4572-98bf-dd3cfcc98a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 90
        },
        {
            "id": "5bbdef00-23d6-48b6-8e73-6df278d05a22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 92
        },
        {
            "id": "88a94821-0a59-40b6-b271-16241dd64b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 93
        },
        {
            "id": "7c98e0c8-2f94-4e92-b067-81d63747d8ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 98
        },
        {
            "id": "7f7649d6-8b1e-4980-8063-afb6351b0a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 100
        },
        {
            "id": "417dc97a-4f2d-4470-81ef-639ffd69694d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 102
        },
        {
            "id": "d380fd3b-3ee8-4fd0-99ee-9cf2f76580ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 103
        },
        {
            "id": "a4351f99-77c0-4373-8280-ce9bf144d4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 104
        },
        {
            "id": "366f7752-7adb-4262-9cd8-f30c9a582a5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 105
        },
        {
            "id": "36b08c45-aa69-41aa-8920-48a602acdfb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 35,
            "second": 106
        },
        {
            "id": "1010cf9d-99b9-4fa9-b9e2-dd5d88b54f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 107
        },
        {
            "id": "abebdbc8-df5a-4d6e-b8f9-c953cc545385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 108
        },
        {
            "id": "1873ea01-43aa-404f-8f63-302dc9a152a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 113
        },
        {
            "id": "84f86fe9-d795-4b94-97ba-f726a91f6549",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 115
        },
        {
            "id": "24f07ca0-67de-43a3-9603-e0273d42a18c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 116
        },
        {
            "id": "d55f1c18-cac9-44b8-8a47-664393bb880c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 118
        },
        {
            "id": "36176ceb-88ac-471c-a3a0-f8f15c35b629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 119
        },
        {
            "id": "9a003b06-5331-4906-9214-70f7c872bd51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 120
        },
        {
            "id": "3d730dd3-8640-4e1c-b998-c9fe27d1015e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 121
        },
        {
            "id": "f5c7b6d0-541b-47f6-ba0d-bc0748e47ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 122
        },
        {
            "id": "93071c55-c7d0-4447-825d-22f76ee4d83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 35,
            "second": 894
        },
        {
            "id": "d73d5e12-5e8b-4ffe-88f6-1faa0709a5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 34
        },
        {
            "id": "fc9f2fde-d02e-4413-a18f-29070617b383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 35
        },
        {
            "id": "7ad8f393-f11f-4281-9362-0c24ea5c0a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 39
        },
        {
            "id": "3d0b2155-fd84-423c-8949-8fded56846b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 40
        },
        {
            "id": "2e97ec14-70cc-4a21-9334-1383400fa0f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 41
        },
        {
            "id": "6b11b44f-e709-4b4a-b776-8ba0b1ddca04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 44
        },
        {
            "id": "72bd47bd-0f57-4ab8-9887-8b8cd38dcef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 45
        },
        {
            "id": "66d7aa54-6f86-43df-b7b6-8ef245984e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 46
        },
        {
            "id": "d3e6ccc3-85ef-4751-bb84-e3d5cfa7acb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 47
        },
        {
            "id": "aa7afb4d-2dee-428a-b8f2-886498b8d945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 49
        },
        {
            "id": "b7dbc722-11f6-41c5-846e-d0f1fcc572f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "66329fb3-346d-41af-a73b-45af73243cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 51
        },
        {
            "id": "71ee6c84-9844-407a-879e-47a5dcc02aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 55
        },
        {
            "id": "26d6ca74-029c-44a4-ba3f-f7b3e4c3946b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "738aaf27-30fa-4e4d-9baf-f030a2e503f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 63
        },
        {
            "id": "07182b43-0740-448e-ac16-3930333884de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 64
        },
        {
            "id": "687ce265-53c6-47f6-9404-ec11bf3bb9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 74
        },
        {
            "id": "9dc72586-f47a-4aae-9aee-6e02594c656e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 84
        },
        {
            "id": "d918d843-ce71-434e-94bf-058709ddbeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 88
        },
        {
            "id": "9f658e2c-88f6-4f52-bec2-175cda28d4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 90
        },
        {
            "id": "9cee1af5-30a9-4af7-b135-e8382504c32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 93
        },
        {
            "id": "c7c353c4-23f6-452d-baff-52a7f1da41a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 100
        },
        {
            "id": "55685e58-5a4f-4da2-9a29-4a3983be8d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 103
        },
        {
            "id": "a9e4e7f3-98c1-45b0-8d0a-ba6ecc19fa37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 105
        },
        {
            "id": "4fd7ed54-65da-46a0-a9f8-7514d39e1eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 106
        },
        {
            "id": "4d81d050-5914-4bed-b1bd-593d71f68818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 113
        },
        {
            "id": "9376d8d9-bb56-425b-b205-4a8fcbe1f9a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 173
        },
        {
            "id": "37119283-e28b-4d03-8811-b6793aeef8c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 8208
        },
        {
            "id": "9e34616e-a11d-444b-a3ae-abb556d4e7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 33
        },
        {
            "id": "05334e9d-b7cf-43b9-b668-7d23fa5a4bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 35
        },
        {
            "id": "96d9cebc-c7ab-43cd-946b-f866051ebf35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 40
        },
        {
            "id": "6eae6701-f95f-4592-be09-141a3d6dd62e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 41
        },
        {
            "id": "d99431fc-c63e-42e7-9979-bf11e4a066dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 45
        },
        {
            "id": "1787e0e2-25d9-4947-b1e6-d1a67498c6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 47
        },
        {
            "id": "eebd9fb6-3f59-40a0-b278-df9f73a8eecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 49
        },
        {
            "id": "4ca8308b-c953-4db4-a389-93491e750a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 50
        },
        {
            "id": "29d9f5ff-1e0a-49fd-850f-27259fcdf121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 51
        },
        {
            "id": "5495f62e-7868-488e-a18a-e35d27bcef0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 52
        },
        {
            "id": "6ee79a5a-9330-4a27-8923-879c70ddcbd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 53
        },
        {
            "id": "ba9c791f-6da3-44ad-91fa-514b413b5ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 55
        },
        {
            "id": "7a1b328e-97df-40a8-86ea-618276fa9ed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 56
        },
        {
            "id": "227ebec6-280f-4398-aa91-9c073555371a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 57
        },
        {
            "id": "96e6ca88-67ca-422b-97c6-153af4b6117f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 58
        },
        {
            "id": "17e6cc97-ed53-4873-a6e7-c752fc12b478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 59
        },
        {
            "id": "26ad6487-6b14-47d9-a65f-776a648eb323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 63
        },
        {
            "id": "454b1b33-285a-4429-816d-25717da58e0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 64
        },
        {
            "id": "fd743687-0068-4028-9f4e-4b4f833f8046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 74
        },
        {
            "id": "d1a5867c-9ef0-4893-b7d3-17b1c47dcad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 83
        },
        {
            "id": "3416346b-4750-4aa8-ba7a-6fee51a13155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 84
        },
        {
            "id": "00357686-9209-4cfa-92ef-806877d011f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 86
        },
        {
            "id": "609e31a0-5281-4a13-b794-e984afe54e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 87
        },
        {
            "id": "f4a045f1-8b47-4566-a2a4-ac1c1dc61cba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 88
        },
        {
            "id": "6de3e2bf-ac72-41ff-bc59-801a149eacec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 89
        },
        {
            "id": "69adacd0-9906-4c12-bd1f-4e0bcedcfb09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 90
        },
        {
            "id": "1a7042f8-f755-4045-b882-26f237c24fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 92
        },
        {
            "id": "10549ab8-d42c-4e64-bd7c-d2a8c084428f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 93
        },
        {
            "id": "e31a9ba3-555e-4df3-8438-8a0ac9d2e8fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 97
        },
        {
            "id": "82f2dcf1-91f1-45c3-a184-efbfdeec316b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 99
        },
        {
            "id": "840889f8-3fd1-4831-ba1f-3176757101c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 100
        },
        {
            "id": "08341c04-f2d3-4c57-9bf7-b98bf4d81cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 101
        },
        {
            "id": "fc8c0864-4c24-40c2-9ba8-1e7771ef45b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 103
        },
        {
            "id": "f026f2b2-9e49-4f65-a4b2-7d3df5fe4358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 105
        },
        {
            "id": "5ae0fd8e-81ff-4b99-a630-66273ccfe7e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 106
        },
        {
            "id": "23098f02-86dc-4edc-8408-375661b01ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 109
        },
        {
            "id": "527eb5c1-8db8-4660-8aed-529d16723db7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 110
        },
        {
            "id": "1501ed5c-e7f9-4252-9af8-f0dc7bf8e701",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 111
        },
        {
            "id": "9d31a912-0e85-4484-8363-cb41f5080dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 112
        },
        {
            "id": "808a7dfd-3751-48c4-87e9-1d4f98c04aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 113
        },
        {
            "id": "4425f449-4b48-4e9f-81e1-f27896537a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 114
        },
        {
            "id": "96e601e0-1026-4978-8eef-7cd6f95bccdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 115
        },
        {
            "id": "cdb4e9cc-422a-42de-99cd-0be2b139fd71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 117
        },
        {
            "id": "476fca39-bdc9-42e0-acc4-a0d981962e27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 118
        },
        {
            "id": "7c93e4fd-efd0-4538-9567-91ded8c0d986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 119
        },
        {
            "id": "27f1f11d-5da3-45c2-81e2-d765de245a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 120
        },
        {
            "id": "f0bec7dd-c13e-46c7-893f-724b51627a6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 121
        },
        {
            "id": "83a593f2-d772-44f5-9f8c-89ab1ae7bb92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 122
        },
        {
            "id": "5bb49cf1-fac4-4ea0-baa2-5e69667fbf20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 173
        },
        {
            "id": "48bd4918-bd40-47e7-959b-4fa8dcdb20e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 894
        },
        {
            "id": "9cdb771f-f6be-4281-97ca-38f3b2464b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 40,
            "second": 8208
        },
        {
            "id": "cfb6c78d-8b99-4525-b5b4-e6fadb63434e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 33
        },
        {
            "id": "126515bd-18bf-469d-90ca-ac4dc63bd324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 34
        },
        {
            "id": "3c51ee42-e000-407f-9817-b484f347cb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 35
        },
        {
            "id": "8de32048-ea67-40dc-a8c6-652efe9164e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 39
        },
        {
            "id": "248246e1-1dab-4920-ac7a-011ced13a06f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 40
        },
        {
            "id": "62d02673-bf2e-4811-9d7d-445461a227c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 41
        },
        {
            "id": "2ba2f4fd-b430-4bd9-8488-f79a9bebe898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 44
        },
        {
            "id": "b444483d-d2c7-4a5c-a2f0-df61b6be3e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 46
        },
        {
            "id": "b5bbb930-0b04-4e48-b036-ba1e1e6b1d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 47
        },
        {
            "id": "650318f2-7a9d-4128-bc30-a1e68e9cdea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 48
        },
        {
            "id": "d5944b1f-9466-4c2f-89f1-ec018f0c6fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 49
        },
        {
            "id": "056ee4e7-12de-4d0e-be92-1b350e5ea288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 50
        },
        {
            "id": "453d2732-a3cf-4be0-8a58-8e7610303440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 51
        },
        {
            "id": "3a3bd877-c7c9-4586-a49e-51b0d98d09c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 52
        },
        {
            "id": "cd945429-a64f-4d02-be50-b3a4fff3bfbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 53
        },
        {
            "id": "f4da0095-c03c-40b9-9ca3-098cf7c0c13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 54
        },
        {
            "id": "a08afb4e-d94e-4ed3-9a33-4223d55d279a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 55
        },
        {
            "id": "daa7d355-92b4-4098-95d0-0001e61af712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 56
        },
        {
            "id": "853fb575-c097-4d50-9673-23471846898a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 57
        },
        {
            "id": "c51a8338-8bdd-445b-bde1-a83592110182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 58
        },
        {
            "id": "997c08ba-d645-434d-a286-0883f73778e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 59
        },
        {
            "id": "953eb5df-7d6d-4039-8d8c-7db77988923b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 63
        },
        {
            "id": "a658c407-8d01-4e58-b6db-46ded85a245d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 64
        },
        {
            "id": "2de3507e-9d8c-4baa-92bd-f853e5a2497c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 65
        },
        {
            "id": "a09d80ae-b550-466b-97cf-d0e2dbaa3b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 66
        },
        {
            "id": "3a2b4371-7d1e-4111-96f2-19c5bdd6c1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 67
        },
        {
            "id": "7a5edc42-1a41-4c4b-aa81-5b36c72b2d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 68
        },
        {
            "id": "1b0d0a6b-87ed-4fd3-89f3-55bf5e3ce9f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 69
        },
        {
            "id": "bc789c0a-de8d-4cff-8280-f8315a3a99a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 70
        },
        {
            "id": "cf89b5f9-97e1-4afd-9069-f81f2755458a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 71
        },
        {
            "id": "beef70cb-20c9-4e65-bd07-203ef4f32878",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 72
        },
        {
            "id": "f1eb5eb6-98e1-49c1-bc08-c59ee6b4d39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 73
        },
        {
            "id": "9bb3260f-863d-4679-881b-b529784d7309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 74
        },
        {
            "id": "3da2d24f-6ad6-408a-be4b-cd4cba811553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 75
        },
        {
            "id": "7d3af270-5363-477e-93e3-a9e84c2bea67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 76
        },
        {
            "id": "e4a31b7e-a3e5-4048-9440-b1b4651445c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 77
        },
        {
            "id": "72ef4158-8472-4a96-8ef2-ab5e783e2c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 78
        },
        {
            "id": "5e1bbf50-d9b2-4f3b-b60b-0ab19f87f889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 79
        },
        {
            "id": "2179c247-90c4-45af-a986-9491e8900728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 80
        },
        {
            "id": "2861ef12-6340-458d-a1c8-e166d0304bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 81
        },
        {
            "id": "097cad6f-9c96-45fc-9b23-3671216ed7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 82
        },
        {
            "id": "48dd202f-1423-4cbf-934a-191d48d90d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 83
        },
        {
            "id": "5b0a0da7-70c2-4da3-87e6-f0ea53e9457c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 84
        },
        {
            "id": "efe9be0a-9781-4e05-8425-64560581447e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 85
        },
        {
            "id": "89bf013a-7e4f-4d27-a558-9bd5778b5d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 86
        },
        {
            "id": "7ab0e4e6-4e58-4658-a52b-f98b7c542231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 87
        },
        {
            "id": "8100906f-e852-42be-9a75-946317c8e3b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 88
        },
        {
            "id": "027dbd9e-a335-435c-8179-f1918aaf3494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 89
        },
        {
            "id": "202f1513-5f2c-4af9-b608-7e626fa1cc6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 90
        },
        {
            "id": "441670dc-7cab-4aa5-ba5c-52e63748df85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 92
        },
        {
            "id": "3c879320-6340-4764-8235-8f65dfffac85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 93
        },
        {
            "id": "0a0c1adc-f81b-4a5b-b1e2-7fa338d4edfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 98
        },
        {
            "id": "4757fcfb-381b-46ae-bb6e-9a20554189cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 100
        },
        {
            "id": "2bf07659-be14-4829-a44f-cc7e51e4361b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 102
        },
        {
            "id": "34804199-0a7c-46f6-af3c-0c3ee30cc16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 103
        },
        {
            "id": "6295bef2-03fa-4926-919b-cf660d0cf641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 104
        },
        {
            "id": "fc48222e-aa8a-4d40-a0af-453e64cc6f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 105
        },
        {
            "id": "089b76c5-5f89-464f-b068-1a6f243906fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 41,
            "second": 106
        },
        {
            "id": "16985e33-59d5-4775-8bee-45c14694e906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 107
        },
        {
            "id": "f8e3a128-f7eb-4d7b-ab29-e38756282244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 108
        },
        {
            "id": "be6aa82a-4723-4cd1-821e-540a0dda64e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 113
        },
        {
            "id": "2c4afcb3-cd6e-44ac-a82e-d81bb0c91c30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 115
        },
        {
            "id": "c6922869-4cab-434a-94fc-03fb2a82fda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 116
        },
        {
            "id": "b324e86f-a5b4-42f2-96ce-a0273906029a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 118
        },
        {
            "id": "e628603b-6cd1-4526-bc5d-c00e6fdc8653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 119
        },
        {
            "id": "41ec672b-aee8-45ec-8ec0-53e15dfe8dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 120
        },
        {
            "id": "37433e44-0f54-40eb-9128-15337de4ecfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 121
        },
        {
            "id": "c3c95f25-65fb-40a2-a355-b354d57d5f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 122
        },
        {
            "id": "a0f18ead-a81d-493c-a801-2afd6a03f259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 41,
            "second": 894
        },
        {
            "id": "3f23d1ee-f1ef-41f3-8710-c7c23423f6f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 33
        },
        {
            "id": "3870c46c-7565-43ae-863b-0324b062c237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 34
        },
        {
            "id": "1b7682e6-2242-48b5-971c-4971e9b71997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 35
        },
        {
            "id": "b2897dad-7aa6-4648-add8-a4b97c5ebf12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 39
        },
        {
            "id": "1ce5ff82-a048-4d63-a265-57e69c0d71d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 40
        },
        {
            "id": "03fa3cf0-cf8c-4ae5-aae4-ed9f6967e285",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 41
        },
        {
            "id": "5b33ab7d-eddd-4c17-a49d-756d492e0feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 44
        },
        {
            "id": "c6f8b23c-298d-466c-9baa-01a32c5662f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 45
        },
        {
            "id": "c20d8774-824c-4a7f-8f8e-45d4ed82f144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 46
        },
        {
            "id": "908672fa-276b-4833-88f4-6626e86e44a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 47
        },
        {
            "id": "ff714b50-f51e-45c5-a1a6-a2e7c7a31388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 48
        },
        {
            "id": "c65ae756-0688-4a76-9c40-8deba83e76dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 49
        },
        {
            "id": "06da6e1e-6a20-4d1a-961a-099f749d7c1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 50
        },
        {
            "id": "c1e93e50-65f7-4b66-85b0-c47716e9a07c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 51
        },
        {
            "id": "8d475325-1223-44b4-88a5-4d6a0e795412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 52
        },
        {
            "id": "dd3cc605-2ba4-4368-a352-e6ab3004fdb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 53
        },
        {
            "id": "cbe145f4-cd5e-4214-a358-13b7104b2db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 54
        },
        {
            "id": "bf4b0dc4-bb94-4141-a278-3a60b58eca35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 55
        },
        {
            "id": "a85f3372-7810-466e-91ce-45ec499ae7de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 56
        },
        {
            "id": "b33e4925-94a4-4358-9d8e-0b6b056f3141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 57
        },
        {
            "id": "b55e26ea-5a5f-41bc-bb80-716eae57b3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 58
        },
        {
            "id": "f1b5efa3-b897-4237-8de1-24b15c773281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 59
        },
        {
            "id": "a9aed243-2109-4a32-93b0-22c61687d098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 63
        },
        {
            "id": "4f4c20e7-8020-4f7b-8b06-6ab2db0638da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 64
        },
        {
            "id": "fea16049-6815-458a-9afa-69bc2b89f143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 65
        },
        {
            "id": "b53ad44e-103a-4284-a810-45793cb34deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 66
        },
        {
            "id": "72a712cf-cda0-4306-b63e-559085a9bc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 67
        },
        {
            "id": "67096b6f-8650-4a44-996e-7aeec40d56ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 68
        },
        {
            "id": "7a1046e5-11e2-406c-ad95-0a7e6135e7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 69
        },
        {
            "id": "99c7e65a-abb5-4a76-a9b6-960f58cd21f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 70
        },
        {
            "id": "d192d7d3-96ca-456b-9315-6b25301d4267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 71
        },
        {
            "id": "cfd43944-43f9-4276-8250-0eb0e12c1b89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 72
        },
        {
            "id": "739faa3d-30dc-4232-b096-dc249b4fbe00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 73
        },
        {
            "id": "fb75cc2a-8202-4e39-a437-024984f7d19d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 74
        },
        {
            "id": "5a4ff96d-2a76-41af-88a1-2b043d0b7b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 75
        },
        {
            "id": "07055e63-349e-4708-9a3c-f1d1bc193d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 76
        },
        {
            "id": "2206ea9b-007f-4679-9b4f-1540b32201f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 77
        },
        {
            "id": "7de2bea0-9b0d-47c5-80c0-d74e6f8a2c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 78
        },
        {
            "id": "ddc76b3f-6ea0-4197-ab77-9e51430a8c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 79
        },
        {
            "id": "546b6922-b15f-476b-9d1f-d7f1aad6c5ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 80
        },
        {
            "id": "56f22c8c-cf2a-42b3-b423-cdcc21d3fc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 81
        },
        {
            "id": "e9b0cb7b-3263-4363-974d-af6d1371404e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 82
        },
        {
            "id": "8e368d0b-32bb-47f8-804f-30b798509274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 83
        },
        {
            "id": "f6d40e6d-ec50-48f7-b0c5-1f2d5616430f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 84
        },
        {
            "id": "31a8e875-8203-4325-8fd2-117724f63289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "ee61a47e-7654-4a3f-ba5e-082b449542b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 86
        },
        {
            "id": "9db2104c-d629-4bec-ae73-07f04cdc4626",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 87
        },
        {
            "id": "fda03df6-d602-43a8-b437-00983c36c53d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 88
        },
        {
            "id": "ec049807-cc8a-447c-be52-2d5a1c4b5320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 89
        },
        {
            "id": "c6ffd10a-c72e-4d62-92cd-b43ac2c1f1e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 90
        },
        {
            "id": "71e85fb4-c52e-4b20-9faa-ebfbd0d32b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 92
        },
        {
            "id": "767831a4-b9ed-4538-9b77-99d477b97f3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 93
        },
        {
            "id": "f20f7028-f209-4a2d-9278-5c6620d19dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 97
        },
        {
            "id": "1672c866-1d91-4f5e-96b5-49331b0ff8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 98
        },
        {
            "id": "5236b851-7d36-4d94-a914-61848ba1011c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 99
        },
        {
            "id": "de48af5f-e3af-4a86-9f28-7f7a02d0f69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 100
        },
        {
            "id": "8350fd70-d47a-4614-a19e-5cc60dea3124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 101
        },
        {
            "id": "bc41686a-9b0e-4365-9456-6962b22daeb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 102
        },
        {
            "id": "468d1532-7ac9-410e-ad1b-e2ac1787876f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 103
        },
        {
            "id": "1aced989-e9e1-4545-b3f8-7c35d8363e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 104
        },
        {
            "id": "a466d106-29bc-404e-9c31-83aae013b12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 105
        },
        {
            "id": "9d2ea02a-0e12-45bf-97f9-4b107d0c597a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 106
        },
        {
            "id": "a52adaf1-58e7-4fe5-91b6-8d173377be46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 107
        },
        {
            "id": "4e9aa848-0029-438c-98b5-a7464a278695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 108
        },
        {
            "id": "c0fa479c-126a-4c93-b535-00b1472f9bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 109
        },
        {
            "id": "d6c23ece-814d-4a95-a3ee-99b1483aa13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 110
        },
        {
            "id": "2d2ed361-41c4-4e90-b6fc-f87666d75309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 111
        },
        {
            "id": "839c4c99-510b-41cd-8144-9477b9d4b734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 112
        },
        {
            "id": "6455c820-05e8-4329-9df0-97b1c38f7b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 113
        },
        {
            "id": "0357dfb8-f851-446f-a52c-1d3d9033fca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 114
        },
        {
            "id": "4bbb6fe9-e15c-4158-ae01-141be5a3b1ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 115
        },
        {
            "id": "912c6c04-55e9-4d32-aa4c-4d64c8dab3e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 116
        },
        {
            "id": "ca32b355-9f9c-48f5-b96f-d1788e70b885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 117
        },
        {
            "id": "f9b4f39e-a068-4995-b163-b0a24e5dbcee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 118
        },
        {
            "id": "e3abab41-a86b-44f5-9cdd-25e6b60d09d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 119
        },
        {
            "id": "5bb29a7c-4cd8-4388-9f27-aa2a2ec67311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 120
        },
        {
            "id": "c4e600a0-73e7-4d6c-baae-8268b311caaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 121
        },
        {
            "id": "75bd3eb2-f84e-49d6-8d73-d44714cb15ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 122
        },
        {
            "id": "ab50a63e-53d7-4251-939b-7156aa1f18dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 173
        },
        {
            "id": "39040e66-ba2f-4d0e-891a-c28c8e99eb8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 894
        },
        {
            "id": "69303538-74b1-4b64-a134-ec543b33c30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 8208
        },
        {
            "id": "4ad780bb-95a7-4551-9468-a913d48242b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 33
        },
        {
            "id": "8bb7bcdc-64bb-4e1c-8884-917ec8228ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 45,
            "second": 34
        },
        {
            "id": "917446fd-3b61-415b-b24f-f0bfebfa1a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 35
        },
        {
            "id": "36533957-a3c8-458f-9845-e4ba28dd812b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 39
        },
        {
            "id": "5322bb9c-d631-48a2-8f61-8a6a9bb587c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 41
        },
        {
            "id": "7d2a027d-33d4-40c5-b4bb-d461243cc442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 47
        },
        {
            "id": "26f7463a-0652-44b2-a939-e53dbb71e1c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 49
        },
        {
            "id": "79720084-80c6-4951-b2ac-1981e8c207fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "9b849d3e-64fc-4431-8f79-e237a37b2989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 52
        },
        {
            "id": "c7a7816e-278e-4f89-a883-ee73e993512c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 53
        },
        {
            "id": "b84788c2-9dda-412b-bb12-a9211bcec026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 55
        },
        {
            "id": "72d3e606-1af4-432b-a733-5c2d6f44e067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 57
        },
        {
            "id": "a155e46c-6b0d-43d4-b075-3bb893d7c4a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 58
        },
        {
            "id": "134ed872-26ca-4f9c-bfd3-89364191834b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 59
        },
        {
            "id": "ecaeb620-1da6-4244-9fd6-52f835fa9b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 63
        },
        {
            "id": "794812dd-c15d-407c-89ba-b3963078cb49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "4ede532f-33f1-4a98-abba-187e10131fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 83
        },
        {
            "id": "f88c979e-cfce-4b4d-bf3a-2366ff45dec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 45,
            "second": 84
        },
        {
            "id": "2594053f-de86-44da-b6d4-7b2ed321d0be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 88
        },
        {
            "id": "b64e998a-2f7a-49f5-bb68-0f9649caaf55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "7aef2ad2-2bb8-4fc6-b1ee-953984c39370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 90
        },
        {
            "id": "2c3155c5-a61b-45a3-9893-a5faa516c6ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 92
        },
        {
            "id": "8502543f-9820-416d-a91c-4a9eeab2ad4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 93
        },
        {
            "id": "911da26f-35d8-4923-b5af-a985821c3a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 106
        },
        {
            "id": "2ac28051-b0f9-41f7-8848-d0c0606c58a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 115
        },
        {
            "id": "c145b0c1-faea-4476-a5d1-0b37641cda46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "d0969155-6bb9-49bf-a8e7-5fc1839fc44d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 122
        },
        {
            "id": "35fd97d0-f908-4bc3-8a0e-0ad9bc06e702",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 894
        },
        {
            "id": "ea513c74-3c9f-4863-9b3f-ac1dc05ba6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 34
        },
        {
            "id": "be9aebeb-21c0-4d25-8a6a-6ded0a6cf4d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 35
        },
        {
            "id": "49c06efd-09f4-4279-92a7-31f3bf2d4d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 39
        },
        {
            "id": "93e25242-55d4-4d2c-a766-3916c2145433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 40
        },
        {
            "id": "6a26bfa9-fa39-4829-97d3-f31b593034ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 49
        },
        {
            "id": "edbf31f8-f318-4ce8-81f7-5d77af0a79f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 52
        },
        {
            "id": "5a692821-5b30-4874-acaf-edef31c0d0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 55
        },
        {
            "id": "99dd6976-25ba-4256-b2cc-9c1d8477c197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "abf1f301-4e58-406e-9f10-b7d59d944253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 64
        },
        {
            "id": "6ba4cdb6-46ba-4fd6-b316-3e197c5f0bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 84
        },
        {
            "id": "f13b6f28-039e-4b2c-bc22-8474f8f18973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 86
        },
        {
            "id": "153d86fd-05f9-433c-b5a0-96b3e4dcdae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 87
        },
        {
            "id": "38fbe069-8efa-480a-a809-8c3f65c10b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 92
        },
        {
            "id": "6fddc56c-b1c2-4432-9a6c-7ff6d01c5e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 103
        },
        {
            "id": "1c92f3ce-c516-4ddd-8855-ffd08e18b577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 106
        },
        {
            "id": "3849a93e-f76c-4e0c-963d-eba984584263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 118
        },
        {
            "id": "21f1c003-140f-45a2-b2d5-dfd783f9c8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 119
        },
        {
            "id": "313303a5-7f1c-4e6e-bb4f-9152f21dee61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 121
        },
        {
            "id": "c238ef75-1bc2-49a0-9064-908496a148fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 33
        },
        {
            "id": "57fb1874-f0d4-4f13-9914-1b598de0f04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 35
        },
        {
            "id": "ab2e3880-6321-4d87-81f4-dcc55a6c6da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 40
        },
        {
            "id": "111f690e-2877-46a7-aa77-5cc3ee445826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 41
        },
        {
            "id": "9bb9c88c-c749-4bbf-8e04-70e463cd2cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 44
        },
        {
            "id": "71ecb1ed-b09d-4f45-ae50-657839018179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 45
        },
        {
            "id": "d6b1ce6f-dbd9-41c8-8776-d800e311400d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 46
        },
        {
            "id": "02952009-c20e-4208-aec6-4164594c5137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 47
        },
        {
            "id": "5de0b063-e3d8-4885-b33b-2afd6a5260a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 49
        },
        {
            "id": "cf56e060-6756-459e-827e-7ba99356cfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 50
        },
        {
            "id": "d5390f1c-4778-44a9-a9c1-3ed842e44f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 51
        },
        {
            "id": "e3b437a2-5810-408a-b882-1ae88761aaa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 52
        },
        {
            "id": "c7b63974-becd-45a3-83b2-83c4b9451520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 53
        },
        {
            "id": "27356cce-9fc7-462d-91e4-1e2d57782649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 55
        },
        {
            "id": "4199c790-ca33-47fd-b760-893ba3ebb465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 56
        },
        {
            "id": "65c5de8d-2c0a-4e28-94dd-bce10cdcc5a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 57
        },
        {
            "id": "8369904c-d5f0-4c31-8ed2-78b8cbfa61d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 58
        },
        {
            "id": "13d9c714-8bbe-4ac1-9833-2f9eadb5e7fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 59
        },
        {
            "id": "c2f46f85-49a7-499c-a463-87dc733f9bbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 63
        },
        {
            "id": "9614b092-dd0f-458e-8367-89992653eeaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 64
        },
        {
            "id": "a339f4fa-0056-49ba-abc1-35d22e22c7c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 74
        },
        {
            "id": "04cc85b4-2b35-4b5d-b6b7-f23a6e584aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 83
        },
        {
            "id": "4dea1625-d31c-4247-aa15-8440aeea8eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 84
        },
        {
            "id": "daa548e2-a25b-4f5e-8978-2416f88cc3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 86
        },
        {
            "id": "129c9b5f-5873-4e0e-8d33-d79184102bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 87
        },
        {
            "id": "d796f7dc-0433-4602-811d-cc900bcb8b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 88
        },
        {
            "id": "7f4a273d-f58c-4688-bd67-b4147af5beef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 89
        },
        {
            "id": "7ca9fa21-4a7a-4c3f-81e6-d4e5c6e19112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 90
        },
        {
            "id": "476b5966-bfb3-470e-abe6-d4f508d9d6e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 92
        },
        {
            "id": "41e3cb0b-b1f8-45b6-a446-cee6cd8a9942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 93
        },
        {
            "id": "1f816c72-84e9-4ef5-b396-f7e1cd5644a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 97
        },
        {
            "id": "d4b2a12d-8462-484d-ab22-004f1b12b44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 99
        },
        {
            "id": "9f207eb8-68cd-4c46-8a55-abd72ce9ee70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 100
        },
        {
            "id": "d731a3a7-9709-4192-9624-9eab93fe9610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 103
        },
        {
            "id": "d19af96e-85b4-45d9-ba81-32789b1f9e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 105
        },
        {
            "id": "9998d5c6-8473-4269-b40a-cf0c87eef99f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 106
        },
        {
            "id": "2e58d55f-cbe9-47e5-a1f2-c86140c41569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 111
        },
        {
            "id": "0e53cb23-b86c-4a10-9884-246875845402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 113
        },
        {
            "id": "9ff26045-39d9-4bd1-91c4-a6bb75085877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 114
        },
        {
            "id": "de0ec29c-f8c2-4e10-b1e1-9174c090cbb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 115
        },
        {
            "id": "3b459dd9-7c12-4d4d-9208-132ee70ce13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 117
        },
        {
            "id": "5293dd4f-addd-4f5e-8d4d-d8112ff86514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 118
        },
        {
            "id": "d477e7f2-ab9c-42dc-9d78-1a0f2fe187ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 119
        },
        {
            "id": "f179a290-b926-4071-9c7a-f03d8adf8f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 120
        },
        {
            "id": "96ac7486-1f32-45df-b3a3-8571308f1d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 121
        },
        {
            "id": "20f3ad32-5c5f-4218-a292-1ac29f07f2d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 122
        },
        {
            "id": "699b4824-297a-4188-a38d-0316ce2c2ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 173
        },
        {
            "id": "a56e4fc2-e84a-498f-b3c7-86d41dd087c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 47,
            "second": 894
        },
        {
            "id": "769cdccb-6653-4065-97fb-712dc81402e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 8208
        },
        {
            "id": "1461d78a-d048-4b8d-97a5-e29d0221e984",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 33
        },
        {
            "id": "f13c19bd-cbfd-4019-8630-1936fc0eb087",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 34
        },
        {
            "id": "037eda53-6a32-4602-8d1f-a7ec4d47bafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 35
        },
        {
            "id": "7e5606e5-f9bc-466e-96a1-ee4b9e6081b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 39
        },
        {
            "id": "dfa46f62-3dfb-4e4a-a748-2cb7119ad16c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 40
        },
        {
            "id": "17f7870e-a6bb-4c24-9b6c-51a5e62f4cb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 41
        },
        {
            "id": "e126085f-3c64-4633-9413-39a6542b2f1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 47
        },
        {
            "id": "9224068e-39b4-41e8-a645-862f54e56961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 58
        },
        {
            "id": "1b2d6dcd-802e-46ae-8952-0c3954a8716d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 59
        },
        {
            "id": "cca9f79d-edcd-4ab5-ae68-b2c60edfeff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 63
        },
        {
            "id": "e0b4c6ae-bffe-4baa-8c83-7ecf63c1de8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 64
        },
        {
            "id": "abfba56e-9050-4b1d-bff7-6c9dc0331bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 74
        },
        {
            "id": "f6134817-aeaf-471f-9f95-70be3458bc55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 83
        },
        {
            "id": "b81e93ed-9839-4a1c-bad4-af36dce26b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "78ea2dc2-b2a4-447f-916b-ad3d4a3c72c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 86
        },
        {
            "id": "4accaee1-d224-499a-b2e2-abe2efa45980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 87
        },
        {
            "id": "27c09ca8-d4a2-484f-9552-0a36ba81fc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 88
        },
        {
            "id": "25a53ec8-743c-46ff-a506-89f788350b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 89
        },
        {
            "id": "c12c26c9-5fa3-49ed-b4b5-1ad25d3fbab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 90
        },
        {
            "id": "ac525d8c-e995-4a0d-87ab-e3d024ed11c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 92
        },
        {
            "id": "ffca369f-3b49-4305-8e57-ec0cb0bc413e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 93
        },
        {
            "id": "e3eae561-719a-4c0f-9902-86fc39b4ca16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 100
        },
        {
            "id": "7b96f962-4181-43d0-8a15-dd2f52088ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 103
        },
        {
            "id": "73137c7c-0cf4-48ae-a0df-5cfb562ebeaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 105
        },
        {
            "id": "2de1180f-ab52-4b8f-aacd-6d5ba5e8727a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 106
        },
        {
            "id": "fbac0827-e1b2-4d30-acde-7b4d0f8c1873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 113
        },
        {
            "id": "11b13414-a110-455b-92d4-175d1cabd5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 115
        },
        {
            "id": "c838ce4f-e9f4-43ea-a6e1-a6210fa118a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 118
        },
        {
            "id": "e3f47f82-f85e-48ea-9613-27458aa23f80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 119
        },
        {
            "id": "e6b2764e-5069-4482-8441-61582430e166",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 120
        },
        {
            "id": "ed0037fd-1079-45bc-a6f6-923b69c7f396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 121
        },
        {
            "id": "10f7d628-6b3e-47eb-87d3-3d757c7e365e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 122
        },
        {
            "id": "4600e8a2-ea92-4c69-bf14-dbb42445e26b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 894
        },
        {
            "id": "371a87ac-0770-4c1b-b7e3-f382d3cf6fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 33
        },
        {
            "id": "4566b161-9bce-4e18-8f5d-5e0aab65dac0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 34
        },
        {
            "id": "2df8b0f2-1a57-4d2f-bfd8-8d4ae01dfd1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 35
        },
        {
            "id": "7b25768b-40b9-4ff7-bca4-1e42357fafd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 39
        },
        {
            "id": "3b55ee69-216c-48d2-a83e-f295eb0f3ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 40
        },
        {
            "id": "b01ce271-700f-471c-a93f-0079496bef46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 41
        },
        {
            "id": "6bef4c6e-7fed-45c6-98c8-fe38c019eb56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 47
        },
        {
            "id": "6c2d3185-7226-4746-b2fe-ac9e9360a438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 58
        },
        {
            "id": "b6d23a76-aec4-48a2-ad72-b8e7709cb990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 59
        },
        {
            "id": "f7732f98-8cc8-440b-af89-f77799e781a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 63
        },
        {
            "id": "23c22035-0e86-446a-9d11-16b94fa46371",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 64
        },
        {
            "id": "53d5f991-f254-438e-a8c3-19978be9cbab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 74
        },
        {
            "id": "e0522ad7-c9c8-43aa-95ee-a820bd69b66e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 83
        },
        {
            "id": "113d76e1-c503-4249-9906-469b06446229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 84
        },
        {
            "id": "89dfeefd-9fd9-49ac-a053-26990acb27c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 86
        },
        {
            "id": "4e9b7c0f-6c13-4f59-965a-252377ee3b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 87
        },
        {
            "id": "04ee92ab-1eee-46d4-bb58-0a0c84293128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 88
        },
        {
            "id": "dcbd0e4c-46f1-4f24-a40e-4a7d753a5d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 89
        },
        {
            "id": "1f2ab242-6e1c-48ad-b7b3-5a0ff54d9806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 90
        },
        {
            "id": "6d7b3a06-c4e4-414a-81d0-28dc9bb8a0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 92
        },
        {
            "id": "71bbdc2f-1906-4256-8186-7b0ee1bff194",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 93
        },
        {
            "id": "09ecddde-5c3b-48a6-80ea-c321bd6fea57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 100
        },
        {
            "id": "8bad67ef-84fd-4b4d-ac06-6a94f3247cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 103
        },
        {
            "id": "4fa13340-8ef6-400e-add2-c8b6d251b181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 105
        },
        {
            "id": "ebd7199f-3fce-4b02-b6ff-2b7b6fb5eab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 106
        },
        {
            "id": "1adcfc78-7f02-45e3-9fa6-a266fb9e59ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 113
        },
        {
            "id": "0bbe4c48-4c06-4b03-a802-173eea91cc08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 115
        },
        {
            "id": "2b161f90-2630-48e1-ba12-88eda856a6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 118
        },
        {
            "id": "c7380545-ffab-494b-a70c-38568c22c2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 119
        },
        {
            "id": "96ef75c5-96d5-435c-9de3-2bd450510c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 120
        },
        {
            "id": "df1e4e71-cf50-4f0f-8957-4622bf3182ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 121
        },
        {
            "id": "e989ad37-533a-46ca-b59d-64fc8dde1e5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 122
        },
        {
            "id": "d43aeadc-2334-4666-b044-839c583a3cc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 894
        },
        {
            "id": "72137e5e-52d2-4815-98bf-902c90559587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 33
        },
        {
            "id": "fa70fb81-bc1f-4ce9-bfb5-11b00424e098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 34
        },
        {
            "id": "a728ea7c-c3dc-403c-9f18-e2e9c26f53e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 35
        },
        {
            "id": "2692a1a8-d5d9-4fb2-81de-0611cc778bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 39
        },
        {
            "id": "6247617e-31c5-449f-9516-cca4e03c182a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 40
        },
        {
            "id": "c357b7cf-3357-417a-b5ee-25daf9b827a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 41
        },
        {
            "id": "e00f986c-0a18-4360-a058-4f9fe11ca7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 45
        },
        {
            "id": "55486f7b-11dc-48ca-936b-c511c65469b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 47
        },
        {
            "id": "7a18c851-addc-4c99-8d77-a9a84134528f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 58
        },
        {
            "id": "37d77df0-94e6-40e3-ad6f-3d06e7cab15f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 59
        },
        {
            "id": "73726696-38b8-40c9-b490-a868f1c3097f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 63
        },
        {
            "id": "2d9095e3-42be-4580-9ba3-aee7e07ff9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 64
        },
        {
            "id": "6cbc6c6a-cb92-429d-8808-bee52d5c36fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 65
        },
        {
            "id": "24c0a827-bd4c-4e4b-9ecd-5d9f3e0b075f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 66
        },
        {
            "id": "a08b687c-929d-4321-8272-7305c97aa81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 67
        },
        {
            "id": "2b572562-d22c-4137-b9c4-13de93162e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 68
        },
        {
            "id": "9098c91e-f316-4b8f-82af-82c474e14d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 69
        },
        {
            "id": "2e98891c-f369-4f64-bb15-59752371f588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 70
        },
        {
            "id": "2f71b47c-b225-47c6-a150-f633057577c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 71
        },
        {
            "id": "f7478d90-2c8b-4eb6-8739-76dfb6bd14dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 72
        },
        {
            "id": "7309ca6d-cf42-4ea4-90bf-40d148eeee57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 73
        },
        {
            "id": "3b30e90e-29f1-4eef-ad58-e6707924c86c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 74
        },
        {
            "id": "9d242688-3f85-41b3-bc4f-dad78cbe2740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 75
        },
        {
            "id": "87fe5df4-fcbf-4748-9768-e4380657db2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 76
        },
        {
            "id": "3ae94feb-6c62-4ee3-bd6f-6254425a661a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 77
        },
        {
            "id": "bcb6326b-3e37-4cc4-afa6-d5bb5996b182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 78
        },
        {
            "id": "a58d6d79-0248-424a-bccc-5a3a5dc7167a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 79
        },
        {
            "id": "f6b084fe-ea2a-457a-b836-fdf6cc1c299f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 80
        },
        {
            "id": "e1460642-08ad-4efe-ad2d-d03ff16ffa23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 81
        },
        {
            "id": "33813a63-eb8e-414e-a64d-5cdc8587e69d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 82
        },
        {
            "id": "02f3fb1d-e751-4bc5-89f2-b8e0e0ecffd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 83
        },
        {
            "id": "a2f00223-84c6-49f7-9691-d52c3cdddc23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 84
        },
        {
            "id": "8a015461-28eb-4121-bfff-8416396126f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 85
        },
        {
            "id": "43e6ba9e-cec7-4ab3-80e6-c48bf5c5854f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 86
        },
        {
            "id": "5f2ff141-97b8-4643-9005-630a56453a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 87
        },
        {
            "id": "fa34c5d8-86a5-4b83-ae41-cfc8e6be9e69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 88
        },
        {
            "id": "48ae7867-82fd-4a60-b39c-6b3ba3aac6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 89
        },
        {
            "id": "99cc4276-2d39-4586-91d6-2ed36c34eb65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "49086074-ce6f-418e-a5f8-6d875418f988",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 92
        },
        {
            "id": "1e536ce7-dfc0-4b46-b113-88296c6cdf0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 93
        },
        {
            "id": "06041850-e23b-474e-b2ba-97725667d4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 97
        },
        {
            "id": "68fe02b2-7aef-404c-b322-5793b5620191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 98
        },
        {
            "id": "979585a6-4309-4895-afb8-a67188ceec36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 99
        },
        {
            "id": "e2520e1e-f23c-4a52-aa44-18a3d3547efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 100
        },
        {
            "id": "92c5cc97-2be8-4b04-824b-ea5d3e4a2781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 101
        },
        {
            "id": "442d1fbe-b89f-4d11-9fa0-33b5d0d13292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 102
        },
        {
            "id": "001b3c35-3480-481b-9439-8ecd6d6d17e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 103
        },
        {
            "id": "8c483238-e428-4489-9393-6a84147232b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 104
        },
        {
            "id": "bf4f1f11-dcd9-4392-8915-18e9487cfbde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 105
        },
        {
            "id": "1afab03e-9d6c-4e33-930e-bf42fcbf75dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 106
        },
        {
            "id": "b1a7f638-a260-40c8-9325-01b156f29f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 107
        },
        {
            "id": "c9b475da-519e-4fc5-9a81-610139ff293f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 108
        },
        {
            "id": "ba521070-3c72-41fb-975a-f5270569bd32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 109
        },
        {
            "id": "7d67d978-af56-45cf-92d7-20e58945f961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 110
        },
        {
            "id": "9893c82a-90d8-4c3b-a26a-893e3ac161c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 111
        },
        {
            "id": "c22dbecf-b0e6-46c1-8818-89c190606235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 112
        },
        {
            "id": "0d13d91b-72f5-4534-b1c4-562e6d3069af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 113
        },
        {
            "id": "dbf140b5-a305-4aeb-b1e8-562ce1a7498e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 114
        },
        {
            "id": "a6de0b10-2b1b-4961-ac59-de9c378e632c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 115
        },
        {
            "id": "ee691daf-5f89-48d4-b940-163e68bb4f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 116
        },
        {
            "id": "654b2948-3b2f-4a74-9bdf-c6b43e02a2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 117
        },
        {
            "id": "862b9349-27c0-4982-9e01-cbc1e51968b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 118
        },
        {
            "id": "f201059b-18a7-4784-99be-e65c081770ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 119
        },
        {
            "id": "0a32db86-28b8-4b1f-b912-50d304a8310c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 120
        },
        {
            "id": "2471226e-0c69-4db7-b537-736cdbc45472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 121
        },
        {
            "id": "45b791ee-9364-4363-8759-70ba9e708f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 122
        },
        {
            "id": "d49ee0d8-7aef-4861-a948-39acd4a53506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 173
        },
        {
            "id": "47337aaa-8f19-481f-b8f5-9f33257fbefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 894
        },
        {
            "id": "851f2acb-ac24-4ddb-a20e-6438de30f5df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8208
        },
        {
            "id": "5193bff9-d5cd-4428-9473-2ecb8209f65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 33
        },
        {
            "id": "3cff17f3-5482-4c27-82f7-1ad6acc0ced3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 34
        },
        {
            "id": "e995044d-cb2f-4aaf-9428-6d1276785c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 35
        },
        {
            "id": "32ade8bf-12d8-4da9-9cdd-5b1fd7ffa103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 39
        },
        {
            "id": "7e343ecc-e005-42b3-a433-f6c3234eeb50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 40
        },
        {
            "id": "e3009776-c920-4ade-a3c6-a91a31bc4bc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 41
        },
        {
            "id": "72d984e8-797a-41d4-b4d6-74482360136d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 47
        },
        {
            "id": "0e8179dc-bd75-4070-8a47-c296ebf69505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 58
        },
        {
            "id": "e44b2691-b083-4d75-99cd-3b4b1881d253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 59
        },
        {
            "id": "7d90886b-e2ea-4e9e-bb75-adfeeff3cb97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 63
        },
        {
            "id": "6e813f71-9330-43bf-aae8-537716a9dfb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 64
        },
        {
            "id": "acdf08d8-4ea5-448a-8432-648247e6383e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 74
        },
        {
            "id": "7e3bda05-cb7e-441a-a67f-1d1990ffe79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 83
        },
        {
            "id": "3aac19e4-d57b-4cde-8b90-e3bf546920d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 84
        },
        {
            "id": "c17183bc-7181-4d5c-9269-3363bb7cf8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 86
        },
        {
            "id": "9803f088-10f3-49ff-b4ff-a0f3c463080c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 87
        },
        {
            "id": "4ce7c224-3c87-44be-8b39-f3cd33bd4719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 88
        },
        {
            "id": "7e61bbf5-5aae-4240-b3e0-d02911f66860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 89
        },
        {
            "id": "42da062a-f0d0-43f6-87e7-f04c98bd01dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 90
        },
        {
            "id": "df13e368-2b9e-4fb8-afcf-61598e1f2cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 92
        },
        {
            "id": "d9a994f9-1b99-418a-a76a-0833f012a4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 93
        },
        {
            "id": "17010b90-31dc-459f-a15b-82f4d9469860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 100
        },
        {
            "id": "91e3d0af-3eb4-4480-9d54-3839ef01df6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 103
        },
        {
            "id": "4cf2d484-58ee-4e60-898a-195f5f2258a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 105
        },
        {
            "id": "b5280435-afae-4051-98ef-75123e715774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 106
        },
        {
            "id": "0a71120c-36e8-4035-8431-4b6ccf884c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 113
        },
        {
            "id": "971f647f-bd35-42c6-8a2b-888106ab2437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 115
        },
        {
            "id": "2fa438f3-b26c-451a-afdf-99c5fc392db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 118
        },
        {
            "id": "76a7a3bc-9ac3-4e0e-8d9f-4bd6a5d02fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 119
        },
        {
            "id": "1aa8b380-06b4-4a7b-bf9f-2bde16a12642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 120
        },
        {
            "id": "57483e5b-1ae2-4e59-b46d-dcde9f137235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 121
        },
        {
            "id": "fa74532b-b8ab-4e8f-b0ca-ab8986708d01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 122
        },
        {
            "id": "fa498a71-55d7-4af8-8a79-6eab9938fd72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 894
        },
        {
            "id": "c8a39381-c081-414c-9d47-e0d55bf0b14c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 33
        },
        {
            "id": "6aba82e9-a9c8-4ab2-936b-97a8b180617d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 34
        },
        {
            "id": "6e662e26-7fe5-4eb1-a2c6-a2a11789ee11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 35
        },
        {
            "id": "67bbfb52-545c-4a67-ba41-d7ce3dd61c0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 39
        },
        {
            "id": "8fc67721-48c3-471a-9cf7-bd811193b9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 40
        },
        {
            "id": "650b93c7-b49b-444b-a6d6-f8eb04c6e4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 41
        },
        {
            "id": "4991b9f5-b10d-4330-bab5-a5548488d8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 47
        },
        {
            "id": "ae4b670c-40dc-4c35-bc9f-07f8b56507d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 58
        },
        {
            "id": "8a7ee4f0-887b-4e07-9420-702d5f733b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 59
        },
        {
            "id": "a24bb9ab-7740-48a9-bc3f-b02f2d5cc2e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 63
        },
        {
            "id": "42e829b6-9b9c-4c16-9908-0cdb0aea32c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 64
        },
        {
            "id": "207c94cb-5516-4e21-aa73-d8f59186141c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 65
        },
        {
            "id": "6b9e31b7-0065-49cd-8fc6-5ca68a912e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 66
        },
        {
            "id": "c4b70c71-62f9-4e67-998e-c7b794dab3e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 67
        },
        {
            "id": "0f7cf91b-94d9-42fe-a6cf-4e09ab048ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 68
        },
        {
            "id": "83fb1a45-a905-4e06-8c30-504bd8b4dc6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 69
        },
        {
            "id": "f59c0772-e1f8-4d93-b63f-944c7395ab01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 70
        },
        {
            "id": "df0c121e-6583-44e1-86ca-b63a27dcd0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 71
        },
        {
            "id": "8fc86c43-84c8-484a-9d3c-0c04ad53c3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 72
        },
        {
            "id": "7e382ddb-79dc-46ff-bc0a-b78d314da920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 73
        },
        {
            "id": "f7f89a28-f59f-479a-b0a2-e808e8de2a3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 74
        },
        {
            "id": "b7c2fd85-f575-4404-bf46-e5bfdc0dfdca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 75
        },
        {
            "id": "7428b64f-3361-48dd-a985-3a29c209e7c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 76
        },
        {
            "id": "05dc75d2-2624-476e-91c7-4e4654affde5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 77
        },
        {
            "id": "f8ad3f7d-8aad-4914-9886-fcabe0d2ebc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 78
        },
        {
            "id": "ed174be7-a2a6-4e58-9a52-e470f1b60368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 79
        },
        {
            "id": "931e2d03-c0d4-410f-a0d9-06c0be040de1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 80
        },
        {
            "id": "aef1de13-a134-43db-8581-145fbc216373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 81
        },
        {
            "id": "8133aaa0-a493-457c-ac70-d91beacce033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 82
        },
        {
            "id": "6d576e65-929b-4b9a-a8d7-320fe241a37b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 83
        },
        {
            "id": "98fc5910-3028-440a-b2cf-3200a415a30c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 84
        },
        {
            "id": "27fd0e4d-8418-4220-b66d-0881dae5e27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 85
        },
        {
            "id": "4a19350e-7f84-42f2-9cb6-3b0065e77f8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 86
        },
        {
            "id": "a2781c69-56a7-4c68-b0c6-f67fbd2cae04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 87
        },
        {
            "id": "37d5f591-ae36-42a5-a479-54301c068950",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 88
        },
        {
            "id": "58cb7013-7d66-465c-9350-673bbff56083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 89
        },
        {
            "id": "1824b6f4-4899-4e5d-ad97-5d601821c5bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 90
        },
        {
            "id": "fdb7e702-ab20-436d-9723-4d4ab5bb3a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 92
        },
        {
            "id": "1b386cf2-ae64-4ae3-9590-34658aac4d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 93
        },
        {
            "id": "c29fcb4d-ba06-4936-ad2e-c0e36805e892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 98
        },
        {
            "id": "46006734-5a5f-4897-baea-0da96305f6e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 100
        },
        {
            "id": "4c58d6d6-1072-454f-8e73-ca29ccb66460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 102
        },
        {
            "id": "aaf23ee2-e17e-46f1-b2a3-6a2c847a8060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 103
        },
        {
            "id": "5e804e7a-89d3-458a-ac28-c7f0ab28a87d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 104
        },
        {
            "id": "b76307c0-9368-4522-bd26-75575f14e0c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 105
        },
        {
            "id": "071ded63-cf10-4732-b9a2-2603839e8ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 106
        },
        {
            "id": "81e33dbd-703d-4c71-b9fc-d70f2db5567d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 107
        },
        {
            "id": "88448c94-deae-4a62-b2b3-79a9684c3362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 108
        },
        {
            "id": "2224f97e-fab3-40ce-bd02-bce333b4ffd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 113
        },
        {
            "id": "24a04113-259b-4cad-b978-3a48017c147d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 115
        },
        {
            "id": "4f1965cf-ea6f-4875-8933-bc2af09e4f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 116
        },
        {
            "id": "73531d24-7245-4f52-a6d2-add0b833b249",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 118
        },
        {
            "id": "f7edd147-98b9-4f7d-9bb9-34646864a767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 119
        },
        {
            "id": "5aee379f-1ce0-4bfc-bef1-b383ba7df858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 120
        },
        {
            "id": "813b6cea-0cba-417c-a305-f53ce58cffbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 121
        },
        {
            "id": "b8519f92-51b2-4712-8600-77702aceb43a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 122
        },
        {
            "id": "3b1e017c-9557-4278-8dfb-8df14f0abe52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 894
        },
        {
            "id": "e9fc4c2b-d94d-418a-9286-95573d1eb66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 33
        },
        {
            "id": "21008dec-ed62-46ce-bcfa-e7a5c975aeca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 34
        },
        {
            "id": "a6bc1f38-4ada-4306-8990-d5babf62672e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 35
        },
        {
            "id": "55b82067-792e-4446-8898-129f3d27cb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 39
        },
        {
            "id": "c3c56c21-806e-4c51-96ce-d05ebdafd700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 40
        },
        {
            "id": "025ffa5d-6cc5-4119-9921-bf06969e328e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 41
        },
        {
            "id": "fe3555b5-de39-41ea-8d84-9b3fb55fbcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 47
        },
        {
            "id": "94c98cf0-fab2-43d6-b55a-711ebc7e6c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 58
        },
        {
            "id": "67b7500e-54f6-489f-bd4d-252367f08f78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 59
        },
        {
            "id": "9f6fc9b3-3883-40f3-8093-aca772f871a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 63
        },
        {
            "id": "2b7cb784-67d9-4871-a910-f7944ad06ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 64
        },
        {
            "id": "88c7dc39-3c68-4c82-ab41-37e2e0c3552b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 65
        },
        {
            "id": "d8c9283c-cefb-412f-bf94-6be588b3fc17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 66
        },
        {
            "id": "b176b74a-6701-487e-9ecf-b02bfe0411e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 67
        },
        {
            "id": "9f57aad2-658c-4f35-a1fd-893abdd21d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 68
        },
        {
            "id": "cf06e5e6-f35d-47dd-a2c3-41bf5570cb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 69
        },
        {
            "id": "11131f71-1f9e-4298-9ee3-0adc86604b93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 70
        },
        {
            "id": "854111bd-9992-4e04-87ed-8611ef82f6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 71
        },
        {
            "id": "11840844-e092-458c-95a9-f5069819c681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 72
        },
        {
            "id": "54538a10-5147-4bed-8a7b-05c48165a31b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 73
        },
        {
            "id": "aa993a9f-eb85-4e78-b08a-cbfa561a5aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 74
        },
        {
            "id": "16622290-c385-4815-a714-8a38ad3576d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 75
        },
        {
            "id": "57a66f63-0a48-43b4-a3bc-f071c4ce366b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 76
        },
        {
            "id": "f1462c03-309a-41af-98e8-92da240e2d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 77
        },
        {
            "id": "1f011292-78f1-4ff3-8076-00853d3760b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 78
        },
        {
            "id": "2015f266-68ae-4736-8d3c-ade06a345f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 79
        },
        {
            "id": "f35a5a3d-8531-45cd-be96-44a2b309f21c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 80
        },
        {
            "id": "6f9ff016-814f-476d-8abb-7ec2f21c4b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 81
        },
        {
            "id": "8d49a356-3f19-4d7a-bb5d-554bf8e0f1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 82
        },
        {
            "id": "f3d17875-34ad-49c4-8df0-b8ec37614765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 83
        },
        {
            "id": "cbe39295-37e5-4805-b350-c15e608b225e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 84
        },
        {
            "id": "9a2a760f-29ae-4362-aebf-93477b57f0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 85
        },
        {
            "id": "0619fa7d-4746-49da-a55d-35e9a0a8f306",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 86
        },
        {
            "id": "0c67a28d-e976-4ee9-a280-a4b878dc7078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 87
        },
        {
            "id": "d1647e34-5147-4d32-bc22-628347cd7a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 88
        },
        {
            "id": "992b8571-12e4-497c-98a5-a2704e5412d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 89
        },
        {
            "id": "e43e09ec-6fb9-4986-a1af-0873a74dfb11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 90
        },
        {
            "id": "9fe9f345-373e-421a-bc39-1923100bc356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 92
        },
        {
            "id": "c25f2f40-3136-4e46-a57d-1559fe405f6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 93
        },
        {
            "id": "cac63b0b-5f18-4988-b602-ce455761223b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 97
        },
        {
            "id": "ebb1e62f-1609-4e70-896e-3be29cbc96fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 98
        },
        {
            "id": "f2fb2eef-2361-4c1c-93c8-177a7132aaf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 99
        },
        {
            "id": "3bbdb29e-cab7-4f07-acd8-06c98d0b1e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 100
        },
        {
            "id": "8aba946a-f330-4968-bf89-fbefb7afdeaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 101
        },
        {
            "id": "c40a06c8-7b8f-4441-b2d5-439ebb52ef83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 102
        },
        {
            "id": "0d3c9e81-515b-4d02-b458-f22dcb49369b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 103
        },
        {
            "id": "b462c4ab-b7ac-4aff-88f2-195df6bb7c68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 104
        },
        {
            "id": "b2a52424-39b2-454a-94eb-df4278905002",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 105
        },
        {
            "id": "8a4b8ede-7c33-4f6a-ba41-c9c45745e681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 106
        },
        {
            "id": "745ad153-f7d0-4a31-8e50-bcd63eae64c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 107
        },
        {
            "id": "f8d7d849-0dd3-4a55-998e-41beb0fdb597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 108
        },
        {
            "id": "5925becb-fccf-4a81-b773-5304f77486ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 109
        },
        {
            "id": "f55deb7b-df62-4b62-8be7-51611c0347a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 110
        },
        {
            "id": "55d62631-8de0-498a-baa3-8f1a779b9926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 111
        },
        {
            "id": "29c08b26-111d-47a3-8410-5a3bda160430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 112
        },
        {
            "id": "598cbbd6-694a-4636-b0cd-7fe0fc75ab52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 113
        },
        {
            "id": "67e78e59-902a-4826-8da8-a466ae661493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 114
        },
        {
            "id": "f97b18e4-931d-4aa9-aad9-05e611263ddc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 115
        },
        {
            "id": "97e18f02-1f67-4963-9268-6a491e7c7eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 116
        },
        {
            "id": "a9105704-e071-4f24-8ccb-023962d5636e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 117
        },
        {
            "id": "5f6e692f-a868-4e18-8fa2-cf7d4ca3e7b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 118
        },
        {
            "id": "ef17cfe7-58a3-4a5e-871c-fb665a415eb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 119
        },
        {
            "id": "fbd878cd-25f3-4387-9dd8-a8af4fc82b5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 120
        },
        {
            "id": "2c503ea6-18d6-4011-9cee-aed60760f757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 121
        },
        {
            "id": "7d828bd4-61c5-4cd3-a534-309c37f27017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 122
        },
        {
            "id": "b9fba133-8c46-447b-9fd0-5bc2aeedfd32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 894
        },
        {
            "id": "2020918b-7728-4376-8232-2ae1108fbe92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 33
        },
        {
            "id": "cb3b9418-c0cd-4e42-8ed6-37003780f7e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 34
        },
        {
            "id": "f0d3531a-c564-452b-88b1-5dc5f60db33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 35
        },
        {
            "id": "4431798f-2ce9-4561-b8f5-3d4da66f9e01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 39
        },
        {
            "id": "36d31860-85e9-4aaf-bc97-f47a359f9476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 40
        },
        {
            "id": "1eaa413e-3aec-419d-aefe-185a8ce615d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 41
        },
        {
            "id": "2a927fa4-c156-4f57-aba8-91b9ff4f975d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 47
        },
        {
            "id": "a6c4f82e-53c6-440b-96c1-1b07800d10a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 58
        },
        {
            "id": "5263f782-8305-4e02-81eb-0c28a11d513b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 59
        },
        {
            "id": "1190fcce-2d62-49b4-a0d6-336522a6d985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 63
        },
        {
            "id": "43a774f7-e536-45dd-9358-4b6bee3df2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 64
        },
        {
            "id": "cc749104-00cb-4082-ae8d-711151349a2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 65
        },
        {
            "id": "bf961242-c12e-4218-95ba-0f60d0ee3fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 66
        },
        {
            "id": "cbc43bd1-26b6-4682-9884-64aff71b5dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 67
        },
        {
            "id": "5133b12e-e5a2-4179-89e2-b19c5c11a6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 68
        },
        {
            "id": "9e608ecd-e083-41a4-b836-059b323fed47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 69
        },
        {
            "id": "5a236ebe-de05-4a91-adb3-6ebdf4e8d215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 70
        },
        {
            "id": "9bffd703-2dc7-4f8b-9235-ebe7337c0351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 71
        },
        {
            "id": "aa32f29a-108f-450b-bcdb-0c4bef784b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 72
        },
        {
            "id": "aa862e17-a512-4ac9-8a94-1866082e71f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 73
        },
        {
            "id": "93e1a48d-831c-4c6b-8eb9-080bc752d108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 74
        },
        {
            "id": "79fdc08d-34e2-49eb-833e-90048b18965e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 75
        },
        {
            "id": "f2df7812-cd26-4b98-adf0-73b40bb64cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 76
        },
        {
            "id": "c95133ed-5954-4b88-b2c5-4f8871bab605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 77
        },
        {
            "id": "c9a084ff-0edc-4d81-9615-1356bcf38255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 78
        },
        {
            "id": "76c4087d-fcaf-4cbe-b7b9-9cd310f4765d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 79
        },
        {
            "id": "e4961480-bb36-4834-953c-86e99198e5b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 80
        },
        {
            "id": "67677939-30e3-45a2-8842-f6604a66785a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 81
        },
        {
            "id": "c5a7b0a3-58d4-4a8e-b28f-26c9306894f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 82
        },
        {
            "id": "3f3f2e04-6d7b-43c7-9b4b-2574544e6be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 83
        },
        {
            "id": "13b2f323-1ec6-421e-b1d3-65fb832a5e95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 84
        },
        {
            "id": "0eaf0212-a731-44e0-a366-4504842e20f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 85
        },
        {
            "id": "c11ef596-f7ac-47f3-96b9-cb34f3095136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 86
        },
        {
            "id": "04185115-bdd2-4055-a365-897bee33f250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 87
        },
        {
            "id": "0915bc40-695c-4f3b-ac5e-5211d1cc2846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 88
        },
        {
            "id": "8f743e38-f7c1-4944-b752-dde3d653f169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 89
        },
        {
            "id": "a9581289-e296-4ac1-b9c5-7a509e83b446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 90
        },
        {
            "id": "d8f53d92-9636-463f-8135-159f1d5664a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 92
        },
        {
            "id": "d0e6a67e-6bdb-43e5-9f47-e4c6cbfc4e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 93
        },
        {
            "id": "568537ec-87b5-41e4-b485-f433f423d56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 97
        },
        {
            "id": "3f5471d9-c3ef-41f0-93f3-d45409f2ec3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 98
        },
        {
            "id": "00826ec7-b591-40c3-b64c-e3a4e9996185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 99
        },
        {
            "id": "f9c564c9-9f46-4d94-bc43-ae657cf18317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 100
        },
        {
            "id": "ab0c3812-5ae4-4d0a-9b30-7e99e3e9cb60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 101
        },
        {
            "id": "fb599a37-1f90-484b-9888-f2b42c41c78b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 102
        },
        {
            "id": "b7bc6b96-9ba5-436d-bb36-7bb20d7e5127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 103
        },
        {
            "id": "4720ec24-45f4-4da7-bb9a-950e6f615b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 104
        },
        {
            "id": "3d736f8d-6f07-4525-b91d-78bce78934f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 105
        },
        {
            "id": "2838b7cd-a676-41d4-9c64-8beb39cd8a63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 106
        },
        {
            "id": "2a9da69c-520c-47b1-ae3c-c89b57dc5320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 107
        },
        {
            "id": "72d6b40d-ae89-406f-8c85-29026a8fd84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 108
        },
        {
            "id": "f14a6862-6be3-4711-9a89-bd28e1a99e60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 109
        },
        {
            "id": "12704511-fe0a-4ced-976d-792ad98ef1a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 110
        },
        {
            "id": "c664e116-cd55-45ae-895a-407f2df2480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 111
        },
        {
            "id": "6497d03f-8a9c-484f-bc5e-1920100647b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 112
        },
        {
            "id": "29d06eb2-1461-4940-b8b2-2cd8c0219b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 113
        },
        {
            "id": "ad2486bf-34ce-44b5-a852-a0f222a04beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 114
        },
        {
            "id": "a558205c-77b1-4b03-b9be-1e2ab40c9405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 115
        },
        {
            "id": "ca50c1ea-fdcd-4db7-9881-56aa7af4f8ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 116
        },
        {
            "id": "1e57d61e-4c7a-4738-a671-8eefdb62c4b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 117
        },
        {
            "id": "6a21b122-02e4-4112-8e98-b68e1e88eed0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 118
        },
        {
            "id": "3e80923a-6539-41e0-94d9-4ffdb4d46ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 119
        },
        {
            "id": "588d33ee-82cd-4f89-9646-bd61c01833d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 120
        },
        {
            "id": "5e8305aa-d433-40b6-96ad-014447a021d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 121
        },
        {
            "id": "16b5eb22-3740-414a-b204-09b90900d565",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 122
        },
        {
            "id": "5db726d4-42ba-4509-a6fb-1e500e8869e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 894
        },
        {
            "id": "d23ba58e-6953-42dd-a3ef-1e29a558e5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 33
        },
        {
            "id": "998be094-70b1-4d05-b018-bb93a1da5c3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 34
        },
        {
            "id": "e4d789f8-2ab1-49f8-b8d0-fd4eddfd7035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 35
        },
        {
            "id": "7729edd2-cbc7-4a7e-a357-307fb875f789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 39
        },
        {
            "id": "d7ed8149-32c1-438d-86f8-2327ee708390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 40
        },
        {
            "id": "8812151b-3821-4f9e-b94d-5216a307c295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 41
        },
        {
            "id": "4029aac9-75c1-4eb3-b235-c8229843a7be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "5538fcb4-2e36-4ef1-bca4-65b40c8c1235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "6b2b7ae3-fa72-4764-94d9-f3a5adf93763",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 59
        },
        {
            "id": "229ecd30-f13b-4092-8894-3c35f5c12f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 63
        },
        {
            "id": "8b2481c2-32f4-4ddd-8f1a-19aaa5fd25f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 64
        },
        {
            "id": "a46507db-d82d-40ca-a85d-e47805fa5607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 74
        },
        {
            "id": "b856a251-777d-477e-9f9b-5c71c300f30f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 83
        },
        {
            "id": "f6ad5329-974c-45af-8c97-cea084a96256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 84
        },
        {
            "id": "3bb65a13-0e4b-4829-ad31-e23365b3e10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 86
        },
        {
            "id": "2eec352b-4211-4905-8326-bccc70390f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 87
        },
        {
            "id": "4bf1a531-d764-4cfd-aa03-9ce8d1750d78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 88
        },
        {
            "id": "9d19b336-b880-4a02-9e7b-49dadc16a999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 89
        },
        {
            "id": "791ec390-b92d-4f90-ae5c-f048b49a86c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 90
        },
        {
            "id": "d3424796-fe91-4aca-92af-cec7ad239f53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 92
        },
        {
            "id": "6d05fcd3-70f7-42f9-bed3-e4d0565a55f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 93
        },
        {
            "id": "538b0bed-8caa-4fd9-86a1-bceec5f6f81c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 100
        },
        {
            "id": "cc1ff01a-9fce-47d0-a564-8f83f9cad7e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 103
        },
        {
            "id": "1d882dca-e3fe-4fe1-9d46-62ec4b94b83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 105
        },
        {
            "id": "bf93a921-5a2b-485d-b49d-5450dd197723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 106
        },
        {
            "id": "fa86af9b-9291-4b8b-a0ab-7d06dbbfdd9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 113
        },
        {
            "id": "4c15b9fa-336d-4140-b645-9949f4caa7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 115
        },
        {
            "id": "ac418cc0-3596-4471-9133-fbe645c319ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 118
        },
        {
            "id": "f8a23c94-7cd8-4e15-bee7-9ec06fff5281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 119
        },
        {
            "id": "50317c6a-36fd-49c3-b15e-291fa4a68946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 120
        },
        {
            "id": "bd3a1225-9e55-42bc-b21e-2c445d1c595c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 121
        },
        {
            "id": "454eb6ee-6a80-4862-a092-d087eff15aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 122
        },
        {
            "id": "91eba203-c578-4ab9-b47c-7a01c66c4655",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 894
        },
        {
            "id": "31caf8e4-bf70-4b18-8a1c-9fa7dce026c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 33
        },
        {
            "id": "bd027ec5-96e5-4de2-a928-d333f0e4b068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 34
        },
        {
            "id": "496aa687-cdf1-434f-afb2-658ef6f04a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 35
        },
        {
            "id": "b25140d4-17eb-4f2a-9700-49d54e287a11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 39
        },
        {
            "id": "7e769396-2a9a-4a4d-a154-2106419b3551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 40
        },
        {
            "id": "f075d4cd-27ae-40b9-a3cd-f5710a4d1c88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 41
        },
        {
            "id": "51dc0add-8ee5-4fff-a480-a8e21204baeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 47
        },
        {
            "id": "f046897e-f9a1-4dc7-8554-14fc2a6e6af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 58
        },
        {
            "id": "7c7e6d37-7812-40d0-9c98-996f5014ed62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 59
        },
        {
            "id": "076873d9-4562-4b99-8c08-909d91cf79ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 63
        },
        {
            "id": "14fde168-476b-4180-a5f3-57b9ad3d0789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 64
        },
        {
            "id": "30039fad-6c35-4606-96f1-c6833069a161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 74
        },
        {
            "id": "7c97687c-98b7-4bb3-9c36-becd4f042ba2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 83
        },
        {
            "id": "0bfe74fa-5f8d-4f2f-a252-c1890e659330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 84
        },
        {
            "id": "bff81960-856a-4cd2-ace7-9f73b9328505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 86
        },
        {
            "id": "0f796126-4262-4c1e-ae38-396336683028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 87
        },
        {
            "id": "69782418-f354-47eb-ad0a-70b9c70a6a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 88
        },
        {
            "id": "2ab69471-aa99-460e-8f0d-bd3ebf3cba06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 89
        },
        {
            "id": "26ad006d-3cab-4eff-a771-2d8c5015fd7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 90
        },
        {
            "id": "e806f4c9-ce6c-4e09-be14-de61209c6c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 92
        },
        {
            "id": "76ce3c36-a96c-480e-9eec-916a3c6c4dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 93
        },
        {
            "id": "ba2d12ec-700b-46c5-b97b-774b20d001e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 100
        },
        {
            "id": "736bd730-b2bb-45c1-9f8a-9dcf74a249d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 103
        },
        {
            "id": "32780db7-4f22-426a-8460-8be484c19c20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 105
        },
        {
            "id": "5217c661-c456-4bf5-bbc1-7be3a2345335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 106
        },
        {
            "id": "7365bbfa-5f82-43ad-b851-8f291c660977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 113
        },
        {
            "id": "ce070fc0-dcbb-4e91-a0d9-a0967cd49a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 115
        },
        {
            "id": "dfd47c82-2798-4909-ab78-5695f030b4f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 118
        },
        {
            "id": "af86c736-2a00-402c-a0e3-51d2bd077bb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 119
        },
        {
            "id": "b9601588-2423-4e97-96d1-773921c79a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 120
        },
        {
            "id": "506496ff-1cbe-4adf-b832-d23c27f70e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 121
        },
        {
            "id": "ef0f4fc6-13d2-4043-8c7e-49a3588b8b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 122
        },
        {
            "id": "35072ba6-9496-45dd-a098-bae03d25c73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 894
        },
        {
            "id": "941abcf0-8b58-480f-a855-d23db583eac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 33
        },
        {
            "id": "40d0d630-9630-4040-9fb5-74e40b2dcfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 34
        },
        {
            "id": "93621634-108f-43f5-bfca-2d8e3c26fdb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 35
        },
        {
            "id": "a43230bf-2f3f-44ab-bae6-028d72654c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 39
        },
        {
            "id": "74b49ddd-9a12-4be0-b57f-8e0d512db6f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 40
        },
        {
            "id": "2ca31378-e31c-417a-8ffc-326f2419b6f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 41
        },
        {
            "id": "7361c1b0-8311-4f22-b0ef-6705afa76c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 47
        },
        {
            "id": "38c6bd6b-8891-4c64-9e70-987350414960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 58
        },
        {
            "id": "16bf2b2e-eb75-4732-bee5-56ec4a8a336b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 59
        },
        {
            "id": "c369f792-df0a-403b-b683-9bfbabefbb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 63
        },
        {
            "id": "095840d3-24e1-4a51-bafe-abddd0304c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 64
        },
        {
            "id": "2dd20985-503f-4082-b734-731e8547257e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 74
        },
        {
            "id": "3b375aa2-dd62-481d-9702-99daeae6a9b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 83
        },
        {
            "id": "7dd71535-5b17-4097-8f35-8ad629d4b65e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 84
        },
        {
            "id": "c77cc174-7b1c-4ad0-8cdd-f9b2086aaa31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 86
        },
        {
            "id": "43c2b164-8d8a-4724-b03a-4ac1270e57e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 87
        },
        {
            "id": "29f0479a-b15d-46e2-956c-0ae032160cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 88
        },
        {
            "id": "4f2151cf-82e1-40df-8409-05ac00d1846d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 89
        },
        {
            "id": "6876ac8f-08a4-4dd4-9d7f-878f024d8a2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 90
        },
        {
            "id": "fdf539f0-25a1-4a2a-968a-b839aa4710d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 92
        },
        {
            "id": "d254fdea-c89e-4874-b3d4-f0361d11d776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 93
        },
        {
            "id": "c14b3afd-8104-4215-a34a-425befa2ef21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 100
        },
        {
            "id": "c244ba1b-6d2c-44dd-896d-8b29a570c35d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 103
        },
        {
            "id": "6f6d4bcd-d96f-4cd7-aa12-4d2ed422ab44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 105
        },
        {
            "id": "4c7bee99-b6e9-4d59-8f00-77fab6a9f566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 106
        },
        {
            "id": "84bbad96-a8fc-4b85-8e53-889d8df2aa75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 113
        },
        {
            "id": "e98bf316-d11a-4ef1-b6ef-8b8ca6c0c0e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 115
        },
        {
            "id": "b3582f93-38bb-448c-944e-344dd038b68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 118
        },
        {
            "id": "641be05a-8f70-4eed-af95-45da97ee1838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 119
        },
        {
            "id": "88f4c6e6-8ee5-4412-8b13-63f0dd1f7fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 120
        },
        {
            "id": "f76ddbcb-2380-4ab5-b551-7a3d6570057d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 121
        },
        {
            "id": "e2b8ac72-79cf-477b-a648-a241eb4a7283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 122
        },
        {
            "id": "c4e200b4-62af-4ccb-b58b-51c43103fa11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 894
        },
        {
            "id": "f55f79cd-0e19-481d-be6d-a8a77f6e3bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 33
        },
        {
            "id": "bf4889a7-dab7-44ed-983c-363253fddd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 35
        },
        {
            "id": "9f3d2106-a747-4594-8f47-b9bf4f67d902",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 40
        },
        {
            "id": "9e5a1625-79dd-459c-8c0b-7e2fe6b4b103",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 41
        },
        {
            "id": "47e36ca5-932a-4038-9b00-83153950a212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 45
        },
        {
            "id": "888943e5-9f5a-45c2-bc09-f81152bae368",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 47
        },
        {
            "id": "8512f4fe-61f3-45a5-9bc4-0a41016c91d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 49
        },
        {
            "id": "a2563dbc-ae1c-4f18-87c5-c11cb6dc9705",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 50
        },
        {
            "id": "c00ecdb2-c71b-4903-9ffc-73cfda29c6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 51
        },
        {
            "id": "ad699537-ea29-4817-a4de-65dc5a4dc74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 52
        },
        {
            "id": "18cb7e00-6c68-41e3-ae47-558819adbad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 53
        },
        {
            "id": "d9e8d3e3-b591-4d78-808e-077ec312eef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 55
        },
        {
            "id": "fad09b9b-1442-4be3-ace6-e16bb4d9efcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 57
        },
        {
            "id": "e0ee12fc-9434-4165-9a5a-3dc1eda39376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 58
        },
        {
            "id": "63ef701b-55b6-461c-b792-96272c4b3ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 59
        },
        {
            "id": "a323fdac-4987-486b-a63d-21a09e084991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 63
        },
        {
            "id": "632b5b89-2945-4949-84b3-53f021390518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 74
        },
        {
            "id": "595a0cef-7695-4aeb-8716-036e83cd0fe6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 83
        },
        {
            "id": "84d6e1f4-cc23-4444-aa58-a0daf193d046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 84
        },
        {
            "id": "798dc826-ff34-4247-b273-a2c191ef93d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 86
        },
        {
            "id": "c813e71d-0718-4c58-b8af-e0fb406b058c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 87
        },
        {
            "id": "20ee51cd-f17a-432c-b5c9-ee333c3bbe5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 88
        },
        {
            "id": "c7452e45-25ba-44b8-b276-ba2a56ad6372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 89
        },
        {
            "id": "90213546-b8b8-4b39-8f0c-39181b1b6c4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 90
        },
        {
            "id": "8dc7bc43-224d-4703-8e95-01567809408f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 92
        },
        {
            "id": "829dad7c-13e8-4303-8bc1-50b004ae0477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 93
        },
        {
            "id": "f45c2127-5c48-4bad-b9e7-3061ac77bf87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 103
        },
        {
            "id": "5f00ba4a-386e-4239-887d-94ae60555e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 105
        },
        {
            "id": "7f7ded9e-ef47-4871-b789-6a6163e2ab88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 106
        },
        {
            "id": "b50e109d-2222-47e3-a770-cbab73c78c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 115
        },
        {
            "id": "006253e2-34a8-4d9c-bd06-c935568ac69a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 118
        },
        {
            "id": "4bfd7c85-0338-49f8-987d-f159837398e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 119
        },
        {
            "id": "ec9ea1c9-824a-4b93-81e0-a2a4352872b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 120
        },
        {
            "id": "3c789e52-a8bf-4f8e-9376-81c57688be49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 121
        },
        {
            "id": "86e68447-3050-4915-9a8e-674211a49798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 122
        },
        {
            "id": "aa761b2a-2c81-4303-a511-d469a6997767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 173
        },
        {
            "id": "a1e6a1e7-1e87-45a3-b40a-33588557226f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 894
        },
        {
            "id": "9f1bc808-5493-47ae-b11b-6dbf43259f93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 58,
            "second": 8208
        },
        {
            "id": "a151005b-5a1a-483a-a08f-70755f5c76e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 33
        },
        {
            "id": "56306b90-a0d8-40a3-bab0-5082553795ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 34
        },
        {
            "id": "a2aaed47-4529-483a-b0e8-1bd34421dfab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 35
        },
        {
            "id": "a064bb96-aef4-4532-9ac4-abde4aaea5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 39
        },
        {
            "id": "3b872c9f-4a00-4f9f-8717-ae0a0e4e2a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 40
        },
        {
            "id": "b9abae55-6b65-40cb-900a-358dd32f087e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 41
        },
        {
            "id": "6ed32de5-bff9-4eec-a542-02fd3d1c953a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 45
        },
        {
            "id": "d924bdd3-1088-430a-b35e-fb721c589ebd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 47
        },
        {
            "id": "ff25e704-5bd0-49ca-9570-0113c223319c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 49
        },
        {
            "id": "f1030490-05d0-4c89-8e45-159e63026195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 50
        },
        {
            "id": "eaa21bce-9e8a-442d-a157-9f7e29b1262c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 51
        },
        {
            "id": "d49f0668-3034-4a69-adb4-c667f1f9a116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 52
        },
        {
            "id": "a3cf4799-2345-4ebf-9eba-cf8a8555dc88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 53
        },
        {
            "id": "6c2b13e0-7939-4014-a9d2-92ea78bf1c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 55
        },
        {
            "id": "7ad11206-e17d-4a08-abcb-fa20531bbb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 56
        },
        {
            "id": "9f8f889e-ba32-4508-a3e0-fc670cdc368b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 57
        },
        {
            "id": "db7af406-5db3-4ed1-87a8-9d68e29d3ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 58
        },
        {
            "id": "7b9456a2-9802-44f1-b160-e43773ceb459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 59
        },
        {
            "id": "dc240c84-1c7e-4e51-850b-e9c8343510ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 63
        },
        {
            "id": "6c3e0e7c-39c5-4343-bfcb-484acfcdda68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 64
        },
        {
            "id": "548175d7-0deb-45f7-b367-e9a195ad3687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 74
        },
        {
            "id": "803fbcbf-ea7e-4a33-9cec-b1bc046933c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 83
        },
        {
            "id": "86fad6d6-5d7a-4b15-9283-af23eea0c572",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "be05a406-dfe8-4be1-b8ae-33428d0ebd14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 86
        },
        {
            "id": "7083241a-74ca-47d2-9f85-80e1c67f4d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 87
        },
        {
            "id": "78183f0f-3961-4700-bf0c-892ca1d0e34f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 88
        },
        {
            "id": "99fb5525-e03e-4f7c-8c4e-abf98e415143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "88322eaf-ae2e-461f-b3ba-7e9512f4b526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 90
        },
        {
            "id": "e0236029-46f7-48f8-8f15-621b800a3f88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "98aaf83e-a919-47be-8b76-475a1f76aa82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 93
        },
        {
            "id": "cbe15447-d3f1-4ec8-b951-e906073da9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 103
        },
        {
            "id": "d10210bf-b4f1-488f-8195-75c4071fda06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 105
        },
        {
            "id": "1aaca735-cda7-44fb-86c3-814c6537a4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 106
        },
        {
            "id": "2375a4ef-b62c-4005-8e7c-37cbffca720d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 113
        },
        {
            "id": "3ac812b9-335f-47d4-a837-0ec2962269e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 115
        },
        {
            "id": "418ac29b-7c3d-4321-97cd-7a92ba5ca573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 118
        },
        {
            "id": "c3cc79e7-7abb-4d03-b5ea-66f376004fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 119
        },
        {
            "id": "3bcd91fa-77e7-474c-8458-c4fef68927e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 120
        },
        {
            "id": "68fae563-a226-44e4-aaf2-451d688f70b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 121
        },
        {
            "id": "9a88ae90-210f-4f30-9497-ebb22f546339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 122
        },
        {
            "id": "de5ad0d4-9a41-4511-8e08-acf8c5fa232a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 173
        },
        {
            "id": "9ecab50b-12c0-44f4-b07e-10ec16a39e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 894
        },
        {
            "id": "de7c3de2-21a1-48b8-ad49-3f20dc1ef0dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8208
        },
        {
            "id": "a2475f91-f5e4-45cf-b9c8-25b8b68a9ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 33
        },
        {
            "id": "70b6a30c-5faf-4b39-8f92-a5de0e0eb7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 35
        },
        {
            "id": "4e4bafa9-9169-45cb-9049-154e68ce5909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 40
        },
        {
            "id": "39c771b8-0088-48f2-9310-cffcfd1884a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 41
        },
        {
            "id": "a8484c68-d503-4e24-8337-df2f401b1813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 44
        },
        {
            "id": "e48e90e4-3da5-47d8-afc0-360f5bd9e4c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 45
        },
        {
            "id": "ad92b69c-2b9a-40be-9847-3e16a81c5c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 63,
            "second": 46
        },
        {
            "id": "da8046e6-0f92-42cc-aa8e-9fb02508d2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 47
        },
        {
            "id": "b282b24f-16f6-48d6-bc2f-6f78744b6d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 49
        },
        {
            "id": "a1e5c7c9-0626-470a-b18e-fb6949b1cfa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 50
        },
        {
            "id": "a7041649-b7cb-4e24-99b1-601c6c3e477f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 51
        },
        {
            "id": "14313361-97bd-4f62-a89b-d115c9018383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 52
        },
        {
            "id": "689e9aee-1b85-4775-b2b6-703ab846d22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 53
        },
        {
            "id": "3e62926f-34ea-4f03-9762-4d61d312ac28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 55
        },
        {
            "id": "45f2bc26-0297-41b2-bb37-a4499ebd91e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 56
        },
        {
            "id": "ad4b3e6c-8e53-40ce-a451-f433fd2266f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 57
        },
        {
            "id": "b94c1835-2a8c-4943-90da-4b65d992bad9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 58
        },
        {
            "id": "eafb5d86-2ae6-4946-8fb3-f15db2b497df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 59
        },
        {
            "id": "772a3a43-ba95-406a-915e-eadafca4738d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 63
        },
        {
            "id": "5f096707-5f7b-4a93-b5d1-a28a9c3b46dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 64
        },
        {
            "id": "984a1261-fde6-4b65-9988-cad3f361ebab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 74
        },
        {
            "id": "4c0e432f-c773-4653-961c-a288ab0f5446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 83
        },
        {
            "id": "5c58881c-0170-4849-ba3b-7138d293f7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 84
        },
        {
            "id": "650cd708-b1e5-4b2a-823c-0cbdca3257b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 86
        },
        {
            "id": "ad48b4e7-16a1-46f5-89f7-f589f4683c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 87
        },
        {
            "id": "f29d1078-8b05-435a-8d0c-2e4922e2e126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 88
        },
        {
            "id": "96a3a134-deb9-4fd9-a573-ebc3f41789ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 89
        },
        {
            "id": "1a101819-aa4c-4333-a2b9-642d8490924c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 90
        },
        {
            "id": "be2e85b9-e452-4195-a2e8-468856ff5179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 92
        },
        {
            "id": "1723bc22-37fd-4c7d-b080-a2f381f10057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 93
        },
        {
            "id": "b3eadd7f-cad1-4e1b-87a4-cbde31bf13c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 97
        },
        {
            "id": "aee43f9b-48e6-47cc-9bc0-1f4ecb6534b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 99
        },
        {
            "id": "ef61c83b-9f92-4455-b2ce-ae49a8ee2af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 100
        },
        {
            "id": "7ebc2b2e-b9dd-4f1f-8e4e-56b5f5434b78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 101
        },
        {
            "id": "b8e80ed4-8d56-4956-a5ce-afcd52ae2bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 103
        },
        {
            "id": "106b0866-edad-46ea-8e29-f962ad9d2d95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 105
        },
        {
            "id": "f1d6bca8-28ae-4419-8e52-504257f8996b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 63,
            "second": 106
        },
        {
            "id": "f00cae18-c542-4c9c-8a3e-9db43127db43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 109
        },
        {
            "id": "4259d62b-7840-4490-a2e7-74c3fffad8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 110
        },
        {
            "id": "d54c6e6f-8d12-44d2-9592-b3eb52155ab7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 111
        },
        {
            "id": "c322fd09-97f4-48cc-99a2-2c181d980a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 112
        },
        {
            "id": "0133ffaa-9f0b-48c6-819b-7d571e8cc70c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 113
        },
        {
            "id": "6c35d1ed-d597-4d27-a89a-00a29ad32c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 114
        },
        {
            "id": "8a228e22-6b87-464a-bb3b-041263cd5ab4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 115
        },
        {
            "id": "1228de29-bd24-49ca-be11-f075ec0bd647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 117
        },
        {
            "id": "df3be831-fbad-4573-b110-3057cd7aaf0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 118
        },
        {
            "id": "c3feafee-0138-444d-9406-6a81c84b0414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 119
        },
        {
            "id": "aa5a96f8-d3f4-4037-ab27-3844f5d9f817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 120
        },
        {
            "id": "8d95f88c-12d3-41ba-a760-6ed72aed6a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 121
        },
        {
            "id": "259c6930-6629-41fe-96d5-68cfdb5c0f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 122
        },
        {
            "id": "cab1a645-74a7-4dc6-a94e-0aac6b263532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 173
        },
        {
            "id": "229b84c5-d4ff-4154-9caa-eb25063742db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 894
        },
        {
            "id": "15d2a802-a17a-43ce-b76c-4bdad3966aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 63,
            "second": 8208
        },
        {
            "id": "80e9a9a2-e098-4181-bea6-a7119ea78fd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 33
        },
        {
            "id": "9f2ecef2-75aa-4ce9-9b83-c728da0f7c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 35
        },
        {
            "id": "c9e2d0c2-eca8-4273-802e-1dcab217b979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 40
        },
        {
            "id": "1ed1e374-dccf-44f6-992c-eb1a271973f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 41
        },
        {
            "id": "bb24654a-01b1-4352-935c-7d8a799536d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 45
        },
        {
            "id": "fc1f3671-1803-415f-9b08-bd14d28538b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 47
        },
        {
            "id": "5f066e47-4924-4675-9058-3158964c4efe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "bb0bf24b-63e2-461f-8326-5fe37b11c5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 50
        },
        {
            "id": "75958677-564d-475d-814c-c2800e9e0cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 51
        },
        {
            "id": "3234c91a-d390-450f-a1f5-87e3d1174fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 52
        },
        {
            "id": "6a801512-2255-4776-94bc-a8db5710032d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 53
        },
        {
            "id": "ba27e7f9-3b20-4219-bbe7-45fc546ea941",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 55
        },
        {
            "id": "1be62f4d-d8b2-494b-b180-61bc616d5d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 56
        },
        {
            "id": "b0abac1a-3e3a-40fb-8d82-560fff231960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 57
        },
        {
            "id": "e4171cd9-e0bd-4928-87e3-893d5b72a813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 58
        },
        {
            "id": "e8525e4f-6087-471e-823c-e5df6b8a773f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 59
        },
        {
            "id": "1633742a-48f8-447e-8403-a57be5de67b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 63
        },
        {
            "id": "4454043b-bef6-445e-98e7-dbb9353c9c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 64
        },
        {
            "id": "92feeec3-856c-43eb-8eb6-c5b7e4659436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "206ef181-c794-42c8-bc9c-714d41d1d1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 83
        },
        {
            "id": "0bc29183-5903-458e-a738-88aa09d3a7e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 84
        },
        {
            "id": "b39ddd04-9f74-4658-93b5-fc2706cdb822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 86
        },
        {
            "id": "d40a180f-65d9-44e7-90a3-0e1e1e55d7a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "0d3f0de9-6aed-4d7c-933a-7f6d9caac20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 88
        },
        {
            "id": "5d72a163-6ea2-4f75-99d5-5820299401d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 89
        },
        {
            "id": "3353d6a4-ff57-4e05-ab8b-12b0468cc477",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 90
        },
        {
            "id": "fe55f3b7-7df0-46bb-8717-8d4a5eff1960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 92
        },
        {
            "id": "167b8d92-47f5-42a9-9ede-dcaa926fdc8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 93
        },
        {
            "id": "9e1d53ef-63da-4ff1-921d-e7c0ff2fa6b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 97
        },
        {
            "id": "1e4b9717-6c7c-4af6-8169-cb06c7210607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 99
        },
        {
            "id": "85234a6d-aef9-491e-af4f-059b5a1df8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 100
        },
        {
            "id": "8f685331-8632-4a17-aa53-44760fbafa37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 103
        },
        {
            "id": "2f9624f3-e9f4-424c-b3fe-4cdcd3bc6826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 105
        },
        {
            "id": "a229536f-49f3-4837-a969-d588f5145a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 106
        },
        {
            "id": "b3e9c345-6187-4bba-8787-39e12e0457af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 111
        },
        {
            "id": "5bb21511-dc5c-47f8-b384-a70a3907bf59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 113
        },
        {
            "id": "0454ff79-2a8d-4aca-8f86-0d70605c24c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 114
        },
        {
            "id": "6478ee59-3c0f-47ab-82c5-f254c6c88845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 115
        },
        {
            "id": "1301ee0e-71a7-4515-874f-2b5681fa8d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 117
        },
        {
            "id": "8465e7b9-d210-47c6-826b-891f901a7fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 118
        },
        {
            "id": "b53f8ad6-d408-48a2-b72f-5d0d18fe34c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 119
        },
        {
            "id": "a4c522dc-0d22-407f-a171-80895b6a6db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 120
        },
        {
            "id": "380d1cf1-2f86-43a2-98cd-29b8ca44372b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 121
        },
        {
            "id": "990ef81d-31cd-4007-9aba-96f860311220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 122
        },
        {
            "id": "1c694f9b-90cf-4995-8c21-8b67d9296eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 173
        },
        {
            "id": "4050e9c2-a313-47a5-ac9a-fa83ea61157c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 894
        },
        {
            "id": "cb61a7e6-c065-46d6-918f-5d42dfbf1bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 8208
        },
        {
            "id": "17594836-728f-4835-bd50-8919c2723505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 33
        },
        {
            "id": "bb22df16-dee3-4ba9-90ce-f4a9897d7e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 34
        },
        {
            "id": "77e852dd-6a5b-4776-b244-cf6b7d520e7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 35
        },
        {
            "id": "b2824d5d-d55d-40dd-b3dc-8312053375d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 39
        },
        {
            "id": "03345059-8a18-4064-8049-b00d2c4856a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 40
        },
        {
            "id": "59b98500-e4cd-4905-8bf3-22f845f34a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 41
        },
        {
            "id": "481e23a2-8c92-4444-9d55-aab660c20193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 47
        },
        {
            "id": "9a877f95-af0c-4c3b-9cdd-e60819c44bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 49
        },
        {
            "id": "463227d5-b7b0-434b-9ad4-6c16e3267b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 50
        },
        {
            "id": "7ab22304-1fb1-48ae-b4f7-d0fd56bf6ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "f2e8e883-7ff5-4840-adaf-3a29b457dde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 52
        },
        {
            "id": "b2a7162e-b72f-4aa3-8225-79af6e407f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 53
        },
        {
            "id": "d282a749-9707-4517-9a62-6e20e97c0eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "f9d04bcd-91c8-4b49-943d-fdc1a252daba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 56
        },
        {
            "id": "2293e5ec-83e3-41be-9086-5a54e2771126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 57
        },
        {
            "id": "c8daa2a1-6213-486b-8736-8569d1e0c0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 58
        },
        {
            "id": "92e45d50-23bd-41a9-b12b-b1ec11a92839",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 59
        },
        {
            "id": "9e511dfb-964d-4321-bcfa-12e2804c2913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 63
        },
        {
            "id": "c8796bb7-11dd-4191-9771-a49f106c4dff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 64
        },
        {
            "id": "6df938a0-e458-4eb3-9160-8b0ee86a9177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 74
        },
        {
            "id": "0e2cc605-e18b-476f-9917-1266a42787cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 83
        },
        {
            "id": "fb0953a8-5a22-4b7a-9f7b-fb2fa5886068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "1c65ea08-4635-4b5e-8826-5268e521d73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "4401c52f-feda-4b97-aa1e-d8064242bf12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "2353140b-4954-4f2a-8765-9ccab09678b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 88
        },
        {
            "id": "2b1aa657-7224-4e11-a0fd-7e0042727e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "04ffffee-7549-419a-97f8-f55a7a32852c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 90
        },
        {
            "id": "545d0dc6-93c1-4893-bcc9-d009dc6cc315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 92
        },
        {
            "id": "0fc16ee2-0e9e-4837-9f48-20afc4a2e021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 93
        },
        {
            "id": "063f2a3a-bd75-4009-b64c-99e46eb0efed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 100
        },
        {
            "id": "b12f6504-8d7d-4092-931f-ce5be46f2e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 103
        },
        {
            "id": "1939cba2-976b-4462-a14d-b6046f9ddaa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 105
        },
        {
            "id": "274d24b5-e67f-49bc-a07d-785cc8e3d752",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 106
        },
        {
            "id": "d15b4f94-2b9a-4319-9cbb-6756fae31cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 113
        },
        {
            "id": "da366ee2-b1a0-4312-9276-0f7341419e5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 115
        },
        {
            "id": "b87bf273-eadb-4e64-bb1e-8ad4520eda04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "576185a1-2f6b-4f67-8c52-2d2ef2755229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "53caae1c-5df7-4cc8-a807-0e3cfee55b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 120
        },
        {
            "id": "e4fea179-f798-4697-a1f7-20607aaf07db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "7a15e9a0-3f50-429d-bee2-886b35e690b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 122
        },
        {
            "id": "2e00fd32-e9fb-4925-8d50-deb5a5e61277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 894
        },
        {
            "id": "ade08eaf-c645-4dfa-a4e7-0d1d62e65aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 33
        },
        {
            "id": "3e5d35ff-3399-42c1-8f04-59f7853face4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 34
        },
        {
            "id": "12c8002d-2aa6-4f3d-9560-0395c77a4dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 35
        },
        {
            "id": "fff4eea4-dfa2-4ffd-ad56-ee7a9c286dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 39
        },
        {
            "id": "4bebc536-adf9-46d6-bffa-22df6b3d7fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 40
        },
        {
            "id": "ca17fd92-29e3-4bec-9e04-ea4848792432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 41
        },
        {
            "id": "2d24d23c-080f-473e-97fe-c881e000a948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 47
        },
        {
            "id": "9778bc94-ad91-4121-9248-52d02ca4e923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 48
        },
        {
            "id": "14b4b03d-f121-474b-b0df-eeaebd482daa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 49
        },
        {
            "id": "192a9a22-abe7-4e84-b015-a67bfeee9bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 50
        },
        {
            "id": "66c80d33-adb9-4a7c-bf98-4319fe9f80bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 51
        },
        {
            "id": "7c413aef-4f33-4690-a5fd-e61ccfabe10a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 52
        },
        {
            "id": "c5c75748-a4a0-45b2-ad7b-38c3ddbc722f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 53
        },
        {
            "id": "e2153301-2d8a-48f3-bd25-8ed011a62557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 54
        },
        {
            "id": "705fae41-8f36-4a54-b882-bb5dc04bc770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 55
        },
        {
            "id": "bcc97f3a-7be3-46e7-b590-92b9dfe3551e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 56
        },
        {
            "id": "5e3d5321-6373-4611-972a-d34c12b59a26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 57
        },
        {
            "id": "5e026289-55f9-4662-9227-f530d9ba2ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 58
        },
        {
            "id": "0cfd0a3f-1545-4d7e-9f71-174242892c8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 59
        },
        {
            "id": "8008550d-5478-4486-a065-4bd6ec135b01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 63
        },
        {
            "id": "70f03461-1ce2-4d03-80ac-6c4262f7cf28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 64
        },
        {
            "id": "8066c0fd-27d4-4d80-819f-8cc84a7b2098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 65
        },
        {
            "id": "458f1700-e826-4e5f-8e8a-49d53931f69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 66
        },
        {
            "id": "666659ea-ad4f-4c8a-9b2e-56c7406ea676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 67
        },
        {
            "id": "5531725e-0b73-4534-ace1-b5d1d19c68a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 68
        },
        {
            "id": "3d0544c4-421e-4c34-b5bf-a01d0dc9ceef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 69
        },
        {
            "id": "deaea40a-d9e1-453a-98b7-0eca30604e9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 70
        },
        {
            "id": "ff4bc742-c7fc-4489-b8e8-3d9c6a11c4df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 71
        },
        {
            "id": "5a4257ad-7af0-459a-b3e8-950a6d89a86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 72
        },
        {
            "id": "f945745b-fd34-4f9b-8a8b-9642dd4ef279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 73
        },
        {
            "id": "9b86dc1b-ffc1-4c0c-8c57-7e360121abfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "fc6fb3e5-db79-4648-a9ad-0cf5f63219cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 75
        },
        {
            "id": "68e8c5de-0c77-4d7d-86d6-0c32c0c63d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 76
        },
        {
            "id": "23de4829-2dc7-402d-abc3-e05c19ded6f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 77
        },
        {
            "id": "0466bc0a-4c67-482f-bef4-8b90a637bf60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 78
        },
        {
            "id": "93383ee4-d6fd-4bbd-a472-fd07a6a66a17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 79
        },
        {
            "id": "94806d9e-50c9-4e1e-91b9-dfef99e80232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 80
        },
        {
            "id": "3ae1bce4-b0d3-434b-b07b-adf462eab85b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 81
        },
        {
            "id": "cdb862bb-712b-4c2e-8580-2202663f20d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 82
        },
        {
            "id": "8598b3e9-761e-405e-8e33-8d5f20b43c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 83
        },
        {
            "id": "b923ef07-593f-45ea-a7c3-0ebb38b6eb41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 66,
            "second": 84
        },
        {
            "id": "f590bc9e-b2e5-4188-b4cd-43aba0469147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 85
        },
        {
            "id": "d461bdc6-13f6-483a-8513-fa2d9e42fd11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 86
        },
        {
            "id": "dd9ffe83-47cd-4a87-8b76-67a006ad4a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 87
        },
        {
            "id": "36d24e29-cc0a-45f7-ab84-2459672c6ab9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 88
        },
        {
            "id": "913e9b6e-d210-4a79-b1a6-ba805ef361be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "37d3bfbb-e4e6-418e-b954-826e0438d86e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 90
        },
        {
            "id": "98b1913b-87ea-4cfa-a7bc-cd2f5480792a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 92
        },
        {
            "id": "683d0859-678b-4c3e-abf2-36dd04f5d6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 93
        },
        {
            "id": "15e32c67-17ff-4f86-9c3a-d6ca0537cf6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 98
        },
        {
            "id": "1f3f4eb9-4089-4317-b704-13ae3dcab344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 100
        },
        {
            "id": "78277d19-9f92-40a0-a61b-c9759f96068d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 101
        },
        {
            "id": "1e10b946-447a-48c2-a5e4-27df9565c66c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 102
        },
        {
            "id": "eeb2e434-9c28-4b00-b489-301332dccf83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 103
        },
        {
            "id": "80e2d158-9ae6-4365-993a-49184b1a2d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 104
        },
        {
            "id": "857b7c1e-247c-4b49-a2cc-b89f41dafd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 105
        },
        {
            "id": "f33a8241-998d-42bc-a0d5-cd6b0f038256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 106
        },
        {
            "id": "38a60fdf-b4df-4776-bcdd-9d647dd78301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 107
        },
        {
            "id": "bd1160e4-b236-48a4-9155-899cc8d16896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 108
        },
        {
            "id": "460acf0d-75aa-4186-abaf-50ea202969ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 109
        },
        {
            "id": "434eeeb9-6be6-4025-b117-1edec55b2857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 110
        },
        {
            "id": "535c9262-e06e-4c74-b5f2-8e0bbe21ecc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 112
        },
        {
            "id": "a0f9d8ed-dfa5-4636-aab9-2cdd81eae312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 113
        },
        {
            "id": "82e82a23-1aac-4c6a-ad8a-c7185e69a7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 115
        },
        {
            "id": "37aa1401-ba5d-4db7-843a-6ca0d52d929e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 116
        },
        {
            "id": "f5b802ce-2b2f-420e-8daa-5d5f35c38677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 118
        },
        {
            "id": "c8fae58a-ef89-4878-9ba0-f6284f2b7eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 119
        },
        {
            "id": "bf57ca54-116a-4003-9826-61c6a6211fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 120
        },
        {
            "id": "9d490728-e911-4685-995d-4cca094f3f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 121
        },
        {
            "id": "f40784d9-a4a3-415d-bf63-4303383514ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 122
        },
        {
            "id": "8baf4483-fecb-440b-844e-3a3ea22505f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 894
        },
        {
            "id": "a3e114d4-88e7-4016-8112-c13d15bf2f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 33
        },
        {
            "id": "d6715c21-9a47-474f-ba28-4690bcc40c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 35
        },
        {
            "id": "2e302bcf-6bc0-4f89-82e2-2284e4cb146e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 40
        },
        {
            "id": "b58d3416-8f28-4543-9d26-a11c914b1e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 41
        },
        {
            "id": "403b76e8-2e63-45ed-b85f-e44f51932364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 45
        },
        {
            "id": "bc25ac60-a09c-44c6-91bf-39bbca254f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 47
        },
        {
            "id": "9d7914e5-3e8e-48a4-8b2f-f11c239207fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 48
        },
        {
            "id": "33ca7d72-ded4-4d0f-92e1-5bd68232b589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "9f7c0f1f-824b-47d7-9baf-7a2de02d5408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 50
        },
        {
            "id": "7ebd881d-32f8-4e4f-beda-12fec16093d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "63ad27d4-94ac-4206-a3d0-59c4123a4aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 52
        },
        {
            "id": "d69d8bc8-8c94-4438-9545-c0c2847111f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 53
        },
        {
            "id": "33a7854c-2595-49ca-9514-080ae67d8aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 54
        },
        {
            "id": "f8d5098d-3452-4c3c-a5a6-a50b8b799c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 55
        },
        {
            "id": "fee896a8-e371-455c-9949-666c9e734df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 56
        },
        {
            "id": "7f68aa65-05a2-4978-9954-c897c87b8339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 57
        },
        {
            "id": "f88ded6f-14e0-4e82-b928-e388c158f95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 58
        },
        {
            "id": "3be6200a-8834-4720-a87a-c29679f187ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 59
        },
        {
            "id": "e4e1b4fb-0f93-4d78-b98b-3dcba47a7259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 63
        },
        {
            "id": "7e47368e-77ae-40c7-8472-e556a293397d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 64
        },
        {
            "id": "9e70195f-66a3-4987-be16-85c3805f1040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 65
        },
        {
            "id": "0d4cd8f4-5b14-460e-9b1b-c5eefba629bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 66
        },
        {
            "id": "139833bf-a295-42d3-91f8-36f9d4cbdd9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "66929cfb-4163-4b57-86ac-6bf7c7708c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 68
        },
        {
            "id": "5a95cc41-669f-4d37-a88a-ac9a0e1c5ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 69
        },
        {
            "id": "fc0b8734-e399-480c-b9fa-cf6da922fa1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 70
        },
        {
            "id": "9f59587b-59a3-4cb0-82d2-e50e1c82cd05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "4e4252fe-e4f6-4b41-8dd0-31b7ca71ee46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 72
        },
        {
            "id": "5e6e7327-843d-44a6-9dab-9fc3e9bab6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 73
        },
        {
            "id": "b2739645-9e08-4412-af1b-80ac051a14bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 74
        },
        {
            "id": "d454d6be-e14e-464a-b0fd-63275f6234c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 75
        },
        {
            "id": "0083377b-9893-415d-9beb-b1be2d22e3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 76
        },
        {
            "id": "11c51439-ac14-43a5-94d7-8a862bad3806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 77
        },
        {
            "id": "6df2e778-9f70-4b33-aa8c-13cd9e44e216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 78
        },
        {
            "id": "9331a6b1-6139-4271-94ed-ccbbaae237bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "5ab2748e-7175-4824-a0d0-e8261e79cdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 80
        },
        {
            "id": "624adc13-c514-4b59-ac86-a4858978d671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "c099c1ca-7755-44cd-89a3-20ccb6250a64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 82
        },
        {
            "id": "2a049ba0-7364-4430-a9e5-36131ec5952d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 83
        },
        {
            "id": "35c52aad-1e80-4825-ab0e-363553000e21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "7551b376-60a7-4323-b3c2-70a6c1856d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 85
        },
        {
            "id": "da00d86e-c18c-4b30-9341-7119dab72769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 86
        },
        {
            "id": "d2a24695-eb95-4194-a090-341862f4d44c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 87
        },
        {
            "id": "922d8402-4d8d-4131-b460-1b159d71ed9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 88
        },
        {
            "id": "08b18af5-a028-4176-b842-3ec65c0fd467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 89
        },
        {
            "id": "ea4c2740-9db1-47c6-bf0e-8f07dc40486a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 90
        },
        {
            "id": "37baf9c6-6f7e-483d-bc3b-3b1ed93dc7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 92
        },
        {
            "id": "06c091d2-9ded-4120-8cd7-6efcb9d77bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 93
        },
        {
            "id": "f6370fee-f91b-4ab2-8c7c-9b162da25774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 97
        },
        {
            "id": "2a0f2f78-02f7-46eb-b31a-ebb642bd89c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 98
        },
        {
            "id": "465a3943-8059-47af-b29c-8a7d176da93e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 99
        },
        {
            "id": "cf9901c6-9ac1-45e6-b7a1-7e444bde93f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 100
        },
        {
            "id": "e5566467-95c4-4118-aec7-0b10184a7456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 101
        },
        {
            "id": "6bc57ecc-de0d-48be-92a5-b8deb6957e2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 102
        },
        {
            "id": "a85d3586-a97d-4759-8d77-3345d4f725ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 103
        },
        {
            "id": "e0da45d3-f026-4848-8463-541e7da255d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 104
        },
        {
            "id": "317994e3-1ee7-4c02-8b5e-b24efdacae19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 105
        },
        {
            "id": "3c235fd9-3783-4ff7-993f-0abae0bdbba6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 106
        },
        {
            "id": "ecf0febd-ea34-40c1-9d17-f26a1280471d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 107
        },
        {
            "id": "073e27bf-f1f7-43d8-a791-cd02276c2d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 108
        },
        {
            "id": "9ffec8d5-0d0a-4018-8f69-a3b5b28a2641",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 109
        },
        {
            "id": "3d41fe91-72b4-46c5-9a2a-1481e1c75266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 110
        },
        {
            "id": "7d524b9b-6eb7-4c21-a4b3-1f19bd37ae38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 111
        },
        {
            "id": "2b541876-47fe-4b03-a0e7-e6b74a203be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 112
        },
        {
            "id": "cd444207-f632-4cd0-812c-f9fc48704ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 113
        },
        {
            "id": "999dfff1-e041-48dc-ae67-376be939651b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 114
        },
        {
            "id": "213ece2c-6fb4-4eba-b4a6-8b04ab8f48dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 115
        },
        {
            "id": "65e6329c-dad7-49a4-945d-992a8d943bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 116
        },
        {
            "id": "9c40245b-a3a4-4365-a6eb-218a94d51c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 117
        },
        {
            "id": "799f7ab1-9f9f-4d2f-8fe3-993eba33ae7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 118
        },
        {
            "id": "e63f10ff-4e44-42ce-b16e-1f3f2b91fa2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 119
        },
        {
            "id": "a7c17859-b732-4d29-9f5b-fc4f158bd5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 120
        },
        {
            "id": "b01448ca-e4e1-4e9e-9cf5-3625cf686aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 121
        },
        {
            "id": "bcbe3f1b-72dd-41c9-834a-3bae51ba9e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 122
        },
        {
            "id": "80059cc7-92b4-49a5-9f43-8901e9d50099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 173
        },
        {
            "id": "85e46ed5-869a-4d2b-94a6-4b54f6604f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 894
        },
        {
            "id": "f00ffecd-4a8f-4550-b7c9-c0433c554ee1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8208
        },
        {
            "id": "fc8b953c-5fcc-4ad1-afdb-9661d5f3cec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 33
        },
        {
            "id": "51ae4fdc-a8ac-4e0b-ba06-e07d6fd86be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 34
        },
        {
            "id": "e3df5cfb-a5cb-47d8-9ddb-207f711c5f3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 35
        },
        {
            "id": "8ee4c67d-1291-4b7e-b2ef-e433d92618ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 39
        },
        {
            "id": "52ca73eb-1aa9-4499-bb24-cbe16524d806",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 40
        },
        {
            "id": "d111de24-5e8d-4c47-852f-d475681b6350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 41
        },
        {
            "id": "a0290819-02b6-478d-8180-2ee16d21d04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 47
        },
        {
            "id": "bb405f46-a9e4-47ca-93a3-c8c9bb1ede37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 48
        },
        {
            "id": "b4b493b1-e255-4d1e-ac13-fcf80888d8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 49
        },
        {
            "id": "e7c1e455-8e27-442f-98d7-3aff317ffafb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 50
        },
        {
            "id": "6dd1a783-f7ca-4387-b818-c6f2b41691fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 51
        },
        {
            "id": "8485356d-1e31-47e5-aded-77e40802cd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 52
        },
        {
            "id": "3e2faa19-156a-4887-b2df-31a6a763f26c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 53
        },
        {
            "id": "0f6607ad-7f81-47d2-8d85-ee120be28cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 54
        },
        {
            "id": "91bd7917-f2ce-4f6c-b9b6-e8a85d15ccbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 55
        },
        {
            "id": "92edede2-c030-4d34-b31c-f664e0169216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 56
        },
        {
            "id": "9c33282e-6e6f-4c84-b54b-407f723269e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 57
        },
        {
            "id": "9f8d9b11-6260-4854-8700-ed0545d5cf78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 58
        },
        {
            "id": "e4d4d47c-7a59-40ef-8fee-56d13745f487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 59
        },
        {
            "id": "57cbe562-36e4-4456-ab88-208c4dd33046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 63
        },
        {
            "id": "b18149f7-7979-4dd9-bdec-22a9cb6e5f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 64
        },
        {
            "id": "753772ed-4aeb-4fca-b78a-e6c7eb524737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 65
        },
        {
            "id": "8ca82daa-4867-4615-a685-4a865acd7c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 66
        },
        {
            "id": "fa29861a-eeeb-49a5-ad55-0645741da081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 67
        },
        {
            "id": "0d964164-0ba5-4254-abf8-371c816bee06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 68
        },
        {
            "id": "fa6d0738-a061-4198-b6f3-7339f92dffae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 69
        },
        {
            "id": "507e31bf-b622-495b-8826-a2ef6a6f23e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 70
        },
        {
            "id": "5294c217-fcb0-4cc8-83d3-6464db64ae15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 71
        },
        {
            "id": "5eaea034-c8bb-44d5-b806-2a38e07b3eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 72
        },
        {
            "id": "bbadf5cb-982d-4bea-957c-49f0f2b3aac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 73
        },
        {
            "id": "c0cd2341-0205-4eea-b922-81730b63ef56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 74
        },
        {
            "id": "68def9d6-9a95-4098-bf4e-5eb6826c01fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 75
        },
        {
            "id": "ad835084-ed60-4227-a9ab-3046d0a36338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 76
        },
        {
            "id": "fffa6cd6-83a0-4972-b8dc-8cc3d11ce3b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 77
        },
        {
            "id": "530ce44b-6ff8-41c5-afb2-a21794f6bfd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 78
        },
        {
            "id": "413a03e8-f2ad-43f8-96d0-c8165526e8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 79
        },
        {
            "id": "5c9a96cb-1ad9-445b-a55d-ec0dd57753ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 80
        },
        {
            "id": "9fb9e2fe-a3f9-4597-b2fd-c71bdbebc9c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 81
        },
        {
            "id": "b6df4ced-6154-4acc-a4e4-0b76d87ee29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 82
        },
        {
            "id": "192299c7-3e1b-44fa-a456-ed9506feefc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 83
        },
        {
            "id": "cff6bcf3-80d6-4358-b79d-c9e0cafc20fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "e3a45a9c-e266-475a-958a-6523c13f4d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 85
        },
        {
            "id": "35d2fd3a-587c-4441-92e5-3a94fb5f679a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "b701f7ee-b369-4d90-9889-84f54d115bc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 87
        },
        {
            "id": "91e38ddd-0cb1-4c1a-9155-61eec826d9f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "858aed8a-2056-4640-a5e6-5777cc8e0447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "1b3b5521-cbfd-46b0-8b3f-5b4d3cae7eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "10de361e-5493-4517-a503-843c2d2f1f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 92
        },
        {
            "id": "51112fad-1d9e-4fc0-9fd4-3cfbfe62839e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 93
        },
        {
            "id": "74a5b5ac-9fae-47c9-a523-0274e947910f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 98
        },
        {
            "id": "4032d373-aba5-45a7-9470-52a9b8f9ba23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 100
        },
        {
            "id": "27fa6e1b-4896-474c-ae7c-8f4826bb08ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 102
        },
        {
            "id": "06da9aa1-b126-439b-9996-2289d74326ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 103
        },
        {
            "id": "8e3f3045-fe49-4f4e-88c9-a2702ec49713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 104
        },
        {
            "id": "fc217885-fc3b-4e20-9519-916179c0bac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 105
        },
        {
            "id": "d16d7656-71d7-46ed-b311-ef4088437e58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 106
        },
        {
            "id": "e7b65311-765c-4945-8c0f-0c6d9c0bb43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 107
        },
        {
            "id": "4de5d07d-40f9-4e88-8438-a7ef4f65bce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 108
        },
        {
            "id": "797c5267-6eee-43d7-8210-fb1b3dd800c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 113
        },
        {
            "id": "7f19c82a-9fe6-4c42-9797-3af8b05bdb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 115
        },
        {
            "id": "1f4e1bc2-e76d-4c29-af3f-3f2c98a2517e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 116
        },
        {
            "id": "3793cc5c-a52e-4369-b04d-1adc291ed527",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 118
        },
        {
            "id": "af9a14be-5a1d-4334-a6f4-9c0aaab82e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 119
        },
        {
            "id": "81f2be3a-46ae-4219-b2b6-ea164eba4315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 120
        },
        {
            "id": "6c855ff5-849c-4e77-8fa9-8b3d2504a209",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 121
        },
        {
            "id": "fe23f82a-97be-44bc-8497-8448cd7f573b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 122
        },
        {
            "id": "010451cc-7d71-422e-9f54-86aacc5da751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 894
        },
        {
            "id": "b19f3fe0-7132-4dce-a09a-e564072e833c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 33
        },
        {
            "id": "c6abfa47-7149-40b1-858d-52345b0c8f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 34
        },
        {
            "id": "a9dbedc4-c4bc-477f-a233-7aed99f9e2ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 35
        },
        {
            "id": "6c955f9f-d3a3-4650-b96d-5a5b161029d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 39
        },
        {
            "id": "d8848144-3c8d-4553-8b0f-386b8ca3ea22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 40
        },
        {
            "id": "c6699f72-be40-4b87-bbaa-b4f4d30714a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 41
        },
        {
            "id": "2d85f738-59d9-4414-ba79-a13b9ff6af68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "2b849e51-cd2f-4904-a673-005d3ce34f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 47
        },
        {
            "id": "ef732869-3680-431d-8831-d292f1062ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 49
        },
        {
            "id": "6b9cf443-eb2c-4e8b-af2b-14ca3bc12dfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 50
        },
        {
            "id": "e2431ebe-5f81-48db-8bc8-8fc45935078a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 51
        },
        {
            "id": "cdc7a6ba-ef01-400d-abb6-2a82502d91e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 52
        },
        {
            "id": "c45dff0b-6bee-457d-bd76-f59438b7bac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 53
        },
        {
            "id": "1c16b7d1-6aa9-4243-b9df-982a41a5cf86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 55
        },
        {
            "id": "5c306428-72de-4ffe-9829-0e22cb404511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 56
        },
        {
            "id": "643cd301-c484-4ee3-9008-c19aeefb8ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 57
        },
        {
            "id": "147caaba-42f8-4d88-8615-6e114c687fbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 58
        },
        {
            "id": "6707dc80-aa15-4aa2-b482-d33044c35608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 59
        },
        {
            "id": "1a3f1ac2-f7e7-4557-9089-1876c0d8f7b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 63
        },
        {
            "id": "9e575015-d625-4f76-a2b1-07aaf9ffc0e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 64
        },
        {
            "id": "659f1cb2-2d73-4fcd-97df-3663df0d4484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "1c251958-e529-4164-bb9f-f9ed8e5f8ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 83
        },
        {
            "id": "05782c2a-b27f-4a05-addb-fce8f7efc592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 84
        },
        {
            "id": "1fcd0024-6947-47d3-8fe5-9e7cd60d718d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 86
        },
        {
            "id": "3ca01c4c-0369-4486-999b-75c06932a32d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 87
        },
        {
            "id": "169052bb-98d4-4f35-b473-f3beece59f25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 88
        },
        {
            "id": "58d8e0cd-8ba1-4c06-9a56-2321301d030c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 89
        },
        {
            "id": "a89d7062-e821-460c-b88c-b2a3f2c695c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 90
        },
        {
            "id": "ccbbffc5-d241-44d5-a67d-431a7814ca53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 92
        },
        {
            "id": "dc91767c-a864-4f2a-95c9-37727326b382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 93
        },
        {
            "id": "2148ad9e-3c73-48e0-a5fb-9d90c821e9d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 97
        },
        {
            "id": "58e29ac0-1e11-40e1-a29f-7172064185ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 99
        },
        {
            "id": "43ca0a00-c0f0-411b-9e58-d29f7171e09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 100
        },
        {
            "id": "b08051b7-c136-4616-be28-997c1f2e2bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 103
        },
        {
            "id": "e2097dee-b86b-49f7-b3ee-55097fdbf473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 105
        },
        {
            "id": "53824e7b-05f3-4f52-8d36-befc7384a3a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 106
        },
        {
            "id": "48ebc586-ce61-41c7-bb64-80f40681480c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 111
        },
        {
            "id": "6829006f-7319-41fc-9e4a-6408dd7a5216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 113
        },
        {
            "id": "330c10f2-b294-4eaa-813b-dd5a19d3c0a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 114
        },
        {
            "id": "b7bdceab-7b69-4200-979c-7f71ae0c0a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 115
        },
        {
            "id": "9823a2b7-009e-48c4-8e48-e8b5b109e843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 117
        },
        {
            "id": "16440d50-62db-40f1-99ec-7277ca84b126",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 118
        },
        {
            "id": "9bcd9339-7c0d-4aa6-93b6-5678d137359d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 119
        },
        {
            "id": "3da50829-28b5-4d9c-8e84-3377542e4869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 120
        },
        {
            "id": "fc3a9689-165c-4f92-9691-3428e82187fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 121
        },
        {
            "id": "b2fc28e8-bca8-4905-84f1-03bafe73355d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 122
        },
        {
            "id": "bd7f46cd-6f3b-4d22-8ece-50be373c08cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 173
        },
        {
            "id": "878d4a18-c938-46d8-b3f3-c206c47d073c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 894
        },
        {
            "id": "6aa6a24f-2c4f-40f1-9a9a-8b0e9c8723d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 8208
        },
        {
            "id": "48bc40fa-aefc-44ee-94bd-d9ef3db2bc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 33
        },
        {
            "id": "00e43235-c801-4156-a658-c0e8267dcd56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 35
        },
        {
            "id": "19164124-47d1-4b93-a828-34bfc19194db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 40
        },
        {
            "id": "d872fee2-20a3-48ca-8575-3219421d2af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 41
        },
        {
            "id": "c44688c8-5ca4-4aa7-a1f9-3cb667e9de0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "15b3b400-f0db-413b-9d41-625d75b3e30b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 45
        },
        {
            "id": "8a4ba136-2d56-4a3d-a351-cceb28af54ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "4b145d74-ac3f-42a3-bcc8-1313d73f8cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 47
        },
        {
            "id": "355114e5-ac24-472e-9621-5c326dc98f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 49
        },
        {
            "id": "2ccd0d76-1dbc-4e7b-b976-8b6cb732532c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 50
        },
        {
            "id": "e12452cd-2b99-4535-8f55-69a261bae3af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "f267c74b-e3d0-4034-aa22-875bc75b15c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "f942b7bd-8071-428f-a1c6-f00b2bc7af31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 53
        },
        {
            "id": "a98db029-ccbc-44b0-8fe0-5e6fbed51220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 55
        },
        {
            "id": "b06138ad-b148-4427-b7a8-ebf81172fa90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 56
        },
        {
            "id": "dc4defbe-a32d-40ca-a41b-036b59a20679",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 57
        },
        {
            "id": "dc555939-aab2-4fbe-b88c-2e9bfba51f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 58
        },
        {
            "id": "30fb1c64-8507-4ff5-a48d-ba9878d40c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 59
        },
        {
            "id": "14503f85-0354-4f0d-ba9d-6082d13f8735",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 63
        },
        {
            "id": "03bffc1e-6994-4ffb-8970-c82137d0ed1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 64
        },
        {
            "id": "8958f1a4-9689-4d36-b935-91ab68df8d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "620f86f5-3c4b-4683-8ff4-52d94ccdc394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 83
        },
        {
            "id": "dbd5d198-2cd9-4fd9-b9d8-9d0e747438d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 84
        },
        {
            "id": "6e621e2c-b70b-49d2-b6ef-a589060ed05f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 86
        },
        {
            "id": "7d827f6a-22f0-426f-ac2f-c83ba9646125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 87
        },
        {
            "id": "6ce69576-dce5-445d-8158-5f6bb8fdc1ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 88
        },
        {
            "id": "265c3e0c-183e-40c5-91b5-280b23527a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 89
        },
        {
            "id": "1a2321bb-794d-4c9a-932a-d5c92880a9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 90
        },
        {
            "id": "6860896a-47f6-4bbd-b407-90081b698498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 92
        },
        {
            "id": "ae96ed5b-284e-4248-b582-11edf76d3f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 93
        },
        {
            "id": "0422784e-e942-4696-9480-ad7f7af19c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "11943d12-3672-4b67-8174-3076fca13e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 99
        },
        {
            "id": "486a1c9a-cd62-4827-86df-2b666f383ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 100
        },
        {
            "id": "88051ba5-9239-4658-bd8c-1ab0af3cdd70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 101
        },
        {
            "id": "28eb8439-6a42-4371-93ab-eff9d4f4647b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 103
        },
        {
            "id": "78b67fa5-4511-4839-808f-73e73ca70edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 106
        },
        {
            "id": "aef8dc5c-a37c-487b-a3f2-e9b5c529a2c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 109
        },
        {
            "id": "778e1b2e-78f0-46e8-bedc-7b1e8873bda3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 110
        },
        {
            "id": "ca7ca488-e925-4d51-b741-59e4094e748c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 111
        },
        {
            "id": "6d8c86d3-36d5-42a5-9af5-373361ee8134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 112
        },
        {
            "id": "2d368bdc-bb5a-4058-9be4-1df0cde66ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 113
        },
        {
            "id": "8e24927c-aa6a-4972-9e53-64d3311d1671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "8849e919-886e-4462-a1c2-36dd45207fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 115
        },
        {
            "id": "5dfda45d-8bb9-48cc-9811-978f87fa94d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "f17ca2a4-c956-491b-b617-c97ad534e328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 118
        },
        {
            "id": "4bf5f915-4090-4d9a-bc91-c12efdfa57ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 119
        },
        {
            "id": "0369a603-86b5-4586-905f-006719c468ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 120
        },
        {
            "id": "3738c34c-c32b-4f58-8733-804540f91a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "ff89058e-bb2a-455b-8668-427748a33138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 122
        },
        {
            "id": "396bf60c-2bd8-4354-b4b3-5977f813d882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 173
        },
        {
            "id": "3a13b648-7997-4b9c-9c98-c10c8d98d04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 894
        },
        {
            "id": "20c1ba5c-a3e4-4d25-9243-5e24b5d5af68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8208
        },
        {
            "id": "0a0cde40-0b38-467a-8a9d-816f422132ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 33
        },
        {
            "id": "20a8979a-32c0-42b3-9f59-a55a5550c922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 34
        },
        {
            "id": "094049c2-2b8d-439c-84ab-9e00e46c2391",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 35
        },
        {
            "id": "829b5be4-508c-4311-8b3c-093d3d2bffa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 39
        },
        {
            "id": "7fc4c57a-a180-4e56-9b3e-23db0cd7a426",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 40
        },
        {
            "id": "b907d189-4de7-4534-8dd0-5c9ba941ca01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 41
        },
        {
            "id": "d95b033d-ea34-4118-b0c4-d92865073859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 47
        },
        {
            "id": "157ca055-0f81-46c4-b328-f1755309c125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 48
        },
        {
            "id": "34e066a6-d306-4bb2-a96a-b7779bf74ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "f0790b9c-d03a-488a-ba9a-39d61f0930b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 50
        },
        {
            "id": "ba269f32-16a2-426f-906a-a77f313cdecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 51
        },
        {
            "id": "15968e73-b90e-420c-a695-54da4d1bebc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 52
        },
        {
            "id": "362bd137-e5e8-4ca4-acbf-dd1f575e2016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 53
        },
        {
            "id": "aea6f80a-6924-4c00-b16f-81bcb407cd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 54
        },
        {
            "id": "8bad9ee5-1283-4e2b-8a2a-e419a0745224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 55
        },
        {
            "id": "64f74274-46b3-4732-bcd5-b0cd2c85877b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 56
        },
        {
            "id": "aa7366bf-f0e0-4ec7-9f20-218db242dbae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 57
        },
        {
            "id": "40025409-19c7-413b-bedc-5e62fd640dc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 58
        },
        {
            "id": "fa1db1de-8fc7-42a9-9b84-1ae1c89b1e75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 59
        },
        {
            "id": "af48c12d-e71b-40ea-8484-e1e796dcc085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 63
        },
        {
            "id": "b45bbf48-1a74-4005-a5ec-dde2be1b19ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 64
        },
        {
            "id": "45f54e27-94eb-4f46-9bbd-d157ee7b7547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 65
        },
        {
            "id": "f4eea3a9-30e9-482b-aadb-8b69b15220e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 66
        },
        {
            "id": "2f6e50c3-924c-41ee-8aac-36163b19a758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 67
        },
        {
            "id": "b5cfbe08-7cc9-4a2c-9e42-f9463c957c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 68
        },
        {
            "id": "2b8d20fb-199a-4aed-a5b2-c8e7e83fbdca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 69
        },
        {
            "id": "327615fc-f30e-43d8-b310-e872615081cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 70
        },
        {
            "id": "5c901b53-2d73-4e51-b100-c025d5bed1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 71
        },
        {
            "id": "fc4f06d0-2c0a-436d-8024-b5a7f055cbad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 72
        },
        {
            "id": "192e0a9d-5b00-4c4f-af78-1d46b367a79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 73
        },
        {
            "id": "f323e45c-e2dd-4042-b506-620a59f3fe3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "358aa224-9ad3-4a42-839b-c70ea972fdfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 75
        },
        {
            "id": "9380610e-4286-4c4d-9c85-4be1d3804c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 76
        },
        {
            "id": "24e5bd13-ecd8-427f-85ec-31dfab7f3f9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 77
        },
        {
            "id": "3f2d685b-6144-4188-96e8-baa603916005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 78
        },
        {
            "id": "24ed8e23-afc2-4c78-b54c-3d2e37f39df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 79
        },
        {
            "id": "1c0d4fde-fd0d-454e-92a8-359cb8bcdd21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 80
        },
        {
            "id": "6479e196-46e0-4f6b-9a67-9bd142188fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 81
        },
        {
            "id": "28466221-776d-4fe7-88da-d9874e8553e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 82
        },
        {
            "id": "6f023c7a-7b18-44fe-9395-d898e87487d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 83
        },
        {
            "id": "30548c4a-e488-412d-bb71-96afa957bfb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 84
        },
        {
            "id": "b30f44d0-cbd2-43e6-9da2-c693c4e1d589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 85
        },
        {
            "id": "7e63ee01-e833-4e6f-9c7a-b3cb231e2894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "d7b218e4-9db4-4787-8ba0-98dfc091e0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "80de0b5d-16b0-4941-842e-9fed5b9a42be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 88
        },
        {
            "id": "584a0442-755d-4a97-9518-cc8818e5bcb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "d2886db5-0cec-4f3c-942b-5c67aa4f6981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 90
        },
        {
            "id": "9dc199ca-7f34-4e1f-995e-b763ac3168ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 92
        },
        {
            "id": "f14d73a5-7120-48ce-9a9c-05d8b1737c2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 93
        },
        {
            "id": "5c4438dc-7cae-498e-ac7f-aab66992ae8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 97
        },
        {
            "id": "4e198001-2e6c-41f3-9c23-9e8c4784a8bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 98
        },
        {
            "id": "743f6ba4-4fc5-4429-8347-d42f9ad08882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 99
        },
        {
            "id": "2813eccf-5ae6-4873-941e-affa8e83b021",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 100
        },
        {
            "id": "bbb1b9b7-9ffc-432a-b554-88a009ba68e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 101
        },
        {
            "id": "52d06f9b-8167-4b92-b6a3-e0d425c32e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 102
        },
        {
            "id": "986a2dfd-2009-4100-890b-7480c6b67eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 103
        },
        {
            "id": "c46c687a-48fc-44fb-87d7-af02a0f8c8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 104
        },
        {
            "id": "9ca4c9b5-ca04-4018-98e1-e8b331049091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 105
        },
        {
            "id": "68438838-7ab2-43c2-85dd-f2dee1a36554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 106
        },
        {
            "id": "fad6d800-57a1-46c6-b758-a39c8f81d8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 107
        },
        {
            "id": "a6cb2d24-7a4c-4041-9de2-aa980d855cec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 108
        },
        {
            "id": "5a472ee6-ae61-471a-b567-5ed12baba1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 109
        },
        {
            "id": "3b46b7a2-ff29-489f-a228-24935bc08530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 110
        },
        {
            "id": "2a801eca-63c1-4c0f-abb8-6df1de9fba79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 111
        },
        {
            "id": "a06aafa3-b35e-4307-8c93-62dcdcb8573d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 112
        },
        {
            "id": "4964f2c5-fdd0-4d19-b7df-b10c00fe1310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 113
        },
        {
            "id": "6b149b39-ba26-4b10-8a92-6383c90f4522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 114
        },
        {
            "id": "ca69bdb8-6cc1-4a19-9222-662ffae20154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 115
        },
        {
            "id": "10bf66c9-2e93-4a01-b531-3e4675f7bc2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 116
        },
        {
            "id": "aba82436-40be-42c3-be3a-ebf8cb156091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 117
        },
        {
            "id": "dfb3ffc1-f5cb-4f76-8d71-4a11463bcfb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 118
        },
        {
            "id": "506bcdd0-1850-46c0-82ca-74b5c99291bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 119
        },
        {
            "id": "4a28c389-6dce-48ec-a82e-d705c052d29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 120
        },
        {
            "id": "8014b37b-939c-4b30-a0cc-152080e955c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 121
        },
        {
            "id": "7515be6f-baf5-42f4-807a-0b85f677dbd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 122
        },
        {
            "id": "aae5d610-dac6-4dd7-a1f8-6104a2e16603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 894
        },
        {
            "id": "49f43c33-f179-4cf1-8eac-1561630dd56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 33
        },
        {
            "id": "39944e66-3d0a-4474-8475-f7f45013f8f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 34
        },
        {
            "id": "afc384c8-a09c-4e9a-b455-c405e259ff0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 35
        },
        {
            "id": "5a7b4270-041f-4393-8fb4-83dc4030a0aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 39
        },
        {
            "id": "f383591d-786e-4f19-8bf1-5552db5db4c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 40
        },
        {
            "id": "6e2f9dda-51b1-4181-a79e-4c3af53e04a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 41
        },
        {
            "id": "a27de051-7262-4e5f-a3a9-d4a0ec18831e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 47
        },
        {
            "id": "489a0f3b-c2b8-472a-999f-b0d98f893c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 49
        },
        {
            "id": "7f75c1f0-e37d-478f-bfd7-1522a10c4458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 50
        },
        {
            "id": "e0aba806-db15-4137-a181-8047602b6a47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "9cd1f80d-788e-4c92-b6a5-a03db1b7f9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 52
        },
        {
            "id": "d40dc663-093d-4db2-8428-33d3229897b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 53
        },
        {
            "id": "890dbca6-e5f4-439e-bdb2-1603d0d4cb6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 55
        },
        {
            "id": "21428252-64bc-4d46-a1fc-4d87a6e4871b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 56
        },
        {
            "id": "df361d86-944b-4e34-a2c1-f16ee6eeff67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 57
        },
        {
            "id": "76de7045-378e-4fd6-bb7f-f40d8bd6879c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 58
        },
        {
            "id": "e1c17aeb-389b-4a79-8a0e-07c49e73242d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 59
        },
        {
            "id": "59026443-fcca-4980-8720-a316d6955e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 63
        },
        {
            "id": "acbf44e0-dbf0-48cc-bf7a-269a7cd1933c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 64
        },
        {
            "id": "4164e746-8424-41fa-aa37-1df2b44ca116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 74
        },
        {
            "id": "eb939931-4135-45fd-84ad-36e87b1ef009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 83
        },
        {
            "id": "6d1d3ff7-2fa9-45cb-938f-d09d4b5950a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 84
        },
        {
            "id": "82771ed7-c8c3-4e2e-9e17-e0b816e5669b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 86
        },
        {
            "id": "e39b392b-99ac-425c-8302-ac862b548ce4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 87
        },
        {
            "id": "7fc619f9-f661-4358-832a-e33890f40694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 88
        },
        {
            "id": "ba354205-0692-4c64-a662-79f9cd2493cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 89
        },
        {
            "id": "ab4e4a50-faeb-4852-a210-7e2cb4762d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 90
        },
        {
            "id": "dbb89571-0502-41dc-8e49-0ba0caec3704",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 92
        },
        {
            "id": "bdbaffbf-3152-4106-91f2-ecc0f9a175a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 93
        },
        {
            "id": "d0509c6b-4e2e-4c97-92b4-e2cc91f3815a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 100
        },
        {
            "id": "a2e8706d-a9a6-46ee-931e-6d3064b1d786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 103
        },
        {
            "id": "43c21620-d02f-484d-9f05-f89785e0aecd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 105
        },
        {
            "id": "648d77af-5759-447a-bdf4-0c4cac68b975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 106
        },
        {
            "id": "3efa2817-90aa-413d-9ea5-1a7648ddb494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 113
        },
        {
            "id": "e318d432-1c79-4a38-bdab-70cd509e8535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 115
        },
        {
            "id": "b7a8106d-2588-48b6-9606-ebde381b6034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 118
        },
        {
            "id": "c2f86fd7-2902-4979-9ef7-0385dacdc502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 119
        },
        {
            "id": "6c85c3ae-2dd5-4c31-98f1-c37edf28fd9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 120
        },
        {
            "id": "20e2edb4-9fcf-42bb-b187-5d30e4df2b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 121
        },
        {
            "id": "e5062660-8135-4def-83c2-c71caa3103bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 122
        },
        {
            "id": "6f89518e-60fa-4ad4-b0f7-da70aa652e0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 894
        },
        {
            "id": "b67d95b4-0357-444e-a74d-2500f3f1bbd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 33
        },
        {
            "id": "e808976c-e182-49f4-af7e-4f2d3e22369e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 34
        },
        {
            "id": "2582976d-e653-4532-b7c0-fde36665bf41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 35
        },
        {
            "id": "bcf8f578-4a63-4832-a98f-80ecb6edd696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 39
        },
        {
            "id": "8d43dfa7-c129-4ff7-9b2b-8123db80c2cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 40
        },
        {
            "id": "6adbcec5-cf01-42fc-94c8-ff6608f1515b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 41
        },
        {
            "id": "e1d6e928-e7f1-4ed7-838f-93f28059c2a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 47
        },
        {
            "id": "1134d2f4-0428-4704-82c1-1b206744aec3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 49
        },
        {
            "id": "e8cce2b4-5d9b-47fe-b6b3-564212e82b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 50
        },
        {
            "id": "8103f7a9-edfb-4457-b8c7-4d9a2eba5500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "91d2b6cc-d7f2-4b28-9d77-6c8c736e49bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "77fb1f2a-a23b-44c1-aace-a21e68d261c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 53
        },
        {
            "id": "2ae59bd0-e97b-4835-9b92-8a8b8fbf6c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 55
        },
        {
            "id": "b1342532-8397-4555-9e9f-48b8888baec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 56
        },
        {
            "id": "e7a2fbdb-5ef6-4a71-a483-4504f3bf2943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 57
        },
        {
            "id": "d9c96071-e00d-4b96-9fdf-dde1012b9594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 58
        },
        {
            "id": "230dd927-6367-45b4-bc58-4091e2846cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 59
        },
        {
            "id": "f45196ec-099f-4fc8-88cc-c9ba2e559294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 63
        },
        {
            "id": "ca70a537-778d-4490-8e57-bc00ad21da86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 64
        },
        {
            "id": "8af80116-0529-4120-91b2-11dd15dae944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 74
        },
        {
            "id": "42b4493d-c29e-4e80-9e45-cc27b5ccf4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 83
        },
        {
            "id": "7217c6ad-75b7-412c-81cf-12a1567ca554",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 84
        },
        {
            "id": "5a523bf7-07bf-47cd-85f0-0ed656c46d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 86
        },
        {
            "id": "49c5c929-8edd-4748-8d1a-d14f12c905b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 87
        },
        {
            "id": "71530941-ba8e-43b4-acf6-f5b823eec95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 88
        },
        {
            "id": "b3e203c4-2ea8-486b-abc0-c59cf5a528ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 89
        },
        {
            "id": "29f8c32e-e1f8-4e3e-a06a-5cc8a4f7f6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 90
        },
        {
            "id": "c44a926b-2706-483f-bc28-d58858c0d9ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 92
        },
        {
            "id": "d9ae9ecc-091c-4ba9-a807-b6a3ba80ac29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 93
        },
        {
            "id": "6e0a28c9-cd07-4056-8329-a8376b3884d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 100
        },
        {
            "id": "1af9bcf2-60b5-4cb0-8a4b-c71f39145305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 103
        },
        {
            "id": "e6d68fe6-062f-4833-baaa-66c318ef2be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 105
        },
        {
            "id": "7c7b6eb5-4faa-4f6c-84dc-396ef2b7052e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 106
        },
        {
            "id": "82d338ff-e8d0-468a-a755-a2f30b665d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 113
        },
        {
            "id": "398898df-4ab0-4eed-9cbe-102bc9ff6894",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 115
        },
        {
            "id": "77e0e609-9c26-4238-9bfd-f09f098208cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 118
        },
        {
            "id": "f0a14061-54f4-4ba4-951a-57b236863bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 119
        },
        {
            "id": "65e91f47-72c9-46ee-8946-61347fe11264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 120
        },
        {
            "id": "a2b5d742-c070-4f9e-9fb4-a6a10edc34ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 121
        },
        {
            "id": "83c66aa1-c96a-4302-a4a1-cbc891ad2a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 122
        },
        {
            "id": "41658fb1-c70c-4c28-b5dc-6e4526a5aabe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 894
        },
        {
            "id": "bc2c065b-799a-41e3-ae5e-1459ccb20244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 33
        },
        {
            "id": "ca2cf70a-c39f-43d2-9145-9f7981b26057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 35
        },
        {
            "id": "3fb829ba-de0c-40ed-adfb-69b3d95594d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 40
        },
        {
            "id": "fe6072c4-adb8-4144-a142-3f8449dbba44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 41
        },
        {
            "id": "e9ad54bf-2fd9-47ab-aaf4-18c5e91df148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 44
        },
        {
            "id": "1c46ea13-44ca-402a-a3d0-185a38d745f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 45
        },
        {
            "id": "5fc5b3fa-8c0a-43d5-a615-a4a371ad556b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 46
        },
        {
            "id": "d8406987-1682-4d12-85c6-52669f65368a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 47
        },
        {
            "id": "bbcc13cc-0ef5-4463-9c0f-3375cec57db9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 49
        },
        {
            "id": "c3249d25-9ccd-43ae-aa82-6676e2e19b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 50
        },
        {
            "id": "c1203be2-d95c-434b-83af-a4f59807c95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 51
        },
        {
            "id": "d94a8c57-0100-42a9-8f4d-8ce91bf8f634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "7451c01d-3b62-4ace-8c88-12073f0916b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 53
        },
        {
            "id": "4df25e7e-394d-4956-a795-1efa77a48699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 55
        },
        {
            "id": "0eefeb4a-cde8-494f-9142-fb38743ff512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 56
        },
        {
            "id": "bd70070a-8da7-4536-be4d-84f883d0dc17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 57
        },
        {
            "id": "fee593b9-74e4-4cb6-8503-8e7f4eb9ddfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 58
        },
        {
            "id": "09f1db41-3b2d-4315-b713-2b4fc8dce644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 59
        },
        {
            "id": "ef55686e-bf20-42e6-a022-8476fcdf830e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 63
        },
        {
            "id": "86d1eca4-b357-45f5-9455-9bf9b182ade6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 64
        },
        {
            "id": "7644fa64-adf7-47e4-8d8a-e83428c5eb2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "c08f48ca-3e04-4b7c-ab86-77149b1e9dee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 83
        },
        {
            "id": "ee2a0d94-d933-44fc-8e86-931ee06ee8a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 84
        },
        {
            "id": "0092c2f9-70a5-439e-973e-04ee42f2dd9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 86
        },
        {
            "id": "6e56808b-c82f-4c82-b0ff-5cae3c1d1910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 87
        },
        {
            "id": "8e253af3-5f6d-4390-9b8e-c2d0444c459e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 88
        },
        {
            "id": "3a655021-f1a0-4fbf-8aad-a5d59203a4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 89
        },
        {
            "id": "f69553cb-a371-4ec6-a56f-26e705ba0110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 90
        },
        {
            "id": "9d7f662b-ce3e-4a10-8236-7a7b9409d2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 92
        },
        {
            "id": "36f373c1-8e52-461d-91d5-4856a679abad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 93
        },
        {
            "id": "515b31cd-7e21-46b3-b72a-6ca7d3ac3a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 97
        },
        {
            "id": "d1f08dac-e7f5-49ba-85fd-6a57233f4918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 99
        },
        {
            "id": "3aa58363-b2d5-41c2-8814-f4d64035f15d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 100
        },
        {
            "id": "0513e055-70a8-4397-80c0-525bc1245e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 101
        },
        {
            "id": "a5f4ced1-a971-4fb1-b8d8-0e6849f46135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 103
        },
        {
            "id": "a5b06697-ff80-4676-91ae-90b28f3acba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 105
        },
        {
            "id": "b68512d2-3a1d-4dc2-8a9b-646a9c87f23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 106
        },
        {
            "id": "70e82386-8e82-4e6e-ad1a-2c853846c71b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 109
        },
        {
            "id": "3aafc5e5-ac09-49c2-b081-cc5308d0bcc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 110
        },
        {
            "id": "a44fa731-f32f-45b7-98b8-8e1cfed9d4f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 111
        },
        {
            "id": "ccafebdb-615e-451a-8224-5792761a8465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 112
        },
        {
            "id": "45ff5675-d4e7-458d-a57f-fc28edc0c820",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 113
        },
        {
            "id": "25131669-98d4-444c-a695-c1372ff6c5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 114
        },
        {
            "id": "6abe064d-4190-4a30-86da-3cff8ef8f86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 115
        },
        {
            "id": "7d9b3690-1807-4fd2-9117-c51a20d40414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 117
        },
        {
            "id": "7f1122d9-9630-481b-84d4-b55c70122666",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 118
        },
        {
            "id": "3c24cd2b-b95c-4649-ba04-77f7ad8dbb73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 119
        },
        {
            "id": "604259d0-1132-485d-af32-1860c4504441",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 120
        },
        {
            "id": "89081f2b-3c96-4b43-868a-a89acdf09f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 121
        },
        {
            "id": "a1025ace-e1d8-41f6-b32a-593ca38ba0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 122
        },
        {
            "id": "11427131-688d-4331-b5aa-d1e83f45ec5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 173
        },
        {
            "id": "9676d0fa-a11d-4fcf-a4aa-020efcffac13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 894
        },
        {
            "id": "62ad91c5-845c-42af-ba2e-3eda3a63bf46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8208
        },
        {
            "id": "a56aeae3-baa0-4a57-a64c-92cfda00d32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 33
        },
        {
            "id": "16d874cb-fd66-4d03-9768-9473d19eb7d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 34
        },
        {
            "id": "5fe0691c-215f-4136-93fd-9454afa1de54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 35
        },
        {
            "id": "b7eaf256-8d78-4018-9556-1fad0e4dc67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "010f276c-ff24-4e97-ac80-6dfc7b60bc56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 40
        },
        {
            "id": "24e65bd2-0029-4daf-b716-04d45cb0a61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 41
        },
        {
            "id": "8f570f79-57b9-4aac-9b61-b14a06bc8bf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 47
        },
        {
            "id": "c565be15-e3da-4590-9ca3-2a9d53f0e293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "3fbb0fab-dd3c-4653-9431-85dca2bae4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 49
        },
        {
            "id": "c70820d4-e6aa-46be-97a3-49bc3be9b46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 50
        },
        {
            "id": "2225d1be-946c-45e4-b6d8-60ee63a6a259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "ffc256da-a600-46cb-9fb4-3eb4425d2b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 52
        },
        {
            "id": "f2942fc2-8371-4b8d-b95b-b37ad2507600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "4fee4d80-3300-48bd-87bf-3d11861991c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "3ded198f-d9e5-4d77-8890-f47d1b3420be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 55
        },
        {
            "id": "96ac889a-6aa4-4d8e-9004-c450629236d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 56
        },
        {
            "id": "593e7d88-6aee-4664-8f8a-18ec756a401c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "a68953a8-8dd5-4096-9c67-82a077517833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 58
        },
        {
            "id": "71643f94-e351-404e-b258-3f969204b375",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 59
        },
        {
            "id": "e79e0d4c-73f0-404f-9ae0-36bd14215ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 63
        },
        {
            "id": "40e4be1e-4680-4f2d-975a-afc2de758d9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 64
        },
        {
            "id": "fcdde60a-024a-4fd8-9f38-0567aa6af408",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 65
        },
        {
            "id": "cfb1b3fa-8169-4573-b188-25c57a1a57b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 66
        },
        {
            "id": "e3c14c3a-a017-4002-a4bf-aa85277350c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "91bd75f1-46ab-4f27-8599-57f2c9dab13e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 68
        },
        {
            "id": "10f58656-e00b-491a-8729-7dccc007170c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 69
        },
        {
            "id": "c0f76f6f-1964-4c21-b6f8-20728c5cb0ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 70
        },
        {
            "id": "dc3798e1-034e-41d5-a49a-8b9e0d7cfffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "d4798754-d14f-45c5-8900-463865ebeedc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 72
        },
        {
            "id": "8cdbb38f-679d-41fc-b0d0-6c328ec3eb41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 73
        },
        {
            "id": "fb313e3c-9c62-4512-8286-c63f83fb7b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "fb3c219b-562c-4ad6-8831-e0340e87ce94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 75
        },
        {
            "id": "5baff1d4-89d6-43fb-9fde-7e54b71b2884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 76
        },
        {
            "id": "5438a98b-263d-40cd-9903-21e97035abec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 77
        },
        {
            "id": "f5c468db-ac61-4c3a-baf0-1eacd8e431b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 78
        },
        {
            "id": "baaca9ff-45f6-4b97-b409-1e21c0b21881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "1e97f0c9-f4d0-4ae3-807f-59e95314526c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 80
        },
        {
            "id": "b7f762e4-1ab0-43aa-8401-c42c07df5cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "59231c0a-5c43-4f56-96b6-29752ce3c14a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 82
        },
        {
            "id": "aa498d49-7a2f-4eea-a10a-13f29db433ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 83
        },
        {
            "id": "64280783-18df-4fb3-b798-435a73270f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "f65fa6d0-53e2-4c4e-8090-f8c09d205a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "395cc5b6-0053-4a51-8174-4ab6625b6253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "c51a11d8-1843-4d4d-b15b-04a300965c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "57c7d84a-83c9-4b0c-afe8-4f47cad7d54e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 88
        },
        {
            "id": "6d99e846-4b96-4984-93d4-58160c1e57c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "8b9d5492-d16e-41f7-b139-14baf60d6ade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 90
        },
        {
            "id": "24bcc704-17d2-47f4-8309-24fc80869870",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 92
        },
        {
            "id": "982c2aef-1555-489f-a049-4627bdd44d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 93
        },
        {
            "id": "0af6bd3f-ac7e-47c5-84ed-a0a96c2c650e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 98
        },
        {
            "id": "40910cdb-5e0e-4de4-8a23-ffd7e89e24ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 100
        },
        {
            "id": "38a00efc-c332-43f8-b253-3f7236865cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 101
        },
        {
            "id": "32e37f81-affc-4dd1-ac97-c6227ccec0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 102
        },
        {
            "id": "338d57a5-5e46-4b26-b662-c13d251bbdae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 103
        },
        {
            "id": "57914495-1067-463c-b973-fe6f2761cfd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 104
        },
        {
            "id": "b3d5661f-cc9b-4801-bf49-dc6cd0e9e600",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 105
        },
        {
            "id": "64e7f270-38e6-4622-bdb7-82112470d37d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "6f331a6a-2f75-49bf-a912-b0e1efe117b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 107
        },
        {
            "id": "2b10b3a3-9ac6-498a-b7ec-c62588d75cb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 108
        },
        {
            "id": "88a1998f-f2da-4fa1-bbc3-4d54872a7571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 109
        },
        {
            "id": "88c0a25a-b3e8-419e-9da9-835a1714368d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 110
        },
        {
            "id": "d95dd195-f43e-4b57-bbdf-8d8708c1064c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 112
        },
        {
            "id": "c2659309-3db1-4d22-83e7-f4b862c6fcef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 113
        },
        {
            "id": "2b66fdd7-287d-4372-b9fc-bd6f15f2e1fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 115
        },
        {
            "id": "2e9a752b-a524-4a70-8be1-adba647f331b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "d60996ac-5c2f-47a7-aba3-ce98db271d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "47ded704-7e63-46e6-9301-4d6698956ef4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "41382fa8-bc29-497b-bf77-b05582de09b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 120
        },
        {
            "id": "d5e31783-ce23-46b8-aa2f-635b5eecf948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "cb48ee17-6644-4f71-aa69-28c6d73dfbe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 122
        },
        {
            "id": "15c87e5b-cd7d-46da-bf95-38474fe9de29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 894
        },
        {
            "id": "751a7b28-36af-4474-8bdf-1de8a6d97134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 33
        },
        {
            "id": "c3803dea-d791-438a-98da-6fa6eb5d5fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 34
        },
        {
            "id": "06d1fa57-b5f0-4f44-b1b8-be00b182aab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 35
        },
        {
            "id": "6f1d2438-fd35-4071-adf8-927cb14125b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 39
        },
        {
            "id": "f22b3379-9cd6-46e7-925a-88e0b213f853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 40
        },
        {
            "id": "8f8743da-c82c-4a41-a75d-da8d10574776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 41
        },
        {
            "id": "de10420c-f5c4-4ec0-b00c-e739334a7be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 45
        },
        {
            "id": "8d2947de-3263-4b90-baf0-bc42c8780235",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 47
        },
        {
            "id": "4388d6cd-c200-43c4-a420-96b62c38bdff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 48
        },
        {
            "id": "31db6539-4fff-4ed1-87b3-53916974cb7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 49
        },
        {
            "id": "1640651e-61e8-49a4-a13e-5e3dcd223ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 50
        },
        {
            "id": "552bab1d-873f-4399-b1bc-5a3f0b8bfd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 51
        },
        {
            "id": "3c908566-7576-4a9d-9f3a-69ba88d29948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 52
        },
        {
            "id": "6d196cfb-43f8-4abe-a8c2-eaa92a8e9b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 53
        },
        {
            "id": "b6f053c7-7293-4c02-b0c3-3994fab566ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 54
        },
        {
            "id": "9472b70d-0ce9-4555-96bc-bb04806b1f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 55
        },
        {
            "id": "fe7a2bdf-056e-4a64-b4c1-58e97d72c10f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 56
        },
        {
            "id": "632d7e83-7292-452a-aa68-0e28223e85a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 57
        },
        {
            "id": "afceb72a-0302-48bd-87d0-8c4534fbdf8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 58
        },
        {
            "id": "4367f2e8-f4c9-418a-b7a6-6199d79a0147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 59
        },
        {
            "id": "654c6bbd-f6f9-4381-a12d-fcf3ee355318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "966e08e3-1dfb-4062-b493-88a9d581e2b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 64
        },
        {
            "id": "c0d5ba25-bed9-4665-8b8b-04e21668737c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 65
        },
        {
            "id": "77023ddf-c2e5-4eea-93a3-cca903e5cd09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 66
        },
        {
            "id": "db35d40f-6e64-47f4-8c9d-796aed5278d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "175e13fa-040d-411f-abd6-842e530ab692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 68
        },
        {
            "id": "55333cf5-03bc-4213-b72e-a19d3810302b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 69
        },
        {
            "id": "95d9af94-ebf1-4358-a676-33f8ab91c403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 70
        },
        {
            "id": "01844dd7-721b-4fb2-a5de-9934fad6b1f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "010a2235-cf07-4216-a00d-b12f5e473ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 72
        },
        {
            "id": "b067aab7-54e0-439c-a7ff-ccd8c23e5269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 73
        },
        {
            "id": "a8488582-6992-4d5b-b958-58598c1a2352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 74
        },
        {
            "id": "7a2407be-9610-4528-8626-25e7fec3a28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 75
        },
        {
            "id": "cf976f17-64b7-4d5d-ac79-9a3498b162c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 76
        },
        {
            "id": "6a3fbff1-3e2c-4b67-bd04-7d73c7630151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 77
        },
        {
            "id": "f6b24db3-01c4-4a73-8108-be900726acb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 78
        },
        {
            "id": "889e4862-0a22-47be-906a-d37657cc144d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "47dd6911-fb68-40ff-8cac-6644df9c5638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 80
        },
        {
            "id": "0cc7130d-fe20-4b44-9849-80fb7169fe8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "cf8b22a3-a8c0-43f6-a9f7-b7d347bdd239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 82
        },
        {
            "id": "684262fc-420f-4224-a393-fe9ffbbb99e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 83
        },
        {
            "id": "d57f729a-0d5f-46e4-be7b-0061385bdbc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "0220da4e-8ed9-4d3f-84cd-e27c94eb59d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "4e4dd26c-3623-4d4a-8b5b-ddaea0f0e689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "466fb598-5f34-4657-a9b4-e6c2e20b2063",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "a6fdd3b8-9e98-48f5-ac60-86533341ef06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 88
        },
        {
            "id": "e0ad787a-dc1e-46af-bf22-51beda1d7c41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "cbe2c406-9197-4211-bba5-1a7087120818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 90
        },
        {
            "id": "98339655-5ee1-4dbd-ac68-8a5089672b7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 92
        },
        {
            "id": "ce055ced-3f79-4993-a353-29f233535ba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 93
        },
        {
            "id": "7ae7490b-634d-4a21-849b-dbf4fef747ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 98
        },
        {
            "id": "325a6486-cbd8-419e-ad71-9ea591f255ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 100
        },
        {
            "id": "ace72be4-9302-4f1f-bf0f-48f6c036fde8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 102
        },
        {
            "id": "9a9cb484-b3a4-4f33-97a8-472f844b0c07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 103
        },
        {
            "id": "cb77d0d1-1abd-414b-9905-67964b2ad47c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 104
        },
        {
            "id": "995764b7-b9d5-4e34-b471-0fe856055ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 105
        },
        {
            "id": "0ff44a13-e3d4-42d5-b268-9e3259adbbdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 106
        },
        {
            "id": "d8b57917-39de-46a8-ae2c-97384e40d90b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 107
        },
        {
            "id": "90aa4134-f787-45e3-8a40-5d71c6b20f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 108
        },
        {
            "id": "010dffcd-82fa-479c-833b-5fe60458c89a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 113
        },
        {
            "id": "934735bf-9b9c-45ba-903f-d3be0e3cb080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 115
        },
        {
            "id": "c462b666-7016-4a5b-8a87-3819414118b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 116
        },
        {
            "id": "cfd70b49-8da4-42cd-b83a-15f85bc9606e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "8c4f88c5-a56e-4364-9a80-4f7ed9c538b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "058c77e2-39d3-4e23-97d7-c44f991901d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 120
        },
        {
            "id": "39f9d236-9b06-4e78-8c0a-673f3c7be511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "a5ab7851-c070-4f8d-b0e4-cb90db08f374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 122
        },
        {
            "id": "d414c914-e7d4-4a5d-a919-47608e194052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 173
        },
        {
            "id": "da64dd5a-7517-4275-bb38-547e16de3e62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 894
        },
        {
            "id": "b20d8ae8-b91f-4546-8527-2444f82d1ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8208
        },
        {
            "id": "1aecdf94-e212-4061-b884-a25bd1520760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 33
        },
        {
            "id": "2c5795c7-65c6-44ec-9f21-c8a8bffe4c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 34
        },
        {
            "id": "688b524b-fa1b-497f-9265-66669ca90144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 35
        },
        {
            "id": "afd440aa-9f82-437e-ab1e-4431088e4973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 39
        },
        {
            "id": "1b18600d-386e-40df-87ff-d785cd50cc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 40
        },
        {
            "id": "67836bad-1268-4265-bf89-1ac075545945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 41
        },
        {
            "id": "619876ab-fec9-4fe2-af4a-bf11721617c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 47
        },
        {
            "id": "f366d4dd-c3ac-45f1-a1d3-9aca8391e9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 48
        },
        {
            "id": "fc7697d2-0056-45ab-bbd8-d7be71987323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 49
        },
        {
            "id": "20c56afd-ce89-4672-aa2a-2234a1163bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 50
        },
        {
            "id": "b8c43565-3834-42d6-ae44-3a3011407e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "614a42f4-647e-4759-a425-351ae1f0dbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "6920707b-819d-4cf5-ac93-035829dfafc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 53
        },
        {
            "id": "6c896d70-acb3-4ee7-85a3-4cd2b0083f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 54
        },
        {
            "id": "8d111f8a-5c67-4674-a0f6-88c5025d77db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 55
        },
        {
            "id": "94492177-29a4-46f2-98f1-817f9fe76523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 56
        },
        {
            "id": "08e6ebd8-033b-44ec-b825-0a7fd363cfba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "f27b5844-4553-485d-9508-cae0303dbb65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 58
        },
        {
            "id": "b803a639-ab72-4b72-8da5-d57861f9e384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 59
        },
        {
            "id": "569841d6-542d-4e5f-a47d-f286397f10ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 63
        },
        {
            "id": "425c4c19-f97d-4e56-99ba-25b2ff6b5992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 64
        },
        {
            "id": "9b662873-eb24-499a-917e-798118648dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 65
        },
        {
            "id": "1f6ae9e8-9790-4136-92c5-be55175e1c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 66
        },
        {
            "id": "19db0454-874c-48d3-bde1-7d5c88a36c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 67
        },
        {
            "id": "0deefa76-d61b-43ff-a63d-6768c0b14abc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 68
        },
        {
            "id": "68b96684-3a7a-4058-b4e1-1f5e668779f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 69
        },
        {
            "id": "72514a52-a743-41b4-b0b9-a9af644d4e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 70
        },
        {
            "id": "9eacb9b9-829c-4803-8579-9ad2585536bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 71
        },
        {
            "id": "502d0bb4-ceb6-41c3-8fb0-b0c8dcbb9321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 72
        },
        {
            "id": "a80ea4c5-e7a8-4684-bc6f-7163bff1345e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 73
        },
        {
            "id": "26cf3a38-3ac9-4f69-8c74-e869a36e6c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 74
        },
        {
            "id": "10b1f4ac-795a-4910-93cc-61cae275abd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 75
        },
        {
            "id": "6142ffd8-8fbf-4caf-86cd-3bb41b33d0c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 76
        },
        {
            "id": "4ea2e73a-0a69-4f28-a3f4-4495f4ba4d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 77
        },
        {
            "id": "f355fc33-bafa-4a80-815d-9ba33a742fef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 78
        },
        {
            "id": "74051f21-39f9-463c-8897-06f108249faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 79
        },
        {
            "id": "713da9ca-b536-43d4-a125-ab77e9b39f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 80
        },
        {
            "id": "4b5c465e-e21c-4841-a02a-6a6c6fa69687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 81
        },
        {
            "id": "5fe4528c-f255-440e-a6de-9402fa078c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 82
        },
        {
            "id": "d1a8b260-8e0d-4331-a5aa-41539566d814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 83
        },
        {
            "id": "9d52aeb9-0440-44d6-b005-c8e2524a2a70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 84
        },
        {
            "id": "d1f78466-9765-4b5c-b6a4-2778b5486f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 85
        },
        {
            "id": "b6d445d1-98a0-485f-ace0-3fe4bd9c5203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 86
        },
        {
            "id": "33c9b734-63d0-487c-9a42-8e6892e78776",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 87
        },
        {
            "id": "35f97d6c-2e5a-4324-b6f2-7e6148f97061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 88
        },
        {
            "id": "291cf2ab-8244-4915-a69e-906a706a93bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 89
        },
        {
            "id": "b7c83ce9-ef21-4f38-9bb4-7f25ec0ca36f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 90
        },
        {
            "id": "b9e1d482-3f0f-4010-b601-275d26520d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 92
        },
        {
            "id": "0c80191d-9952-43ee-9bb3-7828eedc9a7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 93
        },
        {
            "id": "151d33af-c10f-4544-a63c-61d72a5e7580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 98
        },
        {
            "id": "f96647ab-2f2a-4d5e-b939-b0695aa4c488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 100
        },
        {
            "id": "a0d62f3c-78fd-4593-b514-ecd6c3ec6c03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 102
        },
        {
            "id": "7f5d3130-c9b5-4215-b1ae-5b361d071dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 103
        },
        {
            "id": "27622a80-2194-4853-83e2-041fcb401ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 104
        },
        {
            "id": "a325975c-3b3c-44c5-b26d-0529b27722a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 105
        },
        {
            "id": "d407739c-86ed-4f20-8f65-a8509aa108a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 106
        },
        {
            "id": "e0aab9c2-8447-42be-b5f4-f8e529240961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 107
        },
        {
            "id": "00a59a62-8ced-4335-9aa4-d5b5ce1058d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 108
        },
        {
            "id": "9b077009-4614-41ca-85d7-c352b558e0bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 113
        },
        {
            "id": "8775850f-152d-49f5-8d29-886cc0c8e38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 115
        },
        {
            "id": "d76075a8-94a8-4ae9-9f6f-d7a6227c8158",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 116
        },
        {
            "id": "b9454d5a-7a7b-41c7-b3ed-22616e3a4c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 118
        },
        {
            "id": "4b3f2f33-a9c0-4a51-9908-1ee35b60f062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 119
        },
        {
            "id": "2189f5d7-9c69-441c-8b42-a5d43dcd38a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 120
        },
        {
            "id": "c114a1e6-f899-4f10-90f7-b9a1756945f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 121
        },
        {
            "id": "a5aa7066-e1b0-48ec-bfee-da8b4d6dbda0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 122
        },
        {
            "id": "bac9d823-b63d-4334-9ca3-82e704b5d202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 894
        },
        {
            "id": "82b43ec5-0f81-4966-86d9-c7c50bda91ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 33
        },
        {
            "id": "c84935ec-4102-405e-a5e3-f05ae1298132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 34
        },
        {
            "id": "dafee59f-a712-4def-b8e1-5f43dd443700",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 35
        },
        {
            "id": "7ed68d80-8d2e-44dd-8fe0-deeae5b4a7a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 39
        },
        {
            "id": "1498f192-5c52-4934-8914-80f66a0720fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 40
        },
        {
            "id": "6f3ce831-2217-4d4b-84ce-229e40c4b25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 41
        },
        {
            "id": "b253eec3-5918-4a70-8b67-a329158f1b39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 47
        },
        {
            "id": "dfff97a2-c967-4c80-a467-41f69a344d81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 49
        },
        {
            "id": "f44a5321-c042-4682-abba-b9dd9aaeed59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 50
        },
        {
            "id": "84437438-a2b4-43ca-8110-8d312cafed07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "523c6ffb-7081-4aec-a78c-8a2a5679cf27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "15471c88-df28-499a-a45f-1f7d40b7f38b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 53
        },
        {
            "id": "bb6ed02d-9b20-4a8c-bd12-e56a6d36d4bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 55
        },
        {
            "id": "5d4700cf-5959-41c8-83bd-077afbbdf909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 56
        },
        {
            "id": "53f353f7-45b5-46fa-9a83-ffc949361169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 57
        },
        {
            "id": "87473e0b-9ea6-43be-98ec-edc94a6b3504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 58
        },
        {
            "id": "13d374f4-1c8a-4f6c-b1f3-698b01273881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 59
        },
        {
            "id": "839f7269-1e94-45ba-98e6-a4e7870e8646",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 63
        },
        {
            "id": "9238ceba-9e6d-4977-892f-c290a5884640",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 64
        },
        {
            "id": "1a163593-b215-45eb-8718-1146e38ddad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 74
        },
        {
            "id": "42fa6be9-ca8e-4afa-9914-682bac0400af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "6d5fc18c-512d-4177-8b2d-3b083a9c1b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 84
        },
        {
            "id": "970e693d-d02c-4730-95cc-bf3c5f6727ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 86
        },
        {
            "id": "35460de8-45e9-4625-8bf3-7a729f340728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 87
        },
        {
            "id": "90d5eced-1af4-412f-b58a-c24fc20b09be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 88
        },
        {
            "id": "87bfc754-c3f6-41d6-a257-844b2e8463cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 89
        },
        {
            "id": "be464879-dcb6-46fb-bcb4-baeab9a31e91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 90
        },
        {
            "id": "3f1e4af0-da34-40a7-bde6-1d7e2801af38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 92
        },
        {
            "id": "166dac73-bfe6-485f-922c-f0a1ff702122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 93
        },
        {
            "id": "9635ae13-d869-4dbd-be61-c81c07e81dcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 100
        },
        {
            "id": "b58c2787-6212-42c0-a2a8-527f2951f0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 103
        },
        {
            "id": "658a84bc-203c-4abd-8110-ccf3e2ed0ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 105
        },
        {
            "id": "6f95d10a-78ce-4d1c-bc5e-4d7a6a4c162e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 106
        },
        {
            "id": "55af2d2e-8fdd-49cd-b18c-4a82c25bec98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 113
        },
        {
            "id": "5a03aeb6-3eda-4c12-988f-7ed1bab12328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 115
        },
        {
            "id": "f124db2e-99bf-45f8-84ff-66c66bb7abdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 118
        },
        {
            "id": "2a0af080-ce29-47a2-8ab3-4c16680695fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 119
        },
        {
            "id": "5ceb132a-4636-44eb-96af-ae5051a7322b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 120
        },
        {
            "id": "af01681f-745b-48ad-a2d6-9df60a780ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 121
        },
        {
            "id": "c36fa66c-fcae-4371-99ca-702bc8fd8fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 122
        },
        {
            "id": "ca538df4-2cc2-489c-8a44-7d5a31631832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 894
        },
        {
            "id": "6ce7a368-ee03-4259-afc1-2996f50025c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 33
        },
        {
            "id": "08ed5948-c013-49e2-bfaa-a5be8a4ee741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 34
        },
        {
            "id": "09f12073-d258-4d5d-9017-83dfa6b7ea2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 35
        },
        {
            "id": "52c09b88-233d-4192-aee6-e46254c1bd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 39
        },
        {
            "id": "e75f2137-4d87-47a0-8e74-e56b6fd4ad40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 40
        },
        {
            "id": "0549bec6-1803-4d2a-9b8d-fe39d872a669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 41
        },
        {
            "id": "93bbcd2f-47da-41ac-8c16-e253848caf3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 47
        },
        {
            "id": "682b5edd-845b-4e28-99da-161e9fa49407",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 49
        },
        {
            "id": "ae7feff8-cc60-4e9c-b02c-49af9770e0c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 50
        },
        {
            "id": "0a27e22d-7185-49a4-9586-ba46bf1b85aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 51
        },
        {
            "id": "f4d22af8-18bb-4cc6-aac4-0796871fd3f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 52
        },
        {
            "id": "e8b87f30-6940-4bba-ae82-caff4a94606b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 53
        },
        {
            "id": "ff105f29-04e4-40e5-9374-08c981f36a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 55
        },
        {
            "id": "1199ef2e-d006-4063-b7b8-90389d01e4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 56
        },
        {
            "id": "718625b8-2510-4231-8540-254ab8810ea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 57
        },
        {
            "id": "1a46c959-0584-4a6e-a3ea-76e67317c505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 58
        },
        {
            "id": "1281a5d6-11ef-4bf3-9c47-9bc490d9d658",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 59
        },
        {
            "id": "bb6cde6f-fb78-4087-9ff1-c55732fad8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 63
        },
        {
            "id": "bb36dc5c-abea-4e8a-a625-909c2b984e6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 64
        },
        {
            "id": "bef98b3e-76b4-4ef7-a5ca-f93e79bb5b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 74
        },
        {
            "id": "82b3f98b-b1e1-4bb9-93fc-c74885a68e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 83
        },
        {
            "id": "6d714f1c-6631-4031-b383-b062a6b5e38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "8ea3a2b4-deb5-4b09-bf7d-e35924031d7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "2982ebb5-928a-49ee-bac9-3a99aeff03d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 87
        },
        {
            "id": "174ada49-0545-4a9a-bb76-a7c1954f754b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "c92c0a11-e4fa-485c-a73a-df84f9abde35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "bff135db-0b8c-4c06-b2f2-dbed012de77c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "393f9384-be5e-4a80-accb-1baeb2267198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 92
        },
        {
            "id": "b9841686-c78b-4fe1-8440-3fdc9f4e451c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 93
        },
        {
            "id": "848073a7-5db1-423c-a392-b778fcd48720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 100
        },
        {
            "id": "cf7ff2ff-b324-4ceb-98b1-8d7260314c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 103
        },
        {
            "id": "433a806c-340e-4911-983f-7ea23251bc50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 105
        },
        {
            "id": "687bd9d8-a242-42aa-98c3-1fb01813084f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 106
        },
        {
            "id": "46ee3d3e-7b8b-4c03-8a7b-1241e0fde035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 113
        },
        {
            "id": "ce842f28-2d5b-4d30-99b5-11c48708904d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 115
        },
        {
            "id": "9c9d749b-7406-4406-b888-f50fce0641c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 118
        },
        {
            "id": "e067098e-a0c7-4508-87e0-5114a3a34b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 119
        },
        {
            "id": "7733ffb7-c828-48cd-a150-b38ead5b190d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 120
        },
        {
            "id": "9c006b04-d277-45a1-ad8a-222ff908623e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 121
        },
        {
            "id": "d7bd3d3d-bd04-4e3b-a44d-136f9f5c1ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 122
        },
        {
            "id": "4ee996c5-f9fb-494d-b42a-09fd8d929922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 894
        },
        {
            "id": "489d7cdc-7c6e-40c6-9f1a-13814c6959a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 33
        },
        {
            "id": "3e9e1c43-3942-4e05-b14f-081694e036fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 35
        },
        {
            "id": "047952bb-2b7f-4d0a-94b1-ba4fa4f611ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 40
        },
        {
            "id": "c1e3e003-953b-467b-9c93-7eda2ebb7cb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 41
        },
        {
            "id": "b556cb0e-77ba-4c7c-bbb0-743af520bacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "138a825b-0aab-4704-9dcb-52dd05721be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "7e28500b-dcf9-4a3c-8c58-2600d8e12e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 47
        },
        {
            "id": "d46354d3-3856-436e-8faf-02797c200cdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 49
        },
        {
            "id": "7beb5d07-067f-4bea-baa6-456ceb1962a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 50
        },
        {
            "id": "bee18adc-8cd7-4257-9666-647abf5d8535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 51
        },
        {
            "id": "b3a3efce-1883-4ba4-a116-8c1a116f3020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 52
        },
        {
            "id": "849b8b76-3836-4dc0-9d17-6afa03a990e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 53
        },
        {
            "id": "6717c6ca-79b7-481d-91c5-fe1b293a925b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 55
        },
        {
            "id": "615f5c6e-066e-487a-926e-a51a1bf383e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 56
        },
        {
            "id": "8f52d5ff-da88-4fa5-8554-faf9a7a63224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 57
        },
        {
            "id": "9e5c1910-1634-4dbf-a0a5-6289a25435b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 58
        },
        {
            "id": "8213a1aa-01ab-4432-b82b-df02f818af90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 59
        },
        {
            "id": "74a4c34b-90db-4baf-b82b-4f764529475c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 63
        },
        {
            "id": "341cd62f-d9d1-4f90-b623-676626e3a8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 64
        },
        {
            "id": "fe2b7846-6059-405c-bbb0-320d924d09cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "9226d838-3cac-4475-93a8-9432589ac48d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 83
        },
        {
            "id": "458d1322-84dd-488e-8513-ecfa592d2401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 84
        },
        {
            "id": "41f2543a-e334-46cd-8b31-c22ddf4c6b12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "3d3ca0d9-ce30-43bb-b156-83ce51673a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "c3e4cec5-9967-4e3b-9537-e0e90d60d14f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "092f8032-bc06-4adc-918c-8e0c51e9a49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 89
        },
        {
            "id": "7f404da7-b22a-40ca-bf34-f58ad89fe15b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 90
        },
        {
            "id": "b6affb30-ff90-45c7-b41a-799ba2f6942b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 92
        },
        {
            "id": "caaf9003-e7bb-464f-952e-13f3362eff3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 93
        },
        {
            "id": "5d37610d-2eaa-4b06-822a-a16f8a6b8459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "8c079a6b-6be7-4392-ad0f-85a2043676fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "68b42407-7a4f-4ac4-999e-3cad778f4b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "038d8af5-d702-498c-b06f-be0724703e3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "1b783230-b3f0-4e0e-8681-d216d81139af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "5ab5d51a-1944-4e40-94f4-91cc326d25c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 105
        },
        {
            "id": "02797480-2988-48aa-9789-3f9d6e086a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 106
        },
        {
            "id": "a27c32be-46c8-49f3-a5fc-23c5b79c29f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 109
        },
        {
            "id": "04b2f121-276c-4514-99e0-b8de319f1aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 110
        },
        {
            "id": "1e14afb2-90c8-485a-9f46-130bca0ac67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "0b7e2167-bb55-47cf-b6a6-f59a59f988a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 112
        },
        {
            "id": "f49634f6-9624-40f3-afdd-a72ca9945374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "8982ccc3-9b71-476f-ab91-b68c9ade606f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "11d6fb0b-3f44-4804-babb-4188e43d4f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 115
        },
        {
            "id": "cb366178-068b-4024-afa7-bfe88e6bd7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 117
        },
        {
            "id": "662856cc-27f8-4d11-95b7-70a9fdad6af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 118
        },
        {
            "id": "5bf8132b-4bc1-4812-9f71-409e5d43fc0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 119
        },
        {
            "id": "6fa1d490-2558-4a7f-bc05-4b411fbd970c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 120
        },
        {
            "id": "0553bced-f539-41a7-9f7e-f4dbf5619d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 121
        },
        {
            "id": "105e4076-6046-40aa-87b2-6d9af969ee0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 122
        },
        {
            "id": "0a273e1e-2eb5-4a81-b58e-80f76d08e3fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 894
        },
        {
            "id": "8dbc2175-a1c7-49ed-bfe6-39cd3b3782f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 33
        },
        {
            "id": "71d950b1-2f7b-4670-98f4-5fa59a9ac61d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 34
        },
        {
            "id": "b96b6f5f-ba5a-4e46-a5bd-d8c85bbbd221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 35
        },
        {
            "id": "a4fa4c9f-a9a7-4c89-9874-71d7c2e842fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 39
        },
        {
            "id": "b079fba3-9ac0-495b-b027-1423453d3169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 40
        },
        {
            "id": "7218e670-e679-4ea2-8305-f5cdc55ee505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 41
        },
        {
            "id": "b7706f37-f117-47c2-a470-f28ab747b4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 45
        },
        {
            "id": "86ed8848-f586-4317-85e1-9a8dde7585ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 47
        },
        {
            "id": "c3c1d7fc-69d0-4202-af85-11bae9bf7b95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 48
        },
        {
            "id": "bb7db0d9-5c40-4289-a03d-43cb5f6a081e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 49
        },
        {
            "id": "9a709378-efcc-4383-8f38-f93677398447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 50
        },
        {
            "id": "37c841c7-42bb-4d98-9e13-4946bee88fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 51
        },
        {
            "id": "c9594bef-777f-499a-8825-6ff80cb247ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 52
        },
        {
            "id": "816b4832-b4b6-44b3-a926-41812bcb6bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 53
        },
        {
            "id": "86d935dc-9ea1-42b1-a0aa-dcb49cd46590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 54
        },
        {
            "id": "6535b910-3d4f-4833-b03f-6b9b7dedf113",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 55
        },
        {
            "id": "596473bc-b56e-4f5a-ae64-71ece9e0578c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 56
        },
        {
            "id": "1422e2c7-1cd4-4bb7-b3d2-038e3f1d7370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 57
        },
        {
            "id": "e8ab6f5a-3538-492a-b65b-834fea31caea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 58
        },
        {
            "id": "b4370881-1497-4dfe-9b45-2e4b37a27181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 59
        },
        {
            "id": "aa5241e2-03b7-42ad-99bc-a66da962aa2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 63
        },
        {
            "id": "1f7eba6f-da6c-4c64-9301-36345725763a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 64
        },
        {
            "id": "b81a512c-6bd8-4150-a6b7-d5b8e1407183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "13eb5c11-c57d-41d2-b0b0-d06231a11bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 66
        },
        {
            "id": "ed41d94e-07f8-4ebf-9716-f40ef876fb92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 67
        },
        {
            "id": "2383c30e-b811-4583-af34-27dd733e39de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 68
        },
        {
            "id": "a3c5c111-6d14-4abb-8db5-f2ab2914fef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 69
        },
        {
            "id": "8a9ce0ef-0322-4b1e-9fbc-783d7d63266a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 70
        },
        {
            "id": "c955a359-cdac-4a84-9161-8d98ce558430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 71
        },
        {
            "id": "f67849ce-7d57-4d29-8f88-5ddfd78d877c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 72
        },
        {
            "id": "71d718e4-992a-4402-bbc1-3e324281229d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 73
        },
        {
            "id": "b1f204e3-b6ea-4e79-be2f-bce4633afa4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 74
        },
        {
            "id": "3bf552b7-91d6-4ead-a8d8-2db17c5f77e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 75
        },
        {
            "id": "235556e4-11f8-446c-a3db-103173b6ce5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 76
        },
        {
            "id": "1b0cf1e7-bcd0-4071-9bc2-8c036039716d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 77
        },
        {
            "id": "239c2155-564a-46ea-8d65-01b0136fc242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 78
        },
        {
            "id": "9b4eca04-27cf-411d-9520-613bc4ee9963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 79
        },
        {
            "id": "9937fd46-f3a8-4bbc-b394-02eae8219cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 80
        },
        {
            "id": "398b6d7a-08f1-457c-877d-36ecea6f378e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 81
        },
        {
            "id": "19a9bd2a-e3b3-4c2e-8e5c-03420c4c48d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 82
        },
        {
            "id": "94a823ed-e30c-4705-adb4-e4ffb09a1112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 83
        },
        {
            "id": "bf80ad69-cb57-497c-8122-3e97c9e2bfa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 84
        },
        {
            "id": "29934b60-d396-46ff-be00-0feed7c3f34d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 85
        },
        {
            "id": "fc2c54f8-b603-45e9-9c0a-b3769cbd4e44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "fc6a23b0-142e-4889-ad24-51d30c84c87b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "cc33d989-d8e1-4a2b-aacd-c70c2307b491",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "6188d958-6672-4392-b7fb-3c1f7b586917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "5fe3c317-6747-483c-bfe3-cf5ac867d50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "e01d1a98-95a5-4d5f-8fed-ed174533ac76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 92
        },
        {
            "id": "aa065b0d-7581-43cc-b087-dcbbeb3f8bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 93
        },
        {
            "id": "10466389-bce8-42b5-a2de-cb190f58a25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 97
        },
        {
            "id": "99f7e7d3-d19e-45b8-9f79-5501ffad87f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 98
        },
        {
            "id": "51dc2333-9478-403f-9b0a-58bcd8991db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 99
        },
        {
            "id": "4cc2ad9a-7dfb-4946-bb27-a403dcf211ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 100
        },
        {
            "id": "e5ecb949-5805-43ac-8592-cbe6200fb8a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 101
        },
        {
            "id": "0f49b0e1-b18a-406f-8f58-89ebe20a46b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 102
        },
        {
            "id": "508f315b-a1b5-45f6-82b3-530f713fdd44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 103
        },
        {
            "id": "81481537-6a2f-4498-bd52-0a961c210f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 104
        },
        {
            "id": "e083d4ef-c1bc-4676-96d8-e0b3d110f9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 105
        },
        {
            "id": "404b016b-5fa5-4120-a450-853be2ca5c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 106
        },
        {
            "id": "8e2e94ae-04c5-428c-999e-bc0c17904817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 107
        },
        {
            "id": "f3da5090-b65a-4124-ab29-561e9a8a0863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 108
        },
        {
            "id": "b4751e8d-6d30-4075-a790-df9124f4534d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 109
        },
        {
            "id": "746b5cf7-bf72-454b-8f22-552024444cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 110
        },
        {
            "id": "7f247b17-0be9-460c-9a47-1b8d92f3bae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 111
        },
        {
            "id": "34947e60-a52f-4199-8a5e-b48414ce4f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 112
        },
        {
            "id": "ce04df31-ce4c-4889-b225-c441c839ece4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 113
        },
        {
            "id": "2cd1ae7e-f30a-4da5-9471-954789523e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 114
        },
        {
            "id": "58262546-f24f-4485-9b9a-09cf6da307b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 115
        },
        {
            "id": "60de1eeb-92f5-4fa9-8099-5dac32e88b1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 116
        },
        {
            "id": "8cf87f11-052d-4846-85d6-60ab424761de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 117
        },
        {
            "id": "d1b7c65b-3b9b-4a82-bf6e-b4d25ecf75b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 118
        },
        {
            "id": "0840806f-9c70-493d-acf5-e906944e91bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 119
        },
        {
            "id": "17b7d8dc-9621-465f-93e3-21661ca97667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 120
        },
        {
            "id": "9fe9c9ff-cec0-4cd6-aa70-bf1801f3bc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 121
        },
        {
            "id": "a70261bd-669a-45d1-add9-273403418082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 122
        },
        {
            "id": "1dbcfc6b-2ae7-4c1b-8d43-4276a85e0b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 173
        },
        {
            "id": "9ee43be6-a596-49c5-9533-2be3dd0baa5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 894
        },
        {
            "id": "164ecadc-d9bf-41fd-b185-4b4bd22904ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 8208
        },
        {
            "id": "c7feb78b-05e2-48a7-b9ca-2fa040830256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 33
        },
        {
            "id": "a5c67d28-923c-4e93-8b46-04a13be9437d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 34
        },
        {
            "id": "e1376a33-9962-473c-98e5-e5512e4a29b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 35
        },
        {
            "id": "c7e31b99-1e5c-4378-9caa-be9847de3bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 39
        },
        {
            "id": "b37b414b-60cc-4731-b73f-ffa453d5a1c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 40
        },
        {
            "id": "7aa8fd0d-e2b6-46b6-863e-d7ea3feef099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 41
        },
        {
            "id": "777bf022-a468-446a-811a-41c75c7d954e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 47
        },
        {
            "id": "b6aa2e48-a4c2-4a68-8aa1-1c74ab65ce95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 48
        },
        {
            "id": "eb0b0360-f136-4470-9a9b-6a77f27d7b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 49
        },
        {
            "id": "bc750c9d-d020-4775-85f0-ecd282f8226a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 50
        },
        {
            "id": "6a884771-95e0-453e-ab58-67ecd6d03ab3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "867284fe-11ef-4f4f-b0e7-de2a8da3e6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 52
        },
        {
            "id": "628b312f-767a-420b-a4d3-c2a6cf00b7c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "3df44a0f-fa59-4014-bf3a-bec2a416acfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 54
        },
        {
            "id": "32e544c0-e8cb-4123-b492-b4f4cf8cd9f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 55
        },
        {
            "id": "d14d355a-79b9-423b-bd69-b5d96a9d9411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "cbc74be6-13ba-4ce2-b326-95e51dfa6505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "2777a476-a653-490e-b33b-9cfeb58da581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 58
        },
        {
            "id": "43c0e9ca-6b40-48aa-9534-d75663b537d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 59
        },
        {
            "id": "4b90439a-b12b-4480-8b1e-6e35f7c9874a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 63
        },
        {
            "id": "76845ea5-5cd6-4a1a-b69b-bbdec9ac8d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 64
        },
        {
            "id": "a6293ff0-48cd-4ae7-a408-7f03d1b0179c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 65
        },
        {
            "id": "35323362-7c18-42e7-aad2-c4e402bdd1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 66
        },
        {
            "id": "4c251899-204c-4a46-af7d-51fdb27a238e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 67
        },
        {
            "id": "3df307d1-42c0-4427-81ac-2824015f7644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 68
        },
        {
            "id": "3f872fe9-5fd6-4f23-afad-272481f558dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 69
        },
        {
            "id": "76d28b86-5747-4bae-8746-ec5ef6cad0ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 70
        },
        {
            "id": "ee8b0160-9338-449c-99d9-a6ad96d3f1fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 71
        },
        {
            "id": "2db42a4d-9287-496c-8f4a-0c0534e91f64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 72
        },
        {
            "id": "3fa2d402-2818-41b9-ad86-dc042609eefa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 73
        },
        {
            "id": "6c5bcd8e-8814-41f1-a6c8-aec952aec649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 74
        },
        {
            "id": "ae859aeb-14f0-4b14-aa52-c875b364c656",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 75
        },
        {
            "id": "b7767a81-8be4-44ec-877b-75a8ce5d3737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 76
        },
        {
            "id": "d5688fec-a8da-44a5-a3cf-504cdf1c1df8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 77
        },
        {
            "id": "91dfa773-7277-4e1b-a8db-c0063e004d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 78
        },
        {
            "id": "fdcdc86e-5ffc-4ac3-9c62-082d59d90044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 79
        },
        {
            "id": "377f3faf-d772-48c1-a7a9-3c96bf6e577e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 80
        },
        {
            "id": "a558c537-78b5-41f3-8adc-106e766624f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 81
        },
        {
            "id": "2180cf67-cadb-4f1c-be55-d9390f084153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 82
        },
        {
            "id": "155a0bc7-f573-470b-903c-bb512ccc7c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 83
        },
        {
            "id": "b3634b9b-4c1b-4cf0-9b54-059c802aca56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "63cad2cc-88a7-4474-bb3b-e3713865e911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 85
        },
        {
            "id": "f919ea73-60c7-4036-a207-bade31ea3787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "5d67c8f2-7589-4f4b-8194-feb1c3ec90dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "7212c28b-c26f-4898-929c-edfa69f8362a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 88
        },
        {
            "id": "267bd19b-6473-4b4f-bac8-be35d5d77ec9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "603c58f7-317f-4885-8667-bec8ab89f0e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 90
        },
        {
            "id": "24a4c097-c00d-4c4f-9635-042ea59542e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 92
        },
        {
            "id": "3bf74afd-ca6c-4fb7-8fec-da9091cfc326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 93
        },
        {
            "id": "0f9ddcfe-8d21-482f-835b-b868aff7c436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 97
        },
        {
            "id": "2471dd22-f192-48a9-8983-621326362aca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 98
        },
        {
            "id": "a038bfae-8bdd-405e-ac36-ccfc9163b61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "ddf6e1e6-1a5b-4b0e-a431-079e9c491e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "d090e516-4b53-4fe3-aa44-bac89a66bc5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "92016d91-0523-4554-8ab4-08b55fc25020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 102
        },
        {
            "id": "8057a37d-79db-4be3-ab89-cef5b912af32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "35c0d440-4ae5-44f5-98c3-c187b1d542fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 104
        },
        {
            "id": "aa04c8c3-eb60-4f9c-abfe-118adbbbb2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 105
        },
        {
            "id": "e3235302-21e7-437f-9bca-364c998f992a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 106
        },
        {
            "id": "db3076cc-0a54-489e-8a71-90ef36a790e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 107
        },
        {
            "id": "a5664036-b59e-4292-ac77-cdb05a78ff28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 108
        },
        {
            "id": "cb5f981d-1e43-4890-bd6b-3e10890202ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 109
        },
        {
            "id": "bf4df5c6-700c-4cf9-bbe7-acb99091a934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 110
        },
        {
            "id": "23c88170-24e7-4201-8c3c-21b34babe231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "a33ed6e5-3578-46e8-9d90-69f323133ff4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 112
        },
        {
            "id": "583bebef-a5a7-49bd-8c7a-fb592187f3cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "ec7ac3f0-154a-4670-8327-0d70690050ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 114
        },
        {
            "id": "b9b5c005-98c9-4136-9d6b-fe0b4408f4fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 115
        },
        {
            "id": "73136489-ec6b-4668-b1b2-d1dc9fdbda87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 116
        },
        {
            "id": "a7c48e82-edb0-403b-816a-f0ca9256071f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 117
        },
        {
            "id": "91af6ad3-77a0-47f4-a9ea-698c18b6a77a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 118
        },
        {
            "id": "e1a87114-cd4a-4469-9e4b-cde4e35762cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 119
        },
        {
            "id": "32d60e8e-0793-4b36-8be4-b5231f6c9ce5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 120
        },
        {
            "id": "fa17359f-180f-4a01-a403-ee7db0105ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "d87781bb-8aa3-463a-a97e-903dee1394fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 122
        },
        {
            "id": "a488c397-152b-4462-bf9b-97f5df77c1b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 894
        },
        {
            "id": "3aeae22a-10a9-480f-b84e-fcdd03b8dfa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 33
        },
        {
            "id": "d36e6605-dde8-4c6d-a5f4-04e835275699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 34
        },
        {
            "id": "41d2fd1f-9c55-451c-9e90-2e7e33a029e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 35
        },
        {
            "id": "cd0a8c63-78f5-46a8-8e5e-54dc94fabc11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 39
        },
        {
            "id": "d8185aee-626e-4369-a842-15466cb37ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 40
        },
        {
            "id": "e3b81c7c-8c03-4d44-a5f5-816ab4cf02f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 41
        },
        {
            "id": "62746292-da69-4983-9c07-96180cac0cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 47
        },
        {
            "id": "4c2cfdb9-c360-4e38-85bc-697027fe0bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 48
        },
        {
            "id": "9e9b43cb-b9fa-4e5f-8f1b-74d1782e559b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "2d929ac7-93e8-4808-ac5e-f6c3e2fc78ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 50
        },
        {
            "id": "82d3a480-2740-40af-98b5-4a8a7f26e9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 51
        },
        {
            "id": "cad97692-bc64-4c27-8ee7-acfe74297830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 52
        },
        {
            "id": "cf33328f-0a91-4348-82c3-95ccbe99c8e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 53
        },
        {
            "id": "5b767ec4-1cda-4b9e-a7bb-4827bf8a92e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 54
        },
        {
            "id": "8dd9297e-9c94-426d-ab92-f30e9ff35694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 55
        },
        {
            "id": "5e8056cd-cc1e-45eb-9361-c7e89e2b9a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 56
        },
        {
            "id": "88be9654-dc09-44e4-9676-c9802605279d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 57
        },
        {
            "id": "908ed95c-1de5-499d-b780-d17fa7907547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 58
        },
        {
            "id": "8239f35b-26d4-4bde-a097-4a9f8340d717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 59
        },
        {
            "id": "580ef7f0-4765-456e-a657-5e818d831b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 63
        },
        {
            "id": "703df452-4fd3-4257-bde2-02661071064f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 64
        },
        {
            "id": "0809b93e-2eca-4d71-a1de-66de9112c9dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 65
        },
        {
            "id": "e9e8fa0a-3471-4af3-9f38-cc9b1bf73196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 66
        },
        {
            "id": "e77d63c5-5f7b-481b-bbc1-09480f991cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 67
        },
        {
            "id": "cfdcb612-8a91-4f6d-bba2-3566f7c975bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 68
        },
        {
            "id": "3f8b43ed-f662-430c-97e1-8c5715df0bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 69
        },
        {
            "id": "b6dca17b-a263-4f74-b3e8-3d9d7ceb3b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 70
        },
        {
            "id": "ca128850-93c9-4bbe-81a5-5480119557e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 71
        },
        {
            "id": "3bf73573-fc8c-48a3-a621-f83796f9e420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 72
        },
        {
            "id": "36b01273-0d99-449d-ac98-5da0b829bfaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 73
        },
        {
            "id": "ac854ce2-b05a-4fef-a837-ac22caac3879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 74
        },
        {
            "id": "088dc9da-02fd-4b76-90e6-dac9ffa26faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 75
        },
        {
            "id": "479f02a7-2692-452c-bc41-4e6efd24572e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 76
        },
        {
            "id": "710f2463-a91b-4f75-95e3-9eea6a379fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 77
        },
        {
            "id": "43b9018a-0a24-490f-8b3c-d17baa6e2c9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 78
        },
        {
            "id": "9c05627b-f34b-40eb-b702-18f82cfdc338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 79
        },
        {
            "id": "586f8c2d-0127-4a74-ac48-614dc983855b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 80
        },
        {
            "id": "d692da7a-51ce-4ffd-a150-2a00cd532e02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 81
        },
        {
            "id": "a4b56046-8c38-4c84-9715-bead18bd4d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 82
        },
        {
            "id": "26af8302-5daf-4764-8ba2-9b5a23002bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 83
        },
        {
            "id": "d3e848e3-d919-4f0a-b8fe-38089a98ec4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 83,
            "second": 84
        },
        {
            "id": "b5e83c6a-1adb-40d4-9ee3-9069941e149d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 85
        },
        {
            "id": "feb5760a-bde2-465e-8ab4-966014491d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 86
        },
        {
            "id": "6c5f0ceb-8eb3-45ce-a233-98b75cb7e624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 87
        },
        {
            "id": "9be9482a-336c-4b96-a418-829d34c47326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 88
        },
        {
            "id": "abe7bbfe-1998-4289-8f85-8e7fa4c11a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 89
        },
        {
            "id": "7b46abeb-4632-445a-b9de-eef17127dc7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 90
        },
        {
            "id": "493fbbd5-7912-4b71-8187-fee0fa6f145d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 92
        },
        {
            "id": "6a9f7ba7-a0e6-4d75-8980-e42f925af096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 93
        },
        {
            "id": "57621719-af5d-4ec9-a71d-9c4ae9b7d44c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 97
        },
        {
            "id": "06d2dbd8-5983-468e-b529-bd1bfa12d3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 98
        },
        {
            "id": "37376863-d0ae-4471-9d63-d83da43774b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 99
        },
        {
            "id": "63b4339e-a741-42d7-a772-92d2882ec389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 100
        },
        {
            "id": "15969f6b-46b7-4534-b68d-32aa0d0313dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 101
        },
        {
            "id": "5d402154-5177-4d4d-862a-d591a328cf1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 102
        },
        {
            "id": "3f1a539b-ae64-4b11-8db7-f3d4751e1cc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 103
        },
        {
            "id": "a7415a3c-f466-44be-acb2-f42c1518eff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 104
        },
        {
            "id": "5b6d3c4e-707c-4caa-a4ea-3eb8e081ba16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 105
        },
        {
            "id": "b2ee10e0-5d9f-42a9-b840-94a66b410b35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 106
        },
        {
            "id": "cdb398e0-90e1-484d-a5ca-da2b26912aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 107
        },
        {
            "id": "2267a35a-3a4f-45b1-bcc0-229913aca449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 108
        },
        {
            "id": "73b2a7f2-1b87-4ee2-930b-7a1aaa5d371a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 109
        },
        {
            "id": "3e4b6296-79e7-479f-b18b-912cc40e996b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 110
        },
        {
            "id": "5fde6533-e4bd-421b-b7fd-7b3b042fd669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 111
        },
        {
            "id": "111c733d-ae4d-4581-b293-316e9872c102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 112
        },
        {
            "id": "64074297-f817-4686-9c27-34512bbe0d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 113
        },
        {
            "id": "7e439a3d-34c2-4a5d-84f6-28e1252dae47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 114
        },
        {
            "id": "3387775f-015b-4403-b459-74260281fa4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 115
        },
        {
            "id": "739add22-43e4-4e31-b316-ec755bde4f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "b0f7a94a-f5b7-4f92-a92a-868e05e571f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 117
        },
        {
            "id": "5214326c-6c37-499d-9982-ffa0b28ad3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "ba630fb7-1684-40f5-9a67-ee9e63f26538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 119
        },
        {
            "id": "809095fe-7408-4a21-9a61-ea9473681a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 120
        },
        {
            "id": "70666b5f-56ab-460a-afe2-129eed1e3dc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "2f984c2f-3b7f-4feb-80a9-51de41de6e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 122
        },
        {
            "id": "9b8710b0-0a3b-4667-98bc-5920d81faf5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 894
        },
        {
            "id": "e273708d-552a-4d2b-b36b-33cb394aaad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 33
        },
        {
            "id": "92f579e6-e993-4640-a992-bf4fb0fe4177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 35
        },
        {
            "id": "24e7f450-eaaf-444b-912a-61b7848e7c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 40
        },
        {
            "id": "43baea08-bcf1-4345-89b3-6e7e588ccf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 41
        },
        {
            "id": "a9e6d68d-b4db-4055-a9ee-d3da4b179813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "84b6c58b-3625-4e36-afbd-f2eee211d4c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "2e9914fd-6fb8-45f4-bc2b-194055945c47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "465103a8-321f-4fda-afb0-de50f1413e4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 47
        },
        {
            "id": "de6ec041-75ba-46fd-b8f2-e220b30bbf6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 49
        },
        {
            "id": "bf1375ff-bb4b-4734-aedc-8a5b608ca448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 50
        },
        {
            "id": "415f7074-9dde-43f4-9832-8cede46dc522",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "0af763d3-7c60-4e39-836c-e1d006468459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 52
        },
        {
            "id": "09ac57f1-df29-4e03-b125-333c5af0e999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 53
        },
        {
            "id": "9b4ba9be-f6e9-4b6b-a323-7fc7f7f25edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 55
        },
        {
            "id": "b741cb48-dc71-4374-b520-1d5fb1549433",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 56
        },
        {
            "id": "2fc0b931-c646-4926-836f-0e866d486f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 57
        },
        {
            "id": "5f8887ce-a858-4fc7-a403-39a5911d6a46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "cfdc1bca-4e37-48e7-8bc0-31c5e584fe29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "9326ecb5-fe60-4b07-9eb0-5a35c267421e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 63
        },
        {
            "id": "fc03de5b-0ab7-49e1-ae1b-8be68c147a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 64
        },
        {
            "id": "f0b6ff49-160c-40e0-ba64-2a84f34ea0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "a4ddc839-646b-4fdf-b7fe-583af25c0f2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 83
        },
        {
            "id": "ba05bb3c-e4a1-440b-9340-16231a4562ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 84
        },
        {
            "id": "9fa86b7e-b0a6-4869-9a9a-84f87c5def41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 86
        },
        {
            "id": "214ed7b2-b759-406d-805b-16c21b077cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 87
        },
        {
            "id": "5300ada4-d65c-4e9a-b203-2561a7c4f9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 88
        },
        {
            "id": "6edb9819-6384-41e5-b72e-c2de53599514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 89
        },
        {
            "id": "e9396fdd-3a31-4b3d-b52b-b96c0d4c3d21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 90
        },
        {
            "id": "0fad50ca-90a3-4758-b849-43df98018f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 92
        },
        {
            "id": "63dc7901-9baf-4299-8042-3a938a3de419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 93
        },
        {
            "id": "c039a295-5e7a-4a77-8c78-ae6b501936bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "6a526789-f707-4667-9dcd-a3ae3b7ece57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "d68cec49-4503-428f-a700-33af39540880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 100
        },
        {
            "id": "a357e1eb-2770-4f03-b337-9cae012effc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "a9ebb1ac-0aa5-478b-9576-77b00835516d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 103
        },
        {
            "id": "8850f9d8-b5c7-4a02-bc6c-bba68cd2320b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "22baef93-9c0c-4c0c-ba45-76677243fa36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 106
        },
        {
            "id": "d199fe1e-1012-41dc-8fa0-5f6a012cbd65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "4e12d552-785d-453f-9172-4cb33b6f1dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "c5d72892-490d-4d64-be11-e3c589c90808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "e442528e-bbc4-45a0-b092-6e78102e4a1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "f7fd6dd3-97a1-4465-86fc-0a839941f53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 113
        },
        {
            "id": "6083ee28-a68b-4be9-a805-a5a7559f394e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "f6462a30-4c3c-4666-989f-3183f3fea883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "48a92587-3f5d-4159-9058-3526f8925c86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "2a3d3937-f06d-452d-ac0f-2c2852f73709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "bc90a982-674c-4af9-b7a1-7249b859b52b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "78b799ea-5ee8-4a6a-b1f7-b840b057c425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "abadf60a-e663-4d97-9228-ba477c7862d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "f6c4c917-5dd6-40a7-835a-60724d201b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "51862a8f-c976-44a6-acb9-3e36b82163a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "72239c1f-4e1a-474a-a59a-365ffec262ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "9063be92-126b-4d65-a72e-a0b91725daee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8208
        },
        {
            "id": "4de32628-c404-4163-9e49-f75c623d82aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 33
        },
        {
            "id": "f9cebf56-3138-4285-a125-476a267586db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 34
        },
        {
            "id": "067cd0c4-905a-4add-8441-eccc515fb6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 35
        },
        {
            "id": "9db3a54a-d7a2-40c6-9711-c2dcc575d18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 39
        },
        {
            "id": "531db3b0-adb0-4935-9c30-52160df8897a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 40
        },
        {
            "id": "4dfb1a2f-8959-4c96-87d9-df8cf11fa653",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 41
        },
        {
            "id": "b6fc535a-9677-4b12-acd0-8c94621feb91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "c8453993-9d60-4318-b37e-ba75e6c0028b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 47
        },
        {
            "id": "dfee9972-f872-45f9-8588-d2bbc48431e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 48
        },
        {
            "id": "958fe2f2-845a-40d8-bb20-c8bc3cfcf613",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 49
        },
        {
            "id": "0351e35e-bf99-4f2d-915c-c25423c5b538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 50
        },
        {
            "id": "a51707b2-67f7-463f-b5b6-d26dc0296e90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "e3dadce7-38e9-4579-a768-5d2104b411b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "26a454b2-8847-462c-9a4f-9c76d7dad497",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "7f7be125-da36-42b7-90dc-dc724b09f172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 54
        },
        {
            "id": "4fdba2b2-c9eb-4806-9cd7-41479d8124f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 55
        },
        {
            "id": "76c72b5f-b367-40ab-b2aa-104129c3f178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "b8d1a655-0bcc-4bb9-8563-b9788132f858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 57
        },
        {
            "id": "783898f0-88a4-4228-803d-adcdea8de5b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 58
        },
        {
            "id": "998f8b3c-9423-49d7-91d0-767a40d31275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 59
        },
        {
            "id": "85df5b1c-9f9f-40c8-91e3-383d2ad7d847",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 63
        },
        {
            "id": "55f2acbb-a888-433a-87d9-f3d6b2d3ce67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 64
        },
        {
            "id": "9de09546-b093-4659-89a4-26c974560ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "a8330ca5-37f7-4e9b-accc-fce2bd5b3073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 66
        },
        {
            "id": "5d098f8a-c201-40d1-a5b8-f818636ec3bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 67
        },
        {
            "id": "d513fa58-69c6-43ad-b33e-fc4883a44bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 68
        },
        {
            "id": "2deb803d-73e6-48e9-919c-59e1152e84c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 69
        },
        {
            "id": "3b2a0255-d77c-4d9b-bdf4-2e7d2f5387ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 70
        },
        {
            "id": "b092ad11-3bfe-454e-8155-9f97b7ef2732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 71
        },
        {
            "id": "bdd34319-f20f-4d99-b540-0a153d2c268e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 72
        },
        {
            "id": "cd2c1f79-e789-432c-96b5-bc77e7100400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 73
        },
        {
            "id": "3d0151f8-1bf5-41ce-bfc7-63a42f38255c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "4752032a-329c-4c7d-b18e-00b5e01a5ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 75
        },
        {
            "id": "deec2e56-bd52-41da-a14a-878e75858f98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 76
        },
        {
            "id": "0fc074fd-8fe2-443a-8f87-e0dff78958be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 77
        },
        {
            "id": "404bf430-4545-4551-b9c4-93a8d4d40676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 78
        },
        {
            "id": "9da698e1-fb92-4697-8612-dc7e71270897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 79
        },
        {
            "id": "3a720fd4-dfa2-478a-8d1c-9e8b3b3e9cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 80
        },
        {
            "id": "2cb60479-66f4-4dd9-bf28-5bfd423d9ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 81
        },
        {
            "id": "6c000ecc-90f9-41eb-9ba3-c643af11db1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 82
        },
        {
            "id": "5c599fa5-0bd8-499a-bf01-86fd6fe382ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "622287cd-aea6-42b2-8f7f-c3de7cfa7cff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 84
        },
        {
            "id": "603dd8bd-6e9a-463c-8586-7b53a7484e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 85
        },
        {
            "id": "3e076e78-7514-414e-af03-c8a796791a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 86
        },
        {
            "id": "dac5f016-9855-4ddc-8d2c-a03328a81892",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 87
        },
        {
            "id": "9c861f82-544f-4ac4-b803-986b9033484f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 88
        },
        {
            "id": "87b1ea21-7a73-40d1-8e82-cb2de0787cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 89
        },
        {
            "id": "0e525913-a01c-44a4-839b-e2c5fc46e2be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "2561914a-c449-4b00-926b-f89cf086ba1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 92
        },
        {
            "id": "16737cf8-c5e7-4da7-95dc-32c049302ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 93
        },
        {
            "id": "e02b0889-b397-4f58-a150-1640c2904a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 97
        },
        {
            "id": "da2452f4-bf08-42c5-929d-c781de78bfb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 98
        },
        {
            "id": "08d8d75e-5faa-4a15-ab96-f70acf7a9d43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 99
        },
        {
            "id": "a2afb2ed-6ef4-4703-893c-9bb4006ff453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 100
        },
        {
            "id": "d5c79f41-c32c-425c-b2d8-a824a218457f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 101
        },
        {
            "id": "3af7075a-aec3-4002-be36-009b0832b019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 102
        },
        {
            "id": "b4174366-9e3b-4fda-9a39-c43c3a125908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "e0050253-3386-454c-8b09-7c60f53c46e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 104
        },
        {
            "id": "c1e8c7f1-9d0a-43c4-8006-48f0cd9d96bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 105
        },
        {
            "id": "48960f04-71a6-4d0c-b6bd-b0a8c643b7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 106
        },
        {
            "id": "f10a4b33-d2ae-47bd-9c5e-933cd26ca773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 107
        },
        {
            "id": "0796e307-b849-4789-b6f4-5f2ea08bb3ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 108
        },
        {
            "id": "e608f298-9723-4c2b-8304-c184d6608311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 109
        },
        {
            "id": "6bb18e75-6b99-494f-9eae-a3aae8c87b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 110
        },
        {
            "id": "ee2fff9c-5061-4b6e-a3c3-6b7f02656c76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 111
        },
        {
            "id": "35729ce6-56f8-4b76-a606-828042a73d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 112
        },
        {
            "id": "01cec0f6-34a0-4d74-a289-d20e773f0948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 113
        },
        {
            "id": "48df49df-a422-4241-913c-0d04acd65eb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 114
        },
        {
            "id": "f22d2677-4236-487e-bd90-cfb25473e4b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "9661fca7-2707-4822-bc02-1ba5eefae96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 116
        },
        {
            "id": "36194ea5-e34a-480f-a154-c5fcabe24336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 117
        },
        {
            "id": "3d35ae7f-c027-4a13-8a93-1a2dd73b0d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "c1dcd6b7-376a-479c-8058-2af4ad3d2a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 119
        },
        {
            "id": "70f42700-1ef3-458d-9486-08a102a23b49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 120
        },
        {
            "id": "2bab541a-66f8-421d-9fd4-42d4d9a43be4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 121
        },
        {
            "id": "388e06f9-090f-4a50-91b5-47f6043c827f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 122
        },
        {
            "id": "530abb14-fb20-427a-9876-e5fe4cd9db7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 173
        },
        {
            "id": "df73bc3a-e9cb-4417-931e-512c1351bc9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 894
        },
        {
            "id": "6314bff4-96fc-4039-a6c6-f7d8660f5aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8208
        },
        {
            "id": "b4f60484-61f5-4a5c-867b-951937fc4de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 33
        },
        {
            "id": "effaa400-8d49-4a80-85a3-d1872b9bf3e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 34
        },
        {
            "id": "1407f402-ebd5-43c9-adec-988ed60dd949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 35
        },
        {
            "id": "67f628c7-2a08-42fa-8b5c-16f3e9527a96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 39
        },
        {
            "id": "a543260c-ea7f-46c9-b849-0a62b0529b96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 40
        },
        {
            "id": "ba1d0e0f-1c3d-479e-b0cd-d65404f36c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 41
        },
        {
            "id": "ce9a3ec5-f0dd-4e88-9ee1-3abd04c20762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "c3e57b6d-eb5c-4fc3-9ee2-af8a079416b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "cf54d0ef-e481-480d-a6f5-5ccb9d778cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 47
        },
        {
            "id": "b7f89c3f-7f5a-40f0-829a-4aef65dee0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 48
        },
        {
            "id": "5171f74f-a712-4846-9c73-a8bff316affd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 49
        },
        {
            "id": "86126513-d4ff-4d0e-a14a-c93ab5b14ea3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "0677aa36-e9e5-430f-9559-24f3f48ae773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "0a5ff82e-4adf-4386-ad84-498d44d3a2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 52
        },
        {
            "id": "759ae725-2524-4f98-a51d-bf84c6112ada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 53
        },
        {
            "id": "8e7b29f9-5e1c-4ef0-86a6-f807ecfc0f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 54
        },
        {
            "id": "3fb93e7c-c6ac-4b79-bee1-a6a1cad0de55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 55
        },
        {
            "id": "0c76b10b-fe6b-41ad-8f5e-c89840163921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "81acc72c-f711-4c0d-9554-db8c1dfe9a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 57
        },
        {
            "id": "64b1b152-0598-4eb0-b9ba-008f41b1765a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "d938ff22-e758-46ee-822b-edc71b459f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "d871a3f7-549b-4ef1-9d79-7e024e7b6ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 63
        },
        {
            "id": "a17b8f11-a5cf-4e26-ba7c-48eb90886bd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 64
        },
        {
            "id": "2d780289-90c8-4a76-950b-febba609a80a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "cf5a916d-9a7e-420b-b225-51a779a9a743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 66
        },
        {
            "id": "1b1dff86-6973-4b54-bf78-1aa36cda0e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "59848ba6-6bf5-4266-a2f4-84cf89f79055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 68
        },
        {
            "id": "cbd63b50-1d31-48a9-be63-d373659f35ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 69
        },
        {
            "id": "e11bfda4-5a15-44c5-8433-b5bcb146e607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 70
        },
        {
            "id": "713b3a63-0207-46ea-98fb-a238b9cd5788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "784724b0-6078-42a2-88b5-d07866e119d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 72
        },
        {
            "id": "e9f2734c-58bb-4e96-bf55-36fc88d94f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 73
        },
        {
            "id": "39ec7c03-3cad-4ced-ac31-0aac93258a5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "d90f789e-897f-4499-ab9b-4d37d91b8ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 75
        },
        {
            "id": "8f5382a7-02cd-4ab1-b0b9-09122c9068af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 76
        },
        {
            "id": "b4851837-5347-4e51-bed7-a05a8c781591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 77
        },
        {
            "id": "7b8e420d-99a7-4002-b801-1717d016a4cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 78
        },
        {
            "id": "5c5c9541-0632-4158-b301-38f8e0fd21ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 79
        },
        {
            "id": "ff00189e-59bd-4af5-9a86-6a1ee6776f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 80
        },
        {
            "id": "9a9e5850-79a2-44ac-ad7f-18b3a22bd635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 81
        },
        {
            "id": "73342d7d-14c7-4afc-99e0-249fc9ee5ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 82
        },
        {
            "id": "311ed889-aeeb-46bf-8186-ee2a69d089bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 83
        },
        {
            "id": "101d4e64-979e-4efd-aedd-b1d6e3807215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 84
        },
        {
            "id": "9e988fdc-ae5b-43b7-9121-5660a69cc979",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 85
        },
        {
            "id": "9943c4f9-f70b-4f3b-9ccf-15a877a7efb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 86
        },
        {
            "id": "50772203-d261-4082-9b24-24b0a02fa786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 87
        },
        {
            "id": "f9aaac7a-051a-4bba-98db-6440f19149a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 88
        },
        {
            "id": "4b5a0470-ac87-4801-81fe-bcebfead9c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 89
        },
        {
            "id": "118165c8-e080-4c4c-959c-4966bfb6e854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "043ec7a8-a04f-4830-aa0e-8969578d3eac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 92
        },
        {
            "id": "6223453d-d0d9-4ca9-aba7-b24ece2abd79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 93
        },
        {
            "id": "817b474c-d5fb-48c6-b536-9cc1a137d985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "45d72b87-613b-4ea3-8c68-5d83728d1a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 98
        },
        {
            "id": "a530696b-9e52-493d-978a-da54bad5e374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "a853fa6e-a098-438b-a69f-19da7bf17f10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "e031e8ac-132a-4aef-b2fa-d57896f75cd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "0f7ba463-7a9d-468b-8316-98b1fd4903ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 102
        },
        {
            "id": "9ea418fe-aebd-4474-b1de-ed8e2bed6760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "ef1c22cb-7cf8-4c6a-870b-e86fb1df6034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 104
        },
        {
            "id": "7538dc5b-50a4-42ef-a8b7-08a94a92d2cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "7d6c970a-e05f-44e5-abef-c7a812aa6e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 106
        },
        {
            "id": "6c3b1df6-2797-41e7-8235-5b566d27306b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 107
        },
        {
            "id": "e0299868-a9ce-4ce0-b643-ef676abc8743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 108
        },
        {
            "id": "18208322-e3d3-473b-b018-84205e219805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "a7f45446-b3c2-4c6a-a3a6-cad13667d0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "950d9306-fe78-46e6-959b-92ba5356da2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "a84d2c5c-4979-4162-a3f4-ce5548f43a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "d5f29b1f-2340-47be-84dc-7665a5b5c4f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "8d98b6bc-f096-4f6a-9a78-8eb5ccc80fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "c10e0ad6-ffdf-41fe-bcc2-1abc56c7e012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "cbe3457f-f859-47ba-89ee-898d0b02e5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 116
        },
        {
            "id": "159436c8-6212-4afe-8363-727ab844403b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "7092c407-e709-4260-a468-e5fbb678970d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 118
        },
        {
            "id": "88a33ae9-5baa-4946-a88d-e28bb9deb0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 119
        },
        {
            "id": "cb4f0e57-ac8e-4113-b7c5-d0224ed5cb8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 120
        },
        {
            "id": "fd10a2e5-84cc-4c65-8017-b35657273b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "552fc086-4457-45e4-9053-cf3fc38089e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 122
        },
        {
            "id": "79783ce7-8e80-437d-a698-b62aa3ce6531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "dccfe0f5-a151-4e9b-bad9-0b62211a0d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 33
        },
        {
            "id": "ffb6d3cb-f4b5-4a45-b694-386495be018d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 34
        },
        {
            "id": "74b6fc63-8867-438f-a1d9-bb1376c29835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 35
        },
        {
            "id": "cb84c730-7b84-46c1-9f3a-6094456798e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 39
        },
        {
            "id": "4e3c1512-8577-421b-a7a8-dedc4d4c8ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 40
        },
        {
            "id": "823e7a80-8aef-493b-8a11-262b2fb6bd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 41
        },
        {
            "id": "b2be9bdb-779a-4875-9a99-cf9f780fc2af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 47
        },
        {
            "id": "4cfdfd7c-f215-4148-9915-7d07e3574573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 49
        },
        {
            "id": "7a8ecf4c-9f11-4b65-b8aa-826f7ef230e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 50
        },
        {
            "id": "7a05a3de-ebfa-4663-9313-7617d7fa9271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "f3f6fdb0-cddf-4e06-a871-10af2ae697c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 52
        },
        {
            "id": "f91db5b6-2893-4869-a0f9-f10a84ee6d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 53
        },
        {
            "id": "3f4dadd4-c909-41fa-a67b-45cb5c87617e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 55
        },
        {
            "id": "951e1e1e-5f63-4cb5-917d-a1163f47e22a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "e15ce86c-234b-4e37-b713-89791bf25cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 57
        },
        {
            "id": "05834142-411b-4567-b7d1-9625d1188669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "eab97fae-45d8-457a-b349-fa0a07302ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "9523cb10-4498-4a5a-97cb-bf1f2f46dae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 63
        },
        {
            "id": "d1438360-f59a-438f-aec3-01cbd3a24e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 64
        },
        {
            "id": "47e708a5-d57a-4b54-ba54-837a812ed38d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "7737ac21-f787-40f6-b0b0-daadfc04a9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 83
        },
        {
            "id": "f5625dfa-53c0-4867-b285-7433c44c39c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 84
        },
        {
            "id": "de2c909a-0b09-4ffe-95f3-a8cce5ac24bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 86
        },
        {
            "id": "f1e83d7b-b6f7-4666-a990-523f9b162071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 87
        },
        {
            "id": "89b6067e-fd52-4f6d-9621-07cfaa7254f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 88
        },
        {
            "id": "8b9cdb6f-3065-4daa-b8dd-b5fe0f1706d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 89
        },
        {
            "id": "6b63a4bd-07e8-479e-82ef-a769d8218509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 90
        },
        {
            "id": "29579a51-9767-4f6c-8a89-e83ea8c2cd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 92
        },
        {
            "id": "13f57f8d-e437-4506-8406-1dfb3c47d98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 93
        },
        {
            "id": "8c9c6038-13f7-4710-a08f-04cf1b880cee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "7144a504-a1cb-4122-9e61-3733af0edb15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "37a14280-70c3-45a8-8cc6-82db336083fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "40847aa2-191c-4d5b-b312-7763dfd38045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 106
        },
        {
            "id": "5999bef0-7d0a-498a-8548-b1bf15312379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "b7c35a44-f231-4947-9644-462fa5fcacbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "cf0b7571-7b37-4ca1-ba1f-8dc83d9007e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 118
        },
        {
            "id": "ed49e948-6efb-4ada-ba49-2e75eb21ec17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 119
        },
        {
            "id": "34382fa5-9e2e-4ebe-9b74-d7f620968ae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 120
        },
        {
            "id": "1a4fd0e7-a778-49f6-b1f0-7c819c724bd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "da041157-ea26-42f7-8111-8dcf170139b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 122
        },
        {
            "id": "ebb71a51-32cd-4cda-adb2-c68125b89862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "d7730e88-b2e8-490d-acc5-a8ddaabf1262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 33
        },
        {
            "id": "f2d462ab-b6b4-4f13-a35c-2f4ee191a012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 34
        },
        {
            "id": "531c67af-b69d-48da-9389-69b81af48182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 35
        },
        {
            "id": "bf779d96-886e-4510-9b02-0cb7a6207130",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 39
        },
        {
            "id": "2c1f4ffc-6b8c-4439-9d88-9206975ca4da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 40
        },
        {
            "id": "bbea1832-a140-45ff-bb2e-4d61bf659ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 41
        },
        {
            "id": "64cd3752-b94a-4e93-9d12-f9c0daedfcc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "dc27c4c4-b5fb-41ea-95b8-b1d59d752e96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 47
        },
        {
            "id": "16243710-1289-42fb-8753-d6e58ddb7d5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 48
        },
        {
            "id": "b7bd4ce6-efbe-4ca9-b843-6b9e800ed95e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 49
        },
        {
            "id": "9c07b70a-f9dd-461c-ad8f-4d8fd0f13910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 50
        },
        {
            "id": "dfa17533-7995-4381-9ac0-1658687f8f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "18f89c83-10a7-4424-b520-8ac92f6b3623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 52
        },
        {
            "id": "a3523529-b8bc-408d-ac5a-45c2c41a3c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "6074e0e5-51c2-47c7-a0cc-f55bc517d6a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 54
        },
        {
            "id": "1365f486-e45d-4048-a8b1-aa8d3bb9fdc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 55
        },
        {
            "id": "a7dafe63-ecc1-4b52-be20-719c2ae7d8f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 56
        },
        {
            "id": "4d446cd7-26b9-4ee8-9c3b-5ea6dcb3d65f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "b3837f7d-7f4d-4d2b-914a-3d80fd95dd6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 58
        },
        {
            "id": "8cb3c1c2-de06-485b-a775-f56a072e4f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 59
        },
        {
            "id": "ddce10ae-4855-4bfb-b3ce-efc18050758e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 63
        },
        {
            "id": "42229d67-0d58-489d-b26d-cc697553babb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 64
        },
        {
            "id": "af265c34-882a-496b-8989-05d7a5073de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 65
        },
        {
            "id": "950d5286-7c4b-4892-9976-cbe4bb60dd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 66
        },
        {
            "id": "2ba9c08d-d777-4e14-95d6-b6ddd82f29ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 67
        },
        {
            "id": "6a4976d7-3d40-4fb1-9e2d-e93db86bf1ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 68
        },
        {
            "id": "03a83370-7bcc-46c3-a928-cfd8ed1b7879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 69
        },
        {
            "id": "050106dd-c76d-4c45-8705-7cce0199bb09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 70
        },
        {
            "id": "1c6a9e9c-867e-45fb-a0e0-f68ee3067d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 71
        },
        {
            "id": "35b12455-7812-4c69-bcae-35c917c2f64b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 72
        },
        {
            "id": "7afc87fa-3832-4598-97b9-ef014ec4a13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 73
        },
        {
            "id": "054fdc28-da16-4478-8c92-5b4ed20219fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 74
        },
        {
            "id": "5d87cd90-299e-48b7-bc30-94ccc32cd5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 75
        },
        {
            "id": "d2c23f54-055a-4859-ade0-fb955dde656e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 76
        },
        {
            "id": "c27cb257-d072-4ff4-a089-94b43e48f403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 77
        },
        {
            "id": "00bef0a5-4fcf-46aa-bb50-1817856b909a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 78
        },
        {
            "id": "770e750c-cdef-4443-af97-f5b126f68a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 79
        },
        {
            "id": "20c2260b-4ab2-46ce-a7b2-89993d3fc854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 80
        },
        {
            "id": "d2df3356-55bb-4f6b-a4dc-1c898054afed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 81
        },
        {
            "id": "111096ea-7f3f-446d-8d68-78d7b89b1056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 82
        },
        {
            "id": "23d5d19b-2dab-4dc5-9cba-524dd5fe736c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 83
        },
        {
            "id": "bdac10ac-008a-430a-aacf-a05f7104a004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "cd05cf67-0aba-407c-8224-1d5b941e79e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 85
        },
        {
            "id": "d454e1e4-5a01-4e3d-95c6-73cd35ec2d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 86
        },
        {
            "id": "d3f70f8e-88a6-4c39-b026-0f2c574a5fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 87
        },
        {
            "id": "32a7d76e-722c-4f99-b20b-f15aace27cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 88
        },
        {
            "id": "90e3b593-b504-44e5-a8aa-141cb6cfe6c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 89
        },
        {
            "id": "2cc14920-1513-469f-a92b-da904a7cf901",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 90
        },
        {
            "id": "a04b339c-b0d7-4fa2-862e-4f6039e97893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 92
        },
        {
            "id": "0404ba02-522f-42ac-9e17-e15997695728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 93
        },
        {
            "id": "359eec64-7196-4cde-8e3f-62b30fd92cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 97
        },
        {
            "id": "50a891d1-0fb8-435f-985c-7d11671346d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 98
        },
        {
            "id": "419c5225-a8e4-42c8-83cc-a644335bdc85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 99
        },
        {
            "id": "ce7979fb-9f07-4aab-bfcb-f2b7efaf0753",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 100
        },
        {
            "id": "94b1a43a-0364-446c-a79f-0aa5ea431793",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 101
        },
        {
            "id": "5a181be4-5c24-4a55-8164-9ba4344967a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 102
        },
        {
            "id": "37904c43-ef27-4502-8431-5910dfef1ff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 103
        },
        {
            "id": "c7763036-9ac5-4bbf-80f4-2f9ae3173227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 104
        },
        {
            "id": "8876f470-220c-4cab-8246-7d815f9c7605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 105
        },
        {
            "id": "eded136a-a6ca-414f-b89f-50799065b5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 106
        },
        {
            "id": "cebecd11-1546-4a4f-bc62-7f9c779008be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 107
        },
        {
            "id": "745d9f44-75a1-42d7-a8b0-635aa2a68bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 108
        },
        {
            "id": "4f7ea4e2-117c-440d-909a-bc2eb9d1fedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 109
        },
        {
            "id": "8a6835c8-7fe3-45d2-a4a4-7939cf1cfbfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 110
        },
        {
            "id": "e1de0438-3ca6-47b9-8736-32646b4c2c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 111
        },
        {
            "id": "bb38cfd5-e56e-4df6-a83c-cd11fe0ec329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 112
        },
        {
            "id": "5a81b1b0-6f8d-453a-a9e4-24c22406d17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 113
        },
        {
            "id": "a4bd4327-8aac-4382-a3d1-665eacf974eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 114
        },
        {
            "id": "72fc59ec-1eab-4cce-bc97-e6f409e35690",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 115
        },
        {
            "id": "b7cb19c9-487a-4b3d-ae69-d904e760debc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 116
        },
        {
            "id": "de8835bc-a0fa-49e2-a99a-365e86a8d977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 117
        },
        {
            "id": "cabe9e3e-fbfd-4a86-a6f2-3ba59125fb3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 118
        },
        {
            "id": "74f3ad10-50d3-46a1-ba53-39ad97b24b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 119
        },
        {
            "id": "31eca049-2c4c-44b9-a997-039e5c112280",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 120
        },
        {
            "id": "bd488faa-d89e-4de4-91cb-eef68f184f48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "47fcdfb3-52bc-409d-85bb-3a303907f331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 122
        },
        {
            "id": "65c96833-1e71-45e9-ba87-b44b05ebe22e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "76af3175-104c-45b6-ada9-b68a0b89fd3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 894
        },
        {
            "id": "bda33499-a118-46f3-9469-052c0c176c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 8208
        },
        {
            "id": "ce33b789-769a-41af-bb67-8a267c974a81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 33
        },
        {
            "id": "2d49fd44-55ab-487d-aa69-ea3647158338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 34
        },
        {
            "id": "f31ac43b-7a11-48a4-b4e6-bd980fa1bf38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 35
        },
        {
            "id": "e9d62e6a-6d2a-457e-a4d0-2d7580744344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 39
        },
        {
            "id": "b4fce1c2-b300-421a-bb4c-8f48273e64f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 40
        },
        {
            "id": "228a5c5a-f882-48c3-8d22-d6a9651ce329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 41
        },
        {
            "id": "a592f819-c9e7-4d3f-b4dd-c29bf9b10bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 47
        },
        {
            "id": "3f93a5c8-75ad-45fc-b71e-0690d01eeb18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 49
        },
        {
            "id": "143bc8ed-578c-4391-b12c-39a86332526a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 50
        },
        {
            "id": "70cf4177-c754-4e6a-a00e-d0c1475ddaf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "5e6d7946-51c9-4773-ad5c-31a053a02039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 52
        },
        {
            "id": "caf6b281-cce7-42fc-8b5e-502b5a4412e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 53
        },
        {
            "id": "8aba9a2e-983b-42e3-a17e-fb0bcad5b459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 55
        },
        {
            "id": "9a44b8d0-1869-45c3-bf4a-9bd170b7bed5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 56
        },
        {
            "id": "4f3322e0-bc67-4f07-b857-85d5b9b50e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "2bf12984-234b-40a7-8a9e-dc59849696b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "24c23efe-baaa-4f68-ba87-fa4a2f09410a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "38d847bb-9cef-48f0-9d6d-7982b25ce893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 63
        },
        {
            "id": "6be76a3c-c714-4742-a93c-d56db94c5aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 64
        },
        {
            "id": "fd79c56a-de3a-46d4-bf42-6ce8e93e7999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "0b6ce45c-d762-41bf-b575-4da93e89b3a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 83
        },
        {
            "id": "f8b02604-4eb3-440e-8db1-122b52d3c7e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 84
        },
        {
            "id": "0ac31c12-6e48-4fac-8f88-d815d03f560f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 86
        },
        {
            "id": "bd361e1d-926b-4746-871b-e11449117af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 87
        },
        {
            "id": "dc2259db-ff52-48e8-a7c0-539429717df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 88
        },
        {
            "id": "10a0c2af-7ed9-4eed-917f-631adcba83bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 89
        },
        {
            "id": "725f48c0-4419-4a19-b804-692c8dea65ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 90
        },
        {
            "id": "88d78126-de4a-4820-83be-fb412529756d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 92
        },
        {
            "id": "c841e74a-1658-4a80-8a13-03f3b54a2863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 93
        },
        {
            "id": "23592ffe-4cd7-45ea-88ac-f5d16c58c8fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "75d19d0a-9898-4cce-8d9c-c769e8ad4761",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "df93e89b-98bb-4013-859a-3b3de7b7db1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "e98ac960-02b2-4be8-8f87-573e6670cf77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 106
        },
        {
            "id": "f05d4526-b523-479e-bfe6-7d42331a89e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "f0ac897c-7e98-4a8a-814e-54526ac23061",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "91945caa-ab8e-440c-8bbf-6af434083917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "43a0489d-bf36-4f81-9bdf-82695f383fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 119
        },
        {
            "id": "eecc9d38-0e65-4ad9-84ec-2b8d18295ca9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 120
        },
        {
            "id": "55cbad04-d1d2-46c7-a719-db9834c603fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 121
        },
        {
            "id": "d81ca879-7d47-46a4-8bdb-6ef72d56c5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 122
        },
        {
            "id": "e16942ef-9bef-4c41-9790-0757fa7d4a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "ed16d0d9-9b4a-491a-8af9-7cde686b6043",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 33
        },
        {
            "id": "29ba7b12-40e7-4ea8-87d8-420be6d5bc67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 35
        },
        {
            "id": "44bd4cea-122b-4e0e-8fc9-b41b49c62260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 40
        },
        {
            "id": "40d52663-3c7b-4c90-bc61-ae37851c44b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 41
        },
        {
            "id": "9e3f9d4d-0407-4045-8550-ab3a66fa7e19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "c72d28fa-de24-4e45-b0ca-8f992a9d1d2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 47
        },
        {
            "id": "c92d442b-bb4f-452b-9fba-7d5859cfa4de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 48
        },
        {
            "id": "4d5353d2-2049-4026-90d8-a024f6aff06a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 49
        },
        {
            "id": "4c898eeb-dd75-4a38-8cbc-1fc3679c7a12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 50
        },
        {
            "id": "f0c4fc89-2700-44b7-ab2c-3e48818854fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 51
        },
        {
            "id": "612678f7-e4e1-4c85-8eae-996a18dbb512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 52
        },
        {
            "id": "de82fa72-8e2b-4597-b585-5a8697f68e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 53
        },
        {
            "id": "f4d3f3bd-db23-4e39-be6e-ac8ce469b885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 54
        },
        {
            "id": "d79c2cbb-ccc5-4dec-bab2-b0275c4a63ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 55
        },
        {
            "id": "7cfab7b5-0032-4da3-b3c1-4dc79d7e253c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 56
        },
        {
            "id": "8dfd4e1f-48a5-4c05-b931-4e8148553d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "8ba5b3c6-5d3e-4b28-b33e-86da8d5d0a6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 58
        },
        {
            "id": "98c61073-80a5-4511-95b2-54263d3b390e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 59
        },
        {
            "id": "cbdb5836-4a47-451b-87fc-8d9420441d82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 63
        },
        {
            "id": "a8cf498a-3cd4-4a73-a47d-a83b493971d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 64
        },
        {
            "id": "4e505367-927d-4bdf-bdf3-03cbf6ad1160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 65
        },
        {
            "id": "53da6e9b-2f7c-465a-a76e-74169dc1f2e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 66
        },
        {
            "id": "5d1783ab-e5ef-43ca-a35e-f773ffd93d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "709bf242-7623-43e1-873f-4ae82530cbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 68
        },
        {
            "id": "77ea05ad-0726-41b8-8fca-aa9339f0b07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 69
        },
        {
            "id": "e67b5bcd-8127-4323-8331-c036ec22fe24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 70
        },
        {
            "id": "227b3ce4-e08c-4ac4-b753-bac6794a250e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 71
        },
        {
            "id": "1a54cf93-4e52-4938-9831-48aecba5c9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 72
        },
        {
            "id": "eccfec01-06fa-499c-b355-5d4f9cdcb234",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 73
        },
        {
            "id": "c1e7cdc9-aee3-4b0a-95d1-685a73a86ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 74
        },
        {
            "id": "88982ab4-6841-4419-901e-de81b7747b06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 75
        },
        {
            "id": "47f07a65-5f88-4b90-acb7-12304d910746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 76
        },
        {
            "id": "47aa9969-89f2-4c68-83d3-c534e6a1063f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 77
        },
        {
            "id": "2998d9a6-4921-478e-a5ff-ccebc34f681e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 78
        },
        {
            "id": "79814246-08f7-4ea9-a6b7-6b1d0894ffd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 79
        },
        {
            "id": "098fc24d-8623-4671-aab2-8a265fe70df7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 80
        },
        {
            "id": "9de26b59-eaf3-4a54-bb6e-04338eb7a8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 81
        },
        {
            "id": "011a0958-7713-4a2d-a342-59996301e45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 82
        },
        {
            "id": "23059e9b-35dd-4454-a807-3af2cfab45b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 83
        },
        {
            "id": "2bf8eecb-c421-45cd-a0aa-dff39f1bae8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 84
        },
        {
            "id": "699f1a3a-2aa8-4e42-a4c5-00e25d9b7d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 85
        },
        {
            "id": "ed9e4ff5-bdfe-4e77-8072-c98e92fdfae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 86
        },
        {
            "id": "8df2e745-7038-44cd-b31f-b9bddf274071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 87
        },
        {
            "id": "744c5855-dad0-424e-8d7b-9c5a5d42428c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 88
        },
        {
            "id": "f080eaaf-c432-4b93-b91b-84bc787d1271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 89
        },
        {
            "id": "250c21da-da19-483b-af02-9ea4b6b1c206",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 90
        },
        {
            "id": "c0330db9-e2ad-4c4f-af02-7d54a4cede0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 92
        },
        {
            "id": "3e97da37-8278-45fd-aa85-2468274abdd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 93
        },
        {
            "id": "45a01919-96d6-4fe5-9a13-c058b58fbdcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 97
        },
        {
            "id": "5839191a-1752-467b-a546-5094800b6c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 98
        },
        {
            "id": "b1bcaba7-4886-47fb-96d2-1b489e5d7411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 99
        },
        {
            "id": "653d4111-c445-4e86-ba34-044dad3cc151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 100
        },
        {
            "id": "83d66b2c-e336-43de-9e57-f256a497339a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 101
        },
        {
            "id": "827a4dcd-c005-4c44-a689-009d61c4299d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 102
        },
        {
            "id": "30cf39e3-d702-471a-9bf1-ae3d5780ce86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 103
        },
        {
            "id": "75e4353a-1f57-45d2-b955-1929299deae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 104
        },
        {
            "id": "9ac1663c-d7e3-4d27-a331-476173972eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 105
        },
        {
            "id": "a4507ad7-9137-46ee-998e-171ae2c8a59d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 106
        },
        {
            "id": "68d79ede-16fa-4bc6-8475-4f3e6cfd2822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 107
        },
        {
            "id": "7202d815-2b25-4337-a046-aa3a8b0b89b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 108
        },
        {
            "id": "aaf48f7b-b829-444d-b1f8-019d21266dbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 109
        },
        {
            "id": "a2fcd4db-e356-4ec6-91ac-2506dfdc59fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 110
        },
        {
            "id": "b813d4b8-1465-4b02-88e8-9e9be600c1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 111
        },
        {
            "id": "a5d9cf6d-f945-4f32-87aa-33818013bdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 112
        },
        {
            "id": "2ad11b3f-fd08-491f-9a3a-ceb1b762e599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 113
        },
        {
            "id": "e43127ce-4205-477b-b5aa-30e206284a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 114
        },
        {
            "id": "cc0528d4-00a6-4c57-8541-b91b0d48f8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 115
        },
        {
            "id": "24189d24-96b1-41a4-80de-9d8855202fcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 116
        },
        {
            "id": "2749b3c5-3b4a-48a0-9364-dc160fc01812",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 117
        },
        {
            "id": "f727697c-a3e0-4f79-aa5f-ff9a47745f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 118
        },
        {
            "id": "4046afd4-cec0-43ff-8d5a-f9cd7f5d1516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 119
        },
        {
            "id": "12b25562-3d5f-4af9-9912-1216443a1f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 120
        },
        {
            "id": "bbd2a74d-1d04-4ac5-9136-0ec8294d3813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "e1d47def-7457-4749-9104-73d7e9f88d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 122
        },
        {
            "id": "d9dbced5-240d-4c6b-bf8a-9fa59dd707ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 173
        },
        {
            "id": "0ce948cd-1b8a-49eb-a50b-4d123411d340",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 894
        },
        {
            "id": "b36cd21e-4099-4d0d-b4fe-ffea2b55f691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 8208
        },
        {
            "id": "d24ade45-8a48-4fc7-831c-e0629a5b79ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 33
        },
        {
            "id": "b732cd61-e0a4-477e-a741-1dfe6bb6ce54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 34
        },
        {
            "id": "148c5fdf-99c9-425a-b9eb-3220cf5ef26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 35
        },
        {
            "id": "8aa7f837-ab71-4086-90ab-e5888cfcaeb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 39
        },
        {
            "id": "b2eddd0b-4435-472f-84c3-9d359dfe13fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 40
        },
        {
            "id": "e33b089d-07c7-4b88-a7ca-c869afcc5c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 41
        },
        {
            "id": "c86894a2-929c-4728-b358-426205aff873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 47
        },
        {
            "id": "1aa48bb6-bcb4-4e1a-bc2e-7562368cb6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 48
        },
        {
            "id": "9b2e7d45-808d-417f-a32f-5be079dcdbb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 49
        },
        {
            "id": "bc26748a-003f-4b00-a2e9-46d9e9554150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 50
        },
        {
            "id": "4e68c66a-f6ce-4869-a8db-7da5ec08d33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 51
        },
        {
            "id": "fbb762e3-3f5f-45da-abee-75c0e12f305f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 52
        },
        {
            "id": "ef2f0c7d-aa67-4920-8d92-7edf771934ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 53
        },
        {
            "id": "f300312d-e9a8-476f-8905-7442523a8781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 54
        },
        {
            "id": "20bd03d3-fd8e-4667-835e-db733927f33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 55
        },
        {
            "id": "88c89fa8-0bc3-4afe-8f6d-bca203d48c38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 56
        },
        {
            "id": "f3cd645d-15e8-4509-8d6b-507fef363b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 57
        },
        {
            "id": "79995a2a-25fa-44c7-95e5-78e5ecf83110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 58
        },
        {
            "id": "a79cb05a-823d-4e85-bc25-743891308480",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 59
        },
        {
            "id": "f3e9204f-abf8-4470-9243-c2590403fbec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 63
        },
        {
            "id": "174532f2-d096-4355-9e38-21ed3a079390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 64
        },
        {
            "id": "a1699d17-302d-497e-8177-b1b0dac12ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 65
        },
        {
            "id": "43c7ac71-d5e5-4a5d-a69e-fd22371068f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 66
        },
        {
            "id": "f3496cfc-1dae-4d69-9d59-10259986ac5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 67
        },
        {
            "id": "6a88f815-c6f4-45a4-857a-d363c7d8e6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 68
        },
        {
            "id": "45bdf0d9-44cd-46aa-8528-165773d5aa87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 69
        },
        {
            "id": "0870bf01-277b-4375-8d7d-6af005846bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 70
        },
        {
            "id": "8adebe41-314c-4ee2-8367-e482251f7be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 71
        },
        {
            "id": "ba77d788-716a-4134-b099-1d427e150f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 72
        },
        {
            "id": "5c8bddf6-45d0-4d0e-b474-4ef82a0712e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 73
        },
        {
            "id": "5f40d59b-bdf4-4cb4-960e-27a042e5338f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 74
        },
        {
            "id": "1dc9df2d-082a-4c2e-a20a-2137c42e565c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 75
        },
        {
            "id": "7b9854f2-8959-432a-b2e8-84de92ecfcd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 76
        },
        {
            "id": "01dd70b5-110a-4e4e-992a-c17e91a32e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 77
        },
        {
            "id": "74de0fc6-d4c6-42dd-87dd-9ec120ba694d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 78
        },
        {
            "id": "4d019a7a-ea4b-4def-a947-61afb3c96d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 79
        },
        {
            "id": "c49a9e3e-e1b2-490f-959f-14912fe23b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 80
        },
        {
            "id": "48f2e691-b8a2-4225-bd97-148a5d8af67e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 81
        },
        {
            "id": "3259897e-94b9-4619-b9e8-58bb5eb849e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 82
        },
        {
            "id": "31548527-7ab8-4a40-9b49-06bc65fa83e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 83
        },
        {
            "id": "ce163e13-fc7f-44c7-b7a0-9d4772b25f24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 84
        },
        {
            "id": "59d80d8b-89e7-46fb-ab27-498dfb2cf70a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 85
        },
        {
            "id": "306fc297-698b-4057-a841-68f7e2707f8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 86
        },
        {
            "id": "dfce40e2-4179-498e-9428-40dc1c2f8479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 87
        },
        {
            "id": "ab3ace4d-654d-48a6-96be-e8ff15d8c06f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 88
        },
        {
            "id": "a7fb592a-32fb-418a-a9ea-bcf65b10de3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 89
        },
        {
            "id": "945cd264-2af2-438f-937e-f900db0175f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 90
        },
        {
            "id": "f2671918-4d86-43f3-bb49-1a42253d8c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 92,
            "second": 92
        },
        {
            "id": "0c2c33a6-ea29-4c7e-94ff-e0f01099a094",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 93
        },
        {
            "id": "95c9ba92-24c2-4492-860d-dff73bdd0289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 98
        },
        {
            "id": "8bf3cbe5-16de-4090-b1ec-1e160651b33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 100
        },
        {
            "id": "86876ffc-d202-4c4d-a2a5-a192563334c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 101
        },
        {
            "id": "fe36dfee-602c-4e71-995f-543ac3a6b128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 102
        },
        {
            "id": "73bb287e-3f2b-4ad4-9f39-bb2408692533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 103
        },
        {
            "id": "6eb3ac76-5b36-4bcf-9d52-cc4640de97b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 104
        },
        {
            "id": "97cd9187-5d56-4ab4-a52a-11ad41b16e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 105
        },
        {
            "id": "1306e9e8-be06-4705-b4db-1a78440d8af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 106
        },
        {
            "id": "d60ee31b-8372-486c-ade1-8110a8edd5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 107
        },
        {
            "id": "0830e6e0-8ff6-4741-bbb1-899fc54d471b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 108
        },
        {
            "id": "673c3dc2-ecd5-4721-bebf-868d498bba90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 109
        },
        {
            "id": "02656869-f303-412a-974e-6de6bd437f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 110
        },
        {
            "id": "81c937fe-c84d-4c2f-8d8f-51e7e66cd213",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 112
        },
        {
            "id": "19495e04-1864-4b11-84e4-dda009df31fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 113
        },
        {
            "id": "1fe113ff-d7c3-4952-8d16-68d40af2195d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 115
        },
        {
            "id": "a883e6f9-4ff9-4bdb-a916-8044d4fa79a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 116
        },
        {
            "id": "cb2b59bd-4e1d-49a4-922b-e79f8f2be0f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 118
        },
        {
            "id": "d0ad11e0-2999-48b0-9a74-74af8ed3be26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 119
        },
        {
            "id": "ec486f2d-f536-4c8d-b083-365db137ce2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 120
        },
        {
            "id": "dbfa5c25-480e-4df7-8fe2-9d8b88631834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 121
        },
        {
            "id": "e84fdd8f-a2da-4e07-a4ba-ba0128a52a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 122
        },
        {
            "id": "772d85d5-6ba4-4b21-af2b-afdec86bc0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 894
        },
        {
            "id": "490ff9ef-a2cd-431e-a91f-d8778da7ec1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 33
        },
        {
            "id": "73479bbb-14c3-4a86-a944-216d6f00e633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 34
        },
        {
            "id": "3cfad795-a385-4bfe-baae-ac3b5e016f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 35
        },
        {
            "id": "c65d4a0a-c884-43ea-bc36-b85449cf9ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 39
        },
        {
            "id": "19658082-d5d4-40d0-98c1-f206383e753c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 40
        },
        {
            "id": "c0869d8a-b234-432b-86e4-bb1ca248a110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 41
        },
        {
            "id": "93f73d66-8b80-4ca3-b744-f2061f7c9544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 47
        },
        {
            "id": "3a54c101-004c-4133-96a2-b1a6a70d571f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 49
        },
        {
            "id": "5594c63b-aa8e-40b8-8556-181a101bdac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 50
        },
        {
            "id": "b8f1a684-e938-4e84-a1c8-ecce3be94ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 51
        },
        {
            "id": "1cc368c1-f139-40d0-8a97-4bc85ff6f936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 52
        },
        {
            "id": "1a00ec56-5ac3-4f05-a055-14847191e8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 53
        },
        {
            "id": "4ae57b65-6318-45d4-ba4c-6fa799334931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 55
        },
        {
            "id": "46f0f253-db9c-4940-be97-376045808c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 56
        },
        {
            "id": "85ed61c4-c12f-4c2c-b61b-9bcbcf492504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 57
        },
        {
            "id": "de6c907a-da08-4c6f-becc-0dd55266197f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 58
        },
        {
            "id": "c61f47ff-4815-485c-a06d-778345ac00bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 59
        },
        {
            "id": "9f811c76-c3f8-4f37-9502-3ae0dadf17e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 63
        },
        {
            "id": "a772c5f0-08b7-4d71-9fb7-6a29fe45c69f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 64
        },
        {
            "id": "42915c34-ca6b-4b20-a265-363a40852d8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 74
        },
        {
            "id": "1b7675ac-036a-47d7-82a1-86b543447c00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 83
        },
        {
            "id": "a13f6cf3-8ab2-41de-9c36-fe5e4be01827",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 84
        },
        {
            "id": "379d21ff-c221-4084-9665-95cd89fd2181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 86
        },
        {
            "id": "ff749948-1d3a-4276-8b04-c8d65fdde710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 87
        },
        {
            "id": "e4f1cd13-3151-4c79-a7c1-a079e3ca1959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 88
        },
        {
            "id": "b437e523-9c0b-4728-9e65-f2ad7d05bb43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 89
        },
        {
            "id": "30cc2a14-6eb1-4fe5-97c5-118cb5b38b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 90
        },
        {
            "id": "c5686fd0-aa3e-4556-991e-604943ac7221",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 92
        },
        {
            "id": "a6b29c64-4323-4815-96d0-11fb4984e6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 93
        },
        {
            "id": "e80f455e-79e9-4b76-89f3-6e3f70e91204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 100
        },
        {
            "id": "e365e8fa-e6fc-465c-a851-4f53686c63a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 103
        },
        {
            "id": "21357730-f2ce-4b94-a34b-f31dddfb6624",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 105
        },
        {
            "id": "48b9a141-2cd4-4bd5-a0c3-4ec4911f2ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 106
        },
        {
            "id": "545b8538-4a5d-4e4b-b4dc-b7db8f0e389e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 113
        },
        {
            "id": "05fedf40-55b1-4dcf-bba3-6afafbe24fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 115
        },
        {
            "id": "e86019c4-0df6-424f-825d-3df928f406a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 118
        },
        {
            "id": "233e6e23-bdcc-4894-a6df-59c5502e5af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 119
        },
        {
            "id": "7c762f42-47a1-4fe5-8677-eee567ea1fd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 120
        },
        {
            "id": "133e6e32-eaf3-41da-95dc-100d27bea858",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 121
        },
        {
            "id": "b02743e0-1977-42f7-9bde-adab3b3aa077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 122
        },
        {
            "id": "7bfacc33-bc95-4d3c-8bf3-385afda2dba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 93,
            "second": 894
        },
        {
            "id": "0b89b24e-2fb0-411f-a9d7-32c635f6c473",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 33
        },
        {
            "id": "bad6b748-22ff-4b9b-8dc3-09c86ffb7d92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 34
        },
        {
            "id": "ffc860d0-3965-4765-9bc3-682763409a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 35
        },
        {
            "id": "d49bebfe-5ae3-47ff-b036-50519985b79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 39
        },
        {
            "id": "f2acb214-074f-4167-8a4d-3aa5f64cc8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 40
        },
        {
            "id": "0a9b224a-7ddf-4784-8823-f8e092d00b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 41
        },
        {
            "id": "1e7d2cec-e621-4eb1-b338-b2d964b08834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 45
        },
        {
            "id": "59f93790-e6b4-414f-bf22-55d62ac4280f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 47
        },
        {
            "id": "aa98150c-d16b-48fe-890a-91cd81aa3cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 48
        },
        {
            "id": "d68b89ab-4922-43d4-9a7d-1d48936b7495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 49
        },
        {
            "id": "91d5d14e-de4f-4127-9916-3c4057bf07be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 50
        },
        {
            "id": "59f1a83a-2c25-4cf8-afe3-f978e51b9f47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 51
        },
        {
            "id": "e53ee97b-55eb-4032-ace3-0fda1df651c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 52
        },
        {
            "id": "6bfdfbc6-cfd9-4552-a3ca-ade0b6825322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 53
        },
        {
            "id": "9a6c3e06-9173-4d1f-8275-f0aaabf11ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 54
        },
        {
            "id": "381a5ad7-c8f5-4c11-9de9-f94026cb60ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 97,
            "second": 55
        },
        {
            "id": "ce2f53c1-97fb-4662-9a53-da8e963b86c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 56
        },
        {
            "id": "c65d57b1-ab37-4081-aa96-51eff1fde842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 57
        },
        {
            "id": "7d78168f-54f6-40bb-945a-5f35e41cb6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 58
        },
        {
            "id": "3a655ba7-0a24-43f3-82ba-dc076725cfda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 59
        },
        {
            "id": "ab14d0d1-fd83-49b9-9f0b-c0443687834a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 63
        },
        {
            "id": "b355aeef-e2b5-41d0-b75a-c1b09ab71dc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 64
        },
        {
            "id": "a2face2a-00dd-4564-af8b-15789171b7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 97,
            "second": 92
        },
        {
            "id": "609d46b1-ae24-44d3-89dc-2ce4dbd332f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 93
        },
        {
            "id": "f1374da6-617d-4aaf-9b85-e9dc82b03a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 97
        },
        {
            "id": "55e2175f-63c5-40b4-afeb-f20efc990436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 98
        },
        {
            "id": "4c2f0535-b46d-4ab1-8647-5c29e08a046e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 99
        },
        {
            "id": "adaba14b-49bb-4c5c-bb9e-1e9838553814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 100
        },
        {
            "id": "e57479bb-805d-4bde-805f-c2f5c0585b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 101
        },
        {
            "id": "c0d7b3e6-71fa-47b0-aadf-1edfcdb365ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 102
        },
        {
            "id": "a3c4e59e-fe90-469f-b726-39e2b566cc95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 103
        },
        {
            "id": "27085355-2364-4031-9d16-26c4b3ae700f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 104
        },
        {
            "id": "af7d8cf8-1cd3-4766-b632-53a005fdd0af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 105
        },
        {
            "id": "941af549-1bb9-48c7-aea6-1e41697f1abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 106
        },
        {
            "id": "f9daff16-0f8d-4f86-a234-90a6fd3ed232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 107
        },
        {
            "id": "74a64822-92ba-430e-b780-5f358d86a575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 108
        },
        {
            "id": "aa5ce0d9-1962-40f8-8ba6-6f83d62e4d25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 109
        },
        {
            "id": "271b9910-b40c-4338-9297-35d4deb1d7e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 110
        },
        {
            "id": "6c9d1b69-fa81-469a-9662-ff9e32e49694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 111
        },
        {
            "id": "961475c7-c337-44ab-afd3-f43c47d5a621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 112
        },
        {
            "id": "53e4a62b-e56e-4aa7-a433-5edeae891879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 113
        },
        {
            "id": "71ab8f58-c5a4-401e-8031-e0f6b7e9f6a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 114
        },
        {
            "id": "2690a465-f514-4e9c-8fc4-54077a4641f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 115
        },
        {
            "id": "94a87734-99a4-4ecf-9e8d-974a6ed23d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 116
        },
        {
            "id": "5f43ec0d-d535-4451-869d-1ec6b8a032ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 117
        },
        {
            "id": "447a662f-c29c-4a85-9561-5f1233265610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "96874af1-948f-44c0-ab49-ecad011b4299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "0e1eb690-b3a3-447e-8edf-c6fda9b0deba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 120
        },
        {
            "id": "83688db5-8fff-4ce6-976f-02889e6be3c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 121
        },
        {
            "id": "0cf023be-122e-4c28-93fa-b5950b46ce66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 122
        },
        {
            "id": "0021d72c-e9d0-4810-a5d2-058fa832011f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 173
        },
        {
            "id": "08534c51-b15d-41cd-9e73-bafdb2519d36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 894
        },
        {
            "id": "4eed791a-526f-412e-94f9-7159a0fd90f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8208
        },
        {
            "id": "b960fc81-e7c5-45ca-89ae-8133bc8e3e4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 33
        },
        {
            "id": "e3effffc-173a-4ae8-86b9-233a83290c22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 34
        },
        {
            "id": "d46bbd97-efae-4410-b1d3-aa50f0034026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 35
        },
        {
            "id": "5508a90c-64c4-4b37-b85b-97a965e7e242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 39
        },
        {
            "id": "0ce0473d-494b-4493-9d10-e50500ba2999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 40
        },
        {
            "id": "0bdb923b-88b8-4f2d-a798-5da02ad21fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 41
        },
        {
            "id": "de671204-4151-4e14-9508-bafbc501e844",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 47
        },
        {
            "id": "77152d7b-7bc6-48a3-99b3-ea35dd45a30a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 48
        },
        {
            "id": "bdafa235-e08c-494d-ac1e-878c77a9a49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 49
        },
        {
            "id": "21ee4cfa-02b5-4891-be06-25075b7d2d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 50
        },
        {
            "id": "60aa18a6-9de2-41be-9edf-d3817c19ebe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 51
        },
        {
            "id": "c6a493f4-c47e-4eaa-896f-c2d77026dadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 52
        },
        {
            "id": "d5de8fbd-fc03-447c-9d1b-3a0a0aabe8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 53
        },
        {
            "id": "4199b5d9-aced-4284-8ef0-9cc9a993e373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 54
        },
        {
            "id": "8a639a55-e9ff-46cf-b5ea-6d69f380b0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 55
        },
        {
            "id": "266c1509-bd83-4912-bd86-214fbd25bcbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 56
        },
        {
            "id": "bce3f854-51f3-4475-9914-6d5090ca8434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 57
        },
        {
            "id": "b7cc2d5c-28c3-4ba2-9e9e-3fe8953909bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 58
        },
        {
            "id": "6d86d8c0-a340-49b5-baed-1b7422391b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 59
        },
        {
            "id": "c8569a98-cfdd-4296-89d4-d6e5f9a8367c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 63
        },
        {
            "id": "a8bc1f52-5148-474b-930e-791bf10001a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 64
        },
        {
            "id": "7d067a30-a269-408c-98dd-6737a9624f8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 92
        },
        {
            "id": "b082be2d-0a0f-45b0-9a37-c136e593d381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 93
        },
        {
            "id": "409e61e3-788e-4f60-9b97-455ad7cd684e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 98
        },
        {
            "id": "5158bb6e-7cc2-4254-91b9-52d1ed089243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 100
        },
        {
            "id": "7c3c9b6c-9a12-456a-b9f0-8f3d60132c40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 101
        },
        {
            "id": "f890561b-9d60-4fb2-bacf-93a66d701841",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 102
        },
        {
            "id": "57716d86-b359-43c3-b352-3aaa764886ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 103
        },
        {
            "id": "1a949859-598c-498f-8c37-492fe4153458",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 104
        },
        {
            "id": "4131a85f-b8d0-43b9-8920-657c4c9e9ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 105
        },
        {
            "id": "a81e7592-84ec-4c8c-9849-52bb0e7a3a56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 106
        },
        {
            "id": "33ac3dc6-6c46-42ac-b938-2345569b0250",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 107
        },
        {
            "id": "51c18ccb-9846-4e24-a868-7cf955f67476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 108
        },
        {
            "id": "78f25db9-faa5-41a4-9d34-59b0783093c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 109
        },
        {
            "id": "280f6ae2-4b18-4ff2-a61e-a95ae508b0eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 110
        },
        {
            "id": "18d37c7a-aadd-4115-919f-ef07db9f1e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 112
        },
        {
            "id": "610bc19c-8f90-434e-ad01-5c9bd88eba34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 113
        },
        {
            "id": "1355e029-db1e-471d-aeb4-f70c8f801f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 115
        },
        {
            "id": "b4e1a562-5e4f-4b22-8339-276d76808c0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 116
        },
        {
            "id": "bad4255f-ec04-4aa1-9403-89ec4bcb15dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 118
        },
        {
            "id": "f4f113d8-6ba2-4131-8932-10fe37164ad2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 119
        },
        {
            "id": "f29bf09a-5d4f-45dd-a633-31346dc48aec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "cefed3af-7bc3-4541-96af-39a6c1a8b6cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 121
        },
        {
            "id": "7a4c7aa0-de94-4dcf-b815-0c836caf826e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 122
        },
        {
            "id": "2add893e-b087-4395-9c90-67c004161669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 894
        },
        {
            "id": "66f7ac97-f9cd-48dc-b2f2-dd3a7eaf5b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 33
        },
        {
            "id": "b54577e5-7401-4734-a8d8-4ac629216c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 35
        },
        {
            "id": "1496a09b-9c36-434d-a32a-ce30aa9da90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 40
        },
        {
            "id": "65a74ef4-ec05-4114-a061-93ae29bf7136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 41
        },
        {
            "id": "e2e6d05b-79ec-4a99-be5d-74317332d196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 45
        },
        {
            "id": "05c33554-d1db-4c5a-a7cf-edf81cd813df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 47
        },
        {
            "id": "142f1152-c741-46ae-923c-8f13797aea3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 49
        },
        {
            "id": "f4664d65-ad2f-4f43-8006-ef05a29d55b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 50
        },
        {
            "id": "1ad46a7f-bc12-455d-b9b2-7e4614490661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 51
        },
        {
            "id": "056bc5c1-dd1e-4ddd-8a50-64e8fa63ede2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 52
        },
        {
            "id": "8b703c19-5344-498d-9b1d-933c9ab98987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 53
        },
        {
            "id": "ecea06ba-939e-472a-9e28-27d501fc8c4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 55
        },
        {
            "id": "8c889fdd-e15d-4727-b138-950a49a38c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 57
        },
        {
            "id": "2911e989-b19f-4cd4-a2de-bc4cb33d0e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 58
        },
        {
            "id": "6f4123fd-1346-4bfe-a107-d33d29e33ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 59
        },
        {
            "id": "6335f591-5187-41f3-a773-c227e962ff3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 63
        },
        {
            "id": "2d038fee-bc1e-4dec-8211-eb672f87149f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 92
        },
        {
            "id": "3c23c7d8-abea-4eb3-a3fa-145a6090a7ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 93
        },
        {
            "id": "f0c9e060-75d1-45b3-83e9-fcb4f50c647f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 103
        },
        {
            "id": "dd5eab7a-1605-4e40-a94c-01a2f09c0203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 105
        },
        {
            "id": "f42c749f-54d7-4491-b906-367d8f71f2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 106
        },
        {
            "id": "4aed1432-2135-45c5-90dd-5833d462996a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 115
        },
        {
            "id": "a0585a36-77c7-403e-9697-90a44b163d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 118
        },
        {
            "id": "4bf1d3a8-f5d5-4db2-9217-13e404885470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 119
        },
        {
            "id": "654df04d-c471-4624-a697-60ca9b1ec420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 120
        },
        {
            "id": "c11ff095-2290-4c1a-b63a-25fdd2eec510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 121
        },
        {
            "id": "c222c117-2620-4b4c-9416-b34592949504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 122
        },
        {
            "id": "7b6fd5ef-cb3c-43a1-81ae-9714572df3d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 173
        },
        {
            "id": "3889264a-6117-4964-8b09-65b80915790a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 894
        },
        {
            "id": "f51ff803-2a28-49ec-93a9-638ba0f5a83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 8208
        },
        {
            "id": "90d18b6f-5cd1-433c-90c4-2c7f5bbe2838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 33
        },
        {
            "id": "e861f68a-5042-456b-b43f-bac37f691efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 34
        },
        {
            "id": "8e6ae14f-d39d-4023-9cc9-2d8585bc9e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 35
        },
        {
            "id": "26257377-4fa0-4889-abdc-982b3b82c96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 39
        },
        {
            "id": "47b4d6c4-ef6a-4ffc-8077-f64a062c2730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 40
        },
        {
            "id": "1ce74d29-db6d-41a1-9a02-384cb4b2f16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 41
        },
        {
            "id": "fa584db3-a77d-49e6-951c-4e801e2c488d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 47
        },
        {
            "id": "01028599-2184-4ee6-8a84-8cbe578b04fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 49
        },
        {
            "id": "d911f0b4-636d-4dd1-bc89-28c1eafa65da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 50
        },
        {
            "id": "4de03938-da1d-456b-9bed-c640c2916f4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 51
        },
        {
            "id": "cb7102aa-0d54-4f18-a3e9-bdbce1bd7c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 52
        },
        {
            "id": "abe99c37-ec35-4687-a0fb-23882b28ebae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 53
        },
        {
            "id": "dfb30cfb-adb6-42d6-a879-be7a9d720ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 55
        },
        {
            "id": "b4abcb70-c7eb-4209-b3f3-220139d17c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 56
        },
        {
            "id": "2a0548c9-5ecb-4421-b0c8-8b09a6f8ec49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 57
        },
        {
            "id": "8dcbdc2c-b582-4879-8803-8bc84e472f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 58
        },
        {
            "id": "30cb751b-e5ca-402e-8d35-1060fef59b43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 59
        },
        {
            "id": "bd72246a-6454-4506-95fa-6d217200d4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 63
        },
        {
            "id": "f59b4ef3-caae-4a4d-af66-12b248bee1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 64
        },
        {
            "id": "ffe58f04-9dac-417c-8907-fadd1f661617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 92
        },
        {
            "id": "03a29928-4a4b-48ed-a254-07cd6b13c31e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 93
        },
        {
            "id": "ab232ae8-cf38-4eb3-b408-1e17e6862363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 100
        },
        {
            "id": "ba2e6893-2105-45d5-bfcc-f928a97ec3e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 103
        },
        {
            "id": "1afd252f-4fa8-4bef-8e8c-9007c0ea6622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 105
        },
        {
            "id": "f154336d-4236-428a-9bad-fd37d9259de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 106
        },
        {
            "id": "f45c3a5a-b4b4-4336-b437-7b33bec46dca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 113
        },
        {
            "id": "7cab71bb-3686-4802-8c8f-60fe8d9ad20a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 115
        },
        {
            "id": "9c506922-4260-42ce-8782-750832b74eaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 118
        },
        {
            "id": "414221f3-d1ec-4335-b04c-b2f139aa3618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 119
        },
        {
            "id": "56824ac4-cd02-46c8-a0b7-6afd9be1ce60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 120
        },
        {
            "id": "1d8c89f9-3bc6-4b53-bbb6-87171552a971",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 121
        },
        {
            "id": "2ecad7bf-b94e-42fa-a82e-1fcc78cea45c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 122
        },
        {
            "id": "f987ef57-5e74-4e7a-ba76-f016614c517e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 100,
            "second": 894
        },
        {
            "id": "a21bdfb7-7369-4003-a5aa-09f1d5fd8bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 33
        },
        {
            "id": "dc29671f-e9ca-4ea8-89f3-923509cb1c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "da0a17b6-4c53-4043-ba67-4ced4b98e7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 35
        },
        {
            "id": "23d855a5-dec8-4c8a-948b-74391dec6b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 39
        },
        {
            "id": "7b1daf13-3312-4149-ad32-0414ca54012c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 40
        },
        {
            "id": "c0ecb39d-9af1-44de-898f-c6c904ad0785",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 41
        },
        {
            "id": "6c37b536-0196-4ad3-8123-74260fd60434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 45
        },
        {
            "id": "d3c503fc-00bd-4b58-9a44-245d377a9a87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 47
        },
        {
            "id": "bc242bdc-94c5-4b80-b6da-a42d0f686dba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 48
        },
        {
            "id": "fda87fd6-dec3-4981-904e-0a6fd572931b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 49
        },
        {
            "id": "fe337103-343f-4222-8da2-5fb953e36d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 50
        },
        {
            "id": "4d945ead-edb0-4bf1-8a8e-9a7086820be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 51
        },
        {
            "id": "0a087c31-6707-48ae-85c0-1b04238f6e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 52
        },
        {
            "id": "5c696f58-e83d-410a-81e7-67d8d542e33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 53
        },
        {
            "id": "0ad7b7a8-dd01-4fa3-b35e-b0fa53615257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 54
        },
        {
            "id": "6c893c2c-9eb1-425c-aee6-ae95df637732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 55
        },
        {
            "id": "1eb6f0c2-11d9-4ff7-9433-82b73d762e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 56
        },
        {
            "id": "f6865a9c-0e80-45b8-b234-1a6aa89d2507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 57
        },
        {
            "id": "246260db-321b-452d-b79e-16d8c7b08746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 58
        },
        {
            "id": "600a65f4-df9f-4428-b047-374e1d269d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 59
        },
        {
            "id": "33398d22-11be-496b-b503-5954eca4b169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 63
        },
        {
            "id": "e8f15a22-1627-4155-9500-e583038c41af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 64
        },
        {
            "id": "715e3bff-a97f-400a-86ef-9f894567b273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 92
        },
        {
            "id": "8833e86b-95ea-48a1-8ccc-5af20af93fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 93
        },
        {
            "id": "2c5d1624-8256-42a5-80b2-bfa1c675907d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 97
        },
        {
            "id": "ab595a3e-8959-423a-9442-b0324679b63c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 98
        },
        {
            "id": "74bba7f3-ce4a-47b3-bd6c-95351c1499c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 99
        },
        {
            "id": "8e30b49b-48b8-4d41-b815-f78162c28e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 100
        },
        {
            "id": "4c7da03c-2f8f-42f4-a79d-3b9727a648a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 101
        },
        {
            "id": "d24232e8-503d-4ba2-9687-4ad31a85b84a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 102
        },
        {
            "id": "2c3e7171-cc02-41b9-880f-547bea3254fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 103
        },
        {
            "id": "92d987ec-a1be-4305-9f94-87c2a01fcc56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 104
        },
        {
            "id": "a164f5bf-ea25-4da6-b853-d36ca9f9f1b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 105
        },
        {
            "id": "21d0e69f-df3d-4ab5-a44a-9b8b6154ed04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 106
        },
        {
            "id": "f5c752cc-1d24-4f46-b4c9-9c0e0527527b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 107
        },
        {
            "id": "57703c16-fa32-4705-b0ad-b2d06fe382bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 108
        },
        {
            "id": "f017e588-6c19-48fe-b5b3-6e562c9276da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 109
        },
        {
            "id": "a02400d1-46af-4e55-bd77-d25df8fa35e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 110
        },
        {
            "id": "955b9800-2e11-4323-bc95-e68fcee5d7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 111
        },
        {
            "id": "a3a6d7f2-6ecc-4d2b-a802-4d3ac7ffe27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 112
        },
        {
            "id": "842929f1-ee74-4ef9-8f71-6295302a74ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 113
        },
        {
            "id": "ec03cded-26ee-4be2-9ad9-55a68ffd70bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 114
        },
        {
            "id": "f7390236-c9c7-41c0-8190-7fc10d44a855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 115
        },
        {
            "id": "705050e1-9d81-4e56-829d-c0b2c1aa2dce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 116
        },
        {
            "id": "3ad5f5b9-65d0-41af-8999-5849ea166f69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 117
        },
        {
            "id": "98a1187d-1162-4008-919b-e3ab1a10408e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 118
        },
        {
            "id": "b0c301d6-0d2e-4907-9f0b-2ea86e643a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 119
        },
        {
            "id": "b82f421f-c502-428b-8621-dfae7c377cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 120
        },
        {
            "id": "74f1ea26-1d04-416c-a9af-c0f227afd04a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 121
        },
        {
            "id": "c2a17c93-0b0c-4255-910f-f37469f58ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 122
        },
        {
            "id": "88a93952-9cce-436d-9890-0a28eb44944a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 173
        },
        {
            "id": "dbc246bb-7d1a-4853-9f22-0425270813dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 894
        },
        {
            "id": "789b3299-f3b5-406c-80da-9de86247e9c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 8208
        },
        {
            "id": "77c751fc-0873-41fe-afe3-2b1f4097f6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 33
        },
        {
            "id": "3c4ab143-d1c2-4372-80af-bb4ae16d04d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 34
        },
        {
            "id": "3f5f24d1-5d63-4651-aa88-be6ec724df16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 35
        },
        {
            "id": "d1c3ab50-3565-4e6f-8810-a4a07c125c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 39
        },
        {
            "id": "eb16d4b0-5730-4310-8714-f05a3f06b153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 40
        },
        {
            "id": "9eab662b-4a85-484f-9714-dc1ac6c2e7fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 41
        },
        {
            "id": "b07e365e-ad0f-4bd2-a1a8-971f6a6d1a9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 44
        },
        {
            "id": "3bbb43f3-7b53-4bc2-be89-e589c8cc574a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 102,
            "second": 46
        },
        {
            "id": "d4d8e89d-374f-4c17-9572-1b1f2173ca5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 47
        },
        {
            "id": "489d87d6-1290-406b-95fc-45849ddd2ea7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 48
        },
        {
            "id": "020468f2-bc7a-4c16-8010-091872f72009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 49
        },
        {
            "id": "4a7d1279-17d3-4295-a9a7-dc6cc50a97b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 50
        },
        {
            "id": "9d252a11-6ae3-417a-8e62-75178ed79542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 51
        },
        {
            "id": "2fa18efc-cf39-4cca-aaba-2bf803728096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 52
        },
        {
            "id": "921b8151-e5d5-4da6-94fc-2e056980bd57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 53
        },
        {
            "id": "6cd7ff63-6821-4d3f-9aa4-16ad9ec4952b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 54
        },
        {
            "id": "db8c9430-d1c6-41b2-bc29-9a976f20ad0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 55
        },
        {
            "id": "52733f7f-d844-4e9e-adfb-7161c2a4ebc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 56
        },
        {
            "id": "43265851-4303-44a8-bb16-fb730d1aec12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 57
        },
        {
            "id": "4962789b-4671-441c-8f24-25834648dd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 58
        },
        {
            "id": "6cd52fbe-7cb6-4843-993f-916594bd2008",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 59
        },
        {
            "id": "53008d0a-8a72-45f5-9dd3-6c3566967cc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 63
        },
        {
            "id": "b4b5f732-147a-4fa8-963d-f9cbfc2ba12c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 64
        },
        {
            "id": "c686a7cc-7010-41d9-a93d-911d9600f56d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 92
        },
        {
            "id": "baf7d7c5-ff47-4cd2-8632-cbd6526070ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 93
        },
        {
            "id": "595ee02f-bb0f-4fa3-8ff6-73328689194d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 97
        },
        {
            "id": "efdc8800-3603-4b8e-8d0b-cca96230544b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 98
        },
        {
            "id": "2f17ba3c-d696-45ae-b0b7-2661638aa334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 99
        },
        {
            "id": "f17cf458-0e40-4c70-9486-4f44e04003c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 100
        },
        {
            "id": "c9143a7d-201b-4a4b-a0c8-94f7148f4c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 101
        },
        {
            "id": "01aaebf4-0536-4013-ad84-3eaa41cb7f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "2cf9aaca-efc9-40c1-8daf-da23dbffd6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 103
        },
        {
            "id": "a8020c0a-77d4-4813-ae0f-7fba7faab2bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 104
        },
        {
            "id": "3bb942f3-bff1-4968-8d68-9be3a7fd91ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 105
        },
        {
            "id": "e0c1a9ec-82c6-431c-843e-920692e4a470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 106
        },
        {
            "id": "87641aa0-925d-4dde-b036-0f573e99a7bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 107
        },
        {
            "id": "2b1e80de-2b17-4854-ad3b-f602c0a471a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 108
        },
        {
            "id": "c3b964a9-fa3b-4eda-858c-8e2b3cc34f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 109
        },
        {
            "id": "b6803ce7-2d59-45aa-bc8f-8fd4220f53eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 110
        },
        {
            "id": "b9c2c769-9fe4-4de1-a632-a203c6fd6523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 111
        },
        {
            "id": "f3bbe041-0c8f-4d63-b63a-18700be747d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 112
        },
        {
            "id": "345ffc46-a2f7-4737-be1b-b4fb25be1c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 113
        },
        {
            "id": "ad5d9d9d-b6a8-450b-b607-48e2400ce728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 114
        },
        {
            "id": "de11313b-27b7-42f1-b7dd-88f100dc14a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 115
        },
        {
            "id": "97ed8e86-9f5c-45c6-9fe2-364f5e05b672",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 116
        },
        {
            "id": "8279c4ed-c95c-4cb4-8a94-84876d56016a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 117
        },
        {
            "id": "b8072e0a-9bdb-4e2b-b780-c6dfb0aa2506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 118
        },
        {
            "id": "09178a0e-7ec5-45b4-8744-41e3a26f9b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "c09a9701-10c6-47ba-bc00-7cd9635989c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 120
        },
        {
            "id": "489f4f9b-596a-4bf9-8ddd-e468b2735376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 121
        },
        {
            "id": "a21b0b43-544a-4a3f-abc8-2eb731d984a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 122
        },
        {
            "id": "f9cd7d38-ed5a-4d42-99f2-8e58df113f7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 894
        },
        {
            "id": "d4c95d8a-2c26-411f-8eb7-557d4f46f214",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 33
        },
        {
            "id": "cf8cdd93-3ca9-4aac-ae9b-9b682e048911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 34
        },
        {
            "id": "cf2f82e6-19a9-45b0-89e7-8949fbc2113b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 35
        },
        {
            "id": "b82addd4-cd76-47db-923f-de3f4163b00c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 39
        },
        {
            "id": "ba7f365a-9897-4b01-a04f-09555ec74914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 40
        },
        {
            "id": "2e506aa0-0b38-4be0-83e3-6e12c6ee73af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 41
        },
        {
            "id": "b6029f94-dc68-4afc-b6bc-520b8dadf1f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 44
        },
        {
            "id": "a1417932-524e-4ffe-b8bf-3426959b4805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 47
        },
        {
            "id": "2798732f-6374-41e3-880c-3e05d6c60db6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 49
        },
        {
            "id": "21b8ffd6-6215-45af-be52-3db7bc142260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 50
        },
        {
            "id": "2c85804c-3598-4885-9493-5c8375d07cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 51
        },
        {
            "id": "6d9bc6b0-d33e-4374-a301-5cb8ad5fea9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 52
        },
        {
            "id": "0459296c-82e0-42fb-8138-75a84e4d145f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 53
        },
        {
            "id": "0ff47574-e57b-479c-8a9f-b8444bfb6ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 55
        },
        {
            "id": "da6ad9b1-d8c6-4944-a58c-ac6296fa4d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 56
        },
        {
            "id": "1a38e118-4fd9-4f35-b7a8-b2f257dd5b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 57
        },
        {
            "id": "32f8c85e-652f-44bb-8334-ce8becb993d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 58
        },
        {
            "id": "6f142296-1504-4247-9db3-ee927b4f07ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 59
        },
        {
            "id": "7de12972-3297-4126-ab11-9a474f2cef06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 63
        },
        {
            "id": "35459833-3c17-4b71-a492-87dd7c4a0a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 64
        },
        {
            "id": "a11c8c55-bcf5-4202-9f71-f4e14d8ede16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 92
        },
        {
            "id": "8be64013-7cfd-45bf-8f8f-e0295d0b592b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 93
        },
        {
            "id": "06715720-6f15-478a-98ed-5d3365bfe350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 100
        },
        {
            "id": "c537899e-b1a0-4309-b0c9-a572e60fc6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 103
        },
        {
            "id": "4800e912-e3bd-4d6c-bc7d-8d574892aa30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 105
        },
        {
            "id": "2a80c61b-afd2-4a78-b8d1-e0dd8b7f35f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 106
        },
        {
            "id": "e83937fe-5bcb-45d1-87c1-43459634775c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 113
        },
        {
            "id": "65b97580-4e05-4278-a0fa-3abf6f5b1861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 115
        },
        {
            "id": "87beac95-1c6d-44f8-950a-e729f84a0fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 118
        },
        {
            "id": "169df1ab-5dae-4d8c-95a3-bc6ed0822104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 119
        },
        {
            "id": "bafc2fd1-2ad7-472d-a161-9ea505e37a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 120
        },
        {
            "id": "6b7a21da-05a3-4a13-be5e-559c7e3f2447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 121
        },
        {
            "id": "366122be-c8b1-4ce2-93cc-f7116f76580b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 122
        },
        {
            "id": "e45043be-4809-4ef0-95e5-158d1de41b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 103,
            "second": 894
        },
        {
            "id": "a62d737a-9746-4f68-a01d-b1ca39638d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 33
        },
        {
            "id": "e6a5246b-2c82-4934-bbf7-48088feb567a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 34
        },
        {
            "id": "90559b35-b16e-4bea-8245-1fcdb43f6850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 35
        },
        {
            "id": "231f62ac-3402-490b-b797-fdcedda40ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 39
        },
        {
            "id": "997a9472-19d2-48ef-bdf0-19b38cc43650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 40
        },
        {
            "id": "a58647a6-07d4-47c1-96c2-0318b111aed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 41
        },
        {
            "id": "0dd9b5b7-4348-41e1-89be-b0ed0e32e7e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 47
        },
        {
            "id": "3aed690a-13d1-419f-b888-43d968b4ef7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 48
        },
        {
            "id": "d263eabe-892b-4220-9f29-c5bc85526a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 49
        },
        {
            "id": "f028b930-4ef0-4d8a-91d3-123e83bb9071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 50
        },
        {
            "id": "2e4e5480-a891-44db-a879-2b349985e741",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 51
        },
        {
            "id": "b97363fe-de65-45e0-b22e-6e6338c51e83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 52
        },
        {
            "id": "178289aa-53f7-440d-9aee-a7fd2d8215ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 53
        },
        {
            "id": "b8f774d4-2d58-442e-960e-377b9d6e0994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 54
        },
        {
            "id": "b6f49bfa-7b4a-4dab-b0a8-e98a84180618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 55
        },
        {
            "id": "d7161f72-86b9-41e8-9227-438c73b22c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 56
        },
        {
            "id": "9b9546e0-5e45-45f1-b50f-c711a6a2aa83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 57
        },
        {
            "id": "4853a9b2-c018-4f99-96ef-e5e30a571357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 58
        },
        {
            "id": "bf975830-7d54-431f-8820-a11c11d8876d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 59
        },
        {
            "id": "adef315e-77bf-41f6-8bec-c6777d87f8d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 63
        },
        {
            "id": "4631b04f-7465-4553-ad19-44882c40c511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 64
        },
        {
            "id": "2dee3bfa-c289-470f-8291-acf4ee435aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 92
        },
        {
            "id": "cf8d02f4-6701-4b55-8d7c-28e46e2df1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 93
        },
        {
            "id": "45344000-d22a-4065-9c7a-053cea553b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 98
        },
        {
            "id": "49714807-d491-488e-be16-36ae4c8a3836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 100
        },
        {
            "id": "a5c0bf67-9c38-4007-be56-ec71a547ab6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 101
        },
        {
            "id": "d703b060-60a5-4f96-8d25-810a43d9bcce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 102
        },
        {
            "id": "62578b03-5a26-411b-af47-dcb427afe4c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 103
        },
        {
            "id": "25e9a5f0-f12d-469d-8df7-1955bfd81b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 104
        },
        {
            "id": "6fae8d28-6b8b-4742-bfc8-d33ba2c21d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 105
        },
        {
            "id": "a50b2c19-7eab-4d03-82fb-346bd8a41c78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 106
        },
        {
            "id": "10c0c0e2-1dc4-459b-ada2-d7726e87f5f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 107
        },
        {
            "id": "db730060-642a-4e39-b27f-0062aef253d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 108
        },
        {
            "id": "990bb58a-2c2b-4ec7-b8e6-fe437759cfc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 109
        },
        {
            "id": "32185e08-cd11-4410-8f67-29c4abf97be0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 110
        },
        {
            "id": "fc32845f-dc2a-492f-bfb3-d24e4d3fa0b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 112
        },
        {
            "id": "ff3c388f-207e-4848-a833-b407206207d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 113
        },
        {
            "id": "90a5eaed-9b9a-4334-adfc-6f4f6d671488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 115
        },
        {
            "id": "1f5cf532-0a23-4f47-97f7-0d568ddafbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 116
        },
        {
            "id": "073bff86-6963-4b92-8f4e-1d52c627c6ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 118
        },
        {
            "id": "bdb20c2e-aab5-4ed3-b688-b4396fb5f64e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 119
        },
        {
            "id": "bab13ef2-4be5-4a8b-8243-787b9601df73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 120
        },
        {
            "id": "73a57c51-2103-4f42-9656-d0bd8d83c7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 121
        },
        {
            "id": "8a7438a5-03dd-477d-80cb-783fd7b66350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 122
        },
        {
            "id": "6b168306-6e28-4b6c-9f00-7fd07927043e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 894
        },
        {
            "id": "642d4154-d897-4c5a-b74e-f9e209fb1314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 33
        },
        {
            "id": "0c6098d9-c3d9-4fa7-8dfd-cfb41b3131a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 34
        },
        {
            "id": "5e568599-6971-4867-abbf-3e8c4cadfbef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 35
        },
        {
            "id": "dc7c1076-8c90-4956-a1c6-28933e55071c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 39
        },
        {
            "id": "a65f5c25-3925-46a1-a090-5b73eff1bfa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 40
        },
        {
            "id": "ba84db75-15cf-46b6-8e00-72dd3bbaa97b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 41
        },
        {
            "id": "a01ce523-33a5-44a4-af32-90cc59ea928b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 47
        },
        {
            "id": "79ac121a-375b-4d6b-9a8f-e89720b6269a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 48
        },
        {
            "id": "ed62b41a-f6bf-492d-ba8b-e626f48bdd9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 49
        },
        {
            "id": "63443086-d732-41fb-9adb-9bb488b69d84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 50
        },
        {
            "id": "49137cff-276e-4b3c-81fc-83b0f2d025ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 51
        },
        {
            "id": "c5de6b92-86ec-4acb-bd66-bbdae98c2b37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 52
        },
        {
            "id": "c831d23d-e21e-4cbd-bde5-ae4778fef0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 53
        },
        {
            "id": "3f260ecf-b856-47a2-a629-a2526863e39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 54
        },
        {
            "id": "919aab05-278b-4f16-9b39-98179f34d400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 55
        },
        {
            "id": "9c9acf00-9ccf-4806-955a-5540fb54b9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 56
        },
        {
            "id": "0497a59c-af95-4733-9476-fd5afa787d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 57
        },
        {
            "id": "f3332ff6-faba-4185-bcd5-f37701c75c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 58
        },
        {
            "id": "720ec2e2-ee68-476d-ab9d-706681236ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 59
        },
        {
            "id": "b408bc6a-2197-4572-aa4a-344669635182",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 63
        },
        {
            "id": "301ea3f5-16cd-4926-85af-d94a07d186c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 64
        },
        {
            "id": "462f0069-0006-4c7d-a8a9-7ecc6547f1a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 92
        },
        {
            "id": "ca38c682-6989-4b7c-b316-ff9681b2d5c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 93
        },
        {
            "id": "709952ae-00f3-4685-9747-471967ea52b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 97
        },
        {
            "id": "ff139d3a-9f63-4a30-86b2-63ad1b1ceee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 98
        },
        {
            "id": "6d91b617-581b-498c-a879-09cb49d48c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 99
        },
        {
            "id": "13c42b8d-efbc-4c5d-b856-9980d7180de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 100
        },
        {
            "id": "79eb3cfe-9aeb-4595-a381-f7e4a462fdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 101
        },
        {
            "id": "75bef1b0-946a-4bdb-8764-dce790f1e63d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 102
        },
        {
            "id": "b6be9e54-7615-4efc-a367-7db4a4f7e0a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 103
        },
        {
            "id": "424d7edc-a9e6-4f70-a2be-10f779b498f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 104
        },
        {
            "id": "46e1d1d3-86e2-4950-8e1d-f4f90cfddb5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 105
        },
        {
            "id": "300b44a2-c2dc-4dc6-bb6d-e5fa45f8b68d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 106
        },
        {
            "id": "f9abc68c-f960-4b71-abd4-2bc555374c1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 107
        },
        {
            "id": "383388e8-9f90-4ead-82cb-b2d50edf1255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 108
        },
        {
            "id": "8867b732-a349-4f11-85e2-0d3deadc53ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 109
        },
        {
            "id": "b9aa9c07-4503-46b2-838c-9a7cbf01b74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 110
        },
        {
            "id": "a0c17c8a-18b4-4148-93f9-346b0d6771cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 111
        },
        {
            "id": "ddcce84d-c296-4f52-bdb0-0e76503608aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 112
        },
        {
            "id": "4c1a69b1-33db-48cd-ad94-4a816cb72b47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 113
        },
        {
            "id": "afd87a67-ade7-4107-bab6-57714141a731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 114
        },
        {
            "id": "b4cf07e4-b186-48cb-85a6-d9a0dc55d60b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 115
        },
        {
            "id": "416d26cd-1d0e-446e-9fcc-eb8e6c250be6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 116
        },
        {
            "id": "c240fa45-f705-4983-a7c6-5499028a3265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 117
        },
        {
            "id": "85e05b53-73f1-4cc7-b84a-e375efda7afc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 118
        },
        {
            "id": "98a8a798-bfd1-44da-b6b5-ea8596fc21b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 119
        },
        {
            "id": "9f4191d3-3e8f-4fd8-be0f-16f6c36f0bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 120
        },
        {
            "id": "ab30effc-1e35-4d05-9c67-135cd01bbb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 121
        },
        {
            "id": "b6ec83f3-427a-436a-9922-dc0de6f5a857",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 122
        },
        {
            "id": "c650f499-78c6-4fe6-9041-5f0d027b9ebb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 105,
            "second": 894
        },
        {
            "id": "d6072d71-5e4b-484f-9151-9a07fd2d41b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 33
        },
        {
            "id": "59ed8bec-383a-44dc-9ebb-4b3565f6cb10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 34
        },
        {
            "id": "44695fd9-1a94-4283-b5d0-53b7661871a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 35
        },
        {
            "id": "f57888c6-1ef7-4255-a38c-e023089b81af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 39
        },
        {
            "id": "d7223a53-9cce-4d53-a87e-6ed9e181d3da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 40
        },
        {
            "id": "8ec16e83-c4bf-46c6-8a3e-ad1395699f70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 41
        },
        {
            "id": "b481f986-d245-43d9-8f99-02a3ef511703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 44
        },
        {
            "id": "ee22bba7-6b8e-4237-a8ea-00a042df42e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 47
        },
        {
            "id": "4050972b-4970-45ed-8ee6-d8f6c38c60dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 48
        },
        {
            "id": "687c7b78-aff6-4ec0-a0c8-bbebb9654816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 49
        },
        {
            "id": "aa626cc7-4bc1-43e4-aa08-7fe04eb45c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 50
        },
        {
            "id": "02d0a023-8ea7-4909-b904-322bfb00b9e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 51
        },
        {
            "id": "6f2cce2e-9aad-4699-b8a9-a8cd5be765e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 52
        },
        {
            "id": "48f5950e-0a6d-48d7-9468-5a98cdf33260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 53
        },
        {
            "id": "577c78fa-1cf0-4e94-8476-07396e5f0098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 54
        },
        {
            "id": "0047e903-8ffa-47ae-880f-234395e0a4fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 55
        },
        {
            "id": "df004ee2-427a-4675-963a-d96a787e034c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 56
        },
        {
            "id": "e36215b1-0273-444a-bd4f-4640355f8a57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 57
        },
        {
            "id": "d7fdd3de-8d45-46e8-840c-c1c39dd47d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 58
        },
        {
            "id": "995326b9-c23d-410a-af1f-a58ec8dbd7ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 59
        },
        {
            "id": "d78c2aaf-5e30-431d-ab23-43091b65230a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 63
        },
        {
            "id": "b5074e94-9187-4b49-8e35-f2b914bc376b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 64
        },
        {
            "id": "0c7dfa7a-b1af-4cf1-b38d-2a53464e4d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 92
        },
        {
            "id": "121a8879-8882-4cea-9e45-4b74a177d4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 93
        },
        {
            "id": "aa65f252-4556-4e09-88c0-7c5d77b5a073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 97
        },
        {
            "id": "1ce7cf62-0d13-468f-8d27-30c99957700a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 98
        },
        {
            "id": "3c0772ce-fd52-4f8e-8a63-c06bfc1876e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 99
        },
        {
            "id": "e31ff5a2-09b9-4197-8594-7d62fd152f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 100
        },
        {
            "id": "e35b2787-c81a-4383-94db-e961e5871ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 101
        },
        {
            "id": "34b77240-102b-469e-bcad-7684b9b7d400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 102
        },
        {
            "id": "7d50ac26-92cb-43e1-8769-df07b330162e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 103
        },
        {
            "id": "c073d630-f393-4cd2-b895-be8109d97fe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 104
        },
        {
            "id": "3e0995b9-2224-40ee-a875-085db87cd9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 105
        },
        {
            "id": "f0832cd9-da5c-434b-abc5-a9ea55ce0731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 106
        },
        {
            "id": "d3374ca7-8af0-4b94-af78-589accc8dd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 107
        },
        {
            "id": "89f8be3b-164a-4d98-9c83-105a95ce0553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 108
        },
        {
            "id": "22c07774-06bd-4bc8-9510-e222646d3787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 109
        },
        {
            "id": "ad4773b0-14c8-4736-a048-5b49a09d664e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 110
        },
        {
            "id": "a26565a5-c9c1-4cfa-85f3-7fa0210a2e99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 111
        },
        {
            "id": "424b3f45-5b9a-4d0c-9761-45fe6a4adbd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 112
        },
        {
            "id": "ea0ffdf7-4742-4a9d-af5e-fb92e6ebf79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 113
        },
        {
            "id": "9e37a09b-909b-4470-8998-b97f181527b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 114
        },
        {
            "id": "08e5276d-702f-4533-96cd-c7925f8eb072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 115
        },
        {
            "id": "d3861b15-2ad0-46f8-8a70-839c0aacb8f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 116
        },
        {
            "id": "71bf4c91-fe54-442b-8362-9dee06435033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 117
        },
        {
            "id": "34c57116-92cf-4168-9c29-01a2bb4e0eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 118
        },
        {
            "id": "ba1021d9-56ff-4adb-b2ea-157acb411270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 119
        },
        {
            "id": "1d0e3907-099f-48dc-ab25-5adcf5386e1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 120
        },
        {
            "id": "c17dd8f7-5ccd-4dbc-b33c-ea5b7690fca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 121
        },
        {
            "id": "3b460876-cae8-4ce8-bfca-6ef6c729ae96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 122
        },
        {
            "id": "eedaa050-888b-472c-97de-595fecb9970e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 106,
            "second": 894
        },
        {
            "id": "18e7a461-1b11-40c3-be28-1c2098035d2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 33
        },
        {
            "id": "ac26ff21-8485-4039-8de4-b536828f749e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 34
        },
        {
            "id": "89d41f20-3e73-47a0-9a34-a2f3e285a2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 35
        },
        {
            "id": "5826834f-7c98-4429-9d47-dbcf23a735c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 39
        },
        {
            "id": "875360b0-850b-440c-84d3-ad0497d4d6fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 40
        },
        {
            "id": "ae5bfeec-b1a0-44f6-9dbb-55a387a1fbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 41
        },
        {
            "id": "716471fe-b241-48d5-a82e-ddaef25f0fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 47
        },
        {
            "id": "0836719b-79a5-4e4c-b7e5-284e8a980556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 48
        },
        {
            "id": "ed68896d-d4d8-44eb-a256-201b412053f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 49
        },
        {
            "id": "ec9e0006-7d2c-4186-9fe8-870b536eda8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 50
        },
        {
            "id": "335ecaca-5586-4e05-8e0b-a3e71b16861a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 51
        },
        {
            "id": "ba10feb7-448e-4ea3-b65c-4c32af646818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 52
        },
        {
            "id": "f19b064e-3407-4acb-9efd-7c85ccde663b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 53
        },
        {
            "id": "a73eb79b-58c2-4a60-b5e1-3992c21df1da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 54
        },
        {
            "id": "7140a0f9-18fd-49d3-97bd-0d5a2c7fe43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 55
        },
        {
            "id": "8853433b-2c25-4c70-be4c-6eea595c8f60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 56
        },
        {
            "id": "24cbcf1d-161a-42c3-82f8-a6fdc59817d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 57
        },
        {
            "id": "3efe200f-8054-4e82-bfe8-e48b58010ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 58
        },
        {
            "id": "49701cd9-a994-4324-a30d-316c68d8ce7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 59
        },
        {
            "id": "396e169f-0aa6-4887-9934-5fc4f29bdd74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 63
        },
        {
            "id": "f96bc6d0-fccf-483a-bad7-0c5fb4154048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 64
        },
        {
            "id": "c809bc85-5d9a-4732-a695-68731f13eead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 92
        },
        {
            "id": "973c2b78-b7d3-4fc1-a848-e3f4e5d4eb66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 93
        },
        {
            "id": "c5e91356-855b-459a-831f-70cf35057bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 98
        },
        {
            "id": "535248db-828c-4a84-9047-8e164981dfae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 100
        },
        {
            "id": "c53ee256-f18b-4958-af1d-246066cf1590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 101
        },
        {
            "id": "369cad3f-e6b3-4be5-8507-5da141c0cd7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 102
        },
        {
            "id": "c41c0794-4405-431a-ae4c-83e9a03e011b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 103
        },
        {
            "id": "339dfec4-1007-4d6a-bd6f-13ca4696d5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 104
        },
        {
            "id": "29ddb786-79c8-4d3e-b6ac-eb23c19eb5d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 105
        },
        {
            "id": "a0b9ee1c-f723-4f2f-9e63-0f9466c73483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 106
        },
        {
            "id": "44d7829d-85d5-43c1-9a92-fc5534c5ffb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 107
        },
        {
            "id": "6aa09508-972e-4708-933a-e0a1d16dd50a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 108
        },
        {
            "id": "eae66bad-6da0-4973-9cb3-7e85d2ecb849",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 109
        },
        {
            "id": "5588094a-2114-42cb-a44e-77ff1ad79f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 110
        },
        {
            "id": "211816b3-d6f7-4097-a0da-8c0ae61e5ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 112
        },
        {
            "id": "d62b9111-747d-4838-bc91-7c510cba61ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 113
        },
        {
            "id": "2fc3473a-88fe-47b6-b1a2-ad236618f21c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 115
        },
        {
            "id": "797f998b-1a07-447a-b6a2-d9a8e1a5a90f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 116
        },
        {
            "id": "6698af7e-7a28-4fd9-846b-7bad223af8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "13faa4b3-9820-4804-b288-643b4ae53e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "e8392d3f-9da4-4694-abf5-dcc569e9cf76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 120
        },
        {
            "id": "bfae8b92-eded-41d1-9376-58c2506c81a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 121
        },
        {
            "id": "ec5111a6-c79c-469a-baa7-97e2787b8784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 122
        },
        {
            "id": "cac86e90-4139-4167-9351-3e6079b1ae11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 894
        },
        {
            "id": "953993ce-d9ee-45cf-bba2-04957e598200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 33
        },
        {
            "id": "8122047b-1b74-40cc-9af8-1dbb84489bcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 34
        },
        {
            "id": "d37a1f45-7d73-4d41-8607-35f173ba8e92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 35
        },
        {
            "id": "e9239aaa-912e-4792-bba4-492c13b9471a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 39
        },
        {
            "id": "a47f0a22-4731-4d58-9696-2d5b3e73bf5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 40
        },
        {
            "id": "0a6f8784-3399-4c81-b155-963bc73691c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 41
        },
        {
            "id": "e631588c-7a3c-431a-96d2-4f2a6e5d3d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 45
        },
        {
            "id": "e853d7bf-0214-4abf-a01a-55e6dbbb647f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 47
        },
        {
            "id": "4cf69020-e7e8-4a48-9bba-3ec86ac229ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 48
        },
        {
            "id": "b377a7ed-7d55-4bf5-afe2-1846fdbd3a0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 49
        },
        {
            "id": "1df922d4-073a-4aaf-b899-0910fe8a0d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 50
        },
        {
            "id": "6735c6ed-4d1f-4bca-b544-62e06af1941d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 51
        },
        {
            "id": "393ec13a-ed8c-432f-9d73-a40da076080b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 52
        },
        {
            "id": "b8ac97df-db39-45b8-883c-4322e63f70d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 53
        },
        {
            "id": "5f00a7d6-fc1f-4a25-ad6b-145a727664e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 54
        },
        {
            "id": "4f4191cb-c355-466c-bc6e-b1273cab07b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 55
        },
        {
            "id": "a18c7c1d-dd01-4073-a393-c7db83dfad84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 56
        },
        {
            "id": "ef424d53-e47f-458b-acbb-d6f681a4e02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 57
        },
        {
            "id": "814e09c4-7a72-4386-9df6-b201a3e4786d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 58
        },
        {
            "id": "52b8be9f-8584-4a2f-adaa-f1fbbeca6fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 59
        },
        {
            "id": "efd618ae-ef86-41da-b0be-f659e9e93bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 63
        },
        {
            "id": "b2188973-cd34-43a8-b1f5-f1aae36ed25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 64
        },
        {
            "id": "50c09501-77dd-4309-af49-2798e8600d1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 108,
            "second": 92
        },
        {
            "id": "313aa24c-11b8-4d31-9618-ea531336fa99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 93
        },
        {
            "id": "6b9d62a6-17b1-419c-b7f4-d1734dd8eed6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 97
        },
        {
            "id": "74f497f7-d965-41ff-8049-c218dc8473ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 98
        },
        {
            "id": "e4ec876b-c193-4749-8114-12b00a9a5e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 99
        },
        {
            "id": "71f6e5c1-8c4a-4bb9-a3f3-a752caff71d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 100
        },
        {
            "id": "c6d0f158-e10b-4784-9c52-f93a9fecf821",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 101
        },
        {
            "id": "b48e0ff5-d670-431d-8c24-3d55d256b156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 102
        },
        {
            "id": "0e5ab213-244f-43f6-b99a-0110240b08f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 103
        },
        {
            "id": "3fc2324b-6c2e-4350-8f80-c440be42dd05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 104
        },
        {
            "id": "b28c7b2c-f38a-4152-9035-af4134002f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 105
        },
        {
            "id": "a79a7118-fd1b-4506-8151-214282986790",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 106
        },
        {
            "id": "4a4ee1b9-f88a-4f9a-be14-782ebc1c459f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 107
        },
        {
            "id": "cb59e6e9-a082-421d-8354-68e4dd4a6283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 108
        },
        {
            "id": "e0269c20-960a-4de1-b162-6e19c5b799e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 109
        },
        {
            "id": "afcb2263-e70d-42db-a001-e4f9cbb722f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 110
        },
        {
            "id": "ade920a7-a4b4-4572-95cd-a994418b8ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 111
        },
        {
            "id": "7c178c98-b123-4906-b8d3-599d2e62ac72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 112
        },
        {
            "id": "9faacab3-58f4-403d-9178-b258da846938",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 113
        },
        {
            "id": "f20b58ad-de26-43e1-b7b2-ea9e4f73875d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 114
        },
        {
            "id": "c93709f7-aadc-4679-9c86-21530e5eb897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 115
        },
        {
            "id": "4fad52d9-84d6-4909-b6b5-b9bfd3a966f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 116
        },
        {
            "id": "f4aec499-1d7e-4edf-99a2-64bf6bf72eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 117
        },
        {
            "id": "88ca349d-8b8f-40a0-a5c7-0c00f95ecb17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 118
        },
        {
            "id": "b81aefd7-462d-4180-b0c9-61a1d03cfd61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 119
        },
        {
            "id": "d5b373fd-94d7-4945-890d-848dea3e3476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 120
        },
        {
            "id": "d17176e7-8887-45a6-96f1-413805b70f30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 121
        },
        {
            "id": "915b9e36-a17e-4bc1-9471-ad4d6e8b398c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 122
        },
        {
            "id": "67be17a2-b2b3-4a96-a61d-b7458fbe7db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 173
        },
        {
            "id": "7b0d1c61-824e-4cd2-b516-ce9078e0093f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 894
        },
        {
            "id": "89969b9a-6be1-4376-a72f-e2324eafc3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 108,
            "second": 8208
        },
        {
            "id": "17cceb3a-a3c2-4dd5-a9de-228108dd78cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 33
        },
        {
            "id": "816d8187-ff8b-44bf-ad10-89fca3cca51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 34
        },
        {
            "id": "a4d931f3-ec3d-49ed-959f-3d24a782aaed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 35
        },
        {
            "id": "1156344b-a071-47eb-8548-6fc8bf85f345",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 109,
            "second": 39
        },
        {
            "id": "1186356c-95ea-45ad-b8b7-8d8850c09fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 40
        },
        {
            "id": "f3f11855-62ee-46a0-a92f-ae7d39394c26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 41
        },
        {
            "id": "be179f4c-1451-476d-9bd0-78ca32d0c6cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 47
        },
        {
            "id": "cd456f42-fdc1-4ad4-9aca-9568e870f88d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 48
        },
        {
            "id": "a9c129a6-d571-407e-b96f-f769eecd726f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 49
        },
        {
            "id": "7296a58e-8dae-4f1a-9500-42cde7c044ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 50
        },
        {
            "id": "4850f57e-f25a-4929-8d17-4405bec372e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 51
        },
        {
            "id": "c4080e8d-aa92-4a2b-9450-794104eff4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 52
        },
        {
            "id": "bf40eafd-3e24-44a9-8682-6e9ec5005710",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 53
        },
        {
            "id": "0bbf9550-c6e4-40fb-b480-f5943714afa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 54
        },
        {
            "id": "9c3e5a1d-8821-4e30-ac35-9acb72d49932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 109,
            "second": 55
        },
        {
            "id": "c9516773-5c04-46e5-bb99-ca9d3b969122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 56
        },
        {
            "id": "94f32638-fe8a-40f5-a6ba-66c79d10ce41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 57
        },
        {
            "id": "a293073e-f240-4a52-9bc3-5c215e683ee9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 58
        },
        {
            "id": "4b1e02c1-12b0-4545-a6bb-84e8808a7d55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 59
        },
        {
            "id": "48272573-ffb4-4203-b3be-62bb1353c8fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 63
        },
        {
            "id": "f9cfa98e-eca9-40ed-bbbf-f658fcaddf58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 64
        },
        {
            "id": "2dcb3bdc-c54d-4278-bc4d-022f18ae5ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 92
        },
        {
            "id": "5c258ec5-fc3a-41e9-ba33-1dd227d45ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 93
        },
        {
            "id": "01b08e2b-19e5-45d7-88fc-884a4ac8cbab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 98
        },
        {
            "id": "9c86c9e3-2b50-48c3-ac87-b2613a6bcdec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 100
        },
        {
            "id": "5068babb-36dd-457d-9967-49047b493258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 101
        },
        {
            "id": "03547211-d443-44c5-a14b-64d28149a576",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 102
        },
        {
            "id": "15d643be-7f6a-4b07-bd35-ceb2514cfaeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 103
        },
        {
            "id": "ec2d40d7-b453-4598-b669-4c94988b6248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 104
        },
        {
            "id": "e19a6ff7-2b91-47d9-a791-6400d5242b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 105
        },
        {
            "id": "2ee148f9-1e4c-4473-95d9-9af6807b7e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 106
        },
        {
            "id": "46049cb2-5788-4e2f-8de5-7081595b8296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 107
        },
        {
            "id": "8af5eced-3928-49c6-a21e-f973dfbfeda4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 108
        },
        {
            "id": "2755ea69-acb0-480b-9df3-792bb7337ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 109
        },
        {
            "id": "1d0ae87c-7bc2-4cf1-9af1-deae96940f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 110
        },
        {
            "id": "636e7460-825b-4d30-a895-8d56d8e5dc49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 112
        },
        {
            "id": "bf0d1957-fb89-484b-b7cc-a4701eccd35a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 113
        },
        {
            "id": "2af54da6-666b-4ed8-bde0-03227048797d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 115
        },
        {
            "id": "9d5e3d5c-098c-4ae9-872e-ed0e048f2581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 116
        },
        {
            "id": "b69c99f9-94e5-4324-96e4-432515fe55e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 118
        },
        {
            "id": "225f71ee-e6bd-43de-a642-75ca75933b4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 119
        },
        {
            "id": "cc562f1e-a9ca-4086-96e5-f9c91152b8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 120
        },
        {
            "id": "4c010b88-975f-46ed-b52b-e2227564b9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 121
        },
        {
            "id": "f97363df-7983-4482-ad01-0815070629ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 122
        },
        {
            "id": "85611afd-b05b-4936-8dfb-fca270daed9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 894
        },
        {
            "id": "afd53218-e70b-4d01-b1ef-7932f34c5503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 33
        },
        {
            "id": "3192ac54-22a8-44b6-9b03-2b0df9e12b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "2318cd33-29c5-4bfb-8bde-8e1754e1f57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 35
        },
        {
            "id": "140fe605-5640-4ba9-bcec-659377bb8959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 39
        },
        {
            "id": "15c7fc5f-0490-44ce-b89d-7e45795f9815",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 40
        },
        {
            "id": "f3e2ca66-63a6-4b42-8294-e72be4c18b5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 41
        },
        {
            "id": "6e4b90f5-8f6e-41f3-bc42-1658d9d545ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 47
        },
        {
            "id": "043fea26-8e27-41a1-81ca-6c3cbd77007b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 48
        },
        {
            "id": "c74906b1-5e13-49e4-85a9-ad2c8c7b1bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 49
        },
        {
            "id": "e6af9744-5687-462f-bdff-ac7ddd0c6b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 50
        },
        {
            "id": "8fb96919-4f43-4459-b178-254da0443688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 51
        },
        {
            "id": "d4f36c30-c9ae-4ed6-93eb-d10a7a1f409a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 52
        },
        {
            "id": "d87a375e-dc31-47e8-aef3-b421ff24a7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 53
        },
        {
            "id": "a6abeeaa-3459-4ac1-a20e-ca600db8ab4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 54
        },
        {
            "id": "c6ccc855-7c09-45a1-b4da-084baac94e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 55
        },
        {
            "id": "ebdfdc41-f794-4aba-9ff5-e053be73705e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 56
        },
        {
            "id": "e2639c42-1c15-4990-a4bf-3b2a8b905f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 57
        },
        {
            "id": "f1854396-3185-4556-a473-bd1b575b4324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 58
        },
        {
            "id": "1280c430-571b-4d67-9e1c-3efa81584184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 59
        },
        {
            "id": "52231a3f-aa3a-4a37-b1c2-a418ebd2c562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 63
        },
        {
            "id": "f5d6af7e-caf9-4987-b95f-85f2ea273f57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 64
        },
        {
            "id": "7ffb3d19-e198-4f65-bdf0-c87d2a6568ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 92
        },
        {
            "id": "f74c6ff2-6242-4f07-bf62-0a9121655af4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 93
        },
        {
            "id": "1bff481d-3476-44db-bdf8-f8fff874e8c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 98
        },
        {
            "id": "a5f4b5bb-da63-4abd-8971-c6e9f89dc2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 100
        },
        {
            "id": "cbdc199d-e92c-4c99-9bee-8af0527f3269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 101
        },
        {
            "id": "f0fe8c9d-298c-4713-bf4f-be793a56e04c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 102
        },
        {
            "id": "fb5cfd58-488e-46f7-864e-23ed5db768be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 103
        },
        {
            "id": "15769cbe-8e2c-4fe3-8d4a-ae0fabb49162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 104
        },
        {
            "id": "41b524b5-97bc-45f7-ba3f-c822b3fa0550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 105
        },
        {
            "id": "e1175018-dada-47fb-b7de-ff50f4255d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 106
        },
        {
            "id": "e1cfc20d-2b72-4b2a-8b05-0ec51b781aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 107
        },
        {
            "id": "035c5733-f007-4d9b-8419-844a7e1caa63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 108
        },
        {
            "id": "ab05e97e-adc9-40e3-baec-edb91d22e120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 109
        },
        {
            "id": "4513d3aa-99f9-4633-9f85-d6b70ded7b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 110
        },
        {
            "id": "37291e20-3cfb-4691-92db-d95b02133b82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 112
        },
        {
            "id": "97729b16-8c93-4cd7-a57d-04af85594ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 113
        },
        {
            "id": "fae21655-cd4a-4bcc-9fe9-82c8b09d3bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 115
        },
        {
            "id": "df617c48-68e7-48f0-bb20-3601c0f5f9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 116
        },
        {
            "id": "03d1556b-6ac6-418a-abf2-6b83bd4e8a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 118
        },
        {
            "id": "75d01b2e-5595-4c37-82f9-9420b3eb5a58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 119
        },
        {
            "id": "abedeaa5-651a-4bb7-8602-8a96d1d7197e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 120
        },
        {
            "id": "96978d72-9a45-42b5-9965-cfef83c12a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 121
        },
        {
            "id": "587cd99e-d397-49b6-9041-f08f3e12359e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 122
        },
        {
            "id": "1ace8db3-5435-4d43-89ad-76c1194c818b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 894
        },
        {
            "id": "a6b4e31d-1cc1-41c9-b905-9c88f1bd00db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 33
        },
        {
            "id": "9cf4a4e7-17ef-4bb9-a271-79f9c14a742a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 34
        },
        {
            "id": "fc8ece62-17e9-4b66-89fb-91b09eb72ab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 35
        },
        {
            "id": "328a1965-171b-491c-8794-8dd32a640370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 39
        },
        {
            "id": "47302f50-07d0-4a51-a269-32b8bf9a631f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 40
        },
        {
            "id": "717ebd03-c3b6-4dcd-baf3-1e2710ee1d5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 41
        },
        {
            "id": "f8b96ba5-1601-4414-a9c3-c0329256e962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 47
        },
        {
            "id": "94da09e5-a97a-4167-af80-188437e07248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 49
        },
        {
            "id": "67c6359f-0db4-4478-9ca2-666a7fb372eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 50
        },
        {
            "id": "528a049f-b420-404e-bcd4-bbda2b10c1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 51
        },
        {
            "id": "79cbe013-4226-40f0-8bf6-03c622d35066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 52
        },
        {
            "id": "be312157-6927-43ad-8a47-05e8493d08b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 53
        },
        {
            "id": "6bed95d1-c65e-438e-bdab-b1ace94b8d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 55
        },
        {
            "id": "87d7df70-4631-4bf1-90a9-7c000a739c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 57
        },
        {
            "id": "d2f8101a-2800-4230-9d52-35ca45361c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 58
        },
        {
            "id": "bcff23e9-dc0a-419c-92f5-6d1412c4e364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 59
        },
        {
            "id": "5f8801a7-6bed-49f1-90dd-d4094ed09cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 63
        },
        {
            "id": "afdd6ccb-9eca-404b-b269-7038a49cf045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 92
        },
        {
            "id": "a18e328d-f56c-4c40-8949-ad0271a162cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 93
        },
        {
            "id": "10368c87-29b5-4d2b-919e-faa8dd7ae9db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 103
        },
        {
            "id": "b2a6cb39-840b-43f4-aae3-e0bc1be4bce6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 105
        },
        {
            "id": "8f37342f-38f5-448e-93fd-29b53ec27b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 106
        },
        {
            "id": "918a32c6-6a08-4f04-b35f-b2fc27c23cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 115
        },
        {
            "id": "e0f8ff6a-c76a-4c85-975a-2eb5c3e05a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 118
        },
        {
            "id": "42fdc304-cf29-4062-a90a-f3145b329471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 119
        },
        {
            "id": "d6eed606-8f12-416c-bf5a-cc203196b0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "8a6bd49d-8037-4675-9a86-84176a67a3a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 121
        },
        {
            "id": "6a8430d9-84b6-4e4e-ba6c-1287c5010a0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 122
        },
        {
            "id": "5c23021f-b7a3-4993-884b-00972146312a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 894
        },
        {
            "id": "3fd07375-2394-4e2d-b74f-62d08c695afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 33
        },
        {
            "id": "33e68000-41c8-4a8e-938d-94b4f872a45f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 34
        },
        {
            "id": "f51c643d-70de-4ac8-b974-ff2897757760",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 35
        },
        {
            "id": "68360fea-94a7-4b95-9dce-17ebbf53e004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 39
        },
        {
            "id": "2f58c80e-cd7c-4d49-a102-e4f827300cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 40
        },
        {
            "id": "3c663437-67a7-4913-91ca-bb19436fc7cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 41
        },
        {
            "id": "164c81a9-9e34-480d-a594-3011af2cc712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 44
        },
        {
            "id": "e613fd7b-7f25-4936-abf4-745685be759a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 47
        },
        {
            "id": "d39612cf-7874-43ea-ac4c-4feb75474842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 48
        },
        {
            "id": "ac8dcce5-f3fa-4cf8-a1c7-894ae05f5aa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 49
        },
        {
            "id": "c99a6a47-a7ba-41bd-9ad6-ee7cdb4a6964",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 50
        },
        {
            "id": "89370258-2b77-4b27-93d5-28f8bf8a55d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 51
        },
        {
            "id": "2503d41b-41db-4b54-9e90-8c05835e4386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 52
        },
        {
            "id": "603761bc-366f-4c3a-a64d-227b62782d0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 53
        },
        {
            "id": "f52fbe7e-5124-4567-b40e-5f17da7afb29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 54
        },
        {
            "id": "fd5031a9-9ecd-405a-8681-180646866151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 55
        },
        {
            "id": "b08fcd38-a131-4fae-8cef-f4b00cf49447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 56
        },
        {
            "id": "1fb29403-a2dc-428b-95a1-6d93db619e57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 57
        },
        {
            "id": "0d795f5a-9d70-4af8-83b4-450c548089c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 58
        },
        {
            "id": "68de3706-0f06-4cf8-9072-085b2796cf5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 59
        },
        {
            "id": "8ff2df30-d741-4a69-a45a-a26094e6c9a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 63
        },
        {
            "id": "60907f9a-e39b-414a-8060-afd2b7f09862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 64
        },
        {
            "id": "c9056365-d739-4019-92e9-1d462028f427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 92
        },
        {
            "id": "2b3f0a14-228f-49c6-a5b4-bcabe78b42ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 93
        },
        {
            "id": "c0c2f4cc-d089-4be3-b123-43454ffb9292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 98
        },
        {
            "id": "eb042998-64f2-400c-95fa-2713f20adb9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 100
        },
        {
            "id": "5ebcc40f-9725-4110-b948-2e3869104284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 101
        },
        {
            "id": "0ffc01e7-408e-4983-a9de-0b160a111ce7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 102
        },
        {
            "id": "d7f5ae90-8fdf-48b7-8eef-e13e820f0138",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 103
        },
        {
            "id": "52dbd7d4-fbbe-4882-9317-b9967d943683",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 104
        },
        {
            "id": "bff51fba-94b4-4e5c-874f-ed9745949f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 105
        },
        {
            "id": "afa4ff02-e0b2-4201-b7a0-cffa802fb398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 106
        },
        {
            "id": "8b175175-17b5-4c58-b8b1-76e3d31b8c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 107
        },
        {
            "id": "572b4f34-5e52-4db7-9b3f-6266018759e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 108
        },
        {
            "id": "82a0d88d-1be4-42d9-9d30-0f9d0898266a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 109
        },
        {
            "id": "800654ca-4af9-4e15-a562-a5822b8d9c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 110
        },
        {
            "id": "c404f22d-aa36-4e52-99d8-a89a98a2d4f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 112
        },
        {
            "id": "234a48de-0499-4b05-b30e-43953d2a9fab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 113
        },
        {
            "id": "3e0d5cdc-9344-4258-828c-2b89eed2cf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 115
        },
        {
            "id": "323786ac-0567-4bd7-8668-9ffac3a9be6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 116
        },
        {
            "id": "98f5ff31-d42b-403c-aa7a-308257b2dfea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 118
        },
        {
            "id": "515d1d8a-7fc3-4ba0-9c6e-5cea787436ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 119
        },
        {
            "id": "b5b0a4dd-c202-4c18-8c45-bb8030f6863b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "cd5d2af2-2f36-40a1-bbd9-c27a4b2e63d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 121
        },
        {
            "id": "bb8c9a7e-582d-4cd3-b8bb-c7573debcda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 122
        },
        {
            "id": "868bb70a-bb23-4dbd-a3c6-09160340a151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 894
        },
        {
            "id": "953de22a-5ecf-43f9-b417-4d1e47647c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 33
        },
        {
            "id": "bbf99bae-9343-479f-b65d-71c58a958fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 34
        },
        {
            "id": "7fcceff2-9e96-4f63-af7b-65e60c654ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 35
        },
        {
            "id": "c87f8e26-1b5f-49ff-bdd9-3f6c22cb55d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 39
        },
        {
            "id": "63613e9a-0bd2-4d75-aea6-3061e46d5b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 40
        },
        {
            "id": "3846a194-9726-480d-b4d7-d62071db2d67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 41
        },
        {
            "id": "a8c36661-bad9-4f1e-909e-8d773a491e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 44
        },
        {
            "id": "09a316dd-ca77-4ab5-b820-24ebfa109425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 47
        },
        {
            "id": "631d86be-2f5e-4bc4-af57-c8de5917c7f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 49
        },
        {
            "id": "771d221e-2635-4a3f-961f-94e8532e81c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 50
        },
        {
            "id": "b7985c6e-15ec-4cbf-a55d-5daec9c09d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 51
        },
        {
            "id": "00983ccd-edfe-45b5-9eef-7ec052cbc0dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 52
        },
        {
            "id": "aa3803d7-737b-4c3d-8e74-eef8332b4f20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 53
        },
        {
            "id": "60c391b9-1562-442e-bda9-8b587bf14cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 55
        },
        {
            "id": "053c1ae0-9fb8-40b4-950c-469a5517b276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 56
        },
        {
            "id": "5a884181-0fbe-4686-9094-0c0fabb5c6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 57
        },
        {
            "id": "d5774a1f-8d98-4374-8fca-375c8da159cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 58
        },
        {
            "id": "f7ec9237-9c6a-4156-922d-320bb1c98f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 59
        },
        {
            "id": "4e69b72e-b393-42b5-aafb-82f2bae18e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 63
        },
        {
            "id": "be78bb2b-1c26-443a-9e3b-077ba2379291",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 64
        },
        {
            "id": "d54baece-e3af-43ed-a2e7-7af7c67e6b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 92
        },
        {
            "id": "958e88a8-44ef-45da-9404-c4e0badfca34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 93
        },
        {
            "id": "84856e1b-2fd8-48d8-8dee-ecb86dbbf38d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 100
        },
        {
            "id": "a31524b5-9f3f-405b-af3e-6d1e6c39c8b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 103
        },
        {
            "id": "9ea901e1-4ce6-46c7-919e-3b8a11d08fbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 105
        },
        {
            "id": "c6059c07-6111-48d3-8ad1-e3f4bd9dc350",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 106
        },
        {
            "id": "32959905-60d1-4270-bac6-39e4d9930552",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 113
        },
        {
            "id": "eaedf8da-9904-4932-acd5-a4f4bac19520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 115
        },
        {
            "id": "0604d19e-3d1d-4b42-af06-d595cd2c04f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 118
        },
        {
            "id": "ed9fe9d8-e87e-4998-b663-7aeb3297dc3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 119
        },
        {
            "id": "c7085206-eb0f-42a6-a2a7-5f992cbae0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 120
        },
        {
            "id": "07d14246-4851-46fe-b180-614f84ce332a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 121
        },
        {
            "id": "5f64107f-bc45-4ecb-81e6-d656ed316c9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 122
        },
        {
            "id": "b1e1e0b9-1e67-4ff7-b4e1-8005851702f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 113,
            "second": 894
        },
        {
            "id": "91bc981a-0e9b-4852-b9f0-ae9e72043f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 33
        },
        {
            "id": "e6ce7f8c-49c2-4a9f-9c8c-a3fef13dc448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 35
        },
        {
            "id": "bcacd4dc-4f74-4fff-a50e-dbfd3a1e7e10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 40
        },
        {
            "id": "d4bc3ca3-548e-4180-bd46-5455c8844cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 41
        },
        {
            "id": "45225dc4-e5df-47e1-8082-002ff69855d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "d2b9bbfd-2549-47ce-8c09-c0122fa95a00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 45
        },
        {
            "id": "6d8c0134-ae41-4146-8a42-cad35cda525a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "a0cd3282-56c0-4289-8763-8e2c9429fe79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 47
        },
        {
            "id": "0fbee064-ddd8-4e64-b5d8-8533d61a90d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 48
        },
        {
            "id": "ddd42074-ed0e-4542-a3cf-b9a420cd2175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 49
        },
        {
            "id": "03534928-170c-4fa6-89eb-a387c7064cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 50
        },
        {
            "id": "25fea363-0634-4626-9d70-bc0fb2a4dc62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 51
        },
        {
            "id": "219605ee-6904-41fb-8eca-b697abe3e9de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 52
        },
        {
            "id": "40314a52-95c8-4d38-97ab-c48f5cdbd566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 53
        },
        {
            "id": "38e0e526-b7a7-4154-871e-7395bfb4cdaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 54
        },
        {
            "id": "b6e6e98f-30c2-4db8-a47f-12fa703a53d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 55
        },
        {
            "id": "bb609c87-f813-40d5-88cf-6b6bd734b3e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 56
        },
        {
            "id": "02473d19-1f09-49b5-89f6-92a6444788de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 57
        },
        {
            "id": "1e56511f-86b1-4d96-a9f1-6e77cb7fd83f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 58
        },
        {
            "id": "8d83546e-9476-4530-baaf-ecc8ea7cafb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 59
        },
        {
            "id": "33329ce7-9519-4ace-84c1-37290b237e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 63
        },
        {
            "id": "61011553-20ef-485e-bc8d-2f80b56150ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 64
        },
        {
            "id": "e9cbbfc7-d62b-4bea-9c63-36d2a589361d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 92
        },
        {
            "id": "92ff7f8f-0f14-4b83-afc2-090ec17d1f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 93
        },
        {
            "id": "b681a853-feba-4871-ac51-7d45297756ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 97
        },
        {
            "id": "ec1c3670-6f6c-40e2-ad70-0b785bb9220d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 98
        },
        {
            "id": "96c62872-688d-4784-be09-e1e0c34f1769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "5c820b50-3f62-4edd-9731-bc60a589db0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "dfd7fd3a-c5f7-466f-89e9-3621ca56daae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "b7cfdd22-cc89-4110-bb7f-064ee128b215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 102
        },
        {
            "id": "8ef2c40a-2d3b-416e-a245-2b25712f9365",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "9a3cb518-ddb5-4fb2-9d86-df32d8c65136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 104
        },
        {
            "id": "1d824eb0-d17d-4d11-8101-3a9ff83e7036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 105
        },
        {
            "id": "dd689cf5-3198-4039-860c-b43be872b4c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 106
        },
        {
            "id": "98cba597-99f5-4ad1-826d-6068901c8452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 107
        },
        {
            "id": "04105059-3ee9-4167-9114-186d2e9f9bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 108
        },
        {
            "id": "fc06a6a5-0568-483c-b61f-0e4ac59f4ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 109
        },
        {
            "id": "c21e3492-e98d-4f2d-b759-a36c35f0c288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 110
        },
        {
            "id": "b6b845f9-d696-4c00-80bc-d104d6622292",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "b45101d5-4067-4f38-8e60-36ab03854946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 112
        },
        {
            "id": "81c66a57-69ed-459a-97e6-fe4dbc43af8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "70762829-ca26-4861-a763-d44d949660e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 114
        },
        {
            "id": "7a32d85e-b184-4c14-8fdf-69bdf79621ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 115
        },
        {
            "id": "19ee137f-fdf6-465e-aeff-30857131d9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 116
        },
        {
            "id": "e45b3749-891e-48e4-9c43-7278d4af0a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 117
        },
        {
            "id": "d6f9f68b-a020-4d0a-90fc-1a0838b524ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 118
        },
        {
            "id": "90a8d152-cdeb-4c93-a33e-6d40694c6738",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 119
        },
        {
            "id": "23ab3698-ab47-43e6-ac85-baf82a97ed48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 120
        },
        {
            "id": "edce9157-d246-4ab6-b7e9-e1c932b6ae37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 121
        },
        {
            "id": "50701e4b-83a4-4bbb-9ea5-d241878ad563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 122
        },
        {
            "id": "0e447917-285f-47e5-bb4e-be67391c08f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 173
        },
        {
            "id": "b517927a-2bae-4ce9-a853-742348f6f9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 894
        },
        {
            "id": "cdf715f5-9cc1-4743-9d2e-3def2f5ea089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 8208
        },
        {
            "id": "b001c993-5bba-40a3-93ab-995d14285b50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 33
        },
        {
            "id": "4fec74f2-be08-4288-85c1-c33a84b2355c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 34
        },
        {
            "id": "e677caab-0fcd-4dcb-b9d6-d6a0512ead11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 35
        },
        {
            "id": "537b7664-e6bf-4019-a6f3-b2689ab8e8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 39
        },
        {
            "id": "768de0da-d414-48c4-9fbb-fc1819f830ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 40
        },
        {
            "id": "554aaa7c-f241-4007-a31b-ccc03565b25f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 41
        },
        {
            "id": "9b0857a9-0b89-4810-a97c-ee746f9854be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 47
        },
        {
            "id": "b81f6e74-ce5d-4827-9769-eb59c01aa1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 48
        },
        {
            "id": "d5ea94aa-5ff7-458e-a1c9-5251ff0be8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 49
        },
        {
            "id": "163f41ee-41e6-49a2-9e43-90eb805bbbf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 50
        },
        {
            "id": "495578cc-86f8-4b0b-bb47-4adcbcd96210",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 51
        },
        {
            "id": "5065f7fc-241d-47d4-a8b0-fd5086f2fc03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 52
        },
        {
            "id": "1db1b154-b0a3-4e0e-8482-528da7ca9d39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 53
        },
        {
            "id": "729fa779-35f9-45cd-9ef2-0cc099fff939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 54
        },
        {
            "id": "6dc2c63d-b21c-4d7d-9912-55c779a744c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 55
        },
        {
            "id": "8c7247fe-8cc9-4124-bd9a-628ea995cf55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 56
        },
        {
            "id": "9f3678a5-eb99-4e69-9b60-c310e0657ca5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 57
        },
        {
            "id": "7af0ce25-30cc-4bff-8f39-a1c957bbae71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 58
        },
        {
            "id": "912e15b0-7a77-4dc4-8087-7dd0b485089a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 59
        },
        {
            "id": "fdc7aac4-5b55-4dcc-b39b-cd060d12fb4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 63
        },
        {
            "id": "9e8c5803-cbe8-4f8c-b943-e27c25a0de99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 64
        },
        {
            "id": "a9fdafa8-e9bd-4e91-b923-46c071a1b8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 115,
            "second": 92
        },
        {
            "id": "ed2eb89d-42d1-4977-a063-88a8ff1c4a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 93
        },
        {
            "id": "ac5ff8ea-a148-4f5f-9111-f3648178e9d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 97
        },
        {
            "id": "7db9f3de-6a55-4e87-96ee-f944c0660270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 98
        },
        {
            "id": "1f4ff9e3-777b-4870-aae4-720bda4b1c12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 99
        },
        {
            "id": "ef5b2beb-fbba-4c10-8e6a-aef2eeb6e596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 100
        },
        {
            "id": "e71d65e0-b22c-493f-a75e-6c3f016c86bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 101
        },
        {
            "id": "1fda00b1-6650-4d1a-811a-6c6aae07ae75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 102
        },
        {
            "id": "1368c6d9-372f-4b05-8f05-29697d27963f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 103
        },
        {
            "id": "30c85fde-b6b5-457d-bd1c-58465517deef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 104
        },
        {
            "id": "58062f36-fbdb-4cb1-82aa-d701ae3ab699",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 105
        },
        {
            "id": "66546246-8a26-428c-b710-07ba01013a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 106
        },
        {
            "id": "4b99b020-9a0d-453e-9653-c9b0dc3aef27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 107
        },
        {
            "id": "b2adb5e8-c62a-4210-b211-f35e8c7deec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 108
        },
        {
            "id": "30345b8f-34fa-4b2f-a735-ed2e690b3004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 109
        },
        {
            "id": "5cf5fffb-fc99-4d47-8e2e-07390a1bc9a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 110
        },
        {
            "id": "6465c94f-3398-491e-81da-59e278250299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 111
        },
        {
            "id": "d1e7210f-2a5a-4450-b795-be7269f8a732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 112
        },
        {
            "id": "30808cf9-a50f-4adc-a5fa-f438091e6f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 113
        },
        {
            "id": "0b460545-88f6-4570-97f4-462aec87b7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 114
        },
        {
            "id": "baca141e-fbde-40ae-a242-735ddb67d031",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 115
        },
        {
            "id": "a3085d47-48f0-40a7-960e-b247956dd0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 116
        },
        {
            "id": "9db12684-03e1-453a-8201-989b722c4d0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 117
        },
        {
            "id": "5cf8dcbe-3dfb-4bc5-815a-bd37fde4fafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 118
        },
        {
            "id": "5347feac-ea6b-4d1a-909b-1fda7280641c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 119
        },
        {
            "id": "94b414e9-a577-4ee0-9e12-74b8acc889a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 120
        },
        {
            "id": "c5e6e478-7544-4c5e-9c56-a8904b9ccca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 121
        },
        {
            "id": "216aa0ab-a298-4602-999e-06d355b383cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 122
        },
        {
            "id": "0b6d25d3-591d-4393-9d93-d52ea27b756b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 115,
            "second": 894
        },
        {
            "id": "2a3ed849-77c0-4364-aac9-a9189ce9bb57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 33
        },
        {
            "id": "2254f9b8-484c-4a71-a5eb-86c4f383803a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 34
        },
        {
            "id": "7cbe6421-5ffc-40db-be98-9b16d6f1af47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 35
        },
        {
            "id": "cd023a2a-2035-44b4-bddc-ce1ee21840f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 39
        },
        {
            "id": "e1ec2d44-51ea-439f-94e5-4e1be9f61f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 40
        },
        {
            "id": "c1b1ffdc-5df8-4911-837c-946a9e85c085",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 41
        },
        {
            "id": "08d68553-0adf-4cab-8e52-6d751fae2ef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "fea6e2a5-94bd-498e-87cb-613e4d4fce26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 47
        },
        {
            "id": "a51af02b-99c4-429c-83d1-f32e78601e81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 48
        },
        {
            "id": "09d67947-4d78-4710-96a6-af79da55c7dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 49
        },
        {
            "id": "df5ac164-2bb5-45f3-89b0-e570bc85abb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 50
        },
        {
            "id": "20b8e1c6-bb9e-4410-b6f6-37fe6adb53d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 51
        },
        {
            "id": "5d65c837-3e50-4177-b5e0-9593ecbea1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 52
        },
        {
            "id": "701739b8-e9f8-4bb0-8e11-af078dbb5cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 53
        },
        {
            "id": "bd2dd00f-0955-4fc9-87e2-c6c13c02e0e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 54
        },
        {
            "id": "90579237-b15b-4353-b489-7db0524b80f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 55
        },
        {
            "id": "6560bf5f-a596-45a2-9086-7738b1cf7863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 56
        },
        {
            "id": "b7cb8ab4-f1bb-4d3e-a1fa-274755f3b449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 57
        },
        {
            "id": "20c76fda-0be0-479b-8e12-c8036bcef156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 58
        },
        {
            "id": "7c59e476-15f9-48a5-9794-f97aced66a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 59
        },
        {
            "id": "a0ff1b4d-53a6-40e1-bab6-8779b8d9b40f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "5b7deb24-1c8e-440a-a339-42ff72dbf2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 64
        },
        {
            "id": "e9b7e28d-dfb8-46ce-a0af-fc80dc000720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 92
        },
        {
            "id": "fae64557-5576-44c6-95e4-f49021fa4192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 93
        },
        {
            "id": "d57b2731-689e-4637-8b63-b4c3074b8d3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 98
        },
        {
            "id": "25bde9be-345f-453b-abcb-2f2b0719ae92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 100
        },
        {
            "id": "ff0a6191-0eef-4099-bb89-d1943baad5a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 101
        },
        {
            "id": "1b9504b8-e304-4354-902e-a7b73c352851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 102
        },
        {
            "id": "bb385c75-4f2d-4a98-b313-bf42b8712da5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 103
        },
        {
            "id": "fd721ec8-d3db-4ce3-854d-0a74f5bb53da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 104
        },
        {
            "id": "691495db-160c-4ea7-be7e-8baea3318267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 105
        },
        {
            "id": "19e0bdd7-d4d4-4af8-993b-119c96726078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 106
        },
        {
            "id": "c9fa9b3e-22d3-4145-8556-fb3b8ef11ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 107
        },
        {
            "id": "b21841da-2a39-489e-99d6-c4a892ad3668",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 108
        },
        {
            "id": "36c40f96-504e-45fa-84c1-715984416d7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 109
        },
        {
            "id": "a9a40923-f292-466a-9bef-125c9d50c6c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 110
        },
        {
            "id": "c49b7608-1984-49b2-b1b5-54174daff6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 112
        },
        {
            "id": "feac4071-c4a0-484c-a3b6-a7649c038c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 113
        },
        {
            "id": "45796a8a-67f1-4103-810c-f9aadac015f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 115
        },
        {
            "id": "e2992621-90c4-43d7-bebb-9f509cfe6cc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 116
        },
        {
            "id": "5fdd5d5d-5f1a-49c2-bfac-c4a89b65ff91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 118
        },
        {
            "id": "f06b3de4-de15-43a9-a744-ecbd206fa1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 119
        },
        {
            "id": "12d6a11c-135d-4cd7-a47d-5fff35dc09bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 120
        },
        {
            "id": "2d1648b1-3975-466e-a860-bd3585e09f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 121
        },
        {
            "id": "7aac20df-6120-4c99-bdac-e92e6101d022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 122
        },
        {
            "id": "d117a8ae-e3ed-46e7-b677-26f780ad176d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 173
        },
        {
            "id": "7dd06341-0b89-4ff6-b24a-db368a314bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 894
        },
        {
            "id": "0e59f695-8ed9-4d77-84e8-56c642ddd86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8208
        },
        {
            "id": "48e2251c-3222-49f3-833e-3f1396e7759d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 33
        },
        {
            "id": "ee439797-fe1d-4d54-ab14-2ed9b04d9a01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "bd996b37-edd2-4a8c-99b6-2a01d440c040",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 35
        },
        {
            "id": "3d10955b-248e-418b-8f0e-001da84958dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "2b62dc5d-3c5f-4311-be2c-b27fdd3d4be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 40
        },
        {
            "id": "81e3f1d8-c998-485b-9b6b-30f59416b1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 41
        },
        {
            "id": "bd0eee50-0018-41f0-b182-670086d929bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 47
        },
        {
            "id": "2acd2270-7909-490c-b9b3-387310bad3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 49
        },
        {
            "id": "1dc3f89a-d9aa-42a0-a22f-93c971470708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 50
        },
        {
            "id": "e7692460-c1cd-4325-b812-f52599530196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 51
        },
        {
            "id": "ce075e05-f9fa-4003-a254-8fa928dea49a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 52
        },
        {
            "id": "95002f3e-9887-49f1-9238-33605b64952b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 53
        },
        {
            "id": "83628b9b-4772-4644-8c77-36005dd05dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 55
        },
        {
            "id": "14693d00-05e1-4a48-bbbf-bd331fc855a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 57
        },
        {
            "id": "17c6e073-f09b-4286-862b-07e5bad0df79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 58
        },
        {
            "id": "4cddb47c-107f-4d1d-9ae5-5384bf1f95d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 59
        },
        {
            "id": "33cca9ac-3198-481f-bebf-aef4c5afc0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 63
        },
        {
            "id": "09fa55b1-93ad-4830-a1b0-a9855e8f7d4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 92
        },
        {
            "id": "827f8f0a-c781-4834-b5ec-3478a0b99729",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 93
        },
        {
            "id": "f80521d1-43c2-47b4-8bd6-5f4e8a697f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 103
        },
        {
            "id": "a9b6640e-2583-4769-bb01-23ec0961c27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 105
        },
        {
            "id": "8cb4b4de-3e5d-41ef-b038-3b9f8dadb3f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 106
        },
        {
            "id": "4d36d137-054c-45d5-98f6-cd27333c615b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 115
        },
        {
            "id": "4f98b8ba-be31-4769-a7f0-11aaa483d633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 118
        },
        {
            "id": "823c1bc1-99e7-49e5-9af0-9aef7b09222e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 119
        },
        {
            "id": "dc0e9cfa-86a7-4d9b-8594-18e44659bd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 120
        },
        {
            "id": "e231e2d3-4d3b-49af-bb76-dc7bde5e9435",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 121
        },
        {
            "id": "481b97fc-e78b-43b6-9f0d-73b9252f2438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 122
        },
        {
            "id": "15672827-2231-4dc3-9d32-d644fd34f07e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 894
        },
        {
            "id": "5da80d9c-9dc8-4429-b2a1-89ac810249e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 33
        },
        {
            "id": "187b579f-0ebc-4981-bd7c-699618157464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 34
        },
        {
            "id": "0a02273c-2310-4ac1-a8d2-1b4a8d8bbf5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 35
        },
        {
            "id": "7203335f-6d3a-4f1e-b8c5-48d316198ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 39
        },
        {
            "id": "62ccbb32-2f82-4650-b912-3c27e8273b30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 40
        },
        {
            "id": "4012e224-64ed-4a56-8108-5e3734dc86ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 41
        },
        {
            "id": "04794bd7-8e3a-4fc1-bdc9-82da6b424c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "98b2deb6-6e8b-4093-857c-c80a0fb1ddab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "9ae6edfe-fd1c-4553-a539-2b2cf9315e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 47
        },
        {
            "id": "6665c48f-e1cb-4591-ae08-cffc75b05324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 48
        },
        {
            "id": "9758ef80-f2c3-4452-91d8-cab3ef0e157b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 49
        },
        {
            "id": "1544af0b-33f2-45de-ab06-f70cc26008bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 50
        },
        {
            "id": "fa41e194-b969-407d-bb78-5a9516bc198b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 51
        },
        {
            "id": "0107d691-bcd1-4b8e-b430-fb0697807dd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 52
        },
        {
            "id": "c1628879-0782-4e2b-84b6-d0afbbe8d86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 53
        },
        {
            "id": "43061379-f7aa-4f08-8db8-4097cf989883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 54
        },
        {
            "id": "cf05f29a-95d5-4001-97a4-5f7e2b2c238c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 55
        },
        {
            "id": "d9f6fb12-d1fc-4ad4-bd31-f07eae68e5c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 56
        },
        {
            "id": "b6e0d45b-1697-4d87-88cc-f04c5d924592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 57
        },
        {
            "id": "01ad3511-4241-4892-80ec-d425beee19e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 58
        },
        {
            "id": "e06ef068-7041-4332-857e-516525770a7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 59
        },
        {
            "id": "f2d16495-6a60-4c92-a721-e783d3d2db38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 63
        },
        {
            "id": "d8d5a54a-f1fd-4abd-9628-a06570f716ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 64
        },
        {
            "id": "51b098f2-8b97-4ed4-807e-09e6ba8f396e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 92
        },
        {
            "id": "1fbefc6b-c948-4528-8ac9-a14ce6133c83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 93
        },
        {
            "id": "0ecdc60b-660c-470a-92c6-8a9c9e1b4c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 97
        },
        {
            "id": "f3e59ec1-9567-48e7-b2d4-10654db0f68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 98
        },
        {
            "id": "1fc677c6-c9a7-431f-86b5-a942e9972289",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 99
        },
        {
            "id": "640589fa-89d0-4b49-93bb-97a10fe9f922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 100
        },
        {
            "id": "6fca2b0c-024f-437d-9fb7-98a94e294b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 101
        },
        {
            "id": "2384f78d-14db-43a4-9d49-a26bf61ba64f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 102
        },
        {
            "id": "7d19dd2f-5fc8-4ba3-a293-51f4b9183ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 103
        },
        {
            "id": "f8333786-ec9c-4c31-b4fc-f2ec6395be63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 104
        },
        {
            "id": "ededfbe0-bda8-43e2-8203-2c2eaa3561ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 105
        },
        {
            "id": "498b7dd8-c4c0-4cfd-8cfe-31f963a8ddab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 106
        },
        {
            "id": "6fce700f-3e11-4e80-8b82-dd31e96f4751",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 107
        },
        {
            "id": "d6fa1c6d-0cf6-4aa6-b97f-46f2423ad538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 108
        },
        {
            "id": "7fe0d5c8-7d86-41d3-8ea5-9af89827382f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 109
        },
        {
            "id": "b9671466-76f3-44c8-b7dd-5f38757bf774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 110
        },
        {
            "id": "6f741323-9ae3-47c0-9d22-b87338296b94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 111
        },
        {
            "id": "2addcedc-f8bf-4162-8432-acfa9e83fce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 112
        },
        {
            "id": "d3caa538-814c-44eb-a83b-6d5d0eee5e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 113
        },
        {
            "id": "8481bd9e-5c75-49dc-a776-4f1787babd45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 114
        },
        {
            "id": "f5cd0fa9-6512-4f2e-a516-8d499328fd88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 115
        },
        {
            "id": "a1b601a0-0949-4094-9b89-2a492794aeaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 116
        },
        {
            "id": "895e6d01-44f6-4dc9-9a45-e6977a1092e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 117
        },
        {
            "id": "e7941a1c-247a-4d03-be70-629f8de87e73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 118
        },
        {
            "id": "d76ab7e8-befb-4603-8946-c4247ef938d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 119
        },
        {
            "id": "c3415262-1f1f-4731-b8b9-aa67107975c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 120
        },
        {
            "id": "9284971b-8ea9-4ed9-a3d0-8bd8f5df802e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 121
        },
        {
            "id": "29b60dff-8015-419e-8766-08d429f501c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 122
        },
        {
            "id": "7339d53c-eb45-4fa5-9fab-6bdfdc911404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 894
        },
        {
            "id": "c8dc71a4-0079-4025-8c33-a41417b91674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 33
        },
        {
            "id": "3dda7198-250b-42eb-9fd4-539a21ae426f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 34
        },
        {
            "id": "dd2f06e3-ea68-48bf-b58e-c771f4f060d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 35
        },
        {
            "id": "219afd99-bee1-4efb-8546-e36c3732a9c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 39
        },
        {
            "id": "cdff5987-91f0-49dc-a7e7-5e27557b77dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 40
        },
        {
            "id": "75bd1873-a04c-47e6-a4a1-b5fecc4abdef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 41
        },
        {
            "id": "75883573-da98-4b89-ae93-f00f93aa0ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 47
        },
        {
            "id": "fc26c9ed-4605-4af7-ba79-ff093e62faa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 49
        },
        {
            "id": "2eb21320-90ff-4de6-8a8f-80d5f5dc2d05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 50
        },
        {
            "id": "e3c89488-bf39-4fd1-a4ce-f6eaeef3d562",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 51
        },
        {
            "id": "adee098d-4133-4cd8-8cab-77ecbf3986e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 52
        },
        {
            "id": "203bc102-538b-42c7-ab55-3dee056d4a0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 53
        },
        {
            "id": "04b62b38-7373-4c95-a524-2b531616bc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 55
        },
        {
            "id": "6b1f567b-42ec-4b17-89eb-feefba887483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 56
        },
        {
            "id": "7ef8f721-92fe-4c0a-bd7d-344924051516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 57
        },
        {
            "id": "65acf54d-8bd5-495a-b921-dd61bff06fce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 58
        },
        {
            "id": "866c2a6a-c6fd-4a0a-bbac-12ca2415c697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 59
        },
        {
            "id": "6499e89e-c916-43b7-b955-396628fed09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 63
        },
        {
            "id": "87eb9b31-2a22-4b08-91fa-70f8b12f8778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 64
        },
        {
            "id": "6fe10267-e5a1-49e6-861c-4236adef8526",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 92
        },
        {
            "id": "0ec94d93-22f5-477d-978a-5bb9684dbeca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 93
        },
        {
            "id": "6e5c37e7-352c-469b-81e6-52134c55e7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 100
        },
        {
            "id": "f5d3c1f2-4a6b-4bf7-9b40-5d7c8ece988c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 103
        },
        {
            "id": "1c505b38-31e1-49a9-8aa9-089ffd451dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 105
        },
        {
            "id": "be6850d3-3f81-438d-b157-867d86302cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 106
        },
        {
            "id": "4013ee91-852a-49c2-be6f-ffe8d51931f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 113
        },
        {
            "id": "73c20fc5-a581-47de-b7c1-4428baf97239",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 115
        },
        {
            "id": "725e39df-aaa9-441d-bd4e-4b56bcbd5588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 118
        },
        {
            "id": "fa442aec-90ae-489f-9924-532823018810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 119
        },
        {
            "id": "d952ead3-6407-4cef-85ad-9a90cbfa8dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 120
        },
        {
            "id": "e427b0f0-0b4b-4b63-bdcd-ecfa77ab161b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 121
        },
        {
            "id": "78863099-f17f-41e0-ac2f-80f521871fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 122
        },
        {
            "id": "ce5361d7-154f-4e8c-9789-5516fa203478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 894
        },
        {
            "id": "7b5d6989-e9c4-4530-8bbd-470290dff9b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 33
        },
        {
            "id": "672640d9-b89e-4d8d-8a66-0d336acc8788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 34
        },
        {
            "id": "fd0f5f5e-c096-4856-b950-5ba4d9559212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 35
        },
        {
            "id": "f5c8a987-761a-481a-b85b-32d2ddb9df81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 39
        },
        {
            "id": "54129952-0e1d-46d4-8b12-56cb1dd13302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 40
        },
        {
            "id": "411c3c0b-7545-44ed-b8ff-14bffafea258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 41
        },
        {
            "id": "5c32fa5d-994d-4376-b924-8e9fe0c23f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "c12d6065-8769-433f-b755-2ff9c5d7e65b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 47
        },
        {
            "id": "440b6045-7c11-458e-ac37-a4bd45db0bd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 48
        },
        {
            "id": "f13228be-444e-4ca1-a585-0ab98795c7a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 49
        },
        {
            "id": "4145420f-4f7d-4973-a9e1-3ea3374434cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 50
        },
        {
            "id": "812074aa-0c3c-46c6-891a-5b56b97e1675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 51
        },
        {
            "id": "222a696b-cd15-4377-abd1-0da690790f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 52
        },
        {
            "id": "bd8c1ba8-94a4-4ffd-9a48-9e5291fd2d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 53
        },
        {
            "id": "8fff0e20-4c77-42f4-80d7-2d7bb8a0a466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 54
        },
        {
            "id": "bca0299a-1c8b-4d0a-a8dc-701e80544748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 55
        },
        {
            "id": "f5ff35c2-18d3-4892-919b-51cb22103ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 56
        },
        {
            "id": "4b1d1ecc-d276-49ef-a883-02c2b7a491d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 57
        },
        {
            "id": "33f61a08-4053-450d-a029-0b0f29ee04d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 58
        },
        {
            "id": "a3a63695-e665-4d46-80bf-ba4b809c9a95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 59
        },
        {
            "id": "5f248bf4-ef13-40cb-b1e8-4d838e0f1f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 63
        },
        {
            "id": "6e97f19d-a51c-4f34-97c8-4827e6e9ac65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 64
        },
        {
            "id": "17701e69-013e-4d07-8734-e4ff0f3034f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 92
        },
        {
            "id": "25585ac6-2246-4d19-9730-64d6563b0344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 93
        },
        {
            "id": "1fcd1b8b-9b20-4bd5-a7b5-40ecef0192f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 97
        },
        {
            "id": "dff8be48-278a-445f-a710-135e154f5b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 98
        },
        {
            "id": "9bcb5a85-15e8-4956-bd92-8039a8f5af8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "27aa7bb4-9a39-4996-a269-f63b56acae72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "a0b81a1d-6b51-4faa-8d4c-2bdd908695d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "2038b8dd-f61b-4509-9dd9-b8e12d5728e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 102
        },
        {
            "id": "43a6db0f-cc25-4f3b-ad20-e474a121a29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "6d0b3fb4-aca6-47fb-a3ec-96814ae21771",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 104
        },
        {
            "id": "02bfd8db-0ecd-4740-bf82-4a0f7930ba69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 105
        },
        {
            "id": "8d24efd9-e0a8-4fe3-bb87-b86480d92c7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 106
        },
        {
            "id": "097d5b65-c165-4da3-8f68-5fae370a38e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 107
        },
        {
            "id": "3d48b918-9e7b-409e-8637-7966ed0a46c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 108
        },
        {
            "id": "22d24d20-4dc2-44e5-8633-0c23cfa3b82f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 109
        },
        {
            "id": "4d1432e6-62f4-4f3c-b7a6-e069c805056c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 110
        },
        {
            "id": "990d5489-2275-4d4f-a414-7c8aae026d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "6f2948b8-ce9f-4b27-a3ff-2accc699ec42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 112
        },
        {
            "id": "70eccaa8-f763-4c65-9755-735fe8aaa6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "cfcc56d9-34e2-4441-a13d-796aa6a2a111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 114
        },
        {
            "id": "afd5943b-4afc-4acb-935e-c9cac74311dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 115
        },
        {
            "id": "63e5a61b-c97a-4531-b909-5fbac1f1e281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 116
        },
        {
            "id": "ac38e68c-4b2d-4e90-b72b-b1868f47fb1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 117
        },
        {
            "id": "01e86b47-4e92-428e-8df4-c073e2c2bf99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 118
        },
        {
            "id": "fa0b8db3-e48b-4cd2-ac38-5694dfa187e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 119
        },
        {
            "id": "c4f52b7f-75da-4b3b-b0e4-0e7dcdcf7a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 120
        },
        {
            "id": "98c41e01-266a-4bd3-9f53-d1965953a35c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 121
        },
        {
            "id": "e84413fc-06bd-429c-a1b5-8cf0ff5a8ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 122
        },
        {
            "id": "ad865919-fc93-448b-906e-b8b687396fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "341f3e96-101d-4109-957c-a96d615a0c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 894
        },
        {
            "id": "40a5b4e9-3e23-43d9-bfe6-1b7e7fc17ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 8208
        },
        {
            "id": "c48dec81-8d59-4f55-9a23-e35e5f494852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 33
        },
        {
            "id": "fde88bac-015a-4177-aeb0-9f0f80bf2172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 34
        },
        {
            "id": "545b4044-fbf4-43d7-8441-97afe603f08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 35
        },
        {
            "id": "e07aa09e-9a7d-4f7b-b85d-ea2cf19f2c5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 39
        },
        {
            "id": "077cc07c-9e50-42f5-bebd-391877c90f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 40
        },
        {
            "id": "69ebb9cb-cfcd-419a-a324-7641f1de2843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 41
        },
        {
            "id": "9a844d92-d0a9-42a2-91fc-78dd909837fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "be6d00f2-28a5-4236-8840-bf4e22c3bdef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 47
        },
        {
            "id": "04ee988e-29ec-4411-b369-49b5c6e5832f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 49
        },
        {
            "id": "3b90643e-0799-41de-bd94-6b3ca76a6d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 50
        },
        {
            "id": "d3a94b77-a145-4be2-b83b-4526ef0d818b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 51
        },
        {
            "id": "9203696b-fd09-42b7-a16c-dad6bc45d1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 52
        },
        {
            "id": "8cf18020-61b0-4258-b4e7-730aeb7115d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 53
        },
        {
            "id": "36866019-0ff4-4fe4-b55a-248420001aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 55
        },
        {
            "id": "157d126f-d8a9-46f3-9e39-569d462d3a55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 57
        },
        {
            "id": "c33b7a5d-d9b3-4ea2-aae1-04369b3b127a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 58
        },
        {
            "id": "571e7479-e2d9-4025-89cb-7aacb3148e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 59
        },
        {
            "id": "a9ccc20a-53be-4a75-8c08-3cea35a294ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 63
        },
        {
            "id": "99af05fa-33a5-4430-963d-73eb29d66b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 92
        },
        {
            "id": "a148008c-8714-4001-ab3d-bbfd8be2829c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 93
        },
        {
            "id": "1a763f35-edc9-43c3-98e3-fdad7c9aa472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 103
        },
        {
            "id": "b5e1f251-f65b-499c-b5c5-053b4047515c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 105
        },
        {
            "id": "b0152c54-9b33-4bdd-a1a2-7de5167ab82e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 106
        },
        {
            "id": "fb7d600c-6d11-49fa-8ef3-03794cea9948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 113
        },
        {
            "id": "2bc87eeb-17cf-4883-a780-7a5fc25d58e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 115
        },
        {
            "id": "134329a3-554f-4f40-83fe-d3fd867160a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 118
        },
        {
            "id": "c9c25d21-c9c0-4288-96f4-68ebab9c8861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 119
        },
        {
            "id": "90ec8392-0baa-4bc5-88ba-22a47a4d19f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 120
        },
        {
            "id": "8aa09f7d-bc26-4a30-95bd-10374e246270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 121
        },
        {
            "id": "e5b4de7e-37cc-4f2a-a0ec-975811ecd924",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 122
        },
        {
            "id": "e536a72d-b6c6-4c5e-baab-82db4682a320",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 894
        },
        {
            "id": "334da9c6-1c9d-4572-96ec-788414880272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 33
        },
        {
            "id": "473c1ca2-f409-4ca7-bfcb-8c74068a7eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 34
        },
        {
            "id": "cd578417-a15f-48fd-b7de-e97bf6773c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 35
        },
        {
            "id": "6d55aaf7-bc09-4f9c-8b71-5815db44bc29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 39
        },
        {
            "id": "0cd3b6b6-1aa2-4ad6-b24f-46c188ed7ea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 40
        },
        {
            "id": "1b4e2b31-e518-4274-9677-968aaf4aaa6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 41
        },
        {
            "id": "2577eddf-9425-43c7-a53e-a80927a14767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 45
        },
        {
            "id": "83a0b080-cb79-428a-976f-1e05c4318b81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 47
        },
        {
            "id": "14645be1-e04c-47e3-879e-5350d0a0df6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 48
        },
        {
            "id": "dbfae446-e5f8-4cc9-9a2c-30c11b59ee24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 49
        },
        {
            "id": "814db04b-b5bd-488e-848c-4d5d9f5dfc5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 50
        },
        {
            "id": "ec5a2a6c-ff6c-4d08-aec8-4c7bbc72ab34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 51
        },
        {
            "id": "d8f9646f-ef91-449a-95eb-076fcf52973b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 52
        },
        {
            "id": "f3878299-1c50-4372-aa6f-01136f0d3ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 53
        },
        {
            "id": "bb09b583-6b4d-4dbb-b4d3-c5da825afaba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 54
        },
        {
            "id": "7d8dfdcf-ab1a-432b-aeaf-1ae713725a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 55
        },
        {
            "id": "96db7aed-5972-4f78-8d53-2bebbc2de9c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 56
        },
        {
            "id": "9f920b10-e070-4ceb-8957-865b55a2bcef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 57
        },
        {
            "id": "873356a8-705d-4f8b-aa2d-8ffa66cfd966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 58
        },
        {
            "id": "004bec57-5eb9-4bb3-bd2e-ec26c872947c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 59
        },
        {
            "id": "e7ed72d4-d0f2-4ad6-9874-353a068be1b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 63
        },
        {
            "id": "3e33efeb-0b07-4304-a0a1-8f4808f94bfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 64
        },
        {
            "id": "8145b620-4822-49e6-afc5-5ff7c87672ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 92
        },
        {
            "id": "3f57ba9f-dead-43ec-9832-88600cdd1c73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 93
        },
        {
            "id": "307d4bfa-795f-41a4-aa86-3357c8e19635",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 97
        },
        {
            "id": "98b85017-da11-43fd-a41c-c583ec0f6361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 98
        },
        {
            "id": "e9ffbbfe-d1fc-4971-a5a2-b7bb35634c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 99
        },
        {
            "id": "f6511e38-5649-4b05-b364-fc1b7fa16a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 100
        },
        {
            "id": "eef8b159-e68e-4a54-9db2-5f720c0c86a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 101
        },
        {
            "id": "9363d299-72fd-4930-8c3f-b9c7cc63d5b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 102
        },
        {
            "id": "3093f80d-ac06-43da-bdde-b47bf1640b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 103
        },
        {
            "id": "910ed7fb-95b7-438b-91c5-3fd6fa42634f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 104
        },
        {
            "id": "2eeb9619-aced-421b-934b-c0c77350f516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 105
        },
        {
            "id": "8e5daeed-cd8b-4dd8-b14c-2e4c84ce306e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 106
        },
        {
            "id": "3c76b477-bac4-483c-b856-d7c4ba50e366",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 107
        },
        {
            "id": "46fb6506-d57a-402f-8e9b-483ddf0468df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 108
        },
        {
            "id": "a66c2f62-030c-45c8-8db2-2223022c6469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 109
        },
        {
            "id": "770d995b-ae48-4ebf-ab5c-89cc3598c044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 110
        },
        {
            "id": "5fb7c71c-cc7d-4494-a695-f1e99030bbfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 111
        },
        {
            "id": "54fe34d5-bef9-4918-9863-4092e2a62ae1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 112
        },
        {
            "id": "bc92782a-31cb-445a-9eda-cda3df217872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 113
        },
        {
            "id": "40b70873-3b75-4c74-a60e-df1fe9f75876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 114
        },
        {
            "id": "d16f69e2-7489-4689-bcad-45328755cae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 115
        },
        {
            "id": "26588bfe-085e-4c1c-abd4-1775e56ecf19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 116
        },
        {
            "id": "dc50a4d8-5300-4b75-9343-fff7880d8d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 117
        },
        {
            "id": "a4528e78-4aad-48fd-a8d5-91b227d19c09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 118
        },
        {
            "id": "9d185f22-0baa-4b01-bfc9-8ce8054cd6a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 119
        },
        {
            "id": "0fc2f202-a148-4e4d-89c8-b40dbc7481ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 120
        },
        {
            "id": "d177dbd2-0dfe-4416-b61f-81a38ada0054",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 121
        },
        {
            "id": "c643fe12-c957-4249-93d3-98b6d6c11392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 122
        },
        {
            "id": "34d70dd8-7bf0-4c30-8ef9-1178a2904e07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 173
        },
        {
            "id": "207fbd0b-31dc-42ba-a933-75e1384f31f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 894
        },
        {
            "id": "ffb3eda6-ba90-48f6-8340-4841ec8c6867",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 122,
            "second": 8208
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 14,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}