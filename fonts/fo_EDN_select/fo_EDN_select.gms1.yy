{
    "id": "9d63c4b8-3d95-42d8-89ef-5322fba79df5",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fo_EDN_select",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Gabriola",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7fb56782-c180-4373-8345-5b418672091c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 87,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 137,
                "y": 209
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ae38e23d-ac68-4db3-944e-a1dd86cf6fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 56,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 303,
                "y": 209
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "4f467011-8749-433a-a627-a5fc65a5f8df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 292,
                "y": 209
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2967fb83-92a5-4dd0-b4ad-8aae0c53ae36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 56,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 452,
                "y": 71
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "47557767-714b-4f2a-8458-97ef11b6e1b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 59,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 151,
                "y": 140
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4cd9bf84-dcc4-43e7-8805-b664a141c285",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 58,
                "offset": 1,
                "shift": 34,
                "w": 32,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e00d6220-c967-4bc1-b9e9-b0e309335cb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 56,
                "offset": 1,
                "shift": 25,
                "w": 26,
                "x": 303,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2fba75b9-3ba1-4f5e-bedc-af6142a02e2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 337,
                "y": 209
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "ee2b73a1-5ef8-4a1f-b0dc-f0cf3f4b7230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 64,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 173,
                "y": 209
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ca72ceb4-9453-482c-87ed-f08794f22b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 64,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 161,
                "y": 209
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2a6ec4ea-afde-443b-87cc-01c0260cb402",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 209,
                "y": 209
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8cea784e-b807-4907-a2db-9c379370252c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 56,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 284,
                "y": 140
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f4df7ff0-f3da-4a22-bddf-8aea7c2bc50e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 62,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 275,
                "y": 209
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0e800ebf-59d4-4a0e-be3c-e511de652591",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 196,
                "y": 209
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "f69e0e51-0c14-4b06-9f62-ec2c623cd506",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 56,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 326,
                "y": 209
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "0e973b57-e06f-4481-8df0-9120506e504c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 60,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 97,
                "y": 209
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "577353d9-b18c-4bd9-8742-056a284c1e59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 246,
                "y": 140
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "af7f8f85-243f-43e3-870b-54f2eca3b2de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 56,
                "offset": -1,
                "shift": 11,
                "w": 9,
                "x": 234,
                "y": 209
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "b281594c-fcb0-4238-9c9a-d176a9d623c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 56,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 209
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d71b44ce-c31d-45d0-a337-20134e6d0a61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 64,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 41,
                "y": 140
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "01366a08-9063-4abd-a98c-f3cea2ed84b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 64,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 305,
                "y": 71
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "df6d03c4-b3e1-435a-b752-987ce3803ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 64,
                "offset": -1,
                "shift": 16,
                "w": 16,
                "x": 59,
                "y": 140
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8f71fedd-ee75-421e-b8d2-9da8ed11b91d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 227,
                "y": 140
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7a64ca2e-2665-4660-bb5f-62050dcd3708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 65,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 391,
                "y": 71
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "21fee09b-7e50-4428-85da-6298355580f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 56,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 320,
                "y": 140
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4b7f6619-3170-405f-8eb0-d3e9b8e7fd08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 64,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 23,
                "y": 140
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "c2aace53-a418-4be6-98c8-1044e2f70284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 56,
                "offset": 2,
                "shift": 8,
                "w": 5,
                "x": 311,
                "y": 209
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "71b3d9df-fb6d-4d00-a7fe-3be570031dcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 62,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 284,
                "y": 209
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "9dc66aac-dc11-4f65-ba66-faff622300ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 55,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 356,
                "y": 140
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "1eec12f4-df90-4455-a69b-4a167704aa55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 53,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 374,
                "y": 140
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3422bb39-5843-4924-8331-a7aa2b1324c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 55,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 338,
                "y": 140
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8c8fe791-d724-4168-8ef2-294333e65da5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 56,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 443,
                "y": 140
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "89973227-2497-4d4e-b2f9-4bf98ba7a844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 64,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "15875b89-ab64-49ae-8397-0efd14be0fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 56,
                "offset": -1,
                "shift": 23,
                "w": 26,
                "x": 331,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "585458cf-f436-4791-a922-db570bfe1869",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 325,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "7d70812b-d4df-40c0-abee-bf32b4f9df38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 56,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 282,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "edc6db7c-c366-4ab0-abd7-8ab1cda5f0c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 56,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 438,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "65de24c7-d7c5-4469-bb8f-1a4b6a7d560d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 56,
                "offset": 0,
                "shift": 20,
                "w": 19,
                "x": 473,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "fa2777e8-993f-48f0-ad43-51b12bafad73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 56,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 77,
                "y": 140
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7f4bcb76-cbed-4413-8474-447dd486055a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 56,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 236,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4b575da9-caef-4bf2-85f8-089bcad1c858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 56,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "59d92a6f-745c-44d9-aa6a-bdd490a79b12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 56,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 255,
                "y": 209
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "87ff105f-9ea8-4a3e-bb5c-197c15f26602",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 67,
                "offset": -7,
                "shift": 10,
                "w": 15,
                "x": 134,
                "y": 140
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8a7c2075-2dfa-41c6-8c82-55d3923d44c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 24,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e89003fe-8faf-4125-ac89-8a61c027ba39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 410,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "942a1306-8375-49bf-b476-1d914cec3a7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 56,
                "offset": 0,
                "shift": 30,
                "w": 31,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "614c4912-60bb-4837-b662-2fac9086588d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 56,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 412,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "0e3e7bb6-a991-4ff8-aea3-761804c23e30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 56,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 72,
                "y": 71
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "bb9e273f-2548-472b-a0b2-f9d40093e77e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 2,
                "y": 140
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "dbb285a2-5b9e-43e6-82c0-f838dc30028e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 67,
                "offset": 1,
                "shift": 25,
                "w": 44,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fabcd270-9118-4e18-9927-cc03751a5eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 24,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "e5c46afa-002b-40a3-9b63-301439ee2532",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 56,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 426,
                "y": 140
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e4b54bc7-ab5c-4068-85cb-43ec0315328d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 56,
                "offset": 0,
                "shift": 19,
                "w": 21,
                "x": 259,
                "y": 71
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1a5186e5-7905-4e16-97c4-1d1526af9b3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 56,
                "offset": 0,
                "shift": 26,
                "w": 28,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "e25379fe-3efd-4ee3-8b0c-f6b8392ee87b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 56,
                "offset": -2,
                "shift": 21,
                "w": 24,
                "x": 386,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "1f54b651-e586-4417-b867-38bb863887b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 56,
                "offset": -2,
                "shift": 33,
                "w": 36,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "38692e63-5489-44fe-9703-10046d20a6b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 56,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 97,
                "y": 71
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d6691b35-2ad0-4ba5-a99c-bfa608947fee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 56,
                "offset": -2,
                "shift": 19,
                "w": 22,
                "x": 212,
                "y": 71
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "25d9334a-6202-4014-b1a2-8a34f3c357fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 56,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 369,
                "y": 71
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "9e89f541-697d-4a68-be12-372d371a7409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 64,
                "offset": 3,
                "shift": 12,
                "w": 9,
                "x": 185,
                "y": 209
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "8c850afe-7e2c-4f6a-bb4b-6d67a1452fc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 60,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 18,
                "y": 209
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9e3db80f-7136-45ca-bbb3-bb88e590862f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 64,
                "offset": 1,
                "shift": 12,
                "w": 8,
                "x": 224,
                "y": 209
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "80dd1960-1d40-4eab-9882-e4814a301949",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 45,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 33,
                "y": 209
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "58d01bc0-37aa-407d-987e-5efa9b97b0bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 62,
                "offset": -2,
                "shift": 7,
                "w": 13,
                "x": 460,
                "y": 140
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d3ce76e8-d350-4301-b8b2-32fd42102772",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 38,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 318,
                "y": 209
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6a0f325e-29ee-4ee0-9329-90765cdf5132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 56,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 265,
                "y": 140
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "9ba3fa37-2840-42ac-b493-82e9157df200",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 56,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 208,
                "y": 140
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "196f2232-4c68-42dc-aa88-2c4326b2bcda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 56,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 52,
                "y": 209
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5ac0db19-73ed-44fc-b5bb-7d9d10956303",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 56,
                "offset": 1,
                "shift": 20,
                "w": 20,
                "x": 347,
                "y": 71
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "fe47310e-948b-49cc-bae0-3671917506e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 56,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 82,
                "y": 209
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6ae0a327-d9f2-4aed-9fe7-83832e311edd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 67,
                "offset": -8,
                "shift": 11,
                "w": 26,
                "x": 150,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c2783d84-d0d6-481d-b196-9e61d44d2ded",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 67,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 122,
                "y": 71
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "52261564-36ea-40b5-9671-ac88ce1a7ca1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 56,
                "offset": 2,
                "shift": 19,
                "w": 18,
                "x": 97,
                "y": 140
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "909dfe55-d6d8-4f93-b4b3-1840eccf2350",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 56,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 245,
                "y": 209
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "65426cac-575c-4df3-9c3e-28b6426874ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 67,
                "offset": -8,
                "shift": 8,
                "w": 15,
                "x": 117,
                "y": 140
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "6da55fce-b879-42e4-8057-3ab618f4fd3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 56,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 170,
                "y": 140
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "0ffabfe4-47eb-4b26-8e0c-22c19f91d093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 56,
                "offset": 2,
                "shift": 9,
                "w": 8,
                "x": 265,
                "y": 209
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "55bc46a0-1020-4a0e-b190-aa11290acf23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 56,
                "offset": -1,
                "shift": 29,
                "w": 31,
                "x": 178,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "1bc80d3b-c97e-4f2d-bfc7-d6a552953761",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 56,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 164,
                "y": 71
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "23d57f4e-9284-42a0-811b-4dda8be0d148",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 56,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 392,
                "y": 140
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "86675e4d-5066-4054-a402-1eb9e886f311",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 67,
                "offset": -1,
                "shift": 19,
                "w": 20,
                "x": 50,
                "y": 71
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "92d4ee66-7fe0-425e-8c27-06b48e2ca411",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 67,
                "offset": 1,
                "shift": 18,
                "w": 20,
                "x": 28,
                "y": 71
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "6f6d4872-3982-41c7-b621-8a8fe01516c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 56,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 409,
                "y": 140
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5df56043-575a-4c23-9df7-9953aa262b6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 56,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 67,
                "y": 209
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "2acdf6db-6c1d-4857-a50c-adb11716547d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 56,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 147,
                "y": 209
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2559a5a6-9106-47e3-a3df-8d6a00845307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 56,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 188,
                "y": 71
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c02ca6c1-1295-4768-8893-320b45f5349b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 56,
                "offset": -2,
                "shift": 15,
                "w": 17,
                "x": 189,
                "y": 140
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "ea8af67d-0f31-4652-9853-51e0ba93caf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 56,
                "offset": -2,
                "shift": 23,
                "w": 25,
                "x": 359,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e90ed582-1958-4f28-ac73-0a13ef5f8b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 56,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 431,
                "y": 71
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "2efea4b5-a0d1-4cba-a4ea-42fedb574720",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 67,
                "offset": -2,
                "shift": 16,
                "w": 19,
                "x": 143,
                "y": 71
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "409777f3-d244-4ebf-8bad-7fa1d893239a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 56,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 302,
                "y": 140
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5d4bb333-254a-461e-8f9b-5f755449b941",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 64,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 124,
                "y": 209
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "bcd1cbdf-7eb5-4240-8e7b-071c6582fb4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 67,
                "offset": 5,
                "shift": 13,
                "w": 3,
                "x": 332,
                "y": 209
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "013e4811-d76c-421d-b102-37d3c41cc783",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 64,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 111,
                "y": 209
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "b4dbffa1-48cc-4c9b-b99f-2ff3e0c5fe23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 493,
                "y": 140
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "af400fce-1e33-44c0-96f3-7f9334b7ee19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 475,
                "y": 140
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 35,
    "styleName": "Regular",
    "textureGroupId": "00000000-0000-0000-0000-000000000000"
}