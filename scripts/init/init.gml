gml_pragma("global", "init()");

#region Particle Types

// Basic Particle
var _p = part_type_create();
part_type_color1(_p,make_colour_hsv(0, 192, 128));

part_type_shape(_p, pt_shape_square);
part_type_life(_p, 20, 30);
part_type_scale(_p, 0.2, 0.2);
part_type_size(_p, 0.1, 0.1, 0.02, 0);
part_type_speed(_p, 4, 6, 0, 3);
part_type_direction(_p, 0, 360, 0, 0);
part_type_orientation(_p, 0, 0, 0, 0, true);

global.ptBasic = _p;

// Trail Particle
part_type_color1(_p,make_colour_hsv(0, 192, 128));

var _p = part_type_create();

part_type_shape(_p, pt_shape_pixel);
part_type_life(_p, 50, 100);
part_type_size(_p, 0.1, 0.1, -0.01, 0.05);

global.ptTrail = _p;

// Set as step
part_type_color1(_p,make_colour_hsv(0, 192, 128));

part_type_step(global.ptBasic, 1, global.ptTrail);

// Death explosion
var _p = part_type_create();
part_type_color1(_p,make_colour_hsv(0, 192, 128));

part_type_shape(_p, pt_shape_pixel);
part_type_life(_p, 50, 100);
part_type_size(_p, 0.1, 0.1, 0.02, 0);
part_type_alpha3(_p, 0.8, 1, 0);
part_type_blend(_p, true);

global.ptDeathExplosion = _p;

// Set as death
part_type_death(global.ptBasic, 1, global.ptDeathExplosion);

#endregion