{
    "id": "12f388a0-1196-410b-9c62-57a2a7c9e1af",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset_dark_forest2",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 2,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "ac2c0c58-a5be-406f-92fa-a306c2cd4bed",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 5,
    "tileheight": 48,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 48,
    "tilexoff": 0,
    "tileyoff": 0
}